<?php

error_reporting(E_ALL);
ini_set("display_errors",0); 
ini_set("date.timezone", 'Asia/Calcutta');

session_name('examination_front');
session_start();

include_once("start.php");
$DB= new DBFilter();
$DB->ExecuteQuery("set time_zone = '+5:30'");
$DbHelper = new DbHelper();

$mod = '';
if(isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id']))
{
	if(isset($_GET['mod']))
		$mod = $_GET['mod'];
}
else
{
	if(isset($_GET['mod']) && $_GET['mod']=='export')
		$mod = $_GET['mod'];
}
if(isset($_GET['fblogin']))
{
	if(isset($_GET['mod']))
			$mod = $_GET['mod'];
}
if(isset($_GET['googlelogin']) && !empty($_GET['googlelogin']))
{
	if(isset($_GET['mod']))
			$mod = $_GET['mod'];
}
//check the proper mode also if there pagination string were added 
if(strrpos($mod,'?'))
{
	$exact_mod=explode('?',$mod);
	$mod=$exact_mod[0];
}


switch($mod)
{		
	case "guidelines":
	   include_once(MOD."/guidelines/index.php");
	   break;
	case "exam":
		include_once(MOD."/exam/index.php");
		break;
	case "result":
		include_once(MOD."/result/index.php");
		break;
	case "report":
		checker();	
		include_once(MOD."/report/index.php");
		break;
	case "export":
		include_once(MOD."/export/index.php");
		break;
	case "sample_test":
		include_once(MOD."/sample_test/index.php");
		break;
	case "sample_result":
		include_once(MOD."/sample_result/index.php");
		break;
	case "changepwd":
		include_once(MOD."/changepwd/index.php");
		break;
	case "feedback":
		include_once(MOD."/feedback/index.php");
		break;
	case "homework":
		include_once(MOD."/homework/index.php");
		break;
	case "save_homework":
		include_once(MOD."/save_homework/index.php");
		break;
	case "dashboard":
			
		include_once(MOD."/dashboard/index.php");
		break;
	case "account":
		include_once(MOD."/account/index.php");
		break;
	case "message":
		checker();
		include_once(MOD."/message/index.php");
		break;
	case "download_docs":
		checker();
		include_once(MOD."/download_docs/index.php");
		break;
	case "uploaddocs":
		include_once(MOD."/uploaddocs/index.php");
		break;
	case "buyonline":
		checker();
		include_once(MOD."/buyonline/index.php");
		break;
	case "latest_news":
		checker();
		include_once(MOD."/latest_news/index.php");
		break;
	case "help":
		checker();
		include_once(MOD."/help/index.php");
		break;
    case "payment":
        checker();
        include_once(MOD."/payment/index.php");
        break;
	case "buy_package":
        checker();
        include_once(MOD."/buy_package/index.php");
        break;
			
	default :
	case "user":
		include_once(MOD."/user/index.php");
		break;		
}

function checker() {
	$DB= new DBFilter();
	$candi_info = $DB->SelectRecord('candidate', "id = '".$_SESSION['candidate_id']."'");
	//echo '<pre>';print_r($candi_info); exit;
	$first_name = $candi_info->first_name;
	$birth_date = $candi_info->birth_date;
	
	if (filter_var($first_name, FILTER_VALIDATE_EMAIL)) {
		if($birth_date == '0000-00-00' ||  $birth_date == '1970-01-01') {
			Redirect(CreateURL('index.php',"mod=account&do=editaccount&id=".$_SESSION['candidate_id']."&show=1"));
		}
	}
}
?>