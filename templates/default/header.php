<?php 
function getOptionValue($option)
{
	$DBf = new DBFilter();
	$info = $DBf->SelectRecord('default_configuration', 'conf_variable ="'.$option.'"');
	if ($info->variable_value !='')
	{
		return $info->variable_value;
	}
	else
	{
		return false;
	}
}
?>

<div id="header" style="background-color:white">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
	  <tr>
		<td align="left" valign="top">
		<?php
			$src = '';
			if ($value = getOptionValue('left-logo'))
			{
				$src = ADMINURL .'/uploadfiles/' . $value;
			}
			if($src != '')
			echo "<img src='$src'>";
		?>
		</td>
		<?php
			$font_size = 'font-size:56px;';
			if ($value = getOptionValue('font-size'))
			{
				$font_size = 'font-size:'.$value.'px;';
			}
			$color= 'blue';
			if ($value = getOptionValue('color'))
			{
				$color = $value;
			}
		?>
		<td align="center" style="color:#<?php echo $color ?>;<?php echo $font_size; ?>font-weight:bold">
		<?php
			$text ='Recruitment Examination';
			if ($value = getOptionValue('middle-text'))
			{
				$text = $value;
			}
			echo $text;
		?>
		
		</td>
		<td valign="top" align="right">		
		<?php
			//$src = ADMINURL .'/uploadfiles/logo-right.JPG';
			$src = '';
			if ($value = getOptionValue('right-logo'))
			{
				$src =  ADMINURL .'/uploadfiles/' .$value;
			}
			if($src != '')
			echo "<img src='$src'>";
		?>
		</td>	
	  </tr>
	</table>
	<div style="text-align: right;font-size: 12px;margin-right: 10px;">&nbsp;
		<?php 
		if(isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] != '')) 
		{
			echo "Hello ".ucwords($_SESSION['candidate_fname'].' '.$_SESSION['candidate_lname']);
		} 
	?>
	</div>
</div>