<?php

 /*****************Developed by :- Akshay Yadav
				Module       :- Dashboard
				Purpose      :- Template for Browse all data
	***********************************************************************************/
?>
<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" href="<?php echo ROOTURL;?>/css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Dashboard</title>

<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.Charts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionChartsExportComponent.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.jqueryplugin.js"; ?>'></script>


</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
$candi = $DB->SelectRecord('candidate', "id = '".$_SESSION['candidate_id']."'");

?>
<body>
<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
		<div align="center">
		<?php
			if ($_SESSION['error'] != '')
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
		?>
		</div>
<?php include_once(TEMPPATH."/includes/header.php"); ?>
<section id="main_sec" style='overflow:visible'>
<?php include_once(TEMPPATH."/includes/left_sec.php"); ?>
<?php include_once(TEMPPATH."/includes/right_sec.php"); ?>
</section>

</body>
</html>
