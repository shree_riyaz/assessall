<?php
/**************************************************************************************
 * 		Developed by :- Richa verma
 * 		Date         :- 4-july-2011
 * 		Module       :- candidate master
 * 		Purpose      :- Template for Add/Edit particular candidate detail
 *************************************************************************************/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Online Payment</title>

</head>

<body onload="document.addcandidate.first_name.focus();showedu();showexp();checkMarriedStatus();">

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();

include_once 'success.php';
// Merchant id provided by CCAvenue
// Item amount for which transaction perform
$Merchant_Id = "44071";
$Amount = "1";
// Unique OrderId that should be passed to payment gateway
$Order_Id = "ORD-102030";
// Unique Key provided by CCAvenue
$WorkingKey= "F23076F6F6EDF23385C5B51C923C828F";
// Success page URL
$Redirect_Url="success.php";
$Checksum = getCheckSum($Merchant_Id,$Amount,$Order_Id ,$Redirect_Url,$WorkingKey);
?>

<div id="outerwrapper">
    <table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
        <tr>
            <td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
        </tr>
        <tr>
            <td>
                <div id="content">
                    <?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
                    <div id="main">
                        <div id="contents">
                    <form id="addpayment" name="addpayment" method="post" action="https://world.ccavenue.com/servlet/ccw.CCAvenueController">
                                <?php
                                    if(isset($_SESSION['error']))
                                    {
                                        echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
                                        echo $_SESSION['error'];
                                        echo '</td></tr></tbody></table>';
                                        unset($_SESSION['error']);
                                    }
                                    if(isset($_SESSION['success']))
                                    {
                                        echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
                                        echo $_SESSION['success'];
                                        echo '</td></tr></tbody></table>';
                                        unset($_SESSION['success']);
                                    }
                                    ?>

            <table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences" style="padding: 10px;">
                    <tr>
                        <td>
                            <input type="hidden" name="Merchant_Id" value="<?php echo $Merchant_Id; ?>">
                            <input type="hidden" name="Amount" value="<?php echo $Amount; ?>">
                            <input type="hidden" name="Order_Id" value="<?php echo $Order_Id; ?>">
                            <input type="hidden" name="Redirect_Url" value="<?php echo $Redirect_Url; ?>">
                            <input type="hidden" name="TxnType" value="A">
                            <input type="hidden" name="actionID" value="TXN">
                            <input type="hidden" name="Checksum" value="<?php echo $Checksum; ?>">
                            <input type="hidden" name="Currency" value="USD">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10" align="center">
<!--                            <input type="button" class="buttons" value="Buy Now" onclick="">-->
                            <input type="submit" value="Buy Now" />
                        </td>
                    </tr>

                    <tr><td>&nbsp;</td></tr>

            </table>

                            </form>
                        </div><!--Div Contents closed-->
                    </div><!--Div main closed-->
                </div><!--Content div closed-->
            </td>
        </tr>
    </table>
</div><!--Outer wrapper closed-->

</body>
</html>