<?php

 /*****************Developed by :- Rahul Gahlot
	            Date         :- 15-march-2014
				Module       :- Help
				Purpose      :- Template for Show Every Process
	***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Help</title>
<link rel="stylesheet" href="<?php echo ROOTURL; ?>/css/online_exam.css" type="text/css">
<link rel="stylesheet" href="<?php echo ROOTURL; ?>/css/exam.css" type="text/css"/>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/jquery-1.7.1.js"></script>
</head>
<link rel="stylesheet" href="<?php echo ROOTURL;?>/css/style.css" />
<style>


#subdiv li{ padding-top:5px; margin-top:0px; width:100%;height:25px; text-align:left}
.clear{ margin:0 0; padding: 0 0;clear:both;}
#contents{ margin:0 0; padding: 0 0;clear:both;}
#left_nev{ background:#fff; float:right; border-bottom:solid 1px #000; width:20%; }
#right_nev{ background:#fff; float:left; width:79%; min-height:150px; height:auto; border: solid 2px #1d5770 }
.list_menu{ width:100%;border: solid 1px #CCCCCC;}
.list_menu ul{ display:block;padding:0px; margin:0}
.list_menu ul li {height:25px;width:90%; padding-top:10px; list-style-type:none; border-bottom:solid 1px #ccc;}
.help_courses{ cursor:pointer;}
.list_menu ul li a { color:#333; text-decoration:none; padding-left:10%; display:block}
.Course_subdiv,.Unified_subdiv  { display:none;}
.Unified_subdiv, .CBT_subdiv, .Course_subdiv, .Sample_subdiv, .Promotion_subdiv, .Upgrading_subdiv, .Feedback_subdiv  li{ background:#fff; width:90%; font-size:10px; padding-left:5px;}
#subdiv{ display:none}
#content_text{ width:90%; margin-top:20px; margin-left:5%}
#heading h2{ text-align:left; font-size:14px; border-bottom:none !important}
#heading h4{ color:#666; text-align:left;  font-size:11px; border-bottom:none !important}
/*#manage_institute, #add_institute, #course, #manage_course,#add_course,#manage_cbt,#add_cbt{display:none;}
*/
.list_menu ul li:hover a{color:#1d5770; font-weight:bold}
.list_menu ul li:link a{color:#1d5770; font-weight:bold}
#Online,#My,#Upload,#Exam,#Buy,#Message,#Share{ display:none}
</style>
 <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">

$(document).ready(function() 
{
var id = $(this).text();
var id =$.trim(id);
var sub = id+'_subdiv';

$('.help_courses').click(function(){
	$('#Dashboard,#Online,#My,#Upload,#Exam,#Buy,#Message,#Share').hide();
var id_fw = $(this).text();

//$('#subdiv').toggle();
var id_l  = id_fw.split(' ');
var id = id_l[0]

$('#'+id).css('display','block');
var id =$.trim(id);
var sub = id+'_subdiv';
//alert(sub);
//$('#Dashboard_subdiv,#Online_subdiv,#My_subdiv,#Upload_subdiv,#Exam_subdiv,#Buy_subdiv,#Message_subdiv').not('#'+sub+' a').css('color','#FF0000');
$('#Dashboard_subdiv a,#Online_subdiv a,#My_subdiv a,#Upload_subdiv a,#Exam_subdiv a, #Buy_subdiv a,#Message_subdiv a,#Share_subdiv a').css('color','#000');
$('#Dashboard_subdiv a,#Online_subdiv a,#My_subdiv a,#Upload_subdiv a,#Exam_subdiv a, #Buy_subdiv a,#Message_subdiv a,#Share_subdiv a').css('font-weight','normal');
$('#'+sub+' a').css('color','#1d5770');
$('#'+sub+' a').css('font-weight','bold');
//$('.'+sub).slideToggle();

});

});


</script>


 
<body>
<?php include_once(TEMPPATH."/includes/header.php"); ?>
<section id="main_sec" style='overflow:visible; background:none !important'>
<?php include_once(TEMPPATH."/includes/left_sec.php"); ?>


			
			<div id="right_sec" style="margin-top:50px">
			
			<div id="right_nev">
			<div class='green_head_wrp'><div class="green_header">Help</div></div>
			<!---------------------Dashboard------------------->
			<div id="content_text">
			<div id="Dashboard">
        <div id="heading">
          <h1>Dashboard</h1>
          </div>
           	<p>User can see  his/her personal information, reports, information about last given test &amp; latest news and updates.</p>
          
			 <div id="heading"><h2>Personal details</h2></div>
			  <p>User can see his/her uploaded image, name and email id.</p>
			  
			   <div id="heading"><h2>Exams</h2></div>
			  <p>User can see the attempted exams and practice exams.</p>
			  
			 <div id="heading"><h2>Reports and Statistics</h2></div>
			  <p>User can see reports from auto-generated charts.</p>
			  <div id="heading"><h2>Last Test Information</h2></div>
			  <p>User can see the information of his/her last appeared exam.</p>
			<!--  <div id="heading"><h2>News and Updates</h2></div>
			  <p>User can see news and information related to exams.</p>-->
		   	</div>
	<!-------------------------Online Exam---------------------------->
	<div id="Online">
    	<div id="heading"><h1>Online Exam</h1></div>
        <p>User can select an exam from available examination list. </p>
		
		<div id="heading"><h2>Select Course</h2></div>
        <p>User have to select the course firstly. After that user can see a list of exams related to that selected course. </p>
		
		<div id="heading"><h2>Select Test</h2></div>
        <p>User can select a test for which he/she wants to apply. </p>
		<p>A new window will be generated, here you will find the general instruction for the examination. Read Carefully those instructions then click on 'next' to proceed, now you have to select one of these language(English/Hindi) for the exam, checked the check box and click on 'I am ready to begin'.</p>
          <p>The exam will get start, if user close the window before completing the exam, he/she can not select the other exam, he/she must complete the exam before selecting any other exam.
   </div>
<!--   ------------------My Account----------------------->
<div id="My" class="My">
      	<div id="heading"><h1>My Account</h1></div>
        <p>User can see his/her personal information in detail and can edit these data and image.</p>
		<div id="heading"><h2>Edit</h2></div>
		<p>Edit buttons are given on this page, user can edit his/her personal information as like name, address, mobile no., birth date except 'user name'.
    </div>
<!--	-------------------Upload Documents---------------------->
<!--
<div id="Upload" class="Upload">
	<div id="heading"><h1>Upload Documents</h1>
	</div>
	<p class="question">User can upload documents.</p>


</div>
-->
<!--	-------------------------Exam Reports ------------------------->
<div id="Exam" class="Exam">
	<div id="heading"><h1>Exam Reports </h1>
	</div>
	<p class="trade">User can see result sheet, which contains all the information related to the exam.</p>
	
	<div id="heading"><h2>Result Sheet </h2></div>
<p class="trade">User's result sheet contains exam name, exam date and result(Pass/Fail). </p>
	<div id="heading"><h2>Graphs </h2></div>
	<p>It can also contains details related to exams which shows performance of candidate in graph, and shows detail of exam as like student name, subjects name, question no., obtained marks and percentage.</p>
	<div id="heading"><h2>Result by Exam/Year </h2></div>
	<p class="trade">User can see a particular exam by selecting exam name and exam year.</p>
</div>
<!----------------------Buy Package Online------------------------>
<!--
	<div id="Buy" class="Buy">
		<div id="heading"><h1>Buy Package Online</h1>
		</div>
		<p class="equipment">User can buy examination package which are available. Package can be selected from list.</p>
		
	</div>
	-->
<!----------------------Message----------------->

<!--<div id="Message">
    	<div id="heading"><h1>Message</h1></div>
        <p>User can see messages which he/she retrives from owner of exams.</p>
   
	 </div>

-->
<!----------------------Share With Friends----------------->
<div id="Share">
    	<div id="heading"><h1>Share With Friends</h1></div>
        <p>User can share on social media.</p>
   
	 </div>


<!----------------------------------------------------->
			</div>
			
			</div>
			<div id="left_nev">
			<div class="list_menu" id="list_menu">
			<ul>
			
			<li class="help_courses" id="Dashboard_subdiv"><a>Dashboard</a></li>
			
			<li class="help_courses" id="Online_subdiv"><a>Online Exam</a></li>
			
			<li class="help_courses" id="My_subdiv"><a>My Account</a></li>
			
			<!--<li class="help_courses" id="Upload_subdiv"><a>Upload Documents</a></li>-->	
		
			<li class="help_courses" id="Exam_subdiv"><a>Exam Reports</a></li>
			
			<!--<li class="help_courses" id="Buy_subdiv"><a>Buy Package Online</a></li>-->
			
			<!--<li class="help_courses" id="Message_subdiv"><a>Message</a></li>-->
			
			<li class="help_courses" id="Share_subdiv"><a>Share With Friends</a></li>
			
			<!--<li><a href="#">Dashboard</a></li>
			<li class="help_courses" id="course_subdiv"><a>Course</a></li>
		
			
			<li class="help_courses"><a>Exam</a></li>
		
			<li class="help_courses"><a >Question</a></li>
		
			
			<li class="help_courses"><a >Reports</a></li>
			
			<li class="help_courses"><a>Feedbacks</a></li>
		<div class="Role_subdiv" id='subdiv'>
			<li class="sub"><a href="#">Exam Feedbacks</a></li>
			<li class="sub"><a href="#">Software Feedbacks</a></li>
			</div>
			<li class="help_courses"><a href="#">Change Password</a></li>-->
			</ul>
			
			</div>
			</div>
			
				<div class="clear"></div>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->
	<div class="clear"></div>
			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>

</div><!--Outer wrapper closed-->

</body>
</html>
