<?php
	/**
	 * 	@author	:   Rahul Gahlot
	 * 	@desc	: To show exam questiions.
	 */
	 $date=$_SESSION['sdate'];
$current_date= date('m/d/Y h:i:s A');


$currectTestId =0;
	if(isset($_GET['lang']))
	{	
		$_SESSION['lang'] = $_GET['lang'];
	}
	

if($_POST)
{
	$ques_id = $_POST['ques_id'];
	$last_disp_quest = $_POST['display_quest'];
}
if(isset($_POST['mark_review']))
{
	$_SESSION['review'][$ques_id] = $ques_id;
	$current = $last_disp_quest+1;
	$page = $current+1;
}
elseif(isset($_POST['mark_reviewfirst']))
{
	$_SESSION['review'][$ques_id] = $ques_id;
	$page = 0;
	
}
elseif(isset($_POST['next']))
{
	unset($_SESSION['review'][$ques_id]);
	unset($_SESSION['review_pages'][$ques_id]);
	$current = $last_disp_quest+1;
	$page = $current+1;
}
elseif(isset($_POST['gotofirst']))
{	
	unset($_SESSION['review'][$ques_id]);
	unset($_SESSION['review_pages'][$ques_id]);
	$current=0;
	$page = 1;
} 
elseif(isset($_POST['is_finish']) && ($_POST['is_finish']==1))
{
	Redirect(CreateURL('index.php',"mod=user&do=result"));
	exit;
}
elseif(isset($_GET['page']) && $_GET['page'])
{
	$current=$_GET['page']-1;
	$page = $_GET['page'];
}
elseif(isset($_GET['current']) && $_GET['current'])
{
	$current=$_GET['current'];
	$page = $current+1;
}

$current = isset($current) ? $current : 0;

$isReview = 0;
if(isset($_GET['review']) && ($_GET['review'] == 1))
{
	$isReview = 1;
	$countR = count($_SESSION['review']);
	if($countR <= 0)
	{ 
		Redirect(CreateURL('index.php',"mod=$mod"));
	}

	if(!in_array($_SESSION['test_id-'.$currectTestId]['question_id-'.$current], $_SESSION['review']))
	{ 
		if(!in_array($_SESSION['first_review'], $_SESSION['review_pages']))
		{
			$_SESSION['first_review'] = reset($_SESSION['review_pages']);
		}
		$current = $_SESSION['first_review'];	
		Redirect(CreateURL('index.php',"mod=$mod&review=1&page=$current"));
	}
}

if($isReview == 1)
{
	$_SESSION['last_marked_question'] = $current+1;
}
else
{
	$_SESSION['last_unmarked_question'] = $current+1;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Assess All</title>

<link type="text/css" rel="stylesheet" href="<?php echo ADMINURL; ?>/lib/syntaxhighlighter/styles/shCoreDefault.css"/>
<link type="text/css" rel="stylesheet" href="<?php echo ADMINURL; ?>/style/wmd.css" />
<link type="text/css" rel="stylesheet" href="<?php echo ROOTURL; ?>/css/mock_style.css" />
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/common.exam.js"></script>
<script type="text/javascript" src="<?php echo ADMINURL; ?>/js/swfobject.js"></script>
<script type="text/javascript" src="<?php echo ADMINURL; ?>/lib/syntaxhighlighter/scripts/shCore.js"></script>
<script type="text/javascript" src="<?php echo ADMINURL; ?>/lib/syntaxhighlighter/scripts/shBrushGroovy.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script language="JavaScript">
TargetDate = "<?php echo $date; ?>";
BackColor = "";
ForeColor = "black";
CountActive = true;
CountStepper = -1;
LeadingZero = true;
DisplayFormat = "%%H%% : %%M%% : %%S%% ";
FinishMessage = "It is finally here!";
CurrentDate="<?php echo $current_date ?>";

function newpage(path)
{
	document.examform.action = path;
	document.examform.submit();
}
</script>
<script>
function language_para(val)
{
if(val == 'e')
{
	document.getElementById('english_para').style.display='';
	document.getElementById('english_para1').style.display='';
	document.getElementById('hindi_para1').style.display='none';
	document.getElementById('hindi_para').style.display='none';
	document.getElementById('title_para').style.display='none';
	document.getElementById('title_para1').style.display='none';
}
if(val == 'h')
{
	document.getElementById('english_para').style.display='none';
	document.getElementById('english_para1').style.display='none';
	document.getElementById('hindi_para').style.display='';
	document.getElementById('hindi_para1').style.display='';
	document.getElementById('title_para1').style.display='none';
	document.getElementById('title_para').style.display='none';

}
}
function language_pref(val){
if(val == 'e')
{
	document.getElementById('english').style.display='';
	document.getElementById('english.1').style.display='';
	document.getElementById('english.2').style.display='';
	document.getElementById('english.3').style.display='';
	document.getElementById('english.4').style.display='';
	document.getElementById('english.5').style.display='';
	document.getElementById('hindi.1').style.display='none';
	document.getElementById('hindi.2').style.display='none';
	document.getElementById('hindi.3').style.display='none';
	document.getElementById('hindi.4').style.display='none';
	document.getElementById('hindi.5').style.display='none';
	document.getElementById('hindi').style.display='none';
	document.getElementById('title').style.display='none';
	document.getElementById('title.1').style.display='none';
	document.getElementById('title.2').style.display='none';
	document.getElementById('title.3').style.display='none';
	document.getElementById('title.4').style.display='none';
	document.getElementById('title.5').style.display='none';
}
 if(val == 'h')
{
	document.getElementById('hindi').style.display='';
	document.getElementById('hindi.1').style.display='';
	document.getElementById('hindi.2').style.display='';
	document.getElementById('hindi.3').style.display='';
	document.getElementById('hindi.4').style.display='';
	document.getElementById('hindi.5').style.display='';
	document.getElementById('english').style.display='none';
	document.getElementById('english.1').style.display='none';
	document.getElementById('english.2').style.display='none';
	document.getElementById('english.3').style.display='none';
	document.getElementById('english.4').style.display='none';
	document.getElementById('english.5').style.display='none';
	document.getElementById('title').style.display='none';
	document.getElementById('title.1').style.display='none';
	document.getElementById('title.2').style.display='none';
	document.getElementById('title.3').style.display='none';
	document.getElementById('title.4').style.display='none';
	document.getElementById('title.5').style.display='none';
}
}
	TargetDate = "<?php echo $_SESSION['sdate']; ?>";
	CurrentDate="<?php echo date('m/d/Y h:i:s A'); ?>";
	function saveToDataBase()
	{
		<?php if($isSampleTest) echo 'return true;'; ?>
		//alert('saving TO Database');

		var url = "<?php echo CreateURL('index.php',"mod=exam&do=save_time"); ?>";
		$.ajax({url:url});
		setTimeout(saveToDataBase, 10000);
	}
	$(function() {
	    $('.pagination a').tooltip({ content: function() { return getQuestionTooltip($(this).attr('id'));} });
	});

	function getQuestionTooltip(question_id)
	{
		return $('#tool-tip-'+question_id).html();
	}
	$(document).ready(function(){
		saveToDataBase();
	}); 
	 
	 $(document).ready(sizeContent);

//Every resize of window
$(window).resize(sizeContent);

//Dynamically assign height
<!--function sizeContent() {
    //var newHeight = $("html").height() - $("#header").height()-$("#question-area fieldset").height()-$("#underreview").height() - $("#footer").height() + "px";
    //$("#QuestionDiv").css("height", newHeight);
//}-->
</script>
<script>
/*function finishTests()
{
    var x = confirm("Are you sure you want to finish this Test?");
    if (x)
    { 
        //return true;
		//document.getElementById('tbl_totalquestions').style.display='none';
		//load('result.html');
    }
    else
    { 
		return false;
    }
}*/
function ahow_alert(val)
{	//alert(document.getElementById('finish').value);
//alert($("#total_time_remain").val());
		//alert(document.getElementById('hidden_finish').value);
	var subm = document.getElementById("total_time_remain").value;
	
	
	
	if(val==subm)
	{
		document.getElementById('hidden_finish').value = 1;
		//document.examform.submit();
		enb();
	}
	if(val<subm)
	{
		document.getElementById('hidden_finish').value = 1;
		enb();
	}
	
}
function enb()
{
	
	$('#finish').prop( "disabled", false );
}
function enablefinish()
{
	var fin_btn = document.getElementById('hidden_finish').value;
	//alert(fin_btn);
		if(fin_btn==1)
		{
			
			document.getElementById('finish').disabled=false;
		}
		else
		{
			document.getElementById('finish').disabled=false;
			
		}

}
function resetOption($form) {
	
			var form = document.getElementById("examform");
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox')
         .removeAttr('checked').removeAttr('selected');
		
}
 function show_pro()
		 {
			document.getElementById("QPDiv").style.display = 'none';
			document.getElementById("sectionSummaryDiv").style.display = 'none';
			document.getElementById("instructionsDiv").style.display = 'none';
			document.getElementById("show_qus").style.display = 'none';
			document.getElementById("conformsubmitDiv").style.display = 'none';
			document.getElementById("profileDiv").style.display = 'block';
		 }
 function show_ins()
		 {
			document.getElementById("show_qus").style.display = 'none';
			document.getElementById("sectionSummaryDiv").style.display = 'none';
			document.getElementById("QPDiv").style.display = 'none';
			document.getElementById("profileDiv").style.display = 'none';
			document.getElementById("conformsubmitDiv").style.display = 'none';
			document.getElementById("instructionsDiv").style.display = 'block';
		 }	

 function show_Qpaper()
		 {	
		 
		 
			document.getElementById("sectionSummaryDiv").style.display = 'none';
			document.getElementById("show_qus").style.display = 'none';
			document.getElementById("profileDiv").style.display = 'none';
			document.getElementById("conformsubmitDiv").style.display = 'none';
			document.getElementById("instructionsDiv").style.display = 'none';
			document.getElementById("QPDiv").style.display = 'block';
			
		 }	
 function show_sectionSummaryDiv()
		 {
			document.getElementById("show_qus").style.display = 'none';
			document.getElementById("profileDiv").style.display = 'none';
			document.getElementById("instructionsDiv").style.display = 'none';
			document.getElementById("QPDiv").style.display = 'none';
			document.getElementById("conformsubmitDiv").style.display = 'none';
			document.getElementById("sectionSummaryDiv").style.display = 'block';
			
		 }	

		 
function changeQues(val)
	{
		document.getElementById(val).style.display = 'none';
		document.getElementById("show_qus").style.display = 'block';
		document.getElementById('viewProButton').style.backgroundColor = '#C7DCF0';
		document.getElementById('finish').style.backgroundColor = '#C7DCF0';
		document.getElementById('viewQPButton').style.backgroundColor = '#C7DCF0';
		document.getElementById('viewInstructionsButton').style.backgroundColor = '#C7DCF0';
		document.getElementById('viewProButton').style.color = '#000';
		document.getElementById('viewQPButton').style.color = '#000';
		document.getElementById('viewInstructionsButton').style.color = '#000';
		document.getElementById('finish').style.color = '#999';
		
	}
	function changeQuesbyno(val)
	{
		document.getElementById(val).style.display = 'none';
		document.getElementById("show_qus").style.display = 'block';
		document.getElementById('viewProButton').style.backgroundColor = '#C7DCF0';
		document.getElementById('finish').style.backgroundColor = '#C7DCF0';
		document.getElementById('viewQPButton').style.backgroundColor = '#C7DCF0';
		document.getElementById('viewInstructionsButton').style.backgroundColor = '#C7DCF0';
		document.getElementById('viewProButton').style.color = '#000';
		document.getElementById('viewQPButton').style.color = '#000';
		document.getElementById('viewInstructionsButton').style.color = '#000';
		document.getElementById('finish').style.color = '#000';
		
	}
function activeLink(val)
{
		
		document.getElementById('viewProButton').style.backgroundColor = '#C7DCF0';
		document.getElementById('viewQPButton').style.backgroundColor = '#C7DCF0';
		document.getElementById('viewInstructionsButton').style.backgroundColor = '#C7DCF0';
		document.getElementById('finish').style.backgroundColor = '#C7DCF0';
		document.getElementById('viewProButton').style.color = '#000';
		document.getElementById('viewQPButton').style.color = '#000';
		document.getElementById('finish').style.color = '#999';
		document.getElementById('viewInstructionsButton').style.color = '#000';
		
		document.getElementById(val).style.backgroundColor = '#2F72B7';
		document.getElementById(val).style.color = '#fff';
}
function enableButton()
{
	

	//var enableButton =  document.getElementById('finish').disabled = true;
	//var end_time = document.getElementById('total_time').value;
	//alert(end_time);
	//var t = setTimeout("document.getElementById('finish').disabled = false;",5400000); 
// then enable the button in 5 seconds....
//	setTimeout(document.getElementById('finalSub').disabled = false, 5000);
}	

function conf_submit()
{
	var conf = confirm("Are you sure you want to submit this exam.");
	//alert(document.getElementById('is_finish').value);
	if(conf ==true)
	{	
		document.getElementById('is_finish').value=1;
		return true;
		//document.examform.submit();
	}
	else
 		{
			return false;
		}
}
function backtoqus()
{
	
	document.getElementById("show_qus").style.display = 'block';
	document.getElementById("sectionSummaryDiv").style.display = 'none';
	document.getElementById('conformsubmitDiv').style.display='none'
	document.getElementById('finish').style.color = '#000';
	document.getElementById('finish').style.backgroundColor = '#C7DCF0';

}
function gotoconf()
 {
			document.getElementById("show_qus").style.display = 'none';
			document.getElementById("profileDiv").style.display = 'none';
			document.getElementById("instructionsDiv").style.display = 'none';
			document.getElementById("QPDiv").style.display = 'none';
			document.getElementById("conformsubmitDiv").style.display = 'block';
			document.getElementById("sectionSummaryDiv").style.display = 'none';
			
 }	
 function setheight()
 {
// alert(screen.height);
 if(screen.height==858)
 {
 	document.getElementById("QuestionDiv").style.height='520px';
	document.getElementById("numberpanel").style.height='540px';
	document.getElementById("tbs").style.height='347px';
 }
 if(screen.height==960)
 {
 	document.getElementById("QuestionDiv").style.height='610px';
	document.getElementById("numberpanel").style.height='630px';
	document.getElementById("tbs").style.height='430px';
 }
 
 if(screen.height==1024)
 {
 	document.getElementById("show_qus").style.height = '695px';
	document.getElementById("profileDiv").style.height = '695px';
	document.getElementById("instructionsDiv").style.height = '695px';
	document.getElementById("QPDiv").style.height = '695px';
	document.getElementById("conformsubmitDiv").style.height = '695px';
	document.getElementById("sectionSummaryDiv").style.height = '695px';
	document.getElementById("QuestionDiv").style.height='695px';
	document.getElementById("numberpanel").style.height='725px';
	document.getElementById("tbs").style.height='520px';
 }
  if(screen.height==1152)
 {
 	document.getElementById("QuestionDiv").style.height='775px';
	document.getElementById("numberpanel").style.height='800px';
	document.getElementById("tbs").style.height='600px';
 }
 }
 </script>
<style type="">
body
{
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

.buttons{ background:#1C94C4; border-radius:0px}
.buttons:hover{ background:#666}
.currentTd{ border-radius:0px 0px 0px 0px}
#mark-review-parent{ border-bottom: solid 2px #1C94C4;}
#mark_review{ border: solid 2px #1C94C4;border-bottom:none;  background-color:#1C94C4; color:#fff}
#mark_review:hover{ background:#ccc; color:#1C94C4; }
#s1 p { margin-top:1px}
#wrp{ height:inherit}
#footer{ bottom:0px;position:absolute}
</style>
	
</head>

<body onLoad="setheight();document.getElementById('pWait').style.display='none';enableButton();enablefinish();document.getElementById('finish').disabled=true;">
<div id="wrp">
<!--
<table border="0" cellspacing="0" cellpadding="0" width="99%" id="tbl_outer" style="padding: 5px;">-->
	<div id="header">Assess All</div>

	<?php
  	if ($isSampleTest && (!isset($_SESSION['is_valid_test']) || ($_SESSION['is_valid_test'] == 0)))
	{
		echo "</table></div>";
		echo "<p align='center' style='font-family:arail,helvetica, sans-serif; font-size:12px'>
					Sorry! No matching question found.<br>
					 <a style='align:right;color:blue;font-family:arail,helvetica, sans-serif; font-size:12px;cursor:pointer' onclick='window.close()'>Close Window</a>
			  </p>";
		unset($_SESSION['is_sexam_begin']);
		exit;
	}
	?>

	<tr>
	
	</tr>
	<tr>
		<td>
			  <div  id="content" style="min-height:500px;">

 <form method="post"  name="examform" id="examform" action="">
<div style="clear: both;"></div>

<div id="main-area">

<div id="pWait" style="background: none repeat scroll 0% 0% grey; height: 100%; width: 100%; z-index: 1999; position: absolute; opacity: 0.4;">
			<div style="top:45%;position:relative;color:white">
			<center><img src="<?php echo ROOTURL; ?>/css/images/loading.gif" style="height:50px;width:50px;display:block;"><br><h2>Please wait</h2></center>
			</div>
		</div>

<div id="question-area" style="width:79.5%;float: left;background: #fdfdfd;border: 1px solid #ccc;min-height: 400px;">

		
		
		
		<fieldset id="fieldset_sec"><legend><b>Sections</b> <span style="display:none" id="showOptionalSecSummary"> [Attempt any <span id="maxOptSec"></span>&nbsp;of the <span id="noOptSec"></span>&nbsp;optional sections] </span> </legend>
		<div id="sections"><table width="100%"><tbody><tr><td>
		<?php if(!$isSampleTest) { ?>

		
	<?php
	if($_SESSION['subject_info_arr'] && ($isReview != 1))
	{
	
	 $current_display_question=$_SESSION['test_id-'.$currectTestId]['question_id-'.$current];
	
			$result = $DB->SelectRecord('question', "id = '$current_display_question'", 'subject_id');
			$current_q_sub = $result->subject_id;
			$curr_sub_name = $DB->SelectRecord('subject','id='.$current_q_sub);
 			$curr_sub_name = $curr_sub_name->subject_name;
			
		foreach($_SESSION['subject_info_arr'] as $subject_id => $subject) 
		{ 		
		?> 
		<div id="s1" class="allSections currentSectionSelected" ><a class="tooltip tooltipSelected" href="#"><div style="width:90%;overflow:hidden;white-space:nowrap ;padding-left:10px;cursor:pointer;<?php if($curr_sub_name == $subject['subject_name']) { ?>
		 color:#fbab00 <?php } ?>" 
		 >
		<?php
			if(!isset($subject['last_question_id']) || ($subject['last_question_id'] == ''))
			{
				if($current==$subject['start_question_id'])
				{ ?>
					<?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?>
					<input type="hidden" name="question_subject_id" id="question_subject_id" value="<?php echo $subject_id; ?>" />			 				
	
			<?php
				}
				else
				{ ?>
					<p onClick="newpage('<?php echo CreateURL('index.php',"mod=$mod&id=$currectTestId&subject_id=$subject_id&current=".$subject['start_question_id']); ?>');"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?></p><?php
				}
			}
			elseif($current>=$subject['start_question_id'] && $current<=$subject['last_question_id'])
			{ ?>
				<?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?>
				<input type="hidden" name="question_subject_id" id="question_subject_id" value="<?php echo $subject_id; ?>" /><?php 
			}
			else
			{ ?>
				<p onClick="newpage('<?php echo CreateURL('index.php',"mod=$mod&subject_id=$subject_id&current=".$subject['start_question_id']); ?>');"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?></p><?php 
			}
			
		
			$total_qus = ($subject['last_question_id']+1)-$subject['start_question_id'];
			
			$total_qus = array();
			
			for($i=$subject['start_question_id'];$i<=$subject['last_question_id'];$i++)
			{
				 $total_qus[] = $_SESSION['test_id-'.$currectTestId.'']['question_id-'.$i];
			}
			//echo $i;
			//print_r($total_qus);
			$total_qust = count($total_qus);
			//-----------------------------------
			
					
				$total_unatmp = $_SESSION['unattempted_questions'];
				$atmp = $_SESSION['attempted_questions'];
				$tot_rew = $_SESSION['review'];
				$total_review_count = count(array_intersect($tot_rew,$total_qus));
				$total_review = count(array_intersect($tot_rew,$total_qus));
				$total_revw = array_intersect($tot_rew,$total_qus);
				$total_attemp = array_intersect($atmp,$total_qus);
				$total_unattemp = array_intersect($total_unatmp,$total_qus);

				$tot_attempted_questions_count =  count(array_diff($total_attemp, $total_revw));
				$tot_unattempted_questions_count =  count(array_diff($total_unattemp, $total_revw));
			 $total_pages = count($_SESSION['test_id-'.$currectTestId]);
			
			 $not_viewd =  $total_qust - ($tot_unattempted_questions_count +  $tot_attempted_questions_count + $total_review);
			 if(  $total_pages == $not_viewd)
			 {
			 	 $not_view =  $total_pages - ($tot_diff_unatm +  $tot_diff_atm + $total_review)-1;
				 $tot_diff_unatm =  1 ; 
			 }
			 else
			 {
			 	$not_view =   $not_viewd;
				 $tot_diff_unatm =  $tot_diff_unatmp  ; 
			 }
			
			 $current_display_question=$_SESSION['test_id-'.$currectTestId]['question_id-'.$current];
			$result = $DB->SelectRecord('question', "id = '$current_display_question'", 'subject_id');
			$current_q_sub = $result->subject_id;
			?>
			</div><span class="classic"><center><table width="95%" cellspacing="0" class="question_area" style="font-size:14px;margin-top:10px"><tbody><tr><td colspan="2"><b><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?></b></td></tr><tr><td colspan="2"><hr></td></tr></tbody></table><table width="95%" cellspacing="5" class="question_area" style="margin-top:0%"><tbody><tr><td width="80%" style="text-align:left;padding-top:10px">Answered: </td><td valign="top"><span id="tooltip_answered"><?php echo $tot_attempted_questions_count ;?></span></td></tr><tr><td width="80%" style="text-align:left;padding-top:10px">Not Answered: </td><td valign="top"><span id="tooltip_not_answered"><?php echo  $tot_unattempted_questions_count?></span></td></tr><tr><td width="80%" style="text-align:left;padding-top:10px">Marked for Review: </td><td valign="top"><span id="tooltip_review"><?php echo $total_review_count; ?></span></td></tr><tr><td width="80%" style="text-align:left;padding-top:10px">Not Visited: </td><td valign="top"><span id="tooltip_not_visited"><?php echo  $not_view; ?></span></td></tr></tbody></table></center></span></a></div>	
			<?php	
			}
		}
	} 

?>
	
</td></tr></tbody></table></div>
		</fieldset>
		
<?php 
	if($current=='') $current=0;
	//$current_display_question=$_SESSION['test_id-'.$currectTestId]['question_id-'.$current];
			$current_display_question = $_GET[''];
	$toolTipMark = 'Click to review this question later.';
	//$toolTipUnMark = 'Click to unmark this question for review.';
	$resu = $DB->SelectRecord('question', "id = '$current_display_question'", 'group_question_id');
	$is_group_ques =  $resu->group_question_id;
?>
 <div id="show_qus" ><div style="height:440px;<?php if($is_group_ques!=0){ ?> overflow-y:scroll<?php } ?>" id="QuestionDiv">
  <table border="0"  style='margin-top:-30px' cellspacing="0" id="tbl_totalquestions">
  
  <?php
  
	$_SESSION['viewed_questions'][$current_display_question] = $current_display_question;
	
	$result = $DB->SelectRecord('question', "id = '$current_display_question'", 'question_type, subject_id');
	$current_q_sub = $result->subject_id;
	$current_display_question_type = $result->question_type;
	
	$is_multi = false;
	$question_answer=$DB->SelectRecords('question_answer','question_id='.$current_display_question);
	if(count($question_answer) > 1)
	{
		$is_multi = true;
	}
	
	$getans="select q.id as question_id, ans.id, ans.answer_title, q.question_title, q.is_group, q.group_question_id
			from question as q
			join answer as ans on ans.question_id=q.id
			where q.id=$current_display_question
			group by ans.id";

	if(($current_display_question_type == "S") || ($current_display_question_type == "MT"))
	{
		$getans="select *,id as question_id from question where id = $current_display_question";
	}

	$getAnsResult = $DB->RunSelectQuery($getans);
	//echo "<pre>";print_r($getAnsResult);
	
	$path = ADMINURL.'/uploadfiles/';
	$img="";
		
	if($current_display_question_type == "I")
	{
		$getimg = "select image_path 
					from question
        			join question_image on question_image.question_id = question.id
					where question.id = '".$current_display_question."'";
	
		$getImages = $DB->RunSelectQuery($getimg);
		$total_img = count($getImages);
		
		$img = "";
		for($count_img = 0; $count_img < $total_img; $count_img++)
		{
			$images= $getImages[$count_img];
			$final_path = $path.$images->image_path;
			if($images->image_path == '0' || $images->image_path == null)
			{
				$final_path = ADMINURL . "/images/question-mark.png";
			}
				
			$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
		}
	}
	elseif($current_display_question_type == "S")
	{
		// do nothing;
	}
	elseif($current_display_question_type == "MT")
	{
		$left_cols = $DB->SelectRecords('question_match_left',"question_id='$current_display_question'");
		$right_cols = $DB->SelectRecords('question_match_right',"question_id='$current_display_question'");
		$right_cols = shuffle_assoc($right_cols);
		//echo "<pre>";print_r($right_cols);
	}
	
	$is_first=1;
	$total_answer = count($getAnsResult);
	$hint_info = $DB->SelectRecord('question','id='.$current_display_question,'*');
	$lang_counter_option=0;
for($count_ans=0; $count_ans<$total_answer;$count_ans++)
	{
		$option = $getAnsResult[$count_ans];
		if($is_first==1)
		{
			$is_first=0;
			$qno=$current+1;
			
			$title = trim(($option->question_title));
			
			if($_SESSION['lang'] == 'hi')
						$hindi = 'selected';
					if($_SESSION['lang'] == 'eng')
						$eng = 'selected';
					
					$lang_data = $title;
						
					$str_eng_pos = strrpos($lang_data,'[en:]');
					if($str_eng_pos != false )
					{
						$str_eng = substr($lang_data,7,$str_eng_pos-7);
					}
					else
					{
						$str_eng = trim($option->question_title);
					}
					$str_hindi_pos = strpos($lang_data,'[hi]');
					if($str_hindi_pos != false )
					{
						$str_hindi_end = strpos($lang_data,'[hi:]');
						$str_hindi = substr(trim($lang_data),$str_hindi_pos+4,$str_hindi_end);
						$str_hindi = str_replace('[hi:]',' ' ,$str_hindi);
					}
					else
					{
						$str_hindi = trim($option->question_title);
					}
					if(($str_eng_pos != false ) && ($str_hindi_pos != false))
					{
						if($_SESSION['lang'] == 'eng')
						{
							$title = trim($str_eng);
						}
						elseif($_SESSION['lang'] == 'hi')
						{
							$title = trim($str_hindi);
						}
						else
						{
							$title = trim($str_eng);
						}
					}elseif($str_eng_pos == false)
					{
						$title = trim($option->question_title);
					}	

			echo '</span><br></br></td></tr>';
			if($option->group_question_id != 0)
			{		
				$paragraph=$DB->SelectRecord("group_questions","id=".$option->group_question_id,"question_title,no_of_questions");
					if($_SESSION['lang'] == 'hi')
						$hindi = 'selected';
					if($_SESSION['lang'] == 'eng')
						$eng = 'selected';
					
					$lang_data = $paragraph->question_title;
						
					$str_eng_pos_para = strrpos($lang_data,'[en:]');
					if($str_eng_pos_para != false )
					{
					$str_eng_para = substr($lang_data,9,$str_eng_pos_para-9);
					}
					else
					{
					$str_eng_para = trim($lang_data);
					}
					$str_hindi_pos_para = strpos($lang_data,'[hi]');
					if($str_hindi_pos_para != false )
					{
						$str_hindi_end_para = strpos($lang_data,'[hi:]');
						$str_hindi_para = substr(trim($lang_data),$str_hindi_pos_para+3,-9);
					}
					else
					{
					$str_hindi_para = trim($paragraph->question_title);
					}
					if(($str_eng_pos_para != false ) && ($str_hindi_para != false))
					{
						if($_SESSION['lang'] == 'eng')
							$para = trim($str_eng_para);
						if($_SESSION['lang'] == 'hi')
							$para = trim($str_hindi_para);
					}elseif($str_eng_pos_para == false)
					{
						$para = trim($paragraph->question_title);
					}		
		
		
				$questionto = $paragraph->no_of_questions-1;
				$firstrecord = $DB->SelectRecords("question","group_question_id=".$option->group_question_id,"*", "order by id");
				$strt_q = $firstrecord[0]->id;
				$array = $_SESSION['test_id-'.$currectTestId];
				$key = array_search($strt_q, $array);
				$key = explode('-',$key);
				$key = $key[1]+1;
				
				$toq = $key + $questionto;
				
						if(!in_array($option->group_question_id, $_SESSION['show_direction_id']))
						{
							$_SESSION['direction'][$option->group_question_id] = '<b>Directions ('.$key.'-'.$toq.') :</b>';
							$_SESSION['show_direction_id'][] = $option->group_question_id;
						}
					//$paragraph->question_title = strip_tags($paragraph->question_title, '<b>');
						
					echo "<table width='100%'><tr><td valign='top' style='width:60%;padding-bottom:12px;padding-left:10px;padding-top:10px;text-align:justify;padding:7px;' id='question-title'>
					<div id='english_para' style='display:none;'>".$_SESSION['direction'][$option->group_question_id].$str_eng_para.$img."</div>
					<div id='hindi_para' style='display:none;'>".$_SESSION['direction'][$option->group_question_id].$str_hindi_para.$img."</div>
					<div id='title_para'>".$_SESSION['direction'][$option->group_question_id].$para.$img."</div></td>
					<td valign='top' style='width:40%;border-left:1px solid; padding-bottom:12px;padding-left:10px;padding-top:10px;' id='question-title'><b>Question No. $qno</b>
					<select style='float:right;' onchange='return language_para(this.value);'>
								<option value='e' $eng>English</option>
								<option value='h' $hindi>Hindi</option>
							</select><hr width='99%' align='left'>
					<table><tr><td valign='top'>
					<span id='english_para1' style='display:none;'>".$str_eng.$img."</span>
					<span id='hindi_para1' style='display:none;'>".$str_hindi.$img."</span>
					<span id='title_para1'>".$title.$img."</span></td></tr>";
					//echo "<tr><td style='padding-bottom:12px;padding-left:10px;padding-top:10px;' id='question-title'><b>Question No. $qno</b><hr width='99%' align='left'><span>".$title.$img."</span></td></tr>";
			}else{
					
					
						
				echo "<tr>
						<td style='padding-bottom:12px;padding-left:10px;padding-top:10px;' id='question-title'>
						<b>Question No. $qno</b>
						<select style='float:right;' onchange='return language_pref(this.value);'>
							<option value='e' $eng>English</option>
							<option value='h' $hindi>Hindi</option>
						</select>
						<hr width='99%' align='left'>
						<div id='english' style='display:none;'>".$str_eng.$img."</div>
						<div id='hindi' style='display:none;'>".$str_hindi.$img."</div>
						<div id='title'>".$title.$img."</div>
					</td>
					</tr>";
			}
			if($option->is_group == 3)
			{
				echo '<br><label>Read the question carefully; find out which <b>bold</b> part is <i>incorrect</i>.</label>';
			}
			
			if($hint_info->image != '')
			{ ?>
				
				<tr>
					<td><img style="max-width: 300px;max-height: 300px;" src="<?php echo $path.$hint_info->image; ?>" /></td>
				</tr> 
				<tr><td>&nbsp;</td></tr> <?php
			}
			
		}
		
		if ($current_display_question_type == "MT")
		{
			echo "<tr><td style='padding-left:25px'><table class='match_tab'>";
			
			foreach($left_cols as $lcol)
			{
				echo "<input type='hidden' name='left_ids[]' value=' $lcol->id '></input>";
				echo "<tr>
						<td style='padding:15px'>" . $lcol->value . "</td>
						<td style='padding:15px'>
						<select name='". $lcol->id ."' id='". $lcol->id ."' onchange='return blockduplicate(this);' class='match rounded'>
							<option value=''>Select Match</option>";
				
				foreach($right_cols as $rcol)
				{
					$selected = '';
					if($_SESSION['question-'.$current_display_question]['given_answer'][$lcol->id] == $rcol->id)
					{
						$selected = 'selected="selected"';
					}
					echo "<option value='". $rcol->id ."' $selected >". $rcol->value ."</option>";
				}
				
				echo "</select></td></tr>";
			}
			
			echo "</table></td></tr>";
		}
		
		elseif ($current_display_question_type == "S")
		{
			$answer = isset($_SESSION['question-'.$option->question_id]) ? $_SESSION['question-'.$option->question_id]['given_answer'] : '';
			echo "<tr><td style='padding-left:20px;'><b>Write Answer</b><br><textarea rows='12' cols='70' class='rounded' name='option'>$answer</textarea></td></tr>";
		}
		
		else
		{
			$answer = $option->answer_title;
			if($current_display_question_type == "I")
				$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";
			
			$checked = '';
			
			$name = 'option';
			$type = 'radio';
			if($is_multi)
			{
				$name = 'option[]';
				$type = 'checkbox';
			}	
			
			if(isset($_SESSION['question-'.$option->question_id]['given_answer']))
			{
				if(is_array($_SESSION['question-'.$option->question_id]['given_answer']) 
					&& in_array($option->id, $_SESSION['question-'.$option->question_id]['given_answer']))
				{
					$checked = 'checked';
				}
				elseif($_SESSION['question-'.$option->question_id]['given_answer']==$option->id)
				{
					$checked = 'checked';
				}
			}	
			$answer = strip_tags(trim($answer), '<img>');
			
			$str_eng_pos = strrpos($answer,'[en:]');
			if($str_eng_pos != false )
					{
						$str_eng = substr($answer,4,$str_eng_pos-4);
					}
					else
					{
						$str_eng = trim($answer);
					}
						$str_hindi_pos = strpos($answer,'[hi]');
					if($str_hindi_pos != false )
					{
						$str_hindi_end = strpos($answer,'[hi:]');
						$str_hindi = substr(trim($answer),$str_hindi_pos+4,$str_hindi_end-4);
						$str_hindi = str_replace('[hi:]',' ' ,$str_hindi);
					}
					else
					{
						$str_hindi = trim($answer);
					}
			
			if(($str_eng_pos != false) && ($str_hindi_pos != false))
			{
				if($_SESSION['lang'] == 'eng')
					$answer =strip_tags($str_eng,'<img>');
				else
					$answer = strip_tags($str_hindi,'<img>');
			}elseif($str_hindi_pos == false)
			{
				$answer = trim($answer);
				$answer = strip_tags($answer, '<img>');
			}	
				$lang_counter_option++;
			

			
			echo "<tr>
					<td valign='top' style='padding-left:25px'>
						<label>
							<input type='$type' name='$name' value=$option->id $checked>
							<span id='english.$lang_counter_option' style='display:none;'>$str_eng</span>
							<span id='hindi.$lang_counter_option' style='display:none;'>$str_hindi</span>
							<span id='title.$lang_counter_option'>$answer</span>
						</label>
					</td></tr>";
		}
		
		echo"<input type=hidden name=ques_id value=$current_display_question>";
		echo"<input type=hidden name=display_quest value=$current>";
	}echo "</table></td></tr></table>";
	?>
	</table>
	</div>
<?php

	$totalpages = $total_pages = count($_SESSION['test_id-'.$currectTestId]);
	$attempted_questions = array();
	$unattempted_questions = array();
	$review = array();
	$review_attemp = array();
	$total_questions = array();
	
	for($count1=0; $count1<$totalpages; $count1++)
	{
		$questionID = $_SESSION['test_id-'.$currectTestId]['question_id-'.$count1];
		$total_questions[] = $count1+1;
		
		if(in_array($questionID, $_SESSION['attempted_questions']))
			$attempted_questions[] = $count1+1;
		if(in_array($questionID, $_SESSION['unattempted_questions']))
			$unattempted_questions[] = $count1+1;	
		if(in_array($questionID, $_SESSION['review']))
		{
			$review[] = $count1+1;
			$review_pages[$questionID] = $count1+1;
		}
	}
	
	if(count($review) > 0)
	{
		$_SESSION['first_review'] = $review[0];
		$_SESSION['review_pages'] = $review_pages;
	}
	
	$reviewUrl = '';
	if($isReview == 1)
	{
		$total_questions = $review;
		$reviewUrl = '&review=1';
	}
	
	$_SESSION['total_questions'] = $total_questions;
	
	/* Setup vars for query. */
	$targetpage = CreateURL('index.php',"mod=$mod$reviewUrl"); 
	$total_pages = count($total_questions);	
	$adjacents = 5;																// How many adjacent pages should be shown on each side?
	if (!isset($page) || ($page == 0)) $page = $total_questions[0];				//if no page var is given, default to 1.
	
	$prev = '';
	if(array_search($page, $total_questions) > 0)
	$prev = $total_questions[array_search($page, $total_questions) - 1];		//previous page is page - 1
	
	$next = '';
	if(array_search($page, $total_questions) < count($total_questions)-1)
	$next = $total_questions[array_search($page, $total_questions) + 1];		//next page is page + 1
	$lpm1 = $total_questions[$total_pages-2];									//last page minus 1
	
	$pagination = "<div class=\"pagination\">";
	
	/*if(count($_SESSION['review']) > 0 && $isReview != 1)
	{ 
		$lastMarked = '';
		if(isset($_SESSION['last_marked_question']) && ($_SESSION['last_marked_question'] != '')) $lastMarked = "&page=".$_SESSION['last_marked_question'];
		$reviewUrl = CreateURL('index.php',"mod=$mod&review=1$lastMarked");
		$pagination.= "<span style=\"cursor:pointer;\" class=\"current\" onclick=\"newpage('$reviewUrl');\">Review Marked Questions</span><br><br>";
	}
	elseif ($isReview == 1)
	{ 
		if($_SESSION['last_unmarked_question'] != '') $lastUnmarked = "&page=".$_SESSION['last_unmarked_question'];
		$reviewUrl = CreateURL('index.php',"mod=$mod&$lastUnmarked"); 
		$pagination.= "<span style=\"cursor:pointer; \" class=\"current\" onclick=\"newpage('$reviewUrl');\">All Questions</span><br><br>"; 
	}*/

	if($total_pages > 1)
	{	
		//previous button
		if ($page != $total_questions[0])
		{
			$onPreClick = "newpage('$targetpage&page=$prev');";
		}

		for ($counter1 = 0; $counter1 < $total_pages; $counter1++)
		{
			$counter = $total_questions[$counter1];
			$questionID = $_SESSION['test_id-'.$currectTestId]['question_id-'.$counter1];
			
			$is_attempted = ''; if(in_array($counter, $attempted_questions)) $is_attempted = "attepmted";
			$is_unattempted = ''; if(in_array($counter, $unattempted_questions)) $is_unattempted = "unattepmted";
			$is_marked_review = ''; if(($isReview != 1) && in_array($questionID, $_SESSION['review'])) $is_marked_review = "review-question";
			
			
		}
		
		//next button
		if ($page < $total_questions[$counter1-1]) 
		{	$curr_id = $current+1;
			$onNextClick = "newpage('$targetpage&page=$next#$current_display_question');";
		}
	}
	$onNextClickfirst = "newpage('$targetpage&page=1#1');";
	$pagination.= "</div>";
	echo $pagination;
?>
<table style="margin-top:5px;">
	<tr>
		<!--<td width="10%" align="right">
			<?php if ($current+1 != $total_questions[0]) { ?> 
			<input type='button' name='previous' value='Previous' onclick="<?php echo $onPreClick; ?>" style='cursor:pointer' class='buttons'>
			<?php } else echo '&nbsp;'; ?>
	
		</td>--><td width="95%" align="right">&nbsp;</td>
		<td width="10%" align="right">
			<?php if ($current+1 != $total_questions[count($total_questions)-1]) { ?> 
			
			<span  id="Nextdiv_question" style="float:right;"><input type="submit" id="savenext"   name='next' value='Save & Next' onClick="<?php echo $onNextClick; ?>" style='cursor:pointer' class="btnEnabled"></span>
			<?php } else {?> 
			<span  id="Nextdiv_question" style="float:right;"><input type="submit" id="savenext"   name='gotofirst' value='Save & Next' onClick="<?php echo $onNextClickfirst; ?>" style='cursor:pointer' class="btnEnabled"></span>
			
			<?php } ?>
		</td>
	
		<?php if($isSampleTest && $_SESSION['level_choosed'] == 'B') { ?>
		<td width="15%" align="right">
			<a style='font-family:arail, helvetica, sans-serif;cursor:pointer;font-size:12px;background-color:yellow'  id='opener'>Hints</a>
		</td>
		<?php } ?>
	
		
		
		
	</tr>
	<tr>
	<?php 
	if($current=='') $current=0;
//	$current_display_question=$_SESSION['test_id-'.$currectTestId]['question_id-'.$current];
		
	$toolTipMark = 'Click to review this question later.';
	//$toolTipUnMark = 'Click to unmark this question for review.';
?>

<div style="width:99%;margin-left:1%;margin-top:5px;bottom:0px;border-top: solid 1px #ccc;" id="actionButton_QuestionDiv">
	
	<?php //if (in_array($current_display_question, $_SESSION['review'])) { ?>
	
	<!--<span style="float:left"><input type="submit" style='cursor:pointer' name='unmark_review'  onclick="<?php echo $onNextClick; ?>" value='Unmark From Review Later'  class="button actionBn" id="underreview" title='<?php echo $toolTipUnMark; ?>' ></span>-->
	
	
	<?php //} else {	 ?>
	<?php if ($current+1 != $total_questions[count($total_questions)-1]) { ?> 
		
			<span style="float:left; margin-top:8px"> 
			<input  type='submit' title='<?php echo $toolTipMark; ?>'  name='mark_review' onclick="<?php echo $onNextClick; ?>"  style='cursor:pointer' value="Mark for Review &amp; Next" class="button actionBn" id="underreview" onclick="fnSubmit('MARK')"></span>

			<?php } else { ?>
			<span style="float:left; margin-top:8px"> 
			<input  type='submit' title='<?php echo $toolTipMark; ?>'  name='mark_reviewfirst' onclick="<?php echo $onNextClick; ?>"  style='cursor:pointer' value="Mark for Review &amp; Next" class="button actionBn" id="underreview" onclick="fnSubmit('MARK')"></span>
			<?php } ?>
	
	
	<?php //} ?>


<span style="margin-top:8px">	<!--<input type=hidden name='is_finish' id='is_finish' value='0'>-->
<input type="hidden" name="ques_type" id="ques_type" value="<?php echo $current_display_question_type; ?>" /></span>
	
					
				
					<span style="float:left;margin-top:8px"><input type="button" value="Clear Response" class="button actionBn" onClick="resetOption($('form[name=examform]'));" id="clearResponse"></span>
					
</div>				</div>	
	</tr>
</table></div>
<div style="height: 500px; display:none" id="profileDiv">
<?php
			$candi_id = $_SESSION['candidate_id'];	
	$candi_details = $DB->SelectRecords('candidate', "id='$candi_id'");	
	//print_r($candi_details);
	

?>
				<center><font size="4em"><b>Profile Details</b></font></center>
				<br>
				<center>
					<table style="border-bottom: #000 1px solid; border-left: #000 1px solid; font-size: 14px; border-top: #000 1px solid; border-right: #000 1px solid;">
						<tbody><tr><td><b><span id="loginName">User Name</span></b></td><td><b> : </b><?php echo $candi_details[0]->candidate_id;?></td></tr>
						<tr><td><b>Name</b></td><td><b> : </b><?php echo ucwords(strtolower(stripslashes($candi_details[0]->first_name.' '.$candi_details[0]->last_name)))   ?></td></tr>
						<tr><td><b>Date of Birth (DD-MM-YYYY)</b></td><td><b> : </b><?php echo date("d/m/Y", strtotime(''.$candi_details[0]->birth_date.''));;?></td></tr>
						<tr><td id="emailIdText"><b>Email ID</b></td><td id="emailId"><b> : </b><?php echo $candi_details[0]->email;?></td></tr>
						<tr><td id="contactNoText"><b>Contact No</b></td><td id="contactNo"><b> : </b><?php if($candi_details[0]->contact_no!=''){echo $candi_details[0]->contact_no;} else{ echo 'No Contact Avilable';}?></td></tr>
					</tbody></table>
				</center>
				<div style="width:100%">
					<center><input type="button" class="button back" value="Back" onclick="changeQues('profileDiv');removeActiveLinks()"></center>
				</div>
			</div>
			
			<!------------------------------------------	-->
			
			<div id="instructionsDiv" style="height: 500px; display:none;">
			<div style="height:90%;overflow:auto;border: 2px #ccc solid">
				<center><span><b><font size="4em" color="#2F72B7">System Instructions</font></b></span></center>
				<div style="padding:2px" id="firstPage">
					<?php 
				$time = $DB->SelectRecord('test','id='.$_SESSION['testid']);
				 $total_time = $time->time_duration;
				?>
					<br>
					<br>
				<div style="" id="sysInstText1"><p align="center"><strong><span><b>Please read the following instructions carefully</b></span></strong></p>
<p><strong><u>General Instructions:</u></strong> <br></p>
<ol>
<li>Total of <?php echo  $total_time ;?> minutes duration will be given to attempt all the questions</li>
<li>The clock has been set at the server and the countdown timer at the top right corner of your screen will display the time remaining for you to complete the exam. When the clock runs out the exam ends by default - you are not required to end or submit your exam. </li>
<li>The question palette at the right of screen shows one of the following statuses of each of the questions numbered: 
<table>
<tbody>
<tr>
<td valign="top"><span id="tooltip_not_visited" style="PADDING-TOP: 5px">1</span></td>
<td>You have not visited the question yet.</td></tr></tbody></table>
<table>
<tbody>
<tr>
<td valign="top"><span id="tooltip_not_answered" style="PADDING-TOP: 5px">3</span></td>
<td>You have not answered the question.</td></tr></tbody></table>
<table>
<tbody>
<tr>
<td valign="top"><span id="tooltip_answered" style="PADDING-TOP: 5px">5</span></td>
<td>You have answered the question. </td></tr></tbody></table>
<table>
<tbody>
<tr>
<td valign="top"><span id="tooltip_review" style="PADDING-TOP: 5px">7</span></td>
<td>You have NOT answered the question but have marked the question for review.</td></tr></tbody></table>
<table>
<tbody>
<tr>
<td valign="top"><span id="tooltip_reviewanswered" style="PADDING-TOP: 12px">&nbsp;&nbsp;&nbsp; 9</span></td>
<td>You have answered the question but marked it for review. </td></tr></tbody></table></li>
<li style="LIST-STYLE-TYPE: none">The Marked for Review status simply acts as a reminder that you have set to look at the question again. <font color="red"><i>If an answer is selected for a question that is Marked for Review, the answer will be considered in the final evaluation.</i></font></li></ol>
<p><br><b><u>Navigating to a question : </u></b></p>
<ol start="4">
<li>To select a question to answer, you can do one of the following: 
<ol type="a">
<li>Click on the question number on the question palette at the right of your screen to go to that numbered question directly. Note that using this option does NOT save your answer to the current question. </li>
<li>Click on Save and Next to save answer to current question and to go to the next question in sequence.</li>
<li>Click on Mark for Review and Next to save answer to current question, mark it for review, and to go to the next question in sequence.</li></ol></li>
<li>You can view the entire paper by clicking on the <b>Question Paper</b> button.</li></ol>
<p><br><b><u>Answering questions : </u></b></p>
<ol start="6">
<li>For multiple choice type question : 
<ol type="a">
<li>To select your answer, click on one of the option buttons</li>
<li>To change your answer, click the another desired option button</li>
<li>To save your answer, you MUST click on <b>Save &amp; Next</b> </li>
<li>To deselect a chosen answer, click on the chosen option again or click on the <b>Clear Response</b> button.</li>
<li>To mark a question for review click on <b>Mark for Review &amp; Next</b>. <font color="red"><i>If an answer is selected for a question that is Marked for Review, the answer will be considered in the final evaluation. </i></font></li></ol></li>
<li>To change an answer to a question, first select the question and then click on the new answer option followed by a click on the <b>Save &amp; Next</b> button.</li>
<li>Questions that are saved or marked for review after answering will ONLY be considered for evaluation.</li></ol>
<p><br><b><u>Navigating through sections : </u></b></p>
<ol start="9">
<li>Sections in this question paper are displayed on the top bar of the screen. Questions in a section can be viewed by clicking on the section name. The section you are currently viewing is highlighted.</li>
<li>After clicking the <b>Save &amp; Next</b> button on the last question for a section, you will automatically be taken to the first question of the next section. </li>
<li>You can move the mouse cursor over the section names to view the status of the questions for that section. </li>
<li>You can shuffle between sections and questions anytime during the examination as per your convenience. </li></ol></div><div style="display:none;" id="sysInstText2"><p>&nbsp;</p>
<div id="instHindi">
<div>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="4">&nbsp; ????? ?????????? ????????? ?? ????? ?? ????? </font><br><b><u>??????? ??????</u></b> <br></h5></div>
<ol style="TEXT-ALIGN: left; LIST-STYLE-TYPE: decimal; PADDING-LEFT: 4%; PADDING-TOP: 3px">
<li>??? ???????? ?? ?? ???? ?? ??? ???? <b>30 </b>???? ?? ??? ???? ?????? </li>
<li>????? ?? ???? ???? ?? ?? ??? ???? ??????? ?? ?????? ???? ??? ????? ?? ????????? ????? ??? ???? ??? ??????? ?????? ???? ?? ??? ??? ??? ????????? ????? ??????? ??? ?????? ???? ??, ???? ???? ??????? ??? ?? ??? ???? ?? ????? ???? ?? ? ?? ????? ??? ?? ??? ?? ?????? </li>
<li>??????? ?? ?????? ???? ?? ?????? ?????, ???? ??? ???????? ?????? ?? ??? ????? ??? ?? ??? ?? ?????? ????? ???? ??: 
<table class="instruction_area" style="FONT-SIZE: 100%">
<tbody>
<tr>
<td><span title="Not Visited" class="not_visited">1</span></td>
<td>?? ??? ?? ?????? ?? ???? ?? ???? </td></tr>
<tr>
<td><span title="Not Answered" class="not_answered">3</span></td>
<td>???? ?????? ?? ????? ???? ???? ??? </td></tr>
<tr>
<td><span title="Answered" class="answered">5</span></td>
<td>?? ?????? ?? ????? ?? ???? ???? </td></tr>
<tr>
<td><span title="Not Answered &amp; Mark for Review" class="review">7</span></td>
<td>???? ?????? ?? ????? ???? ???? ?? ?? ?????? ?? ?????????? ?? ??? ??????? ???? ??? </td></tr>
<tr>
<td><span title="Answered &amp; Mark for Review" class="review_answered">9</span></td>
<td>?? ?????? ?? ????? ?? ????? ??? ?? ?????? ?? ?????????? ?? ??? ??????? ???? ??? </td></tr></tbody></table></li>
<li style="LIST-STYLE-TYPE: none">?????????? ?? ??? ??????? ?????? ???????? ????????? ?? ??? ??? ????? ???? ?? ???? ???? ?????? ?? ?????? ????? ?? ??? ??? ???? ??? <font color="red"><i>?? ???? ?????? ?? ??? ????? ???? ?? ???? ?????????? ?? ??? ??????? ???? ??, ?? ????? ????????? ??? ?? ????? ?? ????? ???? ??????</i></font> </li></ol><br><b><u>???? ?????? ?? ???? : </u></b>
<ol start="4" style="TEXT-ALIGN: left; LIST-STYLE-TYPE: decimal; PADDING-LEFT: 4%; PADDING-TOP: 3px">
<li>????? ???? ???? ??? ?????? ????? ?? ???, ?? ????? ??? ?? ??? ?? ????? ?? ???? ???: 
<ol type="a" style="TEXT-ALIGN: left; PADDING-LEFT: 4%; PADDING-TOP: 3px">
<li>??????? ?? ????? ?? ?????? ??????? ??? ?????? ?? ???? ???? ?? ??? ?????? ?????? ?? ????? ????? ????? ??? ?? ?? ?????? ?? ?????? ???? ?? ?????? ?????? ?? ??? ???? ????? ???????? ???? ???? ??? </li>
<li>??????? ?????? ?? ????? ???????? ???? ?? ??? ?? ???? ??? ???? ?????? ?? ???? ?? ???<b> Save and Next</b> ?? ????? ????? </li>
<li>??????? ?????? ?? ????? ???????? ???? ?? ???, ?????????? ?? ??? ??????? ???? ?? ???? ??? ???? ?????? ?? ???? ?? ??? <b>Mark for Review and Next</b> ?? ????? ????? </li></ol></li>
<li>?? <b>Question Paper</b> ??? ?? ????? ???? ??????? ?????????? ?? ??? ???? ???? </li></ol><br><b><u>???????? ?? ????? ???? : </u></b>
<ol start="6" style="TEXT-ALIGN: left; LIST-STYLE-TYPE: decimal; PADDING-LEFT: 4%; PADDING-TOP: 3px">
<li>????????? ?????? ?????? ?? ??? 
<ol type="a" style="TEXT-ALIGN: left; PADDING-LEFT: 4%; PADDING-TOP: 3px">
<li>???? ????? ????? ?? ???, ?????? ????? ??? ?? ???? ?? ?? ????? ????? </li>
<li>???? ????? ????? ?? ???, ???? ?????? ?????? ??? ?? ????? ????? </li>
<li>???? ????? ???????? ???? ?? ???, ???? <b>Save &amp; Next</b> ?? ????? ???? ????? ??? </li>
<li>????? ????? ?? ?????? ???? ?? ???, ????? ?????? ?? ?????? ????? ???? ?? <b>Clear Response</b> ??? ?? ????? ????? </li>
<li>???? ?????? ?? ?????????? ?? ??? ??????? ???? ???? <b>Mark for Review &amp; Next</b> ?? ????? ????? <font color="red"><i>??? ???? ?????? ?? ??? ????? ???? ?? ???? ?????????? ?? ??? ??????? ???? ??, ?? ????? ????????? ??? ?? ????? ?? ????? ???? ?????? </i></font></li></ol></li>
<li>???? ?????? ?? ????? ????? ?? ???, ???? ?????? ?? ??? ????, ??? ?? ????? ?????? ?? ????? ???? ?? ??? <b>Save &amp; Next</b> ??? ?? ????? ????? </li>
<li>????? ???? ?? ??? ?? ?????? ???????? ??? ?? ?????????? ?? ??? ??????? ??, ????? ?? ?? ?? ????????? ?? ??? ????? ???? ?????? </li></ol><br><b><u>????? ?????? ?????? ?? ????: </u></b>
<ol start="9" style="TEXT-ALIGN: left; LIST-STYLE-TYPE: decimal; PADDING-LEFT: 4%; PADDING-TOP: 3px">
<li>?? ?????????? ??? ??????? ?? ????? ??? ?? ??? ????????? ???? ???? ???? ??? ??? ??????, ??? ??? ?? ????? ???? ???? ?? ???? ???? ?? ??????? ??? ??? ??? ?? ????? ?? ??? ???, ?? ????????? ????? </li>
<li>???? ??? ?? ??? ????? ?????? ?? <b>Save &amp; Next</b> ??? ?? ????? ???? ?? ???, ?? ???????? ??? ?? ???? ??? ?? ????? ?????? ?? ????? ?????? </li>
<li>?? ?? ??? ?? ??? ???????? ?? ?????? ?? ????? ???? ??? ??? ?? ??? ???? ????? ??? ?? ???? ??? </li>
<li>?? ??????? ?? ????? ???? ?? ??? ????? ?? ???????? ?? ??? ???? ?????? ?? ?????? ?????? ?? ???? ??? </li></ol></div></div></div>
				<br>
				<br>
				<center><span><b><font size="4em" color="#2F72B7">Other Important Instructions</font></b></span></center>
				<div style="padding:2px" id="secondPagep1">
					
					<br>
					<br>
				<div style="" id="cusInstText1"><ol>
<li>This is a Mock test. The Question paper displayed is for practice purposes only. Under no circumstances should this be presumed as a sample paper.</li></ol></div><div style="display:none;" id="cusInstText2"><ol><li>This is a Mock test. The Question paper displayed is for practice purposes only. Under no circumstances should this be presumed as a sample paper.</li></ol></div></div>
			</div>
			<div style="width:100%">
				<center><input type="button" onclick="changeQues('instructionsDiv');" value="Back" class="button back"></center>
			</div>
		</div>
		<!----------------------------end---------------------->
		<!----------------------summery---------------------------->
		
		<div id="sectionSummaryDiv" style="height: 500px; display: none;">
			<center>
			<h3><b>Exam Summary</b></h3>
			<table width="80%" cellspacing="0" align="center" style="margin-top:5%" class="bordertable">
				<tbody>
					<tr>
						<th>Section Name</th>
						<th>No. of Questions</th>
						<th>Answered</th>
						<th>Not Answered</th>
						<th>Marked for Review</th>
						<th>Not Visited</th>
					</tr>
					<!------------------loop---------------------->
					
					<?php
	if($_SESSION['subject_info_arr'] && ($isReview != 1))
	{
	
	 $current_display_question=$_SESSION['test_id-'.$currectTestId]['question_id-'.$current];
	
			$result = $DB->SelectRecord('question', "id = '$current_display_question'", 'subject_id');
			$current_q_sub = $result->subject_id;
			$curr_sub_name = $DB->SelectRecord('subject','id='.$current_q_sub);
 			$curr_sub_name = $curr_sub_name->subject_name;
			
		foreach($_SESSION['subject_info_arr'] as $subject_id => $subject) 
		{ 
		 	
		//	print_r($_SESSION['viewed_questions'])
		//echo $subject['start_question_id'];
	//	print_r($subject);
	//for($i=0;$i<count($subject);$i++)
	//{
		//echo $subject['subject_name'];
//		
//		echo '<br>';
//		echo $subject['last_question_id'];
//	}
	
		
			
		?> 
		<tr><td>
		
			
					<?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?>
								 				
	</td>
		
			<?php
			
		
			$total_qus = ($subject['last_question_id']+1)-$subject['start_question_id'];
			
			$total_qus = array();
			
			for($i=$subject['start_question_id'];$i<=$subject['last_question_id'];$i++)
			{
				 $total_qus[] = $_SESSION['test_id-'.$currectTestId.'']['question_id-'.$i];
			}
			//echo $i;
			//print_r($total_qus);
			$total_qust = count($total_qus);
			//-----------------------------------
			
					
				$total_unatmp = $_SESSION['unattempted_questions'];
				$atmp = $_SESSION['attempted_questions'];
				$tot_rew = $_SESSION['review'];
				$total_review_count = count(array_intersect($tot_rew,$total_qus));
				$total_review = count(array_intersect($tot_rew,$total_qus));
				$total_revw = array_intersect($tot_rew,$total_qus);
				$total_attemp = array_intersect($atmp,$total_qus);
				$total_unattemp = array_intersect($total_unatmp,$total_qus);
				/*print_r($total_attemp);
				echo '<>';
				print_r($total_review);*/
				$tot_attempted_questions_count =  count(array_diff($total_attemp, $total_revw));
				$tot_unattempted_questions_count =  count(array_diff($total_unattemp, $total_revw));
			//	$total_attemp_count = count(array_intersect($atmp,$total_qus));
			//	$tot_diff_atm =  count(array_diff($total_qus, $tot_rew));
				//array_intersect($array1, $array2);
		//	 $tot_diff_unatmp =  count(array_diff($total_qus, $tot_rew));
			 $total_pages = count($_SESSION['test_id-'.$currectTestId]);
			
			 $not_viewd =  $total_qust - ($tot_unattempted_questions_count +  $tot_attempted_questions_count + $total_review);
				//if(!in_array($current_display_question,$total_attemp) && !in_array($current_display_question,$total_unattemp))
			 if(  $total_pages == $not_viewd)
			 {
			 	 $not_view =  $total_pages - ($tot_diff_unatm +  $tot_diff_atm + $total_review)-1;
				 $tot_diff_unatm =  1 ; 
			 }
			 else
			 {
			 	$not_view =   $not_viewd;
				 $tot_diff_unatm =  $tot_diff_unatmp  ; 
			 }
			
			 $current_display_question=$_SESSION['test_id-'.$currectTestId]['question_id-'.$current];
			$result = $DB->SelectRecord('question', "id = '$current_display_question'", 'subject_id');
			$current_q_sub = $result->subject_id;
			
			
			?>
				<td width="25%"><?php echo $total_qust; ?></td>
				<td width="15%"><?php echo $tot_attempted_questions_count ;?></td>
				<td width="15%"><?php echo  $tot_unattempted_questions_count?></td>
				<td width="15%"><?php echo $total_review_count; ?></td>
				<td width="15%"><?php echo  $not_view; ?></td>
			<?php
			
			}
		}
	 

?>
		</tr>	<input type='hidden' name='hidden_finish' id="hidden_finish" value="<?php if($_POST['hidden_finish']) echo $_POST['hidden_finish']; else echo '0';?>"/>		
					
					<!----------------loop-------------------------->
					<!--<tr>
						<td width="25%">General Awareness</td>
						<td width="15%">50</td>
						<td width="15%">0</td>
						<td width="15%">1</td>
						<td width="15%">0</td>
						<td width="15%">49</td>
					</tr>-->
				</tbody>
			</table>
			</center>
			<center>
				<table align="center" style="margin-top:5%;" id="confirmation_buttons1">
					<tbody>
						<tr>
							<td colspan="2">Are you sure you want to submit the Exam ?</td>
							</tr>
							<tr>
							<td style="text-align:center">
							<input type="button" value="Yes" style="width:50px" class="button" onclick="gotoconf()">
							</td>
							<td style="text-align:center"><input type="button" id="summerybutton" value="No" style="width:50px" class="button" 
							 onclick="changeQuesbyno('sectionSummaryDiv');removeActiveLinks()">
							</td>
						 </tr>
					 </tbody>
				 </table>
				 <table align="center" style="margin-top:5%; display:none" id="confirmation_buttons2">
					<tbody>
						
							<tr>
							<td style="text-align:center">
							<input type="button" value="Next" style="width:50px" class="button" onclick="return finishTest();">
							</td>
						</tr>
					 </tbody>
				 </table>
			 </center>
		 </div>
		
		
		
		
		
		<!-----------------------summery--------------------------->
		<!----------------------conform submit---------------------------->
		
		<div id="conformsubmitDiv" style="min-height:462px; display:none;">
			
			<center>
				<table align="center" style="margin-top:5%" id="confirmation_buttons">
					<tbody>
						<tr>
							<td colspan="2">Are you sure you want to submit the Exam ?</td>
							</tr>
							<tr>
							<td style="text-align:center">
							<input type="button" value="Yes" style="width:50px" class="button" onclick="return finishTest();">
							</td>
							<td style="text-align:center"><input type="button" id="summerybutton" value="No" style="width:50px" class="button" 
							 onclick="changeQuesbyno('sectionSummaryDiv');removeActiveLinks()">
							</td>
						 </tr>
					 </tbody>
				 </table>
			 </center>
		 </div>
		<?php $avl_time = $DB->SelectRecord("test", "id='$currectTestId'");
								 $enb_time_min = $avl_time->time_duration;
								 $enb_time_min = $enb_time_min/4;
								 $enb_time_mili_sec = $enb_time_min*60;
								 
							?>
							
							<input type="hidden" name='total_time_remain'  id="total_time_remain" value="<?php echo $enb_time_mili_sec; ?>" /></center></td>
					
		
	
		
		
		
		<!-----------------------conform submit--------------------------->
	
	
		<div id="QPDiv" style="height: 500px; display:none;">
				<div style="height:90%;overflow:auto" id="viewQPDiv">
				<!---------------------STRT HERE------------------------>
				<?php
					$test_detail=$DB->SelectRecord('test','id='.$currectTestId);
		//$sub_name=$DB->SelectRecords('exam_subjects','exam_id='.$test_detail->exam_id);
		$qMedia = $test_detail->question_media;
		
		if($qMedia == 'subject')
		{
			$qSourceTab = 'test';
			$qMediaCondition = " test_id = $currectTestId ";
		}
		elseif($qMedia == 'paper')
		{
			$qSourceTab = 'paper';
			$test_paper = $DB->SelectRecord('test_paper','test_id='.$currectTestId);
			$paper_id = $test_paper->paper_id;
			$qMediaCondition = " paper_id = $paper_id ";
		}
		
		$test_question_detail=$DB->SelectRecords($qSourceTab.'_questions', $qMediaCondition);
			//print_r($test_question_detail);
		//	print_r($_SESSION['test_id-'.$currectTestId]);

			?>
				
<div id="">
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_outer">
<tr>
	<td>
	<div style="clear:both"></div>
		
		


		<?php 
			$group_array =array();
			$path = ROOTURL . "/panel/uploadfiles/";
			//$currectTestId=$test_detail->id;
			$total_subject = $_SESSION['subject_info_arr'];
			$tot_sub = count($_SESSION['subject_info_arr']);
			foreach($total_subject as $key => $total_sub)
			{
			
			?>
			<div>
			<h2 style="text-align:left"><font color="#2F72B7">Section : <?php echo $total_sub['subject_name']; ?></font></h2>
			<?php
			
			//$count=count($test_question_detail);
			//$q_no=0;
			$counter = $total_sub['start_question_id'];
			$count = $total_sub['last_question_id'];
			
			for($counter = $total_sub['start_question_id'];$counter<=$count;$counter++)
			{
				$q_no=$q_no+1;
				//$q_id=$test_question_detail[$counter]->question_id;
				//$sub_ids=$DB->SelectRecords('subject','subject_name='.$total_sub['subject_name']);
				$q_id=$_SESSION['test_id-'.$currectTestId]['question_id-'.$counter];
				$question_title=$DB->SelectRecord('question','id='.$q_id.' and subject_id='.$key);
				echo '<table  border="0" width="100%" align="left" cellpadding="0" cellspacing="0" style="padding:10px; border-bottom:1px solid #000;"> ';
				$img="";
				if($question_title->question_type == 'I')
				{
					$images = $DB->SelectRecords('question_image','question_id='.$q_id);
					$count_img = count($images);
					
					$img = "<br><br>";
					for($i = 0; $i < $count_img; $i++)
					{	
						$final_path = $path.$images[$i]->image_path;
						
						if($images[$i]->image_path == '0')
							$final_path = ROOTURL . "/images/question-mark.png";
						
						$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
					}
					$img .= "<br><br>";
				}
				
				
				$title = (($question_title->question_title));
				//$title = Markdown($title);
				//$title = str_replace('&amp;', '&', $title);
				
				
				if($question_title->group_question_id != 0)
			{
				if(!in_array($question_title->group_question_id, $group_array))
				{
				$group_array[]=$question_title->group_question_id;
				$paragraph=$DB->SelectRecord("group_questions","id=".$question_title->group_question_id,"question_title");
				echo "<tr><td valign='top' colspan='2' id='question-title'><span>".$paragraph->question_title.$img."</span></td></tr>
				
			<tr><td colspan='2'><p><strong> Q ".$q_no."</strong> )<br>" . $title .$img . "</p></td></tr>";
			}
			else
			{
				echo "<tr>             
					<td colspan='2'><p><strong> Q ".$q_no."</strong> )<br>" . $title .$img . "</p></td>
				</tr>";	
			}
				//echo "<tr><td style='padding-bottom:12px;padding-left:10px;padding-top:10px;' id='question-title'><b>Question No. $qno</b><hr width='99%' align='left'><span>".$title.$img."</span></td></tr>";
			}else{
			
				echo "<tr>             
					<td colspan='2'><p><strong> Q ".$q_no."</strong> )<br>" . $title .$img . "</p></td>
				</tr>";	
							
			}
				
				
				
				echo'</table><br/>';
			}
			}
		?>
		
		
		</div> <!--test_question_paper div closed-->
		
	</td></tr>
	</table>
	
</div><!--Outer wrapper closed-->
<div style="clear:both"></div>
</div>		
				
				
				<!-------------------------END HERE---------------------->
				<div style="width:100%">
					<center><input type="button" onclick="changeQues('QPDiv')" value="Back" class="button back"></center>
				</div>
			</div>
		
		<!-----------------------------test end--------------------------------------->

	
</div>
<div id="mainright" style="border-left: 1px solid rgb(0, 0, 0); width: 20%; height:450px; display:none">
			<div style="top:25%;position:relative">
				<center>a<img src="<?php echo ROOTURL;?>/css/images/NewCandidateImage.jpg" id="candidateImg" width="60%"> </center>
			</div>
		</div>
<div id="ques-nav" style="width:20%;float: right;background: #fdfdfd;height:450px;">
	

<div style="height:90px" id="timer">
				<div style="width:34%;height:75px;float:left;padding-top:7px;padding-bottom:7px;" id="candImg"><center>
				<!----------------------profile_image start--------------->
				
				<img width="70px" height="70px" id="candidateImg" src="<?php echo ROOTURL; ?>/css/images/NewCandidateImage.jpg">
				
				 <!----------------------profile_image end--------------->
				</center></div>
				<div style="width:66%;height:75px;float:left;">
				<div id="low-time-note"></div>
					<div id="timer_head" style="margin-top:20px;width:100%; color:#000000;font-size:12px">Time Left : <span id="timeInMins" style="color: red;"><script language="JavaScript" src="<?php echo ROOTURL; ?>/js/count2.js" ></script></span></div>
					<div style="width:100%; color:#000000;text-align:left;padding-left:7px"><i>Hello User</i></div>
					<div style="display:none;height:25px;width:100px;position:relative;" id="showCalc"><img onClick="loadCalculator()" style="height:25px;width:100px;cursor:pointer" src="images/Calculator-button.gif"></div>
				</div>
			</div>
		
<!--------------------------------------->
<?php
$curr_sub_name = $DB->SelectRecord('subject','id='.$current_q_sub);
$curr_sub_name = $curr_sub_name->subject_name;
?>

<div id="parnt-nav" style="background: #fdfdfd;margin-bottom: 10px;float: left;width:104%;">
<div style="height: 199px;" id="numberpanel" class="numberpanel">
				<div style="height: 444px;" class="numberpanel">
				<div id="viewSection">You are viewing <b><?php echo  ucwords(strtolower(stripslashes($curr_sub_name))) ?></b> section </div>
				<div style="height:5%;margin-left:5px" id="quesPallet">Question Palette :</div>
				<div style="height:285px; width:200px; margin:auto; overflow-Y:scroll" id="tbs" class="pagination">
<?php

	$totalpages = $total_pages = count($_SESSION['test_id-'.$currectTestId]);
	$attempted_questions = array();
	$unattempted_questions = array();
	$review = array();
	$review_attemp = array();
	$total_questions = array();
	
	for($count1=0; $count1<$totalpages; $count1++)
	{
		$questionID = $_SESSION['test_id-'.$currectTestId]['question_id-'.$count1];
		$total_questions[] = $count1+1;
		
		if(in_array($questionID, $_SESSION['attempted_questions']))
			$attempted_questions[] = $count1+1;
		if(in_array($questionID, $_SESSION['unattempted_questions']))
			$unattempted_questions[] = $count1+1;	
		if(in_array($questionID, $_SESSION['review']))
		{
			if(in_array($questionID, $_SESSION['attempted_questions']))
			{
				
				$review_attemp[] = $count1+1;
				$review_attemp_pages[$questionID] = $count1+1;
				///print_r($review_attemp);
			}
			else
			{	
				$review[] = $count1+1;
				$review_pages[$questionID] = $count1+1;
			}
		}
	}
	
	if(count($review) > 0)
	{
		$_SESSION['first_review'] = $review[0];
		$_SESSION['review_pages'] = $review_pages;
	}
	
	$reviewUrl = '';
	if($isReview == 1)
	{
		$total_questions = $review;
		$reviewUrl = '&review=1';
	}
	
	$_SESSION['total_questions'] = $total_questions;
	
	/* Setup vars for query. */
	$targetpage = CreateURL('index.php',"mod=$mod$reviewUrl"); 
	$total_pages = count($total_questions);	
	$adjacents = 5;																// How many adjacent pages should be shown on each side?
	if (!isset($page) || ($page == 0)) $page = $total_questions[0];				//if no page var is given, default to 1.
	
	$prev = '';
	if(array_search($page, $total_questions) > 0)
	$prev = $total_questions[array_search($page, $total_questions) - 1];		//previous page is page - 1
	
	$next = '';
	if(array_search($page, $total_questions) < count($total_questions)-1)
	$next = $total_questions[array_search($page, $total_questions) + 1];		//next page is page + 1
	$lpm1 = $total_questions[$total_pages-2];									//last page minus 1
	
	$pagination = "<div id='tbs' class=\"pagination\">";
	
	/*if(count($_SESSION['review']) > 0 && $isReview != 1)
	{ 
		$lastMarked = '';
		if(isset($_SESSION['last_marked_question']) && ($_SESSION['last_marked_question'] != '')) $lastMarked = "&page=".$_SESSION['last_marked_question'];
		$reviewUrl = CreateURL('index.php',"mod=$mod&review=1$lastMarked");
		$pagination.= "<span style=\"cursor:pointer;\" class=\"current\" onclick=\"newpage('$reviewUrl');\">Review Marked Questions</span><br><br>";
	}
	elseif ($isReview == 1)
	{ 
		if($_SESSION['last_unmarked_question'] != '') $lastUnmarked = "&page=".$_SESSION['last_unmarked_question'];
		$reviewUrl = CreateURL('index.php',"mod=$mod&$lastUnmarked"); 
		$pagination.= "<span style=\"cursor:pointer; \" class=\"current\" onclick=\"newpage('$reviewUrl');\">All Questions</span><br><br>"; 
	}*/

	if($total_pages > 1)
	{	
		//previous button
		//print_r($_SESSION);
		//print_r($_SESSION['attempted_questions']);
		//print_r($_SESSION['unattempted_questions']);
		//print_r($_SESSION['review']);
		//echo $count1;
		//echo $questionID;
		//echo $current_display_question;
		if(!in_array($current_display_question, $_SESSION['review']) && !in_array($current_display_question, $_SESSION['attempted_questions']) && !in_array($current_display_question, $_SESSION['unattempted_questions']))
		{
				 $val = 'ok';
		}
		else
		{
				$val =  'not';
		}
		if ($page != $total_questions[0])
		{
			$onPreClick = "newpages('$targetpage&page=$prev','$val');";
		}
		$curr_sub_qus = $_SESSION['subject_id-'.$current_q_sub];
		$total_qust= count($curr_sub_qus);
		foreach($_SESSION['subject_info_arr'] as $subject_id => $subject) 
		{ 	if($subject_id == $current_q_sub)
		{
			
			
			$counter1 =$subject['start_question_id'];
			$total_qust = $subject['last_question_id'];
		for ($counter1 =$subject['start_question_id']; $counter1 <= $total_qust; $counter1++)
		{
			$counter = $total_questions[$counter1];
			$questionID = $_SESSION['test_id-'.$currectTestId]['question_id-'.$counter1];
			
			$is_attempted = '';
			$is_unattempted = ''; 
			$is_review_attemp = '';
			$is_marked_review = '';
			$current ='' ;
			if(in_array($counter, $attempted_questions))
			$is_attempted = "attepmted";
			if(in_array($counter, $unattempted_questions)) 
			$is_unattempted = "unattepmted";
			if(in_array($counter, $review_attemp) && in_array($questionID, $_SESSION['attempted_questions'])) 
			$is_unattempted = "reviwe_attmp";
			if(($isReview != 1) && in_array($questionID, $_SESSION['review']) && in_array($questionID, $_SESSION['unattempted_questions'])) 
			$is_marked_review = "review-question";
			if($is_unattempted=='' && $is_attempted=='' && $isReview == 0 ) $current ="current";
			
			if ($counter == $page)
			{	
				
				$pagination.= "<a style=\"cursor:pointer; \" class=\"$is_attempted $is_unattempted $is_marked_review $current\" onclick=\"newpages('$targetpage&page=$counter#$questionID','$val');\" id=\"$questionID\" title=\"Question\">$counter</a>";
			}
			else
			{
				$pagination.= "<a style=\"cursor:pointer; \" class=\"$is_attempted $is_unattempted $is_marked_review\" onclick=\"newpages('$targetpage&page=$counter#$questionID','$val');\" id=\"$questionID\" title=\"Question\">$counter</a>";					
			}
		}
		}
		else
		{}
		}
		//next button
		if ($page < $total_questions[$counter1-1]) 
		{
			$onNextClick = "newpage('$targetpage&page=$next');";
		}
	}
	
	$pagination.= "</div>";
	echo $pagination;
?>
</div>
<input type=hidden name='is_finish' id='is_finish' value='0'>
<input type="hidden" name="ques_type" id="ques_type" value="<?php echo $current_display_question_type; ?>" />

</form>

	<table width="100%" class="diff_type_notation_area_inner">
						<tbody><tr>
							<td colspan="4"><b>Legend : </b></td>
						</tr>
						<tr>
							<td><span class="answered">&nbsp;</span></td> <td> Answered</td>
							<td><span class="not_answered">&nbsp;</span></td> <td>Not Answered</td>
						</tr>
						<tr>
							<td><span class="review">&nbsp;</span></td> <td> Marked</td>
							<td><span class="not_visited">&nbsp;</span></td> <td> Not Visited</td>
						</tr>
					</tbody></table>
					<!--<table width="97%" style="background:#E4EDF7">
						<tbody><tr>
							<td width="50%"><center> <input type="button" onClick="show_pro();activeLink(this.id)" title="View Profile" value="Profile" class="button1" id="viewProButton"> </center></td>
							<td width="50%"><center><input type="button" onClick="show_ins();activeLink(this.id)" title="View Instructions" value="Instructions" class="button1" id="viewInstructionsButton"> </center> </td>
						</tr>
						<tr>
							<td width="50%" id="viewQPTD"><center> <input type="button" onClick="show_Qpaper();activeLink(this.id)" title="View Entire Question Paper" value="Question Paper" class="button1" id="viewQPButton"> </center></td>
							<!--<td><center> <input type="button" class="button" style="width:110px" id="finalSub" onclick="submitConfirmation('submit')" value="Submit" title="Submit Exam" disabled/></center></td>-->
							<?php $test_time = $DB->SelectRecord("test", "id='$currectTestId'");
								//echo $test_time->time_duration-30;
							?>
							<!--<input type='button' name='finish' id="finish" value='Finish Test' style='cursor:pointer' class="buttons rounded" onclick="return finishTests();" />-->
	<!--	</td>
	</tr>
</table>
	
<input type=hidden name='is_finish' id='is_finish' value='0'>
							<td width="50%" id="submitTD"><center><input type="button" onclick="conf_submit();"  title="Submit Group" value="Submit"  name="is_finish" id="is_finish" class="button1"></center></td>
						</tr>
					</tbody></table>-->
					<table width="97%" style="background:#E4EDF7">
						<tbody><tr>
						<form name="bottom_form">
							<td width="50%"><center> <input type="button" onClick="show_pro();activeLink(this.id)" title="View Profile" value="Profile" class="button1" id="viewProButton"> </center></td>
							<td width="50%"><center><input type="button" onClick="show_ins();activeLink(this.id)" title="View Instructions" value="Instructions" class="button1" id="viewInstructionsButton"> </center> </td>
						</tr>
						<tr>
							<td width="50%" id="viewQPTD"><center> <input type="button" onClick="show_Qpaper();activeLink(this.id)" title="View Entire Question Paper" value="Question Paper" class="button1" id="viewQPButton"> </center></td>
							<!--<td><center> <input type="button" class="button" style="width:110px" id="finalSub" onclick="submitConfirmation('submit')" value="Submit" title="Submit Exam" disabled/></center></td>-->
							<td width="50%" id="submitTD"><center><!--<input type="button"  title="Submit Group" value="Submit" onClick="conf_submit();" id="finalSub" class="button1">-->
							<input type='button' name='finish' id="finish" value='Submit' <?php if($_POST['hidden_finish']!=1){?> disabled="disabled" <?php }?> style='cursor:pointer' class="button1" onclick="show_sectionSummaryDiv();activeLink(this.id)" /><!--return finishTest();-->
								</tr>
					</tbody></table>
<span style="margin-top:8px">	<input type=hidden name='is_finish' id='is_finish' value='0'>
<input type="hidden" name="ques_type" id="ques_type" value="<?php echo $current_display_question_type; ?>" /></span>
</div><!--parnt-nav closed-->

<!--<table>
<?php 
$targetpage = CreateURL('index.php',"mod=$mod");
for($counter=0; $counter<$totalpages;$counter++)
{ 
	$q_id = $_SESSION['test_id-'.$currectTestId]['question_id-'.$counter];
	
	$question_record = $DB->SelectRecord("question", "id='$q_id'");
			
	$qtitle = (($question_record->question_title));
	//$qtitle = Markdown($qtitle);
	//$qtitle = str_replace('&amp;', '&', $qtitle);
	$questionTitle = $qtitle = strip_tags($qtitle,'<img>'); 
	
	if($question_record->question_type == 'MT')
	{
		$left_cols = $DB->SelectRecords('question_match_left',"question_id='$q_id'");
		$right_cols = $DB->SelectRecords('question_match_right',"question_id='$q_id'");
		
		$match_type_count = ($left_cols > $right_cols) ? count($left_cols) : count($right_cols);
	}
	else
	{
		$answers = $DB->SelectRecords('answer', "question_id='$q_id'");	
	}
	
	?>
	
	<tr style="display: none;">
		<td id="tool-tip-<?php //echo $q_id; ?>" style="display: none;">
			<?php //echo $questionTitle; ?><br>
			
			<?php //if($question_record->question_type == 'MT') { ?>
			
			<table style="padding: 2px 5px;">
				<?php //for($question_counter=0; $question_counter < $match_type_count; $question_counter++) { ?>
				
				<tr>
					<td style="padding: 2px 5px;"><?php //echo $left_cols[$question_counter]->value; ?></td>
					<td style="padding: 2px 5px;"><?php //echo $right_cols[$question_counter]->value; ?></td>
				</tr>
				
				<?php //} ?>	
			</table>
						
			<?php// } elseif($answers) { ?>
			
			<ul>
				<?php// foreach($answers as $answer) { ?>
					<li><?php //echo $answer->answer_title; ?></li>
				<?php //} ?>
			</ul>
			
			<?php// } ?>
		</td>
	</tr> <?php
	
	if(!in_array($q_id,$_SESSION['viewed_questions']))
		continue;
?>

<tr>
	<td width="20px"><?php echo $counter+1 ?></td>
		
		<?php 
			
			if(strlen($qtitle) > 25) $qtitle = substr($qtitle, 0, 22).'...';
			
			$is_attempted = ''; 
			if(in_array($counter+1, $attempted_questions)) $is_attempted = ' attempted-que-link ';
			
			$is_current = '';
			if($counter == $current) $is_current = ' current-que-link'; 
			$href = $targetpage.'&page='.($counter+1);
		?>
		
	<td>
		<a class="ques-nav-link <?php echo $is_attempted.$is_current; ?>" href="<?php echo $href; ?>">
			<?php echo $qtitle; ?>
		</a>
	</td>
</tr>

<?php
}
?>
</table>-->

				
			
</div></div>
<?php if(!$isSampleTest) { ?>
<!--<table>
<tr>
	<?php
	if($_SESSION['subject_info_arr'] && ($isReview != 1))
	{
		foreach($_SESSION['subject_info_arr'] as $subject_id => $subject) 
		{
			if(!isset($subject['last_question_id']) || ($subject['last_question_id'] == ''))
			{
				if($current==$subject['start_question_id'])
				{ ?>
					<td class="currentTd"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?>
					<input type="hidden" name="question_subject_id" id="question_subject_id" value="<?php echo $subject_id; ?>" /></td> <?php
				}
				else
				{ ?>
					<td><a class="other" onClick="newpage('<?php echo CreateURL('index.php',"mod=$mod&id=$currectTestId&subject_id=$subject_id&current=".$subject['start_question_id']); ?>');"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?></a></td> <?php
				}
			}
			elseif($current>=$subject['start_question_id'] && $current<=$subject['last_question_id'])
			{ ?>
				<td class="currentTd"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?>
				<input type="hidden" name="question_subject_id" id="question_subject_id" value="<?php echo $subject_id; ?>" /></td> <?php 
			}
			else
			{ ?>
				<td><a class="other" onClick="newpage('<?php echo CreateURL('index.php',"mod=$mod&subject_id=$subject_id&current=".$subject['start_question_id']); ?>');"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?></a></td> <?php 
			}
		}
	}
	?>
</tr>
	
</table>-->
	
<?php  } ?>

	
<div style="clear: both;"></div>
</div>
	
</form>
</div>	
</td>
</tr>	
</table>
</div><!--Outer wrapper closed-->

<!--<div id="hints">	
<?php //echo ($hint_info->hints !='') ? stripslashes($hint_info->hints) : 'No hints provided.'; ?>
</div>-->

<?php 
//if($hint_info->video != '')
//{ 
	$video_href = $path . $hint_info->video; ?>
	
	<!--<div id="video_preview">
		<div id='vid'>
			<a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this video.
		</div>
		
		<script type="text/javascript">
			var s1 = new SWFObject("<?php echo ADMINURL; ?>/lib/mediaplayer.swf","ply","600","385","9","#FFFFFF");
			s1.addParam("allowfullscreen","true");
			s1.addParam("allowscriptaccess","always");
			s1.addVariable('file','');
			s1.addParam("flashvars","&file=<?php echo $video_href; ?>&stretching=exactfit&autostart=true");
			s1.write("vid");
		</script>		
	</div>-->
<?php
//}
?>
	<!----------------------conform submit---------------------------->
		<!--
		<div id="conformsubmitDiv" style="height: 500px; display:none;">
			
			<center>
				<table align="center" style="margin-top:5%" id="confirmation_buttons">
					<tbody>
						<tr>
							<td colspan="2">Are you sure you want to submit the Exam ?</td>
							</tr>
							<tr>
							<td style="text-align:center">
							<input type="button" value="Yes" style="width:50px" class="button" onclick="finalSubmit('group')">
							</td>
							<td style="text-align:center"><input type="button" id="summerybutton" value="No" style="width:50px" class="button" 
							 onclick="backtoqus();removeActiveLinks()">
							</td>
						 </tr>
					 </tbody>
				 </table>
			 </center>
		 </div>
		
		-->
		
		
		
		<!-----------------------conform submit--------------------------->
<div id="footer"><div style="width:100%;padding-top:15px;"><center><font color="white">Assess All</font></center></div></div>
<div id="clear"></div>
<div style="display:none">
</body>
</html>
