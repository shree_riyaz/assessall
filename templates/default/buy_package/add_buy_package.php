<?php
/**
 * @author : Akshay Yadav
 * @desc    : Get an exam for candidate
 */
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");
?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Online Examination - course</title>
    <link rel="stylesheet" href="<?php echo ROOTURL; ?>/css/style.css"/>
    <style>
        body, #content {
            background-color: #f1f1f1;
        }

        #main {
            margin: 0 auto;
        }

        .main_h1 {
            margin-left: 50px;
        }

        .main_h1 div {
            background-color: #066a75;
            color: #FFFFFF;
            width: 220px;
            padding-left: 15px;
        }

        .course_main {
            height: auto;
            min-height: 100px;
            width: 800px;
            margin-left: 50px;
            margin: 50px auto;
            text-align: center

        }

        #link {
            width: 200px;
            height: 18%;
            margin: 20px;
            margin-left: 25px;
            pointer: cursor;
            /*border:1px solid red;*/
            float: left;
        }

        #link div:hover {
            color: #066a75;
        }

        .comp-name {
            text-align: center;
            margin: 0 auto;
            background-color: #f1f1f1;
            color: #000000;
            font-weight: bold;
            height: 30px;
            /*width:150px;
            padding-left:15px;*/
            padding-top: 15px;
            box-shadow: 0 0 2px #666;
            cursor: pointer;
            text-decoration: none;
        }

        .course_main {
            margin: 30px 20px 40px 30px !important;
        }

    </style>
</head>
<body>
<?php include_once(TEMPPATH . "/includes/header.php"); ?>
<section id="main_sec" style='overflow:visible'>
    <?php include_once(TEMPPATH . "/includes/left_sec.php"); ?>
    <?php //include_once(TEMPPATH."/includes/right_sec_report.php"); ?>
</section>

<div id="right_sec">
    <div id="main">
        <div class='right_cat_sec' style="margin-top:50px">
            <div class='green_head_wrp'>
                <div class="green_header">Select Course</div>
            </div>
            <div class="course_main">

                <div id="contents">
                    <!--    <form id="addfrm" type="hidden" name="addfrm" method="post" action="http://localhost/assessall/paytm/pgRedirect.php">-->
                    <form id="addfrm" type="hidden" name="addfrm" method="post" action="">
                        <!--    <form id="addfrm" type="hidden" name="addfrm" method="post" action="http://localhost:1530/assessall/paytm/pgRedirect.php">-->
                        <fieldset class="rounded" style="width: 75%;">
                            <legend><?php /*echo $isEdit ? 'Edit' : 'Create'; */ ?> Package</legend>

                            <?php
                            if (isset($_SESSION['error'])) {
                                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="95%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
                                echo $_SESSION['error'];
                                echo '</td></tr></tbody></table>';
                                unset($_SESSION['error']);
                            }

                            if (isset($_SESSION['success'])) {
                                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
                                echo $_SESSION['success'];
                                echo '</td></tr></tbody></table>';
                                unset($_SESSION['success']);
                            }
                            ?>
                            <input id="ORDER_ID" tabindex="1" maxlength="20" size="20"
                                   type="hidden" name="ORDER_ID" autocomplete="off"
                                   value="<?php echo "ORDS" . rand(10000, 99999999) ?>">
                            <?php $i = 0; ?>

                            <input id="CUST_ID" tabindex="2" maxlength="12" size="12" type="hidden" name="CUST_ID" autocomplete="off"
                                   value="<?php echo "CUST".rand(100, 999) ?>">

                        <input id="INDUSTRY_TYPE_ID" tabindex="4" maxlength="12" size="12" type="hidden" name="INDUSTRY_TYPE_ID"
                                   autocomplete="off" value="Retail">
                            <input id="CHANNEL_ID" tabindex="4" maxlength="12"
                                   size="12" type="hidden" name="CHANNEL_ID" autocomplete="off" value="WEB">

                            <input id="CALLBACK_URL" tabindex="4" maxlength="12" size="12" type="hidden" name="CALLBACK_URL"
                                   autocomplete="off"
                                   value="http://localhost/assessall/index.php?mod=user&do=package_list">

                            <input title="TXN_AMOUNT" tabindex="10" type="hidden" name="TXN_AMOUNT" value="">

                            <table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">
                                <tr>
                                    <td width="110" align="right">Select Exam:<span class="red">*</span></td>
                                    <td colspan="2" align="left" width="200">
                                        </br>
                                        <select
                                            onchange="showPackageDetails(this.value);"
                                            style="width: 40%;" type="hidden" name="package_id" id="package_exam_id" class="rounded">
                                            <option value="0">Please Selecte Package </option>
                                            <?php
                                            if (is_array($package_list_for_student)) {
                                                for ($counter = 0; $counter < count($package_list_for_student); $counter++) {
                                                    ?>
                                                    <option
                                                        value="<?php echo $package_list_for_student[$counter]->id; ?>"><?php echo $package_list_for_student[$counter]->package_name; ?></option>
                                                    <?php
                                                }

                                            }
                                            ?>
                                        </select>
                                        </br>
                                        <span id="txtHint"><b>Package info will be listed here.</b>
                                        </span>
                                    </td>
                                </tr>


                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">
                                        <div align="left">
                                            <input style="width:23%!important; font-size: 0.9em; height: 29px;"
                                                   type="submit" type="hidden" name="addbuypackage" id="addpackag" value="Buy Package"
                                                   class="buttons rounded"/>
                                        </div>
                                    </td>
                                </tr>

                            </table>

                        </fieldset>
                        <br/>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<input id="defaultLang" value="12" type="hidden">
<script>
    function showPackageDetails(str) {
//        alert(str);
        if (str=="") {
            document.getElementById("txtHint").innerHTML="";
            return;
        }
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
                document.getElementById("txtHint").innerHTML=this.responseText;
            }
        }
        xmlhttp.open("GET","index.php?q="+str,true);
        xmlhttp.send();
    }
</script>
<script>
    $(document).ready(
        function () {
            xajax_getExamTypeIdByExamId('<?php echo $frmdata['exam_id']; ?>', false);

            $('.include-check').live('change', function () {
                $('.include-check').not(':checked').parent().parent().removeClass('included')
                $('.include-check').filter(':checked').parent().parent().addClass('included')
            });
            xajax_showFilteredCandidate('<?php echo $frmdata['exam_id']; ?>', true);
        });

    function resetQuestionHistory(val) {
        document.getElementById('total_mark').innerHTML = 0;
        document.getElementById('tbl_reports').style.display = "none";
        document.getElementById('tbl_body').innerHTML = '';
        document.getElementById('total_subject').value = 0;

        if (val != 1)
            document.getElementById('question_media').value = '';
    }
</script>
</body>
</html>