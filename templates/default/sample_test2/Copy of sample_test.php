<?php 
//print_r($_SESSION);
$date=$_SESSION['sdate'];
$current_date= date('m/d/Y h:i:s A');

$currectTestId=0;
$count=count($_SESSION['test_id-'.$currectTestId]);

if ($_SESSION['sample_total_marks'] > 0) //do not start exam if no question found in the query 
{
	$_SESSION['is_sexam_begin']=1; // to set a variable so if page submit there would be no question query run again and again
}	

if(isset($_POST['next']))
{
	$ques_id=$_POST['ques_id'];
	$last_disp_quest=$_POST['display_quest'];
	$_SESSION['question-'.$ques_id]['given_answer']=$_POST['option'];// candidates choosed answer
	$current=$last_disp_quest+1;
	$page = $current+1;
}
elseif(isset($_POST['previous']))
{
	$ques_id=$_POST['ques_id'];
	$last_disp_quest=$_POST['display_quest'];
	$_SESSION['question-'.$ques_id]['given_answer']=$_POST['option'];
	$current=$last_disp_quest-1;
	$page = $last_disp_quest;
}
elseif($_POST['is_finish']==1) // if click on finish test
{
		$ques_id=$_POST['ques_id'];
		$last_disp_quest=$_POST['display_quest'];
		$_SESSION['question-'.$ques_id]['given_answer']=$_POST['option'];
		Redirect(CreateURL('index.php',"mod=user&do=sample_result"));
		exit;
}
elseif($_GET['page'])
{
	$ques_id=$_POST['ques_id'];
	$last_disp_quest=$_POST['display_quest'];
	$_SESSION['question-'.$ques_id]['given_answer']=$_POST['option'];// candidates choosed answer
	$current=$_GET['page']-1;
	$page = $_GET['page'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Guidelines</title>

<link href="css/exam.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="js/functions.js"></script>
<script language="JavaScript">
TargetDate = "<?php echo $date; ?>";
BackColor = "";
ForeColor = "black";
CountActive = true;
CountStepper = -1;
LeadingZero = true;
DisplayFormat = "%%H%% : %%M%% : %%S%% ";
FinishMessage = "It is finally here!";
CurrentDate="<?php echo $current_date ?>";

function newpage(path)
{
	document.examform.action = path;
	document.examform.submit();
}
</script>


</head>
<body>
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php include_once(TEMPPATH.'/header.php');?>
		</td>
	  </tr>
	  <?php
	   		
	  		if (!isset($_SESSION['is_valid_test']) || $_SESSION['is_valid_test'] == 0)
			{
				echo "</table></div>";
				echo "<p align='center' style='font-family:arail,helvetica, sans-serif; font-size:12px'>
							Sorry! No matching question found.<br>
							 <a style='align:right;color:blue;font-family:arail,helvetica, sans-serif; font-size:12px;cursor:pointer' onclick='window.close()'>Close Window</a>
					  </p>";
				unset($_SESSION['is_sexam_begin']);
				exit;
			}
	   ?>
	  <tr>
		<td>
			<div id="timer">
				Time Remaining:&nbsp;<script language="JavaScript" src="<?php echo ROOTURL;?>/js/count2.js"></script>
			&nbsp;&nbsp;
			</div>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content" style="height:450px">
			<div>
			<form method="post" name="examform" id="examform" action="">
<?php 
	// How many adjacent pages should be shown on each side?
	$adjacents = 1;
	global $DB;
	/* Setup vars for query. */
	$targetpage = CreateURL('index.php',"modu=user&do=sample_test"); 	//your file name  (the name of this file)
	$limit =1; 								//how many items to show per page
	$mod = $_GET['mod'];
	
	
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
	/* Get data. */
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	//$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lastpage = ceil($count/$limit);
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$prev');\">� previous</a>";
		else
			$pagination.= "<span class=\"disabled\">� previous</span>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">$counter</span>";
				else
					$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$counter');\">$counter</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$counter');\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$lpm1');\">$lpm1</a>";
				$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$lastpage');\">$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=1');\">1</a>";
				//$pagination.= "<a href=\"$targetpage&page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$counter');\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$lpm1');\">$lpm1</a>";
				$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$lastpage');\">$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=1');\">1</a>";
				$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=2');\">2</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$counter');\">$counter</a>";					
				}
			}
		}
		//next button
		if ($page < $counter - 1)
		{
			$pagination.= "<a style=\"cursor:pointer; \" onclick=\"newpage('$targetpage&page=$next');\">next �</a>";
		} 
		else
			$pagination.= "<span class=\"disabled\">next �</span>";
		$pagination.= "</div>\n";		
	}
echo $pagination;	

?>
	<table border="0" width='100%' cellspacing="0" cellpadding="1" id="tbl_totalquestions">
  <tr>
	<td valign="top"><div style="color:#AF5403; font-size:14px; font-weight:bold; margin-top:3px;">&nbsp;</div></td>
  </tr>
  <tr>
  <td style="padding-bottom:12px">
  <?php
	if($current=='')
	{
		$current=0;
	}
	
	$current_display_question=$_SESSION['test_id-'.$currectTestId]['question_id-'.$current];
	$result = $DB->SelectRecord('question', "id = '$current_display_question'", 'question_type');
	$current_display_question_type = $result->question_type;
	
	$getans="select  answer.id as id,
        answer.answer_title,
        question.question_title,
		question.id as q_id		
       	from question
        join answer on answer.question_id = question.id
		where is_sample = 'T' and
       question.id = $current_display_question
       group by  answer.answer_title";
	
	$getAnsResult = $DB->ExecuteQuery($getans);
	
	$path = ROOTURL . '/admin/uploadfiles/';
	$img="";
	if($current_display_question_type == "I")
	{
		$getimg = "select image_path 
					from question
        			join question_image on question_image.question_id = question.id
					where is_sample = 'T' and
       				question.id = '".$current_display_question."'";
	
		$getImages = $DB->RunSelectQuery($getimg);
		$total_img = count($getImages);
		
		$img = "<br><br>";
		for($count_img = 0; $count_img < $total_img; $count_img++)
		{
			$images= $getImages[$count_img];
			$final_path = $path.$images->image_path;
			if($images->image_path == '0' || $images->image_path == null)
			{
				$final_path = ROOTURL . "/admin/images/question-mark.png";
			}
				
			$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
		}
	}
	
	$is_first=1;
	while($option= $DB->FetchObject($getAnsResult))
	{		
		if($is_first==1)
		{
			$is_first=0;
			$qno=$current+1;
			echo "<b>Q.$qno</b> ".$option->question_title.$img.'</td></tr>';
		}
		if(isset($_SESSION['question-'.$current_display_question]['given_answer']) && ($_SESSION['question-'.$current_display_question]['given_answer']==$option->id ))
		{
			$answer = $option->answer_title;
			if($current_display_question_type == "I")
				$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";
			echo "<tr><td style='padding-left:25px'><input type=radio name=option value=$option->id checked>".$answer."</td></tr>";
		}
		else
		{
			$answer = $option->answer_title;
			if($current_display_question_type == "I")
				$answer = "<img height='50' width='50' src=".$path.$option->answer_title.">";
			echo "<tr><td style='padding-left:25px'><input type=radio name=option value=$option->id>$answer</td></tr>";
		}
		echo"<input type=hidden name=ques_id value=$current_display_question>";
		echo"<input type=hidden name=display_quest value=$current>";
		
	}
	echo'</table>';
	echo'</div>';
	echo'<table align="left" style="margin-top:20px;">';
	//if the level is beginner, show the hints
	if ($_SESSION['level_choosed'] != '' && $_SESSION['level_choosed'] == 'B')
	{
		echo "
				<tr>
					<td colspan='2'><a style='font-family:arail, helvetica, sans-serif;cursor:pointer;font-size:12px;background-color:yellow'  id='opener'>Hints</a></td>
				</tr>
			 	<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>	
			";
	}
	echo '<tr>';
	//echo '<td width="810">&nbsp;</td>';
	echo '<td width="66" align="left">';
	if ($current > 0) 
	echo"<input type='submit' name='previous' value='Previous' style='cursor:pointer' class='buttons'>";
	else
	echo'&nbsp;';
	echo'</td><td width="66" align="right">';
	if ($current < ($count-1)) 
	echo"<input type='submit' name='next' value='Next' style='cursor:pointer' class='buttons'>";
	else
	echo'&nbsp;';
	echo'</td></tr></table><br/>';
	echo "<input type=hidden name=is_finish id=is_finish value=0>";
?>
<div style="text-align:right"><input type='button' name='finish' id="finish" value='Finish Test' style='cursor:pointer' class="buttons rounded" onclick="return finishTest();"></div>
  </td>
	</tr>
		</div>
		</div>
		<br />
		
		</div><!--Content div closed-->
	</td>
	  </tr> 
	</table>
	<input type="hidden" name="path" id="path" value=""/>
	</form>
	
</div><!--Outer wrapper closed-->
<?php 

$hint_info = $DB->SelectRecord('question','id='.$current_display_question,'hints');

?>
<div id="hints">	
	<?php 
		if ($hint_info->hints !='')
		{
			echo stripslashes($hint_info->hints);
		}
		else
		{
			echo 'No hints provided.';
		}	
		
	?>
</div>
</body>
</html>
<script>
$(document).ready(function() {
$("#hints").dialog({ autoOpen: false });
$('#opener').click(function() {
		//$dialog.dialog('open');
		$("#hints").dialog('open')
		// prevent the default action, e.g., following a link
		return false;
	});

	
});
</script>