<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<body>
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer" style="padding: 5px;">
	  <tr>
		<td>		
<div id="content" style="min-height:500px">

<form method="post" name="examform" id="examform" action="">
<div style="clear: both;"></div>
<div id="main-area">

<div id="question-area" style="width: 650px;float: left;background: #fdfdfd;border: 1px solid #ccc;padding: 10px;min-height: 400px;">
<div style="min-height: 360px;">
  <table border="0" cellspacing="0" cellpadding="1" id="tbl_totalquestions">
  <?php
	$_SESSION['viewed_questions'][$current_display_question] = $current_display_question;
	$result = $DB->SelectRecord('question', "id = '$current_display_question'", 'question_type');
	$current_display_question_type = $result->question_type;
	$is_multi = false;
	$question_answer=$DB->SelectRecords('question_answer','question_id='.$current_display_question);
	if(count($question_answer) > 1)
	{
		$is_multi = true;
	}
	
	$getans="select q.id as question_id, ans.id, ans.answer_title, q.question_title, q.is_group 
			from question as q
			join answer as ans on ans.question_id=q.id
			where q.id=$current_display_question
			group by ans.id";

	if(($current_display_question_type == "S") || ($current_display_question_type == "MT"))
	{
		$getans="select *,id as question_id from question where id = $current_display_question";
	}

	$getAnsResult = $DB->RunSelectQuery($getans);

	$path = ADMINURL . '/uploadfiles/';
	$img="";
	if($current_display_question_type == "I")
	{
		$getimg = "select image_path 
					from question
        			join question_image on question_image.question_id = question.id
					where question.id = '".$current_display_question."'";
	
		$getImages = $DB->RunSelectQuery($getimg);
		$total_img = count($getImages);
		
		$img = "";
		for($count_img = 0; $count_img < $total_img; $count_img++)
		{
			$images= $getImages[$count_img];
			$final_path = $path.$images->image_path;
			if($images->image_path == '0' || $images->image_path == null)
			{
				$final_path = ADMINURL . "/images/question-mark.png";
			}
				
			$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
		}
	}
	elseif($current_display_question_type == "S")
	{
		// do nothing;
	}
	elseif($current_display_question_type == "MT")
	{
		$left_cols = $DB->SelectRecords('question_match_left',"question_id='$current_display_question'");
		$right_cols = $DB->SelectRecords('question_match_right',"question_id='$current_display_question'");
		$right_cols = shuffle_assoc($right_cols);
	}

	$is_first=1;
	$total_answer = count($getAnsResult);
	$hint_info = $DB->SelectRecord('question','id='.$current_display_question,'*');
	
	for($count_ans=0; $count_ans<$total_answer;$count_ans++)
	{
		$option= $getAnsResult[$count_ans];
		if($is_first==1)
		{
			$is_first=0;
			$qno=$current+1;
			
			echo '<tr><td style="font-size:11px;"><b>Hint : </b>';
			switch($current_display_question_type)
			{
				case 'MT':
					echo 'Match The Following';
					break;
				case 'S':
					echo 'Subjective Type Question (Write Answer in Textarea)';
					break;
				default:
					echo 'Multiple Choice Question ';
					if($is_multi) echo '(Select <b>More Than One</b> Answers)';
					else echo '(Select <b>One</b> Answer)';
					
			}
			
			$title = (($option->question_title));
			
			echo '<br></br></td></tr>';
			echo "<tr><td style='padding-bottom:12px'><b>Question $qno</b> <br>".$title.$img.'</td></tr>';
			
			if($option->is_group == 3)
			{
				echo '<br><label>Read the question carefully; find out which <b>bold</b> part is <i>incorrect</i>.</label>';
			}
			
			if($hint_info->image != '')
			{ ?>
				
				<tr>
					<td><img style="max-width: 300px;max-height: 300px;" src="<?php echo $path.$hint_info->image; ?>" /></td>
				</tr> 
				<tr><td>&nbsp;</td></tr> <?php
			}
			
		}
		
		if ($current_display_question_type == "MT")
		{
			echo "<tr><td style='padding-left:25px'><table class='match_tab'>";
			
			foreach($left_cols as $lcol)
			{
				echo "<input type='hidden' name='left_ids[]' value=' $lcol->id '></input>";
				echo "<tr>
						<td style='padding:15px'>" . $lcol->value . "</td>
						<td style='padding:15px'>
						<select name='". $lcol->id ."' id='". $lcol->id ."' onchange='return blockduplicate(this);' class='match rounded'>
							<option value=''>Select Match</option>";
				
				foreach($right_cols as $rcol)
				{
					$selected = '';
					if($_SESSION['question-'.$current_display_question]['given_answer'][$lcol->id] == $rcol->id)
					{
						$selected = 'selected="selected"';
					}
					echo "<option value='". $rcol->id ."' $selected >". $rcol->value ."</option>";
				}
				
				echo "</select></td></tr>";
			}
			
			echo "</table></td></tr>";
		}
		
		elseif ($current_display_question_type == "S")
		{
			echo "<tr><td style='padding-left:20px;'><b>Write Answer</b><br><textarea rows='12' cols='70' class='rounded' name='option'>".$_SESSION['question-'.$option->question_id]['given_answer']."</textarea></td></tr>";
		}
		
		else
		{
			$answer = $option->answer_title;
			if($current_display_question_type == "I")
				$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";
			
			$checked = '';
			
			$name = 'option';
			$type = 'radio';
			if($is_multi)
			{
				$name = 'option[]';
				$type = 'checkbox';
			}	
			
			if(isset($_SESSION['question-'.$option->question_id]['given_answer']))
			{
				if(is_array($_SESSION['question-'.$option->question_id]['given_answer']) 
					&& in_array($option->id, $_SESSION['question-'.$option->question_id]['given_answer']))
				{
					$checked = 'checked';
				}
				elseif($_SESSION['question-'.$option->question_id]['given_answer']==$option->id)
				{
					$checked = 'checked';
				}
			}	
			
			echo "<tr>
					<td valign='top' style='padding-left:25px'>
						<label>
							<input type='$type' name='$name' value=$option->id $checked>
							$answer
						</label>
					</td>
				</tr>";
		}	
		echo"<input type=hidden name=ques_id value=$current_display_question>";
		echo"<input type=hidden name=display_quest value=$current>";
	}
	?>
	</table>
	</div>

<table style="margin-top:5px;">
	<tr>
		<td width="10%" align="right">
			<?php if ($current+1 != $total_questions[0]) { ?> 
			<input type='button' name='previous' value='Previous' onclick="<?php echo $onPreClick; ?>" style='cursor:pointer' class='buttons'>
			<?php } else echo '&nbsp;'; ?>
	
		</td>
		<td width="10%" align="right">
			<?php if ($current+1 != $total_questions[count($total_questions)-1]) { ?> 
			<input type='button' name='next' value='Next' onclick="<?php echo $onNextClick; ?>" style='cursor:pointer' class='buttons'>
			<?php } else echo'&nbsp;'; ?>
		</td>	
		<?php if($hint_info->video != '') { ?>
		<td width="15%" align="right">
			<a style='font-family:arail, helvetica, sans-serif;cursor:pointer;font-size:12px;background-color:yellow' id='show_video'>Show Video</a>
		</td>
		<?php } ?>
		<td width="65%" align="right">
			<input type='button' name='finish' id="finish" value='Finish Test' style='cursor:pointer' class="buttons rounded" onclick="return finishTest();" />
		</td>
	</tr>
</table>
	
<input type="hidden" name='is_finish' id='is_finish' value='0'>
<input type="hidden" name="ques_type" id="ques_type" value="<?php echo $current_display_question_type; ?>" />		
</div>
</div>	
</form>
</div>	
</td>
</tr>	
</table>
</div><!--Outer wrapper closed-->

</body>
</html>