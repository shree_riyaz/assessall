<?php
 /*****************Developed by :- Akshay Yadav
	            Date         :- 10-sep-2011
				Module       :- Reports
				Purpose      :- Template for Browse all candidate details and provide functionality to generate report
	***********************************************************************************/
//echo ROOT."/uploadfiles/format_question_list.xlsx";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Reports</title>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.name.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<legend>Student Details</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			<div class="search-div">
			<fieldset class="rounded search-fieldset"><legend>Search</legend>
			<table border="0" cellspacing="1" cellpadding="3">
			   <tr>
			    <td>Name :</td>
			    <td><input type="text" size="20" class="rounded textfield" name="name" id="name" value="<?php echo $frmdata['name'] ?>" onchange="managePageCheckIsChar(this.value,this.id, 'name');" /></td>
			   </tr>
			   <tr>
			    <td>Stream :</td>
			    <td>
				<select class="rounded" name="stream_id" id="stream_id">
					<option value="">Select Stream</option>
					<?php 
					if(is_array($stream))
					{
						for($counter=0;$counter<count($stream);$counter++)
						{
							$selected='';
							if ($stream[$counter]->id == $frmdata['stream_id'])
							{
								$selected='selected';
							}
							echo '<option value="'.$stream[$counter]->id.'"'.$selected.'>'. stripslashes($stream[$counter]->stream_title).'</option>';
						}
					}
						?>
					</select> 
				</td>
				</tr>
				<tr>
			    <td colspan="6" align="center">
			      <input type="submit" class="buttons rounded" name="Submit" value="Show" />
			      <input type="button" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick="window.location='<?php echo CreateURL('index.php','mod=report&do=candidate_detail'); ?>'"/>
			    </td>
			  </tr>
			  
			</table>
			</fieldset>
			</div>
		<?php
			if($candidate)
			{ 
		?>
			<table border="0" cellspacing="1" cellpadding="4" width="100%" style="">
			<tr>
				<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($candidate))." of ".$totalCount; ?></td>
				<td align="right"><?php echo getPageRecords();?> </td>
			</tr>
			</table>
		<?php
			} 
		?>
				<div id="celebs" style="overflow: auto; overflow-y: hidden;">
				<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
				<?php 
				if($candidate)
					{
				?>	  <thead>
					  <tr class="tblheading">
						<td width="1%" >#</td>
						<td width="15%" >Name</td>
						<td width="30%" >Course</td>
						<td width="11%" >Email</td>
						<td width="11%" >Date of Birth</td>
						<td width="11%" >Marital status</td>
					  </tr>
					  </thead>
					  <tbody>
					  
				  <?php 
				  
					$srNo=$frmdata['from'];
					$count=count($candidate);
					
						for($counter=0;$counter<$count;$counter++) 
						{ 
							$srNo=$srNo+1;
							if(($counter%2)==0)
							{
								$trClass="tdbggrey";
							}
							else
							{
								$trClass="tdbgwhite";
							}
						?>
							<tr class='<?= $trClass; ?>'>
								<td><?php echo $srNo; ?></td>
								<td><?php echo highlightSubString($frmdata['name'], ucfirst(strtolower(stripslashes($candidate[$counter]->first_name))) . ' ' . ucfirst(strtolower(stripslashes($candidate[$counter]->last_name)))); ?></td>
								<td><?php echo $candidate[$counter]->exam_name; ?></td>
								<td><?php echo $candidate[$counter]->email; ?></td>
								<td><?php if($candidate[$counter]->birth_date && ($candidate[$counter]->birth_date!='0000-00-00 00:00:00')) echo date('d-M-Y', strtotime($candidate[$counter]->birth_date)); else echo '-'; ?></td>
								<td><?php if($candidate[$counter]->martial_status=='M') echo 'Married'; else echo 'Single'; ?></td>
							</tr>
					<?php 
						}
					 }
						else
						{
							echo "<tr><td colspan='25' align='center'>(0) Record found.</td></tr>";
						}
					?>
					  
					  </tbody>
					</table>
				</div>
				<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
					  <tr>
						<td width="10%">&nbsp;</td>
						<td width="54%">&nbsp;</td>
						<td width="36%" align="right">&nbsp;
						 <?php 
						if($candidate && $exportEnabled)
						{
						?>	
				<!--		<img style="cursor: pointer;" src="<?php //echo IMAGEURL ?>/file-xls.gif" alt="Export to Excel" title="Click here for export data to excel sheet" onclick="window.location='<?php //echo ROOTURL.'/index.php?mod=export'; ?>'"/> 	-->
						<?php } ?>
						</td>
					  </tr>
					  <tr>
					  <?php 
						if($candidate)
						{
						?>
						<td colspan="2"><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?></td>
			   			<?php }?>
						<td align="right">
							<?php PaginationDisplay($totalCount);	?>
							</td>
					  </tr>
				</table>
			<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
  			<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
				<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />
  			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>
