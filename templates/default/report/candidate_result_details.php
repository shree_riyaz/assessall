<?php
/***********************************************************************************************
 * 				Developed by :- Akshay yadav
 * 				Module       :- Reports
 **************************************************************************************************/
unset($_SESSION['lang']);
if($_GET['lang'])
{
	$_SESSION['lang']== $_GET['lang'];
}
else
{
	$_SESSION['lang']=='hi';
}

$enurl = CreateURL('index.php','mod=report&do=candidate_result_details&candidate_id='.$_GET["candidate_id"].'&exam_id='.$_GET["exam_id"].'&test_id='.$_GET["test_id"].'&lang=eng');
$hiurl = CreateURL('index.php','mod=report&do=candidate_result_details&candidate_id='.$_GET["candidate_id"].'&exam_id='.$_GET["exam_id"].'&test_id='.$_GET["test_id"].'&lang=hi');
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Reports</title>

<link rel="stylesheet" type="text/css" href="<?php echo STYLE; ?>/wmd.css" />
<link rel="stylesheet" href="<?php echo ROOTURL;?>/css/style.css" />
<script type="text/javascript">
 document.createElement('header');
 document.createElement('nav');
 document.createElement('menu');
 document.createElement('section');
 document.createElement('article');
 document.createElement('aside');
 document.createElement('footer');
</script>

<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shCore.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shBrushGroovy.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/styles/shCoreDefault.css"/>

<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.Charts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionChartsExportComponent.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.jqueryplugin.js"; ?>'></script>

<script src="<?php echo ROOTURL; ?>/js/preview/jquery.scrollTo-1.4.3.1-min.js"></script>
<script>
function langch(val)
{http://assessall.com/assessall/index.php?mod=report&do=candidate_result_details&candidate_id=274&exam_id=35&test_id=67
if(val.value=='eng')
{
	location.href='<?php echo $enurl; ?>';
}
else
{
	location.href='<?php echo $hiurl; ?>';
}

}
</script>
<script language="javascript" type="text/javascript">
$(document).ready(
function()
{
//	$('pre').addClass('brush: js;');
	SyntaxHighlighter.defaults.toolbar = false;
	SyntaxHighlighter.all();
	
	$(document).on('click', '.assign-marks', function() {
		$('#'+$(this).attr('id').replace('assign', '')).show();

		$(this).val('Save Marks');
		$(this).removeClass('assign-marks');
		$(this).unbind('click');
		$(this).addClass('save-marks');
		$(this).bind('click');
	});

	$(document).on('click', '.save-marks', function() {

		var marks = $('#'+$(this).attr('id').replace('assign', ''));
		var max_marks = parseFloat($('#'+$(this).attr('id').replace('assign', 'max')).val());
		var question_id = $(this).attr('id').replace('assignmarks-', '');

		marks.val($.trim(marks.val()));
		var error = false;

		if(marks.val() == '')
		{
			alert('Please enter marks to be assigned.');
			error = true;
		}
		if(!marks.val().match(/^(?:[1-9]\d*|0)?(?:\.\d+)?$/))
		{
			alert('Please enter numeric value.');
			marks.val('');
			error = true;
		}
		if(marks.val() > max_marks)
		{
			alert('You can assign maximum '+max_marks+ ' marks to this question.');
			error = true;
		}

		if(error)
		{
			marks.val('');
			marks.focus();
			return false;
		}

		xajax_assignMarksToSubjectiveTypeQuestion('<?php echo $candidate_id; ?>', '<?php echo $test_id; ?>', question_id, marks.val(), 'test', '<?php echo $testInfo->maximum_marks; ?>');
		$(this).removeClass('save-marks');
		$(this).unbind('click');
		$(this).addClass('assign-marks');
		$(this).bind('click');
		
		$('#'+$(this).attr('id').replace('assign', '')).hide();
		$('#'+$(this).attr('id').replace('assign', 'final')).html(marks.val());
		$(this).val('Update Marks');
	});
});

function newGraph(path, div)
{
	var width = 400;
	var height = 200;
	var swf = "<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf?noCache=" + new Date().getMilliseconds();	
	var id = "c" + Math.floor(Math.random()*1000);
	
	FusionCharts.setCurrentRenderer('javascript');
	var myChart = new FusionCharts( swf, id, width, height, 0, 1);
	myChart.setXMLUrl(path);
	myChart.render(div);
}

function goToQuestion(question_id)
{
	$(document).scrollTo($('#'+question_id), 'slow', 
			function()
			{
				$('#'+question_id).next().addClass('scrolled', 1000);
				$('#'+question_id).next().removeClass('scrolled', 1000);
			});

//	setTimeout($('#'+question_id).next().removeClass('scrolled', 1000), 1000);
}

function sendMsg(val,val1,val2)
{
xajax_sendTestReult(val,val1,val2,'candidate');
function langchng()
{
	alert('ok');

}
}
</script>
<style>
.match_tab
{
	width: 500px;
	padding: 20px;
	border-collapse: collapse;
	border: 1px solid #e5e5e5;
}
.match_tab td
{
	border: 1px solid #e5e5e5;
	width: 50%;
	padding:15px;
}
.info-block
{
	float: left;
	margin: 8px;
}
.info-block fieldset 
{
	background-color: #f5f5f5;
	display: inline;
}
.info-block fieldset legend 
{
	font-weight: bold;
}
.questions-div
{
	margin: 0 20px;
	padding: 10px;
	background: #f5f5f5;
}
div.scrolled
{
	background-color: #ffd5c0;
}
div.subjective-answer
{
	padding: 10px;
	background: white;
	border: 1px solid #888;
}
a.mail-links 
{
	text-decoration: none;
	font-weight: bold;
	padding: 5px 10px;
	background: #eee;
	border: 1px solid #ccc;
}
</style>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
//include_once(ROOT . '/lib/Markdown/markdown.php');
?>
<body onLoad="document.frmlist.exam_id.focus();">

<?php include_once(TEMPPATH."/includes/header.php"); ?>
	<section id="main_sec" style='overflow:visible'>
<?php include_once(TEMPPATH."/includes/left_sec.php"); ?>
<?php //include_once(TEMPPATH."/includes/right_sec_report.php"); ?>
</section>



<tr>
	<td>
		<div id="right_sec">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				<div class='right_cat_sec' style="margin-top:50px; min-height:200px; overflow:hidden">
<div  class='green_head_wrp' ><div class="green_header"><a href="<?php echo CreateURL('index.php',"mod=report&do=view_test_result&exam_id=$exam_id&test_id=$test_id&ins=".$_GET['ins']); ?>"><?php echo $testInfo->test_name ?></a>
						&#8614;<?php echo ' '.ucwords(strtolower($candidateInfo->first_name.' '.$candidateInfo->last_name)); ?></div></div>
				
						

<div style="float: right;">
	<?php 
	//echo $candidateInfo->id."', '".$examInfo->id."', '".$testInfo->id;
	//$onClickCandidate = "xajax_sendTestReult('".$candidateInfo->id."', '".$examInfo->id."', '".$testInfo->id."', );";
	//$url="index.php?mod=report&do=candidate_result_details&candidate_id=&candidate_id&exam_id=$exam_id&test_id=$test_id";
	?>
<?php
		/*	
			$exact_page=$_SERVER['REQUEST_URI'];
			$exact_page=str_replace("&","&amp;",$exact_page);
			$exact_page=str_replace("=","%3D;",$exact_page);
			$url="http://" . $_SERVER['HTTP_HOST'] . $exact_page;*/
	?>
	<!--<span style="margin-top:-3px;" ><a onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo $url; ?>','sharer','toolbar=0,status=0,width=748,height=525');" href="javascript: void(0)"><img  height="28px" width="30px" alt="Facebook" title="Share on Facebook" src="<?php echo IMAGEURL; ?>/FacebookIcon.jpg"></a></span>&nbsp;&nbsp;-->
	
	<a href="javascript:void(0);" class="mail-links" onClick="sendMsg(<?php echo $candidateInfo->id; ?>,<?php echo $examInfo->id;?>,<?php echo $testInfo->id; ?>);">Mail To Inbox</a>&nbsp;&nbsp;
</div>

<div class="info-block">
	
	<div class="show_news_sec">	<div class='show_news_sec_head'>Basic Information</div>
					
	<table width="" border="0" cellpadding="4" cellspacing="3">
	<tr>
		<td width="180"><div>Student Name</div></td>
		<td width="180"><div><?php echo ucwords(strtolower($candidateInfo->first_name.' '.$candidateInfo->last_name)); ?></div></td>
	</tr>
	  
	<tr>
		<td><div>Course</div></td>
		<td><div><?php echo $examInfo->exam_name; ?></div></td>
	</tr>
	  
	<tr>
		<td><div>Test</div></td>
		<td><div><?php echo $testInfo->test_name; ?></div></td>
	</tr>
	  
	<tr>
		<td><div>Total Questions</div></td>
		<td><div><?php echo count($testQuestions); ?></div></td>
	</tr>
	  
	<tr>
		<td><div>Correct Answers</div></td>
		<td><div><?php echo $no_correctans; ?></div></td>
	</tr>
	  
	<tr>
		<td><div>Wrong Answers</div></td>
		<td><div><?php echo $no_wrongans; ?></div></td>
	</tr>
	
	<tr>
		<td><div>Skipped Questions</div></td>
		<td><div><?php echo $no_skipped; ?></div></td>
	</tr>
	  
	<tr>
		<td><div>Course's Total Marks</div></td>
		<td><div><?php echo $testInfo->maximum_marks; ?></div></td>
	</tr>
	
	<tr>
		<td><div>Total Percentage Marks</div></td>
		<td><div><?php echo number_format($testHistory->percentage, 2, '.', '').'%'; ?></div></td>
	</tr>
	</table>
	</div>
</div>

<div style="clear: both;"></div>

<?php
$passed_subject = 0;
$failed_subject = 0;

$timeTrackXml = "<chart caption='Time Track' showLegend='1' showPercentValues='0' bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' showNames='1' useEllipsesWhenOverflow='0' showBorder='1'>";

for($index=0; $index<count($test_subject_result); $index++)
{ 
	$label = ucfirst(stripslashes($test_subject_result[$index]['subject_name']));
	$value = $test_subject_result[$index]['solve_time'];
	$display_value = (floor($value/60)).'M '.($value%60).' S';
	$timeTrackXml .= "<set label='$label' value='$value' displayValue='$display_value' />";
	
	($test_subject_result[$index]['result'] == 'Pass') ? $passed_subject++ : $failed_subject++;
?>

<div class="info-block">
	<fieldset class="rounded">
	<legend><?php echo ucfirst(stripslashes($test_subject_result[$index]['subject_name'])); ?>'s Result</legend>
	<table cellpadding="4" cellspacing="3" width="">
	<tr>
		<td width="174"><div>Subject Name</div></td>
		<td width="94"><div><? echo ucfirst(stripslashes($test_subject_result[$index]['subject_name'])); ?></div></td>
	</tr>
	<tr>
		<td><div>Maximum Marks</div></td>
		<td><div><? echo $test_subject_result[$index]['subject_max_marks']; ?></div></td>
	</tr>
	<tr>
		<td><div>Minimum Passing Marks</div></td>
		<td><div><? echo $test_subject_result[$index]['subject_min_marks'].'%'; ?></div></td>
	</tr>
	<tr>
		<td><div>Percentage</div></td>
		<td><div><? echo $test_subject_result[$index]['subject_percentage'].'%'; ?></div></td>
	</tr>
	<tr>
		<td><div>Subject Result</div></td>
		<td><div><? echo $test_subject_result[$index]['result']; ?></div></td>
	</tr>
	</table>
	</fieldset>
</div> <?php 

}

$timeTrackXml .= "</chart>";

$marksXml = "<chart caption='Subject Result' showLegend='1' showPercentValues='0' bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' showNames='1' useEllipsesWhenOverflow='0' showBorder='1'>";
$marksXml .= "<set label='Pass' value='$passed_subject' color='55ff55' />";
$marksXml .= "<set label='Fail' value='$failed_subject' color='ff5555' />";
$marksXml .= "</chart>";

?>

<div style="clear: both;"></div>

<div class="info-block">
	<fieldset class="rounded">
	<legend>Time Track</legend>
	<div id="time-graph" style="text-align: center;padding: 10px;"></div>
	</fieldset>
</div>

<div class="info-block">
	<fieldset class="rounded">
	<legend>Subject Result</legend>
	<div id="subject-result-graph" style="text-align: center;padding: 10px;"></div>
	</fieldset>
</div>

<?php

$count = 0;
$totalTime = 0;
$quetionTimeXml = "<chart caption='Time Taken Per Question' xAxisName='Question' yAxisName='Time Taken To Solve' defaultNumberScale='S' numberScaleValue='60,60' numberScaleUnit='M,H' bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' showNames='1' useEllipsesWhenOverflow='0' showBorder='1'>";
foreach($questionHistory as $key => $que)
{
	$count++;
	$totalTime += $que->solve_time;
	$quetionTimeXml .= "<set label='$count' value='$que->solve_time' link='javascript:goToQuestion(".'\"'.$que->question_id.'\"'.");' />";
}
$averageTime = $totalTime/$count; 
$quetionTimeXml .= "<trendLines><line startValue='$averageTime' color='009933' displayvalue='Average' /></trendLines>";
$quetionTimeXml .= "</chart>";

?>

<div class="info-block">
	<fieldset class="rounded">
	<legend>Time Track Per Question</legend>
	<div id="question-time-graph" style="text-align: center;padding: 10px;"></div>
	</fieldset>
</div>

<div style="clear: both;"></div>
<br />
							  
<div class="bgblue" style="margin-left: 10px;">Detailed Results</div>
<br />
<div style="font-weight: bold;margin-left: 12px;">Student's selection is marked in Green (<font color="green">correct answer</font>), Red (<font color="red">wrong answer</font>) and Not attempted or Skipped (<font color="brown">Dark Red</font>).</div>
<!--
<label>Select Language<label>
<label>
<form name='langform' action="<?php echo CreateURL('index.php','mod=report&do=view_test_result&exam_id=$exam_id&test_id=$test_id ') ?>">
<select name='lang' onchange='langch(this)'>
<option value='eng'>English</option>
<option value='hi'>Hindi</option>
</select>
</form>
<label>-->
<?php

global $DB;
$path = ROOTURL . '/uploadfiles/';
$ques_no=0;

$subjectTime = array();

foreach($questionHistory as $key => $que)
{	
	echo '<label id="'.$que->question_id.'">&nbsp;<br>&nbsp;</label>';
	echo '<div class="questions-div rounded">';
	$ques_no=$ques_no+1;
	
	$sql="select q.id as question_id, q.question_title, q.question_type, q.marks from question as q
			where q.id=$que->question_id";
	
	$result = $DB->ExecuteQuery($sql);
	$question= $DB->FetchObject($result);
	
	$img="";
	if($question->question_type=='I')
	{
		$getimg = "select image_path 
					from question
        			join question_image on question_image.question_id = question.id
					where question.id = '".$que->question_id."'";
	
		$getImages = $DB->RunSelectQuery($getimg);
		$total_img = count($getImages);
		
		$img = "<br><br>";
		for($count_img = 0; $count_img < $total_img; $count_img++)
		{
			$images= $getImages[$count_img];
			$final_path = $path.$images->image_path;
			if($images->image_path == '0' || $images->image_path == null)
			{
				$final_path = ROOTURL . "/images/question-mark.png";
			}
				
			$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
		}
	}
	
	elseif($question->question_type == 'MT')
	{
		$left_cols = $DB->SelectRecords('question_match_left',"question_id='$question->question_id'");
		$right_cols = $DB->SelectRecords('question_match_right',"question_id='$question->question_id'");
	}

	//$title = (($question->question_title));
	$title = $question->question_title;
				
				
				// $title =  extractString($title, '[en]', '[en:]');
			
				
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$title = " ".$title;
				$ini = strpos($title, $start);
				
				if ($ini == 0 ) 
				{
					$title = (($question->question_title));
				}
				 else
				 {
				$ini += strlen($start);
				$len = strpos($title, $end, $ini) - $ini;
				$title =  substr($title, $ini, $len);
				}
	//$title = Markdown($title);
	//$title = str_replace('&amp;', '&', $title);
	
	echo "<b>Question $ques_no: </b><br>".$title.$img;
										
	$getans="select q.id as question_id, ans.id as ans_id, ans.answer_title, q.question_title 
				from question as q
				join answer as ans on ans.question_id=q.id
				where q.id=$question->question_id
				group by ans.id";
	
	if(($question->question_type == 'MT') || ($question->question_type == 'S'))
	{
		$getans = 'select *,id as question_id from question where id ="'.$question->question_id.'"';
	}
	
	$getAnsResult = $DB->ExecuteQuery($getans);
				
	$question_id_match=$question->question_id;
	$correctansDetails = $DB->SelectRecord('question_answer',"question_id=$question_id_match",'GROUP_CONCAT(answer_id) as answers','GROUP BY question_id');
	
	if($correctansDetails->answers)
	{
		$correct_ans_id = explode(',', $correctansDetails->answers);
		$correct_ans_title=$DB->SelectRecords('answer',"id IN (".implode(',',$correct_ans_id).")",'*','order by id');
	}
	
	$color="brown";
	$numbering=1;
	echo "<table width='80%'>";
	while($option = $DB->FetchObject($getAnsResult))
	{											
		if($question->question_type=='MT')
		{
			$condition = '';
			
			$condition .= "(candidate_id = '$candidate_id') ";
			$condition .= " AND (exam_id = '$exam_id') ";
			$condition .= " AND (test_id = '$test_id') ";
			$condition .= " AND (question_id = '$question->question_id') ";
			
			$match_history = $DB->SelectRecords('candidate_match_question_history', $condition);
			$given_match_answer = array();
			
			foreach($match_history as $mh)
			{
				$given_match_answer[$mh->question_match_left_id] = $mh->given_answer_id;
			}
			
			$given_answer = $given_match_answer;
			
			$blank = 1;
			$correct = 1;
			
			$given = '<table class="match_tab">';
			$corrt = '<table class="match_tab">';
			
			foreach($given_answer as $l => $r)
			{
				$lcol = $DB->SelectRecord('question_match_left',"id='$l'");
				$answer_id = $lcol->answer_id;
				
				$sub_given = "<td>$lcol->value</td>
							<td>";
				
				$td_clr = '';
				if($r != '')
				{
					$blank = 0;

					$given_rcol = $DB->SelectRecord('question_match_right',"id='$r'");
					$sub_given .= $given_rcol->value;
					
					$td_clr = 'green';
					if($answer_id != $r)
					{
						$correct = 0;
						$td_clr = 'red';
					}
				}
				else
				{
					$td_clr = 'brown';
					$correct = 0;
				}
				
				$given .= "<tr style='color:$td_clr;'>$sub_given</td></tr>";
			}
			
			foreach($left_cols as $lcol)
			{
				$answer_id = $lcol->answer_id;
				
				$sub_corrt = "<td>$lcol->value</td>
							<td>";
				
				$corrt_rcol = $DB->SelectRecord('question_match_right',"id='$answer_id'");
				$sub_corrt .= $corrt_rcol->value;

				$td_clr = 'brown';
				
				$corrt .= "<tr style='color:$td_clr;'>$sub_corrt</td></tr>";
			}
			
			$given .= "</table>";
			$corrt .= "</table>";
			
			$clr = 'red';
			if ($correct == 1)
			{
				$clr = 'green';						
			}
			
			if($blank == 1)
			{
				$clr = $color;
			}
			//else
			{
				echo "<tr><td><font color='$clr'>$given</font></td></tr>";
			}
			
			unset($_SESSION['question-'.$question->question_id]['given_answer']);
		}
		elseif($question->question_type=='S')
		{
			$assigned_marks = 'N/A';
			$button_text = 'Assign Marks';
			if(($que->marks_obtained !== null) && ($que->marks_obtained !== ''))
			{
				$assigned_marks = $que->marks_obtained;
				$button_text = 'Update Marks';
			}
			?>
			<tr>
				<td>
					<b>Given Answer: </b>
					<div class="subjective-answer">
						<?php echo nl2br(htmlspecialchars(str_replace('  ', '&nbsp;&nbsp;', $que->given_answer))); ?>
					</div><br />
					<div class="assign-marks-div">
						
						<b>Maximum Marks : <?php echo $question->marks; ?></b><br/>
						<b>Marks : <label id="finalmarks-<?php echo $que->question_id; ?>"><?php echo $assigned_marks; ?></label></b>
						
						<?php if($_SESSION['admin_user_type'] != 'P') { ?>
						<div style="float: right;">
							<input type="hidden" id="maxmarks-<?php echo $que->question_id; ?>" value="<?php echo $question->marks; ?>" />
							<input type="text" style="width: 50px;display: none;" id="marks-<?php echo $que->question_id; ?>" value="<?php echo $que->marks_obtained; ?>" maxlength="6" class="rounded textfield" />
							<input type="button" id="assignmarks-<?php echo $que->question_id; ?>" value="<?php echo $button_text; ?>" class="buttons rounded assign-marks" />
						</div>
						<?php } ?>
					</div>
				</td>
			</tr><?php
		}
		else
		{
			$answer = $option->answer_title;
			
			
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$answer = " ".$answer;
				$ini = strpos($answer, $start);
				if ($ini == 0) 
				{
					$answer = $option->answer_title;
				}
				 else
				 {
				$ini += strlen($start);
				$len = strpos($answer, $end, $ini) - $ini;
				$answer =  substr($answer, $ini, $len);
				}
					
					
			if($question->question_type=='I')
				$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";

			
			$given_answer_id = explode(',',$que->given_answer_id);
			if(in_array($option->ans_id, $correct_ans_id))
			{
				if(in_array($option->ans_id, $given_answer_id))
				{
					echo '<tr><td><font color="green">'.$numbering.'. '.$answer.'</font>&nbsp;&nbsp;<img src="'.ROOTURL.'/images/right.gif"></td></tr>';
					unset($_SESSION['question-'.$question->question_id]['given_answer']);
				}
				else
					echo "<tr><td><font color=$color>$numbering.&nbsp;$answer</font></td></tr>";
			}
			else
			{
				if(in_array($option->ans_id, $given_answer_id))
				{
					echo '<tr><td><font color="red">'.$numbering.'. '.$answer.'</font>&nbsp;&nbsp;<img src="'.ROOTURL.'/images/wrong.gif"></td></tr>';
					unset($_SESSION['question-'.$question->question_id]['given_answer']);
				}
				else
				echo "<tr><td><font color=$color>$numbering.&nbsp;$answer</font></td></tr>";
			}				
		}
		$numbering++;	
	}
	echo "</table><br/>";
	$corrt_ans= $corrt;
	
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$corrt_ans = " ".$corrt_ans;
				$ini = strpos($corrt_ans, $start);
				if ($ini == 0) 
				{
					
					$corrt_ans= $corrt;
				}
				 else
				 {
				 
				$ini += strlen($start);
				$len = strpos($corrt_ans, $end, $ini) - $ini;
				$corrt_ans =  substr($corrt_ans, $ini, $len);
				}
					
	if($question->question_type=='MT')
	{
			echo "Correct Answer is:<font color=$color>".$corrt_ans."</font><br/>";
	}
	elseif($question->question_type !='S')
	{
		$corr_ans = array();
		foreach($correct_ans_title as $cat)
		{
			$corr_ans[] = $cat->answer_title;
			if($question->question_type=='I')
				$corr_ans[] = "<img height='50' width='50' src=". $path.$cat->answer_title . ">";
		}
			
		$corr_ans = implode(', ', $corr_ans);
		$corrt_ans= $corr_ans;
		
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$corrt_ans = " ".$corrt_ans;
				$ini = strpos($corrt_ans, $start);
				if ($ini == 0) 
				{
					
					$corrt_ans= $corr_ans;
				}
				 else
				 {
				 
				$ini += strlen($start);
				$len = strpos($corrt_ans, $end, $ini) - $ini;
				$corrt_ans =  substr($corrt_ans, $ini, $len);
				}
		
		echo "Correct Answer: <font color=$color>".$corrt_ans."</font><br/>";
	}
	
	if($question->min_time_to_solve != '')
	echo "Minimum Solving Time : <font color=$color>".(floor($question->min_time_to_solve/60)).' Minutes '.($question->min_time_to_solve%60).' Seconds</font><br>';
	
	if($question->max_time_to_solve != '')
	echo "Maximum Solving Time : <font color=$color>".(floor($question->max_time_to_solve/60)).' Minutes '.($question->max_time_to_solve%60).' Seconds</font><br>';
	
	echo "Time Taken to Solve: <font color=$color>".(floor($que->solve_time/60)).' Minutes '.($que->solve_time%60).' Seconds</font>';
	echo '</div>';
	
}
?>
						
<?php 
if($exportEnabled)
{ 
	$key = uniqid();
	?>
	<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
		<tr>
			<td width="36%" align="right">&nbsp;
				<!--<img 
					style="cursor: pointer;" 
					src="<?php //echo IMAGEURL ?>/file-pdf.gif" 
					alt="Export to Pdf" 
					title="Click here for export data to Pdf" 
					onclick="window.location='<?php //echo ROOTURL."/index.php?mod=export&candidate_result_details=$key&candidate_id=$candidate_id&test_id=$test_id&exam_id=$exam_id"; ?>'"
				/>-->
			</td>
		</tr>
	</table><?php
} 
?>

				</div><!--Div Contents closed-->
			</div><!--Div main closed-->
		</div><!--Content div closed-->
	</td>
</tr>
</table>	
</div><!--Outer wrapper closed-->
</body>

<script>
FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf", 'subject-result-chart', 430, 300, 0, 1);
myChart.setXMLData("<?php echo $marksXml; ?>");
myChart.render('subject-result-graph');

FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf", 'time-chart', 430, 300, 0, 1);
myChart.setXMLData("<?php echo $timeTrackXml; ?>");
myChart.render('time-graph');

FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/Line.swf", 'question-time-chart', 850, 285, 0, 1);
myChart.setXMLData("<?php echo $quetionTimeXml; ?>");
myChart.render('question-time-graph');
</script>

</html>