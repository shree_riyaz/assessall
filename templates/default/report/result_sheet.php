
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Reports</title>
<link rel="stylesheet" href="<?php echo ROOTURL;?>/css/style.css" />
<?php 


?>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.Charts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionChartsExportComponent.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.jqueryplugin.js"; ?>'></script>

<script language="javascript" type="text/javascript">
$(document).ready(
function()
{
	$('#year').change(
	function()
	{
		if($(this).val() == '')
			$('#tr_month').hide();
		else
			$('#tr_month').show();
	});

	$('#year').trigger('change');
});
function check_search()
{
	var msg = '';
	var flag=0;
	
	var exam_id = document.getElementById('exam_id');

	if(exam_id.value=='')
	{
		msg+= "Please select course.\n";
		focusAt=exam_id;
		flag=1;
	}
	
	else if(year.value=='')
	{
		msg+= "Please select year.\n";
		focusAt=year;
		flag=1;
	}
	
	if(msg!='')
	{
		alert(msg);
		focusAt.focus();
		return false;
	}
	return true;
}

</script>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>

<body onLoad="document.frmlist.exam_id.focus();">

<?php include_once(TEMPPATH."/includes/header.php"); ?>
	<section id="main_sec" style='overflow:visible'>
<?php include_once(TEMPPATH."/includes/left_sec.php"); ?>
<?php include_once(TEMPPATH."/includes/right_sec_report.php"); ?>
</section>
	
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				
						<!--Outer wrapper closed-->

<script language="javascript" type="text/javascript">

<?php if($return) { ?>
document.getElementById('exam_id').value='<?php echo $exam_id; ?>';
<?php } ?>

<?php if($showGraph) { ?>
FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/StackedColumn3D.swf", 'chart', 870, 400, 0, 1);
myChart.setXMLData("<?php echo $graphXml; ?>");
myChart.render('graph');
<?php } ?>

</script>
</body>
</html>