<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="stylesheet" type="text/css" href="<?php echo STYLE; ?>/wmd.css" />
<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shCore.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shBrushGroovy.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/styles/shCoreDefault.css"/>

<script language="javascript" type="text/javascript">
$(document).ready(
function()
{
//	$('pre').addClass('brush: js;');
	SyntaxHighlighter.defaults.toolbar = false;
	SyntaxHighlighter.all();
});
</script>

<style>
body
{
	margin: 0 auto;
	padding: 0;
	font-family:arial, helvetica, sans-serif;
}
img
{
	border: none; 
}

.fieldset
{
	border: 1px solid #ddd;
	background-color: #eee;
	padding: 5px;
}
.legend 
{
	color:#af5403; 
	font-weight: bold;
}
.match_tab
{
	width: 500px;
	padding: 20px;
	border-collapse: collapse;
	border: 1px solid #e5e5e5;
}
.match_tab td
{
	border: 1px solid #e5e5e5;
	width: 50%;
	padding:15px;
}
.brown
{
	color: #990000;
}
.red
{
	color: red;
}
.green
{
	color: green;
}
.subjective-answer
{
	padding: 10px;
	background: white;
	border: 1px solid #888;
}

</style>

</head>
<?php include_once(ROOT . '/lib/Markdown/markdown.php'); ?>
<body>
<div id="">
<table border="0" cellspacing="0" cellpadding="0" width="700" id="tbl_outer">
<tr>
	<td>
		<div class="legend">Student Test Result Details</div>
					
<table width="100%" border="0" cellpadding="4" cellspacing="3">
  <tr>
	<td width="180"><div>Student Name:</div></td>
	<td><div><?php echo ucwords(strtolower($candidateInfo->first_name.' '.$candidateInfo->last_name)); ?></div></td>
  </tr>
  
  <tr>
	<td><div>Course:</div></td>
	<td><div><?php echo $examInfo->exam_name; ?></div></td>
  </tr>
  
  <tr>
	<td><div>Test:</div></td>
	<td><div><?php echo $testInfo->test_name; ?></div></td>
  </tr>
  
  <tr>
	<td><div>Total Questions:</div></td>
	<td><div><?php echo count($testQuestions); ?></div></td>
  </tr>
  
  <tr>
	<td><div>Correct Answers:</div></td>
	<td><div><?php echo $no_correctans; ?></div></td>
  </tr>
  
  <tr>
	<td><div>Wrong Answers:</div></td>
	<td><div><?php echo $no_wrongans; ?></div></td>
  </tr>

  <tr>
	<td><div>Skipped Questions:</div></td>
	<td><div><?php echo $no_skipped; ?></div></td>
  </tr>
  
  <tr>
	<td><div>Course's Total Marks:</div></td>
	<td><div><?php echo $testInfo->maximum_marks; ?></div></td>
  </tr>
  <tr>
	<td><div>Total Percentage Marks:</div></td>
	<td><div><?php echo number_format($testHistory->percentage, 2, '.', '').'%'; ?></div></td>
  </tr>
<?php 
for($index=0; $index<count($test_subject_result); $index++)
{ ?>

<tr>
	<td colspan="2"><br />
		<div class="rounded fieldset">
			<div class="subject-head legend">&nbsp;<?php echo ucfirst(stripslashes($test_subject_result[$index]['subject_name'])); ?>'s Result</div>

		<table cellpadding="4" cellspacing="3" width="100%">
		<tr>
			<td width="180"><div>Subject Name</div></td>
			<td><div><? echo ucfirst(stripslashes($test_subject_result[$index]['subject_name'])); ?></div></td>
		</tr>
		<tr>
			<td><div>Maximum Marks</div></td>
			<td><div><? echo $test_subject_result[$index]['subject_max_marks']; ?></div></td>
		</tr>
		<tr>
			<td><div>Minimum Passing Marks</div></td>
			<td><div><? echo $test_subject_result[$index]['subject_min_marks'].'%'; ?></div></td>
		</tr>
		<tr>
			<td><div>Percentage</div></td>
			<td><div><? echo $test_subject_result[$index]['subject_percentage'].'%'; ?></div></td>
		</tr>
		<tr>
			<td><div>Subject Result</div></td>
			<td><div><? echo $test_subject_result[$index]['result']; ?></div></td>
		</tr>
		</table>
		</div>
	</td>
  </tr> <?php 
}
?>
</table>
<!--
<br />
<div class="legend">Detailed Results</div>
<div>Student's selection is marked in Green (<font color="green">correct answer</font>), Red (<font color="red">wrong answer</font>) and<br />Not attempted or Skipped (<font color="brown">Dark Red</font>).</div>
<br />
-->
<?php
/*
global $DB;
$path = ROOTURL . '/uploadfiles/';
$ques_no=0;

foreach($questionHistory as $key => $que)
{	
	$ques_no=$ques_no+1;
	
	$sql="select q.id as question_id, q.question_title, q.question_type, q.marks from question as q
			where q.id=$que->question_id";
	
	$result = $DB->ExecuteQuery($sql);
	$question= $DB->FetchObject($result);
	
	$img="";

	if ($question->question_type=='I')
	{
		$getimg = "select image_path 
					from question
        			join question_image on question_image.question_id = question.id
					where question.id = '".$que->question_id."'";
										
		$getImages = $DB->RunSelectQuery($getimg);
		$total_img = count($getImages);
		
		$img = "<br />";
		for($count_img = 0; $count_img < $total_img; $count_img++)
		{
			$images= $getImages[$count_img];
			$final_path = $path.$images->image_path;
			if($images->image_path == '0' || $images->image_path == null)
			{
				$final_path = ROOTURL . "/images/question-mark.png";
			}
				
			$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
		}
	}
	elseif ($question->question_type == 'MT')
	{
		$left_cols = $DB->SelectRecords('question_match_left',"question_id='$question->question_id'");
		$right_cols = $DB->SelectRecords('question_match_right',"question_id='$question->question_id'");
	}

	$title = (($question->question_title));
	//$title = Markdown($title);
	//$title = str_replace('&amp;', '&', $title);

	echo "<table width='500'><tr><td width='500'>";
	echo "<b>Question $ques_no: </b>".$title.$img;
	echo "</td></tr></table>";
	
	$getans="select q.id as question_id, ans.id as ans_id, ans.answer_title, q.question_title 
				from question as q
				join answer as ans on ans.question_id=q.id
				where q.id=$question->question_id
				group by ans.id";
										
	if(($question->question_type == 'MT') || ($question->question_type == 'S'))
	{
		$getans = 'select *,id as question_id from question where id ="'.$question->question_id.'"';
	}
										
	$getAnsResult = $DB->ExecuteQuery($getans);
				
	$question_id_match = $question->question_id;
	$correctansDetails = $DB->SelectRecord('question_answer',"question_id=$question_id_match",'GROUP_CONCAT(answer_id) as answers','GROUP BY question_id');
	
	if($correctansDetails->answers)
	{
		$correct_ans_id = explode(',', $correctansDetails->answers);
		$correct_ans_title=$DB->SelectRecords('answer',"id IN (".implode(',',$correct_ans_id).")",'*','order by id');
	}
										
	$color="brown";
	$numbering=1;
	echo "<table width='600'>";
										
	while($option = $DB->FetchObject($getAnsResult))
	{											
		if($question->question_type=='MT')
		{
			$condition = '';
			
			$condition .= "(candidate_id = '$candidate_id')";
			$condition .= "AND (exam_id = '$exam_id')";
			$condition .= "AND (test_id = '$test_id')";
			$condition .= "AND (question_id = '$question->question_id')";
			
			$match_history = $DB->SelectRecords('candidate_match_question_history', $condition);
			$given_match_answer = array();
			
			foreach($match_history as $mh)
			{
				$given_match_answer[$mh->question_match_left_id] = $mh->given_answer_id;
			}
			
			$given_answer = $given_match_answer;
			
			$blank = 1;
			$correct = 1;
			
			$given = '<table class="match_tab">';
			$corrt = '<table class="match_tab">';
			
			foreach($given_answer as $l => $r)
			{
				$lcol = $DB->SelectRecord('question_match_left',"id='$l'");
				$answer_id = $lcol->answer_id;
				
				$sub_given = "<td>$lcol->value</td>
							<td>";
				
				$td_clr = '';
				if($r != '')
				{
					$blank = 0;
	
					$given_rcol = $DB->SelectRecord('question_match_right',"id='$r'");
					$sub_given .= $given_rcol->value;
					
					$td_clr = 'green';
					if($answer_id != $r)
					{
						$correct = 0;
						$td_clr = 'red';
					}
				}
				else
				{
					$td_clr = 'brown';
					$correct = 0;
				}
				
				$given .= "<tr class='$td_clr'>$sub_given</td></tr>";
			}
			
			foreach($left_cols as $lcol)
			{
				$answer_id = $lcol->answer_id;
				
				$sub_corrt = "<td>$lcol->value</td>
							<td>";
				
				$corrt_rcol = $DB->SelectRecord('question_match_right',"id='$answer_id'");
				$sub_corrt .= $corrt_rcol->value;
	
				$td_clr = 'brown';
				
				$corrt .= "<tr class='$td_clr'>$sub_corrt</td></tr>";
			}
			
			$given .= "</table>";
			$corrt .= "</table>";
			
			$clr = 'red';
			if ($correct == 1)
			{
				$clr = 'green';						
			}
			
			if($blank == 1)
			{
				$clr = $color;
			}
			//else
			{
				echo '<tr><td><font class="'.$clr.'">'.$given.'</font></td></tr>';
			}
			
			unset($_SESSION['question-'.$question->question_id]['given_answer']);
		}
		elseif($question->question_type=='S')
		{
			$assigned_marks = 'N/A';
			if(($que->marks_obtained !== null) && ($que->marks_obtained !== ''))
			{
				$assigned_marks = $que->marks_obtained;
			}
			 ?>
			<tr>
				<td width="500">
					<b>Given Answer: </b>
					<div width="500" class="subjective-answer">
						<?php echo nl2br(htmlspecialchars(str_replace('  ', '&nbsp;&nbsp;', $que->given_answer))); ?>
					</div>
					<div class="assign-marks-div">
						<b>Maximum Marks : <?php echo $question->marks; ?></b><br/>
						<b>Marks : <label id="finalmarks-<?php echo $que->question_id; ?>"><?php echo $assigned_marks; ?></label></b>
					</div>
				</td>
			</tr><?php
		}
		
		else
		{
			$answer = $option->answer_title;
			if($question->question_type == 'I')
				$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";

			$given_answer_id = explode(',',$que->given_answer_id);
			if(in_array($option->ans_id, $correct_ans_id))
			{
				if(in_array($option->ans_id, $given_answer_id))
				{
					echo '<tr><td><font class="green">'.$numbering.'. '.$answer.'</font>&nbsp;&nbsp;<img src="'.ROOTURL.'/images/right.gif"></td></tr>';
					unset($_SESSION['question-'.$question->question_id]['given_answer']);
				}
				else
					echo '<tr><td><font class="brown">'.$numbering.'&nbsp;'.$answer.'</font></td></tr>';
			}
			else
			{
				if(in_array($option->ans_id, $given_answer_id))
				{
					echo '<tr><td><font class="red">'.$numbering.'. '.$answer.'</font>&nbsp;&nbsp;<img src="'.ROOTURL.'/images/wrong.gif"></td></tr>';
					unset($_SESSION['question-'.$question->question_id]['given_answer']);
				}
				else
					echo '<tr><td><font class="brown">'.$numbering.'&nbsp;'.$answer.'</font></td></tr>';
			}				
		}
		$numbering++;	
	}
										
	echo "</table><br/>";
										
	if($question->question_type=='MT')
	{
			echo 'Correct Answer:<font class="brown"><br />'.$corrt."</font><br/>";
	}
	elseif($question->question_type !='S')
	{
		$corr_ans = array();
		foreach($correct_ans_title as $cat)
		{
			$corr_ans[] = $cat->answer_title;
			if($question->question_type=='I')
				$corr_ans[] = "<img height='50' width='50' src=". $path.$cat->answer_title . ">";
		}
			
		$corr_ans = implode(', ', $corr_ans);
		echo 'Correct Answer: <font class="brown">'.$corr_ans."</font><br/>";
	}
										
	if($question->min_time_to_solve != '')
	echo 'Minimum Solving Time : <font class="brown">'.(floor($question->min_time_to_solve/60)).' Minutes '.($question->min_time_to_solve%60).' Seconds</font><br>';
	
	if($question->max_time_to_solve != '')
	echo 'Maximum Solving Time : <font class="brown">'.(floor($question->max_time_to_solve/60)).' Minutes '.($question->max_time_to_solve%60).' Seconds</font><br>';
	
	echo 'Time Taken to Solve: <font class="brown">'.(floor($que->solve_time/60)).' Minutes '.($que->solve_time%60).' Seconds</font>';
	echo '<br><br><br>';
}
*/ ?>

	</td>
</tr>
</table>	
</div><!--Outer wrapper closed-->

</body>
</html>