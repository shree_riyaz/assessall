<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Reports</title>

<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.Charts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionChartsExportComponent.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.jqueryplugin.js"; ?>'></script>
<link rel="stylesheet" href="<?php echo ROOTURL;?>/css/style.css" />
<style>
.mailer-parent
{
	position: absolute;
	z-index: 100;
	background: #e5d7f8;
	border: 1px solid #888;
	margin-left: 15px;
	display: none;
}
.mailer-parent div 
{
	padding: 5px;
	border: 1px solid #aaa;
}
.mailer-parent div a 
{
	display: block;
	text-decoration: none;
	font-weight: bold;
}
</style>

<script>
function newGraph(data, div, type)
{
	var width = 400;
	var height = 200;
	var swf = "<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf?noCache=" + new Date().getMilliseconds();
	if(type == 'stacked')
	{
		width = 880;
		height = 500;
		swf = "<?php echo ROOTURL; ?>/lib/FusionCharts/Column3D.swf?noCache=" + new Date().getMilliseconds();
	}
	
	var id = "c" + Math.floor(Math.random()*1000);
	FusionCharts.setCurrentRenderer('javascript');
	var myChart = new FusionCharts( swf, id, width, height, 0, 1);
	myChart.setXMLData(data);
	myChart.render(div);
}
</script>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
$total_sub_td = count($exam_subject)*2;
?>


<body onLoad="document.frmlist.exam_id.focus();">

<?php include_once(TEMPPATH."/includes/header.php"); ?>
	<section id="main_sec" style='overflow:visible'>
<?php include_once(TEMPPATH."/includes/left_sec.php"); ?>
<?php //include_once(TEMPPATH."/includes/right_sec_report.php"); ?>
</section>

		<div id="right_sec">
			<?php // include_once(CURRENTTEMP."/"."navigation.php"); ?>
			
					<form action="" method="post" name="frmlist" id="frmlist">
						<div class='right_cat_sec' style="margin-top:50px; min-height:200px; overflow:hidden">
<div  class='green_head_wrp' ><div class="green_header"><a href="<?php echo CreateURL('index.php','mod=report&do=result_sheet&exam_id='.$exam_id.'&return=1'); ?>">
								<?php echo $subject_result[0]->exam_name; ?>
							</a>&#8614;<?php echo ' '.($test_name = $subject_result[0]->test_name); ?></div></div>
							
						
			
<table border="0" cellspacing="1" cellpadding="0" width="96%" style="">
<tr>
	<td align="right"><?php echo getPageRecords();?> </td>
</tr>
</table>

<div id="celebs" <?php if($total_sub_td >= 4) {?> style="overflow-x:scroll" <?php } ?> >

<?php

$total_marks=0; 
?>
						
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="96%" bgcolor="#e1e1e1" class="data">
<thead>
	<tr class="tblheading">
		<td width="1%" rowspan="4">#</td>
		<td width="20%" rowspan="4">Student Name</td>
		<td width="13%" id="marks_obtain_td"  colspan="<?php echo $total_sub_td; ?>">Details Of Subjects</td>
		<td width="9%" rowspan="4">Total Marks Obtain <span id="total_marks_span"></span></td>
		<td width="10%" rowspan="4">Percentage (%)</td>
		<td width="9%" rowspan="4">Result</td>
		<td width="9%" rowspan="4">Action</td>
	</tr>
	
	<tr class="tblheading">
		<?php
		for($index=0; $index<count($exam_subject); $index++)
		{
			?>
			<td colspan="2">
				<?php echo ucfirst(strtolower(stripslashes($exam_subject[$index]->subject_name))); ?>
			</td> <?php 
		}
		?>
	</tr>
	
	<tr class="tblheading" id="marks_type_tr">
		<?php
		for($index=0; $index<count($exam_subject); $index++)
		{
			$total_marks+=$exam_subject[$index]->subject_max_mark; ?>
			
			<td>
				<div>Written (<?php echo $exam_subject[$index]->subject_max_mark; ?>)</div>
			</td>
			<td>Result</td><?php 
		} 
		?>
	</tr>
</thead>

<tbody>

<?php

$report_data = array();
for($counter=0; $counter<count($subject_result); $counter++)
{
	$total_marks_obtain = 0;
	$candidate_overall_result = 'Pass';

	if(!isset($report_data[$subject_result[$counter]->id]) && $report_data[$subject_result[$counter]->id]=='')
	{
		$report_data[$subject_result[$counter]->id]['name'] = $_SESSION['candidate_fname'];
		
		for($counter1=0; $counter1<count($subject_result); $counter1++)
		{
			$result = 'Pass';
			if($subject_result[$counter]->id != $subject_result[$counter1]->id)
			{
				continue;
			}
			
			$report_data[$subject_result[$counter]->id][$subject_result[$counter1]->subject_id]['marks']=$subject_result[$counter1]->marks_obtained;
			$total_marks_obtain+=$subject_result[$counter1]->marks_obtained;
			
			if($subject_result[$counter1]->result=='F')
			{
				$result = 'Fail';
				$candidate_overall_result = 'Fail';
			}

			$report_data[$subject_result[$counter]->id][$subject_result[$counter1]->subject_id]['subject_result']=$result;
		}
						
		$report_data[$subject_result[$counter]->id]['candidate_result'] = $candidate_overall_result;
		$report_data[$subject_result[$counter]->id]['candidate_total_marks_obtain'] = $total_marks_obtain;
		
		$report_data[$subject_result[$counter]->id]['exam_id'] = $subject_result[$counter]->exam_id;
		$report_data[$subject_result[$counter]->id]['test_id'] = $subject_result[$counter]->test_id;
		$report_data[$subject_result[$counter]->id]['candidate_id'] = $subject_result[$counter]->id;
	}
}

$srNo=$frmdata['from'];
$totalCount = count($report_data);
$count=count($report_data);
$showGraph = false;
				
if($subject_result)
{
	$showGraph = true;
	$candidatePercentage = '';
	$totalPercentageMarks = array();
	
	foreach ($report_data as $candidate_id=>$data)
	{ 
		$srNo=$srNo+1;
		if(($srNo%2)==0)
		{
			$trClass="tdbgwhite";
		}
		else
		{
			$trClass="tdbggrey";
		} ?>
		
		<tr class='<?php echo $trClass; ?>'>
			<td><?php echo $srNo; ?></td>
			<td><?php echo ($candidateName = ucwords(strtolower(stripslashes($data['name'])))); ?></td>
							
			<?php
			for($counter = 0; $counter<count($exam_subject); $counter++) 
			{ ?>
			
				<td><?php echo $data[$exam_subject[$counter]->subject_id]['marks']; ?></td>
				<td><?php echo $data[$exam_subject[$counter]->subject_id]['subject_result']; ?></td><?php 
			}
			?>

			<td><?php echo $data['candidate_total_marks_obtain']; ?></td>
			<td><?php echo ($percentageMarks = number_format((($data['candidate_total_marks_obtain']/$total_marks)*100), 2, '.', '')); ?></td>
			<td><?php echo $data['candidate_result']; ?></td>
								
			<?php 
			$detailsUrl = CreateURL('index.php','mod=report&do=candidate_result_details');
			$detailsUrl .= "&candidate_id=".$data['candidate_id']; 
			$detailsUrl .= "&exam_id=".$data['exam_id'];
			$detailsUrl .= "&test_id=".$data['test_id'];
			$detailsUrl .= "&ins=".$_GET['ins'];
			$onClickParent = "xajax_sendTestReult('".$data['candidate_id']."', '".$data['exam_id']."', '".$data['test_id']."', 'parent');";
			$onClickTeacher = "xajax_sendTestReult('".$data['candidate_id']."', '".$data['exam_id']."', '".$data['test_id']."', 'teacher');";
			$onClickCandidate = "xajax_sendTestReult('".$data['candidate_id']."', '".$data['exam_id']."', '".$data['test_id']."', 'candidate');";
			
			$randomKey = uniqid();
			$onClick = "$('#".$randomKey."').slideDown();setTimeout('$(\'#".$randomKey."\').slideUp()', 3000);";
			$onClick = "$('#".$randomKey."').slideToggle();";
			
			?>
								
			<td>
				<a title="View Details" href='<?php echo $detailsUrl; ?>'><img src="<?php echo IMAGEURL; ?>/user_info.gif" /></a>&nbsp;
<!--				<img src="<?php echo IMAGEURL; ?>/iconSendMail.png" style="cursor: pointer;" onClick="<?php echo $onClickCandidate; ?>" title="Send Result" />
-->				<!--
				<div class="mailer-parent" id="<?php echo $randomKey; ?>">
					
					<div><a href="javascript:void(0);" onClick="<?php echo $onClickCandidate; ?>">To Student</a></div>
				</div>
				-->
			</td>
		</tr> <?php
		
		$totalPercentageMarks[] = $percentageMarks;
		$color = ($data['candidate_result'] == 'Pass') ? '00ff00' : 'ff0000';
		$candidatePercentage .= "<set label='$candidateName' value='$percentageMarks' link='$detailsUrl' color='$color' />";
	}
}
else
{
	echo "<tr><td colspan='20' align='center'>(0) Record found.</td></tr>";
}
?>

</tbody>
</table>
</div>

<?php
if($subject_result && $exportEnabled)
{ ?>	
<!--	<div align="right"><img style="cursor: pointer;" src="<?php //echo IMAGEURL ?>/file-xls.gif" alt="Export to Excel" title="Click here for export data to excel sheet" onclick="window.location='<?php //echo ROOTURL.'/index.php?mod=export&exam_id='.$exam_id.'&test_id='.$test_id; ?>'"/></div>	-->
	<?php
} 
?>


<?php 
if($showGraph) 
{ 
	$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
	$xml = "<chart caption='$test_name' xAxisName='Student' yAxisName='Marks(%)'"
			." yAxisMinValue='0' yAxisMaxValue='100' numberSuffix='%'"
			." bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270'" 
			." showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1'" 
			." html5ExportHandler='$FCExporter' exportFileName='Test_Result'>";
	
	$xml .= $candidatePercentage;
	
	$averagePercentage = array_sum($totalPercentageMarks)/count($totalPercentageMarks);
	$xml .= "<trendLines><line startValue='$averagePercentage' color='009933' displayvalue='Average' /></trendLines>";
	
	$xml .= '</chart>'; ?>
	
	<div id="candidate-percent" style="margin: 10px auto; width:880px;text-align: center;"></div>
	<script>newGraph("<?php echo $xml; ?>", 'candidate-percent', 'stacked');</script><?php 
} 
?>



<?php 
if($subject_result)
{ ?>

	<table>
	<tr>	
		<td align="left">
			<?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?>
		</td>
		
		<td align="right">
			<?php PaginationDisplay($totalCount); ?>
		</td> 	
	</tr>
	</table><?php

}
?>
	
<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />

  			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->
<script language="javascript" type="text/javascript">
<?php
{ ?>
	var count_td = $('#marks_type_tr').children('td').length;
	document.getElementById('marks_obtain_td').colSpan=count_td;
	document.getElementById('total_marks_span').innerHTML = "("+<?php echo $total_marks; ?>+")";
	<?php
}
?>
</script>
</body>
</html>