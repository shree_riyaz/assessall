<!--		Edit Profile of Candidate		-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination</title>
<style>
.user_data_header{ width:215px !important}
.user_data_image{ width:200px !important}
.user_data_info{ margin(auto,auto,auto,0px) !important}
img.ui-datepicker-trigger {border: medium none; margin-left: 5px; margin-top: 7px; position: absolute;}

input
{
	height:25px;
}
.user_data_info_row{ border-bottom:none !important}

input[type="submit"], input[type="button"]{ width:45% !important}

.user_data_image1{ position:relative;}
.user_data_image2{ position:absolute;}
/*.user_data_image2:hover{ background-color:#FFFFFF; color:#1d5770;}*/
.change_img_form
{border:1px solid #dddddd;
	margin:10px;
	width:390px;
	height:340px;
}
.change_img_msg{ width:380px; height:20px; margin:auto; border:1px solid #dddddd; margin-top:3px; text-align:center;}
.change_img_pic{ height:150px; width:130px; float:left; margin:auto;}
.change_img_pic img{ height:150px; width:130px; border:2px solid #1d5770;}
.change_img_pic p{}
.fancybox-inner{ overflow:hidden !important;}
</style>

<!----------------------- for date picker----------------------->
<!--<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/jquery-ui-timepicker-addon.js"></script>-->
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/jquery-ui-1.8.20.custom.min.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/jquery-ui-1.10.1.custom.js"></script>
<link rel='stylesheet' type='text/css' href='<?php echo ROOTURL; ?>/css/jquery-ui-1.10.1.custom.min.css' />
<!--<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/jquery-ui-timepicker-addon.js"></script>
<script src="<?php //echo ROOTURL; ?>/js/jquery-ui-1.10.1.custom.min.js"></script>-->
<script src="<?php echo ROOTURL; ?>/js/jquery.pngFix.js"></script>

<script>
	$(document).ready(function(){
	$('#red_cross').click(function(){
	$('#msg').fadeOut('slow');
	});
	$('#show_cross').click(function(){
	$('#msg2').fadeOut('slow');
	});
	})	
	
</script>
<script language="javascript" type="text/javascript">
$.noConflict();
$(document).ready(function()
{
	var date = $( ".date" ).length;
	if(date > 0)
	{
		$( ".date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "both",
			buttonImage: "<?php echo IMAGEURL;?>/cal.gif",
			buttonImageOnly: true,
			yearRange: "-40:+20",
			dateFormat: "dd-mm-yy",
			showAnim: 'slideDown'
		});
	}

	var date = $( ".datetime" ).length;
	if(date > 0)
	{
		$( ".datetime" ).datetimepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "both",
			buttonImage: "<?php echo IMAGEURL?>/cal.gif",
			buttonImageOnly: true,
			yearRange: "-40:+20",
			dateFormat: "dd/mm/yy",
			showAnim: 'slideDown',
			ampm: true,
			timeFormat: 'hh:mm:ss TT'
		});
	}

	var combo = $( ".combobox" ).length;
	if(combo > 0)
	{
		$( ".combobox" ).combobox();
	}
	
});
</script>
<!--end date picker-->

<!--------------------for fancy box-------------------------------->
<link rel="stylesheet" type="text/css" href="<?php echo ROOTURL; ?>/css/jquery.fancybox.css?v=2.1.5">
<!--<script  src="<?php echo ROOTURL; ?>/js/jquery-1.10.1.min.js"></script>-->
<script  src="<?php echo ROOTURL; ?>/js/jquery.fancybox.js"></script>

<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}
</style>
<script>
var $=jQuery.noConflict();
</script>
<script type="text/javascript">
		$(document).ready(function() {
			$('.fancyboxes').fancybox();
	});
</script>

<script type="text/javascript">
function change_image()
{
	
	document.getElementById('pWait').style.display=true;

	form1.submit();
}

function closef()
{
$.fancybox.close();
}
</script>

<!--------End fancy box------------------->

<link rel="stylesheet" href="<?php echo ROOTURL;?>/css/style.css"/>
<style>#mandatory {font-size:12px; margin-left:180px;} </style>
</head>

<body>

	<?php include_once(TEMPPATH."/includes/header.php"); ?>
<section id="main_sec" style='overflow:visible'>
<?php include_once(TEMPPATH."/includes/left_sec.php"); ?>
					<form action="" method="post" name="form1" id="form1" enctype="multipart/form-data">

		<div id="right_sec">
			<div style="margin-top:50px" class="right_cat_sec">
			<div class="user_data_wrp" style="margin:0px;">
					<div class="user_data_header">
						<div class="green_header">Account Overview</div>
					</div>

					<div  class="user_data_image">
						<div class="user_data_image1" id="user_data_image3">
						
						<?php
						$arr = parse_url($candidateData->pro_image);   				
						if($arr['path'] == '')
						{
							$path =  ROOTURL.'/css/images/no_image.gif';
						}else if(!isset($arr['host']))
						{	 
							$path = ROOTURL."/panel/uploadfiles/".$candi->pro_image;
						}else if(isset($arr['host'])){

							$path = $candi->pro_image;
						} 
						?>
						<img <?php if($editData->pro_image) {?>src="<?php echo $path; ?>" <?php } else {?> src="<?php echo $path;?>" <?php } ?> />
							<div class="user_data_image2" style=" margin-top:-45px; margin-left:10px; width:130px; height:25px; opacity:0.7; padding:0px;">
								<a  class="fancyboxes" href="#change_img" style="color:#FFFFFF;">Change Image</a>
							</div>
						</div>
						
						
					</div>
					<?php 
					if(isset($_GET['show']) && $_GET['show']==1)
					{
							echo'<div id="msg2" style="min-height: 25px; width: 60%; background: none repeat scroll 0% 0% red; position: relative; box-shadow: 0px 0px 2px rgb(0, 0, 0); margin: auto; margin-left:300px; opacity: 0.7; color: rgb(255, 255, 255); font-weight: bold; padding: 8px; text-align: center;"> Please fill all the Information to proceed<img id="show_cross" src="'.ROOTURL.'/css/images/3.png" style="position:absolute;top:5px;right:5px;height:20px;width:20px"/></div>';
					
					}
					
					if(isset($_SESSION['error']))
					{
						echo'<div id="msg" style="min-height: 25px; width: 60%; background: none repeat scroll 0% 0% red; position: relative; box-shadow: 0px 0px 2px rgb(0, 0, 0); margin: auto; margin-left:300px; opacity: 0.7; color: rgb(255, 255, 255); font-weight: bold; padding: 8px; text-align: center;">';
					echo $_SESSION['error'];
					echo '<img id="red_cross" src="'.ROOTURL.'/css/images/3.png" style="position:absolute;top:5px;right:5px;height:20px;width:20px"/></div>';
					unset($_SESSION['error']);
					}
				
					?>
					<div  class="user_data_info">
						<div id="mandatory" align="left">Note: Fields marked by ( <span style="color:#FF0000; float:none;">*</span> ) are mandatory.</div>
						
						<div class="user_data_info_row">
							<span>First Name :</span><span class="red">* </span>
							&nbsp;&nbsp;<input name="first_name" type="text" id="first_name"  size="40"
											value="<?php if(isset($frmdata['first_name'])) { echo $frmdata['first_name'];}
														else { if($_GET['show'] && $_GET['show']==1){}else { echo $editData->first_name;}}
													?>"
											onchange="isChar(this.value,this.id, 'first name');"
											maxlength="50"  /></div>
						
						
						<div class="user_data_info_row">
							<span>Last Name :</span><span class="red" style="color:#FFFFFF;">*</span>
							&nbsp;&nbsp;&nbsp;<input name="last_name" type="text" id="last_name"  size="40"
											value="<?php if(isset($frmdata['last_name'])) { echo $frmdata['last_name'];}
														else {echo $editData->last_name;}
													?>"
											onchange="isChar(this.value,this.id, 'last name');"
											maxlength="50"  /></div>
						
						
						<div class="user_data_info_row">
							<span>Birth Date :</span><span class="red">*</span>
							&nbsp;&nbsp;&nbsp;&nbsp;<input name="birth_date" type="text" id="birth_date"  size="39"
											value="<?php if(isset($editData->birth_date) && $editData->birth_date != 0000-00-00) {
													echo date('d-m-Y',strtotime($editData->birth_date)); }?>"
											maxlength="40" class="date textfield" readonly="readonly" style="opacity:0.5"/></div>
						
						
						<div class="user_data_info_row">
							<span>Mobile :</span><span class="red" style="color:#FFFFFF;">*</span>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="contact_no" type="text" id="contact_no"  size="40"
											value="<?php if(isset($frmdata['contact_no'])) { echo $frmdata['contact_no'];}
														else { echo $editData->contact_no; }?>"
											maxlength="50"  onchange="checkContactNumber(this);"  /></div>

						
						
						<div class="user_data_info_row">
							<span>Gender :</span><span class="red" style="color:#FFFFFF;">*</span>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input style="height:10px" type="radio" name="gender" id="gender" value="M" 
								<?php  if($editData->gender=='M'){?> checked="checked"<?php }?>/>Male
								<input style="height:10px" type="radio" name="gender" id="gender" value="F" 
								<?php  if($editData->gender=='F'){?> checked="checked"<?php }?>/>Female
							
						</div>
						
						<div class="user_data_info_row">
							<span>Address :</span><span class="red" style="color:#FFFFFF;">*</span>
							&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<textarea name="address" id="address" style="width:261px; margin-left:-4px;"><?php if(isset($frmdata['address'])){echo $frmdata['address']; } else { echo $editData->address; } ?></textarea>
							
						</div>
						<!--
						<div class="user_data_info_row">
							<div class="user_data_info_field"><span>Username :</span><span class="red">*</span></div>
							<div class="user_data_info_col">&nbsp;<input name="candidate_id" type="text" id="candidate_id"  size="40"
											value="<?php /*if(isset($frmdata['candidate_id'])) { echo $frmdata['candidate_id'];}
														else {echo $editData->candidate_id;}*/
													?>"
											onchange="managePageCheckIsAlphaNum(this, 'username');"
											maxlength="50"  /><img src="" id="validate_image" style="display: none;" /></div>
						</div>
						-->
						<br>
						<div class="user_data_info_row">
						<span>Email:</span><span class="red">*</span>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="email" type="text" id="email" size="40"
											value="<?php if(isset($frmdata['email'])) { echo $frmdata['email'];}
														else {echo $editData->email;}
													?>"
											onchange="CheckEmailId(this);" maxlength="100" /></div>
						
						
						<div class="user_data_info_row" style="height:40px;">
							<div class="user_data_info_field"></div>
							&nbsp;<input type="submit"  name="edit_account" class="edit_button" value="Submit" />
								<?php
								if(!($show == 1)) {
								?>
								<input type="button" name="" class="edit_button" value="Cancel"  onclick="location.href='<?php echo CreateURL('index.php',"mod=account&do=myaccount"); ?>';"/>
								<?php
								}
								?>
							
						</div>
						
					</div><!--end of user_data_info div -->
					
				</div><!--end of user_data div -->
				
			</div><!--end of content div -->
		</div><!--end of container div -->
		
		<div class="footer"></div>
	</div><!--end of main div -->

<!--Fancy box -->
<div id="change_img" style="width:410px;heigth:400px;display:none;">
<!--<a class="fancybox-item fancybox-close" title="Close" style="top:0px;"><img onclick="closef();" height="20px" width="20px" src="<?php echo IMAGEURL?>/x.png" /></a>-->
		<div class="change_img_form">
				<div class="change_img_msg"><b>Please upload jpg/jpeg/png/gif image.</b></div>
				
				<div class="change_img_pic" style="margin:10px; margin-left:120px; margin-top:30px;">
					<?php
                      	$arr = parse_url($candi->pro_image);                  
                        if(!isset($arr['host']))
                        { $path = ROOTURL."/panel/uploadfiles/".$candi->pro_image;
                        }else if(isset($arr['host'])){
                            $path = $candi->pro_image;
                        }
                       if($arr['path'] == '')
                        $path =  ROOTURL.'/css/images/no_image.gif';
                	?>
                	<img  src = <?php echo $path; ?> />
				</div>
			<div style=" margin-top:175px;margin-left:120px; width:85px;"><input type="file" name="pro_image" id="pro_image" form="form1"/></div>
			
			<div class="user_data_info_col" style="margin-left:15%; margin-top:15px;">&nbsp;<input type="submit"  form="form1" name="edit_account" class="edit_button" value="Submit" onclick="change_image()"/>
								<input type="button" name="" class="edit_button" value="Cancel"  onclick="closef();"/>
							</div>
		</div>
</div>
</form>


<!------------to show loading image,while image is loading------------->
<div id="pWait" style="background:none repeat scroll 0% 0% grey; height:100%; width:100%; z-index:1999; position:absolute; opacity:0.4; display:none">
			<div style="top:45%;position:relative;color:white">
				<center><img src="<?php echo ROOTURL ?>/css/images/loading.gif" style="height:50px;width:50px;display:block;"><br>
				<h2>Please wait</h2></center>
			</div>
</div>
<!------------End of show loading image,while image is loading------------->


</body>
</html>


