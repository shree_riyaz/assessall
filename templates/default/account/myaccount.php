
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination</title>

<link rel="stylesheet" href="<?php echo ROOTURL;?>/css/style.css"/>
<script>
	$(document).ready(function(){
	$('#red_cross').click(function(){
	$('#msg').fadeOut('slow');
	});
	
	})	
	
</script>
<style>
.user_data
{ width:99%; margin-top:50px; margin-bottom:15px; border:1px solid #1d5770;}
.user_data_wrp
{ margin:0;}
.user_data_info_row{ clear:both;}
input[type="submit"], input[type="button"]{ width:45% !important}
.user_data_image2{width: 125px;}

</style>

<!--------------------for fancy box-------------------------------->
<style>
.change_img_form
{border:1px solid #dddddd;
	margin:10px;
	width:390px;
	height:340px;
}
.change_img_msg{ width:380px; height:20px; margin:auto; border:1px solid #dddddd; margin-top:3px; text-align:center;}
.change_img_pic{ height:150px; width:130px; float:left; margin:auto;}
.change_img_pic img{ height:150px; width:130px; border:2px solid #1d5770;}
.change_img_pic p{}
.fancybox-inner{ overflow:hidden !important;}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo ROOTURL; ?>/css/jquery.fancybox.css?v=2.1.5">
<script  src="<?php echo ROOTURL; ?>/js/jquery-1.10.1.min.js"></script>
<script  src="<?php echo ROOTURL; ?>/js/jquery.fancybox.js"></script>

<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}		.user_data_info_row{ clear: both;    height: auto;    margin-top: 10px;    min-height: 40px;    padding-bottom: 1%;}
</style>
<script>
var $=jQuery.noConflict();
</script>
<script type="text/javascript">
		$(document).ready(function() {
			$('.fancyboxes').fancybox();
	});
</script>

<script type="text/javascript">
function change_image()
{
		document.getElementById('pWait').style.display=true;
	form1.submit();
}

function closef()
{
$.fancybox.close();
}
</script>

<!--------End fancy box------------------->


</head>

<body>

	
		<?php include_once(TEMPPATH."/includes/header.php"); ?>
<section id="main_sec" style='overflow:visible'>
<?php include_once(TEMPPATH."/includes/left_sec.php"); ?>
</section>
		<div id="right_sec">
			<div class="user_data">
				<div class="user_data_wrp">
					<div class='green_head_wrp'>
							<div class="green_header">Account Overview</div>
					</div>
					
					<div  class="user_data_image">
						<div class="user_data_image1">
						<?php
						$arr = parse_url($candidateData->pro_image);   				
						if($arr['path'] == '')
						{
							$path =  ROOTURL.'/css/images/no_image.gif';
						}else if(!isset($arr['host']))
						{	 
							$path = ROOTURL."/panel/uploadfiles/".$candi->pro_image;
						}else if(isset($arr['host'])){

							$path = $candi->pro_image;
						} 
						?>
						<img src="<?php echo $path; ?>" />
						</div>
						<a  class="fancyboxes" href="#change_img" style="color:#FFFFFF;" >
						<div class="user_data_image2">Change Image</div>
					    </a>
					</div>
					<?php if(isset($_SESSION['success']))
					{
						echo'<div id="msg" style="min-height: 25px; width: 60%; background: none repeat scroll 0% 0% green; position: relative; box-shadow: 0px 0px 2px rgb(0, 0, 0); margin: auto; margin-left:300px; opacity: 0.7; color: rgb(255, 255, 255); font-weight: bold; padding: 8px; padding-top:20px; text-align: center;">';
					echo $_SESSION['success'];
					echo '<img id="red_cross" src="'.ROOTURL.'/css/images/3.png" style="position:absolute;top:5px;right:5px;height:20px;width:20px"/></div>';
					unset($_SESSION['success']);
					}
				
					?>
					
					<div  class="user_data_info">
						<div class="user_data_info_row" style="margin-top:10px;">
							<span>Name:</span>
							<div class="user_data_info_col">&nbsp;
								<?php echo ucwords($candidateData->first_name);echo "&nbsp;"; echo ucwords($candidateData->last_name);?>
							</div>								<div style="width:4%;float:right"><a href="<?php echo CreateUrl('index.php', 'mod=account&do=editaccount&id='.$candidateData->id); ?>"><div class="editable_image"></div></a></div>
							
						</div>
						
						<div class="user_data_info_row" style="margin-top:10px;">
							<span>Birth Date:</span>
							<div class="user_data_info_col">&nbsp;<?php
																if($candidateData->birth_date !='' && $candidateData->birth_date != 0000-00-00 )
																		{ echo date('d-m-Y',strtotime($candidateData->birth_date)); }?>
							</div>
							<div style="width:4%;float:right"><a href="<?php echo CreateUrl('index.php', 'mod=account&do=editaccount&id='.$candidateData->id); ?>"><div class="editable_image"></div></a></div>
						</div>
						<div class="user_data_info_row">
							<span>Mobile:</span>
							<div class="user_data_info_col">&nbsp;<?php echo ucwords($candidateData->contact_no);?></div>
							<div style="width:4%;float:right"><a href="<?php echo CreateUrl('index.php', 'mod=account&do=editaccount&id='.$candidateData->id); ?>"><div class="editable_image"></div></a></div>
						</div>
						
						<div class="user_data_info_row">
							<span>Gender:</span>
							<div class="user_data_info_col">&nbsp;
								<?php echo $candidateData->gender; ?>
							</div>
							<div style="width:4%;float:right"><a href="<?php echo CreateUrl('index.php', 'mod=account&do=editaccount&id='.$candidateData->id); ?>"><div class="editable_image"></div></a></div>
						</div>
						
						<div class="user_data_info_row" style="min-height:30px; max-height:auto; border-bottom:none;">
							<span>Address:</span>
							<div class="user_data_info_col" style="width: 295px; max-height:auto; text-align:justify;">&nbsp;<?php echo ucwords($candidateData->address);?></div>
							<div style="width:4%;float:right"><a href="<?php echo CreateUrl('index.php', 'mod=account&do=editaccount&id='.$candidateData->id); ?>"><div class="editable_image"></div></a></div>
						</div>
						
						<div class="user_data_info_row" style="border-top: 1px solid #ccc;">
							<span>Username:</span>
							<div class="user_data_info_col">&nbsp;<?php echo $candidateData->candidate_id; ?></div>
							<div style="width:4%;float:right"><a href="<?php echo CreateUrl('index.php', 'mod=account&do=editaccount&id='.$candidateData->id); ?>"><div class="editable_image"></div></a></div>
						</div>
						
						<div class="user_data_info_row">
							<span>Email:</span>
							<div class="user_data_info_col">&nbsp;<?php echo $candidateData->email; ?></div>
							<div style="width:4%;float:right"><a href="<?php echo CreateUrl('index.php', 'mod=account&do=editaccount&id='.$candidateData->id); ?>"><div class="editable_image"></div></a></div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
		
	</div>

<!--Fancy box -->
<form action="" method="post" name="form1" id="form1" enctype="multipart/form-data">
<div id="change_img" style="width:410px;heigth:400px;display:none;">
		<div class="change_img_form">
				<div class="change_img_msg"><b>Please upload jpg/jpeg/png/gif image.</b></div>
				
				<div class="change_img_pic" style="margin:10px; margin-left:120px; margin-top:30px;">
					<?php
                      	$arr = parse_url($candi->pro_image);                  
                        if(!isset($arr['host']))
                        { $path = ROOTURL."/panel/uploadfiles/".$candi->pro_image;
                        }else if(isset($arr['host'])){
                            $path = $candi->pro_image;
                        }
                       if($arr['path'] == '')
                        $path =  ROOTURL.'/css/images/no_image.gif';
                	?>
                	<img  src = <?php echo $path; ?> />
				</div>
			<div style=" margin-top:175px;margin-left:120px; width:85px;"><input type="file" name="pro_image" id="pro_image" form="form1"/></div>
			
			<div class="user_data_info_col" style="margin-left:130px; margin-top:15px;">&nbsp;
						<input type="submit"  form="form1" name="edit_account" class="edit_button" value="Upload" onclick="change_image()"/>
								<input type="button" name="" class="edit_button" value="Cancel"  onclick="closef();"/>
			</div>
		</div>
</div>
</form>

<!------------to show loading image,while image is loading------------->
<div id="pWait" style="background:none repeat scroll 0% 0% grey; height:100%; width:100%; z-index:1999; position:absolute; opacity:0.4; display:none">
			<div style="top:45%;position:relative;color:white">
				<center><img src="<?php echo ROOTURL ?>/css/images/loading.gif" style="height:50px;width:50px;display:block;"><br>
				<h2>Please wait</h2></center>
			</div>
</div>
<!------------End of show loading image,while image is loading------------->


</body>
</html>
