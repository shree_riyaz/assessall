<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>Assess All</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script src="<?php echo ROOTURL; ?>/js/functions.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/jquery-1.7.2.min.js"></script>
<link rel="stylesheet" href="<?php echo ROOTURL;?>/css/exam.css" type="text/css">
<link rel="stylesheet" href="<?php echo ROOTURL;?>/css/online_exam.css" type="text/css">
<script src="<?php echo ROOTURL; ?>/js/jquery-1.7.1.js"></script>
<script src="<?php echo ROOTURL; ?>/js/jquery-ui-1.10.1.custom.min.js"></script>
<script src="<?php echo ROOTURL; ?>/js/jquery.pngFix.js"></script>
<script src="<?php echo ROOTURL; ?>/js/functions.js"></script>
<!--GoogleAnalyticscode-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52713602-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<?php	
if(isset($CFG->template))
{
	include_once(TEMP."/".$CFG->template);
}
else
{
	include_once(TEMP."/"."home.php");
}

if($CFG->template!='changepwd/change.php')
{
	include_once(TEMP."/"."footer.php");
}
?>
</body>