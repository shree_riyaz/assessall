<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<title>Online Examination</title>
	<link rel="stylesheet" href="<?php echo ROOTURL; ?>/css/user-profile.css">
	<script src="<?php echo ROOTURL; ?>/js/jquery.form.js"></script>
	
<script>
$(document).ready(
function()
{
	$( ".date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		showOn: "both",
		buttonImage: "<?php echo ADMINURL; ?>/images/cal.gif",
		buttonImageOnly: true,
		yearRange: "-40:+20",
		dateFormat: "dd/mm/yy",
		showAnim: 'slideDown'
	});
	
	$('#profile_picture').change(
	function()
	{		
		var file = this;
		var fileName = file.value;
		if(fileName == '')
		{
			return false;
		}

		var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
	
		if(!(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png" || ext == "PNG"))
		{
			alert('Please upload image in .jpg, .jpeg, .gif, .png formats only.');
			return false;
		}
		else if(file.files)
		{
			var f = file.files[0];
			if(f.size > 2000000)
			{
				alert('Please upload image not more than 2MB.');
				return false;
			}
		}

		$('#profile_picture_uploader').ajaxSubmit(
		{
			target: "#upload_here",
            clearForm: true,
			url: '<?php echo CreateUrl('index.php', 'mod=user&do=upload_image'); ?>',
			beforeSend: function() {
				$('#user-profile-picture').css('opacity','.20');
				$('#loading-div').show();
				$('.upload-link').hide();
				$('#remove-image').hide();
		    },
		    uploadProgress: function(event, position, total, percentComplete) {

		    },
			complete: function(xhr) {
		    	
			},
			success : function(result)
			{
				result = $.parseJSON(result);
				if(result.success == 1)
				{
					$('#user-profile-picture').attr('src',result.image);
					$('#uploader').html('Change Picture');
				}
				else
				{
					alert(result.message);	
				}
				
				$('#loading-div').hide();
				$('#user-profile-picture').css('opacity','1');
		    	$('.upload-link').show();
		    	$('#remove-image').show();
			}
		});
	});

	$('#remove-image').click(
	function()
	{
		if(!confirm("Do you really want to delete your profile picture?"))
		{
			return false;
		}

		var url = '<?php echo CreateUrl('index.php', 'mod=user&do=upload_image&delete_image=1'); ?>';
		$.ajax({
			url: url,
			success: function()
			{
				$('#user-profile-picture').attr('src', '<?php echo ADMINURL.'/images/blank.jpg'; ?>');
				$('#uploader').html('Upload Picture');
				$('#remove-image').hide();
			}
		});
	});

	$('#edit-personal-info').click(
	function()
	{
		$('.error').html('');
		fillPersonalInfoForm();
		$('#personal-info').hide('slow');
		$('#personal-info-form').show('slow');
		return false;
	});
	
	$('#personal-info-cancel').click(
	function()
	{
		$('#personal-info-form').hide('slow');
		$('#personal-info').show('slow');
		$('.error').html('');
		return false;
	});

	$('#personal-info-editor').submit(
	function()
	{
		$.ajax({
			url : '<?php echo CreateUrl('index.php', 'mod=user&do=edit_personal_info'); ?>',
			type  : 'POST',
			data : $('#personal-info-editor').serialize(),
			success : function(result){
				result = $.parseJSON(result);
				if(result.success == 0)
				{
					$('.error').html(result.message);
					return false;
				}
				else
				{
					var label;
					$.each(result.data,
					function(index, value){

						label = $('#'+index+'_label');
						if(label.length > 0)
						{
							value = value.replace(/\n/g,"<br>");
							label.html(value);
						}
					});
					$('#personal-info-cancel').trigger('click');
				}
			}
		});

		return false;
	});
});

function fillPersonalInfoForm()
{
	$('#personal-info-editor input,#personal-info-editor textarea').each(
	function(){
		var id = $(this).attr('id');
		var label = $('#'+id+'_label');

		if(label.length > 0)
		{
			if($(this).is('textarea'))
			{
				$(this).val(label.html().replace(/\n/g,'').replace(/(<br>)|(<br \/>)/g, "\n"));
			}
			else
			{
				$(this).val(label.html());
			}
		}
	});
}
</script>
	
</head>

<body>
<div id="outerwrapper">

<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	
<tr>
	<td><?php include_once(TEMPPATH.'/header.php');?></td>
</tr>

<tr>
	<td>
		<div id="content">
		<?php 
		// Show particular Messages
		if(isset($_SESSION['success']))
		{
			echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
			echo $_SESSION['success'];
			echo '</td></tr></tbody></table>';
			unset($_SESSION['success']);
		}
		if(isset($_SESSION['error']))
		{
			echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
			echo $_SESSION['error'];
			echo '</td></tr></tbody></table>';
			unset($_SESSION['error']);
		}
		?>

<table border="0" cellspacing="0" cellpadding="0" width="99%">
	<tr>
		<td width="80%">
			<div class="bgblue">My Profile</div>
		</td>
		<td>
			<div class="bgblue">
				<a href="index.php" style="text-decoration:none;color:#6784C9">Guideline</a>
			</div>
		</td>
		<td nowrap="nowrap">
			<div class="bgblue">
				<a href="#" style="text-decoration:none;color:#6784C9" onclick="popup('<?php echo CreateURL('index.php', 'mod=user&do=changepwd'); ?>', 'change password');">Change Password</a>
			</div>
		</td>
		<td>
			<div class="bgblue">
				<a href="index.php?mod=user&do=logout" style="text-decoration:none;color:#6784C9">Logout</a>
			</div>
		</td>
	</tr>
</table>

<div id="tbl_empdetails_wrapper" class="rounded" style="margin: 10px 0 0 0;padding: 10px;width: 500px;">					

<div id="personal-info">
	<div style="float: left;width: 350px;">
		<div class="user-name">
			<label id="first_name_label"><?php echo ucwords($user->first_name); ?></label>
			<label id="last_name_label"><?php echo ucwords($user->last_name); ?></label>
		</div>
		
		<div class="user-common">
		<table>
			<tr>
				<td><strong>Username</strong></td>
				<td><?php echo $user->candidate_id; ?></div></td>
			</tr>
			
			<?php if($user->birth_date) { ?>

			<tr>
				<td><strong>Birth Date</strong></td>
				<td><label id="birth_date_label"><?php echo date("d/m/Y" ,strtotime($user->birth_date)); ?></label></td>
			</tr>

			<?php } ?>
			
			<tr>
				<td><strong>Email</strong></td>
				<td><label id="email_label"><?php echo $user->email; ?></label></td>
			</tr>
			
			<?php if($user->contact_no) { ?>
			
			<tr>
				<td><strong>Contact No</strong></td>
				<td><label id="contact_no_label"><?php echo $user->contact_no; ?></label></td>
			</tr>

			<?php } ?>
			
			<?php if($user->address) { ?>
			
			<tr>
				<td><strong>Address</strong></td>
				<td><label id="address_label"><?php echo nl2br($user->address); ?></label></td>
			</tr>
			
			<?php } ?>
						
		</table>
		</div>
		
	</div>
	
	<div style="float: right;width: 150px;text-align: right;">
	<div id="image-container">
	
		<?php
			$img = ADMINURL.'/images/blank.jpg';
			$uploadText = 'Upload Picture';
			$removeImageStyle = "display:none;";
			
			if($user->pro_image)
			{
				$img = ADMINURL.'/uploadfiles/'.$user->pro_image;
				$uploadText = 'Change Picture';
				$removeImageStyle = '';
			}
		?>
		
		<?php /* ?>
		<div id="remove-image" style="<?php echo $removeImageStyle; ?>">
			<img src="<?php echo ROOTURL.'/images/button_icon_close.png'; ?>" title="Remove Picture" />
		</div>
		<?php */ ?>
		
		<img id="user-profile-picture" class="user-image" src="<?php echo $img; ?>" />
		
		<?php /* ?>
		<div class="upload-link">
			<label id="uploader" for="profile_picture"><?php echo $uploadText; ?></label>
		</div>
		
		<div id="loading-div">
			<img src="<?php echo ADMINURL.'/images/loading.gif'; ?>" />
		</div>
		
		<form style="display: none;" id="profile_picture_uploader" method="post" enctype="multipart/form-data">
			<input type="file" id="profile_picture" name="profile_picture" ></input>
		</form>
		<?php */ ?>
	
	</div>
	
	<?php if(false && $user->pro_sign) { ?>
	<div id="sign-container">
		<label style="">Signature</label>
		<img id="user-sing-picture" class="user-image" src="<?php echo ADMINURL.'/uploadfiles/'.$user->pro_sign; ?>" />
	</div>
	
	<?php } ?>
	
	</div>

	<div style="clear: both;text-align: right;">
		<a href="" id="edit-personal-info">Edit Info</a>
	</div>
</div>

<div id="personal-info-form" style="display: none;">
<form action="" method="post" name="personal-info-editor" id="personal-info-editor">
					
<table border="0" cellspacing="1" cellpadding="1" align="center" id="tbl_candidatedetails" style="width: auto;margin: 0;">

<tr><td>&nbsp;</td></tr>

<tr><td colspan="2" style="font-weight: bold;">Edit Personal Information</td></tr>

<tr><td colspan="2" style="color: red;padding: 10px;"><div class="error"></div></td></tr>

<tr>
	<td width="20%">
		<div align="">First Name</div>
	</td>
	<td>
		<input name="first_name" type="text" size="25" id="first_name" class="rounded textfield" value="<?php echo $user->first_name; ?>" onchange="isChar(this.value,this.id, 'first name');" maxlength="50" />&nbsp;&nbsp;
	</td>
</tr>

<tr>
	<td>
		<div align="">Last Name</div>
	</td>
	<td>
		<input name="last_name" type="text" size="25" id="last_name" class="rounded textfield" value="<?php echo $user->last_name; ?>" onchange="isChar(this.value,this.id, 'last name');" maxlength="50" />
	</td>
</tr>

<tr>
	<td>
		<div align="">Contact No</div>
	</td>
	<td>
		<input name="contact_no" type="text" size="25" id="contact_no" class="rounded textfield" value="<?php echo $user->contact_no; ?>" onchange="checkContactNumber(this);" maxlength="20" />
	</td>
</tr>

<tr>
	<td>
		<div align="">Email</div>
	</td>
	<td>
		<input name="email" type="text" size="35" id="email" class="rounded textfield" value="<?php echo $user->email; ?>" onchange="CheckEmailId(this);" maxlength="100" />
	</td>
</tr>

<tr>
	<td>
		<div>Address</div>
	</td>
	<td>
		<textarea name="address" id="address" rows="5" cols="30" class="rounded" maxlength="500" ><?php echo $user->address; ?></textarea>
	</td>
</tr>

<tr>
	<td>
		<div align="">Date of birth</div>
	</td>
	<td>
		<input type="text" id="birth_date" readonly="readonly" name="birth_date" class="rounded date textfield" value="<?php echo $user->birth_date; ?>" />
	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td colspan="6" align="center">
		<input type="submit" value="Save" class="buttons" id="personal-info-submit" />
		<input type="button" value="Cancel" class="buttons" id="personal-info-cancel" />
	</td>
</tr>

</table>
</form>
</div>

<div style="border-bottom: 1px dotted #bbb;">&nbsp;</div>

</div>
<br /><br />

		</div><!--Content div closed-->
	</td>
</tr>
</table>

<div id="upload_here" style="display: none;"></div>

</div><!--Outer wrapper closed-->
<?php include_once(TEMP."/"."footer.php"); ?>
</body>
</html>