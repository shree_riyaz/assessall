<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Assess All</title>
		<style>
		
	{
		margin: 0;
		padding: 0;
	}
	 
	
	header, footer, aside, nav, article {
		display: block;
	}
	 
	body {
			margin: 0 auto;
			
			font: 13px/22px Helvetica, Arial, sans-serif;
			background:#f1f1f1 ;
		}
	header #top_head{
	min-height:25px; background-color:#066a75; color:#fff; font:"Times New Roman", Times, serif; font-size:100%; padding:3px;
	opacity: 0.7;
    filter: alpha(opacity=40); /* For IE8 and earlier */}	
	header #headtext{
	background:#fff; letter-spacing:1px; font-size:0.5em !important; padding:5px; box-shadow:0px 0px 3px #666; padding-top:1px; /*font-family:Georgia, "Times New Roman", Times, serif;*/ width:350px; min-height:25px;color:#1d3c41; text-align:center; margin:auto
	}
	#socials img {margin-top:10px;cursor:pointer;}
	
	#login_wrp{
	min-height:430px; margin:60px auto auto; width:450px;background-color:#f7f7f7; box-shadow:0px 0px 2px #ccc; border-bottom: solid 3px #066a75}
	.login_top{ text-align:center;}
	
	.login_top h1{ font-size:1.6em; font-weight:normal; color:#066a75; padding-top:15px}
	 #hr{ width:75%; height:1px; margin:auto; background:#99bcc1; margin-bottom:15px}
	 .login_form{ width:100%;min-height:315px}
	 #form_wrp{ width:80%; margin:auto}
	.login_form input[type=text],input[type=password]{ width:100%; height:25px; margin:auto; margin-bottom:25px; border:solid 2px #99bcc1}
	.login_form input[type=submit]{ height:35px; width:40%; text-align:center; padding:5px 1; font-size:1.5em; margin-bottom:8px; font-weight:normal;  color:#fff; background-color:#066a75; border:none; box-shadow:none; cursor:pointer}
	.login_form input[type=submit]:active{background-color:#fff;color:#066a75; border: solid 1px #066a75;}
	#form_wrp span{ color:#066a75; font-weight:bold; font-size:1.2em}
	#form_wrp a {  font-size:1.1em; font-weight:bold; padding-top:10px; color:#066a75}
	.login_bottom{ width:75%; min-height:25px; padding:8px 10px 5px; background:#066a75; margin:auto; color:#fff; font-weight:bold;font-size:1.2em; text-align:center}
	.login_bottom a { color:#fff}
	@media only screen 
	and (min-device-width : 310px) 
	and (max-device-width : 359px) { 
		.login_form { min-height:245px}
		#top_head{ text-align:center}
		#login_wrp{ min-height:400px; width:250px}
		header #headtext{ width:275px; font-size:1.4em}
	}
	@media only screen 
	and (min-device-width : 321px) 
	and (max-device-width : 550px) { 
		.login_form { min-height:265px}
		#top_head{ text-align:center}
		#login_wrp{ width:300px; min-height:400px}
		header #headtext{ width:270px; font-size:1.4em}
	}
	@media only screen 
	and (min-device-width : 551px) 
	and (max-device-width : 768px) { 
	
		#login_wrp{ height:450px; width:500px}
		.header #headtext{ width:325px; font-size:1.6em}
	}
	
</style>
<script type="text/javascript">
	 document.createElement('header');
	 document.createElement('nav');
	 document.createElement('menu');
	 document.createElement('section');
	 document.createElement('article');
	 document.createElement('aside');
	 document.createElement('footer');
	</script>
	<script>
	$(document).ready(function(){
	$('#red_cross').click(function(){
	$('#msg').fadeOut('slow');
	});
	
	})	
	function fb_login() {
		location.href = '<?php echo $facebookLoginUrl; ?>';
	}
	function google_login() {
		location.href = '<?php echo $googleLoginUrl ?>';
	}
	</script>
	</head>
	<body onLoad="document.getElementById('uname').focus()">
	
		<header>
			<div id="top_head">
				<img src="<?php echo ROOTURL;?>/images/logo.png" height='30'>
			</div>
			<!--<div id="headtext">
				<h1>PLEASE CREATE AN ACCOUNT IN ORDER TO USE OUR SERVICES.</h1>
			</div>-->
		</header>
		<section id="login_wrp">
			
        	<div id="login_sec">
				<div class="login_top">
					<h1><span style="font-size:35px">L</span>OG IN</h1>
				</div>
				<div id="hr">
				
				
				
				</div>
				<?php 
				// Show particular Messages
				if(isset($_SESSION['success']))
				{
					echo'<div id="msg" style="min-height: 25px; width: 90%; background: none repeat scroll 0% 0% green; box-shadow: 0px 0px 2px rgb(0, 0, 0); position:relative; margin:-30px auto 0px auto; opacity: 0.7; color: rgb(255, 255, 255); font-weight: bold; padding: 8px; text-align: center;">';
					echo $_SESSION['success'];
					echo '<img id="red_cross" src="'.ROOTURL.'/css/images/3.png" style="position:absolute;top:5px;right:5px;height:20px;width:20px"/></div>';
					unset($_SESSION['success']);
				}
				elseif(isset($_SESSION['error']))
				{
					echo'<div id="msg" style="min-height: 25px; width: 90%; background: none repeat scroll 0% 0% red; box-shadow: 0px 0px 2px rgb(0, 0, 0); position:relative; margin:-30px auto 0px auto; opacity: 0.7; color: rgb(255, 255, 255); font-weight: bold; padding: 8px; text-align: center;">';
					echo $_SESSION['error'];
					echo '<img id="red_cross" src="'.ROOTURL.'/css/images/3.png" style="position:absolute;top:5px;right:5px;height:20px;width:20px"/></div>';
					unset($_SESSION['error']);
				}
				
			?>
				<div class="login_form">
					<div id="form_wrp">
						<form method="post">
						<label><span>Username :</span> <input id="uname" type="text" value="<?php echo $frmdata['candidate_id'];?>" name="candidate_id"></label>
						<label><span>Password :</span> <input type="password" name="password"></label>	
						<label><input type="submit" name="submit" value="LOG IN"></label>
						<br>
						<a href="<?php echo CreateURL('index.php',"mod=user&do=forgotpass"); ?>">Forgot Password ?</a>
						</form>
					</div>
					<div id='socials' style="width:250px;margin:auto;height:auto;background:">
				
				<img data-provider="google plus" onclick="google_login();" alt ='Google Plus' src="<?php echo ROOTURL;?>/css/images/fb.gif">
				<br>
				<img data-provider="facebook" onclick="fb_login();" alt='Facebook' src="<?php echo ROOTURL;?>/css/images/gp.gif"></div><br>
				</div>
				</div>
				<div > 
				</div>
				
				<?php $pg = $_GET['link'];
					$e= $_GET['e'];
					$t= $_GET['t'];
				if(isset($pg) && isset($e) && isset($t))
				{
					$link =  CreateURL('index.php',"mod=user&do=register&t=".$t."&e=".$e); 
				}
				else
				{
					$link =  CreateURL('index.php',"mod=user&do=register"); 
				}
				
				?>
				<div class="login_bottom">
					<a href="<?php echo $link; ?>">
					Sign Up</a>
				</div>
			</div>
			
    	</section>
		<a onClick="window.open('<?php echo CreateURL('index.php','mod=user&do=sample_guidelines'); ?>', 'newwin', 'height='+screen.height+'px,width='+screen.width+'px');"  />
					Sample Test</a>
	</body>

</html>
<!--
<table width="" align="center">
	<tr>
		<td align="right"><b></b></td>
	</tr>
</table>

</body>
</html>
</body>
-->