<!--
/**********************************************************************************************************
 * 		Author		:	Akshay yadav
 * 		Decsription	:	To show candidate performance graph.
 */
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Student Performance</title>

<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.Charts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionChartsExportComponent.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.jqueryplugin.js"; ?>'></script>
<script language="javascript" type="text/javascript">
function check_search()
{
	var msg = '';
	var flag=0;
	
	var candidate_id = document.getElementById('candidate_id');
	var year = document.getElementById('year');
	var path = '';
	if(candidate_id.value=='')
	{
		msg+= "Please select student no.\n";
		focusAt=candidate_id;
		focusAt.focus();
		flag = 1;
	}
	if(year.value == '')
	{
		msg+= "Please select year.\n";
		if(flag == 0)
		{
			focusAt=year;
			focusAt.focus();
		}
	}
	if(msg!='')
	{
		alert(msg);
		return false;
	}

	return true;
}

function generateGraph()
{
	var candidate_id = document.getElementById('candidate_id');
	var year = document.getElementById('year');
	var path = '';
	
	document.getElementById('chart').innerHTML = '';
	document.getElementById('chart1').innerHTML = '';
	
	path += "index.php?mod=report&do=candidate_performance&getGraph=1";
	path += "&year=" + year.value + "&month=";
	path += "&test=&candidate_id=" + candidate_id.value + "&type=monthly";
	path = escape(path);
	newGraph(path, "chart", 0);
}

function newGraph(path, div, num)
{
	var width = 900;
	if(num > 9)
	{
		width = num * 100;
	}
	var id = "c" + Math.floor(Math.random()*1000);
	FusionCharts.setCurrentRenderer('javascript');
	var myChart = new FusionCharts( "<?php echo ROOTURL; ?>/lib/FusionCharts/MSStackedColumn2D.swf", id, width, "500", "1", "1");
	myChart.setXMLUrl(path);
	myChart.render(div);
}
function showYear()
{
	var value = document.getElementById('per_by').value;
	//alert(value);
	if(value == 'year')
	{
		document.getElementById('td_year').style.display = "";
	}
	else
	{
		document.getElementById('td_year').style.display = 'none';
		document.getElementById('year').value = "";
	}
}
</script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>

<body onload="document.frmlist.candidate_id.focus();">

<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr>
	<td><div id="header">Assess All</div></td>
</tr>
<tr>
	<td>
	<div id="content">
	<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
	<div id="main">
		<div id="contents">
		<legend>Student Performance</legend>
		<?php	// Show particular Messages
		if(isset($_SESSION['error']))
		{
			echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
			echo $_SESSION['error'];
			echo '</td></tr></tbody></table>';
			unset($_SESSION['error']);
		}
		if(isset($_SESSION['success']))
		{
			echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
			echo $_SESSION['success'];
			echo '</td></tr></tbody></table>';
			unset($_SESSION['success']);
		}
		?>
		<form action="" method="post" name="frmlist" id="frmlist">
			<div class="search-div">
			<fieldset class="rounded search-fieldset">
				<legend>Search</legend>

				<br><div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>

				<table border="0" cellspacing="1" cellpadding="3">
				  	
				  	<tr>

				    	<td>
							<input type="hidden" name="candidate_id" id="candidate_id" value="<?php echo stripslashes($candidate[0]->id);?>">
				    	</td>
				    </tr>
				    <tr>
				    	<td id="td_year" align="left" >Year:<span class="red">*</span></td>
				    	<td>
							<select name="year" id="year" class="rounded">
								<option value=''>Select Year</option>
								<?php 
								$count = (date("m") < 6) ? date("Y") - 1 : date("Y");
								for($counter = 2011;$counter <= $count; $counter++)
								{
									$year_value = $counter ." - ". ($counter+1);
									$selected='';
									if($counter == $frmdata['year'])
									{
										$selected = 'selected';
									}
									echo '<option value="'. $counter .'"' .$selected. '>' .$year_value. '</option>';
								}
								?>
							</select>
				    	</td>
					</tr>
														
					<tr>
				    	<td colspan="3" align="center">
					      	<input type="submit" class="buttons rounded" name="Submit" value="Show" onclick="return check_search();"/>
					      	<input type="button" class="buttons" name="clear_search" id="clear_search" value="Clear Search" 
						      	onclick="window.location='<?php echo CreateURL('index.php','mod=report&do=candidate_performance'); ?>'"/>
						</td>
				  	</tr>
				</table>
			</fieldset>
			</div>
		</form><br />
		
<?php
if($report)
{
	$showGraph = true;
	$show_total_racords = count($report); ?>
	
	<table width="100%" align="right" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<?php print "Showing Results:".($frmdata['from']+1).'-<span id="show_total_record_span">'.($frmdata['from']+$show_total_racords)."</span> of <span id='show_total_count_span'>".$totalCount."</span>"; ?>
			</td>
			<td align="right"><?php echo getPageRecords();?></td>
		</tr>
	</table>
	<br /> <?php
} 
?> 
<br />

<?php if(isset($frmdata['candidate_id']) && $frmdata['candidate_id']) { ?>
<div id="celebs" style="overflow: auto; overflow-y: hidden;">

<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
<?php
if($report)
{ ?>
	<thead>
		<tr class="tblheading">
			<td width="1%">#</td>
			<td width="13%">Course Name</td>
			<td width="13%">Test Name</td>
			<td width="15%">Test Start Date</td>
			<td width="15%">Test End Date</td>
			<td width="11%">Result</td>
		</tr>
	</thead>

	<tbody>
	<?php
	$showGraph = true;
	$srNo=$frmdata['from'];
	$count=count($report);
	
	$testCategory = '<categories>';
	$passedCandidates = "<dataset seriesName='Passed Students'>";
	$failedCandidates = "<dataset seriesName='Failed Students'>";
	
	for($counter=0;$counter<$count;$counter++)
	{
		$srNo=$srNo+1;
		if(($counter%2)==0)
		{
			$trClass="tdbggrey";
		}
		else
		{
			$trClass="tdbgwhite";
		} ?>
			
		<tr class='<?php echo $trClass; ?>'>
			<td><?php echo $srNo; ?></td>
			<td><?php echo stripslashes($report[$counter]->exam_name); ?></td>
			<td><?php echo stripslashes($report[$counter]->test_name); ?></td>
			<td><?php echo date('d-M-Y h:i A', strtotime($report[$counter]->test_date)); ?></td>
			<td><?php echo date('d-M-Y h:i A', strtotime($report[$counter]->test_date_end)); ?></td>
			<td><?php echo ($report[$counter]->result == 'P') ? 'Pass' : 'Fail'; ?></td>
		</tr><?php
	}
	
	?>
	
	</tbody><?php
}
else
{
	echo "<tr><td colspan='7' align='center'>(0) Record found.</td></tr>";
}
?>

</table>
</div> <br></br>

<?php } ?>

		<div id="chart" align='center'></div><br></br>
		<div id="chart1" align='center' style="padding-top: 1px;overflow:auto;"></div><br></br>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->
	</div><!--Content div closed-->
	</td>
</tr>
</table>
</div><!--Outer wrapper closed-->
</body>

<?php if($showGraph) { ?>
<script>generateGraph();</script>
<?php } ?>

</html>