<?php 
$_SESSION['lang'] = $_GET['lang'];
unset($_SESSION['sdate']) ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination</title>
<link href="css/exam.css" rel="stylesheet" type="text/css"/>
<script language='javascript'  src='<?php echo ROOTURL; ?>/js/jquery-1.4.min.js' ></script> 
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shCore.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shBrushGroovy.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/styles/shCoreDefault.css"/>

<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.Charts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionChartsExportComponent.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.jqueryplugin.js"; ?>'></script>

<script src="<?php echo ROOTURL; ?>/js/preview/jquery.scrollTo-1.4.3.1-min.js"></script>

<script language="javascript" type="text/javascript">
$(document).ready(
function()
{
//	$('pre').addClass('brush: js;');
	SyntaxHighlighter.defaults.toolbar = false;
	SyntaxHighlighter.all();
	
	$(document).on('click', '.assign-marks', function() {
		$('#'+$(this).attr('id').replace('assign', '')).show();

		$(this).val('Save Marks');
		$(this).removeClass('assign-marks');
		$(this).unbind('click');
		$(this).addClass('save-marks');
		$(this).bind('click');
	});

	$(document).on('click', '.save-marks', function() {

		var marks = $('#'+$(this).attr('id').replace('assign', ''));
		var max_marks = parseFloat($('#'+$(this).attr('id').replace('assign', 'max')).val());
		var question_id = $(this).attr('id').replace('assignmarks-', '');

		marks.val($.trim(marks.val()));
		var error = false;

		if(marks.val() == '')
		{
			alert('Please enter marks to be assigned.');
			error = true;
		}
		if(!marks.val().match(/^(?:[1-9]\d*|0)?(?:\.\d+)?$/))
		{
			alert('Please enter numeric value.');
			marks.val('');
			error = true;
		}
		if(marks.val() > max_marks)
		{
			alert('You can assign maximum '+max_marks+ ' marks to this question.');
			error = true;
		}

		if(error)
		{
			marks.val('');
			marks.focus();
			return false;
		}

		xajax_assignMarksToSubjectiveTypeQuestion('<?php echo $candidate_id; ?>', '<?php echo $test_id; ?>', question_id, marks.val(), 'test', '<?php echo $testInfo->maximum_marks; ?>');
		$(this).removeClass('save-marks');
		$(this).unbind('click');
		$(this).addClass('assign-marks');
		$(this).bind('click');
		
		$('#'+$(this).attr('id').replace('assign', '')).hide();
		$('#'+$(this).attr('id').replace('assign', 'final')).html(marks.val());
		$(this).val('Update Marks');
	});
});

function newGraph(path, div)
{
	var width = 400;
	var height = 200;
	var swf = "<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf?noCache=" + new Date().getMilliseconds();	
	var id = "c" + Math.floor(Math.random()*1000);
	
	FusionCharts.setCurrentRenderer('javascript');
	var myChart = new FusionCharts( swf, id, width, height, 0, 1);
	myChart.setXMLUrl(path);
	myChart.render(div);
}

function goToQuestion(question_id)
{
	$(document).scrollTo($('#'+question_id), 'slow', 
			function()
			{
				$('#'+question_id).next().addClass('scrolled', 1000);
				$('#'+question_id).next().removeClass('scrolled', 1000);
			});

//	setTimeout($('#'+question_id).next().removeClass('scrolled', 1000), 1000);
}

function sendMsg(val,val1,val2)
{
xajax_sendTestReult(val,val1,val2,'candidate');
}
</script>
<script>
var message="";

function clickIE() 
{
	if (document.all) 
	{
		//(message);
		return false;
	}
}
function clickNS(e) 
{
	if(document.layers||(document.getElementById&&!document.all)) 
	{
		if (e.which==2||e.which==3) 
		{
			//(message);
			return false;
		}
	}
}
if (document.layers)
{
	document.captureEvents(Event.MOUSEDOWN);
	document.onmousedown=clickNS;
}
else
{
	document.onmouseup=clickNS;
	document.oncontextmenu=clickIE;
}

document.oncontextmenu=new Function("return false");

$('html').bind('keypress', function(e)
{
	//alert(e.which);
//====================for disable print (CTRL+p or CTRL+P) ====================================
   if(e.ctrlKey && (e.which == 112 || e.which == 80))
   {
		//alert("You can't use print command.");
      return false;
   }
//====================for disable copy (CTRL+c or CTRL+C) ====================================
   if(e.ctrlKey && (e.which == 99 || e.which == 67))
   {
   //alert("You can't use copy command.");
      return false;
   }
});

</script>
</head>
<body>
<div style='height:660px;overflow:scroll'>
<div id="outerwrapper">
	<div id="results_wrapper">

<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div class="bgblue"><img src="images/thumb-icon.png" />&nbsp;Congratulations. You have completed the online examination.</div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>
		<table width="237" border="0" cellpadding="4" cellspacing="3" id="tbl_resultinfo">
		  <tr>
			<td width="166"><div align="right">Total Questions:</div></td>
			<td width="94"><div align="center"><?php echo $no_questions;?></div></td>
		  </tr>
		  <tr>
			<td><div align="right">Correct Answers:</div></td>
			<td><div align="center"><?php echo $no_correctans ?></div></td>
		  </tr>
		 <tr>
			<td><div align="right">Wrong Answers:</div></td>
			<td><div align="center"><?php echo $no_wrongans ?></div></td>
		  </tr>
		  <tr>
			<td><div align="right">Skipped Questions:</div></td>
			<td><div align="center"><?php echo $no_skipped ?></div></td>
		  </tr>
		   <tr>
			<td><div align="right">Exam's Total Marks:</div></td>
			<td><div align="center"><?php echo $_SESSION['sample_total_marks']; ?></div></td>
		  </tr>
		   <tr>
			<td><div align="right">Percentage:</div></td>
			<td><div align="center"><? echo $percentage.'%'; ?></div></td>
		  </tr>
		     <tr>
			<td><div align="right">Result:</div></td>
			<td><div align="center"><? echo $show_result; ?></div></td>
		  </tr>
		</table>

	</td>
  </tr>
</table>

<div id="less" style="margin-left:10px;"><a href="javascript:void(0)" onclick="document.getElementById('less').style.display='none'; document.getElementById('more').style.display='inline';" class="buttons">Click here for detailed result</a><br><br></div>

<div id="more" style="display:none;">

<a href="javascript:void(0)" onclick="document.getElementById('less').style.display='inline'; document.getElementById('more').style.display='none';" class="buttons"  style="margin-left:10px;">Hide detailed result</a>
</br>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <?php
 // $passed_subject=2;
//$failed_subject=3;
  $questionHistory = $DB->SelectRecords('candidate_sample_question_history', "test_id=".$test_id);
 
 $test_subject_result=''; 
$passed_subject = 0;
$failed_subject = 0;
//$query = "select csqh.solve_time,csqh.is_answer_correct,sb.subject_name from candidate_sample_question_history as csqh left join question on question.id=csqh.question_id left join subject as sb on question.subject_id=sb.id where csqh.test_id=".$test_id;
//$test_subject_result=$DB->RunSelectQuery($query);
$timeTrackXml = "<chart caption='Time Track' showLegend='1' showPercentValues='0' bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' showNames='1' useEllipsesWhenOverflow='0' showBorder='1'>";
$query = "sELECT sum( csqh.solve_time )  AS solve_time ,csqh.is_answer_correct,sum(if(csqh.`is_answer_correct`='Y',1,0)) as right_ans ,sum(if(csqh.`is_answer_correct`='N',1,0)) as wrong_ans, csqh.subject_id,sb.subject_name
FROM `candidate_sample_question_history` as csqh left join subject as sb on csqh.subject_id = sb.id
WHERE test_id = ".$test_id."
GROUP BY `subject_id`";
$test_subject_results=$DB->RunSelectQuery($query);
//print_r($test_subject_results);
$passed_subject=0;
$failed_subject=0;
for($index=0; $index<count($test_subject_results); $index++)
{ 	
	$label = ucfirst(stripslashes($test_subject_results[$index]->subject_name));
 	$value = $test_subject_results[$index]->solve_time;
	$display_value = (floor($value/60)).'M '.($value%60).' S';
	$timeTrackXml .= "<set label='$label' value='$value' displayValue='$display_value' />";
	//($test_subject_results[$index]->is_answer_correct == 'Y') ? $passed_subject++ : $failed_subject++;
/*
	if($test_subject_results[$index]->is_answer_correct == 'Y') 
		$passed_subject=$passed_subject+1;
		if($test_subject_results[$index]->is_answer_correct == 'N') 
		$failed_subject=$failed_subject+1;*/
		
		$passed_subject+=$test_subject_results[$index]->right_ans;
		$failed_subject+=$test_subject_results[$index]->wrong_ans;
		
	}
	$skiped_subject = $no_questions-$failed_subject-$passed_subject;
//echo $passed_subject.'test'.$failed_subject;exit;
//for($index=0; $index<count($test_subject_result); $index++)
//{ 	
	//$label = ucfirst(stripslashes($test_subject_result[$index]->subject_name));
	//$value = $test_subject_result[$index]->solve_time;
	//$display_value = (floor($value/60)).'M '.($value%60).' S';
	//$timeTrackXml .= "<set label='$label' value='$value' displayValue='$display_value' />";
	
	//($test_subject_result[$index]->is_answer_correct == 'Y') ? $passed_subject++ : $failed_subject++;
	
?>


<div class="info-block">
	<!--<fieldset class="rounded">
	<legend><?php //echo ucfirst(stripslashes($test_subject_result[$index]['subject_name'])); ?>'s Result</legend>
	<table cellpadding="4" cellspacing="3" width="">
	<tr>
		<td width="174"><div>Subject Name</div></td>
		<td width="94"><div><? //echo ucfirst(stripslashes($test_subject_result[$index]['subject_name'])); ?></div></td>
	</tr>
	<tr>
		<td><div>Maximum Marks</div></td>
		<td><div><? //echo $test_subject_result[$index]['subject_max_marks']; ?></div></td>
	</tr>
	<tr>
		<td><div>Minimum Passing Marks</div></td>
		<td><div><? //echo $test_subject_result[$index]['subject_min_marks'].'%'; ?></div></td>
	</tr>
	<tr>
		<td><div>Percentage</div></td>
		<td><div><? //echo $test_subject_result[$index]['subject_percentage'].'%'; ?></div></td>
	</tr>
	<tr>
		<td><div>Subject Result</div></td>
		<td><div><? //echo $test_subject_result[$index]['result']; ?></div></td>
	</tr>
	</table>
	</fieldset>-->
</div> <?php 

//}


$timeTrackXml .= "</chart>";

$marksXml = "<chart caption='Question Result' showLegend='1' showPercentValues='0' bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' showNames='1' useEllipsesWhenOverflow='0' showBorder='1'>";
$marksXml .= "<set label='Right' value='$passed_subject' color='55ff55' />";
$marksXml .= "<set label='Wrong' value='$failed_subject' color='ff5555' />";
$marksXml .= "<set label='skip' value='$skiped_subject' color='f3f3f3' />";
$marksXml .= "</chart>";

?>

<div style="clear: both;"></div>
<div style="width: 100%; margin: 10px auto;">
<div style="float: left; width: 475px;" class="info-block">
	<fieldset class="rounded">
	<legend>Time Track</legend>
	<div id="time-graph" style="text-align: center;padding: 10px;"></div>
	</fieldset>
</div>

<div  class="info-block">
	<fieldset class="rounded">
	<legend>Question Result</legend>
	<div id="subject-result-graph" style="text-align: center;padding: 10px;"></div>
	</fieldset>
</div>
</div>
<?php 
//echo $test_id;

//print_r($questionHistory);
$count = 0;
$totalTime = 0;
$quetionTimeXml = "<chart caption='Time Taken Per Question' xAxisName='Question' yAxisName='Time Taken To Solve' defaultNumberScale='S' numberScaleValue='60,60' numberScaleUnit='M,H' bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' showNames='1' useEllipsesWhenOverflow='0' showBorder='1'>";
foreach($questionHistory as $key => $que)
{
	$count++;
	$totalTime += $que->solve_time;
	$quetionTimeXml .= "<set label='$count' value='$que->solve_time' link='javascript:goToQuestion(".'\"'.$que->question_id.'\"'.");' />";
}
$averageTime = $totalTime/$count; 
$quetionTimeXml .= "<trendLines><line startValue='$averageTime' color='009933' displayvalue='Average' /></trendLines>";
$quetionTimeXml .= "</chart>";
echo $quetionTimeXml;

?>
<div style="width:960px; margin:auto;">
<div class="info-block">
	<fieldset class="rounded">
	<legend>Time Track Per Question</legend>
	<div id="question-time-graph" style="text-align: center;padding: 10px;"></div>
	</fieldset>
</div>
</div>
<div style="clear: both;"></div>
<b
  <tr>
    <td><div class="bgblue">Detailed Results</div></td>
  </tr>
  <tr>
    <td>
	
	<br>
	Your selection is marked in Green (<font color="green">correct answer</font>), Red (<font color="red">wrong answer</font>) and Not attempted or Skipped (<font color="brown">Dark Red</font>).<br>
	<br>
	<?
		global $DB;
		$path = ROOTURL .'/panel/uploadfiles/';
		$ques_no=0;
		$testid=0;
		foreach($_SESSION['test_id-'.$testid] as $key=>$value)
		{	
			$ques_no=$ques_no+1;
			
			$sql="SELECT *
			FROM question
			WHERE question.is_sample = 'T'
			AND question.id =$value
			 ";
			
			$result = $DB->ExecuteQuery($sql);
			$question= $DB->FetchObject($result);
			$img="";
			if($question->question_type=='I')
			{
				$getimg = "select image_path 
							from question
		        			join question_image on question_image.question_id = question.id
							where is_sample = 'T' and
		       				question.id = '".$value."'";
			
				$getImages = $DB->RunSelectQuery($getimg);
				$total_img = count($getImages);
				
				$img = "<br><br>";
				for($count_img = 0; $count_img < $total_img; $count_img++)
				{
					$images= $getImages[$count_img];
					$final_path = $path.$images->image_path;
					if($images->image_path == '0' || $images->image_path == null)
					{
						$final_path = ROOTURL . "/panel/images/question-mark.png";
					}
						
					$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
				}
			}
			$title = $question->question_title;
				
				
				// $title =  extractString($title, '[en]', '[en:]');
			
				
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$title = " ".$title;
				$ini = strpos($title, $start);
				
				if ($ini == 0 ) 
				{
					$title = (($question->question_title));
				}
				 else
				 {
				$ini += strlen($start);
				$len = strpos($title, $end, $ini) - $ini;
				$title =  substr($title, $ini, $len);
				}
			echo "<b>Question $ques_no:</b>$title.$img<br>";
			
			$getans=" select answer.id as ans_id,
						answer.answer_title,
						question.question_title,
						question.id
				 from question
						  join answer on answer.question_id = question.id
				 where question.id =$question->id";	
			$getAnsResult = $DB->ExecuteQuery($getans);	
			
			$question_id_match=$question->id;
			$correctansDetails=$DB->SelectRecord('question_answer',"question_id=$question_id_match",'*','order by id');
			
			$correct_ans_id=$correctansDetails->answer_id;
			$correct_ans_title=$DB->SelectRecord('answer',"id=$correct_ans_id",'*','order by id');
			$color="brown";
			$numbering=1;
			echo "<table>";
			while($option= $DB->FetchObject($getAnsResult))
			{	
				if($correct_ans_title->id==$_SESSION['question-'.$question->id]['given_answer'])
				{
					if($_SESSION['question-'.$question->id]['given_answer']==$option->ans_id)
					{
						$answer = $option->answer_title;
						//$title = $question->question_title;
				
				
				// $title =  extractString($title, '[en]', '[en:]');
			
				
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$answer = " ".$answer;
				$ini = strpos($answer, $start);
				if ($ini == 0) 
				{
					$answer = $option->answer_title;
				}
				 else
				 {
				$ini += strlen($start);
				$len = strpos($answer, $end, $ini) - $ini;
				$answer =  substr($answer, $ini, $len);
				}
						if($question->question_type=='I')
							$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";
						echo '<tr><td><font color="green">'.$numbering.'. '.$answer.'</font>&nbsp;&nbsp;<img src="images/right.gif"></td></tr>';
						//echo'<table cellpadding="0" cellspacing="0"><tbody><tr><td><font color="green">'.$option->answer_title.'</font>&nbsp;&nbsp;</td><td><img src="images/yes.jpg"></td></tr></tbody></table>';
						unset($_SESSION['question-'.$question->id]['given_answer']);
					}
					else
					{
						$answer = $option->answer_title;
							//$title = $question->question_title;
				
				
				// $title =  extractString($title, '[en]', '[en:]');
			
				
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$answer = " ".$answer;
				$ini = strpos($answer, $start);
				if ($ini == 0) 
				{
						$answer = $option->answer_title;
				}
				 else
				 {
				$ini += strlen($start);
				$len = strpos($answer, $end, $ini) - $ini;
				$answer =  substr($answer, $ini, $len);
				}
						if($question->question_type=='I')
							$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";
						echo "<tr><td><font color=$color>$numbering.&nbsp;$answer</font></td></tr>";
					}
				}
				else
				{
					if($_SESSION['question-'.$question->id]['given_answer']==$option->ans_id)
					{	
						$answer = $option->answer_title;
							//$title = $question->question_title;
				
				
				// $title =  extractString($title, '[en]', '[en:]');
			
				
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$answer = " ".$answer;
				$ini = strpos($answer, $start);
				if ($ini == 0) 
				{
						$answer = $option->answer_title;
				}
				 else
				 {
				$ini += strlen($start);
				$len = strpos($answer, $end, $ini) - $ini;
				$answer =  substr($answer, $ini, $len);
				}
						if($question->question_type=='I')
							$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";
						echo '<tr><td><font color="red">'.$numbering.'. '.$answer.'</font>&nbsp;&nbsp;<img src="images/wrong.gif"></td></tr>';
						//echo '<table cellpadding="0" cellspacing="0"><tbody><tr><td><font color="red">'.$option->answer_title.'</font>&nbsp;&nbsp;</td><td><img src="images/no.jpg"></td></tr></tbody></table>';
						unset($_SESSION['question-'.$question->id]['given_answer']);
					}
					else
					{
						$answer = $option->answer_title;
							//$title = $question->question_title;
				
				
				// $title =  extractString($title, '[en]', '[en:]');
			
				
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$answer = " ".$answer;
				$ini = strpos($answer, $start);
				if ($ini == 0) 
				{
						$answer = $option->answer_title;
				}
				 else
				 {
				$ini += strlen($start);
				$len = strpos($answer, $end, $ini) - $ini;
				$answer =  substr($answer, $ini, $len);
				}
						if($question->question_type=='I')
							$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";
						echo "<tr><td><font color=$color>$numbering.&nbsp;$answer</font></td></tr>";
					}
				}				
				$numbering++;	
			}
			
			echo "</table><br/>";
			$correct_ans_id=$correctansDetails->answer_id;
			$correct_ans_title=$DB->SelectRecord('answer',"id=$correct_ans_id",'*','order by id');
			$corr_ans = $correct_ans_title->answer_title;
				//$title = $question->question_title;
				
				
				// $title =  extractString($title, '[en]', '[en:]');
			
				
				if($_SESSION['lang']=='hi')
				{
					$start = "[hi]" ;
					$end = "[hi:]" ;
				}
				else
				{
					$start = "[en]" ;
					$end = "[en:]" ;
				}
				
				
				
				$corr_ans = " ".$corr_ans;
				$ini = strpos($corr_ans, $start);
				if ($ini == 0) 
				{
					$corr_ans = $correct_ans_title->answer_title;
				}
				 else
				 {
				$ini += strlen($start);
				$len = strpos($corr_ans, $end, $ini) - $ini;
				$corr_ans =  substr($corr_ans, $ini, $len);
				}
			if($question->question_type=='I')
				$corr_ans = "<img height='50' width='50' src=". $path.$correct_ans_title->answer_title . ">";
			echo "Correct Answer:<font color=$color>".$corr_ans."</font><br/><br/><br/>";
		}

		
	
?>
	</td>
  </tr>
</table>
</div>
<script>


FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf", 'subject-result-chart', 430, 300, 0, 1);
myChart.setXMLData("<?php echo $marksXml; ?>");
myChart.render('subject-result-graph');
FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf", 'time-chart', 430, 300, 0, 1);
myChart.setXMLData("<?php echo $timeTrackXml; ?>");
myChart.render('time-graph');

FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/Line.swf", 'question-time-chart', 850, 285, 0, 1);
myChart.setXMLData("<?php echo $quetionTimeXml; ?>");
myChart.render('question-time-graph');
</script>
<?php //include_once(MOD."/feedback/index.php");?>
	</div><!--Div Results wrapper closed-->

</div><!--Outer wrapper closed-->
</div>
</body>
</html>
<?php
unset($_SESSION['is_sexam_begin']);
unset($_SESSION['sdate']);
unset($_SESSION['total_question']);
unset($_SESSION['is_valid_test']);
//unset($_SESSION['sample_total_marks']);
?>