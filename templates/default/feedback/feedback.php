<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link type="text/css" rel="stylesheet" href="<?php echo ROOTURL; ?>/css/mock_style.css" />
<script>
function checkform()
{
	var exam = document.getElementById('forexam');
	var soft = document.getElementById('forsoftware');

	if((exam.value == '') && (soft.value == ''))
	{
		alert('Please give some feedback.');
		exam.focus();
		return false;
	}
	else
	{
		return true;
	}
}
</script>
<style>
#footer{ bottom:0px;position:absolute}
</style>
</head>

<body>
<div id="">
	
		<div id="header" style="text-align:center">Examination</div>
		
	  <tr>
		<td>
			
		<div id="content" style="min-height:500px">
		<?php if(!isset($_SESSION['feed_done']) || ($_SESSION['feed_done'] != true)) { ?>
		<form action="" method="post">
	
			<div style="font-size: 20px;text-align: center; padding-top: 20px;font-weight: bold;text-decoration: underline;">
				Give Your Feedback	
			</div><br>
			<div style="font-size: 16px;text-align: center;">
				Please take your time to submit feedback, This will help us to improve the system.
			</div>
			</br>
	
			<table style="margin: 0 auto;">
				<tr>
					<td>
					<b>Give your feedback about this test</b><br>
					<textarea rows="10" cols="70" name="forexam" id="forexam" class="rounded"></textarea>
					</td>
				</tr>
				<tr><td>&nbsp;<br><br></td></tr>
				<tr>
					<td>
					<b>Give your feedback about this examination software</b><br>
					<textarea rows="10" cols="70" name="forsoftware" id="forsoftware" class="rounded"></textarea>
					</td>
				</tr>
				
				<tr>
					<td style="text-align: right;">
						<input type="submit" value="Submit Feedback" class="btnEnabled" onclick="return checkform();" />&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" value="Not Interested" class="btnEnabled" onclick="location.href='<?php echo CreateURL('index.php',"mod=feedback&close=1"); ?>'" />
					</td>
				</tr>
			</table>

		</form>
		<?php } else { ?>
		
		<br></br>
		<div style="text-align: center; font-weight: bold;font-size: 16px;color: #889988;">Thank You for Your Feedback!</div>
		<br></br>
		<div style="text-align: center;"><a href="javascript:window.close();">Click Here</a> to Close.</div>
		
		<?php } ?>
		
		<br><br>
		</div>	

		</td>
 	  </tr>	
	</table>
	<div id="footer"><div style="width:100%; text-align:center;padding-top:15px;"><center><font color="white">Examination</font></center></div></div>
<div id="clear"></div>
</div><!--Outer wrapper closed-->