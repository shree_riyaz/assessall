<?php //echo $totalmarks.'-rgfd-'.$marksObtained;
$test_duration = $_SESSION['test_end_time']-$_SESSION['test_start_time'];
//echo "=".($_SESSION['test_end_time'])." ".($_SESSION['test_start_time']+$_SESSION['time_takenfortest']*60);//exit;
$date=date('m/d/Y h:i:s A', ($_SESSION['test_end_time']+$_SESSION['time_takenfortest']*60) - $test_duration);
//$date=date('m/d/Y h:i:s A', ($_SESSION['test_start_time']+20) - $test_duration);
$current_date= date('m/d/Y h:i:s A');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination</title>
<link href="<?php echo ROOTURL;?>/css/exam.css" rel="stylesheet" type="text/css"/>
<script language='javascript'  src='<?php echo ROOTURL; ?>/js/jquery-1.4.min.js' ></script> 
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/functions.js"></script>
<script>
var message="";
///////////////////////////////////
function clickIE() 
{
	if (document.all) 
	{
		//(message);
		return false;
	}
}
function clickNS(e) 
{
	if(document.layers||(document.getElementById&&!document.all)) 
	{
		if (e.which==2||e.which==3) 
		{
			//(message);
			return false;
		}
	}
}
if (document.layers)
{
	document.captureEvents(Event.MOUSEDOWN);
	document.onmousedown=clickNS;
}
else
{
	document.onmouseup=clickNS;
	document.oncontextmenu=clickIE;
}

document.oncontextmenu=new Function("return false");

$('html').bind('keypress', function(e)
{
	//alert(e.which);
//====================for disable print (CTRL+p or CTRL+P) ====================================
   if(e.ctrlKey && (e.which == 112 || e.which == 80))
   {
		//alert("You can't use print command.");
      return false;
   }
//====================for disable copy (CTRL+c or CTRL+C) ====================================
   if(e.ctrlKey && (e.which == 99 || e.which == 67))
   {
   //alert("You can't use copy command.");
      return false;
   }
});

</script>


<style>
.match_tab
{
	width: 500px;
	padding: 20px;
	border-collapse: collapse;
	border: 1px solid #e5e5e5;
}
.match_tab td
{
	border: 1px solid #e5e5e5;
	width: 50%;
	padding:15px;
}
</style>
</head>
<body>

<div style="overflow-y: scroll; height: 685px;">
<div id="">
	<div id="results_wrapper">

<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div class="bgblue"><img src="images/thumb-icon.gif" />&nbsp;Congratulations. You have completed the online examination.</div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>
		<table width="30%" border="0" cellpadding="4" cellspacing="3" id="tbl_resultinfo">
		  <tr>
			<td width="166"><div align="right">Total Questions:</div></td>
			<td width="94"><div align="center"><?php echo $no_questions;?></div></td>
		  </tr>
		  <tr>
			<td><div align="right">Correct Answers:</div></td>
			<td><div align="center"><?php echo $no_correctans ?></div></td>
		  </tr>
		  <tr>
			<td><div align="right">Wrong Answers:</div></td>
			<td><div align="center"><?php echo $no_wrongans ?></div></td>
		  </tr>
		  <tr>
			<td><div align="right">Skipped Questions:</div></td>
			<td><div align="center"><?php echo $no_skipped ?></div></td>
		  </tr>
		  
		  <tr>
			<td><div align="right">Course's Total Marks:</div></td>
			<td><div align="center"><?php echo $_SESSION['total_marks']; ?></div></td>
		  </tr>
	<?php
	for($index=0; $index<count($test_subject_result); $index++)
	{
	?>
		<tr>
			<td colspan="2">
				<fieldset class="rounded"><legend><?php echo ucfirst(stripslashes($test_subject_result[$index]['subject_name'])); ?>'s Result</legend>
					<table cellpadding="4" cellspacing="3" width="100%">
					<tr>
						<td width="174"><div align="right">Subject Name:</div></td>
						<td width="94" nowrap="nowrap"><div align="center"><? echo ucfirst(stripslashes($test_subject_result[$index]['subject_name'])); ?></div></td>
					</tr>
					<tr>
						<td width="174"><div align="right">Percentage:</div></td>
						<td width="94"><div align="center"><? echo $test_subject_result[$index]['subject_percentage'].'%'; ?></div></td>
					</tr>
					<tr>
						<td width="174"><div align="right">Subject Result:</div></td>
						<td width="94"><div align="center"><? echo $test_subject_result[$index]['result']; ?></div></td>
					</tr>
					</table>
				</fieldset>
			</td>
	  	</tr>
	<?php 
	}
	?>
		</table>

	</td>
  </tr>
</table>
<br><br>

<div style="margin-left:10px;margin-bottom: 5px;"><a href="<?php echo CreateURL('index.php',"mod=feedback"); ?>" class="buttons">Click here to submit feedback</a><br><br></div>
<div id="less" style="margin-left:10px;"><a href="javascript:void(0)" onclick="document.getElementById('less').style.display='none'; document.getElementById('more').style.display='inline';" class="buttons">Click here for detailed result</a><br><br></div>

<div id="more" style="display:none;">

<a href="javascript:void(0)" onclick="document.getElementById('less').style.display='inline'; document.getElementById('more').style.display='none';" class="buttons"  style="margin-left:10px;">Hide detailed result</a>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div class="bgblue">Detailed Results</div></td>
  </tr>
  <tr>
    <td>
	
	<br>
	Your selection is marked in Green (<font color="green">correct answer</font>), Red (<font color="red">wrong answer</font>) and Not attempted or Skipped (<font color="brown">Dark Red</font>).<br>
	<br>
	<?
		global $DB;
		$path = ADMINURL . '/uploadfiles/';
		$sql = "select test.id,
			tq.question_id,
			question.question_title
			from test
			join test_questions as tq on tq.test_id = test.id
			JOIN question on question.id = tq.question_id where test.id=$testid";
		$result = $DB->ExecuteQuery($sql);
		$ques_no=0;
		//foreach($_SESSION['test_id-'.$testid] as $key=>$value)
		//{	
		//	echo $value;
		//}
		foreach($_SESSION['test_id-'.$testid] as $key=>$value)
		{	
			$ques_no=$ques_no+1;
			
			/*$sql = "select test.id,
			tq.question_id,
			question.question_title
			from test
			join test_questions as tq on tq.test_id = test.id
			JOIN question on question.id = tq.question_id where test.id=$testid and question.id=$value";*/
			
			$sql="select q.id as question_id, q.question_title, q.question_type from question as q
					where q.id=$value";
			
			$result = $DB->ExecuteQuery($sql);
			$question= $DB->FetchObject($result);
			
			$img="";
			if($question->question_type=='I')
			{
				$getimg = "select image_path 
							from question
		        			join question_image on question_image.question_id = question.id
							where question.id = '".$value."'";
			
				$getImages = $DB->RunSelectQuery($getimg);
				$total_img = count($getImages);
				
				$img = "<br><br>";
				for($count_img = 0; $count_img < $total_img; $count_img++)
				{
					$images= $getImages[$count_img];
					$final_path = $path.$images->image_path;
					if($images->image_path == '0' || $images->image_path == null)
					{
						$final_path = ADMINURL . "/images/question-mark.png";
					}
						
					$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
				}
			}
			
			elseif($question->question_type == 'MT')
			{
				$left_cols = $DB->SelectRecords('question_match_left',"question_id='$question->question_id'");
				$right_cols = $DB->SelectRecords('question_match_right',"question_id='$question->question_id'");
			}
			
			echo "<b>Question $ques_no:</b>$question->question_title.$img";
			
			 /*$getans="select test.id,
						tq.question_id,
						answer.id as ans_id,
						answer.answer_title,
						question.question_title
						from test
						join test_questions as tq on tq.test_id = test.id
						JOIN question on question.id = tq.question_id 
						join answer on answer.question_id=tq.question_id where test.id=$testid and tq.question_id=$question->question_id
						group by answer.id";*/
			
			$getans="select q.id as question_id, ans.id as ans_id, ans.answer_title, q.question_title 
						from question as q
						join answer as ans on ans.question_id=q.id
						where q.id=$question->question_id
						group by ans.id";
			
			if($question->question_type == 'MT')
			{
				$getans = 'select * from question where question.id ="'.$question->question_id.'"';
			}
			
			$getAnsResult = $DB->ExecuteQuery($getans);
						
			$question_id_match=$question->question_id;
			$correctansDetails=$DB->SelectRecord('question_answer',"question_id=$question_id_match",'*','order by id');
			$correct_ans_id=$correctansDetails->answer_id;
			$correct_ans_title=$DB->SelectRecord('answer',"id=$correct_ans_id",'*','order by id');
			$color="brown";
			$numbering=1;
			echo "<table>";
			while($option= $DB->FetchObject($getAnsResult))
			{	
				//echo $_SESSION['question-'.$question->question_id]['given_answer']."given";
				//echo $correct_ans_title->id."correct";
				
				if($question->question_type=='MT')
				{
					$given_answer = $_SESSION['question-'.$question->question_id]['given_answer'];
					$blank = 1;
					$correct = 1;
					
					$given = '<table class="match_tab">';
					$corrt = '<table class="match_tab">';
					
					foreach($given_answer as $l => $r)
					{
						$lcol = $DB->SelectRecord('question_match_left',"id='$l'");
						$answer_id = $lcol->answer_id;
						
						$sub_given = "<td>$lcol->value</td>
									<td>";
						
						$td_clr = '';
						if($r != '')
						{
							$blank = 0;

							$given_rcol = $DB->SelectRecord('question_match_right',"id='$r'");
							$sub_given .= $given_rcol->value;
							
							$td_clr = 'green';
							if($answer_id != $r)
							{
								$correct = 0;
								$td_clr = 'red';
							}
						}
						else
						{
							$td_clr = 'brown';
							$correct = 0;
						}
						
						$given .= "<tr style='color:$td_clr;'>$sub_given</td></tr>";
					}
					
					foreach($left_cols as $lcol)
					{
						$answer_id = $lcol->answer_id;
						
						$sub_corrt = "<td>$lcol->value</td>
									<td>";
						
						$corrt_rcol = $DB->SelectRecord('question_match_right',"id='$answer_id'");
						$sub_corrt .= $corrt_rcol->value;

						$td_clr = 'brown';
						
						$corrt .= "<tr>$sub_corrt</td></tr>";
					}
					
					$given .= "</table>";
					$corrt .= "</table>";
					
					$clr = 'red';
					if ($correct == 1)
					{
						$clr = 'green';						
					}
					
					if($blank == 1)
					{
						$clr = $color;
					}
					else
					{
						echo "<tr><td><font color='$clr'>$given</font></td></tr>";
					}
					
					unset($_SESSION['question-'.$question->question_id]['given_answer']);
				}
				else
				{
					$answer = $option->answer_title;
					if($question->question_type=='I')
						$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";
								
					if($correct_ans_title->id==$_SESSION['question-'.$question->question_id]['given_answer'])
					{
						if($_SESSION['question-'.$question->question_id]['given_answer']==$option->ans_id)
						{
							echo '<tr><td><font color="green">'.$numbering.'. '.$answer.'</font>&nbsp;&nbsp;<img src="images/right.gif"></td></tr>';
							//echo'<table cellpadding="0" cellspacing="0"><tbody><tr><td><font color="green">'.$option->answer_title.'</font>&nbsp;&nbsp;</td><td><img src="images/yes.jpg"></td></tr></tbody></table>';
							unset($_SESSION['question-'.$question->question_id]['given_answer']);
						}
						else
							echo "<tr><td><font color=$color>$numbering.&nbsp;$answer</font></td></tr>";
					}
					else if(trim($_SESSION['question-'.$question->question_id]['given_text_answer'])!='')
					{
						if($_SESSION['question-'.$question->question_id]['given_text_answer']==$option->answer_title)
						{
							$answer = trim($_SESSION['question-'.$question->question_id]['given_text_answer']);
							echo '<tr><td><font color="green">'.$answer.'</font>&nbsp;&nbsp;<img src="images/right.gif"></td></tr>';
							//echo'<table cellpadding="0" cellspacing="0"><tbody><tr><td><font color="green">'.$option->answer_title.'</font>&nbsp;&nbsp;</td><td><img src="images/yes.jpg"></td></tr></tbody></table>';
							unset($_SESSION['question-'.$question->question_id]['given_text_answer']);
						}
						else
						{
								$answer = trim($_SESSION['question-'.$question->question_id]['given_text_answer']);
							echo "<tr><td><font color=$color>rahul$answer</font></td></tr>";
							unset($_SESSION['question-'.$question->question_id]['given_text_answer']);
						}
					}
					else
					{
						if($_SESSION['question-'.$question->question_id]['given_answer']==$option->ans_id)
						{
							echo '<tr><td><font color="red">'.$numbering.'. '.$answer.'</font>&nbsp;&nbsp;<img src="images/wrong.gif"></td></tr>';
							//echo '<table cellpadding="0" cellspacing="0"><tbody><tr><td><font color="red">'.$option->answer_title.'</font>&nbsp;&nbsp;</td><td><img src="images/no.jpg"></td></tr></tbody></table>';
							unset($_SESSION['question-'.$question->question_id]['given_answer']);
						}
						else
						echo "<tr><td><font color=$color>$numbering.&nbsp;$answer</font></td></tr>";
					}				
				}
				$numbering++;	
			}
			echo "</table><br/>";
			
			if($question->question_type=='MT')
			{
					echo "Correct Answer:<font color=$color>".$corrt."</font><br/><br/><br/>";
			}
			else
			{
				$correct_ans_id=$correctansDetails->answer_id;
				$correct_ans_title=$DB->SelectRecord('answer',"id=$correct_ans_id",'*','order by id');
				$corr_ans = $correct_ans_title->answer_title;
				if($question->question_type=='I')
					$corr_ans = "<img height='50' width='50' src=". $path.$correct_ans_title->answer_title . ">";
				echo "Correct Answer:<font color=$color>".$corr_ans."</font><br/><br/><br/>";
				//$_SESSION['question-'.$question->question_id]['given_answer']."<br/><br/><br/>";
			}
		}

		
	
?>
	</td>
  </tr>
</table>
</div>


	</div><!--Div Results wrapper closed-->

</div><!--Outer wrapper closed-->
</div>
</body>
</html>