<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="css/mock_style.css" />
		<link rel="stylesheet" href="css/keyboard.css" />
		<link rel="stylesheet" href="css/number_style.css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/top.js"></script>
		<script src="js/keyboard.js" type="text/javascript"></script>
		<title>Exit Page - Assessment Examination Center</title>
		<script>
		 

		window.onresize= function(){alignHeight();};

		</script>
	</head>
	<body onload="validateClosePageURL();alignHeight();" onselectstart="return false;" ondrag="return false;">
	
	<div id="container">
		<div id="pWait" style="background:grey;height:100%;width:100%;z-index:1999;position:absolute;opacity:0.4;filter:alpha(opacity=40);">
			<div style="top:45%;position:relative;color:white">
			<center><img src="images/loading.gif" style="height:50px;width:50px;display:block;"/><br/><h2>Please wait</h2></center>
			</div>
		</div>
	
	<div id="header">
		 <table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody>
			  <tr>
				<td align="left" id="bannerImage"></td>
			  </tr>
			</tbody>
		  </table>
	</div>
			<div id="mainleft">
			<center>
				<br/>
				<br/>
				<br/>
				<font size=5>You have successfully completed the exam</font>
				<br/><br/>
				<font size=5>Please click on the below button to exit.</font>
				<br/><br/><br/><br/>
				<input class="button" style="width:75px" value="Close" type="button" onclick="window.parent.close();"/>
			</center>
				
			</div>
			<div id="mainright" style="border-left:1px #000 solid">
			</div>
			
	</div>
	<div id="footer"></div>
	</body>
</html>