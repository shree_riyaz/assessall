<?php 
 /*****************Developed by :- Richa verma
	                Date         :- 5-july-2011
					Module       :- Paper master
					Purpose      :- Template for Browse all paper details and provide functionality to edit delete and see all question
	***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Paper</title>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>

<body onload="document.frmlist.paper_title_manage.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
		<tr>
			<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
		</tr>
		<tr>
			<td>
				<div id="content">
					<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					<div id="main">
						<div id="contents">
							<form action="" method="post" name="frmlist">
									<legend>Manage Paper</legend>
									<?php
									if(isset($_SESSION['error']))
									{
										echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
										echo $_SESSION['error'];
										echo '</td></tr></tbody></table>';
										unset($_SESSION['error']);
									}
									if(isset($_SESSION['success']))
									{
										echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
										echo $_SESSION['success'];
										echo '</td></tr></tbody></table>';
										unset($_SESSION['success']);
									}
									?>

<div class=" search-div">
<fieldset class="rounded search-fieldset"><legend>Search</legend>
	<table border="0" cellspacing="1" cellpadding="3" width="100%">
		<tr>
			<td align="center">
				Paper Title:
				<input type="text" size="30" class="rounded textfield" name="paper_title_manage" value="<?php echo $frmdata['paper_title_manage']; ?>" onchange="managePageCheckIsAlphaNum(this, 'paper title');" />
			</td>
		</tr>
				
		<tr>
			<td align="center">
				<input type="submit" class="buttons rounded" name="Submit" value="Show" />
			    <input type="submit" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick=""/>
			</td>
		</tr>
	</table>
</fieldset>
</div><br />

<?php
if($paperlist)
{ ?>

<table border="0" cellspacing="1" cellpadding="4" width="100%" style="">
	<tr>
		<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($paperlist))." of ".$totalCount; ?></td>
		<td align="right"><?php echo getPageRecords();?> </td>
	</tr>
</table> <?php
	
}
?>
	
<div id="celebs">
	
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
<?php
if($paperlist[0]->id!='')
{ ?>

	<thead>
		<tr class="tblheading">
			<td width="1%">#</td>
			<td width="26%">
				<div align="center">
					<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('paper_title');">Paper Title
						<?php 
						if($frmdata['orderby']=='paper_title') 
						{
							print '<img src="'.ROOTURL.'/images/asc.gif">';
						} 
						elseif($frmdata['orderby']=='paper_title desc')
						{
							print '<img src="'.ROOTURL.'/images/desc.gif">';
						}
						?>
					</a>
				</div>
			</td>
			<td width="16%">
				<div align="center">
					<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('series');">Series
						<?php 
						if($frmdata['orderby']=='series') 
						{
							print '<img src="'.ROOTURL.'/images/asc.gif">';
						} 
						elseif($frmdata['orderby']=='series desc')
						{
							print '<img src="'.ROOTURL.'/images/desc.gif">';
						}
						?>
					</a>
				</div>
			</td>
			
			<td width="25%">Action</td>
		</tr>
	</thead>
					  
	<tbody id="tbody">
	<?php 
	$srNo=$frmdata['from'];
	$count=count($paperlist);
	
	for($counter=0;$counter<$count;$counter++) 
	{ 
		$id = $paperlist[$counter]->id;
		$title = stripslashes(highlightSubString($frmdata['paper_title_manage'], $paperlist[$counter]->paper_title));
		$srNo=$srNo+1;
		
		if(($counter%2)==0)
		{
			$trClass="tdbggrey";
		}
		else
		{
			$trClass="tdbgwhite";
		} ?>

		<tr class='<?php echo $trClass; ?>' id='row_<?php echo $id; ?>'>
			<td><?php echo $srNo; ?></td>
			<td align='center'><?php echo $title; ?></td>
			<td align='center'><?php echo $paperlist[$counter]->series; ?></td>
			<td>
				<a href="<?php echo CreateURL('index.php','mod=paper&do=edit&nameID='.$id) ?>" title='Edit'><img src="<?php echo IMAGEURL ?>/b_edit.png" /></a>&nbsp;&nbsp;

				<img src="<?php echo IMAGEURL ?>/b_drop.png" onclick="xajax_deletePermission(<?php echo $id; ?>, 'paper');" style="cursor:pointer" title="Delete">
			</td>
		</tr><?php
	}
	?>
	</tbody> <?php
}
else
{
	echo "<tr><td colspan='7' align='center'>(0) Record found.</td></tr>";
}
?>
</table>
				
</div>
				
<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
	<tr>
		<td width="10%">&nbsp;</td>
		<td width="54%">&nbsp;</td>
		<td width="36%">&nbsp;</td>
	</tr>
	<tr>
		<?php 
		if($paperlist[0]->id!='')
		{ ?>
			
			<td colspan="2">
				<?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?>
			</td><?php 
		} 
		?>
						
		<td align="right">
			<?php PaginationDisplay($totalCount); ?>
		</td>
	</tr>
</table>

			 					<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
  								<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
								<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />	
  							</form>
						</div><!--Div Contents closed-->
					</div><!--Div main closed-->
				</div><!--Content div closed-->
			</td>
	  	</tr>
	</table>
</div><!--Outer wrapper closed-->

</body>
</html>