<?php
	/*****************Developed by :- Chirayu Bansal
	 Date         :- 23-aug-2011
	 Module       :- Test master
	 Purpose      :- Template for Add particular subject question
	 ***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/jquery.tabs.css' />
<script language="javascript" type="text/javascript">

function updateTotalMarks(marksToAdd)
{
	marksToAdd = parseInt(marksToAdd);
	
	var current_marks = document.getElementById('subject_total_mark').innerHTML;
	current_marks = Number($.trim(current_marks));
	
	document.getElementById('subject_total_mark').innerHTML = marksToAdd + current_marks;
}

function findSubjectNumofQues(subject_id, is_call_marks_function,exam_id)
{
	var que_id_set="";
	var que_value_set="";
	var sid='';
	var random =  document.getElementById('subject_random_que_selection');

	if(subject_id=='')
		subject_id = document.getElementById('subject_id').value;
	
	if(subject_id =='')
	{	
		document.getElementById('subject_random_que').style.display = "none";
		return false;
	}

	if(document.getElementById('subject_custom_que_selection').checked && document.getElementById('subject_custom_que').style.display == "")
	{
		var cust_question = document.addfrm.custom_question;
		if(typeof(cust_question)!='undefined')
		{
			if(typeof(cust_question.length)=='undefined')
			{
				if(cust_question.checked)
				{
					var cust_que_value = cust_question.value.split('_');
					var que_id = cust_que_value[0];
					var que_mark = cust_que_value[1];
					que_id_set= que_id;
					que_value_set= cust_question.value+"_check";
				}
				else
				{
					que_value_set= cust_question.value+"_uncheck";
				}
			}
			else if(cust_question.length>1)
			{
				for(var counter = 0; counter<cust_question.length; counter++)
				{
					if(cust_question[counter].checked)
					{
						var cust_que_value = cust_question[counter].value.split('_');
						var que_id = cust_que_value[0];
						var que_mark = cust_que_value[1];
						que_id_set+=que_id+",";
						que_value_set+= cust_question[counter].value+"_check,";
					}
					else
					{
						que_value_set+= cust_question[counter].value+"_uncheck,";
					}
				}
				
				que_id_set = que_id_set.substr(0, que_id_set.length-1);
			}
		}
	}

	if(random.checked)
	{
		document.getElementById('subject_random_que').style.display = "";
		document.getElementById('subject_diff_que_option_tr').style.display = "";
		xajax_getquestion(que_id_set, subject_id, exam_id);
	}
	else
	{
		document.getElementById('subject_random_que').style.display = "none";
		document.getElementById('subject_message').innerHTML="";
		document.getElementById('subject_beg_level').innerHTML="";
		document.getElementById('subject_int_level').innerHTML="";
		document.getElementById('subject_high_level').innerHTML="";
		document.getElementById('show_subject_diff_question').checked=false;
		document.getElementById('subject_diff_que_option_tr').style.display = "none";

		var current_marks = Number($.trim(document.getElementById('subject_total_mark').innerHTML));
		if(!(is_call_marks_function))
			xajax_gettotalmarks('random_reset',current_marks,que_value_set,'','','',subject_id);
	}
}

function ShowSubjectCusQues(subject_id, is_call_marks_function)
{
	xajax_saveCustomQuestionSession('', '', true);
	var sid='';
	var custom =  document.getElementById('subject_custom_que_selection');
	if(subject_id!='' && custom.checked)
	{
		document.getElementById('subject_custom_que').style.display = "";
		xajax_getcustomquestion(subject_id,'',document.getElementById('exam_id').value);
	}
	else
	{
		document.getElementById('subject_custom_que').style.display = "none";
		document.getElementById('subject_showques').innerHTML = "";
		findSubjectNumofQues(subject_id,'',document.getElementById('exam_id').value);
		var current_marks = Number($.trim(document.getElementById('subject_total_mark').innerHTML));
		if(!(is_call_marks_function))
			xajax_gettotalmarks('custom_reset',current_marks,'','','','',subject_id);
	}
}

function subjectQuestionRestrict(val,name)
{
	var mainval = $.trim(val);
	val = parseInt($.trim(val));
	var max = new Array();
	var max = name.split('_');
	var level = max[0];			// question level
	var Mark = max[1]; 			//marks 
	var totalques = max[2];		// totalques

	if(mainval == '')
	{
		val = '';
	}
	
	if(isNaN(val))
	{
		alert('Please enter numeric value only.');
		document.getElementById(name).focus();
		val = '';
	}

	if(val < 0)
	{
		alert('Please enter a value greater than or equal to zero.');
		document.getElementById(name).focus();
		val = '';
	}
	
	if(val>totalques)
	{
		alert("Please enter value less than or equal to "+totalques);
		document.getElementById(name).focus();
		val = '';
	}

	document.getElementById(name).value = val;
	var current_marks = Number($.trim(document.getElementById('subject_total_mark').innerHTML));
	var subject_id = Number(document.getElementById('subject_id').value);
	xajax_gettotalmarks('add',current_marks,val,Mark,totalques,level,subject_id);
}

function showSubjectCustomTotal(val)
{
	var current_marks = Number($.trim(document.getElementById('subject_total_mark').innerHTML));
	var subject_id = Number(document.getElementById('subject_id').value);
	if(val.checked)
	{
		xajax_gettotalmarks('add',current_marks, val.value+"_check", '', '', '', subject_id);
	}
	else if(!val.checked)
	{
		xajax_gettotalmarks('add',current_marks, val.value+"_uncheck", '', '', '', subject_id);
	}
	//findSubjectNumofQues();	
}

function unselectElement(val)
{
	$('#subject_showques').find("input[value='"+val+"']").attr('checked',false);
}

function closewindow(val)
{
	var cust_que_id_set='';
	var random_que_set = '';
	var msg="";
	var flag=0;
	
	var random_que_selection = document.getElementById('subject_random_que_selection');
	var custom_que_selection = document.getElementById('subject_custom_que_selection');
	var randomTd = document.getElementById('randomTd');
	var subject_total_mark = Number($.trim(document.getElementById('subject_total_mark').innerHTML));
	var exam_subject_max_mark = document.getElementById('exam_subject_max_marks').value;
	var show_subject_diff_question = document.getElementById('show_subject_diff_question');

	if(!(random_que_selection.checked) && !(custom_que_selection.checked))
	{
		msg+= "Please select any question selection's option.\n";
		focusAt=random_que_selection;
		flag=1;
	}
	else
	{
		var subject_id = document.getElementById('subject_id').value;
		var cust_question = document.addfrm.custom_question;
		if(typeof(cust_question)!='undefined')
		{
			if(typeof(cust_question.length)=='undefined')
			{
				if(cust_question.checked)
				{
					var cust_que_value = cust_question.value.split('_');
					var que_id = cust_que_value[0];
					var que_mark = cust_que_value[1];
					cust_que_id_set= que_id;
					
				}
			}
			else if(cust_question.length>1)
			{
				for(var counter = 0; counter<cust_question.length; counter++)
				{
					if(cust_question[counter].checked)
					{
						var cust_que_value = cust_question[counter].value.split('_');
						var que_id = cust_que_value[0];
						var que_mark = cust_que_value[1];
						cust_que_id_set+=que_id+",";
					}
				}
				if(cust_que_id_set!='')
					cust_que_id_set = cust_que_id_set.substr(0, cust_que_id_set.length-1);
			}
		}

		if(random_que_selection.checked)
		{
			var input_element = $("#randomTd :input");
			for(var index=0; index<input_element.length; index++)
			{
				if(input_element[index].value!='')
					random_que_set+=input_element[index].id+"_"+input_element[index].value+",";
				//alert(input_element[index].id);
			}

			if(random_que_set!='')
				random_que_set = random_que_set.substr(0, random_que_set.length-1);
		}

		if(random_que_set=='' && cust_que_id_set=='')
		{
			msg+= "Please select questions.\n";
			if(flag==0)
				focusAt=randomTd;
			flag=2;
		}

		if(subject_total_mark!=exam_subject_max_mark)
		{
			msg+= "Subject's total marks should be equal to selected course's subject's maximum marks which is "+exam_subject_max_mark+".\n";
			if(flag==0)
				focusAt=random_que_selection;
			flag=2;
		}
	}

	if(msg=='')
	{
	    if(val=='Save')
	    {    
		    var total_subject = document.getElementById('total_subject');
		    var rowCount = Number(total_subject.value)+1;
			var mode="add";
	    }
	    else
	    {
	    	var total_subject = document.getElementById('total_subject');
		    var rowCount = Number(<?= $getVars['rowno']; ?>);
			var subject_total_mark = $.trim(document.getElementById('subject_total_mark').innerHTML);
			var mode="edit";
		}

		if(show_subject_diff_question.checked)
		{
			var subject_diff_question='Y';
		}
		else
		{
			var subject_diff_question='N';
		}
		
	    xajax_getSubjectQuestion(subject_id, cust_que_id_set, random_que_set, subject_total_mark, rowCount, mode, subject_diff_question);
	    closeQuestionTab();
	}
	else
	{
		alert(msg);
		focusAt.focus();
	}
}

function closeQuestionTab()
{
	$('#question-tab').html('');
	$('#tabs').tabs('option', 'active', 0);
    $('#tabs').tabs('disable', 1);
}

function callclosefunction(val)
{
	if(val.value == 'Cancel')
	{
		closeQuestionTab();
		return true;
	}
	
	setTimeout(function() {	closewindow(val.value);	}, 1000);
}

function searchQuestion(isClear)
{
	var ques = document.getElementById('search_question');
	var mark = document.getElementById('search_marks');
	var level = document.getElementById('search_level');

	if(isClear == 1)
	{
		ques.value = '';
		mark.value = '';
		level.value = '';
	}

	var filter = new Array();
	filter['question_title'] = ques.value;
	filter['marks'] = mark.value;
	filter['level'] = level.value;
	
	xajax_getcustomquestion(document.getElementById('subject_id').value, filter);
}
$(document).ready(
function()
{
	$("#tabs").tabs();
	$("#randomize-help").tooltip();
});
</script>

<?php

include_once(ROOT."/incajax.php");
$xajax->printJavascript();
if($_SESSION['total'])
{
	unset($_SESSION['total']);
}
if(isset($_SESSION['subject']))
{
	$_SESSION['total'] = $_SESSION['subject'];
	unset($_SESSION['subject']);
}
if(isset($_SESSION['checkedCustomQuestion']))
{
	unset($_SESSION['checkedCustomQuestion']);
}

?>
<style>
.border 
{
	border-width: 1px;
	border-style: solid;
	border-color: #CCCCCC;
	border-radius: 5px 5px 5px 5px;
}
.quesTab
{
	padding: 10px;
	background: #fff;
	border: 1px solid #ddd;
}
</style>

</head>

<body>
<div id="" style="width:820px;">
	<table border="0" cellspacing="0" cellpadding="0" width="820" id="">
		<tr>
			<td>
				<div id="">
					<div id="">
			
<form id="addfrm" name="addfrm" method="post">
	<fieldset class="rounded">
		<legend>Add Question</legend><br/>
		<?php
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
		?>
		
	<div align="center">
		<table width="99%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">
			<tr>
				<td valign="top" width="25%">
					<div align=""><b>Subject :</b></div>
				</td>
				<td>
					<div align=""><b><?php echo $subject_info->subject_name; ?></b></div>
				</td>
				<td>
					<div align="right"><b>Maximum Marks :</b></div>
				</td>
				<td>
					<div style="font-weight: bold"><?php echo $exam_subject_info->subject_max_mark; ?></div>
				</td>
			</tr>
			
			<tr>
				<td valign="top" width="20%">
					<div align="">Question Selection :<span class="red">*</span></div>
				</td>
				<td>
					<label><input id="subject_random_que_selection" name="subject_random_que_selection" type="checkbox" value="R" onclick="findSubjectNumofQues(document.getElementById('subject_id').value,'',document.getElementById('exam_id').value);" <?php if($random_que_id_set!='') echo "checked"; ?>/> Random</label> 
					<label><input type="checkbox" id="subject_custom_que_selection" name="subject_custom_que_selection" value="C" onclick="ShowSubjectCusQues(document.getElementById('subject_id').value);" <?php if($custom_que_id_set!='') echo "checked"; ?> /> Custom</label>
				</td>
				<td>
					<div align="right">Total Marks :</div>
				</td>
				<td>
					<div id="subject_total_mark">
						<?php if($subject_total_mark!='') echo $subject_total_mark; else echo 0; ?>
					</div>
						
					<input type="hidden" id="subject_maximum_marks" name="subject_maximum_marks" value="" />
				</td>
			</tr>
			
			<tr id="subject_diff_que_option_tr" style="display: none;">
				<td width="20%">
					<div align="">
						<?php $show_diff_help = "Students will find different randomly picked questions from question bank.\nTest paper may vary to different students."; ?>
						Randomize Questions :
						[<label id="randomize-help"
							style="cursor: pointer;text-decoration: underline;color: blue;font-size: 11px;"
							title="<?php echo $show_diff_help; ?>">?</label>]
					</div>
				</td>
				<td>
					<input type="checkbox" name="show_subject_diff_question" id="show_subject_diff_question" value="Y" <?php if($subject_diff_question=="Y") echo "checked"; ?> />
				</td>
			</tr>
			
			<tr>
				<td colspan="5" valign="top" id="randomTd">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%;">
						<tr>
							<td valign="top" align="left" id="subject_random_que" style="display:none;" class="quesTab">
								<table border="0" style="width: 100%;" class="">
									<tr>
										<td valign="top" colspan="3">
											<div id="subject_message"></div>
										</td>
									</tr>
									<tr>
										<td valign="top" width="33%">
											<div id="subject_beg_level"></div>
										</td>
										<td valign="top" width="34%">
											<div id="subject_int_level"></div>
										</td>
										<td valign="top" width="33%">
											<div id="subject_high_level"></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<table cellpadding="0" cellspacing="0" border="0" width="98%">
			<tr>
				<td valign="top" id="subject_custom_que" style="display:none;" class="quesTab">
					<fieldset class="rounded">
						<legend>Search Custom Questions</legend>
						<div align="center">
							<table width="80%">
								<tr>
									<td>Question:</td>
									<td><input class="rounded" type="text" id="search_question" /></td>
									<td>Marks:</td>
									<td><input class="rounded" type="text" id="search_marks" /></td>
									<td>Level:</td>
									<td>
										<select class="rounded" id="search_level">
											<option value="">Select Level</option>
											<option value="B">Beginner</option>
											<option value="I">Intermediate</option>
											<option value="H">Higher</option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="center">
										<input type="button" class="buttons" value="Search" onclick="searchQuestion();" />
										<input type="button" class="buttons" value="Clear Search" onclick="searchQuestion(1);" />
									</td>
								</tr>
							</table>
						</div>
					</fieldset>
					<div id="subject_showques" style="margin-top: 10px;"></div>
				</td>
			</tr>
		</table>

		<br />
		<div align="center">
		<?php
			if($subject_total_mark!='') 
			{ ?>
					<input type="button" name="editsub_question" id="editsub_question" value="Update" class="buttons rounded" onclick="callclosefunction(this);"/><?php
			}
			else
			{ ?>
			    	<input type="button" name="addsub_question" id="addsub_question" value="Save" class="buttons rounded" onclick="callclosefunction(this);"/><?php
			} 
            ?>
	            	<input type="button" name="cancel" value="Cancel" class="buttons" onclick="callclosefunction(this);"/>
		</div><br />
	</div>
			
	</fieldset>
	<br/>
				
	<input type="hidden" name="exam_subject_max_marks" id="exam_subject_max_marks" value="<?php echo $exam_subject_info->subject_max_mark; ?>" />
	<input type="hidden" name="subject_id" id="subject_id" value="<?php echo $subject_id; ?>" />
    	<input type="hidden" name="exam_id" id="exam_id" value="<?php echo $exam_id; ?>" />
	
</form>
	  
					</div><!--Div Contents closed-->
				</div><!--Div main closed-->
			</td>
		</tr>
	</table>	
</div><!--Outer wrapper closed-->

<?php
if($subject_total_mark!='') 
{ ?>
	<script language="javascript" type="text/javascript">
		ShowSubjectCusQues('<?php echo $subject_id; ?>', 'no');
		setTimeout("findSubjectNumofQues('<?php echo $subject_id; ?>', 'no','<?php echo $exam_id; ?>')", 100);
	</script> <?php
}
?>
</body>
</html>