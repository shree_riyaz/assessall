<?php 
/**
 *	@author : Ashwini Agarwal
 *	@desc	: Common form to add/edit paper.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Online Examination - <?php echo $isEdit ? 'Edit' : 'Add'; ?> Paper</title>
	<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/jquery.tabs.css' />
	
<script language="javascript" type="text/javascript">
$(function() 
{ 
	$( "#tabs" ).tabs();
	$( "#tabs" ).tabs('disable', 1);
});
function numbering()
{
	var count = 1;
	$('#tbl_body tr').each(function() {
	    $(this).find("td:first-child").html(count++);    
	 });
}

function getmaxmarks()
{
	var total=Number(document.getElementById('total_mark').innerHTML);
	document.getElementById('maximum_marks').value=total;

	document.getElementById('submit<?php echo $isEdit ? 'Edit' : 'Add'; ?>_form').value=1;
	document.addpaper.submit();
}

function subjectquestionform(val)
{
	var msg="";
	var flag=0;
	var total_subject = document.getElementById("total_subject");
	
	if(val.value!='')
	{
		for(var i=0; i<total_subject.value; i++)
		{
			if(document.getElementById('subject_id_'+(i+1))==null)
			{
				continue;
			}
				
			if(val.value==document.getElementById('subject_id_'+(i+1)).value)
			{
				msg+= "This subject is already selected. Please try another subject.\n";
				focusAt=val;
				flag=1;
			}
		}

		if(msg=='')
		{
			$('#question-tab').html('<br><div align="center"><img src="<?php echo IMAGEURL.'/loading.gif'; ?>" /></div><br>');
			var url = "index.php?mod=paper&do=addsubjectquestion&subject_id="+val.value;
			$('#question-tab').load(url);
			$( "#tabs" ).tabs('enable', 1);
			$('#tabs').tabs('option', 'active', 1); 
		}
		else
		{
			alert(msg);
			focusAt.focus();
		}
	}
}

function addRow()
{
	var subject_name = document.getElementById("subject_name");
	var subject_id = document.getElementById("select_subject_id");
	var subject_total_question = document.getElementById("subject_total_question");
	var subject_paper_mark = document.getElementById("subject_paper_mark");
	var total_subject = document.getElementById("total_subject");
	var subject_diff_question=document.getElementById("subject_diff_question");
	
	var table = document.getElementById('tbl_body');
    var rowCount = table.rows.length;
	if(total_subject.value==0)
	{
		document.getElementById('tbl_reports').style.display='';
	}
    var row = table.insertRow(rowCount);
	var row_no = Number(total_subject.value)+1;

	if((row_no%2)==0)
		row.className="tdbgwhite";
	else
		row.className="tdbggrey";

//=======SrNo td======================	
    var cell1 = row.insertCell(0);
    cell1.innerHTML = row_no;

//=======Subject name=================
    var cell2 = row.insertCell(1);

    cell2.align = "center";
    cell2.id = "subject_nameTd_"+row_no;
    cell2.innerHTML=subject_name.value;

//=======Subject Total Question=================
    var cell3 = row.insertCell(2);

    cell3.align = "center";
    cell3.id = "subject_questionTd_"+row_no;
    cell3.innerHTML=subject_total_question.value;

//=======Subject Total Marks=================
    var cell4 = row.insertCell(3);

    cell4.align = "center";
    cell4.id = "subject_markTd_"+row_no;
    cell4.innerHTML=subject_paper_mark.value;

//=======Subject Show Different Question=================
    var cell5 = row.insertCell(4);

    cell5.align = "center";
    cell5.id = "subject_diff_questionTd_"+row_no;
    cell5.innerHTML=subject_diff_question.value;

//==========Option for Edit and Delete==============
    var cell6 = row.insertCell(5);
   
    cell6.align = "center";
    cell6.innerHTML='<input type="button" class="buttons rounded" name="edit_'+row_no+'" id="edit_'+row_no+'" value="Edit" onclick="editRow(this);" /> <img src="<?php echo IMAGEURL; ?>/b_drop.png" style="cursor: pointer;" id="delete_img_'+row_no+'" title="Remove" onclick="deleteRow(this);" /> ';
    
    subject_id.value='';
    subject_name.value='';
    subject_total_question.value='';
    subject_paper_mark.value='';
    subject_diff_question.value='';

    var subject_mark_td_value = document.getElementById("subject_markTd_"+row_no).innerHTML;
	var subject_marks = Number(subject_mark_td_value.substr(0,subject_mark_td_value.indexOf("<")-1));
	var total_mark =  Number(document.getElementById("total_mark").innerHTML);
	
	document.getElementById("total_mark").innerHTML=total_mark+subject_marks;
	
    total_subject.value=row_no;
    $('#sub_tbl').show();
    numbering();
}

function deleteRow(r)
{
	var confirmation_flag = confirm("Do you really want to remove this subject?");
	if(confirmation_flag)
	{
		var rowno = r.id.substr(r.id.lastIndexOf("_")+1);
		//alert(rowno);
		var total_mark =  Number(document.getElementById("total_mark").innerHTML);
		var subject_marks = document.getElementById("subject_total_mark_"+rowno).value;
		
		document.getElementById("total_mark").innerHTML=total_mark-subject_marks;
		
		var row_index = r.parentNode.parentNode.rowIndex-1;
		document.getElementById('tbl_body').deleteRow(row_index);

		if($('#tbl_reports tr').length <= 1)
		{
			$('#sub_tbl').hide();
		}
		numbering();
	}
	else
	{
		return false;
	}
}

function editRow(val,row_no)
{
	if(val=='update')
	{
		var subject_name = document.getElementById("subject_name");
		var subject_total_question = document.getElementById("subject_total_question");
		var subject_paper_mark = document.getElementById("subject_paper_mark");
		var subject_diff_question = document.getElementById("subject_diff_question");

		document.getElementById("subject_nameTd_"+row_no).innerHTML = subject_name.value;
		document.getElementById("subject_questionTd_"+row_no).innerHTML = subject_total_question.value;
		document.getElementById("subject_markTd_"+row_no).innerHTML = subject_paper_mark.value;
		document.getElementById("subject_diff_questionTd_"+row_no).innerHTML = subject_diff_question.value;

		subject_name.value = '';
		subject_total_question.value = '';
		subject_paper_mark.value = '';
		subject_diff_question.value='';
		document.getElementById('total_mark').innerHTML=0;
		showTotalMarks();
		
	}
	else
	{
		var row_no = val.id.substr(val.id.lastIndexOf('_')+1);
		var subject_id = document.getElementById('subject_id_'+row_no).value;
		var random_que_id_set = document.getElementById('random_que_id_'+row_no).value;
		var custom_que_id_set = document.getElementById('custom_que_id_'+row_no).value;
		var subject_total_mark = document.getElementById('subject_total_mark_'+row_no).value;
		var subject_diff_question = document.getElementById('subject_diff_question_'+row_no).value;
		
		$('#question-tab').html('<br><div align="center"><img src="<?php echo IMAGEURL.'/loading.gif'; ?>" /></div><br>');
		var url = "index.php?mod=paper&do=addsubjectquestion&rowno="+row_no+"&subject_id="+subject_id+"&random_que_id_set="+random_que_id_set+"&custom_que_id_set="+custom_que_id_set+"&subject_total_mark="+subject_total_mark;
		$('#question-tab').load(url);
		$( "#tabs" ).tabs('enable', 1);
		$('#tabs').tabs('option', 'active', 1);   
	}
}

function showTotalMarks()
{
	var total_subject = document.getElementById('total_subject');
	var total_mark = Number(document.getElementById('total_mark').innerHTML);
	
	for(var i=0; i<total_subject.value; i++)
	{
		if(document.getElementById('subject_total_mark_'+(i+1))==null)
		{
			continue;
		}
		else
		{
			var subject_marks = Number(document.getElementById('subject_total_mark_'+(i+1)).value);
			total_mark = subject_marks+total_mark;
		}
	}
	document.getElementById('total_mark').innerHTML = total_mark;
}
</script>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();

if($_SESSION['total'])
{
	unset($_SESSION['total']);
}

if($_SESSION['custom_total_marks'])
{
	unset($_SESSION['custom_total_marks']);
	unset($_SESSION['question']);
}

if(isset($_SESSION['random_total_value']))
{
	unset($_SESSION['random_total_value']);
}

if(isset($_SESSION['custom_question_id_set']))
{
	unset($_SESSION['custom_question_id_set']);
}

?>
</head>

<body onload="paper_title.focus();">
<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	<tr>
		<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
	</tr>
	<tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				<div id="main">
					<div id="contents">
						<fieldset class="rounded">
							<legend><?php echo $isEdit ? 'Edit' : 'Create'; ?> Paper</legend><br />
							<?php
								if(isset($_SESSION['error']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
									echo $_SESSION['error'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['error']);
								}
								if(isset($_SESSION['success']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
									echo $_SESSION['success'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['success']);
								}
							?>

<div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div><br>
<form action="" method="post" name="addpaper" id="addpaper">

<div id="tabs">
    <ul>
        <li><a href="#paper-info-tab">Paper Information</a></li>
        <li><a href="#question-tab">Questions</a></li>
    </ul>
    
    <div id="paper-info-tab" class="exam-tabs">
    
<table border="0" cellspacing="0" cellpadding="4" id="tbl_preferences" width="" style="margin-left: 30px;">
	<tr>
		<td width="40%">
			<div>Paper Title:<span class="red">*</span></div>
		</td>
		<td width="">
			<input name="paper_title" id="paper_title" type="text" class="rounded textfield" 
					value="<?php echo $frmdata['paper_title']; ?>" size="35" 
					onchange="IsAlphaNum(this, 'paper title');" maxlength="25" />
		</td>
	</tr>
	<tr>
		<td>
			<div>Paper Series:</div>
		</td>
		<td>
			<input name="series" id="series" type="text" class="rounded" 
					value="<?php echo $frmdata['series'] ?>" size="10" maxlength="10" />
		</td>
	</tr>
	<tr>
		<td valign="top">
			<div>Description:</div>
		</td>
		<td>
			<textarea name="description" id="description" class="rounded" rows="5" cols="50"><?php echo $frmdata['description']; ?></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<div>Subject:<span class="red">*</span></div>
		</td>
		<td>
			<select name="select_subject_id" id="select_subject_id" class="rounded" onchange="subjectquestionform(this);">
				<option value="">Select Subject</option>
				<?php 
				if(is_array($subject))
				{
					for($counter=0;$counter<count($subject);$counter++)
					{
						echo '<option value="'.$subject[$counter]->id.'">'.stripslashes($subject[$counter]->subject_name).'</option>';
					}
				}
				?>
			</select>
		</td>
	</tr>
</table>

<div style="float: right;margin-right: 20px;display: none;">
	<div style="float: left;">Total Marks:&nbsp;</div>

	<div id="total_mark" style="font-weight: bold;float: right;">
	<?php 
		if($frmdata['maximum_marks'] > 0) echo $frmdata['maximum_marks'];
		else echo 0;
	?>
	</div>

	<input type="hidden" id="maximum_marks" name="maximum_marks" value="" />
</div>
<div style="clear: both;"></div>

<?php
	$total_subject = 0;
	if($frmdata['total_subject'] > 0)
	{
		$total_subject = $frmdata['total_subject'];
	}
	elseif ($paper_subject)
	{
		$total_subject = count($paper_subject);
	}
	
	$tableStyle = 'display:none;';
	if(($total_subject > 0))
	{
		$tableStyle = '';
	}
?>
<br>
<div id='sub_tbl' align="center">
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="98%" bgcolor="#e1e1e1" class="data" style="<?php echo $tableStyle; ?>">
<thead>
  	<tr class="tblheading">
		<td width="1%">#</td>
		<td width="25%">Subject</td>
		<td width="11%">Total Questions</td>
		<td width="11%">Total Marks</td>
		<td width="11%">Show Diff. Question</td>
		<td width="17%">Action</td>
	</tr>
</thead>

<tbody id="tbl_body">
<?php
	if($total_subject > 0)
	{
		$count = 1;
		for($counter=0; $counter < $total_subject; $counter++)
		{
			if(($_POST) && ($frmdata['subject_id_'.($counter+1)] == ''))
			{
				continue;
			}
			else 
			{
				if(($count%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				} ?>
				
				<tr class="<?php echo $trClass; ?>">
					<td><?php echo $count++; ?></td>
					<td id="subject_nameTd_<?php echo $counter+1; ?>">
						<?php 
						for($counter1=0;$counter1 < count($subject);$counter1++)
						{
							if($frmdata['subject_id_'.($counter+1)])
							{
								$selected_value = $frmdata['subject_id_'.($counter+1)];
							}
							else if(isset($paper_subject))
							{
								$selected_value = $paper_subject[$counter]->subject_id;
							}
							
							$selected='';
							if ($subject[$counter1]->id == $selected_value)
							{
								echo stripcslashes($subject[$counter1]->subject_name);
								echo '<input type="hidden" name="subject_id_'.($counter+1).'" id="subject_id_'.($counter+1).'" value="'.$subject[$counter1]->id.'">';
								break;
							}
						}
						?>
					</td>
					<td id="subject_questionTd_<?php echo $counter+1; ?>">
						<?php
						$total_question = '';
						$random_que_id = '';
						$custom_que_id = '';
						if ($frmdata['total_question_'.($counter+1)])
						{	
							$total_question = $frmdata['total_question_'.($counter+1)];
							$random_que_id = $frmdata['random_que_id_'.($counter+1)];
							$custom_que_id = $frmdata['custom_que_id_'.($counter+1)];
						}
						elseif (isset($subject_total_question))
						{
							$total_question = $subject_total_question[$paper_subject[$counter]->subject_id];
							$random_que_id = substr($subject_random_question_set[$paper_subject[$counter]->subject_id],0,strlen($subject_random_question_set[$paper_subject[$counter]->subject_id])-1);
							$custom_que_id = substr($subject_custom_question_set[$paper_subject[$counter]->subject_id],0,strlen($subject_custom_question_set[$paper_subject[$counter]->subject_id])-1);
						}
							
						echo $total_question;
						echo '<input type="hidden" name="total_question_'.($counter+1).'" id="total_question_'.($counter+1).'" value="'.$total_question.'">';
						echo '<input type="hidden" name="random_que_id_'.($counter+1).'" id="random_que_id_'.($counter+1).'" value="'.$random_que_id.'">';
						echo '<input type="hidden" name="custom_que_id_'.($counter+1).'" id="custom_que_id_'.($counter+1).'" value="'.$custom_que_id.'">';
						?>
					</td>
					<td id="subject_markTd_<?php echo $counter+1; ?>">
						<?php 
						if ($frmdata['subject_total_mark_'.($counter+1)])
							$total_marks = $frmdata['subject_total_mark_'.($counter+1)];
						elseif (isset($paper_subject))
							$total_marks = $paper_subject[$counter]->subject_total_marks;
							
						echo $total_marks;
						echo '<input type="hidden" name="subject_total_mark_'.($counter+1).'" id="subject_total_mark_'.($counter+1).'" value="'.$total_marks.'">';
						?>
					</td>
					<td id="subject_diff_questionTd_<?php echo $counter+1; ?>">
						<?php
						if ($frmdata['subject_diff_question_'.($counter+1)] == 'Y')
							$showDiff = 'Y';
							
						elseif (isset($paper_subject) && ($paper_subject[$counter]->show_subject_diff_question=='Y'))
							$showDiff = 'Y';
								 	
						else
							$showDiff = 'N';
							
						echo ($showDiff == 'Y') ? 'Yes' : 'No';
						echo '<input type="hidden" name="subject_diff_question_'.($counter+1).'" id="subject_diff_question_'.($counter+1).'" value="'.$showDiff.'">';
						?>
					</td>
					<td align="center">
						<input type="button" class="buttons rounded" onclick="editRow(this);" value="Edit" id="edit_<?php echo $counter+1; ?>" name="edit_<?php echo $counter+1; ?>" /> 
						<img onclick="deleteRow(this);" id="delete_img_<?php echo $counter+1; ?>" title="Remove" style="cursor: pointer;" src="<?php echo IMAGEURL; ?>/b_drop.png" /> 
					</td>
				</tr><?php 
			}
		}
	} 
?>
</tbody>
</table>
</div><br>

<?php if($isEdit) { ?>
<input type="hidden" name="submitEdit_form" id="submitEdit_form" value="" />
<?php } else { ?>
<input type="hidden" name="submitAdd_form" id="submitAdd_form" value="" />
<?php } ?>

<input type="hidden" name="subject_name" id="subject_name" value="" />
<input type="hidden" name="subject_total_question" id="subject_total_question" value="" />
<input type="hidden" name="subject_paper_mark" id="subject_paper_mark" value="" />
<input type="hidden" name="subject_diff_question" id="subject_diff_question" value="" />
<input type="hidden" name="total_subject" id="total_subject" value="<?php echo $total_subject; ?>" />
<input type="hidden" name="stream_id" id="stream_id" value="<?php echo $frmdata['stream_id']; ?>" />

	</div>

	<div id="question-tab" class="exam-tabs"></div>
</div>

<br />
<div align="center">
	<input type="button" name="<?php echo $isEdit ? 'edit' : 'add'; ?>newpaper" value="Save" class="buttons" onclick="getmaxmarks();" /> 
	<input type="button" name="cancel" value="Cancel" class="buttons" onclick="location.href='<?php print CreateURL('index.php',"mod=paper");?>'" />
</div>
<br />

</form>

						</fieldset>
					</div>
				</div>
			</div>
		</td>
	</tr>
</table>
</div>
</body>
</html>