<?php
defined("ACCESS") or die("Access Restricted");

$mod = '';
if(isset($_GET['mod']))
{
	$mod= $_GET['mod'];
}
$do = '';
if(isset($_GET['do']))
{
	$do = $_GET['do'];
}

$admin_info = $DB->SelectRecord('admin_users','user_name = "admin"');
$isAdmin = ($_SESSION['admin_user_name'] == 'admin');

$module = $mod;
if ($mod=='test_master' || $mod=='examination' || $mod=='paper' || $mod=='homework')
	$module = 'exam';
if ($mod=='stream_master' || $mod=='subject_master')
	$module = 'stream';
if ($mod == 'role' || $mod == 'assignrole' || $mod == 'users')
	$module = 'role';

if($mod == 'homework_history')
	$module = 'report';
if ($mod == 'package')
    $module = 'package';

if(in_array($mod, array('backup', 'mailer', 'feedback')))
	$module = 'tools';

$current[$module] = "current";

$do = $do ? $do : 'manage';
$do = ($do != 'edit') ? $do : 'manage';
$current_sub[$mod][$do] = 'current_sub';
?>
<?php
	// 4px solid #F07700;
?>
<div id="main-menu">
<ul id="dropline">
	<?php
	 //Call the isModuleAccessible($module,$user_id) and decide whether to show the module in navigation or not
	 if ($isAdmin || $auth->isModuleAccessible('dashboard',$user_id)):
	 ?>
		<li class="<?php echo $current['dashboard']; ?>"><a href="<?php print CreateURL('index.php','mod=dashboard&do=showinfo&master_nav=1'); ?>"><b>Dashboard</b></a>
			<?php if($current['dashboard']) { ?>
			<ul id="secondary">
				<li class="<?php echo $current_sub['dashboard']['showinfo']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=dashboard&do=showinfo'); ?>">Dashboard</a>
				</li>

				<li style="float:right;">
					<a class="last">Welcome, <?php echo (($_SESSION['admin_user_name'])); ?></a>
				</li>
			</ul>
			<?php } ?>
		</li>
	<?php

	endif;
	if ($isAdmin || $auth->isModuleAccessible('examination',$user_id) ||  $auth->isModuleAccessible('test_master',$user_id) || $auth->isModuleAccessible('paper',$user_id) || $auth->isModuleAccessible('homework',$user_id)):

	?>

		<li class="<?php echo $current['exam']; ?>">
        <a href="<?php print CreateURL('index.php','mod=examination&do=manage&master_nav=1'); ?>"><b>Exams</b></a>
        <ul id="secondary">

			<?php if($isAdmin || $auth->isModuleAccessible('examination',$user_id)) { ?>
				<li class="<?php echo $current_sub['examination']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=examination&do=manage');?>">Manage Exams</a>
				</li>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'examination', 'add')): ?>
				<li class="<?php echo $current_sub['examination']['add']; ?>">
					<a href="<?php print CreateURL('index.php','mod=examination&do=add');?>">Add Exam</a>
				</li>
				<?php endif; ?>

			<?php
			} ?>

			<?php if($isAdmin || $auth->isModuleAccessible('test_master',$user_id)) { ?>
				<li class="<?php echo $current_sub['test_master']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=test_master&do=manage');?>">Manage Tests</a>
				</li>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'test_master', 'add')): ?>
				<li class="<?php echo $current_sub['test_master']['add']; ?>">
					<a href="<?php print CreateURL('index.php','mod=test_master&do=add');?>">Add Test</a>
				</li>
				<?php endif; ?>

				<li class="<?php echo $current_sub['test_master']['archive']; ?>">
					<a href="<?php print CreateURL('index.php','mod=test_master&do=archive');?>">Archive Tests</a>
				</li>
			<?php } ?>

			<?php if($isAdmin || $auth->isModuleAccessible('paper',$user_id)) { ?>
				<li class="<?php echo $current_sub['paper']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=paper&do=manage');?>">Manage Papers</a>
				</li>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'paper', 'add')): ?>
				<li class="<?php echo $current_sub['paper']['add']; ?>">
					<a href="<?php print CreateURL('index.php','mod=paper&do=add');?>">Add Paper</a>
				</li>
				<?php endif; ?>

			<?php } ?>

			<?php  if($isAdmin || $auth->isModuleAccessible('homework',$user_id)) { ?>
				<!--<li class="<?php echo $current_sub['homework']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=homework&do=manage');?>">Manage Homeworks</a>
				</li>-->

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'homework', 'add')): ?>
				<!--<li class="<?php echo $current_sub['homework']['add']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=homework&do=add');?>">Add Homework</a>
				</li>-->
				<?php endif;  ?>

			<?php } ?>
			</ul>
		</li>
		<?php
	endif;

	if ($isAdmin ||  $auth->isModuleAccessible('question_master',$user_id)):
		?>
		<li class="<?php echo $current['question_master']; ?>"><a href="<?php print CreateURL('index.php','mod=question_master&do=manage&master_nav=1');?>"><b>Questions</b></a>
			<ul id="secondary">
				<li class="<?php echo $current_sub['question_master']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=question_master&do=manage');?>">Manage Questions</a>
				</li>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'question_master', 'add')): ?>
				<li class="<?php echo $current_sub['question_master']['add']; ?>">
					<a href="<?php print CreateURL('index.php','mod=question_master&do=add');?>">Add Question</a>
				</li>
				<?php endif; ?>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'question_master', 'upload_question')): ?>
				<li class="<?php echo $current_sub['question_master']['upload_question']; ?>">
					<a href="<?php print CreateURL('index.php','mod=question_master&do=upload_question');?>">Upload Questions CSV</a>
				</li>
				<?php endif; ?>
                <?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'question_master', 'upload_question_doc')): ?>
                    <li class="<?php echo $current_sub['question_master']['upload_question_doc']; ?>">
                        <a href="<?php print CreateURL('index.php','mod=question_master&do=upload_question_doc');?>">Upload Questions DOC</a>
                    </li>
                <?php endif; ?>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'question_master', 'export_question')): ?>
				<li class="<?php echo $current_sub['question_master']['export_question']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=question_master&do=export_question');?>">Export Questions</a>
				</li>
				<?php endif; ?>
				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'question_master', 'group_questions')): ?>
				<li class="<?php echo $current_sub['question_master']['group_questions']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=question_master&do=group_questions');?>">Add Group Questions</a>
				</li>
				<?php endif; ?>
				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'question_master', 'group_questions')): ?>
				<li class="<?php echo $current_sub['question_master']['group_questions']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=question_master&do=group_questions_listing');?>">Manage Group Questions</a>
				</li>
				<?php endif; ?>

			</ul>
		</li>
		<?php
	endif;

	if ($isAdmin || $auth->isModuleAccessible('subject_master',$user_id)):
		?>
		<li class="<?php echo $current['stream']; ?>"><a href="<?php print CreateURL('index.php','mod=subject_master&do=manage&master_nav=1');?>"><b>Subjects</b></a>
			<ul id="secondary">
				<li class="<?php echo $current_sub['subject_master']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=subject_master&do=manage');?>">Manage Subjects</a>
				</li>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'subject_master', 'add')): ?>
				<li class="<?php echo $current_sub['subject_master']['add']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=subject_master&do=add');?>">Add Subject</a>
				</li>
				<?php endif; ?>
			</ul>
		</li>
		<?php
	endif;

	/*if ($isAdmin ||  $auth->isModuleAccessible('teacher_master',$user_id)):
		?>
		<li class="<?php echo $current['teacher_master']; ?>"><a href="<?php print CreateURL('index.php','mod=teacher_master&do=manage&master_nav=1');?>"><b>Teachers</b></a>
			<ul id="secondary">
				<li class="<?php echo $current_sub['teacher_master']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=teacher_master&do=manage');?>">Manage Teachers</a>
				</li>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'teacher_master', 'add')): ?>
				<li class="<?php echo $current_sub['teacher_master']['add']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=teacher_master&do=add');?>">Add Teacher</a>
				</li>
				<?php endif; ?>
			</ul>
		</li>
		<?php
	endif;
*/
	if ($isAdmin ||  $auth->isModuleAccessible('candidate_master',$user_id)):
		?>
		<li class="<?php echo $current['candidate_master']; ?>"><a href="<?php print CreateURL('index.php','mod=candidate_master&do=manage&master_nav=1');?>"><b>Students</b></a>
			<ul id="secondary">
				<li class="<?php echo $current_sub['candidate_master']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=candidate_master&do=manage');?>">Manage Students</a>
				</li>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'candidate_master', 'add')): ?>
				<li class="<?php echo $current_sub['candidate_master']['add']; ?>">
					<a href="<?php print CreateURL('index.php','mod=candidate_master&do=add');?>">Add Student</a>
				</li>
				<?php endif; ?>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'candidate_master', 'upload_candidate')): ?>
				<li class="<?php echo $current_sub['candidate_master']['upload_candidate']; ?>">
					<a href="<?php print CreateURL('index.php','mod=candidate_master&do=upload_candidate');?>">Upload Students</a>
				</li>
				<?php endif; ?>

				<?php if($_SESSION['admin_user_type'] == 'P'): ?>
				<li class="<?php echo $current_sub['candidate_master']['link']; ?>">
					<a href="<?php print CreateURL('index.php','mod=candidate_master&do=link');?>">Link Student</a>
				</li>
				<?php endif; ?>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'candidate_master', 'log')): ?>
				<li class="<?php echo $current_sub['candidate_master']['log']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=candidate_master&do=log');?>">Student Log Details</a>
				</li>
				<?php endif; ?>

			</ul>
		</li>
		<?php
	endif;

	/*if ($isAdmin ||  $auth->isModuleAccessible('parent_master',$user_id)):
		?>
		<li class="<?php echo $current['parent_master']; ?>"><a href="<?php print CreateURL('index.php','mod=parent_master&do=manage&master_nav=1');?>"><b>Parents</b></a>
			<ul id="secondary">
				<li class="<?php echo $current_sub['parent_master']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=parent_master&do=manage');?>">Manage Parents</a>
				</li>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'parent_master', 'add')): ?>
				<li class="<?php echo $current_sub['parent_master']['add']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=parent_master&do=add');?>">Add Parent</a>
				</li>
				<?php endif; ?>

			</ul>
		</li>
		<?php
	endif;
	*/
	if ($isAdmin ||  $auth->isModuleAccessible('report',$user_id)):
		?>
		<li class="<?php echo $current['report']; ?>"><a href="<?php print CreateURL('index.php','mod=report&do=result_sheet&master_nav=1');?>"><b>Reports</b></a>
			<ul id="secondary">
				<li class="<?php echo $current_sub['report']['result_sheet']; ?>">
					<a href="<?php print CreateURL('index.php','mod=report&do=result_sheet');?>">Result Sheets</a>
				</li>

				<?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'report', 'candidate_detail')): ?>
				<li class="<?php echo $current_sub['report']['candidate_detail']; ?>">
					<a href="<?php print CreateURL('index.php','mod=report&do=candidate_detail');?>">Student Details</a>
				</li>
				<?php endif; ?>

				<li class="<?php echo $current_sub['report']['candidate_performance']; ?>">
					<a href="<?php print CreateURL('index.php','mod=report&do=candidate_performance');?>">Student Performance</a>
				</li>
				<!--<li class="<?php echo $current_sub['homework_history']['manage']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=homework_history');?>">Homework History</a>
				</li>-->
			</ul>
		</li>
		<?php
	endif;

	/*if ($isAdmin ||  $auth->isModuleAccessible('role',$user_id)):
		?>
		<li class="<?php echo $current['role']; ?>"><a href="<?php print CreateURL('index.php','mod=role&master_nav=1'); ?>"><b>Roles</b></a>
			<ul id="secondary">
				<li class="<?php echo $current_sub['role']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=role'); ?>">Manage Roles</a>
				</li>
				<li class="<?php echo $current_sub['role']['add']; ?>">
					<a href="<?php print CreateURL('index.php','mod=role&do=add'); ?>">Add Role</a>
				</li>
				<li class="<?php echo $current_sub['users']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=users'); ?>">Manage Admin Users</a>
				</li>
				<li class="<?php echo $current_sub['users']['add']; ?>">
					<a href="<?php print CreateURL('index.php','mod=users&do=add'); ?>">Add Admin Users</a>
				</li>
				<?php
				if ($isAdmin):
					?>
					<li class="<?php echo $current_sub['users']['cpass']; ?>">
						<a class="last" href="<?php print CreateURL('index.php','mod=users&do=cpass&nameID='.$admin_info->id); ?>">Change Admin Password</a>
					</li>
					<?php
				endif;
				?>
			</ul>
		</li>
		<?php
	endif;*/

	if ($isAdmin || $auth->isModuleAccessible('backup',$user_id) || $auth->isModuleAccessible('feedback',$user_id) || $auth->isModuleAccessible('mailer',$user_id)):
		?>
		<li class="<?php echo $current['tools']; ?>"><a href="<?php print CreateURL('index.php','mod=mailer&master_nav=1'); ?>"><b>Tools</b></a>
			<ul id="secondary">

				<?php if($isAdmin || $auth->isModuleAccessible('mailer',$user_id)) { ?>
				<li class="<?php echo $current_sub['mailer']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=mailer'); ?>">Send Mail</a>
				</li>
				<?php } ?>

				<?php if ($isAdmin ||  $auth->isModuleAccessible('feedback',$user_id)) { ?>
				<li class="<?php echo $current_sub['feedback']['manage']; ?>">
					<a href="<?php print CreateURL('index.php','mod=feedback&do=manage');?>">Feedback</a>
				</li>
				<?php } ?>

				<?php if($isAdmin || $auth->isModuleAccessible('backup',$user_id)) { ?>
				<li class="<?php echo $current_sub['backup']['list']; ?>">
					<a href="<?php print CreateURL('index.php','mod=backup&do=list'); ?>">Backup Details</a>
				</li>
				<li class="<?php echo $current_sub['backup']['takeBackup']; ?>">
					<a href="<?php print CreateURL('index.php','mod=backup&do=takeBackup'); ?>">Take Backup</a>
				</li>
				<li class="<?php echo $current_sub['backup']['upload']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=backup&do=upload'); ?>">Restore Backup</a>
				</li>
				<?php } ?>

			</ul>
		</li>
		<?php
	endif;

	/*
	if ($isAdmin ||  $auth->isModuleAccessible('preferences',$user_id)):
		?>
		<li class="<?php echo $current['preferences']; ?>"><a href="<?php print CreateURL('index.php','mod=preferences&do=view');?>"><b>Configuration</b></a>
			<ul class="secondary">
				<li class="<?php echo $current_sub['preferences']['view']; ?>">
					<a class="last" href="<?php print CreateURL('index.php','mod=preferences&do=view');?>">Configuration</a>
				</li>
			</ul>
		</li>
		<?php
	endif;
	*/

//Package module start
if ($isAdmin || $auth->isModuleAccessible('package',$user_id)):
    ?>
    <li class="<?php echo $current['package']; ?>"><a href="<?php print CreateURL('index.php','mod=package&do=manage&master_nav=1');?>"><b>Package</b></a>
        <ul id="secondary">
            <li class="<?php echo $current_sub['package']['manage']; ?>">
                <a href="<?php print CreateURL('index.php','mod=package&do=manage');?>">Manage Package</a>
            </li>

            <?php if($isAdmin || $auth->isAuthorisedAction($user_id, 'package', 'add')): ?>
                <li class="<?php echo $current_sub['package']['add']; ?>">
                    <a class="last" href="<?php print CreateURL('index.php','mod=package&do=add');?>">Add Package</a>
                </li>
            <?php endif; ?>
        </ul>
    </li>
<?php
endif;
// package module end
?>

<li><a href="<?php print CreateURL('index.php','mod=admin&do=logout');?>"><b>Logout</b></a></li>
</ul>
<div id="menu-border"></div>
</div>