<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
<form name="frmlist" method="post">

<table width="45%" height="145" border="0" align="center" cellpadding="0" cellspacing="1" class="tablecss"style="background: white;padding: 10px;">
<tr>
<th colspan="2">Restore</th>
</tr>
<?php 
if($backupSingalData)
{
			if(!file_exists(BACKUP."/".$backupSingalData->backupFilePath))
			{
				 echo '<tr><td align="center">Sorry! no backup file found &nbsp;&nbsp;<input type="button" name="back"  value="Back" onclick="location.href=\''.createURL('index.php','mod=backup').'\'"/></td></tr>';
			}
			else
			{
?>
				
                  <tr>
                    <td>Date: <?php echo date('m-d-Y' ,strtotime($backupSingalData->backup_date)); ?></td>
                  </tr>
                  <tr>
                    <td>File Name: <?php echo $backupSingalData->backup_file_path; ?></td>
                  </tr>
                  <tr>
                    <td>File Size: <?php echo number_format((filesize(BACKUP."/".$backupSingalData->backup_file_path)/1024)/1024,2); ?> MB</td>
                  </tr>
                 <!-- <tr>
                    <td>Remarks: <?php echo $backupSingalData->remarks; ?></td>
                  </tr>-->
                  <!--<tr>
                    <td><div style="width:700px;border:1px #3399FF solid;display:inline-block;"><?php echo readfile(BACKUP."/".$backupSingalData->backupFilePath); ?></div></td>
                  </tr>-->
                  <tr>
					<td height="41" colspan="2" align="center" >
							<?php if(!$logRes)
						 		  {
							?> 
                            			<input type="submit" name="restore" onclick="return confirm('This will remove all the updates that have been made into the system after this backup file creation.\nDo you really want to restore?');" class="buttons"  value="Restore"/> 
                            <?php
                            	  }
								  else
								  {
								  	echo "<strong style='color:red;'>Parameter update module is in use. First close the update parameter module log by committing or canceling the changes done in temporary tables.</strong><br><br>";
								  }
							?>            
    <input type="hidden" name="fileID" class="button" value="<?php echo $getVars['id']?>" />
	<input type="button" name="back" class="buttons"  value="Back" onclick="location.href='<?php echo createURL('index.php','mod=backup')?>'"/></td>
</tr>
                  

<?php
			
			}	
}
else
{
	 echo '<tr><td align="center">Sorry! no backup found &nbsp;&nbsp;<input type="button" name="back"  value="Back" onclick="location.href=\''.createURL('index.php','mod=backup').'\'"/></td></tr>';
}
?>

</table>
</form>
</body>
</html>
