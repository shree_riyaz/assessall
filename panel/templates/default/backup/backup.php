<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Backup</title>
</head>
<body onload="document.addfrm.download.focus();">
<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
				include_once(CURRENTTEMP."/"."header.php");
			?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				<div id="main">
					<div id="contents">
						<form id="addfrm" name="addfrm" method="post" action="">
						<fieldset class="rounded" style="width: 40%;"><legend>Backup</legend><br/>
						<?php
							if(isset($_SESSION['error']))
							{
							echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
							echo $_SESSION['error'];
							echo '</td></tr></tbody></table>';
							unset($_SESSION['error']);
							}
							
							if(isset($_SESSION['success']))
							{
							echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
							echo $_SESSION['success'];
							echo '</td></tr></tbody></table>';
							unset($_SESSION['success']);
							}
						?>
							<table border="0" cellpadding="4" cellspacing="0" align="left" style="margin-left: 30px;">
							  <tr>
								<td align="center"><input type="checkbox" name="download" id="download" <?php echo $exportEnabled; ?>/> 
								Download Backup File </td>
							  </tr>
							  
							  <tr>							  	
							  	<td colspan="1">
						  			<div align="center">
									    <input type="submit" name="finalBackup" id="finalBackup" value="Start" class="buttons rounded" />
							            <input type="button" name="cancel" value="Cancel" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=backup'"/>
								  	</div>
								</td>
							  </tr>		
							  <tr>
							  	<td>&nbsp;</td>
							  </tr>						  
							</table>
							</fieldset>
							<br/>
							
							
						  </form>
				  
					</div><!--Div Contents closed-->
				</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	</table>	
</div><!--Outer wrapper closed-->
</body>
</html>

