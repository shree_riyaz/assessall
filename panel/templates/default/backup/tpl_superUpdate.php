<?php 
echo "<script language='javascript'  src='".ROOTURL."/js/jquery.js' /></script>";
?>
<script type="text/javascript">
	function ajaxRequest()
	{
		
		
		var superIDs = Array();
		
		<?php
			if($superUserList)
			{
				$sUcount=count($superUserList);
				for($counter = 0; $counter < $sUcount; $counter++)
				{
					echo 'superIDs['.$counter.']='.$superUserList[$counter]->nameID.";";
				}
			}
			
			
		?>
		//disable start buttun
		$("#start").attr("disabled",false);
		
		
		var getError=false;
		
		//hide the error div from beginning
		$("#error").css('display','none');
		//add remove the text
		$("#error").text('');
		$("#start").attr("disabled",true);
		
		var len=superIDs.length;
		//alert(len);
		for(var counter=0;counter<len;counter++)
		{
			var msg='';
			var getError=false;
			
			//loading image for current process
			$("#suser"+superIDs[counter]).attr("src","<?php echo IMAGEURL."/ajax-loading.gif" ?>");
			
			//sent request through ajax 
			var html = $.ajax({
						  type: "POST",
						  url: "<?php echo  createURL('index.php',"mod=backup&do=checkConditions"); ?>",
						  data: "userID="+superIDs[counter],
						  dataType: 'json',
						  async: false,
						  success: function (data, status)
						  {
						  	msg=data.msg;
							
							//I for incorrect means error is raised
							if(data.msg=='I')
							{
								$("#suser"+superIDs[counter]).attr("src","<?php echo IMAGEURL."/wrong.png" ?>");
								$("#error").css('display','');
								
								$("#error").append(data.error);
								getError=true;
								
							}
							//completed successfully
							else if(data.msg=='C')
							 {
								//alert(data.log);
								$("#suser"+superIDs[counter]).attr("src","<?php echo IMAGEURL."/right.png" ?>");
							 }
							 //weird errrr 
							 else
							 {
							 	$("#suser"+superIDs[counter]).attr("src","<?php echo IMAGEURL."/wrong.png" ?>");
								$("#error").css('display','');
								$("#error").append(data.error);
								msg='E';
								getError=true;
								
							 } 
							 	
						  },
						  error: function (data, status, e)
						  {
						  	//alert(e);
							msg='E';
						  }
						 }).responseText;
			
			 //if error do not have any proper message
			if(msg=='E' || msg=='I')
			{
				if(msg=='E')
				{
					$("#error").css('display','');
					$("#error").append(html);
					$("#suser"+superIDs[counter]).attr("src","<?php echo IMAGEURL."/wrong.png" ?>");
				}
				getError=true;
				//break;
			}	
				 
		}
		
		//if did not any error enable the superDataFilefinish button
		if(!getError)
		{
			$("#superDataFilefinish").attr("disabled",false);
			
		}
		
	}
	
	
</script>

<form name="dataFileProcess" method="post">	
<?php
if($superUserList)
{
	$sUcount=count($superUserList);
?>
<table width="66%"  border="0" align="center" cellpadding="0" cellspacing="1">
  <tr>
    <td height="36" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td  style="padding-left:0px">  
        <table width="100%" height="276"  border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <th height="20" colspan="2"><h3>Update Super User Data File</h3></th>
          </tr>
          <tr>
            <td height="52" colspan="2"><strong> &nbsp;Click on &quot;Start&quot; button to start execution, and wait for a while. Click on &quot;Finish&quot; button to go to next page.</strong></td>
          </tr>
          <?php
		  	for($counter = 0; $counter < $sUcount; $counter++)
			{
		  ?>
              <tr>
                <td width="6%" align="right"><img id="suser<?php echo $superUserList[$counter]->nameID ?>" src="<?php echo IMAGEURL."/bullate.png" ?>" /></td>
                <td width="94%"><?php echo $superUserList[$counter]->fname." ". $superUserList[$counter]->lname." ( ".$superUserList[$counter]->entityName." )" ?></td>
              </tr>
          <?php
		  	}
		  ?>
         
          <tr>
            <td  colspan="2" align="right"><div id="SC" >
              <input type="button" name="start" id="start" value="Start" onClick="ajaxRequest();"  />
              <input type="submit" name="superDataFilefinish" id="superDataFilefinish" value="Finish"  disabled="disabled" />
            </div></td>
          </tr>
      </table>
    </td>
  </tr>
</table>  
<?php
}
?>     
<br />
<br />
</form>
<table width="66%" height="153" border="0" align="center" cellpadding="0"  cellspacing="0">
  <tr>
  <td> <div id="error" style="display:none;border:2px #FF0000 solid;padding:5px 5px 5px 5px; width:400px;" ></div>
  </td></tr></table><br />
<br />

