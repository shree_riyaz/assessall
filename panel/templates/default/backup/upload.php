<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Restore Backup</title>
</head>
<body onload="document.addfrm.sql.focus();">

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
				include_once(CURRENTTEMP."/"."header.php");
			?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				<div id="main">
					<div id="contents">
						<?php
							if(isset($_SESSION['error']))
							{
							echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
							echo $_SESSION['error'];
							echo '</td></tr></tbody></table>';
							unset($_SESSION['error']);
							}
							
							if(isset($_SESSION['success']))
							{
							echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
							echo $_SESSION['success'];
							echo '</td></tr></tbody></table>';
							unset($_SESSION['success']);
							}
						?>
						<form id="addfrm" name="addfrm" method="post" action="" enctype="multipart/form-data">
						<fieldset class="rounded" style="width: 50%;"><legend>Restore Backup</legend><br/>
						<table width="" border="0" align="left" cellpadding="0" cellspacing="1" class="">
						  <tr class="trwhite" style="height:36px">
							<td width="151" align="right">Backup File:</td>
							<td width="214" align="left"><input type="file" name="sql" id="sql" size="30" onblur="validUpload(this)"/></td>
						  </tr>
						  <tr>
						  	<td colspan="2" align="center">							
								<input name="upload"  class="buttons rounded" type="submit" id="upload" onClick="return validUpload(document.getElementById('sql'))" value="Restore" />
								&nbsp;
				  				<input name="back" type="button" id="back" value="Back" class="buttons rounded" onclick="location.href='<?php echo CreateURL('index.php','mod=backup');?>'"/>
							</td>
						  </tr>
						  <tr><td>&nbsp;</td></tr>
						</table>  
							</fieldset>
							<br/>
							
							
						  </form>
				  
					</div><!--Div Contents closed-->
				</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	</table>	
</div><!--Outer wrapper closed-->
</body>
</html>
<script>
function validUpload(val)
{
   var error = 0;
   var message =''; 
  //================================================================================================ 
    if(val.value!="") // For invoice uploading
	 { 
       //check the extension of the uploaded file
       Ext=val.value.substring(val.value.lastIndexOf('.')+1);
	   Ext=Ext.toLowerCase();
	   
		  if(Ext!='sql' )
		  {
		    error=1;
			message=message + "Please upload file in .sql format.\n";
		  }
	 }
 
	 if(error==1)
       {
	     val.value='';
		 alert(message);
		 val.focus();
		 return false;
	   }
	else
	  {
	     return true;
	   } 
 }
</script>