<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Backup Details</title>
</head>
<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.fromDates.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); 
			?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
			<div id="main">
				<div id="contents">
				<form action="" method="post" name="frmlist" id="frmlist">
				<legend>Backup Details</legend>
				<?php 
				if (isset($_SESSION['ActionAccess']))
				{
					echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
					echo $_SESSION['ActionAccess'];
					echo '</td></tr></tbody></table>';
					unset($_SESSION['ActionAccess']);
				}
				// Show particular Messages
				if(isset($_SESSION['error']))
				{
					echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
					echo $_SESSION['error'];
					echo '</td></tr></tbody></table>';
					unset($_SESSION['error']);
				}
				if(isset($_SESSION['success']))
				{
					echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
					echo $_SESSION['success'];
					echo '</td></tr></tbody></table>';
					unset($_SESSION['success']);
				}
			
				//print_r($frmdata);
				?>
			<form name="frmlist" method="post" action="">
			
			<div class=" search-div">
			<fieldset class="rounded search-fieldset"><legend>Search</legend>
				<table border="0" cellspacing="1" cellpadding="3" width="100%">
					<tr>
			    		<td align="right">From:</td>
						<td>
							<input name="fromDates" class="rounded textfield date" onBlur="isDate(this)" id="fromDates" type="text" value="<?php echo $frmdata['fromDates']; ?>" maxlength="10" />
			    		</td>
			    	</tr>
			    	<tr>
			    		<td align="right">To:</td>
				      	<td>
				      		<input name="toDates" id="toDates"  type="text" class="rounded textfield date" onBlur="isDate(this)" value="<?php echo $frmdata['toDates']; ?>" maxlength="10" />
				      	</td>
					</tr>
					<tr>
						<td>Name:</td>
						<td>
							<input name="names" id="names" class="rounded textfield" value="<?php echo $frmdata['names']; ?>" type="text" maxlength="50" />
						</td>
					</tr>
					<tr>
			    		<td colspan="6" align="center">
							<input name="search"  class="buttons rounded" type="submit" id="search2" onClick="return validBackup()" value="Search" />
							<input name="clearsearch" type="submit" id="clearsearch" value="Clear Search" class="buttons rounded"  /></td>
			    		</td>
		  			</tr>
				</table>
			</fieldset>
			</div><br>

			   <?php 
			   
				if($backupData)
				{
				   
					$srNo=$frmdata['from'];
					$count=count($backupData);
					
			  ?>
			  	<table border="0" cellspacing="0" cellpadding="4" width="100%" style="" align="right">
				<tr>
					<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($list))." of ".$totalCount; ?></td>
					<td align="right"><?php echo getPageRecords();?> </td>
				</tr>
				</table>
		
				<br/>
			  	<div id="celebs">
				<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
					<thead>
					<tr class="tblheading"> 
						<td width="31" align="center" height="29" ><b>S.No</b></td>
						<td width="150" align="center" >Name</td>
						<td width="135" align="center" ><a href="#" style="cursor:pointer;color:#af5403;" onClick="OrderPage('backup_date');">Date<?php if($frmdata['orderby']=='backup_date') {echo '<img src="'.ROOTURL.'/images/asc.gif">';} elseif($frmdata['orderby']=='lname desc'){echo '<img src="'.ROOTURL.'/images/desc.gif">';}?></a></td>
						<td width="250" align="center" height="29" >FileName</td>						
						<td width="74" align="center" height="29">Restore</td>
						<td width="65"  align="center" >Download</td>
				  	</tr>
				  	</thead>
					
				<?php 	
					for($counter=0;$counter<$count;$counter++)
					{
						$srNo++;
					    if(($counter%2)==0)
						{
							$trClass="tdbggrey";
						}
						else
						{
							$trClass="tdbgwhite";
						}
						if ($backupData[$counter]->user_type == 'C')
						{
							$candidate = $DB->SelectRecord('candidate','id ='.$backupData[$counter]->user_id);
							if ($candidate)
							{
								$name = $candidate->first_name. ' '. $candidate->last_name;
							}
							else
							{
								$name ='n/a';
							}
						}
						else
						{
							$name = "Admin";
						}
					//if($counter%2==0) { 
				?>
					  <tr class='<?= $trClass; ?>'>
				 <td align="center" ><?php  echo $srNo ?> </td>
				 <td width="67" align="center" class="fontstyle" ><?php echo highlightSubString($frmdata['names'],$backupData[$counter]->user_name); ?></td>
				<td width="135" align="left" class="fontstyle">&nbsp;&nbsp;<?php echo date('d-m-Y H:i:s' ,strtotime($backupData[$counter]->backup_date)); ?></td>
				   <td  align="left" class="fontstyle" >&nbsp;&nbsp;<?php echo $backupData[$counter]->backup_file_path; ?></td>				
				 <td align="center">
				<?php echo '<a style="cursor:hand" class="fontstyle" href="'.CreateURL('index.php','mod=backup&do=restore&id='. Encode($backupData[$counter]->id)).'">Restore</a>';  ?> 
					</td>
				  <td align="center">
				 <a style="cursor:hand" class="fontstyle" href='<?php echo CreateURL('index.php','mod=backup&do=download&id='. $backupData[$counter]->id);?>'>Download</a>
					</td>  
				<!--<td align="center"> 
						<input name="saleschk[]" type="checkbox"  style="border:none" id="saleschk" value="<?php // echo $saleslist[$counter]->salesconID;?>">
				</td-->
			</tr>
			
				<?php
					} 
				?>
				</table>
				</div>
			<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">	
						<!--<tr>
						 <td align="right" colspan="10" ><a onClick="Checkall(document.frmlist,'true')" style=" font-family:Arial, Helvetica, sans-serif;cursor:pointer;">Select All</a> / <a style=" font-family:Arial, Helvetica, sans-serif;cursor:pointer;" onClick="Checkall(document.frmlist,'')">Unselect All</a></td>
						</tr>-->
				
					 <tr>
					 <td align="center" colspan="10">
					<?php
						PaginationDisplay($totalCount);
					?>
					</td>
					</tr>
			</table> 
				 <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr class="tr" valign="bottom"> 
				  <td height="34" align="center">
				 
			<!--  <input class="input" type="submit" name="deletesales" value="Delete Sales Consultant" style="width:125px" onClick="if(CheckSubmit('<?php echo $count;?>',document.frmlist.saleschk,'delete')) return ifdeletesalesconsultant('<?php echo $count?>'); else return false;  "title="Click to delete" >
			-->	  		
				   
				  </td>
				</tr>
			  </table>
			  
			  <?php
			  }
			  else
			  {
				echo "<center>No Record found.</center>";
			  }
			  ?>
			  
			<input name="pageNumber" type="hidden" value="<?php echo $frmdata['pageNumber']?>">
			<input name="orderby" type="hidden" value="<?php echo $frmdata['orderby']?>">
			<input name="order" type="hidden" value="<?php echo $frmdata['order']?>">
			<input name="searchStatus" type="hidden" value="<?php echo $frmdata['searchStatus']?>">
			</form>
	</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
<?php
if(isset($tempVar))
{
	echo '<script>window.location.href="'.Decode($tempVar).'"</script>';
}
?>  
</html>