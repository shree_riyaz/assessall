<?php
/*******************************************************************************************************
 * 			Developed by :- Ashwini Agarwal
 *             Date         :- March 6, 2012
 *             Module       :- dashboard
 *             Purpose      :- Details of candidate in last exam.
 ********************************************************************************************************/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Total <?php echo $page_name; ?> Students</title>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.name.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<div>
				<?php $href_dash = CreateURL('index.php','mod=dashboard&do=showinfo'); ?>
				<a href = "<?php echo $href_dash ?>">Dashboard</a> &#8594; <?php echo $_SESSION['lastTestName'] . " &#8594; " . $page_name; ?> Students
			</div><br>
			<form action="" method="post" name="frmlist" id="frmlist">
			<fieldset class="rounded"><legend>Student Details</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['warning']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="left" style="padding:3px 3px 3px 3px;">';
				echo $_SESSION['warning'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['warning']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			<div class="search-div">
			<fieldset class="rounded search-fieldset"><legend>Search</legend>
				<table border="0" cellspacing="1" cellpadding="3" width="100%">
				<tr>
				    <td align="right">Name:</td>
			    	<td>
			    		<input type="text" size="20" class="rounded textfield" name="name" id="name" value="<?php echo $frmdata['name'] ?>" onchange="managePageCheckIsChar(this.value,this.id, 'name');" />
			    	</td>
			    </tr>
				
				<tr>
					<td colspan="6" align="center">
			      		<input type="submit" class="buttons rounded" name="Submit" value="Show" />
			      		<input type="submit" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick=""/>
			    	</td>
			  	</tr>
				</table>
			</fieldset>
			</div>
			<?php
			if($candidatelist)
			{ 
			?>
			<table border="0" cellspacing="0" cellpadding="4" width="100%" style="" align="right">
			<tr>
				<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($candidatelist))." of ".$totalCount; ?></td>
				<td align="right"><?php echo getPageRecords();?> </td>
			</tr>
			</table>
			
			<br/>
			<?php
				} 
			?>
			<br/>
				<div id="celebs">
				<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
					  <thead>
					  <tr class="tblheading">
						<td width="1%">#</td>
						<td width="31%">
						<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('first_name');">Student Name
						<?php if($frmdata['orderby']=='first_name') 
							{
								print '<img src="'.ROOTURL.'/images/asc.gif">';
							} 
							elseif($frmdata['orderby']=='first_name desc')
							{
								print '<img src="'.ROOTURL.'/images/desc.gif">';
							}
						?>
						</a></td>
						<?php 
							if($page_name == 'Appeared')
							{
								echo '<td width="10%">Test Result</td>';
							}
						?>
					  </tr>
					  </thead>
					  <tbody>
					  
				  <?php 
				 // echo "fdknm".count($questionlist);
				 //print_r($candidatelist);
				 //exit; 
					$srNo=$frmdata['from'];
					$count=count($candidatelist);
					if($candidatelist[0]->id!='')
					{
						for($counter=0;$counter<$count;$counter++) 
						{ 
							$id=$candidatelist[$counter]->id;
							$fname=ucfirst(strtolower(stripslashes($candidatelist[$counter]->first_name)));
							$lname=ucfirst(strtolower(stripslashes($candidatelist[$counter]->last_name)));
							$name=$fname.' '.$lname;
							$stream=stripslashes($candidatelist[$counter]->stream_title);
							
							$srNo=$srNo+1;
							if(($counter%2)==0)
							{
								$trClass="tdbggrey";
							}
							else
							{
								$trClass="tdbgwhite";
							}
						?>
							<tr class='<?= $trClass; ?>'>
								<td><?php echo $srNo; ?></td>
								<td><?php echo highlightSubString($frmdata['name'], $name); ?></td>
								<?php if($page_name == 'Appeared') echo "<td>".stripslashes($candidatelist[$counter]->result)."</td>"; ?>
							</tr>
						<?php 
						}
					 }
						else
						{
							echo "<tr><td colspan='8' align='center'>(0) Record found.</td></tr>";
						}
					?>
					  
					  </tbody>
					</table>
				</div>
				<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
					  <tr>
						<td width="10%">&nbsp;</td>
						<td width="54%">&nbsp;</td>
						<td width="36%" align="right">&nbsp;
						<?php if(($totalCount > 0) && $exportEnabled) 
							{ 
								$exportfor = ($page_name == 'Passed') ? 'pass' : (($page_name == 'Failed') ? 'fail' : 'total');
						?>
							<img style="cursor: pointer;" src="<?php echo IMAGEURL ?>/file-xls.gif" alt="Export to Excel" title="Click here for export data to excel sheet" onclick="window.location='<?php echo ROOTURL.'/index.php?mod=export&exportfor='.$exportfor; ?>'"/>
						<?php } ?>
						</td>
					  </tr>
					  <tr>
					  <?php 
						if($candidatelist[0]->id!='')
						{
						?>
						<td colspan="2"><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?></td>
			   			<?php }?>
						<td align="right">
							<?php PaginationDisplay($totalCount);	?>
							</td>
					  </tr>
				</table>
				<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
				<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
				<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />	
  			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>