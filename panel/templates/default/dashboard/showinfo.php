<?php
error_reporting(E_ALL);
ini_set('display_errors', '0');
 /*****************Developed by :-Akshay Yadav
				Module       :- Dashboard
				Purpose      :- Template for Browse all data
	***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Dashboard</title>

<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.Charts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionChartsExportComponent.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.jqueryplugin.js"; ?>'></script>

<script>
function newGraph(path, div, type)
{
	var width = 400;
	var height = 200;
	var swf = "<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf?noCache=" + new Date().getMilliseconds();
	if(type == 'stacked')
	{
		width = 900;
		height = 500;
		swf = "<?php echo ROOTURL; ?>/lib/FusionCharts/MSColumn3D.swf?noCache=" + new Date().getMilliseconds();
	}

	var id = "c" + Math.floor(Math.random()*1000);
	FusionCharts.setCurrentRenderer('javascript');
	var myChart = new FusionCharts( swf, id, width, height, 0, 1);
	myChart.setXMLUrl(path);
	myChart.render(div);
}

function studentLinkRequest(id, approve)
{
	xajax_studentLinkRequest(id, approve);
	$('#pc-'+id).remove();
}
</script>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>
<body>

<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr>
	<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
</tr>
<tr>
	<td>
		<div id="content">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
			<div id="main">
				<div id="contents">
					<div align="center">
					<?php
						if ($_SESSION['error'] != '')
						{
							echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
							echo $_SESSION['error'];
							echo '</td></tr></tbody></table>';
							unset($_SESSION['error']);
						}
					?>
					</div>

<?php
if($parent_candidate && (count($parent_candidate) > 0))
{
	foreach($parent_candidate as $pc)
	{ ?>

		<div class="dashboard-notification" id="pc-<?php echo $pc->id; ?>">
			<div style="float: left;">Parent (<a href="<?php echo CreateUrl('index.php', 'mod=parent_master&do=edit&nameID='.$pc->parent_id); ?>"><?php echo $pc->parent_name; ?></a>) wants to link with student (<a href="<?php echo CreateUrl('index.php', 'mod=candidate_master&do=edit&nameID='.$pc->candidate_id); ?>"><?php echo $pc->candidate_name; ?></a>).</div>
			<div style="float: right;">
				<a href="javascript:void(0);" onclick="studentLinkRequest('<?php echo $pc->id; ?>', 1);">Allow</a>&nbsp;&nbsp;
				<a href="javascript:void(0);" onclick="studentLinkRequest('<?php echo $pc->id; ?>', 0);">Deny</a>
			</div>
			<div style="clear: both;"></div>
		</div><?php

	} ?>

	<br /><?php
} ?>

<?php
if($homeworkhistory && (count($homeworkhistory) > 0)) { ?>

<div>Recently Submitted Homeworks</div>
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
<thead>
	<tr class="tblheading">
	  	<td width="15%">Course Name</td>
		<td width="15%">Homework</td>
		<td width="11%">Student Name</td>
		<td width="11%">Date</td>
		<td width="11%">Details</td>
  	</tr>
</thead>
<tbody>

<?php
$count = 0;
foreach($homeworkhistory as $hh)
{
	$detailedUrl = CreateUrl('index.php', "mod=homework_history&do=details&homework_id=$hh->homework_id&candidate_id=$hh->candidate_id");
	$class = ($count++%2) ? 'tdbgwhite' : 'tdbggrey'; ?>

	<tr class='<?php echo $class; ?>'>
		<td><?php echo stripslashes($hh->exam_name); ?></td>
		<td><?php echo stripslashes($hh->homework_name); ?></td>
		<td><?php echo stripslashes($hh->first_name.' '.$hh->last_name); ?></td>
		<td><?php echo date('d M Y h:i A',strtotime($hh->created_date)); ?></td>
		<td>
			<a href="<?php echo $detailedUrl; ?>" target="_blank"><img src="<?php echo IMAGEURL; ?>/user_info.gif" /></a>
		</td>
	</tr>

<?php } ?>

</tbody>
</table><br />
<?php } ?>

<div>Last Test Information</div>
<?php
if($testdetail[0]->test_name)
{
	$_SESSION['maximumMarks'] = $testdetail[0]->maximum_marks;
	$_SESSION['examName'] = $testdetail[0]->exam_name; ?>

	<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
	<thead>
		<tr class="tblheading">
		  	<td width="15%">Exam Name</td>
			<td width="15%">Test Name</td>
			<td width="11%">Start Date</td>
			<td width="11%">End Date</td>
			<td width="11%">Total Appeared</td>
			<td width="11%">Passed Students</td>
			<td width="11%">Failed Students</td>
	  	</tr>
	</thead>
	<tbody>

	<tr class='tdbggrey'>
		<td><?php echo stripslashes($testdetail[0]->exam_name); ?></td>
		<td><?php echo stripslashes($testdetail[0]->test_name); $_SESSION['lastTestName']=stripslashes($testdetail[0]->test_name); ?></td>
		<td><?php echo date('d M Y',strtotime($testdetail[0]->test_date)); ?></td>
		<td><?php echo date('d M Y',strtotime($testdetail[0]->test_date_end)); ?></td>
		<td>
			<?php
			if($testresult[0]->totalcandidate)
			{
				$href = CreateURL('index.php','mod=dashboard&do=total');
				echo "<a href = '$href'>" . number_format($testresult[0]->totalcandidate) . "</a>";
			}
			else echo 0;
			?>
		</td>
		<td>
			<?php
			if($testresult[0]->pass)
			{
				$href = CreateURL('index.php','mod=dashboard&do=pass');
				echo "<a href = '$href'>" . number_format($testresult[0]->pass) . "</a>";
			}
			else echo 0;
			?>
		</td>
		<td>
			<?php
			if($testresult[0]->fail)
			{
				$href = CreateURL('index.php','mod=dashboard&do=fail');
				echo "<a href = $href>" . number_format($testresult[0]->fail) . "</a>";
			}
			else echo 0;
			?>
		</td>
	</tr>

	</tbody>
	</table><?php

}
else
{ ?>
	<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
		<tbody>
		<tr class='tdbggrey'>
			<td colspan="5">No test has been conducted yet.</td>
		</tr>
		</tbody>
	</table><?php
}
?>

<br />
<fieldset class="rounded" style="padding: 5px;">
<legend>Test & Student</legend>
<?php
$FCExporter = ROOTURL ."/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
	//	$marksXml = "<chart caption='Subject_Result' showLegend='1' showPercentValues='0'
				//	bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270'
				//	showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1'
				//	html5ExportHandler='$FCExporter' exportFileName='Test'>";

			//	$marksXml = " <set label='Conducted Test' value='20' color='55ff55' />";
			//	$marksXml = "<set label='Remaining Test' value='40'  />";
		//	$marksXml = "</chart>";

			//	echo $marksXml ;
			$finishedTest = $DashboardOBJ->countfinishedTest();
		$remainingTest = $DashboardOBJ->countremainingTest();

$marksXml = "<chart caption='Test' showLegend='1' showPercentValues='0' bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1' html5ExportHandler='$FCExporter' exportFileName='marks_obtained'>";
$marksXml .= "<set label='Conducted Test' value='".$finishedTest[0]->finishtest."' color='55ff55' />";
$marksXml .= "<set label='Remaining Test' value='".$remainingTest[0]->test."' color='ff5555' />";
$marksXml .= "</chart>";

				?>
<div style="width: 50%;text-align: center;float: left;">
	<?php if(($remainingTest[0]->test + $finishedTest[0]->finishtest) > 0) { ?>
	<div id="test-graph" style="text-align: center;padding: 10px;">

	</div>
	<?php } ?>
	<div>
		<table cellspacing="1" cellpadding="3" style="margin: 0 auto;">
		<tr>
			<td >Total tests conducted</td>
			<td><strong><?php if($finishedTest[0]->finishtest) echo number_format($finishedTest[0]->finishtest); else echo 0; ?></strong></td>
		</tr>
		<tr>
			<td>Total remaining tests</td>
			<td><strong><?php if($remainingTest[0]->test) echo number_format($remainingTest[0]->test); else echo 0; ?></strong></td>
		</tr>
		</table>
	</div>
</div>

<?php
		$candidates = $DashboardOBJ->countStreamCandidate();

		$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";

		//--------------------------

		$timeTrackXml = "<chart caption='Students' showLegend='1' showPercentValues='0' bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270' showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1' html5ExportHandler='$FCExporter' exportFileName='Students'>";

foreach( $candidates as $candidate )
		{

			$timeTrackXml .= "<set label='".$candidate->exam_name."' value='".$candidate->candidate."' />";
		}

$timeTrackXml .= "</chart>";



		//-----------------------------

		?>

<div style="width: 50%;text-align: center;float: left;">
	<?php if(($totalCandidate->candidate) > 0) { ?>
	<div id="candidate-graph" style="text-align: center;padding: 10px;">
			</div>
	<?php } ?>
	<div>
		<table cellspacing="1" cellpadding="3" style="margin: 0 auto;">
		<tr>
			<td>Total Students</td>
			<td><strong><?php if($totalCandidate->candidate) echo number_format($totalCandidate->candidate); else echo 0; ?></strong></td>
		</tr>
		</table>
	</div>
</div>

<div style="clear: both;"></div>
</fieldset>


<?php
if($streamQue)
{ ?>

<br />
<fieldset class="rounded" style="padding: 5px;">
<?php

//-------------------------------------///////////////
/*$totalQue = $this->countSubjectQuestion();
		$subjects = array();

		$subjectCategoty = "<categories>";
		$biginnerData = "<dataset seriesName='Beginner'>";
		$intermediateData = "<dataset seriesName='Intermediate'>";
		$higherData = "<dataset seriesName='Higher'>";

		foreach($totalQue as $question)
		{
			if(in_array($question->subject_id, $subjects)) continue;

			$subjectCategoty .= "<category label='".ucwords($question->subject_name)."' />";

			$biginnerData .= "<set value='$question->beg' />";
			$intermediateData .= "<set value='$question->inter' />";
			$higherData .= "<set value='$question->high' />";

			$subjects[$question->subject_id] = $question->subject_name;
		}

		$biginnerData .= "</dataset>";
		$intermediateData .= "</dataset>";
		$higherData .= "</dataset>";
		$subjectCategoty .= "</categories>";

		$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
		$xml = "<chart caption='Question Bank' xAxisName='Subjects' yAxisName='Questions'
					bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270'
					showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1'
					html5ExportHandler='$FCExporter' exportFileName='Question_Bank'>";

		$xml .= $subjectCategoty;
		$xml .= $biginnerData;
		$xml .= $intermediateData;
		$xml .= $higherData;
		$xml .= '</chart>';

*/
//---------------------------------------/////////////
//---------------------
$totalQues = $DashboardOBJ->countSubjectQuestion();
$count=count($totalQues);
		$subjects = array();

//$testCategory = '<categories>';
	//$passedCandidates = "<dataset seriesName='Passed Students'>";
	//$failedCandidates = "<dataset seriesName='Failed Students'>";
	$subjectCategoty = "<categories>";
		$biginnerData = "<dataset seriesName='Beginner'>";
		$intermediateData = "<dataset seriesName='Intermediate'>";
		$higherData = "<dataset seriesName='Higher'>";

	foreach($totalQues as $question)
		{
			if(in_array($question->subject_id, $subjects)) continue;

			$subjectCategoty .= "<category label='".ucwords($question->subject_name)."' />";

			$biginnerData .= "<set value='$question->beg' />";
			$intermediateData .= "<set value='$question->inter' />";
			$higherData .= "<set value='$question->high' />";

			$subjects[$question->subject_id] = $question->subject_name;
		}


	$biginnerData .= "</dataset>";
	$intermediateData .= "</dataset>";
	$higherData .= "</dataset>";
	$subjectCategoty .= '</categories>';
	$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
	$graphXml = "<chart caption='Question Bank' xAxisName='Subjects' yAxisName='Questions'"
				." bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270'"
				." showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1'"
				." html5ExportHandler='$FCExporter' exportFileName='Question_Bank'>";

	$graphXml .= $subjectCategoty;
	$graphXml .= $biginnerData;
	$graphXml .= $intermediateData;
	$graphXml .= $higherData;
	$graphXml .= '</chart>';



//-------------------------

?>
<legend>Question Bank</legend>
<div id="question-graph" style="text-align: center;padding: 10px;"></div>
<?php if(($totalQue[0]->que) > 0) { ?>
<div id="question-graph" style="text-align: center;padding: 10px;"></div>
<?php } ?>

<div id="celebs">
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
<thead>
	<tr class="tblheading">
		<td width="1%">#</td>
		<td width="21%">Subject</td>
		<td width="11%">Beginner</td>
		<td width="11%">Intermediate </td>
		<td width="11%">Higher</td>
		<td width="15%">Total Questions</td>
	</tr>
</thead>
<tbody>

<?php
for($counter=0; $counter<count($streamQue); $counter++)
{
	$srNo = $counter+1;
	$stream_or_subject_name = stripslashes($streamQue[$counter]->subject_name);
	if(($counter%2)==0)
	{
		$trClass="tdbggrey";
	}
	else
	{
		$trClass="tdbgwhite";
	}  ?>

	<tr class='<?php echo $trClass; ?>'>
		<td><?php echo $srNo; ?></td>
		<td><?php echo $stream_or_subject_name; ?></td>
		<td><?php echo number_format($streamQue[$counter]->beg); ?></td>
		<td><?php echo number_format($streamQue[$counter]->inter); ?></td>
		<td><?php echo number_format($streamQue[$counter]->high); ?></td>
		<td><?php echo number_format($streamQue[$counter]->question); ?></td>
	</tr> <?php
}
?>

<tr class="<?= $trClass; ?>">
	<td colspan="2" align="right"><strong>Total Questions in Bank:</strong></td>
	<td><strong><?php if($totalQue[0]->totalbeg) echo number_format($totalQue[0]->totalbeg); else echo 0; ?></strong></td>
	<td><strong><?php if($totalQue[0]->totalinter) echo number_format($totalQue[0]->totalinter); else echo 0; ?></strong></td>
	<td><strong><?php if($totalQue[0]->totalhigh) echo number_format($totalQue[0]->totalhigh); else echo 0; ?></strong></td>
	<td><strong><?php if($totalQue[0]->que) echo number_format($totalQue[0]->que); else echo 0; ?></strong></td>
</tr>
</tbody>
</table>
</div>
</fieldset> <?php

}
?>

  			<!--</form>-->
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>

	</table>

</div><!--Outer wrapper closed-->

</body>
</html>
<script language="javascript" type="text/javascript">

FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf", 'test-chart', 430, 300, 0, 1);
myChart.setXMLData("<?php echo $marksXml; ?>");
myChart.render('test-graph');

FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/Pie3D.swf", 'time-chart', 430, 300, 0, 1);
myChart.setXMLData("<?php echo $timeTrackXml; ?>");
myChart.render('candidate-graph');

FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/MSColumn3D.swf", 'chart', 900, 400, 0, 1);
myChart.setXMLData("<?php echo $graphXml; ?>");
myChart.render('question-graph');


</script>