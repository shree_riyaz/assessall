<?php
/*****************Developed by :- Chirayu Bansal
 * Date         :- 18-aug-2011
 * Module       :- Subject master
 * Purpose      :- Template for Edit particular Subject
 ***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Online Examination - Subject Master</title>

    <style type="text/css">
        #result > span {
            color: green;
            font-weight: bold;
            line-height: 23px;
            margin-left: 1%;
        }
    </style>
</head>
<body onload="document.editfrm.package_name.focus();">
<?php
include_once(ROOT . "/incajax.php");
$xajax->printJavascript();
?>
<div id="outerwrapper">
    <table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
        <tr>
            <td>
                <?php
                include_once(CURRENTTEMP . "/" . "header.php");
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <div id="content">
                    <?php include_once(CURRENTTEMP . "/" . "navigation.php"); ?>
                    <div id="main">
                        <div id="contents">
                            <form id="editfrm" name="editfrm" method="post" action="">
                                <fieldset class="rounded" style="width: 50%;">
                                    <legend>Edit Subject</legend>
                                    <br/>
                                    <?php
                                    if (isset($_SESSION['error'])) {
                                        echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="85%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
                                        echo $_SESSION['error'];
                                        echo '</td></tr></tbody></table>';
                                        unset($_SESSION['error']);
                                    }

                                    if (isset($_SESSION['success'])) {
                                        echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
                                        echo $_SESSION['success'];
                                        echo '</td></tr></tbody></table>';
                                        unset($_SESSION['success']);
                                    }
                                    ?>
                                    <!------------------------------------------Riyaz code start------------------------------------------------------------------>

                                    <table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">

                                        <tr>
                                            <td colspan="4">
                                                <div id="mandatory" align="left">
                                                    &nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="170">
                                                <div align="right">Subject Name:<span class="red">*</span></div>
                                            </td>
                                            <td width="200" colspan="2">
                                                <input name="package_name" id="package_name" type="text" maxlength="50"
                                                       class="rounded textfield"
                                                       value="<?php if ($frmdata['package_name']) echo htmlentities(stripslashes($frmdata['package_name'])); else echo htmlentities(stripslashes($package_details_by_joining[0]->package_name)); ?>"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="170" align="right">Select Exam:<span class="red">*</span></td>
                                            <?php
//                                            echo '<pre>';
                                            $selected_exam_ids = [];
                                            //                            print_r($package_details_by_joining);
                                            foreach ($package_details_by_joining as $key => $package_details_by_joining_lists) {
                                                foreach ($package_details_by_joining_lists as $k => $package_details_val) {
                                                    if ($k == 'exam_id')
                                                        $selected_exam_ids[] = $package_details_val;
                                                }
                                            }
                                            ?>
                                            <td colspan="2" align="left" width="200">
                                                <select style="width: 52%;" name="exam_id[]" id="package_exam_id"
                                                        multiple class="rounded">
                                                    <?php
                                                    $count_array = [];
                                                    foreach ($examList as $examLists) {

//                                    foreach($package_details_by_joining as $package_details_by_joining_lists ) {

                                                        if (in_array($examLists->exam_id, $selected_exam_ids)) {
                                                            $selected = 'selected';
                                                        } else {
                                                            $selected = '';
                                                        }
//                                   if (in_array($examLists->id,$selected_exam_ids))    {
                                                        // $count_array[] = $examLists->id;

                                                        ?>
                                                        <option <?php echo $selected; ?>
                                                                value="<?php echo $examLists->exam_id; ?>"><?php echo $examLists->exam_name; ?>
                                                        </option>

                                                        <?php
//                                }
//                                }
                                                    }

                                                    ?>
                                                </select>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="right" width="170">Test Details:<span class="red">*</span></td>
                                            <td id="cleanall">
                                                <div id="result">
                                                </div>
                                                <?php
                                                $i = 1;
                                                foreach ($package_details_by_joining as $package_details_by_joining_lists) {
                                                    if ($package_details_by_joining_lists->exam_id == '') {
                                                        echo 'No Option selected';
                                                    } else {
                                                        ?><?php

                                                        ?>

                                                        <div class="results">
                                                        <?php
                                                        $id = $package_details_by_joining_lists->exam_id;

                                                        $get_course_name_by_its_id = $package_details_by_joining_lists->exam_id;
                                                        $query = "select ex.exam_name from examination ex WHERE id in (" . $get_course_name_by_its_id . ")";

                                                        $get_course_name = $DB->RunSelectQuery($query);

                                                        $count = 0;

                                                        ?>
                                                        <span style="width:70%;color: green; font-weight: bold"
                                                              id="selected_exam<?php echo $i + 1; ?>"> <?php echo $get_course_name[$count]->exam_name; ?> </span>
                                                        </br>

                                                        <select onchange="sum(<?php echo $i; ?>)" style='width: 33%' onkeyup='sum(<?php echo $i; ?>);'
                                                                name="number_of_paper[<?php echo $id ?>]"
                                                                id="number_of_paper_<?php echo $i; ?>"/>
                                                        <?php
                                                        $query = "select COUNT(exam_id) as total, exam_id from test t where t.exam_id = " . $package_details_by_joining_lists->exam_id;
                                                        $get_count_course = $DB->RunSelectQuery($query);
                                                        for ($j = 1; $j <= $get_count_course[0]->total; $j++) {
                                                            ?>
                                                            <option <?php if ($package_details_by_joining_lists->number_of_paper == $j) { ?> selected <?php } ?>
                                                                    value="<?php echo $j; ?>"> <?php echo $j; ?>
                                                            </option>
                                                        <?php } ?>
                                                        </select>
                                                        <input onkeypress="return isNumberKey(event)" style='width: 33%'
                                                               type="text" onkeyup='sum(<?php echo $i; ?>);'
                                                               name="price_per_paper[]"
                                                               id="price_per_paper_<?php echo $i; ?>"
                                                               value="<?php echo $package_details_by_joining_lists->price_per_paper; ?>"/>
                                                        <input readonly style='width: 12%' type="text" name="total[]"
                                                               id="price_<?php echo $i; ?>"
                                                               value="<?php echo $package_details_by_joining_lists->total; ?>"/>
                                                    <?php }
//                                }
                                                    ?>
                                                    </div>
                                                    <?php
                                                    $i++;
//                                    }
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="170" align="right">Sub Total:<span class="red">*</span></td>
                                            <td colspan="2" width="200" align="left">
                                                <input name="total_price" readonly id="total_price" type="text"
                                                       style="width: 39%" class="rounded textfield"
                                                       value="<?php if ($frmdata['package_name']) echo htmlentities(stripslashes($frmdata['total_price'])); else echo htmlentities(stripslashes($package_details_by_joining[0]->total_price)); ?>"
                                                       onchange=""/></td>
                                        </tr>
                                        <tr>
                                            <td width="170" align="right">Special Price:</td>
                                            <td colspan="2" width="200" align="left">
                                                <input onkeypress='return isNumberKey(event)' name="special_price"
                                                       id="special_price" type="text" style="width: 39%"
                                                       class="rounded textfield"
                                                       value="<?php if ($frmdata['package_name']) echo htmlentities(stripslashes($frmdata['special_price'])); else echo htmlentities(stripslashes($package_details_by_joining[0]->special_price)); ?>"
                                                       onchange=""/></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td>&nbsp;</td>
                                            <td colspan="3">
                                                <div align="left">
                                                    <input type="submit" name="editpackage" id="editpackage"
                                                           value="Update" class="buttons rounded"/>
                                                    <input type="button" name="cancel" value="Cancel" class="buttons"
                                                           onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=package&do=manage'"/>
                                                </div>
                                            </td>
                                        </tr>

                                    </table>

                                </fieldset>
                                <br/>


                            </form>

                        </div><!--Div Contents closed-->
                    </div><!--Div main closed-->

                </div><!--Content div closed-->
            </td>
        </tr>

    </table>

</div><!--Outer wrapper closed-->

<script type="text/javascript">

    function getNumberOfTest() {
        var element = $('#package_exam_id :selected');
        var multipleValues = [];
        element.each(function (i, selected) {
            multipleValues[i] = $(selected).val();
        });
        $.ajax({
            type: "POST",
            url: "http://localhost/assessall/ajax.php",
            //url: "http://assessall.com/assessall/ajax.php",
            datatype: "json",
            data: {"exam_id": multipleValues},
            cache: false,
            success: function (data) {

                var jsonData = $.parseJSON(data);
                $(jsonData).each(function (i, e) {
                    var option = getOptions(e.total);
//                    console.log(option);
//                    console.log($('select[name="number_of_paper['+e.exam_id+']"]'));
                    $('select[name="number_of_paper[' + e.exam_id + ']"]').html(option);
                });
            }
        });

//    });
    }
    function getOptions(limit) {
        var html = '';
        for (var i = 1; i <= limit; i++) {
            html += "<option value='" + i + "'>" + i + "</option>";
        }
        return html;
    }
</script>
<script type="text/javascript">
    $(function () {
        $(document).ready(function () {

            $('#package_exam_id').change(function () {
                getNumberOfTest()
            })

        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#package_exam_id").change(function () {

            var elems = document.getElementsByClassName('results'), i;
            for (i in elems) {
                elems[i].innerHTML = '';
            }
            var multipleValues = $("#package_exam_id").val() || "";
            var result = "";
            if (multipleValues != "") {
                var aVal = multipleValues.toString().split(",");
                var count = $("#package_exam_id :selected").length;
                $.each(aVal, function (i, value) {

                    result += "<span  style='width: 70%' id='selected_exam" + (parseInt(i) + 1) + "' >" + $("#package_exam_id").find("option[value=" + value + "]").text().trim() + " </span> </br>";

//                    result += "<input onkeypress='return isNumberKey(event)' autocomplete='off' onkeyup='sum("+ (parseInt(i) + 1) +");' style='width: 35%' required='required' type='text' id='number_of_paper_"+ (parseInt(i) + 1) +"' name='number_of_paper[" + value.trim() + "]' placeholder='Number of tests' value=''>";

                    result += "<select onchange='sum("+ (parseInt(i) + 1) +")' onkeypress='return isNumberKey(event)' onkeyup='sum(" + (parseInt(i) + 1) + ");' style='width: 35%' id='number_of_paper_" + (parseInt(i) + 1) + "' name='number_of_paper[" + value.trim() + "]' > <!--<option> 123 </option>--> </select>";

                    result += "<input onkeypress='return isNumberKey(event)' autocomplete='off' onkeyup='sum(" + (parseInt(i) + 1) + ");' style='width: 33%' required='required' type='text' id='price_per_paper_" + (parseInt(i) + 1) + "' name='price_per_paper[]' placeholder='Price per tests' value=''>";
                    result += "<input autocomplete='off' readonly style='width: 12%' required='required' type='text' id='price_" + (parseInt(i) + 1) + "' name='total[]' placeholder='Total' value='' >"; //result should display in this textbox .i.e (result= total_price/count )
                    result += "</br>";
                });
            }
            //Set Result multipleValues
            $("#result").html(result);

        });

    });
</script>

<script type="text/javascript">

    function sum(idCounter) {
//        alert('dd');
        var number_of_paper = document.getElementById('number_of_paper_' + idCounter).value;
        var price_per_paper = document.getElementById('price_per_paper_' + idCounter).value;
        if (number_of_paper === '') {
            number_of_paper = 0;
        }
        if (price_per_paper === '') {
            price_per_paper = 0;
        }

        var tot = 0;
        var total_value = document.getElementById('price_' + idCounter).value = parseInt(price_per_paper) * parseInt(number_of_paper);

//        for (var i = 1; i <= idCounter; i++) {
        for (var i = 1; i <= $("#package_exam_id :selected").length; i++) {
            tot = parseInt(tot) + parseInt(document.getElementById('price_' + i).value);
        }
        document.getElementById('total_price').value = tot;
    }
</script>
<script type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>
<!------------------------------------------Riyaz code end------------------------------------------------------------------>

</body>
</html>
