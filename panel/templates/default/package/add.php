<?php
/*****************Developed by :- Chirayu Bansal
 * Date         :- 18-aug-2011
 * Module       :- Subject master
 * Purpose      :- Template for Add particular Subject
 **********************************************************************************
 * http://codebins.com/bin/4ldqp7a# */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Add Package</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <style>
        .spanmsg {
            position: absolute;
            text-align: center;
            color: red;
        }

        #result > span {
            color: green;
            font-weight: bold;
            line-height: 23px;
            margin-left: 1%;
        }
    </style>
</head>

<body onload="document.addfrm.package_name.focus();">
<?php
include_once(ROOT . "/incajax.php");
$xajax->printJavascript();

?>
<div id="outerwrapper">
    <!--    ------------------------------------------------Riyaz code start----------------------------------------------------->

    <table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
        <tr>
            <td>
                <?php
                include_once(CURRENTTEMP . "/" . "header.php");
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <div id="content">
                    <?php include_once(CURRENTTEMP . "/" . "navigation.php"); ?>
                    <div id="main">
                        <div id="contents">
                            <form id="addfrm" name="addfrm" method="post" action="">
                                <fieldset class="rounded" style="width: 50%;">
                                    <legend><?php echo $isEdit ? 'Edit' : 'Create'; ?> Package</legend>
                                    <br/>

                                    <?php
                                    if (isset($_SESSION['error'])) {
                                        echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="85%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
                                        echo $_SESSION['error'];
                                        echo '</td></tr></tbody></table>';
                                        unset($_SESSION['error']);
                                    }

                                    if (isset($_SESSION['success'])) {
                                        echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
                                        echo $_SESSION['success'];
                                        echo '</td></tr></tbody></table>';
                                        unset($_SESSION['success']);
                                    }
                                    ?>
                                    <table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">

                                        <tr>
                                            <td colspan="4">
                                                <div id="mandatory" align="left">
                                                    &nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="170" align="right">Package Name:<span class="red">*</span></td>
                                            <td colspan="2" width="200" align="left"><input name="package_name"
                                                                                            id="package_name"
                                                                                            type="text" maxlength="50"
                                                                                            class="rounded textfield"
                                                                                            value="<?php echo htmlentities(stripslashes($frmdata['package_name'])); ?>"
                                                                                            onchange=""/></td>
                                        </tr>
                                        <?php
//                                        echo '<pre>';
                                        ?>
                                        <tr>
                                            <td width="170" align="right">Select Exam:<span class="red">*</span></td>
                                            <td colspan="2" align="left" width="200">
                                                <select style="width: 52%;" name="exam_id[]"  id="package_exam_id"
                                                        multiple size="5" class="rounded">
                                                    <?php
                                                    if (is_array($examList)) {
                                                        for ($counter = 0; $counter < count($examList); $counter++) {
                                                            $selected = '';
                                                            $selected = in_array($examList[$counter]->exam_id, $_SESSION['exam_id']) ? ' selected="selected"' : '';
                                                            $total = $test = $price = 0;

                                                            $test = isset($_SESSION['allformdata']['number_of_paper'][$examList[$counter]->exam_id]) ? $_SESSION['allformdata']['number_of_paper'][$examList[$counter]->exam_id] : '';
                                                            $price = isset($_SESSION['allformdata']['price_per_paper'][$examList[$counter]->exam_id]) ? $_SESSION['allformdata']['price_per_paper'][$examList[$counter]->exam_id] : '';
                                                            $total = isset($_SESSION['allformdata']['total'][$examList[$counter]->exam_id]) ? $_SESSION['allformdata']['total'][$examList[$counter]->exam_id] : '';

                                                            echo '<option value="' . $examList[$counter]->exam_id . '" ' . $selected . ' data-total="' . $total . '" data-test="' . $test . '" data-price="' . $price . '">' . stripslashes($examList[$counter]->exam_name) . '</option>';
                                                        }
                                                    }
                                                    ?>

                                            </td>
                                        </tr>
                                        <!--                                                    Session-->
                                        <?php
//                                        echo '<pre>';
                                        //                                        print_r($_SESSION['exam_id'])
                                        //                                        print_r($frmdata);
                                        //                                        print_r(array_values($frmdata['number_of_paper']));

                                        ?>
                                        <?php if (isset($_SESSION['exam_id']) && count($_SESSION['exam_id']) > 1) {
//                        unset($_SESSION['exam_id']);
                                            ?>
                                            <tr>
                                                <td id="test_detail" width="170" align="right">Test Details:<span
                                                            class="red">*</span></td>
                                                <td>
                                                    <div id="result">
                                                        <?php
                                                        for ($i = 0; $i < count($frmdata['exam_id']); $i++) {
                                                            ?>
                                                            <span style='width:70%'
                                                                  id="selected_exam<?php echo $i + 1; ?>"> <?php echo $frmdata['price_per_paper'][$i]; ?> </span>
                                                            </br>

<input style="width: 35%" onkeypress="return isNumberKey(event)" onkeyup="sum(<?php echo $i + 1; ?>);" required="required" id="number_of_paper_<?php echo $i + 1; ?>" name="number_of_paper[<?php echo array_keys($frmdata['number_of_paper'])[$i]; ?>]" value="<?php echo array_values($frmdata['number_of_paper'])[$i]; ?>" placeholder="Number of tests" type="text">
<input style="width: 30%" onkeypress="return isNumberKey(event)" onkeyup="sum(<?php echo $i + 1; ?>);" required="required" id="price_per_paper_<?php echo $i + 1; ?>" name="price_per_paper[]" value="<?php echo $frmdata['price_per_paper'][$i]; ?>" placeholder="Price per tests" type="text">
<input readonly="" style="width: 12%" required="required" id="price_<?php echo $i + 1; ?>" name="total[]" class="price"
                                                                   value="<?php echo $frmdata['total'][$i]; ?>"
                                                                   placeholder="Total" type="text">
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td id="test_details" width="170" align="right">Test Details:<span
                                                            class="red">*</span></td>
                                                <td>
                                                    <div id="result"></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <!--                                                    Session-->

                                        <tr>
                                            <td width="170" align="right">Total Price:<span class="red">*</span></td>
                                            <td colspan="2" width="200" align="left">
                                                <input name="total_price" id="total_price" type="text"
                                                       style="width: 39%" class="rounded textfield"
                                                       value="<?php echo htmlentities(stripslashes($frmdata['total_price'])); ?>"
                                                       onchange=""/></td>
                                        </tr>
                                        <tr>
                                            <td width="170" align="right">Special Price:</td>
                                            <td colspan="2" width="200" align="left">
                                                <input name="special_price" id="special_price" type="text"
                                                       style="width: 39%" class="rounded textfield"
                                                       value="<?php echo htmlentities(stripslashes($frmdata['special_price'])); ?>"
                                                       onchange=""/></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td colspan="3">
                                                <div align="left">
                                                    <input type="submit" name="addpackage" id="addpackage"
                                                           value="Add Package" class="buttons rounded"/>
                                                    <input type="button" name="cancel" value="Cancel" class="buttons"
                                                           onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=package&do=manage'"/>
                                                </div>
                                            </td>
                                        </tr>

                                    </table>
                                </fieldset>
                                <br/>


                            </form>

                        </div><!--Div Contents closed-->
                    </div><!--Div main closed-->

                </div><!--Content div closed-->
            </td>
        </tr>

    </table>
    <?php
    //    echo '<pre>';
    //    print_r($_POST);
    //    exit();

    if (isset($_SESSION['allformdata'])) {
        unset($_SESSION['allformdata']);
    }
    if (isset($_SESSION['exam_id'])) {
        unset($_SESSION['exam_id']);
    }
    ?>

</div><!--Outer wrapper closed-->

<script type="text/javascript">
    
    
    function getNumberOfTest() {
        var element = $('#package_exam_id :selected');
        var multipleValues = [];
        element.each(function(i, selected){
            multipleValues[i] = $(selected).val();
        });
        $.ajax({
            type: "POST",
            url: "http://localhost/assessall/ajax.php",
//            url: "http://assessall.com/assessall/ajax.php",
            datatype: "json",
            data: {"exam_id":multipleValues},
            cache: false,
            success: function(data) {

                var jsonData = $.parseJSON(data);
                $(jsonData).each(function (i,e) {
                    var option = getOptions(e.total);
//                    console.log(option);
//                    console.log($('select[name="number_of_paper['+e.exam_id+']"]'));
                   $('select[name="number_of_paper['+e.exam_id+']"]').html(option);
                });
            }
        });

//    });
    }

    function getOptions(limit) {
        var html = '';
        for(var i=1;i<=limit;i++){
            html += "<option value='"+i+"'>"+i+"</option>";
        }
    return html;
    }
</script>
<script type="text/javascript">
    $(function () {
        $(document).ready(function () {
            
            $('#package_exam_id').change(function () {
                getNumberOfTest()
            })

            getExamTextBox();
            $("#package_exam_id").change(function () {
//            $('#package_exam_id').children('option:enabled').eq(0).prop('selected',true);

                getExamTextBox();

            });

            $("#validate").click(function () {
                var multipleValues = $("#package_exam_id").val() || "";
                var total_price = $("#toal_price").val();
                var aVal = multipleValues.toString().split(",");
                var count = $("#package_exam_id :selected").length;
                var chk_total = 0;
                if (multipleValues != "") {
                    $.each(aVal, function (i, value) {
                        chk_total += parseInt($("input[name=option" + (parseInt(i) + 1) + "]").val());
                    });
                }
                if (total_price != chk_total) {
                    alert('Invalid Entry');
                    return false;
                } else {
                    alert('Valid Entry');
                }

            });


        });
    });

    function getExamTextBox() {
        var multipleValues = $("#package_exam_id").val() || "";
//        alert(multipleValues);

        var result = "";

        if (multipleValues != "") {

            var aVal = multipleValues.toString().split(",");

            var count = $("#package_exam_id :selected").length;

            $.each(aVal, function (i, value) {

                var option = $("#package_exam_id option[value='" + value + "']");
                var test = option.attr('data-test');
                var price = option.attr('data-price');
                var total = option.attr('data-total');


                result += "<span style='width: 70%'  id='selected_exam" + (parseInt(i) + 1) + "' >" + $("#package_exam_id").find("option[value=" + value + "]").text().trim() + " </span> </br>";

//                result += "<input onkeypress='return isNumberKey(event)' style='width: 35%' autocomplete='off' onkeyup='sum(" + (parseInt(i) + 1) + ");' required='required' type='text' id='number_of_paper_" + (parseInt(i) + 1) + "' name='number_of_paper[" + value.trim() + "]' value='"+test+"' placeholder='Number of tests'>";
                result += "<select onchange='sum("+ (parseInt(i) + 1) +")' id='number_of_paper_" + (parseInt(i) + 1) + "' name='number_of_paper[" + value.trim() + "]'> <!--<option>Select test</option>--> <option value="+test+">" +test+ "</option> </select>";

                result += "<input onkeypress='return isNumberKey(event)' style='width: 35%' autocomplete='off' onkeyup='sum(" + (parseInt(i) + 1) + ");' required='required' type='text' id='price_per_paper_" + (parseInt(i) + 1) + "' name='price_per_paper[" + value.trim() + "]' value='" + price + "' placeholder='Price per test'>";
                result += "<input readonly style='width: 12%' autocomplete='off' required='required' type='text' id='price_" + (parseInt(i) + 1) + "' name='total[" + value.trim() + "]' class='price' value='" + total + "' placeholder='Total' >"; //result should display in this textbox .i.e (result= total_price/count )
                result += "</br>";

            });
        }
        //Set Result multipleValues
        $("#result").html(result);
    }
</script>
<script type="text/javascript">
    function sum(idCounter) {
//        alert('riyaz');
        var number_of_paper = document.getElementById('number_of_paper_' + idCounter).value;
        var price_per_paper = document.getElementById('price_per_paper_' + idCounter).value;
        if (number_of_paper === '') {
            number_of_paper = 0;
        }
        if (price_per_paper === '') {
            price_per_paper = 0;
        }

        var tot = 0;
        var total_value = document.getElementById('price_' + idCounter).value = parseInt(price_per_paper) * parseInt(number_of_paper);

//        for (var i = 1; i <= idCounter; i++) {
        for (var i = 1; i <= $("#package_exam_id :selected").length; i++) {
            tot = parseInt(tot) + parseInt(document.getElementById('price_' + i).value);
            document.getElementById('total_price').value = tot;
        }
    }
</script>
<script type="text/javascript">
    function final_price(total_value) {
        document.getElementById('total_price').value = total_value;
    }
</script>
<script type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>
<!----------------------------------------------------Riyaz code end----------------------------------------------------->

</body>
</html>
