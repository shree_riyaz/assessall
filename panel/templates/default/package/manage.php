<?php
/*****************Developed by :- Chirayu Bansal
Date         :- 18-aug-2011
Module       :- Subject master
Purpose      :- Template for Browse all Subject details and provide functionality to edit and delete
 ***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Online Examination - Manage Package Details</title>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>
<body onload="document.frmlist.package_name.focus();">

<div id="outerwrapper">
    <table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
        <tr>
            <td>
                <?php
                include_once(CURRENTTEMP."/"."header.php"); ?>
            </td>
        </tr>
        <tr>
            <td>
                <div id="content">
                    <?php include_once(CURRENTTEMP."/"."navigation.php"); ?>

                    <div id="main">
                        <div id="contents">
                            <form action="" method="post" name="frmlist" id="frmlist">
                                <legend>Package Details</legend>
                                <?php
                                // Show particular Messages
                                if(isset($_SESSION['error']))
                                {
                                    echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
                                    echo $_SESSION['error'];
                                    echo '</td></tr></tbody></table>';
                                    unset($_SESSION['error']);
                                }
                                if(isset($_SESSION['success']))
                                {
                                    echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
                                    echo $_SESSION['success'];
                                    echo '</td></tr></tbody></table>';
                                    unset($_SESSION['success']);
                                }

                                ?>
<!--------------------------------------------Riyaz code start----------------------------------------------------------------->

                                <div style="width: 70%;margin: 10px auto 0;">
                                    <?php
//                                    var_dump($frmdata);
                                    if($packageList)
                                    {
                                        ?>
                                        <table border="0" cellspacing="1" cellpadding="4" width="100%" style="">
                                            <tr>
                                                <td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($packageList))." of ".$totalCount; ?></td>
                                                <td align="right"><?php echo getPageRecords();?> </td>
                                            </tr>
                                        </table>
                                        <?php
                                    }
                                    ?>
                                    <div id="celebs">
                                        <table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
                                            <?php
                                            if($packageList)
                                            {
                                            ?>	  <thead>
                                            <tr class="tblheading">
                                                <td width="1%">#</td>
                                                <td width="8%">
                                                    <a style="cursor:pointer;color:#af5403;" onClick="OrderPage('package_name');">Package Name
                                                        <?php if($frmdata['orderby'] =='package_name')
                                                        {
                                                            print '<img src="'.ROOTURL.'/images/asc.gif">';
                                                            $frmdata['orderby']='package_name';
                                                        }
                                                        elseif($frmdata['orderby'] =='package_name desc')
                                                        {
                                                            print '<img src="'.ROOTURL.'/images/desc.gif">';
                                                            $frmdata['orderby']='package_name';
                                                        }
                                                        ?>
                                                    </a>
                                                </td>
                                                <td width="2%">Price</td>
                                                <td width="7%">Actions</td>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php
                                            $srNo=$frmdata['from'];
                                            $count=count($packageList);

                                            for($counter=0;$counter<$count;$counter++)
                                            {
                                                $srNo=$srNo+1;
                                                if(($counter%2)==0)
                                                {
                                                    $trClass="tdbggrey";
                                                }
                                                else
                                                {
                                                    $trClass="tdbgwhite";
                                                }
                                                ?>
                                                <tr class='<?= $trClass; ?>'>
                                                    <td><?php echo $srNo; ?></td>
                                                    <td><?php echo stripslashes(highlightSubString($frmdata['subject_name_manage'], $packageList[$counter]->package_name)); ?></td>
                                                    <td><?php echo stripslashes(highlightSubString($frmdata['subject_price_manage'], $packageList[$counter]->special_price ? $packageList[$counter]->special_price : $packageList[$counter]->total_price)); ?></td>

                                                    <td>
                                                        <a href="<?php echo CreateURL('index.php','mod=package&do=edit&nameID='.$packageList[$counter]->id); ?>" title='Edit'>
                                                            <img src="<?=IMAGEURL ?>/b_edit.png" />
                                                        </a>&nbsp;&nbsp;
                                                        <a style="cursor: pointer;" title='Delete'>
                                                            <img src="<?=IMAGEURL ?>/b_drop.png" onclick="xajax_deletePermission(<?php echo $packageList[$counter]->id; ?>,'package');" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            }
                                            else
                                            {
                                                echo "<tr><td colspan='7' align='center'>(0) Record found.</td></tr>";
                                            }
                                            ?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
                                        <tr>
                                            <?php
                                            if($packageList)
                                            {
                                                ?>
                                                <td colspan="2"><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?></td>
                                            <?php }?>
                                            <td align="right">
                                                <?php PaginationDisplay($totalCount);	?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

<!--------------------------------------------Riyaz code end----------------------------------------------------------------->

                                <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
                                <input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
                                <input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />
                            </form>
                        </div><!--Div Contents closed-->
                    </div><!--Div main closed-->

                </div><!--Content div closed-->
            </td>
        </tr>

    </table>

</div><!--Outer wrapper closed-->

</body>
</html>
