<?php 
/**
 * 	@author	:	Ashwini Agarwal
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Parent Details</title>

<script language="javascript" type="text/javascript">
function deleteParent(val)
{
	var x = confirm("Are you sure you want to delete this parent?");
    if (x)
    { 
        //return true;
		//var val=document.getElementById('questionId').value;
		//alert(host);
		load("<?php echo ROOTURL; ?>" + "/index.php?mod=parent_master&do=delete&nameID="+val);
    }
    else
    { 
		return false;
    }
}

function changeAuthority(id, is_disabled)
{
	var confirm = 1;

/*	if(is_disabled == 1)
	{
		if(confirm("Do you really want to enable this parent?"))
		{
			confirm = 1;
		}
	}
	else
	{
		if(confirm("Do you really want to disable this parent?"))
		{
			confirm = 1;
		}
	}
*/
	if(confirm == 1)
	{
		xajax_changeParentAuthority(id);
	}
}

</script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.name.focus();">

<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr>
	<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
</tr>
<tr>
	<td>
		<div id="content">
		<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
		<div id="main">
			<div id="contents">
				<form action="" method="post" name="frmlist" id="frmlist">
					<legend>Parent Details</legend>
					<?php 
					// Show particular Messages
					if(isset($_SESSION['error']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
						echo $_SESSION['error'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['error']);
					}
					if(isset($_SESSION['success']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
						echo $_SESSION['success'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['success']);
					}
					?>


<div class="search-div">
<fieldset class="rounded search-fieldset"><legend>Search</legend>
<table border="0" cellspacing="1" cellpadding="3" width="">
<tr>
    <td>Name :</td>
      <td>
      	<input type="text" size="20" class="rounded textfield" name="name" id="name" value="<?php if(isset($frmdata['name'])) echo $frmdata['name']; ?>" onchange="managePageCheckIsChar(this.value,this.id, 'name');" />
      </td>
</tr>

<tr>
	<td colspan="6" align="center">
      	<input type="submit" class="buttons rounded" name="Submit" value="Show" />
      	<input type="submit" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick=""/>
    </td>
</tr>
</table>
</fieldset>
</div>
		
<?php if($parentlist) {	?>

<table border="0" cellspacing="0" cellpadding="4" width="100%" style="" align="right">
	<tr>
		<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($parentlist))." of ".$totalCount; ?></td>
		<td align="right"><?php echo getPageRecords();?> </td>
	</tr>
</table><br />

<?php } ?>

<br />
<div id="celebs">
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">

<?php if($parentlist) { ?>	  

<thead>
	<tr class="tblheading">
	<td width="1%">#</td>
	<td width="31%">
		<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('first_name');">Name
		<?php 
		if($frmdata['orderby']=='first_name') 
		{
			print '<img src="'.ROOTURL.'/images/asc.gif">';
		} 
		elseif($frmdata['orderby']=='first_name desc')
		{
			print '<img src="'.ROOTURL.'/images/desc.gif">';
		}
		?>
		</a>
	</td>

	<td width="10%">Actions</td>
	</tr>
</thead>

<tbody>

<?php 
$srNo = $frmdata['from'];
$count = count($parentlist);

foreach($parentlist as $parent) 
{ 
	$id=$parent->id;
	$fname=ucfirst(strtolower(stripslashes($parent->first_name)));
	$lname=ucfirst(strtolower(stripslashes($parent->last_name)));
	$name=$fname.' '.$lname;
	
	$srNo = $srNo+1;
	if(($srNo%2)==0)
	{
		$trClass="tdbggrey";
	}
	else
	{
		$trClass="tdbgwhite";
	} ?>

<tr class='<?php echo $trClass; ?>'>
	<td><?php echo $srNo; ?></td>
	<td><?php echo highlightSubString($frmdata['name'], $name); ?></td>
	<td>
		<?php 
		if($parent->is_disabled == 0)
		{
			$a_title = 'Disable Parent';
			$a_image = ROOTURL . '/images/disable.png';
		}
		else
		{
			$a_title = 'Enable Parent';
			$a_image = ROOTURL . '/images/enable.png';
		}
		?>
		
		<a href="<?php echo CreateURL('index.php','mod=parent_master&do=edit&nameID='.$id); ?>" title='Edit'><img src="<?php echo IMAGEURL; ?>/b_edit.png" /></a>&nbsp;
									
		<img 
			style="cursor: pointer;"
			id="auth_<?php echo $id; ?>"
			title='<?php echo $a_title; ?>' 
			src="<?php echo $a_image; ?>" 
			onclick="changeAuthority('<?php echo $id; ?>', '<?php echo $parent->is_disabled; ?>');" />&nbsp;
									
		<a href='javascript: void(0);' title='Delete'><img src="<?php echo IMAGEURL; ?>/b_drop.png" onclick="deleteParent('<?php echo $id; ?>');" /></a>
	</td>
</tr>
						
<?php } } else	{ ?>

<tr><td colspan='10' align='center'>(0) Record found.</td></tr>

<?php }	?>
				  
</tbody>
</table>
</div>

<?php if($parentlist) { ?>

<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
<tr>
	<td colspan="2">
		<?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?>
	</td>
	<td align="right">
		<?php PaginationDisplay($totalCount);	?>
	</td>
</tr>
</table>

<?php } ?>

		<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
		<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
		<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />
  			
				</form>
			</div><!--Div Contents closed-->
		</div><!--Div main closed-->
		</div><!--Content div closed-->
	</td>
</tr>
</table>
</div><!--Outer wrapper closed-->
</body>
</html>