<?php 
/**
 * 	@author	:	Ashwini Agarwal
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Online Examination - Parent Master</title>
	
<script>
function generatePassword()
{
	var length = 6;
	charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	retVal = "";
	for (var i = 0, n = charset.length; i < length; ++i) 
	{
		retVal += charset.charAt(Math.floor(Math.random() * n));
	}
	return retVal;
}

function getPass()
{
	chk = document.getElementById('passgen');
	if(chk.checked != '')
	{
		var pass = generatePassword();
		$('#showPass').html(pass);
		$('#auto_pass').val(pass);
		$('.password').val(pass);
		$('#showPass').show();
	}
	else
	{
		$('#showPass').html('');
		$('.password').val('');
		$('#auto_pass').val('');
		$('#showPass').hide();
	}
}
function addCandidate()
{
	var candidate = $('#candidate_id');
	
	if(candidate.val() == '')
	{
		alert('Please select student.');
		candidate.focus();
		return false;
	}

	xajax_addParentCandidate(candidate.val());
}
function finishCandidateAddition()
{
	var candidate = $('#candidate_id');
	candidate.val('');
	numbering();

    $('.ui-autocomplete-input').val('');
    $('.ui-autocomplete-input').autocomplete('close');
}

function numbering()
{
	var counter = 1;
	$('.pc-sno').each(function(){
		$(this).html(counter++);
	});

	if(counter == 1)
	{
		$('#parent-candidate').hide();
	}
	else
	{
		$('#parent-candidate').show();
	}
}

function deleteCandidate(id)
{
	xajax_addParentCandidate('', id);
	$('tr#pc-'+id).remove();
	numbering();
}

$(document).ready(function(){
	numbering();
});
</script>
</head>

<body onload="document.addparent.first_name.focus();">

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>

<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr>
	<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
</tr>
<tr>
	<td>
		<div id="content">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
			<div id="main">
				<div id="contents">
					<form id="addparent" name="addparent" method="post" action="" enctype="multipart/form-data">
						<fieldset class="rounded">
							<legend>Enter Parent Details</legend>
							<?php
								if(isset($_SESSION['error']))
								{
								echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
								echo $_SESSION['error'];
								echo '</td></tr></tbody></table>';
								unset($_SESSION['error']);
								}
								if(isset($_SESSION['success']))
								{
								echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
								echo $_SESSION['success'];
								echo '</td></tr></tbody></table>';
								unset($_SESSION['success']);
								}
							?>
							
<table width="90%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences" style="padding: 10px;">
<tr>
	<td colspan="4">
		<div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>
	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td width="120px">
		<div>First Name:<span class="red">*</span></div>
	</td>
	<td width="400px">
		<input name="first_name" id="first_name" type="text" class="rounded textfield" size="30" maxlength="100" value="<?php echo $frmdata['first_name']; ?>" onchange="isChar(this.value,this.id, 'first name');" />
	</td>
	<td width="120px">
		<div>Last Name:</div>
	</td>
	<td>
		<input name="last_name" id="last_name" type="text" class="rounded textfield" size="30" maxlength="100" value="<?php echo $frmdata['last_name']?>" onchange="isChar(this.value,this.id, 'last name');"/>
	</td>	   
</tr>

<tr>
	<td>
		<div>Contact No.:</div>
	</td>
	<td>
		<input name="contact_no" id="contact_no" type="text" class="rounded textfield" size="30" value="<?php echo $frmdata['contact_no']?>" onchange="checkContactNumber(this);" maxlength="20"/>
	</td> 
	<td>
		<div>Email:<span class="red">*</span></div>
	</td>
	<td>
		<input name="email" type="text" size="30" class="rounded textfield" value="<?php echo $frmdata['email']?>" onchange="CheckEmailId(this)"/>
	</td>
</tr>

<tr>
	<td>
		<div>Username:<span class="red">*</span></div>
	</td>
	<td>
		<input name="username" type="text" size="30" class="rounded textfield" value="<?php echo $frmdata['username']; ?>" />
	</td>
</tr>

<tr>
	<td>&nbsp;</td>
	
	<?php $display = 'display:none;'; if($frmdata['passgen']) $display = ''; ?>
	<td colspan="10">
		<input type="checkbox" <?php if($frmdata['passgen']) echo 'checked'; ?> name='passgen' onchange="getPass();" id='passgen'></input>
		<label for="passgen">Auto Generate Password</label>
		<span id='showPass' style="padding: 2px 5px; background-color: #ddd; <?php echo $display; ?>">
			<?php echo $frmdata['auto_pass']; ?>
		</span>
		<input type='hidden' name='auto_pass' id='auto_pass' value="<?php if($frmdata['passgen']) echo $frmdata['auto_pass']; ?>"/>
	</td>
</tr>

<tr>
	<td>
		<div>Password:<?php if(!$isEdit) { ?><span class="red">*</span><?php } ?></div>
	</td>
	<td>
		<input name="password" type="password" value="<?php if($frmdata['passgen']) echo $frmdata['auto_pass']; ?>" class="rounded password textfield" size="30" maxlength="25" onchange="checkPassword(this);"/>
	</td>
	<td nowrap="nowrap">
		<div>Confirm Password:<?php if(!$isEdit) { ?><span class="red">*</span><?php } ?></div>
	</td>
	<td>
		<input name="confirm_password" type="password" value="<?php if($frmdata['passgen']) echo $frmdata['auto_pass']; ?>" class="rounded password textfield" size="30" maxlength="25" onchange="checkPassword(this);"/>
	</td>
</tr> 

<tr>
	<td valign="top">
		<div>Address:</div>
	</td>
	<td>
		<textarea name="address" rows="5" cols="30" class="rounded"><?php echo $frmdata['address']; ?></textarea>
	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td>&nbsp;</td>
	<td colspan="3">
		<fieldset class="rounded">
			<legend>Add Students</legend>
				<table width="60%" align="center">
				<tr>
					<td width="80%">
						<span>Student:</span>&nbsp;&nbsp;
						<select class="rounded textfield combobox" name="candidate_id" id="candidate_id" >
							<option value="">Select Student</option>
							<?php 
							if(is_array($candidate))
							{
								for($counter=0;$counter<count($candidate);$counter++)
								{
									$name = stripslashes($candidate[$counter]->first_name.' '.$candidate[$counter]->last_name);
									$username = '('. stripslashes($candidate[$counter]->candidate_id).')';
									echo '<option value="'.$candidate[$counter]->id.'">'. $name.' '.$username .'</option>';
								}
							}
							?>
						</select>
					</td>
					<td width="20%">
						<input type="button" name="addcandidate" value="Add Student" class="buttons" onclick="addCandidate();" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<table border="0" cellspacing="1" cellpadding="4" id="parent-candidate" width="100%" bgcolor="#e1e1e1" class="data">
						<thead>
							<tr class="tblheading">
								<td>#</td>
								<td>Student Name</td>
								<td>Action</td>
							</tr>
						</thead>
						
							<?php $count=0; foreach($_SESSION['parent_candidate'] as $key => $parent_candidate) { ?>
							<tr class="tdbgwhite" id="pc-<?php echo $key; ?>">
								<td class="pc-sno"><?php ++$count; ?></td>
								<td><?php echo $parent_candidate['candidate_name']; ?></td>
								<td><a href="javascript:void(0);" onclick="deleteCandidate('<?php echo $key; ?>');">Delete</a></td>
							</tr>
							<?php } ?>
							
						</table>
					</td>
				</tr>
				</table>
		</fieldset>
	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td>&nbsp;</td>
	<td colspan="3">
	  <div align="left">
	  	<?php if($isEdit) { ?>
	  	
	    <input type="submit" name="editparent" value="Update" class="buttons rounded" />
	    
	  	<?php } else { ?>
	    
	    <input type="submit" name="addparent" value="Save" class="buttons rounded" />
	    
	  	<?php } ?>
		
		<input type="button" name="cancel" value="Cancel" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=parent_master&do=manage'"/>
	  </div>
	</td>
</tr>
</table>

						</fieldset><br />
					</form>
				</div><!--Div Contents closed-->
			</div><!--Div main closed-->
		</div><!--Content div closed-->
	</td>
</tr>
</table>	
</div><!--Outer wrapper closed-->

</body>
</html>