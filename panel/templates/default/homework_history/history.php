<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Homework History</title>

<script language="javascript" type="text/javascript">
$(document).ready(
function()
{
	$('#year').change(
	function()
	{
		if($(this).val() == '')
			$('#tr_month').hide();
		else
			$('#tr_month').show();
	});

	$('#year').trigger('change');
});
function check_search()
{
	var msg = '';
	var flag=0;

	var allBlank = true;
	$('#search-form select').each(function(){
		if($(this).val() != '')
		{
			allBlank = false;
		}
	});

	if(allBlank)
	{
		alert('Please enter search criteria.');
		return false;
	}

	return true;
}

</script>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>

<body onload="document.frmlist.exam_id.focus();">
<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr><td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td></tr>
<tr>
	<td>
		<div id="content">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
			<div id="main">
				<div id="contents">
					<form action="" method="post" name="frmlist" id="frmlist">
						<legend>Homework History</legend> 
						<?php 
							// Show particular Messages
							if(isset($_SESSION['error']))
							{
								echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
								echo $_SESSION['error'];
								echo '</td></tr></tbody></table>';
								unset($_SESSION['error']);
							}
					
							if(isset($_SESSION['success']))
							{
								echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
								echo $_SESSION['success'];
								echo '</td></tr></tbody></table>';
								unset($_SESSION['success']);
							} 
						?>

<div class="search-div">
<fieldset class="rounded search-fieldset">
<legend>Search</legend>
	
<br><div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>

<table border="0" cellspacing="1" cellpadding="4" width="100%" style="" id="search-form">
<tr>
	<td align="right">Course Name:</td>
	<td>
		<select name="exam_id" id="exam_id" class="rounded">
			<option value=''>Select Course</option>
			<?php
			if(is_array($exam))
			{
				for($counter=0;$counter<count($exam);$counter++)
				{
					$selected='';
					if ($exam[$counter]->id == $params['exam_id'])
					{
						$selected='selected';
					}
					echo '<option value="'.$exam[$counter]->id.'"'.$selected.'>'. stripslashes($exam[$counter]->exam_name).'</option>';
				}
			}
			?>
		</select>
	</td>
</tr>

<tr>
	<td align="right">Homework:</td>
	<td>
		<select name="homework_id" id="homework_id" class="rounded">
			<option value=''>Select Homework</option>
			<?php
			if(is_array($homework))
			{
				for($counter=0;$counter<count($homework);$counter++)
				{
					$selected='';
					if ($homework[$counter]->id == $params['homework_id'])
					{
						$selected='selected';
					}
					echo '<option value="'.$homework[$counter]->id.'"'.$selected.'>'. stripslashes($homework[$counter]->homework_name).'</option>';
				}
			}
			?>
		</select>
	</td>
</tr>

<tr>
	<td align="right">Student:</td>
	<td>
		<select name="candidate_id" id="candidate_id" class="rounded">
			<option value=''>Select Student</option>
			<?php
			if(is_array($candidate))
			{
				for($counter=0;$counter<count($candidate);$counter++)
				{
					$selected='';
					if ($candidate[$counter]->id == $params['candidate_id'])
					{
						$selected='selected';
					}
					echo '<option value="'.$candidate[$counter]->id.'"'.$selected.'>'. stripslashes($candidate[$counter]->candidate_name).'</option>';
				}
			}
			?>
		</select>
	</td>
</tr>


<tr>
	<td align="right">Year:</td>
	<td>
		<select name="year" id="year" class="rounded">
			<option value=''>Select Year</option>
			<?php
			if(is_array($homeworkyearlist))
			{
				foreach($homeworkyearlist as $year)
				{
					$selected='';
					if ($year == $params['year'])
					{
						$selected='selected';
					}
					echo "<option value='$year' $selected >$year</option>";
				}
			}
			?>
		</select>
	</td>
</tr>
<tr id="tr_month" style="display: none;">
	<td align="right">Month:</td>
	<td>
		<select name="month" id="month" class="rounded">
			<option value=''>Select Month</option>
			<?php
			for($counter=1;$counter<=12;$counter++)
			{
				$selected='';
				if ($counter == $params['month'])
				{
					$selected='selected';
				}
				echo '<option value="'.$counter.'"'.$selected.'>'. date('F', strtotime("01-$counter-2011")).'</option>';
			}
			?>
		</select>
	</td>
</tr>

<tr>
	<td colspan="6" align="center">
		<input type="hidden" name="show_history" value="1" />
		<input type="submit" class="buttons rounded" name="Submit" value="Show" onclick="return check_search();" />
		<input type="button" class="buttons" name="clear_search" id="clear_search" value="Clear Search"	onclick="window.location='<?php echo CreateURL('index.php','mod=homework_history'); ?>'" />
	</td>
</tr>
</table>

</fieldset>
</div>

<?php
if($homework_history)
{ ?>
	<table width="100%" align="right" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<?php print "Showing Results:".($frmdata['from']+1).'-<span id="show_total_record_span">'.($frmdata['from']+count($homework_history))."</span> of <span id='show_total_count_span'>".count($homework_history)."</span>"; ?>
			</td>
			<td align="right"><?php echo getPageRecords();?></td>
		</tr>
	</table>
	<br /> <?php
}
?> 
<br />

<div id="celebs" style="overflow: auto; overflow-y: hidden;">
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">

<?php if($homework_history) { ?>

<thead>
	<tr class="tblheading">
		<td width="1%">#</td>
		<td width="13%">Course</td>
		<td width="15%">Homework</td>
		<td width="10%">Student</td>
		<td width="10%">Homework Date</td>
		<td width="9%">Details</td>
	</tr>
</thead>
<tbody>

<?php 
foreach($homework_history as $history) 
{ 
	$srNo = $srNo + 1;
	$trClass = (($srNo % 2) == 0) ? 'tdbgwhite' : 'tdbggrey'; 
	$detailedUrl = CreateUrl('index.php', "mod=homework_history&do=details&homework_id=$history->homework_id&candidate_id=$history->candidate_id&backUrl=$backUrl");
	?>

	<tr class='<?php echo $trClass; ?>'>
		<td><?php echo $srNo; ?></td>
		<td><?php echo $history->exam_name; ?></td>
		<td><?php echo $history->homework_name; ?></td>
		<td><?php echo $history->first_name.' '.$history->last_name; ?></td>
		<td><?php echo date('M d, Y h:i A', strtotime($history->created_date)); ?></td>
		<td>
			<a href="<?php echo $detailedUrl; ?>"><img src="<?php echo IMAGEURL; ?>/user_info.gif" /></a>
		</td>
	</tr>
	<?php 
} 
?>

</tbody>

<?php } elseif(isset($params['show_history'])) { ?>

<tr><td colspan='20' align='center'>(0) Record found.</td></tr>

<?php } ?>

</table>
</div>
		
						<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" /> 
						<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
						<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />
					</form>
				</div><!--Div Contents closed-->
			</div><!--Div main closed-->
		</div><!--Content div closed-->
	</td>
</tr>
</table>
</div><!--Outer wrapper closed-->
</body>
</html>