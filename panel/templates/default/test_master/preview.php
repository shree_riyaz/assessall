<?php 
include_once(ROOT . '/lib/Markdown/markdown.php');
$date=$_SESSION['date'];
$current_date= date('m/d/Y h:i:s A');
$_SESSION['is_exam_begin']=1; // to set a variable so if page submit there would be no question query run again and again
$currectTestId=$_SESSION['preview_id'];
$count=count($_SESSION['test_id-'.$currectTestId]);

if(isset($_POST))
{
	$ques_id=$_POST['ques_id'];
	$last_disp_quest=$_POST['display_quest'];
	$is_empty = true;
	
	if($_POST['ques_type'] == "MT")
	{
		$left_ids = $_POST['left_ids'];
		$_SESSION['question-'.$ques_id]['given_answer'] = array();
		foreach($left_ids as $left_id)
		{
			$left_id = (int) $left_id;
			$_SESSION['question-'.$ques_id]['given_answer'][$left_id] = $_POST[$left_id];
			
			if($_POST[$left_id] != '')
				$is_empty = false;
		}
	}
	else
	{
		$_SESSION['question-'.$ques_id]['given_answer']=$_POST['option'];// candidates choosed answer
		
		if($_POST['option'] != '')
			$is_empty = false;
	}

	$_SESSION['question-'.$ques_id]['subject_id']=$_POST['question_subject_id']; //===question subject id
}


if(isset($_POST['mark_review']))
{
	$_SESSION['review'][]=$ques_id;
	$page = $current = $last_disp_quest;
	$page++;
}
elseif(isset($_POST['unmark_review']))
{
	$pos = array_search($ques_id, $_SESSION['review']);
	$_SESSION['review'][$pos] = '';
	unset($_SESSION['review'][$pos]);
	
	$review_count = array();
	foreach($_SESSION['review'] as $key => $val)
	{
		$review_count[] = $val;
	}
	$_SESSION['review'] = $review_count;
	
	
	$page = $current = $last_disp_quest;
	$page++;
}
elseif(isset($_POST['next']))
{
	$current=$last_disp_quest+1;
	$page = $current+1;
}
elseif(isset($_POST['previous']))
{
	$current=$last_disp_quest-1;
	$page = $last_disp_quest;
}
elseif($_POST['is_finish']==1) // if click on finish test
{
	$_SESSION['test_end_time'] = strtotime('now');

	$_SESSION['question-'.$ques_id]['subject_id']=$_POST['question_subject_id']; //===question subject id
	$_SESSION['test_start_time'] = $_POST['test_start_time'];
	
	unset($_SESSION['test_id-'.$currectTestId]);
	$_SESSION['is_exam_begin']=0;
	unset($_SESSION['is_exam_begin']);
	unset($_SESSION['review']);
	foreach($_SESSION as $key=>$value)
	{
		if(is_array($value))
		{
			if(isset($value['given_answer']))
			{
				unset($_SESSION[$key]);
			}
		}
	}
	echo "<script>window.close()</script>";
	exit;
}
elseif($_GET['page'])
{
	$current=$_GET['page']-1;
	$page = $_GET['page'];
}
elseif($_GET['current']!='')
{
	$current=$_GET['current'];
	$page = $current+1;
}


$isReview = 0;
if(isset($_GET['review']) && ($_GET['review'] == 1))
{
	$isReview = 1;
	$countR = count($_SESSION['review']);
	if($countR <= 0)
	{ ?>
		<script>location.href='<?php echo CreateURL('index.php',"mod=test_master&do=preview&id=$currectTestId"); ?>'</script>
		<?php 
	}

	if(!in_array($_SESSION['test_id-'.$currectTestId]['question_id-'.$current], $_SESSION['review']))
	{ 
		$current = $_SESSION['first_review'];	?>
		<script>location.href='<?php echo CreateURL('index.php',"mod=test_master&do=preview&id=$currectTestId&review=1&page=$current"); ?>'</script>
		<?php
	}
}

if($isReview == 1)
{
	$_SESSION['last_marked_question'] = $current+1;
}
else
{
	$_SESSION['last_unmarked_question'] = $current+1;
}

if($is_empty)
{
	unset($_SESSION['attempted_questions'][$ques_id]);
}
else
{
	$_SESSION['attempted_questions'][$ques_id] = $ques_id;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination</title>

<link href="<?php echo ROOTURL; ?>/js/preview/exam.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo ROOTURL; ?>/js/preview/functions.js" language="JavaScript"></script>
<script src="<?php echo ROOTURL; ?>/js/preview/jquery-1.7.1.js"></script>
<script src="<?php echo ROOTURL; ?>/js/preview/jquery.ui.dialog.js"></script>
<script src="<?php echo ROOTURL; ?>/js/preview/jquery.scrollTo-1.4.3.1-min.js"></script>
<link href="<?php echo ROOTURL; ?>/js/preview/demos.css" rel="stylesheet" >
<link href="<?php echo ROOTURL; ?>/js/preview/jquery.ui.all.css" rel="stylesheet"/>
<script src="<?php echo ROOTURL; ?>/js/swfobject.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo STYLE; ?>/wmd.css" />
<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shCore.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shBrushGroovy.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/styles/shCoreDefault.css"/>

<script language="JavaScript">
TargetDate = "<?php echo $date; ?>";
BackColor = "";
ForeColor = "black";
CountActive = true;
CountStepper = -1;
LeadingZero = true;
DisplayFormat = "%%H%% : %%M%% : %%S%% ";
FinishMessage = "It is finally here!";
CurrentDate="<?php echo $current_date ?>";

function newpage(path)
{
	document.examform.action = path;
	document.examform.submit();
}
var message="";
///////////////////////////////////
function clickIE() 
{
	if (document.all) 
	{
		//(message);
		return false;
	}
}
function clickNS(e) 
{
	if(document.layers||(document.getElementById&&!document.all)) 
	{
		if (e.which==2||e.which==3) 
		{
			//(message);
			return false;
		}
	}
}
if (document.layers)
{
	document.captureEvents(Event.MOUSEDOWN);
	document.onmousedown=clickNS;
}
else
{
	document.onmouseup=clickNS;
	document.oncontextmenu=clickIE;
}

document.oncontextmenu=new Function("return false");

$('html').bind('keypress', function(e)
		{
	//alert(e.which);
//====================for disable print (CTRL+p or CTRL+P) ====================================
		   if(e.ctrlKey && (e.which == 112 || e.which == 80))
		   {
				//alert("You can't use print command.");
		      return false;
		   }
//====================for disable copy (CTRL+c or CTRL+C) ====================================
		   if(e.ctrlKey && (e.which == 99 || e.which == 67))
		   {
			   //alert("You can't use copy command.");
		      return false;
		   }

		});

function blockduplicate(sbox)
{
	var match = $('.match');

	if(sbox.value == '')
	{
		return true;
	}
	for(var count=0; count < match.length; count++)
	{
		if(sbox.id == match[count].id)
		{
			continue;
		}
		if(sbox.value == match[count].value)
		{
			alert("This value is already matched.\nPlease select a diffrent one.");
			sbox.value = '';
			return false;
		}
	}
	return true;
}
$(document).ready(function()
{
	$('#ques-nav').scrollTop($('.current-que-link').offset().top-300);
	SyntaxHighlighter.defaults.toolbar = false;
	SyntaxHighlighter.all();
});

</script>

<style>
.match_tab
{
	width: 500px;
	padding: 20px;
	border-collapse: collapse;
	border: 1px solid #e5e5e5;
}
.match_tab td
{
	border: 1px solid #e5e5e5;
	width: 50%;
}
#header
{
	border: 1px solid #ccc;
	background-color:white;
	margin: 2px;
	padding: 20px 2px;
	height: auto;
	width:950px;
	margin:2px auto;
	font-size: 30px;
	font-weight: bold;
	text-align: center;
	color: #888888;
}
#content
{
	background-color:#fff;
	margin: 2px auto;
	padding: 10px;
	border: 1px solid #ccc;
	width:933px;
}
div.pagination a.attepmted
{
	background: green;
	color: white;
}

a.ques-nav-link
{
	text-decoration: none;
	cursor: pointer;
	color: #009;
}
a.ques-nav-link:HOVER
{
	text-decoration: underline;
}
a.attempted-que-link
{
	color: green;
}
a.current-que-link
{
	color: black;
	font-weight: bold;
}
#low-time-note
{
	position: absolute;
	margin-left: 320px;
}
</style>

</head>
<body>
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer" style="padding: 5px;">
	  <tr>
		<td>
			<div id="header" style="background-color:white;width:950px;height:auto;margin:0 auto" >
				<div class="welcome">Welcome to Online-Examination</div>
			</div>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="timer">
				<div id="low-time-note"></div>
				Time Remaining:&nbsp;<script language="JavaScript" src="<?php echo ROOTURL; ?>/js/preview/count.js"></script>
			&nbsp;&nbsp;
			</div>
		</td>
	  </tr>
	  <tr>
		<td>
			
<div id="content" style="min-height:500px">

<form method="post" name="examform" id="examform" action="">
	<input type="hidden" name="test_start_time" id="test_start_time" value="<?php if($_POST) echo $_POST['test_start_time']; else echo strtotime('now'); ?>" />

	<div id="parnt-nav" style="background: #fdfdfd;border: 1px solid #ccc;padding: 10px;margin-bottom: 10px;float: left;width: 910px;">
	
	<table>
		<tr>
		<?php
		if($_SESSION['subject_info_arr'] && ($isReview != 1))
		{
			foreach($_SESSION['subject_info_arr'] as $subject_id => $subject) 
			{
				//foreach($value as $subject_id => $subject)
				if(!isset($subject['last_question_id']) || ($subject['last_question_id'] == ''))
				{
					if($current==$subject['start_question_id'])
					{ ?>
						<td class="currentTd"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?>
						<input type="hidden" name="question_subject_id" id="question_subject_id" value="<?php echo $subject_id; ?>" /></td> <?php
					}
					else
					{ ?>
						<td><a class="other" onClick="newpage('<?php echo CreateURL('index.php',"mod=test_master&do=preview&id=$currectTestId&subject_id=$subject_id&current=".$subject['start_question_id']); ?>');"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?></a></td> <?php
					}
				}
				elseif($current>=$subject['start_question_id'] && $current<=$subject['last_question_id'])
				{
		?>
				<td class="currentTd"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?>
				<input type="hidden" name="question_subject_id" id="question_subject_id" value="<?php echo $subject_id; ?>" /></td>
		<?php 
				}
				else
				{
		?>
				<td><a class="other" onClick="newpage('<?php echo CreateURL('index.php',"mod=test_master&do=preview&id=$currectTestId&subject_id=$subject_id&current=".$subject['start_question_id']); ?>');"><?php echo ucwords(strtolower(stripslashes($subject['subject_name']))); ?></a></td>
		<?php 
				}
			}
		}
			?>
			
		</tr>
	</table>
	
<?php 
	
	global $DB;

	if($_SESSION['questionMedia'] == 'subject')
	{
		$query = "select tq.question_id as num
	        from test
			join test_questions as tq on tq.test_id = test.id
			JOIN question on question.id = tq.question_id where test.id=".$currectTestId.$_SESSION['subject_condition'];
	}
	elseif($_SESSION['questionMedia'] == 'paper')
	{
		$query = "select pq.question_id as num
        from paper
		join paper_questions as pq on pq.paper_id = paper.id
		JOIN question on question.id = pq.question_id where paper.id=".$_SESSION['testPaperId'].$_SESSION['subject_condition'];
	}
	$total_pages = $DB->ExecuteQuery($query);
	$totalpages = $total_pages = $DB->NumRows($total_pages);

	$attempted_questions = array();
	$review = array();
	$total_questions = array();
	
	for($count1=0; $count1<$totalpages; $count1++)
	{
		$questionID = $_SESSION['test_id-'.$currectTestId]['question_id-'.$count1];
		$total_questions[] = $count1+1;
		
		if(in_array($questionID, $_SESSION['attempted_questions']))
			$attempted_questions[] = $count1+1;
			
		if(in_array($questionID, $_SESSION['review']))
			$review[] = $count1+1;
	}
	
	$_SESSION['first_review'] = $review[0];
	$reviewUrl = '';
	if($isReview == 1)
	{
		$total_questions = $review;
		$reviewUrl = '&review=1';
	}
	
	$_SESSION['total_questions'] = $total_questions;
	$targetpage = CreateURL('index.php',"mod=test_master&do=preview&id=$currectTestId$reviewUrl");

	/* Setup page vars for display. */
	
	$total_pages = count($total_questions);	
	$adjacents = 5;																// How many adjacent pages should be shown on each side?
	if ($page == 0) $page = $total_questions[0];								//if no page var is given, default to 1.
	$prev = $total_questions[array_search($page, $total_questions) - 1];		//previous page is page - 1
	$next = $total_questions[array_search($page, $total_questions) + 1];		//next page is page + 1
	$lpm1 = $total_questions[$total_pages-2];									//last page minus 1
	
	$pagination = "<div class=\"pagination\">";
	
	if(count($_SESSION['review']) > 0 && $isReview != 1)
	{ 
		if($_SESSION['last_marked_question'] != '') $lastMarked = "&page=".$_SESSION['last_marked_question'];
		$reviewUrl = CreateURL('index.php',"mod=test_master&do=preview&id=$currectTestId&review=1$lastMarked");
		$pagination.= "<span style=\"cursor:pointer;\" class=\"current\" onclick=\"newpage('$reviewUrl');\">Review Marked Questions</span><br><br>";
	}
	elseif ($isReview == 1)
	{ 
		if($_SESSION['last_unmarked_question'] != '') $lastUnmarked = "&page=".$_SESSION['last_unmarked_question'];
		$reviewUrl = CreateURL('index.php',"mod=test_master&do=preview&id=$currectTestId$lastUnmarked"); 
		$pagination.= "<span style=\"cursor:pointer; \" class=\"current\" onclick=\"newpage('$reviewUrl');\">All Questions</span><br><br>"; 
	}
	
	if($total_pages > 1)
	{	
		//previous button
		if ($page != $total_questions[0])
		{
			$onPreClick = "newpage('$targetpage&page=$prev');";
		}

		//pages	
//		if ($total_pages < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter1 = 0; $counter1 < $total_pages; $counter1++)
			{
				$counter = $total_questions[$counter1];
				
				$is_attempted = ''; if(in_array($counter, $attempted_questions)) $is_attempted = " class = 'attepmted' ";
					
				if ($counter == $page)
				{
					$pagination.= "<span class=\"current\">$counter</span>";
				}
				else
				{
					$pagination.= "<a style=\"cursor:pointer; \" $is_attempted onclick=\"newpage('$targetpage&page=$counter');\">$counter</a>";					
				}
				
//				if(($counter1+1) % 30 == 0) $pagination.= '<br></br>';
			}
		}
		//next button
		
		if ($page < $total_questions[$counter1-1]) 
		{
			$onNextClick = "newpage('$targetpage&page=$next');";
		}
	}
	
$pagination.= "</div>";
echo $pagination;
?>
</div>
<div style="clear: both;"></div>
<div id="main-area">

<div id="question-area" style="width: 650px;float: left;background: #fdfdfd;border: 1px solid #ccc;padding: 10px;min-height: 400px;">
<?php 
	if($current=='') $current=0;
	$current_display_question=$_SESSION['test_id-'.$currectTestId]['question_id-'.$current];

	$toolTipMark = 'Click to review this question later.';
	$toolTipUnMark = 'Click to unmark this question for review.';
?>

<div id="mark-review-parent" style="position: absolute;width: 650px;text-align: right;">
<?php if (in_array($current_display_question, $_SESSION['review'])) { ?>
<input type='submit' name='unmark_review' value='Unmark From Review Later' style='cursor:pointer' id='unmark_review' title='<?php echo $toolTipUnMark; ?>'>
<?php } else { ?>
<input type='submit' name='mark_review' value='Mark To Review Later' style='cursor:pointer' id='mark_review' title='<?php echo $toolTipMark; ?>'>
<?php } ?>
</div>	

<div style="min-height: 360px;">
  <table border="0" cellspacing="0" cellpadding="1" id="tbl_totalquestions">

  <?php

	$_SESSION['viewed_questions'][$current_display_question] = $current_display_question;
	
	$result = $DB->SelectRecord('question', "id = '$current_display_question'", 'question_type');
	$current_display_question_type = $result->question_type;
		
	$is_multi = false;
	$question_answer=$DB->SelectRecords('question_answer','question_id='.$current_display_question);
	if(count($question_answer) > 1)
	{
		$is_multi = true;
	}
	
	$getans="select q.id as question_id, ans.id, ans.answer_title, q.question_title, q.is_group 
			from question as q
			join answer as ans on ans.question_id=q.id
			where q.id=$current_display_question
			group by ans.id";
	
	if(($current_display_question_type == "S") || ($current_display_question_type == "MT"))
	{
		$getans="select *,id as question_id from question where id = $current_display_question";
	}
	
	$getAnsResult = $DB->RunSelectQuery($getans);

	$path = ROOTURL . '/uploadfiles/';
	$img="";
	if($current_display_question_type == "I")
	{
		$getimg = "select image_path 
					from question
        			join question_image on question_image.question_id = question.id
					where question.id = '".$current_display_question."'";
	
		$getImages = $DB->RunSelectQuery($getimg);
		$total_img = count($getImages);
		
		$img = "<br><br>";
		for($count_img = 0; $count_img < $total_img; $count_img++)
		{
			$images= $getImages[$count_img];
			$final_path = $path.$images->image_path;
			if($images->image_path == '0' || $images->image_path == null)
			{
				$final_path = ROOTURL . "/images/question-mark.png";
			}
				
			$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
		}
	}
	elseif($current_display_question_type == "S")
	{
		// do nothing;
	}
	elseif($current_display_question_type == "MT")
	{
		$left_cols = $DB->SelectRecords('question_match_left',"question_id='$current_display_question'");
		$right_cols = $DB->SelectRecords('question_match_right',"question_id='$current_display_question'");
		$right_cols = shuffle_assoc($right_cols);
		//echo "<pre>";print_r($right_cols);
	}

	$is_first=1;
	$total_answer = count($getAnsResult);
	$hint_info = $DB->SelectRecord('question','id='.$current_display_question,'*');
	
	for($count_ans=0; $count_ans<$total_answer;$count_ans++)
	{
		$option= $getAnsResult[$count_ans];
		if($is_first==1)
		{
			$is_first=0;
			$qno=$current+1;
			
			echo '<tr><td style="font-size:11px;"><b>Hint : </b>';
			switch($current_display_question_type)
			{
				case 'MT':
					echo 'Match The Following';
					break;
				case 'S':
					echo 'Subjective Type Question (Write Answer in Textarea)';
					break;
				default:
					echo 'Multiple Choice Question ';
					if($is_multi) echo '(Select <b>More Than One</b> Answers)';
					else echo '(Select <b>One</b> Answer)';
					
			}
			
			$title = (($option->question_title));
			//$title = Markdown($title);
			//$title = str_replace('&amp;', '&', $title);
			
			echo '<br></br></td></tr>';
			echo "<tr><td style='padding-bottom:12px'><b>Question $qno</b> <br>".$title.$img.'</td></tr>';
			
			if($hint_info->image != '')
			{ ?>
				
				<tr>
					<td><img style="max-width: 300px;max-height: 300px;" src="<?php echo $path.$hint_info->image; ?>" /></td>
				</tr> 
				<tr><td>&nbsp;</td></tr> <?php
			}
		}
		
		if ($current_display_question_type == "MT")
		{
			echo "<tr><td style='padding-left:25px'><table class='match_tab'>";
			
			foreach($left_cols as $lcol)
			{
				echo "<input type='hidden' name='left_ids[]' value=' $lcol->id '></input>";
				echo "<tr>
						<td style='padding:15px'>" . $lcol->value . "</td>
						<td style='padding:15px'>
						<select name='". $lcol->id ."' id='". $lcol->id ."' onchange='return blockduplicate(this);' class='match rounded'>
							<option value=''>Select Match</option>";
				
				foreach($right_cols as $rcol)
				{
					$selected = '';
					if($_SESSION['question-'.$current_display_question]['given_answer'][$lcol->id] == $rcol->id)
					{
						$selected = 'selected="selected"';
					}
					echo "<option value='". $rcol->id ."' $selected >". $rcol->value ."</option>";
				}
				
				echo "</select></td></tr>";
			}
			
			echo "</table></td></tr>";
		}
		
		elseif ($current_display_question_type == "S")
		{
			echo "<tr><td style='padding-left:20px;'><b>Write Answer</b><br><textarea rows='15' cols='70' class='rounded' name='option'>".$_SESSION['question-'.$option->question_id]['given_answer']."</textarea></td></tr>";
		}
		
		else
		{
			$answer = $option->answer_title;
			if($current_display_question_type == "I")
				$answer = "<img height='50' width='50' src=". $path.$option->answer_title . ">";
			
			$checked = '';
			
			$name = 'option';
			$type = 'radio';
			if($is_multi)
			{
				$name = 'option[]';
				$type = 'checkbox';
			}
			
			if(isset($_SESSION['question-'.$option->question_id]['given_answer']))
			{
				if(is_array($_SESSION['question-'.$option->question_id]['given_answer']) 
					&& in_array($option->id, $_SESSION['question-'.$option->question_id]['given_answer']))
				{
					$checked = 'checked';
				}
				elseif($_SESSION['question-'.$option->question_id]['given_answer']==$option->id)
				{
					$checked = 'checked';
				}
			}
				
			echo "<tr>
					<td valign='top' style='padding-left:25px'>
						<label>
							<input type='$type' name='$name' value=$option->id $checked>
							$answer
						</label>
					</td>
				</tr>";
		}
		
		echo"<input type=hidden name=ques_id value=$current_display_question>";
		echo"<input type=hidden name=display_quest value=$current>";
	}
	?>
	</table>
	</div>

<table style="margin-top:5px;">
	<tr>
		<td width="10%" align="right">
			<?php if ($current+1 != $total_questions[0]) { ?> 
			<input type='button' name='previous' value='Previous' onClick="<?php echo $onPreClick; ?>" style='cursor:pointer' class='buttons'>
			<?php } else echo '&nbsp;'; ?>
	
		</td>
		<td width="10%" align="right">
			<?php if ($current+1 != $total_questions[count($total_questions)-1]) { ?> 
			<input type='button' name='next' value='Next' onClick="<?php echo $onNextClick; ?>" style='cursor:pointer' class='buttons'>
			<?php } else echo'&nbsp;'; ?>
		</td>
	
		<?php if($hint_info->video != '') { ?>
		<td width="15%" align="right">
			<a style='font-family:arail, helvetica, sans-serif;cursor:pointer;font-size:12px;background-color:yellow' id='show_video'>Show Video</a>
		</td>
		<?php } ?>
		<td width="65%" align="right">
			<input type='button' name='finish' id="finish" value='Finish Test' style='cursor:pointer' class="buttons rounded" onClick="return finishTest();" />
		</td>
	</tr>
</table>
	
<input type=hidden name='is_finish' id='is_finish' value='0'>
<input type="hidden" name="ques_type" id="ques_type" value="<?php echo $current_display_question_type; ?>" />

</div>

<div id="ques-nav" style="width: 220px;float: right;background: #fdfdfd;border: 1px solid #ccc;padding: 10px;height: 400px;overflow-y: auto;">
<table>
<?php 
$targetpage = CreateURL('index.php',"mod=test_master&do=preview&id=$currectTestId");
for($counter=0; $counter<$totalpages;$counter++)
{ 
	$q_id = $_SESSION['test_id-'.$currectTestId]['question_id-'.$counter];
	
	if(!in_array($q_id,$_SESSION['viewed_questions']))
		continue;
?>

<tr>
	<td width="20px"><?php echo $counter+1 ?></td>
		
		<?php 
			
			$question_record = $DB->SelectRecord("question", "id='$q_id'");
			
			$qtitle = (($question_record->question_title));
			//$qtitle = Markdown($qtitle);
			//$qtitle = str_replace('&amp;', '&', $qtitle);
			$qtitle = strip_tags($qtitle);
			if(strlen($qtitle) > 25) $qtitle = substr($qtitle, 0, 22).'...';
			
			$is_attempted = ''; 
			if(in_array($counter+1, $attempted_questions)) $is_attempted = ' attempted-que-link ';
			
			$is_current = '';
			if($counter == $current) $is_current = ' current-que-link'; 
			$href = $targetpage.'&page='.($counter+1);
		?>
		
	<td>
		<a class="ques-nav-link <?php echo $is_attempted.$is_current; ?>" href="<?php echo $href; ?>">
			<?php echo $qtitle; ?>
		</a>
	</td>
</tr>

<?php
}
?>
</table>
</div>

<div style="clear: both;"></div>
</div>
	
</form>

</div>	
</td>
</tr>	
</table>
</div><!--Outer wrapper closed-->
<?php 
if($hint_info->video != '')
{ 
	$video_href = $path . $hint_info->video; ?>
	
	<div id="video_preview">
		<div id='vid'>
			<a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this video.
		</div>
		
		<script type="text/javascript">
			var s1 = new SWFObject("<?php echo ROOTURL ?>/lib/mediaplayer.swf","ply","600","385","9","#FFFFFF");
			s1.addParam("allowfullscreen","true");
			s1.addParam("allowscriptaccess","always");
			s1.addVariable('file','');
			s1.addParam("flashvars","&file=<?php echo $video_href; ?>&stretching=exactfit&autostart=true");
			s1.write("vid");
		</script>		
	</div>
<?php
}
function shuffle_assoc($list) { 
  if (!is_array($list)) return $list; 

  $keys = array_keys($list); 
  shuffle($keys); 
  $random = array(); 
  foreach ($keys as $key) { 
    $random[] = $list[$key]; 
  }
  return $random; 
} 
?>
</body>
</html>
<script>
$(document).ready(function() {
$("#video_preview").dialog({ autoOpen: false,width: 'auto' });
$('#show_video').click(function() {
	//$dialog.dialog('open');
	$("#video_preview").dialog('open')
	// prevent the default action, e.g., following a link
	return false;
});
});
</script>
<style>
#mark_review
{
	padding: 5px;
	font-weight:bold;
	background-color: #eeffee;
	border: 1px solid #aaaaaa;
}
#mark_review:HOVER
{
	color: #008800;
}
#unmark_review
{
	padding: 5px;
	font-weight:bold;
	background-color: #ffeeee;
	border: 1px solid #aaaaaa;
}
#unmark_review:HOVER
{
	color: #880000;
}
</style>