<?php
 /*****************Developed by :- Richa verma
	                Date         :- 7-july-2011
					Module       :- Test master
					Purpose      :- Template for See all question and disscussion of a test
***********************************************************************************/
include_once(ROOT . '/lib/Markdown/markdown.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Question Paper</title>

<script type="text/javascript">
function show(id)
{
	if(document.getElementById(id).style.display=='none')
	{
		document.getElementById(id).style.display='inline';
	}
	else
	{
		document.getElementById(id).style.display='none';
	}
}
$(document).ready(function()
{
	SyntaxHighlighter.defaults.toolbar = false;
	SyntaxHighlighter.all();
});
</script>
<link rel="stylesheet" type="text/css" href="<?php echo STYLE; ?>/wmd.css" />
<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shCore.js"></script>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/scripts/shBrushGroovy.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo ROOTURL; ?>/lib/syntaxhighlighter/styles/shCoreDefault.css"/>
</head>
<body>

<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr>
	<td>
		<div id="test_question_paper">
		<div class="bgblue">Test Name: <?php echo $test_detail->test_name ?></div>
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_questions_wrapper">
  <tr>
    <td>

		<?php 
			$path = ROOTURL . "/uploadfiles/";
			$currectTestId=$test_detail->id;
			$count=count($test_question_detail);
			$q_no=0;
			for($counter=0;$counter<$count;$counter++)
			{
				$q_no=$q_no+1;
				$q_id=$test_question_detail[$counter]->question_id;
				$question_title=$DB->SelectRecord('question','id='.$q_id);
				echo '<table  border="0" width="100%" align="left" cellpadding="0" cellspacing="0" style="padding:10px; border-bottom:1px dashed #666;"> ';
				$img="";
				if($question_title->question_type == 'I')
				{
					$images = $DB->SelectRecords('question_image','question_id='.$q_id);
					$count_img = count($images);
					
					$img = "<br><br>";
					for($i = 0; $i < $count_img; $i++)
					{	
						$final_path = $path.$images[$i]->image_path;
						
						if($images[$i]->image_path == '0')
							$final_path = ROOTURL . "/images/question-mark.png";
						
						$img .= "&nbsp;&nbsp;&nbsp;<img height='50' width='50' src=". $final_path ." />";
					}
					$img .= "<br><br>";
				}
				
				
				$title = (($question_title->question_title));
				//$title = Markdown($title);
				//$title = str_replace('&amp;', '&', $title);
			
				
				echo "<tr>             
					<td colspan='2'><p><strong> Q ".$q_no."</strong><br>" . $title .$img . "</p></td>
				</tr>";
				
				if ($question_title->question_type == 'MT')
				{
					$left_cols = $DB->SelectRecords('question_match_left',"question_id='$q_id'");
					$right_cols = $DB->SelectRecords('question_match_right',"question_id='$q_id'");
					
					echo "<tr><td><table style='color:#666699'>";
					foreach($left_cols as $lcol)
					{
						$answer_id = $lcol->answer_id;
						$corr_col = $DB->SelectRecord('question_match_right',"id='$answer_id'");
						
						echo "<tr>
								<td>" . $lcol->value . "</td>
								<td>&nbsp;--&nbsp;</td>
								<td>" . $corr_col->value . "</td>
							</tr>";
					}
					
					echo "</table></td></tr>";
				}
				elseif ($question_title->question_type == 'S')
				{
				
				}
				else
				{
					$answer=$DB->SelectRecords('answer','question_id='.$q_id);
					$correct_answers=$DB->SelectRecords('question_answer','question_id='.$q_id);
					
					$correct_answer = array();
					foreach($correct_answers as $ca)
					{
						$correct_answer[] = $ca->answer_id;
					}
					
					$countw=count($answer);
					
					for($counts=0;$counts<$countw;$counts++)
					{
						$ans= $answer[$counts]->answer_title;
						if(in_array($answer[$counts]->id, $correct_answer))
						{
							$check='checked';
						}
						else
						{
							$check='';
						}
						if($question_title->question_type == 'I')
						{
							$ans = "<img height='50' width='50' SRC = '$path$ans' />";
						}
						echo '<tr> 
						<td style="color:#666699">
							<input type="radio" disabled="disabled" '.$check.'> <font size="-1">'.$ans.'</font> 
							</td>        
						</tr>';
						
					}
				}
				if($question_title->discussion!='')
				{
					echo "<tr><td align='right'><div aling='right'><span  onclick=\"show('$q_id')\" style='cursor:pointer;color:#666699'>Discussion</span></div></td></tr>";
					echo'<tr><td><div id="'.$q_id.'" style="color:#666699;display:none;"><textarea cols="80" rows="3" readonly>'.$question_title->discussion.'</textarea></div></td></tr>';
				}
				echo'</table><br/>';
			}
			
		?>
		
			</div><!--test_question_paper div closed-->
		</td>
	  </tr>
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>
