
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Online Examination - <?php echo $isEdit ? 'Edit' : 'Add'; ?> Test</title>

<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/jquery.tabs.css' />

<style>
	#exam-subject-info
	{
		font-weight: bold;
		color: #888;
	}
</style>

<script language="javascript" type="text/javascript">
	$(function() 
	{ 
		$( "#tabs" ).tabs();
		$( "#tabs" ).tabs('disable', 1);
	});
	function numbering()
	{
		var count = 1;
		$('#tbl_body tr').each(function() {
		    $(this).find("td:first-child").html(count++);    
		 });
	}
	
	function showGeneralExamDetail(isLevelShow)
	{	
		if(isLevelShow=='no')
		{
			document.getElementById('question_media_tr').style.display="";
		}
		else
		{
			document.getElementById('question_media_tr').style.display="none";
		}
		
		document.getElementById('select_subject_tr').style.display="none";
		document.getElementById('select_paper_tr').style.display="none";
		selectQuestionMedia('');
		xajax_showFilteredCandidate($('#exam_id').val(), true);
	}

	function getmaxmarks()
	{
		var total=Number(document.getElementById('total_mark').innerHTML);
		document.getElementById('maximum_marks').value=total;
	
		document.getElementById('submit<?php echo $isEdit ? 'Edit' : 'Add'; ?>_form').value=1;
		document.addtest.submit();
	}
	
	function subjectquestionform(val)
	{
		var msg="";
		var flag=0;
		var total_subject = document.getElementById("total_subject");
		var exam_id = document.getElementById("exam_id").value;
		
		if(val.value!='')
		{
			for(var i=0; i<total_subject.value; i++)
			{
				if(document.getElementById('subject_id_'+(i+1))==null)
				{
					continue;
				}
					
				if(val.value==document.getElementById('subject_id_'+(i+1)).value)
				{
					msg+= "This subject is already selected. Please try another subject.\n";
					focusAt=val;
					flag=1;
				}
			}
	
			if(msg=='')
			{
				$('#question-tab').html('<br><div align="center"><img src="<?php echo IMAGEURL.'/loading.gif'; ?>" /></div><br>');
				var url = "index.php?mod=test_master&do=addsubjectquestion&exam_id="+exam_id+"&subject_id="+val.value;
				$('#question-tab').load(url);
				$( "#tabs" ).tabs('enable', 1);
				$('#tabs').tabs('option', 'active', 1); 
			}
			else
			{
				alert(msg);
				focusAt.focus();
			}
		}
	}
	
	function addRow()
	{
		var subject_name = document.getElementById("subject_name");
		var subject_id = document.getElementById("select_subject_id");
		var subject_total_question = document.getElementById("subject_total_question");
		var subject_paper_mark = document.getElementById("subject_paper_mark");
		var total_subject = document.getElementById("total_subject");
		var subject_diff_question=document.getElementById("subject_diff_question");
		
		var table = document.getElementById('tbl_body');
		
	    var rowCount = table.rows.length;
	    //if(rowCount==0)
	     if(total_subject.value==0)
		    document.getElementById('tbl_reports').style.display='';
	    //alert(rowCount);
	    var row = table.insertRow(rowCount);
	
		//var row_no = rowCount + 1;
		var row_no = Number(total_subject.value)+1;
	
		if((row_no%2)==0)
			row.className="tdbgwhite";
		else
			row.className="tdbggrey";
	
	//=======SrNo td======================	
	    var cell1 = row.insertCell(0);
	    cell1.innerHTML = row_no;
	
	//=======Subject name=================
	    var cell2 = row.insertCell(1);
	
	    cell2.align = "center";
	    cell2.id = "subject_nameTd_"+row_no;
	    cell2.innerHTML=subject_name.value;
	
	//=======Subject Total Question=================
	    var cell3 = row.insertCell(2);
	
	    cell3.align = "center";
	    cell3.id = "subject_questionTd_"+row_no;
	    cell3.innerHTML=subject_total_question.value;
	
	//=======Subject Total Marks=================
	    var cell4 = row.insertCell(3);
	
	    cell4.align = "center";
	    cell4.id = "subject_markTd_"+row_no;
	    cell4.innerHTML=subject_paper_mark.value;
	
	//=======Subject Show Different Question=================
	    var cell5 = row.insertCell(4);
	
	    cell5.align = "center";
	    cell5.id = "subject_diff_questionTd_"+row_no;
	    cell5.innerHTML=subject_diff_question.value;
	
	//==========Option for Edit and Delete==============
	    var cell6 = row.insertCell(5);
	   
	    cell6.align = "center";
	    cell6.innerHTML='<input type="button" class="buttons rounded" name="edit_'+row_no+'" id="edit_'+row_no+'" value="Edit" onclick="editRow(this);" /> <img src="<?php echo IMAGEURL; ?>/b_drop.png" style="cursor: pointer;" id="delete_img_'+row_no+'" title="Remove" onclick="deleteRow(this);" /> ';
	    
	    subject_id.value='';
	    subject_name.value='';
	    subject_total_question.value='';
	    subject_paper_mark.value='';
	    subject_diff_question.value='';
	
	    var subject_mark_td_value = document.getElementById("subject_markTd_"+row_no).innerHTML;
		var subject_marks = Number(subject_mark_td_value.substr(0,subject_mark_td_value.indexOf("<")-1));
		var total_mark = Number(document.getElementById("total_mark").innerHTML);
		
		document.getElementById("total_mark").innerHTML=total_mark+subject_marks;
		
	    total_subject.value=row_no;
	    $('#sub_tbl').show();
	    numbering();
	}
	
	function editRow(val, row_no)
	{	
		if(val=='update')
		{	
			//alert(row_no);
			var subject_name = document.getElementById("subject_name");
			var subject_total_question = document.getElementById("subject_total_question");
			var subject_paper_mark = document.getElementById("subject_paper_mark");
			var subject_diff_question = document.getElementById("subject_diff_question");
	
			//alert(subject_name.value);
			document.getElementById("subject_nameTd_"+row_no).innerHTML = subject_name.value;
			document.getElementById("subject_questionTd_"+row_no).innerHTML = subject_total_question.value;
			document.getElementById("subject_markTd_"+row_no).innerHTML = subject_paper_mark.value;
			document.getElementById("subject_diff_questionTd_"+row_no).innerHTML = subject_diff_question.value;
	
			subject_name.value = '';
			subject_total_question.value = '';
			subject_paper_mark.value = '';
			subject_diff_question.value='';
			showTotalMarks();
		}
		else
		{
			var row_no = val.id.substr(val.id.lastIndexOf('_')+1);
			var exam_id = document.getElementById('exam_id').value;
			var subject_id = document.getElementById('subject_id_'+row_no).value;
			var random_que_id_set = document.getElementById('random_que_id_'+row_no).value;
			var custom_que_id_set = document.getElementById('custom_que_id_'+row_no).value;
			var group_que_id_set = document.getElementById('group_que_id_'+row_no).value;
			var subject_total_mark = document.getElementById('subject_total_mark_'+row_no).value;
			var subject_diff_question = document.getElementById('subject_diff_question_'+row_no).value;
			
			$('#question-tab').html('<br><div align="center"><img src="<?php echo IMAGEURL.'/loading.gif'; ?>" /></div><br>');
			var url = "index.php?mod=test_master&do=addsubjectquestion&rowno="+row_no+"&exam_id="+exam_id+"&subject_id="+subject_id+"&random_que_id_set="+random_que_id_set+"&custom_que_id_set="+custom_que_id_set+"&group_que_id_set="+group_que_id_set+"&subject_total_mark="+subject_total_mark+"&subject_diff_question="+subject_diff_question;
			$('#question-tab').load(url);
			$( "#tabs" ).tabs('enable', 1);
			$('#tabs').tabs('option', 'active', 1);  
		}
	}
	
	function deleteRow(r)
	{
		var confirmation_flag = confirm("Do you really want to remove this subject?");
		if(confirmation_flag)
		{
			var rowno = r.id.substr(r.id.lastIndexOf("_")+1);
			var total_mark =  Number(document.getElementById("total_mark").innerHTML);
			var subject_marks = document.getElementById("subject_total_mark_"+rowno).value;
			
			document.getElementById("total_mark").innerHTML=total_mark-subject_marks;
			
			var row_index = r.parentNode.parentNode.rowIndex-1;
			document.getElementById('tbl_body').deleteRow(row_index);
	
			if($('#tbl_reports tr').length <= 1)
			{
				$('#sub_tbl').hide();
			}
			numbering();
		}
		else
		{
			return false;
		}
	}
	
	function showTotalMarks()
	{
		var total_subject = document.getElementById('total_subject');
		var total_mark = 0;
		
		for(var i=0; i < total_subject.value; i++)
		{
			if(document.getElementById('subject_total_mark_'+(i+1)) == null)
			{
				continue;
			}
			else
			{
				var subject_marks = Number(document.getElementById('subject_total_mark_'+(i+1)).value);
				total_mark = subject_marks+total_mark;
			}
		}
		document.getElementById('total_mark').innerHTML = total_mark;
	}
	
	function resetQuestionHistory(val)
	{
		document.getElementById('total_mark').innerHTML=0;
		document.getElementById('tbl_reports').style.display="none";
		document.getElementById('tbl_body').innerHTML='';
		document.getElementById('total_subject').value=0;
	
		if(val != 1)
		document.getElementById('question_media').value='';
	}
	
	function selectQuestionMedia(value)
	{
		if((value == '') || (typeof(value)!='undefined'))
		{
			value = document.getElementById('question_media').value;
		}
		
		var subject = document.getElementById('select_subject_tr');
		var paper = document.getElementById('select_paper_tr');
		
		$('#question-tab').html('');
		$('#tabs').tabs('option', 'active', 0);
	    $('#tabs').tabs('disable', 1);
		
		if(value == 'subject')
		{
			paper.style.display = 'none';
			subject.style.display = '';
		}
		else if(value == 'paper')
		{
			subject.style.display = 'none';
			paper.style.display = '';
		}
		else
		{
			subject.style.display = 'none';
			paper.style.display = 'none';
		}
	}
	
	$(document).ready(
	function()
	{
		xajax_getExamTypeIdByExamId('<?php echo $frmdata['exam_id']; ?>', false);
		
		$('.include-check').live('change', function() {
			$('.include-check').not(':checked').parent().parent().removeClass('included')
			$('.include-check').filter(':checked').parent().parent().addClass('included')
		});
		xajax_showFilteredCandidate('<?php echo $frmdata['exam_id']; ?>', true);	
	});
</script>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();

if($_SESSION['total'])
{
	unset($_SESSION['total']);
}

if($_SESSION['custom_total_marks'])
{
	unset($_SESSION['custom_total_marks']);
	unset($_SESSION['question']);
}
if($_SESSION['group_total_marks'])
{
	unset($_SESSION['group_total_marks']);
	unset($_SESSION['question']);
}

if(isset($_SESSION['random_total_value']))
{
	unset($_SESSION['random_total_value']);
}

if(isset($_SESSION['custom_question_id_set']))
{
	unset($_SESSION['custom_question_id_set']);
}

if(isset($_SESSION['group_question_id_set']))
{
	unset($_SESSION['group_question_id_set']);
}
?>
</head>

<body onload="document.addtest.test_name.focus();">
<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	<tr>
		<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
	</tr>
	<tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				<div id="main">
					<div id="contents">
						<fieldset class="rounded">
							<legend><?php echo $isEdit ? 'Edit' : 'Create'; ?> Test</legend><br />
							<?php
								if(isset($_SESSION['error']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
									echo $_SESSION['error'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['error']);
								}
								if(isset($_SESSION['success']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
									echo $_SESSION['success'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['success']);
								}
							?>

<div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div><br>
<form action="" method="post" name="addtest" id="addtest">

<div id="tabs">
    <ul>
        <li><a href="#test-info-tab">Test Information</a></li>
        <li><a href="#question-tab">Questions</a></li>
        <li><a href="#student-tab">Students</a></li>
    </ul>
    
    <div id="test-info-tab" class="exam-tabs">
    
<table border="0" cellspacing="0" cellpadding="4" id="tbl_preferences" width="" style="margin-left: 30px;">
	<tr>
		<td width="150px">
			<div>Test Name:<span class="red">*</span></div>
		</td>
		<td width="">
			<input name="test_name" id="test_name" type="text"
					class="rounded textfield" value="<?php echo $frmdata['test_name'] ?>"
					size="35" onchange="IsAlphaNum(this, 'test name');" maxlength="200" />
		</td>
	</tr>
	<tr>
		<td>
			<div>Course:<span class="red">*</span></div>
		</td>
		<td>
			<select name="exam_id" id="exam_id" class="rounded" 
					onchange="xajax_getExamTypeIdByExamId(this.value);resetQuestionHistory();" >
				<option value="">Select Course</option>
				<?php 
					if(is_array($exam))
					{
						for($counter=0;$counter<count($exam);$counter++)
						{
							$selected='';
							if ($exam[$counter]->id == $frmdata['exam_id'])
							{
								$selected='selected';
							}
							echo '<option value="'.$exam[$counter]->id.'"'.$selected.'>'.stripslashes($exam[$counter]->exam_name).'</option>';
						}
					}
				?>
			</select>&nbsp;<span id="exam-subject-info"></span>
		</td>
	</tr>
	<tr>
		<td>
			<div>Start Date:<span class="red">*</span></div>
		</td>
		<td>
			<input type="text" id="demo1" name="test_date" size="25"
					value="<?php echo $frmdata['test_date'] ?>" maxlength="35"
					class="rounded datetime" readonly="readonly" />
		</td>
	</tr>
	<tr>
		<td>
			<div>End Date:<span class="red">*</span></div>
		</td>
		<td>
			<input type="text" id="demo2" name="test_date_end" size="25"
					value="<?php echo $frmdata['test_date_end'] ?>" maxlength="35"
					class="rounded datetime" readonly="readonly" />
		</td>
	</tr>
	<tr>
		<td>
			<div>Time Duration (H:M):<span class="red">*</span></div>
		</td>
		<td>
			<select name="test_hr" class="rounded">
				<option value='0'>00</option>
					<?php
						for($counter=1;$counter<=3;$counter++)
						{
							$selected='';
							if ($counter == $frmdata['test_hr']) $selected='selected';
							echo '<option value="'.$counter.'"'.$selected.'>'. $counter.'</option>';
						}
					?>
			</select> : <select name="test_min" class="rounded">
				<option value="0">00</option>
					<?php
						for($counter=1;$counter<=59;$counter++)
						{
							$selected='';
								if ($counter == $frmdata['test_min']) $selected='selected';
								echo '<option value="'.$counter.'"'.$selected.'>'. $counter.'</option>';
							}
							?>
			</select>
		</td>
	</tr>
	
	<?php $display = 'display: none;'; if($frmdata['exam_id'] != '') $display = ''; ?>
	
	<tr id="question_media_tr" style="<?php echo $display; ?>">
		<td align="">Question Media:<span class="red">*</span></td>
		<td>
			<select id="question_media" onchange="selectQuestionMedia(this.value);resetQuestionHistory(1);" class="rounded" name="question_media">
				<option value="">Select Question Media</option>
				<option value="subject" id="subject" <?php if($frmdata['question_media'] == 'subject') echo 'selected'; ?>>Subject</option>
				<option value="paper" id="paper" <?php if($frmdata['question_media'] == 'paper') echo 'selected'; ?>>Paper</option>
			</select>
		</td>
	</tr>

	<tr id="select_subject_tr" style="display:none;">
		<td align="">Subject:<span class="red">*</span></td>
		<td id="select_subject_td">
		</td>
	</tr>
	
	<tr id="select_paper_tr" style="display:none;">
		<td align="">Paper:<span class="red">*</span></td>
		<td id="select_paper_td"></td>
	</tr>
</table>

<div style="float: right;margin-right: 20px;display: none;">
	<div style="float: left;">Total Marks:&nbsp;</div>

	<div id="total_mark" style="font-weight: bold;float: right;">
	<?php 
		if($frmdata['maximum_marks'] > 0) echo $frmdata['maximum_marks'];
		else echo 0;
	?>
	</div>

	<input type="hidden" id="maximum_marks" name="maximum_marks" value="" />
</div>
<div style="clear: both;"></div>

<?php
	$show = 0;
	if($frmdata['question_media'] == 'subject')
	{
		$show = 1;
	}
	$total_subject = 0;
	if($frmdata['total_subject'] > 0)
	{
		$total_subject = $frmdata['total_subject'];
	}
	elseif ($test_subject)
	{
		$total_subject = count($test_subject);
	}
	
	$tableStyle = 'display:none;';
	if(($total_subject > 0) && ($show))
	{
		$tableStyle = '';
	}
?>
<br>
<div id='sub_tbl' align="center">
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="98%" bgcolor="#e1e1e1" class="data" style="<?php echo $tableStyle; ?>">
<thead>
  	<tr class="tblheading">
		<td width="1%">#</td>
		<td width="25%">Subject</td>
		<td width="11%">Total Questions</td>
		<td width="11%">Total Marks</td>
		<td width="11%">Show Diff. Question</td>
		<td width="17%">Action</td>
	</tr>
</thead>

<tbody id="tbl_body">
<?php
	if($total_subject > 0)
	{
		$count = 1;
		for($counter=0; $counter < $total_subject; $counter++)
		{
			if(($_POST) && ($frmdata['subject_id_'.($counter+1)] == ''))
			{
				continue;
			}
			else 
			{
				if(($count%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				} ?>
				
				<tr class="<?php echo $trClass; ?>">
					<td><?php echo $count++; ?></td>
					<td id="subject_nameTd_<?php echo $counter+1; ?>">
						<?php 
						for($counter1=0;$counter1 < count($subject);$counter1++)
						{
							if($frmdata['subject_id_'.($counter+1)])
							{
								$selected_value = $frmdata['subject_id_'.($counter+1)];
							}
							else if(isset($test_subject))
							{
								$selected_value = $test_subject[$counter]->subject_id;
							}
							
							$selected='';
							if ($subject[$counter1]->id == $selected_value)
							{
								echo stripcslashes($subject[$counter1]->subject_name);
								echo '<input type="hidden" name="subject_id_'.($counter+1).'" id="subject_id_'.($counter+1).'" value="'.$subject[$counter1]->id.'">';
								break;
							}
						}
						?>
					</td>
					<td id="subject_questionTd_<?php echo 'test'.$counter+1; ?>">
						<?php
						$total_question = '';
						$random_que_id = '';
						$custom_que_id = '';
						$group_que_id = '';
						if ($frmdata['total_question_'.($counter+1)])
						{	
							$total_question = $frmdata['total_question_'.($counter+1)];
							$random_que_id = $frmdata['random_que_id_'.($counter+1)];
							$custom_que_id = $frmdata['custom_que_id_'.($counter+1)];
							$group_que_id = $frmdata['group_que_id_'.($counter+1)];
						}
						elseif (isset($subject_total_question))
						{	
							
							$total_question = $subject_total_question[$test_subject[$counter]->subject_id];
							$random_que_id = substr($subject_random_question_set[$test_subject[$counter]->subject_id],0,strlen($subject_random_question_set[$test_subject[$counter]->subject_id])-1);
							$custom_que_id = substr($subject_custom_question_set[$test_subject[$counter]->subject_id],0,strlen($subject_custom_question_set[$test_subject[$counter]->subject_id])-1);
							$group_que_id  = substr($subject_group_question_set[$test_subject[$counter]->subject_id],0,strlen($subject_group_question_set[$test_subject[$counter]->subject_id])-1);
						}
							
						echo $total_question;
						echo '<input type="hidden" name="total_question_'.($counter+1).'" id="total_question_'.($counter+1).'" value="'.$total_question.'">';
						echo '<input type="hidden" name="random_que_id_'.($counter+1).'" id="random_que_id_'.($counter+1).'" value="'.$random_que_id.'">';
						echo '<input type="hidden" name="custom_que_id_'.($counter+1).'" id="custom_que_id_'.($counter+1).'" value="'.$custom_que_id.'">';
						echo '<input type="hidden" name="group_que_id_'.($counter+1).'" id="group_que_id_'.($counter+1).'" value="'.$group_que_id.'">';
						?>
					</td>
					<td id="subject_markTd_<?php echo $counter+1; ?>">
						<?php 
						if ($frmdata['subject_total_mark_'.($counter+1)])
							$total_marks = $frmdata['subject_total_mark_'.($counter+1)];
						elseif (isset($test_subject))
							$total_marks = $test_subject[$counter]->subject_total_marks;
							
						echo $total_marks;
						echo '<input type="hidden" name="subject_total_mark_'.($counter+1).'" id="subject_total_mark_'.($counter+1).'" value="'.$total_marks.'">';
						?>
					</td>
					<td id="subject_diff_questionTd_<?php echo $counter+1; ?>">
						<?php
						if ($frmdata['subject_diff_question_'.($counter+1)] == 'Y')
							$showDiff = 'Y';
							
						elseif (isset($test_subject) && ($test_subject[$counter]->show_subject_diff_question=='Y'))
							$showDiff = 'Y';
								 	
						else
							$showDiff = 'N';
							
						echo ($showDiff == 'Y') ? 'Yes' : 'No';
						echo '<input type="hidden" name="subject_diff_question_'.($counter+1).'" id="subject_diff_question_'.($counter+1).'" value="'.$showDiff.'">';
						?>
					</td>
					<td align="center">
						<input type="button" class="buttons rounded" onclick="editRow(this);" value="Edit" id="edit_<?php echo $counter+1; ?>" name="edit_<?php echo $counter+1; ?>" /> 
						<img onclick="deleteRow(this);" id="delete_img_<?php echo $counter+1; ?>" title="Remove" style="cursor: pointer;" src="<?php echo IMAGEURL; ?>/b_drop.png" /> 
					</td>
				</tr><?php 
			}
		}
	} 
?>
</tbody>
</table>
</div><br>

<?php if($isEdit) { ?>
<input type="hidden" name="submitEdit_form" id="submitEdit_form" value="" />
<?php } else { ?>
<input type="hidden" name="submitAdd_form" id="submitAdd_form" value="" />
<?php } ?>

<input type="hidden" name="subject_name" id="subject_name" value="" />
<input type="hidden" name="subject_total_question" id="subject_total_question" value="" />
<input type="hidden" name="subject_paper_mark" id="subject_paper_mark" value="" />
<input type="hidden" name="subject_diff_question" id="subject_diff_question" value="" />
<input type="hidden" name="total_subject" id="total_subject" value="<?php echo $total_subject; ?>" />
<input type="hidden" name="stream_id" id="stream_id" value="<?php echo $frmdata['stream_id']; ?>" />

	</div>

	<div id="question-tab" class="exam-tabs"></div>
	<div id="student-tab" class="exam-tabs">
		<div id='filtered_candidate'></div>
	</div>
</div>

<br />
<div align="center">
	<input type="button" name="<?php echo $isEdit ? 'edit' : 'add'; ?>newtest" value="Save" class="buttons" onclick="getmaxmarks();" /> 
	<?php $pagename = isset($_SESSION['pagename']) ? $_SESSION['pagename'] : 'manage'; ?>
	<input type="button" name="cancel" value="Cancel" class="buttons" onclick="location.href='<?php print CreateURL('index.php',"mod=test_master&do=$pagename");?>'" />
</div>
<br />

</form>
	
						</fieldset>
					</div>
				</div>
			</div>
		</td>
	</tr>
</table>
</div>
</body>
</html>