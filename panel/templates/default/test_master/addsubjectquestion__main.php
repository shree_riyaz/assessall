<?php
	/*****************Developed by :- Chirayu Bansal
	 Date         :- 23-aug-2011
	 Module       :- Test master
	 Purpose      :- Template for Add particular subject question
	 ***********************************************************************************/
if ($custom_que_id_set!='') {
	$_SESSION['custom_question_id_set']=$custom_que_id_set;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<script language="javascript" type="text/javascript">
function findSubjectNumofQues(subject_id, is_call_marks_function)
{
	
	var que_id_set="";
	var que_value_set="";
	var sid='';
	var random =  document.getElementById('subject_random_que_selection');

	//alert(sid+", "+gid);
	if(subject_id=='')
		subject_id = document.getElementById('subject_id').value;
	//alert(subject_id);
	if(subject_id!='')
	{
		//alert(random.checked);
		if(document.getElementById('subject_custom_que_selection').checked && document.getElementById('subject_custom_que').style.display == "")
		{
			var cust_question = document.addfrm.custom_question;
			//alert(cust_question);
			//alert("ok");
			if(typeof(cust_question)!='undefined')
			{
				if(typeof(cust_question.length)=='undefined')
				{
					if(cust_question.checked)
					{
						var cust_que_value = cust_question.value.split('_');
						var que_id = cust_que_value[0];
						var que_mark = cust_que_value[1];
						que_id_set= que_id;
						que_value_set= cust_question.value+"_check";
					}
					else
					{
						que_value_set= cust_question.value+"_uncheck";
					}
				}
				else if(cust_question.length>1)
				{
					for(var counter = 0; counter<cust_question.length; counter++)
					{
						if(cust_question[counter].checked)
						{
							var cust_que_value = cust_question[counter].value.split('_');
							var que_id = cust_que_value[0];
							var que_mark = cust_que_value[1];
							que_id_set+=que_id+",";
							que_value_set+= cust_question[counter].value+"_check,";
						}
						else
						{
							que_value_set+= cust_question[counter].value+"_uncheck,";
						}
					}
					
						que_id_set = que_id_set.substr(0, que_id_set.length-1);
							//alert(que_id_set.substr(0, que_id_set.length-1));	
				}
			}
		}

		if(random.checked)
		{
			//alert(que_id_set);
			//alert(subject_id);
			document.getElementById('subject_random_que').style.display = "";
			document.getElementById('subject_diff_que_option_tr').style.display = "";
			xajax_getquestion(sid, que_id_set, subject_id);
		}
		else
		{
			document.getElementById('subject_random_que').style.display = "none";
			document.getElementById('subject_message').innerHTML="";
			document.getElementById('subject_beg_level').innerHTML="";
			document.getElementById('subject_int_level').innerHTML="";
			document.getElementById('subject_high_level').innerHTML="";
			document.getElementById('show_subject_diff_question').checked=false;
			document.getElementById('subject_diff_que_option_tr').style.display = "none";

			var current_marks = Number(document.getElementById('subject_total_mark').innerHTML);
			//alert(que_value_set);
			if(!(is_call_marks_function))
				xajax_gettotalmarks('random_reset',current_marks,que_value_set,'','','',subject_id);
		}
	}
	else
		document.getElementById('subject_random_que').style.display = "none";
	
}
function ShowSubjectCusQues(subject_id, is_call_marks_function)
{
	var sid='';
	var custom =  document.getElementById('subject_custom_que_selection');
	if(subject_id!='' && custom.checked)
	{
		document.getElementById('subject_custom_que').style.display = "";
		xajax_getcustomquestion(sid, subject_id,'','', document.getElementById('qtyp').value);
	}
	else
	{
		document.getElementById('subject_custom_que').style.display = "none";
		document.getElementById('subject_showques').innerHTML = "";
		findSubjectNumofQues(subject_id);
		var current_marks = Number(document.getElementById('subject_total_mark').innerHTML);
		if(!(is_call_marks_function))
			xajax_gettotalmarks('custom_reset',current_marks,'','','','',subject_id);
	}
}

function subjectQuestionRestrict(val,name)
{
	//alert(name);
	var max = new Array();
	var max=name.split('_');
	var level=max[0];// question level
	var Mark=max[1]; //marks 
	var totalques=max[2];// totalques
	//alert(val+' '+totalques)
	if(isNaN(val))
	{
		alert('Please enter numeric value only.');
		document.getElementById(name).value='';
		document.getElementById(name).focus();
		return false;
	}
	else
	{
		//alert(val);
		val = parseInt(val);
		if(val>totalques)
		{
			alert("Please enter value less than or equal to "+totalques);
			var last_value = '';
			/*if(document.getElementById(name).oldvalue!='' && document.getElementById(name).oldvalue!=undefined)
			{
				alert(document.getElementById(name).oldvalue);
				last_value = document.getElementById(name).oldvalue;
			}*/
			document.getElementById(name).value=last_value;
			document.getElementById(name).focus();
			
			//return false;
			
		}
		/*else
		{
			//alert(val);
			//xajax_showtotalmarks(val,Mark,totalques,level);
			var current_marks = Number(document.getElementById('total_mark').innerHTML);
			xajax_gettotalmarks('add',current_marks,val,Mark,totalques,level);
		}*/
		var current_marks = Number(document.getElementById('subject_total_mark').innerHTML);
		var subject_id = Number(document.getElementById('subject_id').value);
		xajax_gettotalmarks('add',current_marks,val,Mark,totalques,level,subject_id);
	}
	//document.getElementById(name).focus();
	//document.addtest.'total_high_'+maxMark.value;
	//alert(totalques);
	
}

function showSubjectCustomTotal(val)
{
	//alert(val.checked);
		//xajax_customMarks(val.value);
		var current_marks = Number(document.getElementById('subject_total_mark').innerHTML);
		var subject_id = Number(document.getElementById('subject_id').value);
		if(val.checked)
		{
			//alert(current_marks);
			xajax_gettotalmarks('add',current_marks, val.value+"_check", '', '', '', subject_id);
		}
		else if(!val.checked)
		{
			xajax_gettotalmarks('add',current_marks, val.value+"_uncheck", '', '', '', subject_id);
		}
		//findSubjectNumofQues();	
}

function unselectElement(val)
{
	//alert(val);
	$('#subject_showques').find("input[value='"+val+"']").attr('checked',false);
}

function closewindow(val)
{
	
	if(val=='Save' || val=='Update')
	{
		//alert(val);
		//return;
		var cust_que_id_set='';
		var random_que_set = '';
		var msg="";
		var flag=0;
		
		var random_que_selection = document.getElementById('subject_random_que_selection');
		var custom_que_selection = document.getElementById('subject_custom_que_selection');
		var randomTd = document.getElementById('randomTd');
		var subject_total_mark = document.getElementById('subject_total_mark').innerHTML;
		var exam_subject_max_mark = document.getElementById('exam_subject_max_marks').value;
		var show_subject_diff_question = document.getElementById('show_subject_diff_question');
		
		if(!(random_que_selection.checked) && !(custom_que_selection.checked))
		{
			//alert("error");
			msg+= "Please select any question selection's option.\n";
			focusAt=random_que_selection;
			flag=1;
		}
		else
		{
			var subject_id = document.getElementById('subject_id').value;
			var cust_question = document.addfrm.custom_question;
			if(typeof(cust_question)!='undefined')
			{
				if(typeof(cust_question.length)=='undefined')
				{
					if(cust_question.checked)
					{
						var cust_que_value = cust_question.value.split('_');
						var que_id = cust_que_value[0];
						var que_mark = cust_que_value[1];
						cust_que_id_set= que_id;
						
					}
				}
				else if(cust_question.length>1)
				{
					for(var counter = 0; counter<cust_question.length; counter++)
					{
						if(cust_question[counter].checked)
						{
							var cust_que_value = cust_question[counter].value.split('_');
							var que_id = cust_que_value[0];
							var que_mark = cust_que_value[1];
							cust_que_id_set+=que_id+",";
						}
					}
					if(cust_que_id_set!='')
						cust_que_id_set = cust_que_id_set.substr(0, cust_que_id_set.length-1);
				}
			}

			if(random_que_selection.checked)
			{
				var input_element = $("#randomTd :input");
				for(var index=0; index<input_element.length; index++)
				{
					if(input_element[index].value!='')
						random_que_set+=input_element[index].id+"_"+input_element[index].value+",";
					//alert(input_element[index].id);
				}
	
				if(random_que_set!='')
					random_que_set = random_que_set.substr(0, random_que_set.length-1);
			}

			if(random_que_set=='' && cust_que_id_set=='')
			{
				msg+= "Please select questions.\n";
				if(flag==0)
					focusAt=randomTd;
				flag=2;
			}

			if(subject_total_mark!=exam_subject_max_mark)
			{
				msg+= "Subject's total marks should be equal to selected exam's subject's maximum marks which is "+exam_subject_max_mark+".\n";
				if(flag==0)
					focusAt=random_que_selection;
				flag=2;
			}
		}

		if(msg=='')
		{
			//var table = document.getElementById('tbl_body');
		    //var rowCount = table.rows.length;
		    if(val=='Save')
		    {    
			    var total_subject = document.getElementById('total_subject');
			    var rowCount = Number(total_subject.value)+1;
				//var subject_total_mark = document.getElementById('subject_total_mark').innerHTML;
				var mode="add";
				//xajax_getSubjectQuestion(subject_id, cust_que_id_set, random_que_set, subject_total_mark, rowCount);
				//addRow();
		    }
		    else
		    {
		    	var total_subject = document.getElementById('total_subject');
			    var rowCount = Number(<?= $getVars['rowno']; ?>);
				var subject_total_mark = document.getElementById('subject_total_mark').innerHTML;
				var mode="edit";
				//xajax_getSubjectQuestion(subject_id, cust_que_id_set, random_que_set, subject_total_mark, rowCount);
			}

			if(show_subject_diff_question.checked)
			{
				var subject_diff_question='Y';
			}
			else
			{
				var subject_diff_question='N';
			}
			//alert(subject_diff_question);
			//alert(random_que_set);
			//alert(subject_total_mark);
			
		    xajax_getSubjectQuestion(subject_id, cust_que_id_set, random_que_set, subject_total_mark, rowCount, mode, subject_diff_question);
			tb_remove();
		}
		else
		{
			alert(msg);
			focusAt.focus();
		}
	}
	else if(val=='Cancel')
	{
		tb_remove();
	}
	//$("#TB_window").remove();
	//alert(que_id_set);
}

function callclosefunction(val)
{
	var name = val.value;

if(name =='Cancel')
{
	closewindow(name);
}
	var total_marks=0;


	if(document.getElementById('subject_random_que_selection').checked)
	{
		var input_element = $("#randomTd :input");
		for(var index=0; index<input_element.length; index++)
		{
			if(input_element[index].value!='')
			{
				if(!input_element[index].id) continue;
				var rand_que = input_element[index].id.split('_');
				var level = rand_que[0];
				var marks = rand_que[1];
				var available_que = rand_que[2];
				total_marks+=Number(input_element[index].value*marks);
			}
			
			//if(input_element[index].value!='')
				//random_que_set+=input_element[index].id+"_"+input_element[index].value+",";
			//alert(input_element[index].id);
		}
	}
	
	if(document.getElementById('subject_custom_que_selection').checked)
	{
		
		var input_element = $("#subject_custom_que :input");
		for(var index=0; index<input_element.length; index++)
		{
			if(input_element[index].checked)
			{
				var rand_que = input_element[index].value.split('_');
				var que_id = rand_que[0];
				var marks = rand_que[1];
				var level = rand_que[2];
				total_marks+=Number(marks);
			}
		}
	}

	//if(total_marks!=0)
	
	{
		document.getElementById('subject_total_mark').innerHTML = total_marks;
	}
	//setTimeout("closewindow('"+name+"')", 200);

	closewindow(name);
}

function callclosefunction_intell(val)
{
	var name = val.value;
	
	if(name =='Cancel')
	{
		closewindow(name);
	}
	else
	{
		if(name=='Save')
		{
			var total_subject = document.getElementById('total_subject');
			var subject_id = document.getElementById('subject_id').value;
			var rowCount = Number(total_subject.value)+1;
			var subject_total_mark = document.getElementById('subject_total_marks').value;
			var subject_existing_total_mark =document.getElementById('subject_existing_total_mark').value;
			//var subject_existing_total_mark=0;
			var mode="add";
		}
		else	
		{
			var total_subject = document.getElementById('total_subject');
			var subject_id = document.getElementById('subject_id').value;
			var rowCount = Number(<?= $getVars['rowno']; ?>);
			var subject_total_mark = document.getElementById('subject_total_marks').value;
			var subject_existing_total_mark = document.getElementById('subject_existing_total_mark').value;
			var mode="edit";
		}
		//alert(subject_existing_total_mark);
		xajax_getSubjectQuestion_intell(subject_id,subject_total_mark,rowCount,mode,subject_existing_total_mark);
		tb_remove();
	}	
			
}



</script>

<?php

include_once(ROOT."/incajax.php");
$xajax->printJavascript();
if($_SESSION['total'])
{
	//unset($_SESSION['custom_total_marks']);
	//unset($_SESSION['question']);
	unset($_SESSION['total']);
	
}
if(isset($_SESSION['subject']))
{
	$_SESSION['total'] = $_SESSION['subject'];
	unset($_SESSION['subject']);
}

/*if(isset($_SESSION['total_paper_set']))
{
	unset($_SESSION['total_paper_set']);
}*/
//print_r($frmdata);
//exit;
?>
<style>
.border {
	border-width: 1px;
	border-style: solid;
	border-color: #CCCCCC;
	border-radius: 5px 5px 5px 5px;
}
</style>

</head>

<body>
<div id="outerwrapper" style="width:820px;">
	<table border="0" cellspacing="0" cellpadding="0" width="820" id="tbl_outer">
	  <tr>
		<td>
			<div id="content">
	<div id="">
		<div id="contents">
			<form id="addfrm" name="addfrm" method="post">
			<fieldset class="rounded"><legend>Add Question</legend><br/>
			<?php
				if(isset($_SESSION['error']))
				{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
				}
				if(isset($_SESSION['success']))
				{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
				}
				
				if( $getVars['intelligent']=='Y')
				{

				?>
					<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">
					  
					  <tr>
					  	<td colspan="4"><div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div></td>
					  </tr>
					  <tr>
						<td valign="top"  colspan=2>
							<div align="right">No. of questions:<span class="red">*</span></div>
						</td>
						<td colspan=2>
							<input type="text" name="subject_total_marks" id="subject_total_marks" value="<?php echo $subject_total_mark?>" />
				
						</td>
						</tr>
						
					<tr>
						<td colspan=4 align="center">
						<br>
						<?php
						if($subject_total_mark!='') 
						{
						?>
							<input type="button" name="editsub_question" id="editsub_question" value="Update" class="buttons rounded" onclick="callclosefunction_intell(this);"/>
						<?php
						}
						else
						{ 
						?>
							 <input type="button" name="addsub_question" id="addsub_question" value="Save" class="buttons rounded" onclick="callclosefunction_intell(this);"/>
						<?php
						}
						?>						
							  <input type="button" name="cancel" id="cancel" value="Cancel" class="buttons" onclick="callclosefunction_intell(this);"/>
						</td>
					</tr>
					
				<?php		
				}
				else
				{
			?>
					<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">
					  
					  <tr>
					  	<td colspan="4"><div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div></td>
					  </tr>
					  <tr>
						<td valign="top" width="20%">
							<div align="right">Question Selection:<span class="red">*</span></div>
						</td>
						<td>
							<label> <input id="subject_random_que_selection" name="subject_random_que_selection" type="checkbox" value="R" onclick="findSubjectNumofQues(document.getElementById('subject_id').value);" <?php if($random_que_id_set!='') echo "checked"; ?>/> Random</label> 
							<label> <input type="checkbox" id="subject_custom_que_selection" name="subject_custom_que_selection" value="C" onclick="ShowSubjectCusQues(document.getElementById('subject_id').value);" <?php if($custom_que_id_set!='') echo "checked"; ?> /> Custom</label>
						</td>
						<td>
							<div align="right">Maximum Marks:</div>
						</td>
						<td>
							<div style="font-weight: bold"><?php echo $exam_subject_info->subject_max_mark; ?></div>
						</td>
					  </tr>
					  <tr>
					  	<td colspan="3">
					  		<div align="right">Total Marks:</div>
					  	</td>
					  	<td>
					  		<div id="subject_total_mark" style="font-weight: bold"><?php if($subject_total_mark!='') echo $subject_total_mark; else echo 0; ?></div>
							<input type="hidden" id="subject_maximum_marks" name="subject_maximum_marks" value="" />
					  	</td>
					  </tr>
					  <tr id="subject_diff_que_option_tr" style="display: none;">
						<td width="20%"><div align="right">Show different questions from question bank?:</div></td>
						<td><input type="checkbox" name="show_subject_diff_question" id="show_subject_diff_question" value="Y" <?php if($subject_diff_question=="Y") echo "checked"; ?> /></td>
					</tr>
					  <tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
					  </tr>
					  <tr>
					  <td colspan="5">
					  <table width="100%">
					  <tr>
					  	<td colspan="" width="55%" valign="top" id="randomTd">
					  		<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td valign="top" align="left" id="subject_random_que" style="display:none;">
										<table border="0" style="" width="100%">
											<tr>
												<td valign="top" colspan="3">
													<div id="subject_message"></div>
												</td>
											</tr>
											<tr>
												<td valign="top" width="33%">
													<div id="subject_beg_level"></div>
												</td>
												<td valign="top" width="34%">
													<div id="subject_int_level"></div>
												</td>
												<td valign="top" width="33%">
													<div id="subject_high_level"></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
					  	</td>
						<td width="45%" valign="top" id="subject_custom_que" style="display:none;">
							<div id="subject_showques" style="margin-left: 25px;float: left;"></div>
						</td>
					  </tr>
					  </table>
					  </td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td colspan="3">
						  <div align="left">
					  <?php
						if($subject_total_mark!='') 
						{
						?>
							<input type="button" name="editsub_question" id="editsub_question" value="Update" class="buttons rounded" onclick="callclosefunction(this);"/>
						<?php
						}
						else
						{ 
						?>
						 <input type="button" name="addsub_question" id="addsub_question" value="Save" class="buttons rounded" onclick="callclosefunction(this);"/>
			            <?php
						} 
			            ?>
				            <input type="button" name="cancel" value="Cancel" class="buttons" onclick="callclosefunction(this);"/>
						  </div></td>
				
						<input type="hidden" name="qtyp" id="qtyp" value="N" />
						<input type="hidden" name="exam_subject_max_marks" id="exam_subject_max_marks" value="<?php echo $exam_subject_info->subject_max_mark; ?>" />
				
				<?php
				}
				?>
			
					 </tr>
					  
					</table>
				</fieldset>
				<input type="hidden" name="subject_id" id="subject_id" value="<?php echo $subject_id; ?>" />
				<input type="hidden" name="subject_existing_total_mark" id="subject_existing_total_mark" value="<?php echo $subject_total_mark?>" />
				<input type="hidden" name="intellignet" id="intellignet" value="<?php echo $getVars['intelligent']?>" />
				
			  </form>
	  
		</div><!--Div Contents closed-->
		</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->
<?php
if($subject_total_mark!='' &&  $getVars['intelligent']!='Y') 
{
?>
<script language="javascript" type="text/javascript">
ShowSubjectCusQues('<?php echo $subject_id; ?>', 'no');
setTimeout("findSubjectNumofQues('<?php echo $subject_id; ?>', 'no')", 100);
</script>
<?php
} 
//print_r($_SESSION);
?>
<table width="820" align="center">
	<tr>
		<td align="right"><b>Parakha 1.0</b></td>
	</tr>
</table>
</body>

</html>