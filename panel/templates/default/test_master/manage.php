<?php 
 /*****************Developed by :- Richa verma
	                Date         :- 5-july-2011
					Module       :- Test master
					Purpose      :- Template for Browse all test details and provide functionality to edit delete and see all question
	***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php $page_title = ($do=='archive') ? 'Manage Archive Test' : 'Manage Test'; ?>
<title><?php echo 'Online Examination - '.$page_title; ?></title>

<script language="javascript" type="text/javascript">
function deleteTest(val)
{
	var x = confirm("Are you sure you want to delete this test?");
    if (x)
    { 
        load("<?php echo ROOTURL; ?>" + "/index.php?mod=test_master&do=delete&nameID="+val);
	}
    else
    { 
		return false;
    }
}
function archiveTest(id, name, href)
{
	var pagename = document.getElementById('pagename').value;

	var action = 'archive';
	if(pagename == 'archive')
	{
		action = 'activate';	
	}

	var confrm = confirm("Do you really want to " + action + " '" + name + "' test?");
	if(confrm)
	{
		xajax_archiveTest(id);
	//	setTimeout("location.href='"+ href +"';",200);
	}
}

function informCandidates(id, is_informed, informed_count, informed_date)
{
	go = false;
	if((is_informed == true) && (informed_count > 0))
	{
		if(confirm(informed_count + ' students are already informed about this test on '+ informed_date+'.\nDo you really want to re-inform students?'))
		{
			go = true;
		}
	}
	else if(confirm('Do you really want to inform students about this test?'))
	{
		go = true;
	}

	if(go)
	{
		xajax_informCandidatesForTest(id);
	}

	return true;
}
function reload()
{
	location.href = document.URL;
}
</script>
</head>

<?php

include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.name.focus();">
<input type="hidden" value="<?php echo $do; ?>" name="pagename" id="pagename"/>
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist">
			<legend><?php echo $page_title; ?></legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			<div class="search-div">
			<fieldset class="rounded search-fieldset"><legend>Search</legend>
			<table border="0" cellspacing="1" cellpadding="3" width="100%">
			  <tr>
				<td align="center">
				Test Name:
				<input type="text" size="30" class="rounded textfield" name="name" value="<?php echo $frmdata['name'] ?>" onchange="managePageCheckIsAlphaNum(this, 'test name');" />
				</td>
			   	</tr>
				
			    <tr>
			   <td align="center">
			    	<input type="submit" class="buttons rounded" name="Submit" value="Show" />
			    	<input type="submit" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick=""/>
			    </td>
			    
			  </tr>
			 
			</table>
			</fieldset>
			</div><br />
		<?php
			if($testlist)
			{ 
		?>
			<table border="0" cellspacing="1" cellpadding="4" width="100%" style="">
			<tr>
				<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($testlist))." of ".$totalCount; ?></td>
				<td align="right"><?php echo getPageRecords();?> </td>
			</tr>
			</table>
		<?php
			}
			?>
			<div id="celebs">
				<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
				<?php
				if($testlist[0]->id!='')
				{
				?>
					  <thead>
					  <tr class="tblheading">
						<td width="1%">#</td>
						<td width="26%"><div align="center">
						<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('test_name');">Test Name
						<?php if($frmdata['orderby']=='test_name') 
							{
								print '<img src="'.ROOTURL.'/images/asc.gif">';
							} 
							elseif($frmdata['orderby']=='test_name desc')
							{
								print '<img src="'.ROOTURL.'/images/desc.gif">';
							}
						?>
						</a></div></td>
						<td width="18%"><div align="center">
						<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('exam.exam_name');">Course Name
						<?php if($frmdata['orderby']=='exam.exam_name') 
							{
								print '<img src="'.ROOTURL.'/images/asc.gif">';
							} 
							elseif($frmdata['orderby']=='exam.exam_name desc')
							{
								print '<img src="'.ROOTURL.'/images/desc.gif">';
							}
						?>
						</a></div></td>
						<td width="11%">
						<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('test_date');">From
						<?php if($frmdata['orderby']=='test_date') 
							{
								print '<img src="'.ROOTURL.'/images/asc.gif">';
							} 
							elseif($frmdata['orderby']=='test_date desc')
							{
								print '<img src="'.ROOTURL.'/images/desc.gif">';
								$frmdata['orderby']='test_date desc';
							}
						?>
						</a></td>
						<td width="11%">
						<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('test_date_end');">To
						<?php if($frmdata['orderby']=='test_date_end') 
							{
								print '<img src="'.ROOTURL.'/images/asc.gif">';
							} 
							elseif($frmdata['orderby']=='test_date_end desc')
							{
								print '<img src="'.ROOTURL.'/images/desc.gif">';
								$frmdata['orderby']='test_date_end desc';
							}
						?>
						</a></td>
						<td width="10%">
						<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('time_duration');">Duration(min)
						<?php if($frmdata['orderby']=='time_duration') 
							{
								print '<img src="'.ROOTURL.'/images/asc.gif">';
							} 
							elseif($frmdata['orderby']=='time_duration desc')
							{
								print '<img src="'.ROOTURL.'/images/desc.gif">';
							}
						?>
						</a></td>
						<td width="25%">Action</td>
						</tr>
					  </thead>
					  <tbody id="tbody">
					  
					  <?php 
					 // echo "fdknm".count($questionlist);
					 //print_r($testlist);
					 //exit; 
						$srNo=$frmdata['from'];
						$count=count($testlist);
					
					  for($counter=0;$counter<$count;$counter++) 
					  { 
					  		//echo "=".$testlist[$counter]->enable_test;
						  $id=$testlist[$counter]->id;
						  $name=stripslashes(highlightSubString($frmdata['name'], $testlist[$counter]->test_name));
						  $exam_name=stripslashes($testlist[$counter]->exam_name);
						  $stream=stripslashes($testlist[$counter]->stream_title);
						  $date=date('d-m-Y h:i:s A', strtotime($testlist[$counter]->test_date));
						  $date_end=date('d-m-Y h:i:s A', strtotime($testlist[$counter]->test_date_end));
						  $duration=$testlist[$counter]->time_duration;
						  $srNo=$srNo+1;
					  		if(($counter%2)==0)
							{
								$trClass="tdbggrey";
							}
							else
							{
								$trClass="tdbgwhite";
							}
						  echo"<tr class='".$trClass."' id='row_".$id."'><td>$srNo.</td>
							<td align='center'>
							$name
							</td>
							<td>$exam_name</td>
							
							<td>$date</td>
							<td>$date_end</td>
							<td>$duration</td>
							";
							
					 ?>
					 		<td>
							<img title="Discussion" src="<?php echo IMAGEURL ?>/icon-discussion.gif" onclick="popup('<?php echo CreateURL('index.php',"mod=test_master&do=discussion&nameID=".$id)?>')" style="cursor:pointer"/></a>&nbsp;&nbsp;
						<?php
							if($testlist[$counter]->enable_test=='Y')
							{
						?>
								<img title="Disable test" src="<?php echo IMAGEURL ?>/disable.gif" onclick="xajax_enableTest('<?php echo $id; ?>');" style="cursor:pointer" id="enableTest_<?php echo $id; ?>" /></a>&nbsp;&nbsp;
						<?php 
							}
							else
							{
						?>
								<img title="Enable test" src="<?php echo IMAGEURL ?>/right.gif" onclick="xajax_enableTest('<?php echo $id; ?>');" id="enableTest_<?php echo $id; ?>" style="cursor:pointer" /></a>&nbsp;&nbsp;
						<?php 
							}
							$title = ($do=='archive') ? 'Activate Test' : 'Archive Test';
							$image = ($do=='archive') ? '/active.gif' : '/archive.gif';
							$href = CreateURL('index.php',"mod=test_master&do=$do");
							
							$is_informed = $testlist[$counter]->is_informed;
							$informed_count = $testlist[$counter]->informed_count;
							$informed_date = date('d-m-Y h:i:s A', strtotime($testlist[$counter]->informed_date));
						?>
							<img src="<?php echo IMAGEURL ?>/preview.png" onclick="popup('<?php echo CreateURL('index.php',"mod=test_master&do=preview&id=$id&first_time=1")?>');" style="cursor:pointer" title="Preview" />&nbsp;&nbsp;
						
						<?php if($testlist[$counter]->enable_test=='Y') { ?>
							<img title="Inform Students" src="<?php echo IMAGEURL ?>/iconSendMail.png" onclick="informCandidates('<?php echo $id; ?>','<?php echo $is_informed; ?>','<?php echo $informed_count; ?>','<?php echo $informed_date; ?>');" style="cursor:pointer"/>&nbsp;&nbsp;
						<?php } else { ?>
							<img title="Please enable test first." src="<?php echo IMAGEURL ?>/iconSendMail_disable.png" />&nbsp;&nbsp;
						<?php } ?>

							<img title="<?php echo $title; ?>" src="<?php echo IMAGEURL .$image; ?>" onclick="archiveTest('<?php echo $id; ?>', '<?php echo $name; ?>','<?php echo $href; ?>');" id="archiveTest_<?php echo $id; ?>" style="cursor:pointer" /></a>&nbsp;&nbsp;

							<a href="<?php echo CreateURL('index.php','mod=test_master&do=edit&nameID='.$id) ?>" title='Edit'><img src="<?php echo IMAGEURL ?>/b_edit.png" /></a>&nbsp;&nbsp;

							<img src="<?php echo IMAGEURL ?>/b_drop.png" onclick="xajax_deletePermission(<?php echo $id; ?>, 'test_master');" style="cursor:pointer" title="Delete"></td>
							</tr>
					 
					  <?php }
					  ?>
					  </tbody>
					  <?php
					  }
					  else
					  {
						echo "<tr><td colspan='7' align='center'>(0) Record found.</td></tr>";
					  }
					  ?>
					  
					</table>
				</div>
				<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
					  <tr>
						<td width="10%">&nbsp;</td>
						<td width="54%">&nbsp;</td>
						<td width="36%">&nbsp;</td>
					  </tr>
					  <tr>
						<?php if($testlist[0]->id!='')
							{
						?>
						<td colspan="2"><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?></td>
			   			<?php }?>
						<td align="right">
							<?php 
								PaginationDisplay($totalCount);	
							?>
							</td>
					  </tr>
				</table>
			 <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
  				<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
			<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />	
  			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>