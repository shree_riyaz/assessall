<?php
/**************************************************************************************
 * 		Developed by :- Richa verma
 * 		Date         :- 4-july-2011
 * 		Module       :- candidate master
 * 		Purpose      :- Template for Add/Edit particular candidate detail
 *************************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Online Examination - Student Master</title>
	
<script>

function generatePassword()
{
	var length = 6;
	charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	retVal = "";
	for (var i = 0, n = charset.length; i < length; ++i) 
	{
		retVal += charset.charAt(Math.floor(Math.random() * n));
	}
	return retVal;
}

function getPass()
{
	chk = document.getElementById('passgen');
	if(chk.checked != '')
	{
		var first_name = document.getElementById('first_name').value.toLowerCase();;
		var candidate_id = document.getElementById('candidate_id').value;
		
	//	var pass = first_name + '_' + candidate_id;
		var pass = generatePassword();
		$('#showPass').html(pass);
		$('#auto_pass').val(pass);
		$('.password').val(pass);
		$('#showPass').show();
	}
	else
	{
		$('#showPass').html('');
		$('.password').val('');
		$('#auto_pass').val('');
		$('#showPass').hide();
	}
}
function addedu()
{
	var cls = document.getElementById('class');
	var year = document.getElementById('year');
	var pmarks = document.getElementById('pmarks');
	var mmarks = document.getElementById('mmarks');
	var omarks = document.getElementById('omarks');
	var grade = document.getElementById('grade');
	var stream = document.getElementById('stream');

	if(cls.value == '')
	{
		alert('Please select class.');
		cls.focus();
		return false;
	}
	else if(cls.value == 'other')
	{
		var other = $('#other').val();
		if(other == '')
		{
			alert('Please enter a class name.');
			$('#other').focus();
			return false;
		}
		else
		{
			var cls_value = other;
		}
	}
	else
	{
		var cls_value = cls.value;
	}
	
	if(year.value == '')
	{
		alert('Please enter year of passing.');
		year.focus();
		return false;
	}
	else if(isNum(year.value,year.id) == false)
	{
		return false;
	}
	else if(year.value.length < 4)
	{
		alert('Please enter a 4 digit year.');
		year.focus();
		return false;
	}
	else
	{
		var curr_year = new Date().getFullYear();
		if(Number(year.value) > curr_year)
		{
			alert('Please do not enter a future year.');
			year.focus();
			return false;
		}
		else if(Number(year.value) < 1950)
		{
			alert('Please enter a year greater than 1950.');
			year.focus();
			return false;
		}
	}

	if(mmarks.value == '' && omarks.value == '' && grade.value == '')
	{
		alert('Please enter marks or grade.');
		mmarks.focus();
		return false;
	}

	if(mmarks.value != '')
	{
		if(isNum(mmarks.value,mmarks.id) == false)
		{
			return false;
		}
		else if(omarks.value == '')
		{
			alert('Please enter obtained marks.');
			omarks.focus();
			return false;
		}
	}
	if(omarks.value != '')
	{
		if(isNum(omarks.value, omarks.id) == false)
		{
			return false;
		}
		else if(mmarks.value == '')
		{
			alert('Please enter maximum marks.');
			mmarks.focus();
			return false;
		}
	}

	if(mmarks.value != '' && omarks.value != '')
	{
		if(Number(mmarks.value) < Number(omarks.value))
		{
			alert('Please enter obtained marks less than or equal to maximum marks.');
			omarks.focus();
			return false;
		}
		else
		{
			mm = Number(mmarks.value);
			om = Number(omarks.value);

			pm = (om/mm)*100;
			pm = roundNumber(pm, 2);

			pmarks.value = pm;
		}
	}

	if(pmarks.value != '')
	{
	//	if(checkforPercentage(pmarks) == false)
		{
		//	return false;
		}
	}
	
/*	if(stream.value == '')
	{
		alert('Please enter stream.');
		stream.focus();
		return false;
	}
	else*/ if(isChar(stream.value,stream.id, 'stream') == false)
	{
		return false;
	}

	xajax_addedu(cls_value, year.value, pmarks.value, mmarks.value, omarks.value, grade.value, stream.value);	
	cls.focus();
}
function clearEdu()
{
	var cls = document.getElementById('class');
	var year = document.getElementById('year');
	var pmarks = document.getElementById('pmarks');
	var mmarks = document.getElementById('mmarks');
	var omarks = document.getElementById('omarks');
	var grade = document.getElementById('grade');
	var stream = document.getElementById('stream');
	
	cls.value = '';
	year.value = '';
	pmarks.value = '';
	mmarks.value = '';
	omarks.value = '';
	grade.value = '';
	stream.value = '';
}
function fillPercentageMarks()
{
	var mmarks = document.getElementById('mmarks');
	var omarks = document.getElementById('omarks');
	var pmarks = document.getElementById('pmarks');
	
	if(mmarks.value != '' && omarks.value != '')
	{
		if(Number(mmarks.value) >= Number(omarks.value))
		{
			mm = Number(mmarks.value);
			om = Number(omarks.value);

			pm = (om/mm)*100;
			pm = roundNumber(pm, 2);
			
			pmarks.value = pm;
		}
		else
		{
			pmarks.value = '';
		}
	}
	else
	{
		pmarks.value = '';
	}
}

function showedu()
{
	if($('#tbl_edu tr').length > 1)
	{
		var num = 1;
		$('.serial_edu').each(function()
			{
				$(this).html(num++);
			});
		$('#tbl_edu').show();
	}
	else
	{
		$('#tbl_edu').hide();
	}
}

function remove_edu(key)
{
	if(!confirm('Do you really want to delete this education detail?'))
	{
		return false;
	}
	xajax_remove_edu(key);
	$('tr#edu-'+key).remove();
	showedu();
}

function addexp()
{
	var company = document.getElementById('company');
	var from = document.getElementById('from');
	var to = document.getElementById('to');
	var stream = document.getElementById('ex-stream');
	var detail = document.getElementById('detail');

	if(company.value == '')
	{
		alert('Please enter organization name.');
		company.focus();
		return false;
	}
	
	if(from.value == '')
	{
		alert('Please enter from date.');
		from.focus();
		return false;
	}
	else
	{
		var fromDate = from.value.split("-");
		fromDate = new Date(fromDate[2], (fromDate[1] - 1), fromDate[0]);
	}
	
	if(fromDate > new Date())
	{		
		alert('From date can not be in future.');
		from.focus();
		return false;
	}
	
	if(to.value == '')
	{
		alert('Please enter to date.');
		to.focus();
		return false;
	}
	else
	{
		var toDate = to.value.split("-");
		toDate = new Date(toDate[2], (toDate[1] - 1), toDate[0]);
	}

	if(fromDate >= toDate)
	{		
		alert('To date must be greater than from date.');
		to.focus();
		return false;
	}

	if(toDate > new Date())
	{		
		alert('To date can not be in future.');
		to.focus();
		return false;
	}
	
/*	if(stream.value == '')
	{
		alert('Please enter stream.');
		stream.focus();
		return false;
	}
	else*/ if(isChar(stream.value,stream.id, 'stream') == false)
	{
		return false;
	}

	xajax_addexp(company.value, from.value, to.value, stream.value, detail.value);
	company.focus();
}

function clearExp()
{
	var company = document.getElementById('company');
	var from = document.getElementById('from');
	var to = document.getElementById('to');
	var stream = document.getElementById('ex-stream');
	var detail = document.getElementById('detail');
	
	company.value = '';
	from.value = '';
	to.value = '';
	stream.value = '';
	detail.value = '';
}

function showexp()
{
	if($('#tbl_exp tr').length > 1)
	{
		var num = 1;
		$('.serial_exp').each(function()
			{
				$(this).html(num++);
			});
		$('#tbl_exp').show();
	}
	else
	{
		$('#tbl_exp').hide();
	}
}

function remove_exp(key)
{
	if(!confirm('Do you really want to delete this experience detail?'))
	{
		return false;
	}
	xajax_remove_exp(key);
	$('tr#exp-'+key).remove();
	$('tr#detail-'+key).remove();
	showexp();
}

function showDetail(key)
{
	$('#detail-'+key).toggle();
}

function checkOther(cls)
{
	if(cls.value == 'other')
	{
		$('#other').show();
	}
	else
	{
		$('#other').hide();
	}
}
$(document).ready(function()
	{
		$('input:radio[name=martial_status]').change(function() {checkMarriedStatus();});
	}
);
function checkMarriedStatus()
{
	var value = $('input:radio[name=martial_status]:checked').val();
	if(value == 'S')
	{
		$('#children').val(0);
		$('#children').attr('readonly','true');
	}
	else
	{
		$('#children').removeAttr('readonly');
	}
}
</script>

</head>

<body onload="document.addcandidate.first_name.focus();showedu();showexp();checkMarriedStatus();">

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>

<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr>
	<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
</tr>
<tr>
	<td>
		<div id="content">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
			<div id="main">
				<div id="contents">
					<form id="addcandidate" name="addcandidate" method="post" action="" enctype="multipart/form-data">
						<fieldset class="rounded">
							<legend>Enter Student Details</legend>
							<?php
								if(isset($_SESSION['error']))
								{
								echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
								echo $_SESSION['error'];
								echo '</td></tr></tbody></table>';
								unset($_SESSION['error']);
								}
								if(isset($_SESSION['success']))
								{
								echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
								echo $_SESSION['success'];
								echo '</td></tr></tbody></table>';
								unset($_SESSION['success']);
								}
							?>
							
<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences" style="padding: 10px;">
<tr>
	<td colspan="4">
		<div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>
	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td width="120px">
		<div>First Name:<span class="red">*</span></div>
	</td>
	<td width="400px">
		<input name="first_name" id="first_name" type="text" class="rounded textfield" size="30" maxlength="100" value="<?php echo $frmdata['first_name']?>" onchange="isChar(this.value,this.id, 'first name');" />
	</td>
	<td width="120px">
		<div>Last Name:</div>
	</td>
	<td>
		<input name="last_name" id="last_name" type="text" class="rounded textfield" size="30" maxlength="100" value="<?php echo $frmdata['last_name']?>" onchange="isChar(this.value,this.id, 'last name');"/>
	</td>	   
</tr>

<tr>
	<td>
		<div>Contact No.:</div>
	</td>
	<td>
		<input name="contact_no" id="contact_no" type="text" class="rounded textfield" size="30" value="<?php echo $frmdata['contact_no']?>" onchange="checkContactNumber(this);" maxlength="20"/>
	</td> 
	<td>
		<div>Email:<span class="red"></span></div>
	</td>
	<td>
		<input name="email" type="text" size="30" class="rounded textfield" value="<?php echo $frmdata['email']?>" onchange="CheckEmailId(this)"/>
	</td>
</tr>

<tr>
	<td>
		<div>Username:<span class="red">*</span></div>
	</td>
	<td>
		<input name="candidate_id" id="candidate_id" type="text" class="rounded textfield" value="<?php echo $frmdata['candidate_id']?>" onchange="managePageCheckIsAlphaNum(this, 'username');"/>
	</td>
</tr>

<tr>
	<td>&nbsp;</td>
	
	<?php $display = 'display:none;'; if($frmdata['passgen']) $display = ''; ?>
	<td colspan="10">
		<input type="checkbox" <?php if($frmdata['passgen']) echo 'checked'; ?> name='passgen' onchange="getPass();" id='passgen'></input>
		<label for="passgen">Auto Generate Password</label>
		<span id='showPass' style="padding: 2px 5px; background-color: #ddd; <?php echo $display; ?>">
			<?php echo $frmdata['auto_pass']; ?>
		</span>
		<input type='hidden' name='auto_pass' id='auto_pass' value="<?php if($frmdata['passgen']) echo $frmdata['auto_pass']; ?>"/>
	</td>
</tr>

<tr>
	<td>
		<div>Password:<span class="red">*</span></div>
	</td>
	<td>
		<input name="password" type="password" value="<?php if($frmdata['passgen']) echo $frmdata['auto_pass']; ?>" class="rounded password textfield" size="30" maxlength="25" onchange="checkPassword(this);" />
	</td>
	<td>
		<div>Confirm Password:<span class="red">*</span></div>
	</td>
	<td>
		<input name="confirm_password" type="password" value="<?php if($frmdata['passgen']) echo $frmdata['auto_pass']; ?>" class="rounded password textfield" size="30" maxlength="25" onchange="checkPassword(this);"/>
	</td>
</tr> 

<tr>
	<td>
		<div>Birth Date:</div>
	</td>
	<td>	
		<input type="text" id="birth_date" readonly="readonly" name ="birth_date" value="<?php if($frmdata['birth_date'] && $frmdata['birth_date']!=''){echo $frmdata['birth_date'];} ?>" maxlength="35" size="15" class="rounded date textfield" />&nbsp;
	</td>
	
	<?php /* ?>
	<td>
		<div>State:</div>
    </td>
	<td>
		<select class="rounded" name="state_id" id="state_id" >
			<option value="" >Select State</option>
			<?php 
			if(is_array($state))
			{
				for($counter=0;$counter<count($state);$counter++)
				{
					$selected='';
					if ($state[$counter]->id == $frmdata['state_id'])
					{
						$selected='selected';
					}
					echo '<option value="'.$state[$counter]->id.'"'.$selected.'>'. ucwords(strtolower(stripslashes($state[$counter]->state_title))).'</option>';
				}
			}
			?>
		</select>
	</td>
	<?php */ ?>
</tr>

<tr>	
	<td valign="top">
		<div>Course:<span class="red">*</span></div>
	</td>
	<td>
		<select class="rounded textfield" name="exam_id[]" id="exam_id" multiple="multiple" size="5" style="width: 240px;">
			<?php 
			if(is_array($exam))
			{
				for($counter=0;$counter<count($exam);$counter++)
				{
					$selected='';
					if (in_array($exam[$counter]->id, $frmdata['exam_id']))
					{
						$selected='selected';
					}
					echo '<option value="'.$exam[$counter]->id.'"'.$selected.'>'. stripslashes($exam[$counter]->exam_name).'</option>';
				}
			}
			?>
		</select>
	</td>
	<td valign="top">
		<div>Address:</div>
	</td>
	<td>
		<textarea name="address" rows="5" cols="30" class="rounded"><?php echo $frmdata['address']; ?></textarea>
	</td>
</tr>
 
<tr>
	<td>
		<div>Marital status: </div>
	</td>
    <td>
        <input type="radio" name="martial_status" <?php if($frmdata['martial_status'] == 'M') echo 'checked'; ?> value="M" />Married				      
        <input type="radio" name="martial_status" <?php if($frmdata['martial_status'] != 'M') echo 'checked'; ?> value="S" />Single
	</td>
	<td>
		<div>Children: </div>
	</td>
    <td>
    	<input name="children" id="children" value="<?php echo $frmdata['children']; ?>" type="text" class="rounded textfield" size="2" maxlength="2" onkeyup="isNum(this.value,this.id)"/>
	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td valign="top">
		<div>Upload Resume:</div>
	</td>
	<td>
		<?php if($cand_detail->pro_resume!='') { ?>
	        <a target="_blank" href="<?=ROOTURL.'/uploadfiles/'.$cand_detail->pro_resume;?>"><?php echo $cand_detail->pro_resume; ?></a><br />
        <?php } ?>
		<input type="file" name="pro_resume" id="pro_resume" class="rounded textfield" value="<?php echo $frmdata['pro_resume']; ?>" />
		<div>Please upload resume in .DOC, .DOCX, and .PDF format. File size should not be more than 2MB.</div>
	</td>
	
	<td valign="top">Upload Image:</td>
	<td>
		<?php
		if($cand_detail->pro_image!='')
		{ ?>
	        <a class="thumbnail" href="#thumb">
	        	<?php echo $cand_detail->pro_image;	?>
	        	<span>
	        		<img  src="<?=ROOTURL.'/uploadfiles/'.$cand_detail->pro_image;?>" />
	        	</span>
	        </a><br /> <?php 
		}
		?>
		<input type="file" name="pro_image" id="pro_image" class="rounded textfield" value="<?php echo $frmdata['pro_image']; ?>" />
		<div>Please upload image in .JPG, .GIF, .PNG and .JPEG format. Image size should not be more than 2MB.</div>
	</td>
</tr>

<tr>
	<td valign="top">Upload Signature:</td>
	<td>
		<?php
		if($cand_detail->pro_sign!='')
		{ ?>
	        <a class="thumbnail" href="#thumb">
	        	<?php echo $cand_detail->pro_sign;	?>
	        	<span>
	        		<img  src="<?=ROOTURL.'/uploadfiles/'.$cand_detail->pro_sign;?>" />
	        	</span>
	        </a><br /> <?php 
		}
		?>
		<input type="file" name="pro_sign" id="pro_sign" class="rounded textfield" />
		<div>Please upload image in .JPG, .GIF, .PNG and .JPEG format. Image size should not be more than 2MB.</div>
	</td>
</tr>

<tr>
	<td valign="top">
		<div>Remark: </div>
	</td>
    <td colspan="5">
    	<textarea name="remark" class="rounded textfield" cols="50" rows="5"><?php echo $frmdata['remark'] ?></textarea>
	</td>
</tr>

<tr><td>&nbsp;</td></tr>
 
<tr>
	<td colspan="5">
  		<fieldset class="rounded" style="width: 70%;">
  			<legend>Education Details</legend>
  				<table align="center" border="0" width="100%">
  					<tr>
  						<td>Class:<span class="red">*</span></td>
  						<td>
  							<select class="rounded" name="class" id="class" onchange="checkOther(this);">
  								<option value="">Select Class</option>
  								<option value="10" <?php if($frmdata['class'] == '10') echo 'selected'; ?>>10</option>
  								<option value="12" <?php if($frmdata['class'] == '12') echo 'selected'; ?>>12</option>
  								<option value="Graduation" <?php if($frmdata['class'] == 'Graduation') echo 'selected'; ?>>Graduation</option>
  								<option value="Post Graducation" <?php if($frmdata['class'] == 'Post Graducation') echo 'selected'; ?>>Post Graducation</option>
  								<option value="other" <?php if($frmdata['class'] == 'other') echo 'selected'; ?>>Other</option>
  							</select>
  							<?php $style = 'style="display: none;"'; if($frmdata['class'] == 'other') $style=''; ?>
  							<input type="text" value="<?php echo $frmdata['other']; ?>" name="other" id="other" class="rounded" <?php echo $style; ?> />
  						</td>
  						
  						<td>&nbsp;</td>
  						
  						<td>Year of Passing:<span class="red">*</span></td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['year']; ?>" name="year" id="year" maxlength='4' class="rounded"/>
  						</td>
  						
  					</tr>
  					<tr>
  						<td>Maximum Marks:</td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['mmarks']; ?>" id="mmarks" name="mmarks" maxlength="4" class="rounded" onblur="fillPercentageMarks();" />
  						</td>
  						
  						<td>&nbsp;</td>
  						
  						<td>Obtained Marks:</td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['omarks']; ?>" id="omarks" name="omarks" maxlength="4" class="rounded" onblur="fillPercentageMarks();" />
  						</td>
  						
  					</tr>
  					<tr>
  						<td>Percentage Marks:</td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['pmarks']; ?>" id="pmarks" name="pmarks" class="rounded" readonly="readonly" />
  						</td>
  						
  						<td>&nbsp;</td>
  						
  						<td>Grade:</td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['grade']; ?>" name="grade" id="grade" maxlength="2" class="rounded"/>
  						</td>
  						
  					</tr>
  					<tr>
  						<td>Stream:</td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['stream']; ?>" name="stream" id="stream" class="rounded"/>
  						</td>
  						
  					</tr>
  					<tr>
  						<td colspan="10" align="center">
  							<input type="button" class="buttons rounded" value="Add" onclick="addedu();"></input>
  						</td>
  					</tr>
  					<tr><td>&nbsp;</td></tr>
  					
  					<tr>
  						<td colspan="10">
  							<table style="display: none;" border="0" cellspacing="1" cellpadding="4" id="tbl_edu" width="100%" bgcolor="#e1e1e1" class="data">
								<thead>
	  							<tr class="tblheading">
	  								<th>S.No.</th>
	  								<th>Class</th>
	  								<th>Passing Year</th>
	   								<th>Maximum Marks</th>
	  								<th>Obtained Marks</th>
	  								<th>Marks(%)</th>
	  								<th>Grade</th>
	  								<th>Stream</th>
	  								<th>Action</th>
	  							</tr>
	  							</thead>
	  							<tbody>
	  								<?php 
	  								$num = 1;
	  								for($count=0;$count <= $_SESSION['cand_edu']['count_edu'];$count++)
	  								{ 
	  									if($_SESSION['cand_edu']['class_'.$count] != '')
	  									{ ?>
	  										<tr class="tdbgwhite" id="edu-<?php echo $count; ?>">
	  										
	  											<td class='serial_edu'><?php echo $num++; ?>
	  											<td><?php echo $_SESSION['cand_edu']['class_'.$count]; ?></td>
	  											<td><?php echo $_SESSION['cand_edu']['year_'.$count]; ?></td>
	  											<td><?php echo $_SESSION['cand_edu']['mmarks_'.$count]; ?></td>
	  											<td><?php echo $_SESSION['cand_edu']['omarks_'.$count]; ?></td>
	  											<td><?php echo $_SESSION['cand_edu']['pmarks_'.$count]; ?></td>
	  											<td><?php echo $_SESSION['cand_edu']['grade_'.$count]; ?></td>
	  											<td><?php echo $_SESSION['cand_edu']['stream_'.$count]; ?></td>
	  											<td><a onclick='remove_edu(<?php echo $count; ?>);' class="links">Remove</a></td>
		  									</tr><?php
	  									}	
	  								}
	  								?>
	  							</tbody>
  							</table>
  						</td>
  					</tr>
  				</table>
  		</fieldset>	
  	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
  	<td colspan="5">
  		<fieldset class="rounded" style="width: 70%;">
  			<legend>Experience Details</legend>
  				<table align="center" border="0" width="100%">
  					<tr>
  						<td>Organisation:<span class="red">*</span></td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['company']; ?>" name="company" id="company" class="rounded" />
  						</td>
  						
  						<td>&nbsp;</td>
  						
  						<td>From Date:<span class="red">*</span></td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['from']; ?>" readonly="readonly" name="from" id="from" class="rounded date"/>
  						</td>
  						
  					</tr>
  					<tr>
  						<td>To Date:<span class="red">*</span></td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['to']; ?>" readonly="readonly" id="to" name="to"  class="rounded date"/>
  						</td>
  						
  						<td>&nbsp;</td>
  						
  						<td>Stream:</td>
  						<td>
  							<input type="text" value="<?php echo $frmdata['ex-stream']; ?>" name="ex-stream" id="ex-stream" class="rounded"/>
  						</td>
  						
  					</tr>
					<tr>
						<td valign='top'>Details:</td>
						<td colspan="8">
							<textarea name='detail' id='detail' class='rounded' cols='50' rows='4'><?php echo $frmdata['detail']; ?></textarea>
						</td>
					</tr>
  					<tr>
  						<td colspan="10" align="center">
  							<input type="button" class="buttons rounded" value="Add" onclick="addexp();"></input>
  						</td>
  					</tr>
  					<tr><td>&nbsp;</td></tr>
  					
  					<tr>
  						<td colspan="10">
  							<table style="display: none;" border="0" cellspacing="1" cellpadding="4" id="tbl_exp" width="100%" bgcolor="#e1e1e1" class="data">
								<thead>
	  							<tr class="tblheading">
	  								<th>S.No.</th>
	  								<th>Organisation</th>
	  								<th>From Date</th>
	  								<th>To Date</th>
	  								<th>Stream</th>
	  								<th>Action</th>
	  							</tr>
	  							</thead>
	  							<tbody>
	  								<?php 
	  								$num = 1;
	  								
	  								for($count=0;$count <= $_SESSION['cand_exp']['count_exp'];$count++)
	  								{ 
	  									if($_SESSION['cand_exp']['company_'.$count] != '')
	  									{ ?>
	  										<tr class="tdbgwhite" id="exp-<?php echo $count; ?>">
	  										
	  											<td class='serial_exp'><?php echo $num++; ?>
	  											<td><?php echo $_SESSION['cand_exp']['company_'.$count]; ?></td>
	  											<td><?php echo $_SESSION['cand_exp']['from_'.$count]; ?></td>
	  											<td><?php echo $_SESSION['cand_exp']['to_'.$count]; ?></td>
	  											<td><?php echo $_SESSION['cand_exp']['stream_'.$count]; ?></td>
	  											<td>
													<a onclick='showDetail(<?php echo $count; ?>);' class="links">Details</a>&nbsp;&nbsp;
													<a onclick='remove_exp(<?php echo $count; ?>);' class="links">Remove</a>
												</td>
											</tr>
	  										<tr id="detail-<?php echo $count; ?>" class="tdbggrey" style='display:none;'>
	  											<td valign="top">Detail</td>
												<td colspan='8' align="left"><?php echo $_SESSION['cand_exp']['detail_'.$count]; ?> </td>
		  									</tr><?php
	  									}	
	  								}
	  								?>
	  							</tbody>
  							</table>
  						</td>
  					</tr>
  				</table>
  		</fieldset>
  	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td>&nbsp;</td>
	<td colspan="3">
	  <div align="left">
	  	<?php if($isEdit) { ?>
	  	
	    <input type="submit" name="editCandidate" value="Update" class="buttons rounded" />
	    
	  	<?php } else { ?>
	    
	    <input type="submit" name="addCandidate" value="Save" class="buttons rounded" />
	    
	  	<?php } ?>
		
		<input type="button" name="cancel" value="Cancel" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=candidate_master&do=manage'"/>
	  </div>
	</td>
</tr>
</table>

						</fieldset><br />
					</form>
				</div><!--Div Contents closed-->
			</div><!--Div main closed-->
		</div><!--Content div closed-->
	</td>
</tr>
</table>	
</div><!--Outer wrapper closed-->

</body>
</html>