<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Link Student </title>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body>

<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr>
	<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
</tr>
<tr>
	<td>
		<div id="content">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
		
			<div id="main">
				<div id="contents">
				<form action="" method="post" name="frmlist" id="frmlist">
					<?php 
					// Show particular Messages
					if(isset($_SESSION['error']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
						echo $_SESSION['error'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['error']);
					}
					if(isset($_SESSION['success']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
						echo $_SESSION['success'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['success']);
					}
					?>
					<br />
					<fieldset class="rounded" style="width: 50%;">
						<legend>Link Student</legend>
				
<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">
<tr><td>&nbsp;</td></tr>
<tr>
	<td colspan="4">
		<div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>
	</td>
</tr>
<tr>
	<td width="" align="right">Student Email:</td>
	<td width="">
		<input name="email" type="text" size="30" class="rounded textfield" value="<?php echo $frmdata['email']?>" onchange="CheckEmailId(this);" />
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td colspan="3">
		<div align="left">
			<input type="submit" name="linkCandidate" id="linkCandidate" value="Link Student" class="buttons rounded" />
			<input type="button" name="cancel" value="Cancel" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=candidate_master&do=manage'"/>
		</div>
	</td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

					</fieldset>
  				</form>
				</div><!--Div Contents closed-->
			</div><!--Div main closed-->
		</div><!--Content div closed-->
	</td>
</tr>
</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>