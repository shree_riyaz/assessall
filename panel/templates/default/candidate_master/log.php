<?php
 /*******************************************************************************************
  * 			Developed by :- Ashwini Agarwal
  * 			Date         :- June 8, 2012
  * 			Module       :- Candidate Log
  * 			Purpose      :- Template for Browse all candidate log details.
  *******************************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Student Log Details</title>
<script language="javascript" type="text/javascript">
</script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.name.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	<tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	</tr>
	
	<tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				<div id="main">
					<div id="contents">
						<form action="" method="post" name="frmlist" id="frmlist" enctype="multipart/form-data">
								<legend>Student Log Details</legend> <?php 
								
								// Show particular Messages
								if(isset($_SESSION['error']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td valign="top" align="left" style="padding:3px 3px 3px 3px;color:red;">';
									echo $_SESSION['error'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['error']);
								}
								if(isset($_SESSION['warning']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="left" style="padding:3px 3px 3px 3px;">';
									echo $_SESSION['warning'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['warning']);
								}
								if(isset($_SESSION['success']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
									echo $_SESSION['success'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['success']);
								} ?>
								
								<div class="search-div">
								<fieldset class="rounded search-fieldset"><legend>Search</legend>
									<table border="0" cellspacing="1" cellpadding="3" width="100%">
									<tr>	
										<td align="right">Name:</td>
										<td>
											<input type="text" size="20" class="rounded textfield" name="name" 
											id="name" value="<?php echo $frmdata['name'] ?>" 
											onchange="managePageCheckIsChar(this.value,this.id, 'name');" />
										</td>
									</tr>
									<tr>
										<td align="right">Log Type:</td>
										<td>
											<select name="log_type" id="log_type" class="rounded">
												<option value="">Select Log Type</option> <?php 
												foreach ($log_type as $log_t)
												{ 
													$selected = ($log_t == $frmdata['log_type']) ? "selected='selected'" : ''; ?>
													<option value="<?php echo $log_t; ?>" <?php echo $selected; ?>><?php echo $log_t; ?></option> <?php
												} ?>
											</select>
										</td>
									</tr>
									
									<tr>
			    						<td colspan="6" align="center">
			      							<input type="submit" class="buttons rounded" name="Submit" value="Show" />
			      							<input type="submit" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick=""/>
			    						</td>
			  						</tr>
									
									</table>
								</fieldset>
								</div>
								
								<?php 
								
								if($candidatelog)
								{ ?>
									<table border="0" cellspacing="0" cellpadding="4" width="100%" style="" align="right">
									<tr>
										<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($candidatelog))." of ".$totalCount; ?></td>
										<td align="right"><?php echo getPageRecords();?> </td>
									</tr>
									</table>
									<br/> <?php
								} ?>
								<br/>
								
								<div id="celebs">
									<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data"> <?php 
									
									if($candidatelog[0]->id!='')
									{ ?>
										<thead>
											<tr class="tblheading">
												<td width="1%">#</td>
												<td width="50%">
													<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('first_name');">Name <?php 
													
													if($frmdata['orderby']=='first_name') 
													{
														print '<img src="'.ROOTURL.'/images/asc.gif">';
													} 
													elseif($frmdata['orderby']=='first_name desc')
													{
														print '<img src="'.ROOTURL.'/images/desc.gif">';
													}	?>
													</a>
												</td>
												<td>
													<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('log_type');">Log Type <?php 

													if($frmdata['orderby']=='log_type') 
													{
														print '<img src="'.ROOTURL.'/images/asc.gif">';
													} 
													elseif($frmdata['orderby']=='log_type desc')
													{
														print '<img src="'.ROOTURL.'/images/desc.gif">';
													}	?>
													
													</a>
												</td>
												<td width="20%">
													<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('log.created_date');">Log Date <?php 

													if($frmdata['orderby']=='log.created_date') 
													{
														print '<img src="'.ROOTURL.'/images/asc.gif">';
													} 
													elseif(($frmdata['orderby']=='log.created_date desc') || ($frmdata['orderby']==''))
													{
														$frmdata['orderby'] = 'log.created_date desc';
														print '<img src="'.ROOTURL.'/images/desc.gif">';
													}	?>
												
													</a>
												</td>
											</tr>
										</thead>
										
										<tbody> <?php 
										
										$srNo=$frmdata['from'];
										$count=count($candidatelog);
										
										for($counter=0;$counter<$count;$counter++) 
										{ 
											$id=$candidatelog[$counter]->id;
											$fname=ucfirst(strtolower(stripslashes($candidatelog[$counter]->first_name)));
											$lname=ucfirst(strtolower(stripslashes($candidatelog[$counter]->last_name)));
											$name=$fname.' '.$lname;
											$log_type = stripslashes($candidatelog[$counter]->log_type);;
											$log_date=stripslashes($candidatelog[$counter]->created_date);
											$srNo=$srNo+1;
											
											if(($counter%2)==0)
											{
												$trClass="tdbggrey";
											}
											else
											{
												$trClass="tdbgwhite";
											} ?>
											
											<tr class='<?= $trClass; ?>'>
												<td><?php echo $srNo; ?></td>
												<td><?php echo highlightSubString($frmdata['name'], $name); ?></td>
												<td><?php echo $log_type; ?></td>
												<td><?php echo $log_date; ?></td>
											</tr> <?php 
										} ?>
										
										</tbody> <?php 
									}
									else
									{
										echo "<tr><td colspan='10' align='center'>(0) Record found.</td></tr>";
									} ?>
											  
									</table>
									
								</div><!-- Div celebs closed  -->
								
								<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
									<tr>
										<td width="10%">&nbsp;</td>
										<td width="54%">&nbsp;</td>
										<td width="36%">&nbsp;</td>
									</tr>

									<tr><?php		
										if($candidatelog[0]->id!='')
										{	?>
											<td colspan="2"><?php 
												print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?>
											</td> <?php 
										}?>
										
										<td align="right"><?php PaginationDisplay($totalCount);	?></td>
									</tr>
								</table>
								
							
							<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
							<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
							<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />	
							<input name="page_name" type="hidden" value="log" />	
			  			
			  			</form>
					</div><!--Div Contents closed-->
				</div><!--Div main closed-->
			</div><!--Content div closed-->
		</td>
	</tr>
	
	</table>
</div><!--Outer wrapper closed-->

</body>
</html>