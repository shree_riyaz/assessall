<?php
/*****************************************************************************************
 * 		Developed by :- Ashwini Agarwal
 * 		Date         :- November 2, 2012
 * 		Module       :- Feedback
 * 		Purpose      :- Template for Browse all feedback details.
 ****************************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Feedback</title>

<script language="javascript" type="text/javascript">
function deleteFeed(val)
{
	var x = confirm("Are you sure you want to delete this feedback?");
    if (x)
    { 
		load("<?php echo ROOTURL; ?>" + "/index.php?mod=feedback&do=delete&nameID="+val);
    }
    else
    { 
		return false;
    }
}

function showReplyForm()
{
	document.getElementById('reply-btn').style.display = 'none';
	document.getElementById('reply-form').style.display = '';
}

</script>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<fieldset class="rounded"><legend>Feedback Details</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['warning']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="left" style="padding:3px 3px 3px 3px;">';
				echo $_SESSION['warning'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['warning']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>

<div id="feed-details" style="width: 80%;margin: 0 auto;">
	<table width="100%">
	
		<tr><td>&nbsp;</td></tr>
		
		<tr>
			<td width="20%">Student:</td>
			<td><?php echo ucwords($feedback->candidate_name); ?></td>
		</tr>
		
		<tr>
			<td>Test:</td>
			<td><?php echo ucwords($feedback->test_name); ?></td>
		</tr>
		
		<tr>
			<td>Test Feedback:</td>
			<td><?php echo nl2br($feedback->feed_exam); ?></td>
		</tr>
		
		<tr>
			<td>Software Feedback:</td>
			<td><?php echo nl2br($feedback->feed_software); ?></td>
		</tr>
		
		<tr>
			<td>Feedback Date:</td>
			<td><?php echo date('d-m-Y h:i A', strtotime($feedback->created_date)); ?></td>
		</tr>
		
		<tr><td>&nbsp;</td></tr>
		
		<?php if($reply && (count($reply) > 0)) { ?>
		<tr>
			<td colspan="4">
				<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
				<thead>
					<tr class="tblheading">
						<th width="1%">#</th>
						<th width="15%">Reply By</th>
						<th width="70%">Message</th>
						<th width="14%">Reply Date</th>
					</tr>
				</thead>
				<?php 
					$counter == 0;
					foreach($reply as $r)
					{ 
						$srNo=$srNo+1;
						if(($counter%2)==0)
						{
							$trClass="tdbggrey";
						}
						else
						{
							$trClass="tdbgwhite";
						}
					?>
						
						<tr class='<?php echo $trClass; ?>'>
							<td><?php echo $srNo; ?></td>
							<td><?php echo ucwords($r->admin_name); ?></td>
							<td align="left"><?php echo nl2br($r->message); ?></td>
							<td><?php echo date('d-m-Y h:i A', strtotime($r->created_date)); ?></td>
						</tr>
						
					<?php
					}
				?>
				</table>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		
		<?php } ?>
				
		<tr id="reply-btn" style="<?php echo $mail_error ? 'display:none;' : ''; ?>">
			<td align="right" colspan="4">
				<input type="button" value="Reply" class="buttons" onclick="showReplyForm();" />
				<input type="button" name="cancel" value="Go Back" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=feedback&do=manage'"/>
			</td>
		</tr>
		
		<tr style="<?php echo $mail_error ? '' : 'display:none;'; ?>" id="reply-form">
			<td valign="top">Message:<span class="red">*</span></td>
			<td colspan="4">
				
				<form action="" method="post" name="frmmail" id="frmmail">
					<?php 
						if (($mail_error) && ($frmdata['mailmessage'] ==''))
						{
							$editor->value=stripslashes($frmdata['mailmessage']);
						}
						else
						{	
							$editor->value='<p><font face="Calibri">&nbsp;</font></p>';
						}
						$editor->display('100%', 300); 
					?>
					
					<br></br>
					<div align="right">
						<input type="submit" value="Send Message" name="sendmail" class="buttons" />
						<input type="button" name="cancel" value="Cancel" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=feedback&do=manage'"/>
					</div>
					
				</form>
				
			</td>
		</tr>
		
		<tr><td>&nbsp;</td></tr>
	</table>
</div>

			</fieldset>
  			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>