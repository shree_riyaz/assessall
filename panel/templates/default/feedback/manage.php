<?php
/*****************************************************************************************
 * 		Developed by :- Ashwini Agarwal
 * 		Date         :- November 2, 2012
 * 		Module       :- Feedback
 * 		Purpose      :- Template for Browse all feedback details.
 ****************************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Feedback</title>

<script language="javascript" type="text/javascript">
function deleteFeed(val)
{
	var x = confirm("Are you sure you want to delete this feedback?");
    if (x)
    { 
		load("<?php echo ROOTURL; ?>" + "/index.php?mod=feedback&do=delete&nameID="+val);
    }
    else
    { 
		return false;
    }
}

</script>

<style>
tr.unread
{
	background-color: #fff;
	font-weight: bold;
}
</style>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.candidate_name.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<legend>Manage Feedback</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['warning']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="left" style="padding:3px 3px 3px 3px;">';
				echo $_SESSION['warning'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['warning']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			
			<div class=" search-div">
			<fieldset class="rounded search-fieldset"><legend>Search</legend>
				<table border="0" cellspacing="1" cellpadding="3" width="100%">
					<tr>
			    		<td align="right">Student:</td>
						<td>
							<input type="text" class="rounded textfield" size="20" name="candidate_name" id="candidate_name" value="<?php echo $frmdata['candidate_name'] ?>" onchange="managePageCheckIsChar(this.value,this.id, 'student name');" />
			    		</td>
			    	</tr>
			    	<tr>
			    		<td align="right">Test:</td>
				      	<td>
				      		<input type="text" size="20" class="rounded textfield" name="test_name" id="test_name" value="<?php echo $frmdata['test_name'] ?>" onchange="managePageCheckIsChar(this.value,this.id, 'test name');" />
				      	</td>
					</tr>
					<tr>
			    		<td colspan="6" align="center">
			      			<input type="submit" class="buttons rounded" name="Submit" value="Show" />
							<input type="submit" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick=""/>
			    		</td>
		  			</tr>
				</table>
			</fieldset>
			</div>
		
		<?php
			if($list)
			{ 
		?>
		<table border="0" cellspacing="0" cellpadding="4" width="100%" style="" align="right">
		<tr>
			<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($list))." of ".$totalCount; ?></td>
			<td align="right"><?php echo getPageRecords();?> </td>
		</tr>
		</table>
		
		<br/>
		<?php
			} 
		?>
		<br/>
			<div id="celebs">
			<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
			<?php 
			if($list[0]->id != '')
				{
			?>	  <thead>
				  <tr class="tblheading">
					<td width="1%">#</td>
					<td width="30%">
					<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('candidate_name');">Student
					<?php if($frmdata['orderby']=='candidate_name') 
						{
							print '<img src="'.ROOTURL.'/images/asc.gif">';
						} 
						elseif($frmdata['orderby']=='candidate_name desc')
						{
							print '<img src="'.ROOTURL.'/images/desc.gif">';
						}
					?>
					</a></td>
					<td width="30%">
					<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('test_name');">Test
					<?php if($frmdata['orderby']=='test_name') 
						{
							print '<img src="'.ROOTURL.'/images/asc.gif">';
						} 
						elseif($frmdata['orderby']=='test_name desc')
						{
							print '<img src="'.ROOTURL.'/images/desc.gif">';
						}
					?>
					</a></td>
					<td width="15%">Date</td>
					<td width="9%">Actions</td>
				  </tr>
				  </thead>
				  <tbody>
				  
			  <?php 
				$srNo=$frmdata['from'];
				$count=count($list);
				
					for($counter=0;$counter<$count;$counter++) 
					{ 
						$id=$list[$counter]->id;
						$cname=$list[$counter]->candidate_name;
						$tname=$list[$counter]->test_name;
						
						$srNo=$srNo+1;
						if(($counter%2)==0)
						{
							$trClass="tdbggrey";
						}
						else
						{
							$trClass="tdbgwhite";
						}
						
						$unread = '';
						if($list[$counter]->is_read == 0)
						{
							$unread = 'unread';
						}
					?>
						<tr class='<?= $trClass.' '.$unread; ?>'>
							<td><?php echo $srNo; ?></td>
							<td><?php echo highlightSubString($frmdata['candidate_name'], $cname); ?></td>
							<td><?php echo highlightSubString($frmdata['test_name'], $tname); ?></td>
							<td><?php echo date('d-m-Y h:i A', strtotime($list[$counter]->created_date)); ?></td>
							<td>
								<a href='<?php echo CreateURL('index.php','mod=feedback&do=preview&nameID='.$list[$counter]->id); ?>' title='View And Reply Feedback'><img src="<?php echo IMAGEURL; ?>/note_view.gif" /></a>&nbsp;&nbsp;
								<a href='javascript: void(0);' title='Delete'><img src="<?php echo IMAGEURL; ?>/b_drop.png" onclick="deleteFeed('<?php echo $id; ?>');" /></a>
							</td>
						</tr>
					<?php 
					}
				 }
					else
					{
						echo "<tr><td colspan='10' align='center'>(0) Record found.</td></tr>";
					}
				?>
				  
				  </tbody>
				</table>
			</div>
			<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
				  <tr>
					<td width="10%">&nbsp;</td>
					<td width="54%">&nbsp;</td>
					<td width="36%">&nbsp;</td>
				  </tr>
				  <tr>
				  <?php 
					if($list[0]->id!='')
					{
					?>
					<td colspan="2"><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?></td>
		   			<?php }?>
					<td align="right">
						<?php PaginationDisplay($totalCount);	?>
						</td>
				  </tr>
			</table>
			<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
			<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
			<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />
  		</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>