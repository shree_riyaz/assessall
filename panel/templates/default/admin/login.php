<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Online Examination - Admin Panel</title>
</head>

<body onload="document.loginform.loginid.focus();">
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	
	<tr><td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td></tr>
	
	<tr>
		<td>
			<div id="content" style="height:auto;">
				<div class="border_blue" style="text-align: center;">
					<h2>Online Examination Administration Login</h2>
					<?php 
					// Show particular Messages
					if(isset($_SESSION['error']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
						echo $_SESSION['error'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['error']);
					}
					?>
					
<form id="loginform" name="loginform" method="post" action="">
	<table width="343" border="0" cellpadding="2" cellspacing="2" align="center" id="logintable" style="border:1px solid #ccc;">
	<tr>
		<td height="30" colspan="4" bgcolor="#6ba6d0">
			<div align="center" style="color:#FFFFFF; font-weight:bold; font-size:14px;">Login</div>
		</td>
	</tr>
	
	<tr><td colspan="4">&nbsp;</td></tr>
					
	<tr>
		<td width="106">
			<div align="right"><span class="style3">Username </span></div>
		</td>
		<td width="226" colspan="3" align="left">
			<input name="loginid" id="loginid" type="text" size="25" class="rounded textfield" value="<?php if(isset($frmdata['loginid'])) echo $frmdata['loginid']; ?>" />
		</td>
	</tr>
	<tr>
		<td>
			<div align="right"><span class="style3">Password </span></div>
		</td>
		<td colspan="3" align="left">
			<input name="password" id="password" type="password" size="25" class="rounded textfield" />
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td colspan="4" align="center">
		  	<input type="submit" name="login" value="Login"  class="buttons rounded"  />&nbsp;
			<input type="reset" name="reset" value="Reset" class="buttons rounded" onclick="location.href=document.URL;"/>
		</td>
	</tr>
	
	<tr><td colspan="4" align="center">&nbsp;</td></tr>
	</table>
</form>
	
				</div>
			</div><!--Content div closed-->
		</td>
	</tr>
	</table>	
</div><!--Outer wrapper closed-->
<?php include_once(CURRENTTEMP."/"."footer.php"); ?>
</body>
</html>