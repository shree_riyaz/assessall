<?php
 /*****************Developed by :- Chirayu Bansal
	            Date         :- 20-aug-2011
				Module       :- Examination
				Purpose      :- Template for Browse all Examination details and provide functionality to edit and delete 
	***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Examination Details</title>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.exam_name_manage.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<legend>Course Details</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			
			<div class=" search-div">
			<fieldset class="rounded search-fieldset"><legend>Search</legend>
				<table border="0" cellspacing="1" cellpadding="3" width="100%">
			  	<tr>
			  		<td align="right">Course Name:</td>
			   		<td>
			   			<input type="text" name="exam_name_manage" id="exam_name_manage" class="rounded textfield" value="<?php echo $frmdata['exam_name_manage']; ?>" onchange="managePageCheckIsAlphaNum(this, 'course name');"/>
			    	</td>
			    </tr>
			   	<tr>
			   		<td colspan="6" align="center">
			   			<input type="submit" class="buttons rounded" name="Submit" value="Show" />
			   			<input type="submit" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick=""/>
			   		</td>
			  	</tr>
			  
				</table>
			</fieldset>
			</div><br />
		
<?php if($examlist)	{ ?>

<table border="0" cellspacing="1" cellpadding="4" width="100%" style="">
<tr>
	<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($examlist))." of ".$totalCount; ?></td>
	<td align="right"><?php echo getPageRecords();?> </td>
</tr>
</table>
<?php }	?>
			
<div id="celebs">
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">

<?php if($examlist)	{ ?>

<thead>
<tr class="tblheading">
	<td width="1%">#</td>
	<td width="25%">
		<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('exam_name');">Course Name
			<?php 
			if($frmdata['orderby']=='exam_name') 
			{
				print '<img src="'.ROOTURL.'/images/asc.gif">';
				$frmdata['orderby']='exam_name';
			} 
			elseif($frmdata['orderby']=='exam_name desc')
			{
				print '<img src="'.ROOTURL.'/images/desc.gif">';
			}
			?>
		</a>
	</td>
	<td width="20%">Subjects</td>
	<td width="7%">Actions</td>
</tr>
</thead>

<tbody>
					  
<?php
	$srNo=$frmdata['from'];
	$count=count($examlist);
					
	for($counter=0;$counter<$count;$counter++) 
	{ 
		$srNo=$srNo+1;
		$exam_subject = getSubjectNameByExamId($examlist[$counter]->id);
		$subjectStr = "";
		if($exam_subject)
		{
			for($counter1=0; $counter1<count($exam_subject); $counter1++)
			{
				$subjectStr.= stripcslashes($exam_subject[$counter1]->subject_name).", ";
			}
			$subjectStr = substr($subjectStr,0,strlen($subjectStr)-2);
		}
		else
		{
			$subjectStr='-';
		}
								
		if(($counter%2)==0)
		{
			$trClass="tdbggrey";
		}
		else
		{
			$trClass="tdbgwhite";
		} ?>
		
		<tr class='<?php echo $trClass; ?>'>
			<td><?php echo $srNo; ?></td>
			<td><?php echo stripslashes(highlightSubString($frmdata['exam_name_manage'], $examlist[$counter]->exam_name)); ?></td>
			<td><?php echo $subjectStr; ?></td>
			<td>
				<a href="<?php echo CreateURL('index.php','mod=examination&do=edit&nameID='.$examlist[$counter]->id); ?>" title='Edit'><img src="<?=IMAGEURL ?>/b_edit.png" /></a>&nbsp;&nbsp;
				<a style="cursor: pointer;" title='Delete'><img src="<?=IMAGEURL ?>/b_drop.png" onclick="xajax_deletePermission(<?php echo $examlist[$counter]->id; ?>, 'examination');" /></a>
			</td>
		</tr> <?php 
	}
}
else
{
	echo "<tr><td colspan='7' align='center'>(0) Record found.</td></tr>";
}
?>
  
</tbody>
</table>
</div>

<?php if($examlist) { ?>

<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
<tr>
	<td colspan="2"><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?></td>
	<td align="right"><?php PaginationDisplay($totalCount);	?></td>
</tr>
</table>
<?php }?>
			<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
			<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
			<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />	
  			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>