<?php 
/**
 *	@author : Ashwini Agarwal
 *	@desc	: Common form to add/edit exam.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Course</title>

<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/jquery.tabs.css' />

<style>
#copy-tab
{
	display: none;
	text-align: left;
	background: #f5f5f5;
	padding: 15px 15px 2px;
	-moz-border-radius-bottomleft: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-khtml-border-bottom-left-radius: 10px;
	border-bottom-left-radius: 10px;
	-moz-border-radius-bottomright: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-khtml-border-bottom-right-radius: 10px;
	border-bottom-right-radius: 10px;
	-moz-border-radius-topleft: 10px;
	-webkit-border-top-left-radius: 10px;
	-khtml-border-top-left-radius: 10px;
	border-top-left-radius: 10px;
}
#copy-link
{
	padding: 5px 20px;
	color: #1C94C4;
	background: #f5f5f5;
	font-weight: bold;
	border: none;
	-moz-border-radius-topright: 10px;
	-webkit-border-top-right-radius: 10px;
	-khtml-border-top-right-radius: 10px;
	border-top-right-radius: 10px;
	-moz-border-radius-topleft: 10px;
	-webkit-border-top-left-radius: 10px;
	-khtml-border-top-left-radius: 10px;
	border-top-left-radius: 10px;
}
#copy-link a
{
	color: #1C94C4;
	font-weight: bold;
	text-decoration: none;
}
</style>

<script language="javascript" type="text/javascript">
$(function() 
{ 
	$('#tabs').tabs();
	$('#tabs').tabs('option', 'active', <?php echo $_SESSION['select_tab']; ?>);
	$("#tabs").tabs({
		activate : function(event, ui) 
	    {
			showExtraInfo(ui.newTab.index());
		}
	});

});

$(document).ready(
function()
{
	showExtraInfo('<?php echo $_SESSION['select_tab']; ?>');
});

function showExtraInfo(index)
{
	index = Number(index);
	switch(index)
    {
        case 1:
	        $("#sub-extra-info").show();
	        break;
	    default :
	        $("#sub-extra-info").hide();
    }
}

function numbering_subject()
{
	var count = 1;
	$('#tbl_body tr').each(function() {
	    $(this).find("td:first-child").html(count++);    
	 });
}

function addRow(field_name) 
{
	var msg="";
	var flag=0;

	//var total_subject =  document.getElementById('total_subject');
	var subject = document.getElementById('subject_id');
	var subject_min = document.getElementById('subject_min_mark');
	var subject_max = document.getElementById('subject_max_mark');
	var total_subject = document.getElementById('total_subject');
	
	if(subject.value=='')
	{
		msg+= "Please select subject.\n";
		focusAt=subject;
		flag=1;
	}
	else
	{
		var totalSubjects = Number(total_subject.value);
		for(var i=0; i<totalSubjects; i++)
		{
			if(document.getElementById('subject_id_'+(i+1))==null)
			{
				continue;
			}
			
			if(subject.value==document.getElementById('subject_id_'+(i+1)).value)
			{
				msg+= "This subject is already selected. Please try another subject.\n";
				focusAt=subject;
				flag=1;
			}
		}
	}

	if(subject_min.value=='')
	{
		msg+= "Please enter minimum marks of subject.\n";
		if(flag==0)
			focusAt=subject_min;
		flag=2;
	}

	if(subject_max.value=='' || subject_max.value==0)
	{
		msg+= "Please enter maximum marks of subject.\n";
		if(flag==0)
			focusAt=subject_max;
		flag=2;
	}

	if(msg!='')
	{
		alert(msg);
		focusAt.focus();
		return false;
	}
		
    var table = document.getElementById('tbl_body');
    var rowCount = table.rows.length;
    
   	if(total_subject.value==0)
	    document.getElementById('all_subject_tr').style.display='';
    
    var row = table.insertRow(rowCount);
	var row_no = Number(total_subject.value)+1;
	
	if((row_no%2)==0)
		row.className="tdbgwhite";
	else
		row.className="tdbggrey";

//=======SrNo td======================	
    var cell1 = row.insertCell(0);
    cell1.innerHTML = Number(document.getElementById('count_subject').value)+1;

//=======Subject name=================
    var cell2 = row.insertCell(1);

    var element1 = document.createElement("input");
    element1.type = "hidden";
    element1.name = "subject_id_"+row_no;
    element1.id = "subject_id_"+row_no;
    element1.value = subject.value;
    
    cell2.align = "center";
    cell2.id = "subject_nameTd_"+row_no;
    cell2.innerHTML=$("#subject_id option:selected").text();
    cell2.appendChild(element1);

//=======subject minimum marks=====================
	var cell3 = row.insertCell(2);
    
    var element2 = document.createElement("input");
    element2.type = "hidden";
    element2.name = "subject_min_value_"+row_no;
    element2.id = "subject_min_value_"+row_no;
    element2.value = subject_min.value;
    
    cell3.id = "subject_minTd_"+row_no;
    cell3.align = "center";
    cell3.innerHTML=subject_min.value;
    cell3.appendChild(element2);

//=======subject maximum marks=====================
	var cell4 = row.insertCell(3);
    
    var element3 = document.createElement("input");
    element3.type = "hidden";
    element3.name = "subject_max_value_"+row_no;
    element3.id = "subject_max_value_"+row_no;
    element3.value = subject_max.value;
    
    cell4.id = "subject_maxTd_"+row_no;
    cell4.align = "center";
    cell4.innerHTML=subject_max.value;
    cell4.appendChild(element3);
    
//==========Option for Edit and Delete==============
    var cell8 = row.insertCell(4);
    //var element5 = document.createElement("td");
    //element5.type = "text";
    cell8.align = "center";
    cell8.innerHTML='<input type="button" class="buttons rounded" name="edit_'+row_no+'" id="edit_'+row_no+'" value="Edit" onclick="editRow(this);" /> <img src="<?php echo IMAGEURL; ?>/b_drop.png" style="cursor: pointer;" title="Delete" onclick="deleteRow(this);" /> ';
    //cell6.appendChild(element5); 

    document.getElementById('total_subject').value=row_no;
    document.getElementById('count_subject').value = Number(document.getElementById('count_subject').value) + 1;
    subject.value="";
    subject_min.value='';
    subject_max.value='';
    $('#all_subject_tr').show();
}

function deleteRow(r, field_name, row_no)
{
	var confirmation_flag = confirm("Do you really want to remove this subject?");
	if(confirmation_flag)
	{
		var i = r.parentNode.parentNode.rowIndex-1;
		document.getElementById('tbl_body').deleteRow(i);
		document.getElementById('count_subject').value-=1;
		numbering_subject();
		if(document.getElementById('count_subject').value == 0)
		{
			$('#all_subject_tr').hide();
		}
	}
	else
	{
		return false;
	}
}

function editRow(val, field_name)
{
	var row_no = val.name.substr(val.name.lastIndexOf('_')+1);
	var option='';

	var subject = document.getElementById('subject_id_'+row_no+'');
	var subject_min = document.getElementById('subject_min_value_'+row_no+'');
	var subject_max = document.getElementById('subject_max_value_'+row_no+'');
	var total_subject = document.getElementById('total_subject');
	
	if(val.value=="Edit")
	{
		<?php
		if(is_array($subject))
		{
			for($index=0; $index<count($subject); $index++)
			{ ?>
			
				var selected='';
				if(<?= $subject[$index]->id; ?>==subject.value)
				{
					selected='selected';
				}
				option+= '<option value="<?= $subject[$index]->id; ?>" '+selected+' ><?= stripslashes($subject[$index]->subject_name); ?></option>';
				<?php 
			} 
		}
		?>
		
		$('#subject_nameTd_'+row_no).html('<select name="subject_id_'+row_no+'" id="subject_id_'+row_no+'" class="rounded">'+option+'</select>');
		$('#subject_minTd_'+row_no).html('<input type="text" maxlength="3" name="subject_min_value_'+row_no+'" id="subject_min_value_'+row_no+'" size="4" class="rounded textfield" value="'+subject_min.value+'" onkeyup="checkforPercentage(this);"/>');
		$('#subject_maxTd_'+row_no).html('<input type="text" maxlength="3" name="subject_max_value_'+row_no+'" id="subject_max_value_'+row_no+'" size="4" class="rounded textfield" value="'+subject_max.value+'" onkeyup="isNum(this.value,this.id);"/>');
		val.value="Update";
	}
	else if(val.value=="Update")
	{

		var msg='';
		var flag=0;
		
		var totalSubjects = Number(total_subject.value);
		for(var i=0; i<totalSubjects; i++)
		{
			if(document.getElementById('subject_id_'+(i+1))==null)
			{
				continue;
			}
			if((i+1)==row_no)
			{
				continue;
			}
			
			if(subject.value==document.getElementById('subject_id_'+(i+1)).value)
			{
				msg+= "This subject is already selected. Please try another subject.\n";
				focusAt=subject;
				flag=1;
			}
		}
		
		if(subject_min.value=='')
		{
			msg+= "Please enter minimum passing marks of subject.\n";
			if(flag==0)
				focusAt=subject_min;
			flag=2;
		}

		if(subject_max.value=='' || subject_max.value==0)
		{
			msg+= "Please enter maximum marks of subject.\n";
			if(flag==0)
				focusAt=subject_max;
			flag=2;
		}

		if(msg!='')
		{
			alert(msg);
			focusAt.focus();
			return false;
		}
		
		$('#subject_nameTd_'+row_no).html($("#subject_id_"+row_no+" option:selected").text()+' <input type="hidden" name="subject_id_'+row_no+'" id="subject_id_'+row_no+'" value="'+subject.value+'" />');
		$('#subject_minTd_'+row_no).html(subject_min.value+' <input type="hidden" name="subject_min_value_'+row_no+'" id="subject_min_value_'+row_no+'" value="'+subject_min.value+'">');
		$('#subject_maxTd_'+row_no).html(subject_max.value+' <input type="hidden" name="subject_max_value_'+row_no+'" id="subject_max_value_'+row_no+'" value="'+subject_max.value+'">');
		val.value="Edit";
	}
}

function checkcopyfield()
{
	if($('#exam_list').val() == '')
	{
		alert('Please select course.');
		return false;
	}
	return true;
}
</script>

</head>
<body onload="document.addfrm.exam_name.focus();">
<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
		<tr>
			<td>
				<?php include_once(CURRENTTEMP."/"."header.php"); ?>
			</td>
		</tr>
		<tr>
			<td>
				<div id="content">
					<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					<div id="main">
						<div id="contents">
	
<fieldset class="rounded">
	<legend><?php echo ($isEdit) ? 'Edit Course' : 'Add Course'; ?></legend>
	
	<?php
		if(isset($_SESSION['error']))
		{
		echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
		echo $_SESSION['error'];
		echo '</td></tr></tbody></table>';
		unset($_SESSION['error']);
		}
		
		if(isset($_SESSION['success']))
		{
		echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
		echo $_SESSION['success'];
		echo '</td></tr></tbody></table>';
		unset($_SESSION['success']);
		}
	?>
<br>
<div style="width: 700px;">
	<div id="mandatory" style="float: left;">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>
	
	<div style="float: right;" id="copy-link">
		<a href="#copy-tab" onclick="$('#copy-tab').slideToggle();return false;">Copy An Existing Course</a>
	</div>
	
	<div style="clear: both;"></div>

	<div id="copy-tab">
	    <form id="copyfrm" name="copyfrm" method="post" action="">
	    	<div>
	    		Course: 
				<select name="exam_list" id="exam_list"  class="rounded">
					<option value="">Select Course</option>
					<?php 
					if($examlist != '')
					{
						$counter_exam = count($examlist);
						for ($count_exam = 0; $count_exam < $counter_exam; $count_exam++)
						{
							$checked = ($frmdata['exam_list'] == $examlist[$count_exam]->id) ? "selected = 'selected'" : '';
							echo "<option value='" . $examlist[$count_exam]->id . "' $checked>" . $examlist[$count_exam]->exam_name . "</option>";
						}
					}								
					?>
				</select>
				<input type="submit" name="copyexam" id="copyexam" value="Copy" class="buttons rounded" onclick="return checkcopyfield();" />
			</div>
			<div style="font-size: 11px;margin-top: 5px;">
				<strong>Note : </strong>Subjects will also be copied along with course's basic information.
			</div>
		</form>
	</div>
</div>

<form id="addfrm" name="addfrm" method="post" action="">
<input type="hidden" name="exam_list" value="<?php echo $frmdata['exam_list']; ?>" />

<div id="tabs" style="width: 700px;">
    <ul>
        <li><a href="#exam-info-tab">Course Information</a></li>
        <li><a href="#subject-tab">Subject</a></li>
    </ul>
    
    <div id="exam-info-tab" class="exam-tabs">
    	<table width="" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">
    		<tr>
				<td>
					<div>Course Name:<span class="red">*</span></div>
				</td>
				<td width="">
					<input maxlength="100" type="text" name="exam_name" id="exam_name" size="35" class="rounded textfield" value="<?php if($frmdata['exam_name']) echo $frmdata['exam_name']; else echo $exam_info->exam_name; ?>" onchange="examName(this, 'course name');" />
				</td>
			</tr>
		</table>
	</div>
    
    <div id="subject-tab" class="exam-tabs">
		<table cellpadding="0" cellspacing="4" width="">
			<tr>
				<td>
					<div>Subject Name:<span class="red">*</span></div>
				</td>
				<td>
					<select name="subject_id" id="subject_id" class="rounded">
						<option value="">Select Subject</option>
						<?php 
							if(is_array($subject))
							{
								for($counter=0;$counter<count($subject);$counter++)
								{
									$selected='';
									if ($subject[$counter]->id == $frmdata['subject_id'])
									{
										$selected='selected';
									}
									echo '<option value="'.$subject[$counter]->id.'"'.$selected.'>'. stripslashes($subject[$counter]->subject_name).'</option>';
								}
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<div>Subject's Minimum Passing Marks <br/>(in percentage):<span class="red">*</span></div>
				</td>
				<td>
					<input type="text" value="<?php echo $frmdata['subject_min_mark']; ?>" maxlength="3" 
							name="subject_min_mark" id="subject_min_mark" size="4" class="rounded textfield" 
							onkeyup="checkforPercentage(this);" />
				</td>
			</tr>
			<tr>
				<td>
					<div>Subject's Maximum Marks:<span class="red">*</span></div>
				</td>
				<td>
					<input type="text" value="<?php echo $frmdata['subject_max_mark']; ?>" maxlength="3" 
							name="subject_max_mark" id="subject_max_mark" size="4" class="rounded textfield" 
							onkeyup="isNum(this.value,this.id);" />
				</td>
			</tr>
		</table>
		
		<table width="300px">
			<tr>
				<td colspan="4" align="center">
					<input type="button" class="buttons rounded" name="addsubject" id="addsubject" 
						value="Add" onclick="addRow();" />
				</td>
			</tr>
		</table>
		</div>
</div>

<?php
	$trStyle = 'display:none;';
	if($isCopy)
	{
		$trStyle = '';
	}
	elseif(!($_POST) && count($subject_info) != 0)
	{
		$trStyle = '';
	}
	if($frmdata['count_subject'] > 0)
	{
		$trStyle = "";
	}
?>

<div id="sub-extra-info" style="display: none;">
<div id="all_subject_tr" style="<?php echo $trStyle; ?>">
	<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="700px" bgcolor="#e1e1e1" class="data">
	<thead>
		<tr class="tblheading">
			<td width="1%">#</td>
			<td width="26%">Subject</td>
			<td width="11%">Subject Min Marks(%)</td>
			<td width="13%">Subject Max Marks</td>
			<td width="11%">Action</td>
		</tr>
	</thead>

	<tbody id="tbl_body">
	<?php
		$ttl_sub = isset($frmdata['total_subject']) ? $frmdata['total_subject'] :  count($subject_info);
						
		for($counter=0; $counter<$ttl_sub; $counter++)
		{
			if(isset($frmdata['total_subject']))
			{
				if(($frmdata['subject_id_'.($counter+1)]==''))
				continue;
			}
			else
			{
				if($subject_info[$counter] == '')
				continue;
			}
			
			if(($counter%2)==0)
			{
				$trClass="tdbggrey";
			}
			else
			{
				$trClass="tdbgwhite";
			} ?>
			
			<tr class="<?php echo $trClass; ?>">
				
				<td><?php echo $counter+1; ?></td>
				
				<td id="subject_nameTd_<?php echo $counter+1; ?>">
				<?php
					if(isset($frmdata['total_subject']))
					{ 
						for($counter1=0;$counter1<count($subject);$counter1++)
						{
							$selected='';
							if ($subject[$counter1]->id == $frmdata['subject_id_'.($counter+1)])
							{
								echo stripcslashes($subject[$counter1]->subject_name);
								echo '<input type="hidden" name="subject_id_'.($counter+1).'" id="subject_id_'.($counter+1).'" value="'.$frmdata['subject_id_'.($counter+1)].'">';
								break;
							}
						}
					}
					elseif (count($subject_info) > 0)
					{
						echo stripcslashes($subject_info[$counter]->subject_name);
						if($frmdata['subject_id_'.($counter+1)]!='')
							$subject_id_hidden_value = $frmdata['subject_id_'.($counter+1)];
						else 
							$subject_id_hidden_value = $subject_info[$counter]->subject_id;

						echo '<input type="hidden" name="subject_id_'.($counter+1).'" id="subject_id_'.($counter+1).'" value="'.$subject_id_hidden_value.'">';
					}
				?>
				</td>
			
				<td id="subject_minTd_<?php echo $counter+1; ?>">
				<?php 
					$subject_min_hidden_value='';
					if($frmdata['subject_min_value_'.($counter+1)]!='')
					{
						echo $frmdata['subject_min_value_'.($counter+1)];
						$subject_min_hidden_value = $frmdata['subject_min_value_'.($counter+1)];
					}
					elseif ($subject_info[$counter]->subject_min_mark!='')
					{
						echo $subject_info[$counter]->subject_min_mark;
						$subject_min_hidden_value = $subject_info[$counter]->subject_min_mark;
					}
					else
						echo "-";

					echo '<input type="hidden" name="subject_min_value_'.($counter+1).'" id="subject_min_value_'.($counter+1).'" value="'.$subject_min_hidden_value.'">';
				?>
				</td>
				
				<td id="subject_maxTd_<?php echo $counter+1; ?>">
				<?php 
					$subject_max_hidden_value='';
					if($frmdata['subject_max_value_'.($counter+1)]!='')
					{
						echo $frmdata['subject_max_value_'.($counter+1)];
						$subject_max_hidden_value = $frmdata['subject_max_value_'.($counter+1)];
					}
					elseif ($subject_info[$counter]->subject_max_mark!='')
					{
						echo $subject_info[$counter]->subject_max_mark;
						$subject_max_hidden_value = $subject_info[$counter]->subject_max_mark;
					}
					else
						echo "-";

					echo '<input type="hidden" name="subject_max_value_'.($counter+1).'" id="subject_max_value_'.($counter+1).'" value="'.$subject_max_hidden_value.'">';
				?>
				</td>
				
				<td>
					<input type="button" class="buttons rounded" value="Edit" id="edit_<?php echo $counter+1; ?>" name="edit_<?php echo $counter+1; ?>" onclick="editRow(this);"/> 
					<img onclick="deleteRow(this);" title="Delete" style="cursor: pointer;" src="<?php echo IMAGEURL; ?>/b_drop.png" /> 
				</td>
			</tr><?php
		} ?>
	</tbody>
	</table>
</div>
</div>

<br>
<div align="center">
	<?php if($isEdit) { ?>
	    <input type="submit" name="editexam" id="editexam" value="Update" class="buttons rounded" />
	<?php }else { ?>
		<input type="submit" name="addexam" id="addexam" value="Add" class="buttons rounded" />
	<?php } ?>
	
	<input type="button" name="cancel" value="Cancel" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=examination&do=manage'"/>
</div>
<br></br>

	<input type="hidden" name="count_subject" id="count_subject" value="<?php if(isset($frmdata['count_subject'])) echo $frmdata['count_subject']; else echo count($subject_info); ?>"/>
	<input type="hidden" name="total_subject" id="total_subject" value="<?php if(isset($frmdata['total_subject'])) echo $frmdata['total_subject']; else echo count($subject_info); ?>"/>
	<input type="hidden" name="count_filter" id="count_filter" value="<?php if(isset($frmdata['count_filter'])) echo $frmdata['count_filter']; elseif(is_array($filter_info)) echo count($filter_info) == count($stream) ? 1 : count($filter_info); else echo 0; ?>"/>
	<input type="hidden" name="total_filter" id="total_filter" value="<?php if(isset($frmdata['total_filter'])) echo $frmdata['total_filter']; elseif(is_array($filter_info)) echo count($filter_info) == count($stream) ? 1 : count($filter_info); else echo 0; ?>"/>
	<input type="hidden" name="is_all_stream" id="is_all_stream" value="<?php if(isset($frmdata['is_all_stream'])) echo $frmdata['is_all_stream']; elseif(count($stream)==count($filter_info)) echo 1; else echo 0; ?>"/>
</form>
</fieldset>
						</div><!--Div Contents closed-->
					</div><!--Div main closed-->
				</div><!--Content div closed-->
			</td>
		</tr>
	</table>
</div><!--Outer wrapper closed-->

</body>
</html>