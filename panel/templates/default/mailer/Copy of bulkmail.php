<?php
 /*****************Developed by :- Chirayu Bansal
	                Date         :- 14-july-2011
					Module       :- mail master
	***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Bulk Mail Master</title>

<sctipt src="<?php echo ROOTURL.'/js/function.js'; ?>"></sctipt>
<script>

function insertAtCaret(tag)
{
	if (tag == 'FN')
	{
		set='&lt;firstname&gt;';
	}
	else if(tag == 'UN')
	{
		set='&lt;username&gt;';
	}
	else if (tag == 'E')
	{
		set='&lt;email&gt;';
	}
	else if (tag == 'P')
	{
		set='&lt;password&gt;';
	}
	else if (tag == 'LN')
	{
		set='&lt;lastname&gt;';
	}
	else  
	{
		return false;
	}
	WPro.editors['mailmessage'].insertAtSelection(set);		
}

function showTestTr()
{
	$('#test-tr').show();
}
function loadTest(exam)
{
	if(exam == '')
	{
		$('#test-select').val();
		$('#test-tr').hide();
		return true;
	}
	xajax_getTestOptionsByExam(exam);
}
$(document).ready(
function()
{
	$('#exam_id').trigger('change');
/*	$('.candidate_seletion').change(
	function()
	{
		if($('#test-select').val() == '')
		{
			$('.candidate_seletion').removeAttr('checked');
			$('#test-select').focus();
			return false;
		}
	}); */
});
</script>

</head>

<body onload="document.addfrm.exam_id.focus();">
<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
		<tr>
			<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
		</tr>
		
		<tr>
			<td>
				<div id="content">
					<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				
					<div id="main">
						<div id="contents">
							<form id="addfrm" name="addfrm" method="post" action="">
								<fieldset class="rounded">
									<legend>Send Mail</legend><br/>
									<?php
										if(isset($_SESSION['error']))
										{
										echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
										echo $_SESSION['error'];
										echo '</td></tr></tbody></table>';
										unset($_SESSION['error']);
										}
										
										if(isset($_SESSION['success']))
										{
										echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
										echo $_SESSION['success'];
										echo '</td></tr></tbody></table>';
										unset($_SESSION['success']);
										}
									?>

<table width="98%" align="center" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">
<tr>
	<td colspan="4">
		<div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>
	</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td>
		Select Student Group:
	</td>
	<td>
		<select class="rounded textfield" name="exam_id" id="exam_id" onchange="loadTest(this.value);">
			<option value="">Select Course</option>
			<?php
				if(is_array($exam))
				{
					for($counter=0;$counter<count($exam);$counter++)
					{
						$selected='';
						if ($exam[$counter]->id == $frmdata['exam_id'])
						{
							$selected='selected';
						}
						echo '<option value="'.$exam[$counter]->id.'"'.$selected.'>'.stripslashes($exam[$counter]->exam_name).'</option>';
					}
				}
			?>
		</select>
	</td>
</tr>

<tr id="test-tr" style="display: none;">
	<td>&nbsp;</td>
	<td>
		<span id="test-span">
			<select name="test_id" id="test-select" class="rounded textfield"></select>
		</span>
		&nbsp;&nbsp;&nbsp;
		<span>
			<label><input type="checkbox" class="candidate_seletion" name="candidate_seletion[]" value="P" <?php if(in_array('P', $frmdata['candidate_seletion'])) echo 'checked'; ?> />Passed</label>
			<label><input type="checkbox" class="candidate_seletion" name="candidate_seletion[]" value="F" <?php if(in_array('F', $frmdata['candidate_seletion'])) echo 'checked'; ?> />Failed</label>
		</span>
	</td>
</tr>

<tr>
	<td>
		<div>To:</div>
	</td>
	<td colspan="4">
		<input id="mailto" name="mailto" class="rounded textfield" class="input" value="<?php echo $frmdata['mailto'] ;?>"
			onBlur="return CheckEmailMultipleId(this)" size="90">
	</td>
</tr>

<tr>
	<td>
		<div>Cc:</div>
	</td>
	<td colspan="4">
		<input name="mailcc" type="text" class="rounded textfield" value="<?php echo $frmdata['mailcc']; ?>" 
			onBlur="return CheckEmailMultipleId(this)" size="90">
		</td>
</tr>
<tr>
	<td>
		<div>
			Subject:<span class="red">*</span>
		</div>
	</td>
	<td colspan="4">
		<input name="mailsubject" type="text" class="rounded textfield"
		value="<?php echo $frmdata['mailsubject']; ?>" size="90"> 
	</td>
</tr>
<tr>		
	<td height="34" align="left">
		<div>Tags:</div>
	</td>
  	<td>
		<select name="tags" class="rounded textfield" id="tags" onChange="insertAtCaret(this.value);">
			<option value="">Select Tag</option>
			<?php 
			foreach ($tags as $key => $value)
			{
				echo '<option value="'.$key.'" >'.htmlentities($value).'</option>';
			}
			?>
		</select>
	</td>
</tr>

<tr>
	<td valign="top">Message:<span class="red">*</span></td>
	<td colspan="4">
	<?php 
		if ($frmdata['mailmessage'] =='')
		{
			$editor->value='<p><font face="Calibri">&nbsp;</font></p>';
		}
		else
		{
			$editor->value=stripslashes($frmdata['mailmessage']);
		}
		$editor->display('80%', 300); 
	?>
	</td>
</tr>

<tr>
	<td>&nbsp;</td>
</tr>

<tr>
	<td>&nbsp;</td>
	<td colspan="3">
		<div align="left">
			<input type="submit" name="sendmail" id="sendmail" value="Send" class="buttons rounded" />
			<input type="button" name="cancel" value="Reset" class="buttons" onclick="location.href=document.URL;"/>
		</div>
	</td>
</tr>
</table>
<br></br>
				
								</fieldset><br/>
							</form>
	  					</div><!--Div Contents closed-->
					</div><!--Div main closed-->
				</div><!--Content div closed-->
			</td>
		</tr>
	</table>
</div><!--Outer wrapper closed-->

</body>
</html>