<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Homework</title>

<script language="javascript" type="text/javascript">
function deleteHomework(val)
{
	var x = confirm("Are you sure you want to delete this homework?");
    if (x)
    { 
        load("<?php echo ROOTURL; ?>" + "/index.php?mod=homework&do=delete&nameID="+val);
	}
    else
    { 
		return false;
    }
}

function informCandidates(id, is_informed, informed_count, informed_date)
{
	go = false;
	if((is_informed == true) && (informed_count > 0))
	{
		if(confirm(informed_count + ' students are already informed about this homework on '+ informed_date+'.\nDo you really want to re-inform students?'))
		{
			go = true;
		}
	}
	else if(confirm('Do you really want to inform students about this homework?'))
	{
		go = true;
	}

	if(go)
	{
		xajax_informCandidatesForHomework(id);
	}

	return true;
}
function reload()
{
	location.href = document.URL;
}
</script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>

<body onload="document.frmlist.name.focus();">
<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr>
	<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
</tr>
<tr>
	<td>
		<div id="content">
		<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
		<div id="main">
			<div id="contents">
				<form action="" method="post" name="frmlist">
				<legend>Manage Homework</legend>
				<?php 
				// Show particular Messages
				if(isset($_SESSION['error']))
				{
					echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
					echo $_SESSION['error'];
					echo '</td></tr></tbody></table>';
					unset($_SESSION['error']);
				}
				if(isset($_SESSION['success']))
				{
					echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
					echo $_SESSION['success'];
					echo '</td></tr></tbody></table>';
					unset($_SESSION['success']);
				}
				?>
				
<div class="search-div">
<fieldset class="rounded search-fieldset"><legend>Search</legend>
<table border="0" cellspacing="1" cellpadding="3" width="100%">
<tr>
	<td align="center">
		Homework Title:
		<input type="text" size="30" class="rounded textfield" name="name" value="<?php echo $frmdata['name'] ?>" onchange="managePageCheckIsAlphaNum(this, 'homework title');" />
	</td>
</tr>
<tr>
	<td align="center">
    	<input type="submit" class="buttons rounded" name="Submit" value="Show" />
    	<input type="submit" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick=""/>
	</td>
</tr> 
</table>
</fieldset>
</div><br />

		
<?php if($homeworklist)	{ ?>

<table border="0" cellspacing="1" cellpadding="4" width="100%" style="">
<tr>
	<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($homeworklist))." of ".$totalCount; ?></td>
	<td align="right"><?php echo getPageRecords();?> </td>
</tr>
</table>

<?php }	?>

<div id="celebs">
<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">

<?php if($homeworklist) { ?>

<thead>
	<tr class="tblheading">
	<td width="1%">#</td>
	<td width="26%">
		<div align="center">
			<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('homework_name');">Homework Title
			<?php if($frmdata['orderby']=='homework_name') 
				{
					print '<img src="'.ROOTURL.'/images/asc.gif">';
				} 
				elseif($frmdata['orderby']=='homework_name desc')
				{
					print '<img src="'.ROOTURL.'/images/desc.gif">';
				}
			?>
			</a>
		</div>
	</td>
	<td width="18%">
		<div align="center">
			<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('exam.exam_name');">Course Name
			<?php if($frmdata['orderby']=='exam.exam_name') 
				{
					print '<img src="'.ROOTURL.'/images/asc.gif">';
				} 
				elseif($frmdata['orderby']=='exam.exam_name desc')
				{
					print '<img src="'.ROOTURL.'/images/desc.gif">';
				}
			?>
			</a>
		</div>
	</td>
	<td width="11%">
		<div align="center">
			<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('homework_date');">From
			<?php if($frmdata['orderby']=='homework_date') 
				{
					print '<img src="'.ROOTURL.'/images/asc.gif">';
				} 
				elseif($frmdata['orderby']=='homework_date desc')
				{
					print '<img src="'.ROOTURL.'/images/desc.gif">';
					$frmdata['orderby']='homework_date desc';
				}
			?>
			</a>
		</div>
	</td>
	<td width="11%">
		<div align="center">
			<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('homework_date_end');">To
			<?php if($frmdata['orderby']=='homework_date_end') 
				{
					print '<img src="'.ROOTURL.'/images/asc.gif">';
				} 
				elseif($frmdata['orderby']=='homework_date_end desc')
				{
					print '<img src="'.ROOTURL.'/images/desc.gif">';
					$frmdata['orderby']='homework_date_end desc';
				}
			?>
			</a>
		</div>
	</td>
	<td width="25%">Action</td>
</tr>
</thead>

<tbody id="tbody">
					  
<?php 
$srNo=$frmdata['from'];
$count=count($homeworklist);
					
foreach($homeworklist as $homework) 
{
	$id = $homework->id;
	$name = stripslashes(highlightSubString($frmdata['name'], $homework->homework_name));
	$exam_name = stripslashes($homework->exam_name);
	
	$date = date('d-m-Y h:i:s A', strtotime($homework->homework_date));
	$date_end = date('d-m-Y h:i:s A', strtotime($homework->homework_date_end));
	
	$srNo = $srNo+1;
  	if(($srNo%2)==0)
	{
		$trClass="tdbggrey";
	}
	else
	{
		$trClass="tdbgwhite";
	} ?>

	<tr class='<?php echo $trClass; ?>' id='row_<?php echo $id; ?>'>
		<td><?php echo $srNo; ?></td>
		<td><?php echo $name; ?></td>
		<td><?php echo $exam_name;?></td>
		<td><?php echo $date; ?></td>
		<td><?php echo $date_end;?></td>
 		<td>
			<?php if($homework->enable_homework=='Y') { ?>
			
				<img title="Disable homework" src="<?php echo IMAGEURL ?>/disable.gif" onclick="xajax_enableHomework('<?php echo $id; ?>');" style="cursor:pointer" id="enableHomework_<?php echo $id; ?>" /></a>&nbsp;&nbsp;
						
			<?php } else { ?>

				<img title="Enable homework" src="<?php echo IMAGEURL ?>/right.gif" onclick="xajax_enableHomework('<?php echo $id; ?>');" id="enableHomework_<?php echo $id; ?>" style="cursor:pointer" /></a>&nbsp;&nbsp;
						
			<?php } ?>
			
			<?php
				$is_informed = $homework->is_informed;
				$informed_count = $homework->informed_count;
				$informed_date = date('d-m-Y h:i:s A', strtotime($homework->informed_date));
			?>
						
			<?php if($homework->enable_homework=='Y') { ?>
			
					<img title="Inform Students" src="<?php echo IMAGEURL ?>/iconSendMail.png" onclick="informCandidates('<?php echo $id; ?>','<?php echo $is_informed; ?>','<?php echo $informed_count; ?>','<?php echo $informed_date; ?>');" style="cursor:pointer"/>&nbsp;&nbsp;
			
			<?php } else { ?>
					
					<img title="Please enable homework first." src="<?php echo IMAGEURL ?>/iconSendMail_disable.png" />&nbsp;&nbsp;
						
			<?php } ?>


			<a href="<?php echo CreateURL('index.php','mod=homework&do=edit&nameID='.$id) ?>" title='Edit'><img src="<?php echo IMAGEURL ?>/b_edit.png" /></a>&nbsp;&nbsp;

			<img src="<?php echo IMAGEURL ?>/b_drop.png" onclick="xajax_deletePermission(<?php echo $id; ?>, 'homework');" style="cursor:pointer" title="Delete" />
		</td>
	</tr> <?php 
}
?>

</tbody>
					  
<?php  } else  { ?>

	<tr><td colspan='7' align='center'>(0) Record found.</td></tr>

<?php } ?>
					  
</table>
</div>

<?php if($homeworklist) { ?>

<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
<tr>
	<td colspan="2">
		<?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($homeworklist))." of ".$totalCount; ?>
	</td>
	<td align="right">
		<?php PaginationDisplay($totalCount); ?>
	</td>
</tr>
</table>

<?php } ?>
				 	<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
	  				<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
					<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />	
  				</form>
			</div><!--Div Contents closed-->
		</div><!--Div main closed-->
		</div><!--Content div closed-->
	</td>
</tr>
</table>	
</div><!--Outer wrapper closed-->
</body>
</html>