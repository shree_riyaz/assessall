<?php 
/**
 * 	@author	:	Ashwini Agarwal
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Online Examination - Teacher Master</title>

<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/jquery.tabs.css' />
	
<script>
$(function() { 
	$('#tabs').tabs();
	$('#tabs').tabs('option', 'active', <?php echo $_SESSION['select_tab']; ?>);
});

function generatePassword()
{
	var length = 6;
	charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	retVal = "";
	for (var i = 0, n = charset.length; i < length; ++i) 
	{
		retVal += charset.charAt(Math.floor(Math.random() * n));
	}
	return retVal;
}

function getPass()
{
	chk = document.getElementById('passgen');
	if(chk.checked != '')
	{
		var pass = generatePassword();
		$('#showPass').html(pass);
		$('#auto_pass').val(pass);
		$('.password').val(pass);
		$('#showPass').show();
	}
	else
	{
		$('#showPass').html('');
		$('.password').val('');
		$('#auto_pass').val('');
		$('#showPass').hide();
	}
}

function addTeacherSubject()
{
	var exam = $('#exam_id');
	var subject = $('#subject_id');

	if(exam.val() == '')
	{
		alert('Please select course.');
		exam.focus();
		return false;
	}

	if(subject.val() == '')
	{
		alert('Please select subject.');
		subject.focus();
		return false;
	}

	xajax_addTeacherSubject(exam.val(), subject.val());
}
function finishSubjectAddition()
{
	var exam = $('#exam_id');
	var subject = $('#subject_id');

	xajax_getExamSubjectOptions('', 'subject_id')
	
	exam.val('');
	subject.val('');
	numbering();
}

function numbering()
{
	var counter = 1;
	$('.ts-sno').each(function(){
		$(this).html(counter++);
	});

	if(counter == 1)
	{
		$('#teacher-subject').hide();
	}
	else
	{
		$('#teacher-subject').show();
	}
}

function deleteSubject(id)
{
	if(!confirm('Do you really want to delete this subject?'))
		return false;
	
	xajax_addTeacherSubject('', '', id);
	$('tr#ts-'+id).remove();
	numbering();
}

$(document).ready(function(){

	numbering();
	$('.include-check').live('change', function() {
		$('.include-check').not(':checked').parent().parent().removeClass('included')
		$('.include-check').filter(':checked').parent().parent().addClass('included')
	});	
	xajax_showFilteredCandidate();
});
</script>
</head>

<body onload="document.addteacher.first_name.focus();">

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>

<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
<tr>
	<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
</tr>
<tr>
	<td>
		<div id="content">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
			<div id="main">
				<div id="contents">
					<form id="addteacher" name="addteacher" method="post" action="" enctype="multipart/form-data">
						<fieldset class="rounded">
							<legend>Enter Teacher Details</legend>
							<?php
								if(isset($_SESSION['error']))
								{
								echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
								echo $_SESSION['error'];
								echo '</td></tr></tbody></table>';
								unset($_SESSION['error']);
								}
								if(isset($_SESSION['success']))
								{
								echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
								echo $_SESSION['success'];
								echo '</td></tr></tbody></table>';
								unset($_SESSION['success']);
								}
							?>
							
<div id="mandatory" style="margin: 10px;"><?php echo MANDATORYNOTE; ?></div>

<div id="tabs">
    <ul>
        <li><a href="#teacher-info-tab">Personal Information</a></li>
        <li><a href="#subject-tab">Subject</a></li>
    </ul>
    
    <div id="teacher-info-tab" class="exam-tabs">
    	<table width="90%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences" style="padding: 10px;">		
		<tr>
			<td width="120px">
				<div>First Name:<span class="red">*</span></div>
			</td>
			<td width="400px">
				<input name="first_name" id="first_name" type="text" class="rounded textfield" size="30" maxlength="100" value="<?php echo $frmdata['first_name']; ?>" onchange="isChar(this.value,this.id, 'first name');" />
			</td>
			<td width="120px">
				<div>Last Name:</div>
			</td>
			<td>
				<input name="last_name" id="last_name" type="text" class="rounded textfield" size="30" maxlength="100" value="<?php echo $frmdata['last_name']?>" onchange="isChar(this.value,this.id, 'last name');"/>
			</td>	   
		</tr>
		
		<tr>
			<td>
				<div>Contact No.:</div>
			</td>
			<td>
				<input name="contact_no" id="contact_no" type="text" class="rounded textfield" size="30" value="<?php echo $frmdata['contact_no']?>" onchange="checkContactNumber(this);" maxlength="20"/>
			</td> 
			<td>
				<div>Email:<span class="red">*</span></div>
			</td>
			<td>
				<input name="email" type="text" size="30" class="rounded textfield" value="<?php echo $frmdata['email']?>" onchange="CheckEmailId(this)"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<div>Username:<span class="red">*</span></div>
			</td>
			<td>
				<input name="username" type="text" size="30" class="rounded textfield" value="<?php echo $frmdata['username']; ?>" />
			</td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			
			<?php $display = 'display:none;'; if($frmdata['passgen']) $display = ''; ?>
			<td colspan="10">
				<input type="checkbox" <?php if($frmdata['passgen']) echo 'checked'; ?> name='passgen' onchange="getPass();" id='passgen'></input>
				<label for="passgen">Auto Generate Password</label>
				<span id='showPass' style="padding: 2px 5px; background-color: #ddd; <?php echo $display; ?>">
					<?php echo $frmdata['auto_pass']; ?>
				</span>
				<input type='hidden' name='auto_pass' id='auto_pass' value="<?php if($frmdata['passgen']) echo $frmdata['auto_pass']; ?>"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<div>Password:<?php if(!$isEdit) { ?><span class="red">*</span><?php } ?></div>
			</td>
			<td>
				<input name="password" type="password" value="<?php if($frmdata['passgen']) echo $frmdata['auto_pass']; ?>" class="rounded password textfield" size="30" maxlength="25" onchange="checkPassword(this);"/>
			</td>
			<td nowrap="nowrap">
				<div>Confirm Password:<?php if(!$isEdit) { ?><span class="red">*</span><?php } ?></div>
			</td>
			<td>
				<input name="confirm_password" type="password" value="<?php if($frmdata['passgen']) echo $frmdata['auto_pass']; ?>" class="rounded password textfield" size="30" maxlength="25" onchange="checkPassword(this);"/>
			</td>
		</tr> 
		
		<tr>
			<td valign="top">
				<div>Address:</div>
			</td>
			<td>
				<textarea name="address" rows="5" cols="30" class="rounded"><?php echo $frmdata['address']; ?></textarea>
			</td>
			
			<td valign="top">Upload Image:</td>
			<td>
				<?php
				if($teacher_detail->pro_image!='')
				{ ?>
			        <a class="thumbnail" href="#thumb">
			        	<?php echo $teacher_detail->pro_image;	?>
			        	<span>
			        		<img  src="<?=ROOTURL.'/uploadfiles/'.$teacher_detail->pro_image;?>" />
			        	</span>
			        </a><br /> <?php 
				}
				?>
				<input type="file" name="pro_image" id="pro_image" class="rounded textfield" value="<?php echo $frmdata['pro_image']; ?>" />
				<div>Please upload image in .JPG, .GIF, .PNG and .JPEG format. Image size should not be more than 2MB.</div>
			</td>
		</tr>
		</table>
    </div>
    
    <div id="subject-tab" class="exam-tabs">
    	<fieldset class="rounded">
			<legend>Add Subjects</legend>
			<table width="80%" align="center">
			<tr>
				<td width="40%">
					<span>Course:</span>&nbsp;&nbsp;
					<select class="rounded textfield" name="exam_id" id="exam_id" onchange="xajax_getExamSubjectOptions(this.value, 'subject_id');">
						<option value="">Select Course</option>
						<?php 
						if(is_array($exam))
						{
							for($counter=0;$counter<count($exam);$counter++)
							{
								echo '<option value="'.$exam[$counter]->id.'"'.$selected.'>'. stripslashes($exam[$counter]->exam_name).'</option>';
							}
						}
						?>
					</select>
				</td>
				<td width="40%">
					<span>Subject:</span>&nbsp;&nbsp;
					<select class="rounded textfield" name="subject_id" id="subject_id">
						<option value="">Select Subject</option>
					</select>
				</td>
				<td width="20%">
					<input type="button" name="addsubject" value="Add Subject" class="buttons" onclick="addTeacherSubject();" />
				</td>
			</tr>
			
			<tr><td>&nbsp;</td></tr>
			
			<tr>
				<td colspan="4">
					<table border="0" cellspacing="1" cellpadding="4" id="teacher-subject" width="100%" bgcolor="#e1e1e1" class="data">
					<thead>
						<tr class="tblheading">
							<td>#</td>
							<td>Course</td>
							<td>Subject</td>
							<td>Action</td>
						</tr>
					</thead>
					
						<?php $count=0; foreach($_SESSION['teacher_subject'] as $key => $teacher_subject) { ?>
						<tr class="tdbgwhite" id="ts-<?php echo $key; ?>">
							<td class="ts-sno"><?php ++$count; ?></td>
							<td><?php echo $teacher_subject['exam_name']; ?></td>
							<td><?php echo $teacher_subject['subject_name']; ?></td>
							<td><a href="javascript:void(0);" onclick="deleteSubject('<?php echo $key; ?>');">Delete</a></td>
						</tr>
						<?php } ?>
						
					</table>
				</td>
			</tr>
			
			<tr><td>&nbsp;</td></tr>
			
			<tr>
				<td colspan="4"><div id='filtered_candidate'></div></td>
			</tr>
			
			</table>
		</fieldset>
    </div>
    
</div>

<br />

<div align="center">
	<?php if($isEdit) { ?>

    <input type="submit" name="editteacher" value="Update" class="buttons rounded" />
    
  	<?php } else { ?>
    
    <input type="submit" name="addteacher" value="Save" class="buttons rounded" />
    
  	<?php } ?>
	
	<input type="button" name="cancel" value="Cancel" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=teacher_master&do=manage'"/>
</div>
<br />

						</fieldset><br />
					</form>
				</div><!--Div Contents closed-->
			</div><!--Div main closed-->
		</div><!--Content div closed-->
	</td>
</tr>
</table>	
</div><!--Outer wrapper closed-->

</body>
</html>