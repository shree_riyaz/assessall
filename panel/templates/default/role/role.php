<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Role</title>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.role.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<legend>Roles</legend>
			<?php 
			if (isset($_SESSION['ActionAccess']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['ActionAccess'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['ActionAccess']);
			}
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>	

			<div class=" search-div">
			<fieldset class="rounded search-fieldset"><legend>Search</legend>
				<table border="0" cellspacing="1" cellpadding="3" width="100%">
					<tr>
			    		<td align="right">Role Name:</td>
						<td>
							<input name="role" type="text" class="rounded textfield" id="role" maxlength="20" value="<?php echo $frmdata['role']; ?>" />
			    		</td>
			    	</tr>
					<tr>
			    		<td colspan="6" align="center">
							<input class="buttons rounded" type="submit" name="searchrole" id="searchrole"  value="Search"  title="Click to search">
					        <input class="buttons rounded" type="submit" name="clearsearch" value="Clear Search"   title="Click to clear">
			    		</td>
		  			</tr>
				</table>
			</fieldset>
			</div>		
			<div style="width: 70%;margin: 10px auto 0;">	
	  <?php 
		if($rolelist)
		{		   
		 $srNo=$frmdata['from'];
		 $count=count($rolelist);	 	   
		?>
		
		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td width="454" align="left" valign="top">
				<?php	
					print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
				?>
				</td>		
				<td width="205" align="right" valign="top"><?php echo getPageRecords();?></td>
			</tr>
		</table>
	
<div id="celebs">		  
	<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
	<thead>
		<tr class="tblheading">
			<td width="33" align="center">S.No</td>
			<td width="118" align="center" >
				<a style="color:#AF5403;cursor:pointer" onClick="OrderPage('roleName');">
					Role<?php if($frmdata['orderby']=='roleName') {print '<img src="'.ROOTURL.'/images/asc.gif">';} elseif($frmdata['orderby']=='roleName desc'){print '<img src="'.ROOTURL.'/images/desc.gif">';}?>
				</a>
			</td>
		 	<td width="100" align="center" >Action</td>
		</tr>
	</thead>
		
	<?php 	
		for($counter=0;$counter<$count;$counter++)
		{
			$srNo++;
			if(($counter%2)==0)
			{
				$trClass="tdbggrey";
			}
			else
			{
				$trClass="tdbgwhite";
			}
	?>
	
		<tr class='<?= $trClass; ?>'>
		   <td align="center" ><?php  print $srNo ?></td>		   
		   <td align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<?php echo highlightSubString($frmdata['role'],$rolelist[$counter]->roleName); ?></td>		 
		   <td align="center" <?php echo $style ?>>
		  		<a class="fontstyle" href='<?php print CreateURL('index.php','mod=role&do=edit&nameID='.$rolelist[$counter]->roleID);?>' title="Edit Role" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 /></a>
				<a title="Delete"  href='<?php print CreateURL('index.php','mod=role&do=del&nameID='.$rolelist[$counter]->roleID);?>' onclick="return confirm('Do you really want to delete this role?')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a>
		   </td>
		</tr>		
		<?php
			} 
		?>
	</table>
	<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">		
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
	 </table>
	</div>
	  <?php
	  }
	  else
	  { 
		echo "<center>No Record found.</center>";
	   }
	  
	  ?>
	  </div>
	  <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" />
	</form>	
		
		</div><!--Div Contents closed-->
				</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>
