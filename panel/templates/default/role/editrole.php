<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Edit Role</title>

</head>
<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.roleName.focus();">
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	 	<tr>
			<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
		</tr>
	  	<tr>
			<td>
				<div id="content">
					<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
				<fieldset class="rounded" style="width:70%;">
					<legend>Edit Role</legend>
					<?php 
					// Show particular Messages
					if(isset($_SESSION['error']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
						echo $_SESSION['error'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['error']);
					}
					if(isset($_SESSION['success']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
						echo $_SESSION['success'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['success']);
					}
					
					//print_r($frmdata);
					?>
<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences" style="padding:20px;">
	<tr>
		<td colspan="4"><div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div></td>	
	</tr>
	<tr><td></td></tr>
	<tr>
		<td width="" align="">Role Name:<span class="red">*</span></td>
		<td>
			<input name="roleName" type="text" id="roleName" class="rounded textfield" value="<?php if($frmdata['roleName']) echo $frmdata['roleName']; else echo $role->roleName; ?>" size="40" maxlength="50" onchange="examName(this, 'role name');" />
		</td>
	</tr>
	<tr>
		<td width="" align="" valign="top">Module Access:<span class="red">*</span></td>
		<td>
			<table width="100%" class="tableccss" cellspacing="1" cellpadding="3">
				<tr>
					<td align="right" colspan="2">
						<span style="">
							<a style="cursor:pointer;font-weight:bold" onclick="selectAllModule()">Select All</a>
						</span>
						&nbsp;&nbsp;
						<span style="">
							<a style="cursor:pointer;font-weight:bold" onclick="unselectAllModule()">Unselect All</a>
						</span>
					</td>
				</tr>
				<?php
					$mdcnt=count($modules); 
					for($counter =0; $counter < $mdcnt; $counter++)
					{
						if ($modules[$counter]->moduleID != '')
						{	
							($counter == ($mdcnt -1)) ? $colspan='2' : $colspan ='';	
							$checkeda = '';
							$checkede = '';
							$checkeded = '';
							$check = '';
							$display = 'display:none';
							
							$addPerm = $DB->SelectRecord('rolepermission','roleID='.$role->roleID.' and moduleID='.$modules[$counter]->moduleID.' and permissionID='.$add);
							if ($addPerm)
							{
								$checkeda = ' checked ';
							}
							
							$editPerm = $DB->SelectRecord('rolepermission','roleID='.$role->roleID.' and moduleID='.$modules[$counter]->moduleID.' and permissionID='.$edit);
							if ($editPerm)
							{
								$checkede = ' checked ';
							}
							
							$exportPerm = $DB->SelectRecord('rolepermission','roleID='.$role->roleID.' and moduleID='.$modules[$counter]->moduleID.' and permissionID='.$export);
							
							if ($exportPerm)
							{
								$checkeded = ' checked ';
							}
							
							$readPerm = $DB->SelectRecord('rolepermission','roleID='.$role->roleID.' and moduleID='.$modules[$counter]->moduleID.' and permissionID='.$read);
							if ($readPerm)
							{
								$check = ' checked ';
								$display = 'display:""';
							}	
							if ($modules[$counter]->moduleName == 'preferences')
							{
								$modules[$counter]->moduleName = 'Configuration';
							}
							if ($modules[$counter]->moduleName == 'candidate_master')
							{
								$modules[$counter]->moduleName = 'Student_master';
							}	
							if ($modules[$counter]->moduleName == 'examination')
							{
								$modules[$counter]->moduleName = 'Course_master';
							}					
							echo ($counter % 2 == 0) ? '<tr>' : '';								
							echo '<td colspan="'.$colspan.'" width="50%"><input type="checkbox" '.$check.' name="modules['.$modules[$counter]->moduleID.']" id="'.$modules[$counter]->moduleID.'" value="'.$modules[$counter]->moduleID.'" onclick="showhidePer(this,this.id)"> <label for="'.$modules[$counter]->moduleID.'">'.ucfirst($modules[$counter]->moduleName).'</label>';
		
							echo ' <div style="margin-left:25px;'.$display.'" id="div:'.$modules[$counter]->moduleID.'">';
							
							if(!in_array($modules[$counter]->moduleName, array('feedback','mailer')))
							{
								echo '<input type="checkbox" '.$checkeda.' name="modules['.$modules[$counter]->moduleID.'][]" value="A" id="'.$modules[$counter]->moduleID.':A" >Add';
								echo '<input type="checkbox" '.$checkede.' name="modules['.$modules[$counter]->moduleID.'][]" value="E" id="'.$modules[$counter]->moduleID.':E">Edit';
							}
							
							if (in_array($modules[$counter]->moduleName, array('dashboard','question_master','backup','report'))) 
							{
								echo '<input type="checkbox" '.$checkeded.' name="modules['.$modules[$counter]->moduleID.'][]" value="ED" id="'.$modules[$counter]->moduleID.':ED" >Export Data';
							}
							echo   	'</div>					
									</td>';				
							echo ($counter % 2 != 0) ? '</tr>' : '';
							
						}	 
					}
				?>	
			</table>
		</td>
	</tr>

	<tr>
		<td colspan="6">
			<div align="center" style="margin-top:10px">
				<input type="submit" name="editrole"   id="editrole" value="Update Role" class="buttons rounded"  title="Click to edit">
				  &nbsp;&nbsp;
				<input type="button" name="fileback" id="fileback" class="buttons rounded"  value="Back" onClick="location.href='<?php print CreateURL('index.php','mod=role');?>'">
			</div>
		</td>
	</tr>
</table>
				</fieldset>
			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

				</div><!--Content div closed-->
			</td>
		</tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>
<script>
function showhidePer(module, id)
{
	div = 'div:'+id;
	//alert(div);
	if (module.checked)
	{	
		document.getElementById(div).style.display='';
	}
	else
	{
		document.getElementById(id+':A').checked=false;
		document.getElementById(id+':E').checked=false;
		document.getElementById(div).style.display='none';
		if (document.getElementById(id+':ED'))
		{
			document.getElementById(id+':ED').checked = false;
		}
	}	
}
</script>