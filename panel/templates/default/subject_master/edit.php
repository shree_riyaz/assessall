<?php
 /*****************Developed by :- Chirayu Bansal
	                Date         :- 18-aug-2011
					Module       :- Subject master
					Purpose      :- Template for Edit particular Subject
	***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Subject Master</title>


</head>
<body onload="document.editfrm.subject_name.focus();">
<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
				include_once(CURRENTTEMP."/"."header.php");
			?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
			<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
	<div id="main">
		<div id="contents">
			<form id="editfrm" name="editfrm" method="post" action="">
			<fieldset class="rounded" style="width: 50%;"><legend>Edit Subject</legend><br/>
			<?php
				if(isset($_SESSION['error']))
				{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
				}
				
				if(isset($_SESSION['success']))
				{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
				}
			?>
					<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">
					  
					  <tr>
					  <td colspan="4"><div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div></td>
					  </tr>
					  <tr>
						<td width="170"><div align="right">Subject Name:<span class="red">*</span></div></td>
						<td width="200" colspan="2"><input name="subject_name" id="subject_name" type="text" maxlength="50" class="rounded textfield" value="<?php if($frmdata['subject_name']) echo htmlentities(stripslashes($frmdata['subject_name'])); else echo htmlentities(stripslashes($subject_detail->subject_name)); ?>" onchange="isChar(this.value,this.id, 'subject name');"/></td>
					  </tr>
					  <tr>
						<td></td>
					  </tr>
					  <!--<tr>
						<td>&nbsp;</td>
						<td><div id="mandatory" align="left">Note: Fields marked ( <span style="color:#FF0000;">*</span> ) is mandatory.</div></td>
						<td>&nbsp;</td>
					  </tr>
					  --><tr>
						<td>&nbsp;</td>
						<td colspan="3">
						  <div align="left">
						    <input type="submit" name="editsubject" id="editsubject" value="Update" class="buttons rounded" />
				            <input type="button" name="cancel" value="Cancel" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=subject_master&do=manage'"/>
						  </div></td>
					  </tr>
					  
					</table>
				</fieldset>
				<br/>
				
		
			  </form>
	  
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>
