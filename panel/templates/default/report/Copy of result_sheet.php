<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Reports</title>


<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.Charts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionChartsExportComponent.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.jqueryplugin.js"; ?>'></script>

<script language="javascript" type="text/javascript">
$(document).ready(
function()
{
	$('#year').change(
	function()
	{
		if($(this).val() == '')
			$('#tr_month').hide();
		else
			$('#tr_month').show();
	});

	$('#year').trigger('change');
});
function check_search()
{
	var msg = '';
	var flag=0;
	
	var exam_id = document.getElementById('exam_id');
	var result_type = document.getElementById('result_type');

	if(exam_id.value=='')
	{
		msg+= "Please select course.\n";
		focusAt=exam_id;
		flag=1;
	}
	else if(result_type.value=='')
	{
		msg+= "Please select result type.\n";
		if(flag==0)
			focusAt=result_type;
		flag=2;
	}

	if(msg!='')
	{
		alert(msg);
		focusAt.focus();
		return false;
	}
	return true;
}

</script>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>

<body onload="document.frmlist.exam_id.focus();">
<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	<tr><td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td></tr>
	<tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				<div id="main">
					<div id="contents">
						<form action="" method="post" name="frmlist" id="frmlist">
							<legend>Result Sheet</legend> 
							<?php 
								// Show particular Messages
								if(isset($_SESSION['error']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
									echo $_SESSION['error'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['error']);
								}
						
								if(isset($_SESSION['success']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
									echo $_SESSION['success'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['success']);
								} 
							?>

<div class="search-div">
<fieldset class="rounded search-fieldset">
<legend>Search</legend>
	
<br><div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>

<table border="0" cellspacing="1" cellpadding="4" width="100%" style="">
<tr>
	<td align="right">Course Name:<span class="red">*</span></td>
	<td>
		<select name="exam_id" id="exam_id" class="rounded">
			<option value=''>Select Course</option>
			<?php
			if(is_array($exam))
			{
				for($counter=0;$counter<count($exam);$counter++)
				{
					$selected='';
					if ($exam[$counter]->id == $frmdata['exam_id'])
					{
						$selected='selected';
						$exam_name = stripslashes($exam[$counter]->exam_name);
					}
					echo '<option value="'.$exam[$counter]->id.'"'.$selected.'>'. stripslashes($exam[$counter]->exam_name).'</option>';
				}
			}
			?>
		</select>
	</td>
</tr>
<tr>
	<td align="right" nowrap="nowrap">Result Type:<span class="red">*</span></td>
	<td>
		<span id="result_type_span">
			<select name="result_type" id="result_type" class="rounded">
				<option value=''>Select Result Type</option>
				<option value='test_wise' <?php if($frmdata['result_type']=='test_wise') echo 'selected'; ?>>Test Wise Result</option>
				<option value='overall'	<?php if($frmdata['result_type']=='overall') echo 'selected'; ?>>Overall Result</option>
			</select>
		</span>
	</td>
</tr>
<tr>
	<td align="right">Year:</td>
	<td>
		<select name="year" id="year" class="rounded">
			<option value=''>Select Year</option>
			<?php
			if(is_array($testyearlist))
			{
				foreach($testyearlist as $year)
				{
					$selected='';
					if ($year == $frmdata['year'])
					{
						$selected='selected';
					}
					echo "<option value='$year' $selected >$year</option>";
				}
			}
			?>
		</select>
	</td>
</tr>
<tr id="tr_month" style="display: none;">
	<td align="right">Month:</td>
	<td>
		<select name="month" id="month" class="rounded">
			<option value=''>Select Month</option>
			<?php
			//if(is_array($testmonthlist))
			{
				for($counter=1;$counter<=12;$counter++)
				{
					$selected='';
					if ($counter == $frmdata['month'])
					{
						$selected='selected';
					}
					echo '<option value="'.$counter.'"'.$selected.'>'. date('F', strtotime("01-$counter-2011")).'</option>';
				}
			}
			?>
		</select>
	</td>
</tr>

<tr>
	<td colspan="6" align="center">
		<input type="submit" class="buttons rounded" name="Submit" value="Show" onclick="return check_search();" />
		<input type="button" class="buttons" name="clear_search" id="clear_search" value="Clear Search"	onclick="window.location='<?php echo CreateURL('index.php','mod=report&do=result_sheet'); ?>'" />
	</td>
</tr>
</table>

</fieldset>
</div>

<?php
if($subject_result || $test_detail)
{
	if($result_type=='overall')
	{
		$show_total_racords = count($subject_result);
		$totalCount = count($subject_result);
	}
	elseif($result_type=='test_wise')
	{
		$show_total_racords = count($test_detail);
	} ?>
	
	<table width="100%" align="right" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<?php print "Showing Results:".($frmdata['from']+1).'-<span id="show_total_record_span">'.($frmdata['from']+$show_total_racords)."</span> of <span id='show_total_count_span'>".$totalCount."</span>"; ?>
			</td>
			<td align="right"><?php echo getPageRecords();?></td>
		</tr>
	</table>
	<br /> <?php
} 
?> 
<br />

<div id="celebs" style="overflow: auto; overflow-y: hidden;">

<?php
if($result_type=='overall')
{
	$total_marks=0; ?>

	<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">

	<?php
	if(is_array($subject_result))
	{ ?>
		
		<thead>
			<tr class="tblheading">
				<td width="1%" rowspan="4">#</td>
				<td width="20%" rowspan="4">Student Name</td>
				<td width="13%" id="marks_obtain_td" colspan="20">Details Of Subjects</td>
				<td width="9%" rowspan="4">Total Marks Obtain (<span id="total_marks_span"></span>)</td>
				<td width="10%" rowspan="4">Percentage (%)</td>
				<td width="9%" rowspan="4">Result</td>
			</tr>
			
			<tr class="tblheading">
			<?php
			for($index=0; $index<count($exam_subject); $index++)
			{
				?>
				<td colspan="2">
					<?php echo stripslashes($exam_subject[$index]->subject_name); ?>
				</td> <?php
			}
			?>
			</tr>
			
			<tr class="tblheading" id="marks_type_tr">
				<?php
				for($index=0; $index<count($exam_subject); $index++)
				{
					$total_marks+=$exam_subject[$index]->subject_max_mark; ?>
					
					<td>Written (<?php echo $exam_subject[$index]->subject_max_mark; ?>)</td>				
					<td>Result</td> <?php
				} 
				?>
			</tr>
		</thead>
		
		<tbody>	<?php

		$report_data = array();
		for ($counter = 0; $counter < count($subject_result); $counter++)
		{
		    $total_marks_obtain = 0;
		    $candidate_overall_result = 'Pass';
		    if (!isset($report_data[$subject_result[$counter]->id]) && $report_data[$subject_result[$counter]->id] == '')
		    {
		        $report_data[$subject_result[$counter]->id]['name'] = $subject_result[$counter]->first_name . ' ' . $subject_result[$counter]->last_name;

		        for ($counter1 = 0; $counter1 < count($subject_result); $counter1++)
		        {
		            $result = 'Pass';
		            if ($subject_result[$counter]->id == $subject_result[$counter1]->id)
		            {
		                $report_data[$subject_result[$counter]->id][$subject_result[$counter1]->subject_id]['marks'] = $subject_result[$counter1]->marks_obtained;
		                $total_marks_obtain += $subject_result[$counter1]->marks_obtained;
		                if ($subject_result[$counter1]->result == 'F')
		                {
		                    $result = 'Fail';
		                    $candidate_overall_result = 'Fail';
		                }
		                $report_data[$subject_result[$counter]->id][$subject_result[$counter1]->subject_id]['subject_result'] = $result;
		            }
		        }
		       
		        $report_data[$subject_result[$counter]->id]['candidate_result'] = $candidate_overall_result;
		        $report_data[$subject_result[$counter]->id]['candidate_total_marks_obtain'] = $total_marks_obtain;
		    }
		}
	}

	$srNo=$frmdata['from'];
	$totalCount = count($report_data);
	$count=count($report_data);
	if($report_data)
	{
		foreach ($report_data as $candidate_id=>$data)
		{
			$srNo=$srNo+1;
			if(($srNo%2)==0)
			{
				$trClass="tdbgwhite";
			}
			else
			{
				$trClass="tdbggrey";
			} ?>
				
			<tr class='<?= $trClass; ?>'>
			
			<td><?php echo $srNo; ?></td>
			<td><?php echo ucfirst(strtolower(stripslashes($data['name']))); ?></td>
			
			<?php
			for($counter = 0; $counter<count($exam_subject); $counter++)
			{ ?>
				
				<td>
					<?php 
					if(isset($data[$exam_subject[$counter]->subject_id]['marks']))
					{
						echo $data[$exam_subject[$counter]->subject_id]['marks'];
					}
					else
					{
						echo 'NA';
						$data['candidate_result']='NA';
					} 
					?>
				</td>
									
				<td>
					<?php 
						if(isset($data[$exam_subject[$counter]->subject_id]['marks'])) 
							echo $data[$exam_subject[$counter]->subject_id]['subject_result']; 
						else 
							echo 'NA'; 
					?>
				</td> <?php
			}
			?>
			
			<td><?php echo $data['candidate_total_marks_obtain']; ?></td>
			<td><?php echo number_format((($data['candidate_total_marks_obtain']/$total_marks)*100), 2, '.', ''); ?></td>
			<td><?php echo $data['candidate_result']; ?></td>
			</tr><?php
		}
	}
	else
	{
		echo "<tr><td colspan='20' align='center'>(0) Record found.</td></tr>";
	}
	?>

	</tbody>
	</table><?php
}

elseif($result_type=='test_wise')
{ ?>

	<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
	<?php
	if($test_detail)
	{ ?>
		
		<thead>
			<tr class="tblheading">
				<td width="1%">#</td>
				<td width="13%">Test Name</td>
				<td width="15%">Test Start Date</td>
				<td width="15%">Test End Date</td>
				<?php if($_SESSION['admin_user_type'] != 'P') { ?>
				<td width="11%">Total Appeared</td>
				<td width="11%">Passed Student</td>
				<td width="11%">Failed Student</td>
				<?php } ?>
				<td width="9%">Details</td>
			</tr>
		</thead>
	
		<tbody>
		<?php
		
		$srNo=$frmdata['from'];
		$count=count($test_detail);
		
		if($_SESSION['admin_user_type'] != 'P')
		$showGraph = true;
		$testCategory = '<categories>';
		$passedCandidates = "<dataset seriesName='Passed Students'>";
		$failedCandidates = "<dataset seriesName='Failed Students'>";
		
		for($counter=0;$counter<$count;$counter++)
		{
			$srNo=$srNo+1;
			if(($counter%2)==0)
			{
				$trClass="tdbggrey";
			}
			else
			{
				$trClass="tdbgwhite";
			} ?>
				
			<tr class='<?php echo $trClass; ?>'>
				<td><?php echo $srNo; ?></td>
				<td><?php echo ($test_name = stripslashes($test_detail[$counter]->test_name)); ?></td>
				<td><?php echo date('d-M-Y h:i A', strtotime($test_detail[$counter]->test_date)); ?></td>
				<td><?php echo date('d-M-Y h:i A', strtotime($test_detail[$counter]->test_date_end)); ?></td>
				<?php if($_SESSION['admin_user_type'] != 'P') { ?>
				<td><?php echo $test_detail[$counter]->total_candidate; ?></td>
				<td><?php echo ($passed = $test_detail[$counter]->pass); ?></td>
				<td><?php echo ($failed = $test_detail[$counter]->fail); ?></td>
				<?php } ?>
				
				<?php 
				$detailed_url = CreateURL('index.php','mod=report&do=view_test_result&exam_id='.$exam_id.'&test_id='.$test_detail[$counter]->test_id);
				?>
				
				<td>
					<img src="<?php echo IMAGEURL; ?>/user_info.gif" style="cursor: pointer;"
						title="Click here for view student result and generate report"
						onclick="window.location='<?php echo $detailed_url; ?>'" 
					/>
				</td>
			</tr><?php
			
			$testCategory .= "<category label='$test_name' />";
			$passedCandidates .= "<set value='$passed' link='$detailed_url' />";
			$failedCandidates .= "<set value='$failed' link='$detailed_url' />";
		}
		
		$testCategory .= '</categories>';
		$passedCandidates .= "</dataset>";
		$failedCandidates .= "</dataset>";
		
		$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
		$graphXml = "<chart caption='$exam_name' xAxisName='Test' yAxisName='No of Students'"
					." bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270'" 
					." showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1'" 
					." html5ExportHandler='$FCExporter' exportFileName='Exam_Result'>";

		$graphXml .= $testCategory;
		$graphXml .= $passedCandidates;
		$graphXml .= $failedCandidates;
		$graphXml .= '</chart>';
		
		?>
		
		</tbody><?php
	}
	else
	{
		echo "<tr><td colspan='7' align='center'>(0) Record found.</td></tr>";
	}
	?>

	</table> <?php
} ?>

</div>

<div id="graph" style="margin-top: 20px;text-align: center;"></div>

<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
	<tr>
		<td width="10%">&nbsp;</td>
		<td width="54%">&nbsp;</td>
		<td width="36%" align="right">&nbsp; 
			<?php 
			if($subject_result)
			{
				$year_params='';
				$month_params='';
				$stream_params='';
				if($frmdata['year']!='')
				{
					$year_params = "&year=".$frmdata['year'];
				}
						
				if($frmdata['month']!='')
				{
					$month_params = "&month=".$frmdata['month'];
				}
						
				if($frmdata['stream_id']!='')
				{
					$stream_params = "&stream_id=".$frmdata['stream_id'];
				}
	
				if($exportEnabled)
				{ ?> 
					
					<img style="cursor: pointer;" src="<?php echo IMAGEURL ?>/file-xls.gif" alt="Export to Excel"
						title="Click here for export data to excel sheet"
						onclick="window.location='<?php echo ROOTURL.'/index.php?mod=export&exam_id='.$exam_id.$year_params.$stream_params.$month_params; ?>'" 
					/>
				<?php /* ?>	
					&nbsp;&nbsp;
				
					<img style="cursor: pointer;" src="<?php echo IMAGEURL ?>/file-pdf.gif" alt="Export to PDF"
						title="Click here for export data to PDF"
						onclick="window.location='<?php echo ROOTURL.'/index.php?mod=export&toPDF=1&exam_id='.$exam_id.$year_params.$stream_params.$month_params; ?>'" 
					/>	<?php */
				}
			}
			?>
		</td>
	</tr>
	
	<tr>
		<?php
		if($subject_result || $test_detail)
		{ ?>
			
			<td colspan="2">
				<?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?>
			</td><?php
		}

		if (!($test_detail))
		{ ?>
			
			<td align="right">
				<?php PaginationDisplay($totalCount); ?>
			</td> <?php
		} ?>
	</tr>
</table>
		
						<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" /> 
						<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
						<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />
					</form>
				</div><!--Div Contents closed-->
			</div><!--Div main closed-->
		</div><!--Content div closed-->
		</td>
	</tr>
</table>

</div><!--Outer wrapper closed-->

<script language="javascript" type="text/javascript">

<?php
if($result_type=='overall')
{ ?>

	var count_td = $('#marks_type_tr').children('td').length;
	document.getElementById('marks_obtain_td').colSpan=count_td;
	document.getElementById('total_marks_span').innerHTML='<?php echo $total_marks; ?>';
	typeof(document.getElementById('show_total_record_span'));
	document.getElementById('show_total_record_span').innerHTML='<?php echo $count; ?>';
	document.getElementById('show_total_count_span').innerHTML='<?php echo $totalCount; ?>';
	<?php
}
 
if($return && !(isset($_SESSION['return_submit'])))
{
	$_SESSION['return_submit']=1; ?>
	
	document.getElementById('exam_id').value='<?php echo $exam_id; ?>';
	document.getElementById('result_type').value='test_wise';
	document.frmlist.submit(); <?php
}
?>

<?php if($showGraph) { ?>
FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/StackedColumn3D.swf", 'chart', 900, 400, 0, 1);
myChart.setXMLData("<?php echo $graphXml; ?>");
myChart.render('graph');
<?php } ?>

</script>
</body>
</html>