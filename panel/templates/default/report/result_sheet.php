<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Reports</title>

<?php 


?>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.HC.Charts.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionChartsExportComponent.js"; ?>'></script>
<script language="JavaScript" src='<?php echo ROOTURL . "/lib/FusionCharts/FusionCharts.jqueryplugin.js"; ?>'></script>

<script language="javascript" type="text/javascript">
$(document).ready(
function()
{
	$('#year').change(
	function()
	{
		if($(this).val() == '')
			$('#tr_month').hide();
		else
			$('#tr_month').show();
	});

	$('#year').trigger('change');
});
function check_search()
{
	var msg = '';
	var flag=0;
	
	var exam_id = document.getElementById('exam_id');

	if(exam_id.value=='')
	{
		msg+= "Please select course.\n";
		focusAt=exam_id;
		flag=1;
	}

	if(msg!='')
	{
		alert(msg);
		focusAt.focus();
		return false;
	}
	return true;
}

</script>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript();
?>

<body onload="document.frmlist.exam_id.focus();">
<div id="outerwrapper">
<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	<tr><td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td></tr>
	<tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
				<div id="main">
					<div id="contents">
						<form action="" method="post" name="frmlist" id="frmlist">
							<legend>Result Sheet</legend> 
							<?php 
								// Show particular Messages
								if(isset($_SESSION['error']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
									echo $_SESSION['error'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['error']);
								}
						
								if(isset($_SESSION['success']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
									echo $_SESSION['success'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['success']);
								} 
							?>

<div class="search-div">
<fieldset class="rounded search-fieldset">
<legend>Search</legend>
	
<br><div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>

<table border="0" cellspacing="1" cellpadding="4" width="100%" style="">
<tr>
	<td align="right">Course Name:<span class="red">*</span></td>
	<td>
		<select name="exam_id" id="exam_id" class="rounded">
			<option value=''>Select Course</option>
			<?php
			if(is_array($exam))
			{
				for($counter=0;$counter<count($exam);$counter++)
				{
					$selected='';
					if ($exam[$counter]->id == $frmdata['exam_id'])
					{
						$selected='selected';
						$exam_name = stripslashes($exam[$counter]->exam_name);
					}
					echo '<option value="'.$exam[$counter]->id.'"'.$selected.'>'. stripslashes($exam[$counter]->exam_name).'</option>';
				}
			}
			?>
		</select>
	</td>
</tr>
<tr>
	<td align="right">Year:</td>
	<td>
		<select name="year" id="year" class="rounded">
			<option value=''>Select Year</option>
			<?php
			if(is_array($testyearlist))
			{
				foreach($testyearlist as $year)
				{
					$selected='';
					if ($year == $frmdata['year'])
					{
						$selected='selected';
					}
					echo "<option value='$year' $selected >$year</option>";
				}
			}
			?>
		</select>
	</td>
</tr>
<tr id="tr_month" style="display: none;">
	<td align="right">Month:</td>
	<td>
		<select name="month" id="month" class="rounded">
			<option value=''>Select Month</option>
			<?php
			//if(is_array($testmonthlist))
			{
				for($counter=1;$counter<=12;$counter++)
				{
					$selected='';
					if ($counter == $frmdata['month'])
					{
						$selected='selected';
					}
					echo '<option value="'.$counter.'"'.$selected.'>'. date('F', strtotime("01-$counter-2011")).'</option>';
				}
			}
			?>
		</select>
	</td>
</tr>

<tr>
	<td colspan="6" align="center">
		<input type="submit" class="buttons rounded" name="Submit" value="Show" onclick="return check_search();" />
		<input type="button" class="buttons" name="clear_search" id="clear_search" value="Clear Search"	onclick="window.location='<?php echo CreateURL('index.php','mod=report&do=result_sheet'); ?>'" />
	</td>
</tr>
</table>

</fieldset>
</div>

<?php if($showResult) { ?>

<?php
if($test_detail)
{
	$show_total_racords = count($test_detail); ?>
	
	<table width="100%" align="right" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<?php print "Showing Results:".($frmdata['from']+1).'-<span id="show_total_record_span">'.($frmdata['from']+$show_total_racords)."</span> of <span id='show_total_count_span'>".$totalCount."</span>"; ?>
			</td>
			<td align="right"><?php echo getPageRecords();?></td>
		</tr>
	</table>
	<br /> <?php
} 
?> 
<br />

<div id="celebs" style="overflow: auto; overflow-y: hidden;">

<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
<?php
if($test_detail)
{ ?>
	<thead>
		<tr class="tblheading">
			<td width="1%">#</td>
			<td width="13%">Test Name</td>
			<td width="15%">Test Start Date</td>
			<td width="15%">Test End Date</td>
	
			<?php if($_SESSION['admin_user_type'] != 'P') { ?>
			<td width="11%">Total Appeared</td>
			<td width="11%">Passed Student</td>
			<td width="11%">Failed Student</td>
			<?php } ?>
	
			<td width="9%">Details</td>
		</tr>
	</thead>

	<tbody>
	<?php
	
	$srNo=$frmdata['from'];
	$count=count($test_detail);
	
	if($_SESSION['admin_user_type'] != 'P')	$showGraph = true;
	
	$testCategory = '<categories>';
	$passedCandidates = "<dataset seriesName='Passed Students'>";
	$failedCandidates = "<dataset seriesName='Failed Students'>";
	
	for($counter=0;$counter<$count;$counter++)
	{
		$srNo=$srNo+1;
		if(($counter%2)==0)
		{
			$trClass="tdbggrey";
		}
		else
		{
			$trClass="tdbgwhite";
		} ?>
			
		<tr class='<?php echo $trClass; ?>'>
			<td><?php echo $srNo; ?></td>
			<td><?php echo ($test_name = stripslashes($test_detail[$counter]->test_name)); ?></td>
			<td><?php echo date('d-M-Y h:i A', strtotime($test_detail[$counter]->test_date)); ?></td>
			<td><?php echo date('d-M-Y h:i A', strtotime($test_detail[$counter]->test_date_end)); ?></td>
			
			<?php if($_SESSION['admin_user_type'] != 'P') { ?>
			<td><?php echo $test_detail[$counter]->total_candidate; ?></td>
			<td><?php echo ($passed = $test_detail[$counter]->pass); ?></td>
			<td><?php echo ($failed = $test_detail[$counter]->fail); ?></td>
			<?php } ?>
			
			<?php 
			$detailed_url = CreateURL('index.php','mod=report&do=view_test_result&exam_id='.$test_detail[$counter]->exam_id.'&test_id='.$test_detail[$counter]->test_id);
			?>
			
			<td>
				<img src="<?php echo IMAGEURL; ?>/user_info.gif" style="cursor: pointer;"
					title="Click here for view student result and generate report"
					onclick="window.location='<?php echo $detailed_url; ?>'" 
				/>
			</td>
		</tr><?php
		
		$testCategory .= "<category label='$test_name' />";
		$passedCandidates .= "<set value='$passed' link='$detailed_url' />";
		$failedCandidates .= "<set value='$failed' link='$detailed_url' />";
	}
	
	$testCategory .= '</categories>';
	$passedCandidates .= "</dataset>";
	$failedCandidates .= "</dataset>";
	
	$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
	$graphXml = "<chart caption='$exam_name' xAxisName='Test' yAxisName='No of Students'"
				." bgColor='E6E6E6,F0F0F0' bgAlpha='100,50' bgRatio='50,100' bgAngle='270'" 
				." showNames='1' useEllipsesWhenOverflow='0' showBorder='1' exportEnabled='1'" 
				." html5ExportHandler='$FCExporter' exportFileName='Exam_Result'>";

	$graphXml .= $testCategory;
	$graphXml .= $passedCandidates;
	$graphXml .= $failedCandidates;
	$graphXml .= '</chart>';
	
	?>
	
	</tbody><?php
}
else
{
	echo "<tr><td colspan='7' align='center'>(0) Record found.</td></tr>";
}
?>

</table>
</div>
<div id="graph" style="margin-top: 20px;text-align: center;"></div>

<?php } ?>

<?php if($test_detail) { ?>
<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
<tr>
	<td colspan="2">
		<?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?>
	</td>
</tr>
</table>
<?php } ?>
	
						<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" /> 
						<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
						<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />
					</form>
				</div><!--Div Contents closed-->
			</div><!--Div main closed-->
		</div><!--Content div closed-->
		</td>
	</tr>
</table>

</div><!--Outer wrapper closed-->

<script language="javascript" type="text/javascript">

<?php if($return) { ?>
document.getElementById('exam_id').value='<?php echo $exam_id; ?>';
<?php } ?>

<?php if($showGraph) { ?>
FusionCharts.setCurrentRenderer('javascript');
var myChart = new FusionCharts("<?php echo ROOTURL; ?>/lib/FusionCharts/StackedColumn3D.swf", 'chart', 900, 400, 0, 1);
myChart.setXMLData("<?php echo $graphXml; ?>");
myChart.render('graph');
<?php } ?>

</script>
</body>
</html>