<?php
 /*****************Developed by :- Ashwini Agarwal
	                Date         :- 7-june-2012
					Module       :- Question master
					Purpose      :- Template for video preview
	***********************************************************************************/
?>
<?php $video_href = ROOTURL . '/uploadfiles/' . $question_detail->video; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Online Examination</title>
<script type="text/javascript" src="<?php echo ROOTURL; ?>/js/swfobject.js"></script>
</head>

<body>
<div id="video_preview" align="center" style="">
<a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this video.
</div>
<script type="text/javascript">
	var s1 = new SWFObject("<?php echo ROOTURL ?>/lib/mediaplayer.swf","ply","600","385","9","#FFFFFF");
	s1.addParam("allowfullscreen","true");
	s1.addParam("allowscriptaccess","always");
	s1.addVariable('file','');
	s1.addParam("flashvars","&file=<?php echo $video_href; ?>&stretching=exactfit&autostart=false");
	s1.write("video_preview");
</script>
</body>
</html>