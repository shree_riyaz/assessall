<?php
/**************************************************************************************
 * 		Developed by :- Akshay Yadav
 * 		Module       :- Question master
 * 		Purpose      :- Template for Add/Edit particular test detail
 **************************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Questions Entry Form</title>

<script type="text/javascript" src="<?php echo JS . "/jquery.min.js"; ?>"></script>
<script type="text/javascript" src="<?php echo JS . "/jquery.form.js";?>"></script>
<script type="text/javascript" src="<?php echo ROOTURL.'/lib/ckeditor/'; ?>ckeditor.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(
function()
{
	CKEDITOR.replace( 'question_title', { toolbar : [ [ 'Equation', 'Bold', 'Italic','base64image' ] ] });
	//CKEDITOR.replace( 'question_title_hindi', { toolbar : [ [ 'Equation', 'Bold', 'Italic','base64image' ] ] });
});

$(document).ready(function() {
    $("#content_tabs").find("[id^='tab']").hide(); // Hide all content
    $("#tabs li:first").attr("id","current"); // Activate the first tab
    $("#content_tabs #tab1").fadeIn(); // Show first tab's content
    
    $('#tabs a').click(function(e) {
        e.preventDefault();
        if ($(this).closest("li").attr("id") == "current"){ //detection for current tab
         return;       
        }
        else{             
          $("#content_tabs").find("[id^='tab']").hide(); // Hide all content
          $("#tabs li").attr("id",""); //Reset id's
          $(this).parent().attr("id","current"); // Activate this
          $('#' + $(this).attr('name')).fadeIn(); // Show content for the current tab
        }
    });
});


function showoption(type)
{	
	if(type == 'S')
	{
		$('#tr_hints').hide();
	}
	else
	{
		$('#tr_hints').show();
	}
	var no_of_images = Math.abs(document.getElementById('no_of_images').value);
	var no_of_options = Math.abs(document.getElementById('no_of_options').value);
	var no_of_cols = Math.abs(document.getElementById('no_of_cols').value);
	xajax_showoption(type, no_of_images, no_of_options, no_of_cols);
	if(type == 'M')
	{
		if(setTimeout("CKEDITOR.replace( 'option_1', { toolbar : [ [ 'Equation', 'Bold', 'Italic' ] ] })",140));
		if(setTimeout("CKEDITOR.replace( 'option_2', { toolbar : [ [ 'Equation', 'Bold', 'Italic' ] ] })",150));
		if(setTimeout("CKEDITOR.replace( 'option_3', { toolbar : [ [ 'Equation', 'Bold', 'Italic' ] ] })",200));
		if(setTimeout("CKEDITOR.replace( 'option_4', { toolbar : [ [ 'Equation', 'Bold', 'Italic' ] ] })",250));
		setTimeout("CKEDITOR.replace( 'option_5', { toolbar : [ [ 'Equation', 'Bold', 'Italic' ] ] })",300);
	}
	if(type == 'S')
	{
	//	"<input type='text' name='answer_title'>";
	}
}
function numonly()
{
	$('#image_type').val('');
	var marks = document.getElementById("marks");
	if(marks.value.length>0)
	{
		if(isNaN(parseInt(marks.value)))
		{
			alert('Please enter numeric integer value in marks.');
			marks.value='';
			marks.focus();
			return false;
		}
		
		if(marks.value>10)
		{
			alert('Please enter marks less than or equal to 10.');
			marks.value='';
			marks.focus();
			return false;
		}
		else if(marks.value<=0)
		{
			alert('Please enter marks greater than 0.');
			marks.value='';
			marks.focus();
			return false;
		}
	}
	return true;
}
function uploadTempFile(val)
{
	if(val == null)
		return false;

	var value = val;
	var img_div = "img_" + val;
	var path = "<img height='20' width='20' src='images/loading.gif'>";
	document.getElementById(img_div).innerHTML = path;

	var file = document.getElementById(value);

	var fileName = file.value;
	var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

	if(!(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png" || ext == "PNG"))
	{
		var path = "<font color='red'>Please upload image in correct format.</font>";
		document.getElementById(img_div).innerHTML = path;
		return true;
	}
	else if(file.files)
	{
		var f = file.files[0];
		if(f.size > 2000000)
		{
			var path = "<font color='red'>Please upload image not more than 2MB.</font>";
			document.getElementById(img_div).innerHTML = path;
			file.value = '';
			return true;
		}
	}
	
	document.getElementById('image_type').value=val;
	div = "#" + img_div;
	$("#addquestion").ajaxForm({target: div	}).submit();
	$("#addquestion").ajaxFormUnbind();
	document.getElementById('addquestion').target="";
}
function addMoreImage()
{
	var no_of_images = Math.abs(document.getElementById('no_of_images').value) + 1;
	document.getElementById('no_of_images').value = no_of_images;

	var str = "";
	str += "<tr id='tr_image_"+ no_of_images +"'>";
	str += "<td valign='top'><div align='right'>"+ no_of_images +"</div></td>"; 
	str += "<td width=''><input type='file' name ='image_"+ no_of_images +"' id ='image_"+ no_of_images +"' class='rounded textfield required' size='30' onchange='uploadTempFile(this.name);'/>&nbsp;";
	str += "<input name='question_mark' type='radio' value='"+ no_of_images +"' />&nbsp;";
	str += "<span id='img_image_"+ no_of_images +"'></span></td>";
	str += "</tr>";

	$('#image_add_more').before(str);

	if(no_of_images == 4)
	{
		str = '<a href="javascript:void(0);" onclick="removeLastImage();" >Remove Last</a>';
		$('#remove_image').html(str);
	}
}
function addMoreOption()
{
	var no_of_options = Math.abs(document.getElementById('no_of_options').value) + 1;
	document.getElementById('no_of_options').value = no_of_options;
	
	var str = "";
	str += "<tr id='tr_option_"+ no_of_options +"'>";
	str += "<td valign='top'><div align='right'>"+ no_of_options +"</div></td>"; 
	str += "<td width=''><input type='file' name ='option_"+ no_of_options +"' id ='option_"+ no_of_options +"' class='rounded textfield required' size='30' onchange='uploadTempFile(this.name);'/>";
	str += "<input name='correct_ans[]' type='checkbox' value='"+ no_of_options +"' />&nbsp;";
	str += "<span id='img_option_"+ no_of_options +"'></span></td>";
	str += "</tr>";

	$('#option_add_more').before(str);

	if(no_of_options == 5)
	{
		str = '<a href="javascript:void(0);" onclick="removeLastOption();" >Remove Last</a>';
		$('#remove_option').html(str);
	}
}

function removeLastImage()
{
	var no_of_images = Math.abs(document.getElementById('no_of_images').value);
	document.getElementById('no_of_images').value = no_of_images - 1;
	$('#tr_image_'+no_of_images).remove();

	xajax_unsetSession("image_" + no_of_images);
	
	if(no_of_images == 4)
	{
		$('#remove_image').html('');
	}
}
function removeLastOption()
{
	var no_of_options = Math.abs(document.getElementById('no_of_options').value);
	document.getElementById('no_of_options').value = no_of_options - 1;
	$('#tr_option_'+no_of_options).remove();

	xajax_unsetSession("option_" + no_of_options);
	
	if(no_of_options == 5)
	{
		$('#remove_option').html('');
	}
}
function resetQMark()
{
	$('#image_tab').find("input:radio:checked").attr('checked', false);
	$('#image_tab').find("input:radio:checked").prop('checked', false);
}

function addMoreCol()
{
	var no_of_cols = Math.abs(document.getElementById('no_of_cols').value) + 1;
	document.getElementById('no_of_cols').value = no_of_cols;

	var str = "";
	str += "<tr id='tr_col_"+ no_of_cols +"'>";
	str += "<td valign=''><div align='right'>"+ no_of_cols +"</div></td>"; 
	str += "<td><input type='text' name ='left_col_"+ no_of_cols +"' id ='left_col_"+ no_of_cols +"' class='rounded textfield required' size='30' />";
	str += "<td><input type='text' name ='right_col_"+ no_of_cols +"' id ='right_col_"+ no_of_cols +"' class='rounded textfield required' size='30' />";
	str += "</tr>";

	$('#col_add_more').before(str);

	if(no_of_cols == 3)
	{
		str = '<a href="javascript:void(0);" onclick="removeLastCol();" >Remove Last</a>';
		$('#remove_col').html(str);
	}
}
function removeLastCol()
{
	var no_of_cols = Math.abs(document.getElementById('no_of_cols').value);
	document.getElementById('no_of_cols').value = no_of_cols - 1;
	$('#tr_col_'+no_of_cols).remove();

	if(no_of_cols == 3)
	{
		$('#remove_col').html('');
	}
}

</script>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
$type= $frmdata['question_type'];
?>
</head>

<body onload="showoption(<?php echo "'$type'"; ?>);document.addquestion.question_title.focus();">
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
		<tr>
			<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
		</tr>
		<tr>
			<td>
				<div id="content">
					<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					<div id="main">
						<div id="contents">
							<form action="" method="post" name="addquestion" id="addquestion" enctype="multipart/form-data">

								<fieldset class="rounded">
								<?php if($isEdit){ ?>
									<legend>Edit Question</legend>
								<?php  }else{ ?>
									<legend>Add Question</legend>
									<?php } ?>
									<input type="hidden" name="no_of_images" id="no_of_images" value="<?php echo isset($_SESSION['no_of_images']) ? $_SESSION['no_of_images'] : 3 ?>"></input>
									<input type="hidden" name="no_of_options" id="no_of_options" value="<?php if($total_ans_options!='') echo $total_ans_options;else echo 5; ?>"/>
									<input type="hidden" name="no_of_cols" id="no_of_cols" value="<?php echo isset($frmdata['no_of_cols']) ? $frmdata['no_of_cols'] : 5?>"/>
				
									<?php 
										if(isset($_SESSION['no_of_images']))
											unset($_SESSION['no_of_images']);
										if(isset($_SESSION['no_of_options']))
											unset($_SESSION['no_of_options']);
									?>
					
									<?php 
									// Show particular Messages
									if(isset($_SESSION['error']))
									{
										echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
										echo $_SESSION['error'];
										echo '</td></tr></tbody></table>';
										unset($_SESSION['error']);
									}
									if(isset($_SESSION['success']))
									{
										echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
										echo $_SESSION['success'];
										echo '</td></tr></tbody></table>';
										unset($_SESSION['success']);
									}
?>								
<table border="0" cellspacing="0" cellpadding="4" id="tbl_dashboard" width="100%" style="padding: 10px;">
<tr>
	<td colspan="4">
		<div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>
	</td>
</tr>
 
<?php

if($_GET['id'])
{
?>
    <tr>
            <td colspan="1">
                    <div>Entered Paragraph:<span class="red">*</span></div>
            </td>
            <td colspan="3">
                    <div class="rounded" ><?php echo $result_data->question_title; ?></div>
            </td>
    </tr>
<?php
}
?>

<tr>
	<td valign="top">
		<div>Enter Question:<span class="red">*</span></div>
	</td>
	<td colspan="3">
		<div id="english"><textarea id="question_title" cols="85" rows="7" class="rounded" name="question_title"><?php echo $frmdata['question_title']; ?></textarea></div>
	</td>
</tr>

<tr>
	<td valign="top">
		<div>Marks:<span class="red">*</span></div>
	</td>
	<td>
		<input type="text" name="marks" id="marks" class="rounded" colspan="3" valign="top" value="<?php echo $frmdata['marks'] ?>" size="2" maxlength="2"/>
	</td>
	<td valign="top" rowspan="3">
		<fieldset align='left' class="rounded">
			<legend>Select Level</legend>
			<table border="0" cellspacing="3" cellpadding="0" align="center">
			<tr>
				<td>
					<p>
					<?php 
						foreach ($question_level as $key=>$value)
						{
							if(!$frmdata['question_level'])
							{
								$frmdata['question_level']='B';
							}
							$selected='';
							if ($frmdata['question_level']==$key)
							{
								$selected='checked';
							}
							echo '<label style="padding-right:15px;"><input type="radio" name="question_level" value="'.$key.'"'.$selected.' >'.$value.'</label>';
						}
					?>
					</p>
				</td>
			</tr>
			<tr>
				<td>
			  		Experience Level: 
			  		<select class="rounded" name="experience_level">
			  		<?php
			  			for($exp = 0; $exp <= 20; $exp++)
			  			{
			  				$selected = '';
			  				if($exp == $frmdata['experience_level'])
			  					$selected = 'selected'; ?>
			  				
			  				<option <?php echo $selected; ?> value="<?php echo $exp; ?>"><?php echo $exp; ?></option><?php
			  			}
			  		?>
			  		</select>
			  		Years
			  	</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>

<tr>
	<td valign="top">
		<div>Subject:<span class="red">*</span></div>
	</td>
	<td valign="top">
	<?php if($_GET['id']){?>
	<input type='hidden' name="subject_id" id="subject_id" value="<?php echo $result_data->subject_id; ?>" >
	<span><?php echo $result_data_name->subject_name; ?> </span>
	<?php }else{ ?>
             <select class="rounded" name="subject_id" id="subject_id"  >
			<option value="" >Select Subject</option>
			<?php 
                       
                         if(is_array($subject))
			{
				for($counter=0;$counter<count($subject);$counter++)
				{
					$selected='';
                                 if($_GET['id'] && $result_data->subject_id)
                                {
                                    if ($subject[$counter]->id == $result_data->subject_id)
                                     {
                                         $selected='selected';
                                     }                              
                                }elseif ($subject[$counter]->id == $frmdata['subject_id'])
                                   {
                                     $selected='selected';
                                   }

                                     echo '<option value="'.$subject[$counter]->id.'"'.$selected.'>'. stripslashes($subject[$counter]->subject_name).'</option>';
				}
			}              
			?>
		</select>
		<?php } ?>
	</td>
</tr>

<tr>
	<td valign="top">
	
		<div>Question Type:<span class="red">*</span></div>
	</td>
	<td width="350" valign="top">
		<select class="rounded" name="question_type" id="question_type" onchange="showoption(this.value);">
			<option value="">Select Type</option>
			<?php 
				foreach ($question_type as $key=>$value)
				{
					$selected='';
					if ($frmdata['question_type']==$key)
					{
						$selected='selected';
					}
					echo '<option value="'.$key.'"'.$selected.' >'.$value.'</option>';
				}
			?>
		</select>
	</td>
</tr>

<tr>
	<td colspan="5" id="opt"></td>
</tr>

<tr>
	<td valign="top">
		<div>Minimum Solving<br />Time (M:S):</div>
	</td>
	<td>
		<select name="minimum_min" class="rounded">
		<?php 
			for($min=0;$min<60;$min++)
			{	
				$selected = '';
				if($min == $frmdata['minimum_min'])
					$selected = 'selected'; ?>
					
				<option value='<?php echo $min; ?>' <?php echo $selected; ?>><?php echo $min; ?></option> <?php
			}
		?>
		</select> :
		<select name="minimum_sec" class="rounded">
		<?php 
			for($min=0;$min<60;$min++)
			{	
				$selected = '';
				if($min == $frmdata['minimum_sec'])
					$selected = 'selected'; ?>
					
				<option value='<?php echo $min; ?>' <?php echo $selected; ?>><?php echo $min; ?></option> <?php
			}
		?>
		</select>
	</td>
</tr>

<tr>
	<td valign="top">
		<div>Maximum Solving<br />Time (M:S):</div>
	</td>
	<td>
		<select name="maximum_min" class="rounded">
		<?php 
			for($min=0;$min<60;$min++)
			{	
				$selected = '';
				if($min == $frmdata['maximum_min'])
					$selected = 'selected'; ?>
					
				<option value='<?php echo $min; ?>' <?php echo $selected; ?>><?php echo $min; ?></option> <?php
			}
		?>
		</select> :
		<select name="maximum_sec" class="rounded">
		<?php 
			for($min=0;$min<60;$min++)
			{	
				$selected = '';
				if($min == $frmdata['maximum_sec'])
					$selected = 'selected'; ?>
					
				<option value='<?php echo $min; ?>' <?php echo $selected; ?>><?php echo $min; ?></option> <?php
			}
		?>
		</select>
	</td>
</tr>

<tr id="tr_hints">
	<td valign="top">
		<div>Hints:</div>
	</td>
	<td colspan="2">
		<textarea cols="60" rows="5" class="rounded" name="hints"><?php echo $frmdata['hints'] ?></textarea>
	</td>
</tr>

<tr>
	<td valign="top">
		<div>Upload Image Hint:</div>
	</td>
	<td>
		<input type="file" name="image" id="image" class="rounded"/>
		<?php 
  			if(($question_detail->image != '') && file_exists(ROOT.'/uploadfiles/'.$question_detail->image))
  			{  ?>
  				&nbsp;<a class="thumbnail" href="#thumb"><?php echo $question_detail->image; ?>
        			<span><img src="<?php echo ROOTURL.'/uploadfiles/'.$question_detail->image; ?>" /></span>
        		</a><?php						  				
  			}
  		?>
		<div align="left" id="mandatory"><strong>Note:</strong> Please upload image in .JPG, .GIF, .PNG and .JPEG format.<br> Image size should not be more than 2MB. </div>
	</td>
</tr>

<tr>
	<td valign="top">
		<div>Upload Video Hint:</div>
	</td>
	<td>
		<input type="file" name="video" id="video" class="rounded"/>
		<?php 
  			if(($question_detail->video != '') && file_exists(ROOT.'/uploadfiles/'.$question_detail->video))
  			{  
  				$id = $question_detail->id ; ?>
  				&nbsp;<a href="javascript:void(0);" onclick="popup('<?php echo CreateURL('index.php',"mod=question_master&do=preview&id=$id")?>','video_preview');"><?php echo $question_detail->video; ?></a> <?php						  				
  			}
  		?>
		<div align="left" id="mandatory"><strong>Note:</strong> Please upload video in .FLV, .MP4, .AVI, .WAV, <br>.MPEG, .MPEG4, .MOV, .VMW, .MPG, .VID, .M4V format. <br>Video size should not be more than 20MB. </div>
	</td>
</tr>
						  
<tr>
	<td valign="top">
		<div>Discussion:</div>
	</td>
	<td colspan="2">
		<textarea cols="60" rows="5" class="rounded" name="discussion"><?php echo $frmdata['discussion'] ?></textarea>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
	<td style="align:center;">
	<?php if($isEdit) { ?>
		<input name="editQuestion" type="submit" class="buttons rounded"  value="Update" onclick="return numonly();"/>
	<?php } else { ?>
		<input name="addQuestion" type="submit" class="buttons rounded" value="Save" onclick="return numonly();" />
	<?php }
        if($_GET['id'])
        { ?>
            <input name="addQuestioncancle" type="button" class="buttons rounded" value="Cancel" onClick="location.href='<?php print CreateURL('index.php','mod=question_master&do=manage&id='.$_GET['id']);?>'"/>
     <?php   }else{ ?>
		<input name="addQuestioncancle" type="button" class="buttons rounded" value="Cancel" onClick="location.href='<?php print CreateURL('index.php','mod=question_master&do=manage');?>'"/>
        <?php } ?>
     </td>
</tr>
</table>
								</fieldset>
							</form>
					</div>
					</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>
</body>
</html>