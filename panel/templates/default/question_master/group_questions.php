<?php
/**************************************************************************************
 * 		Developed by :- Akshay Yadav
 * 		Date         :- 19-May-2014
 * 		Module       :- Group Questions
 * 		Purpose      :- Template for Add/Edit Group Type of questions
 **************************************************************************************/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Questions Entry Form</title>

<script type="text/javascript" src="<?php echo JS . "/jquery.min.js"; ?>"></script>
<script type="text/javascript" src="<?php echo JS . "/jquery.form.js";?>"></script>
<script type="text/javascript" src="<?php echo ROOTURL.'/lib/ckeditor/'; ?>ckeditor.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(
function()
{
	CKEDITOR.replace( 'question_title', { toolbar : [ [ 'Equation', 'Bold', 'Italic','base64image' ] ] });
});

function showoption(type)
{	
	if(type == 'S')
	{
		$('#tr_hints').hide();
	}
	else
	{
		$('#tr_hints').show();
	}
	var no_of_images = Math.abs(document.getElementById('no_of_images').value);
	var no_of_options = Math.abs(document.getElementById('no_of_options').value);
	//alert(no_of_options);exit;
	var no_of_cols = Math.abs(document.getElementById('no_of_cols').value);
	xajax_showoption(type, no_of_images, no_of_options, no_of_cols);
}
function numonly()
{
	$('#image_type').val('');
	var question_no = document.getElementById("questions");
	var marks = document.getElementById("marks");
	if(marks.value.length>0)
	{
		if(isNaN(parseInt(question_no.value)))
		{
			alert('Please enter numeric integer value in Question No.');
			question_no.value='';
			question_no.focus();
			return false;
		}
		if(isNaN(parseInt(marks.value)))
		{
			alert('Please enter numeric integer value in marks.');
			marks.value='';
			marks.focus();
			return false;
		}
		
		if(question_no.value>10)
		{
			alert('Please enter Question no less than or equal to 10.');
			question_no.value='';
			question_no.focus();
			return false;
		}
		else if(question_no.value<=0)
		{
			alert('Please enter question no greater than 0.');
			question_no.value='';
			question_no.focus();
			return false;
		}
		if(marks.value>10)
		{
			alert('Please enter marks less than or equal to 10.');
			marks.value='';
			marks.focus();
			return false;
		}
		else if(marks.value<=0)
		{
			alert('Please enter marks greater than 0.');
			marks.value='';
			marks.focus();
			return false;
		}
	}
	return true;
}

function resetQMark()
{
	$('#image_tab').find("input:radio:checked").attr('checked', false);
	$('#image_tab').find("input:radio:checked").prop('checked', false);
}
</script>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
$type= $frmdata['question_type'];
?>

</head>

<body onload="showoption(<?php echo "'$type'"; ?>);document.addquestion.question_title.focus();">
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
		<tr>
			<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
		</tr>
		<tr>
			<td>
				<div id="content">
					<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					<div id="main">
						<div id="contents">
							<form action="" method="post" name="addquestion" id="addquestion" enctype="multipart/form-data">
								<fieldset class="rounded">
									<legend>Edit Question</legend>
									<input type="hidden" name="no_of_images" id="no_of_images" value="<?php echo isset($_SESSION['no_of_images']) ? $_SESSION['no_of_images'] : 3 ?>"></input>
									<input type="hidden" name="no_of_options" id="no_of_options" value="<?php echo isset($_SESSION['no_of_options']) ? $_SESSION['no_of_options'] : 5 ?>"/>
									<input type="hidden" name="no_of_cols" id="no_of_cols" value="<?php echo isset($frmdata['no_of_cols']) ? $frmdata['no_of_cols'] : 5?>"/>
									<?php 
										if(isset($_SESSION['no_of_images']))
											unset($_SESSION['no_of_images']);
										if(isset($_SESSION['no_of_options']))
											unset($_SESSION['no_of_options']);
									// Show particular Messages
									if(isset($_SESSION['error']))
									{
										echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
										echo $_SESSION['error'];
										echo '</td></tr></tbody></table>';
										unset($_SESSION['error']);
									}
									if(isset($_SESSION['success']))
									{
										echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
										echo $_SESSION['success'];
										echo '</td></tr></tbody></table>';
										unset($_SESSION['success']);
									}
									?>
				
<table border="0" cellspacing="0" cellpadding="4" id="tbl_dashboard" width="100%" style="padding: 10px;">
<tr>
	<td colspan="4">
		<div id="mandatory" align="left">&nbsp;&nbsp;<?php echo MANDATORYNOTE; ?></div>
	</td>
</tr>
<tr>
	<td valign="top">
		<div>Enter Question:<span class="red">*</span></div>
	</td>
	<td colspan="3">
		<textarea id="question_title" cols="85" rows="7" class="rounded" name="question_title"><?php echo $frmdata['question_title']; ?></textarea>
	</td>
</tr>
<tr>
	<td valign="top">
		<div>No of Questions:<span class="red">*</span></div>
	</td>
	<td>
		<input type="text" name="no_of_questions" id="no_of_questions" class="rounded" colspan="3" valign="top" value="<?php echo $frmdata['questions'] ?>" size="2" maxlength="2"/>
	</td>
</tr>
<tr>
	<td valign="top">
		<div>Marks:<span class="red">*</span></div>
	</td>
	<td>
		<input type="text" name="marks" id="marks" class="rounded" colspan="3" valign="top" value="<?php echo $frmdata['marks'] ?>" size="2" maxlength="2"/>
	</td>
</tr>
    <tr>
        <td valign="top">
            <div>Start:<span class="red">*</span></div>
        </td>
        <td>
            <input type="text" name="start" id="start" class="rounded" colspan="3" valign="top" value="<?php echo $frmdata['start'] ?>" size="2" maxlength="3"/>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <div>End:<span class="red">*</span></div>
        </td>
        <td>
            <input type="text" name="end" id="end" class="rounded" colspan="3" valign="top" value="<?php echo $frmdata['end'] ?>" size="2" maxlength="3"/>
        </td>
    </tr>

<tr>
	<td valign="top">
		<div>Subject:<span class="red">*</span></div>
	</td>
	<td valign="top">
		<select class="rounded" name="subject_id" id="subject_id">
			<option value="" >Select Subject</option>
			<?php 
			if(is_array($subject))
			{
				for($counter=0;$counter<count($subject);$counter++)
				{
					$selected='';
					if ($subject[$counter]->id == $frmdata['subject_id'])
					{
						$selected='selected';
					}
					echo '<option value="'.$subject[$counter]->id.'"'.$selected.'>'. stripslashes($subject[$counter]->subject_name).'</option>';
				}
			}
			?>
		</select>
	</td>
</tr>
<tr>
	<td valign="top">&nbsp;</td>
	<td valign="top">
	<?php if($isEdit) { ?>
		<input name="editQuestion" type="submit" class="buttons rounded"  value="Update" onclick="return numonly();"/>
	<?php } else { ?>
		<input name="addGroupQuestion" type="submit" class="buttons rounded" value="Save" onclick="return numonly();" />
	<?php } ?>
		<input name="addQuestioncancle" type="button" class="buttons rounded" value="Cancel" onClick="location.href='<?php print CreateURL('index.php','mod=question_master&do=manage');?>'"/>
	</td>
</tr>
</table>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>
</body>
</html>