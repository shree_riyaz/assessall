<?php
 /*****************Developed by :- Chirayu Bansal
	            Date         :- 10-sep-2011
				Module       :- Reports
				Purpose      :- Template for Browse all candidate details and provide functionality to generate report
	***********************************************************************************/
//echo ROOT."/uploadfiles/format_question_list.xlsx";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Upload Question List</title>
<script language="javascript" type="text/javascript">

</script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body>

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist" enctype="multipart/form-data">
			<fieldset class="rounded" style="width: 60%;">
			<legend>Question Details</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td valign="top" align="left" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['warning']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="left" style="padding:3px 3px 3px 3px;">';
				echo $_SESSION['warning'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['warning']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			
			<div align="center" style="padding: 5px 0 10px;">
				<a href="#" onclick="window.location='<?php echo ROOTURL.'/index.php?mod=export&csv_file=question'; ?>'">Download format of question list.</a>
			</div>
				<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences">
					  
					  <tr>
					  	<td width="25%" align="right">CSV file:</td>
					  	<td width="25%">
					  		<input type="file" name="question_list" id="question_list" />
					  	</td>
					  	<td width="25%" align="right">Skip first row:</td>
					  	<td width="25%">
					  		<input type="checkbox" name="skip" id="skip" value="y" />
						</td>
					  </tr>
					  <tr><td></td></tr>
					  <tr>
						<td colspan="5">
						  <div align="center">
						    <input type="submit" name="listQuestion" id="listQuestion" value="Upload" class="buttons rounded" />
				            <input type="button" name="cancel" value="Cancel" class="buttons" onclick="window.location='<?php echo ROOTURL; ?>/index.php?mod=question_master&do=manage'"/>
						  </div></td>
					  </tr>
					</table><br />
			</fieldset>
  			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>
