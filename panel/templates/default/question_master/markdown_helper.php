<?php
 /*****************Developed by :- Chirayu Bansal
	            Date         :- 10-sep-2011
				Module       :- Reports
				Purpose      :- Template for Browse all candidate details and provide functionality to generate report
	***********************************************************************************/
//echo ROOT."/uploadfiles/format_question_list.xlsx";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Markdown Help</title>
<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/markdown_helper.css' />

<script>
$(window).scroll(function(){
    $("#scroller").css("top",Math.max(0,275-$(this).scrollTop()));
});
</script>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body>

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<fieldset class="rounded">
			<legend>Markdown help</legend>

<div class="subheader"><h1>Markdown help</h1></div>
<div id="mainbar">
<div class="content-page">
    
<div class="col-section expandable">
    <h2 id="link-code" title="click to expand/collapse this section"><a href="#link-code" class="section-link"><br />Code and Preformatted Text<span class="expander-arrow-small-hide"></span></a></h2>
<div class="col-summary">
    <p>
        Indent four spaces to create an escaped <code>&lt;pre&gt;</code><code>&lt;code&gt;</code>
        block:</p>
    <pre><span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>printf("goodbye world!");  /* This code is
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>                              written in C */
</pre>
</div>
<div class="col-detail">
    <p>
        The text will be wrapped in tags, and displayed in a monospaced font. The first
        four spaces will be stripped off, but all other whitespace will be preserved.</p>
    <p>
        Markdown and HTML are ignored within a code block:</p>
    <pre><span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>&lt;blink&gt;
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>   You would hate this if it weren't
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>   wrapped in a code block.
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>&lt;/blink&gt;
</pre>
    <h3 id="link-code-spans"><a href="#link-code-spans" class="section-link"><br >Code Spans</a></h3>
    <p>
        Use backticks to create an inline <code>&lt;code&gt;</code> span:</p>
    <pre>The <span class="hi">`</span>$<span class="hi">`</span> character is just a shortcut for <span class="hi">`</span>window.jQuery<span class="hi">`</span>.
</pre>
    <p>
        (The backtick key is in the upper left corner of most keyboards.)</p>
    <p>
        Like code blocks, code spans will be displayed in a monospaced font. Markdown and
        HTML will not work within them. Note that, <i>unlike</i> code blocks, code spans
        require you to manually escape any HTML within!</p>
    <p>If your code itself contains backticks, you may have to use multiple backticks as
        delimiters:</p>
    <pre>The name <span class="hi">``</span>Tuple`2<span class="hi">``</span> is a valid .NET type name.
</pre>
</div>
<div class="col-detail share-more help-menu"><a href="#" class="show-less">collapse</a>&nbsp;<span class="lsep">|</span>&nbsp;<a href="#" class="show-all" title="expand all FAQ sections">expand all</a>&nbsp;<span class="lsep">|</span>&nbsp;<a title="permalink to this section" href="#code">link</a></div><a href="#" class="show-more">show more</a></div>
    
<div class="col-section">
    <h2 id="link-linebreaks"><a href="#link-linebreaks" class="section-link"><br />Linebreaks</a></h2>
    <p>
        End a line with two spaces to add a <code>&lt;br/&gt;</code> linebreak:</p>
    <pre>How do I love thee?<span class="spaces">&nbsp;&nbsp;</span>  
Let me count the ways
</pre>
</div>
    
<div class="col-section">
    <h2 id="link-italics-bold"><a href="#link-italics-bold" class="section-link"><br />Italics and Bold</a></h2>
    <pre><span class="hi">*</span>This is italicized<span class="hi">*</span>, and so is <span class="hi">_</span>this<span class="hi">_</span>.
<span class="hi">**</span>This is bold<span class="hi">**</span>, and so is <span class="hi">__</span>this<span class="hi">__</span>.
Use <span class="hi">***</span>italics and bold together<span class="hi">***</span> if you <span class="hi">___</span>have to<span class="hi">___</span>.
</pre>
</div>

<div class="col-section expandable">
    <h2 id="link-links" title="click to expand/collapse this section"><a href="#link-links" class="section-link"><br />Links<span class="expander-arrow-small-hide"></span></a></h2>
<div class="col-detail">
    <h3 id="link-basic-links"><a href="#link-basic-links" class="section-link">Basic Links</a></h3>
</div>
<div class="col-summary">
    <p>
        There are three ways to write links. Each is easier to read than the last:</p>
    <pre>Here's an inline link to <span class="hi">[Google](http://www.google.com/)</span>.
Here's a reference-style link to <span class="hi">[Google][1]</span>.
Here's a very readable link to <span class="hi">[Yahoo!][yahoo]</span>.

  <span class="hi">[1]:</span> http://www.google.com/
  <span class="hi">[yahoo]:</span> http://www.yahoo.com/
</pre>
</div>
<div class="col-detail">
    <p>
        The link definitions can appear anywhere in the document -- before or after the
        place where you use them. The link definition names <code>[1]</code> and <code>[yahoo]</code>
        can be any unique string, and are case-insensitive; <code>[yahoo]</code> is the
        same as <code>[YAHOO]</code>.</p>

    <h3 id="link-advanced-links"><a href="#link-advanced-links" class="section-link">Advanced Links</a></h3>
    <p>
        Links can have a title attribute, which will show up on hover. Title attributes
        can also be added; they are helpful if the link itself is not descriptive enough
        to tell users where they're going.</p>
    <pre>Here's a <span class="hi">[poorly-named link](http://www.google.com/ "Google")</span>.
Never write "<span class="hi">[click here][^2]</span>".
Visit <span class="hi">[us][web]</span>.

  <span class="hi">[^2]:</span> http://www.w3.org/QA/Tips/noClickHere
        (Advice against the phrase "click here")
  <span class="hi">[web]:</span> http://google.com/ "Google.com"
</pre>
    <p>
        You can also use standard HTML hyperlink syntax.</p>
    <pre>&lt;a href="http://example.com" title="example"&gt;example&lt;/a&gt;
</pre>
    </div>
<div class="col-detail share-more help-menu"><a href="#" class="show-less">collapse</a>&nbsp;<span class="lsep">|</span>&nbsp;<a href="#" class="show-all" title="expand all FAQ sections">expand all</a>&nbsp;<span class="lsep">|</span>&nbsp;<a title="permalink to this section" href="#link-links">link</a></div><a href="#" class="show-more">show more</a></div>
    
<div class="col-section">
    <h2 id="link-bare-urls"><a href="#link-bare-urls" class="section-link"><br />Bare URLs</a></h2>
<div class="col-summary">
    <p>
        We have modified our Markdown parser to support "naked" URLs (in most but not <i>all</i>
        cases -- beware of unusual characters in your URLs); they will be converted to links
        automatically:</p>
    <pre>I often visit http://example.com.
</pre>
    <p>
        Force URLs by enclosing them in angle brackets:</p>
    <pre>Have you seen <span class="hi">&lt;</span>http://example.com<span class="hi">&gt;</span>?
</pre>
    <p>
        URLs can be relative or full.</p>
</div>
</div>

<div class="col-section expandable">
    <h2 id="link-headers" title="click to expand/collapse this section"><a href="#link-headers" class="section-link">
        <br />Headers<span class="expander-arrow-small-hide"></span></a></h2>
<div class="col-summary">
    <p>
        Underline text to make the two <code>&lt;h1&gt;</code> <code>&lt;h2&gt;</code> top-level
        headers :</p>
    <pre>Header 1
<span class="hi">========</span>

Header 2
<span class="hi">--------</span>
</pre>
</div>
<div class="col-detail">
    <p>
        The number of = or - signs doesn't matter; one will work. But using enough to underline
        the text makes your titles look better in plain text.</p>
    <p>
        Use hash marks for several levels of headers:</p>
    <pre><span class="hi">#</span> Header 1 <span class="hi">#</span>
<span class="hi">##</span> Header 2 <span class="hi">##</span>
<span class="hi">###</span> Header 3 <span class="hi">###</span>
</pre>
    <p>
        The closing # characters are optional.</p>
</div>
<div class="col-detail share-more help-menu"><a href="#" class="show-less">collapse</a>&nbsp;<span class="lsep">|</span>&nbsp;<a href="#" class="show-all" title="expand all FAQ sections">expand all</a>&nbsp;<span class="lsep">|</span>&nbsp;<a title="permalink to this section" href="#headers">link</a></div><a href="#" class="show-more">show more</a></div>

    <div class="col-section expandable">
    <h2 id="link-horizontal-rules" title="click to expand/collapse this section"><a href="#link-horizontal-rules" class="section-link">
        <br />Horizontal Rules<span class="expander-arrow-small-hide"></span></a></h2>
<div class="col-summary">
    <p>
        Insert a horizontal rule <code>&lt;hr/&gt;</code> by putting three or more hyphens,
        asterisks, or underscores on a line by themselves:</p>
</div>
<div class="col-summary only">
    <pre><span class="hi">---</span>
</pre>
</div>
<div class="col-detail">
    <pre>Rule #1

<span class="hi">---</span>

Rule #2

<span class="hi">*******</span>

Rule #3

<span class="hi">___</span>

</pre>
    <p>
        Using spaces between the characters also works:</p>
    <pre>Rule #4    

<span class="hi">- - - -</span>

</pre>
</div>
<div class="col-detail share-more help-menu"><a href="#" class="show-less">collapse</a>&nbsp;<span class="lsep">|</span>&nbsp;<a href="#" class="show-all" title="expand all FAQ sections">expand all</a>&nbsp;<span class="lsep">|</span>&nbsp;<a title="permalink to this section" href="#horizontal-rules">link</a></div><a href="#" class="show-more">show more</a></div>

<div class="col-section expandable">
    <h2 id="link-simple-lists" title="click to expand/collapse this section"><a href="#link-simple-lists" class="section-link">
        <br />Simple lists<span class="expander-arrow-small-hide"></span></a></h2>
    <p class="col-detail">
        A bulleted <code>&lt;ul&gt;</code> list:</p>
    <pre class="col-summary"><span class="hi">-</span><span class="spaces">&nbsp;</span>Use a minus sign for a bullet
<span class="hi">+</span><span class="spaces">&nbsp;</span>Or plus sign
<span class="hi">*</span><span class="spaces">&nbsp;</span>Or an asterisk
</pre>
    <p class="col-detail">
        A numbered <code>&lt;ol&gt;</code> list:</p>
    <pre class="col-summary"><span class="hi">1.</span><span class="spaces">&nbsp;</span>Numbered lists are easy
<span class="hi">2.</span><span class="spaces">&nbsp;</span>Markdown keeps track of the numbers for you
<span class="hi">7.</span><span class="spaces">&nbsp;</span>So this will be item 3.
</pre>
<div class="col-detail">
    <p>
        A double-spaced list:</p>
    <pre><span class="hi">-</span><span class="spaces">&nbsp;</span>This list gets wrapped in &lt;p&gt; tags
<span class="spaces">&nbsp;</span>
<span class="hi">-</span><span class="spaces">&nbsp;</span>So there will be extra space between items
</pre>
</div>
<div class="col-detail share-more help-menu"><a href="#" class="show-less">collapse</a>&nbsp;<span class="lsep">|</span>&nbsp;<a href="#" class="show-all" title="expand all FAQ sections">expand all</a>&nbsp;<span class="lsep">|</span>&nbsp;<a title="permalink to this section" href="#simple-lists">link</a></div><a href="#" class="show-more">show more</a></div>

<div class="col-section expandable">
    <h2 data-title="Advanced lists" id="link-advanced-lists" title="click to expand/collapse this section"><a href="#link-advanced-lists" class="section-link">
        <br />Advanced lists: Nesting<span class="expander-arrow-small-hide"></span></a></h2>
    <p class="col-detail">
        To put other Markdown blocks in a list; just indent four spaces for each nesting
        level:</p>
    <pre class="col-summary only"><span class="hi">1.</span><span class="spaces">&nbsp;</span>Lists in a list item:
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hi">-</span><span class="spaces">&nbsp;</span>Indented four spaces.
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hi">*</span><span class="spaces">&nbsp;</span>indented eight spaces.
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hi">-</span><span class="spaces">&nbsp;</span>Four spaces again.
</pre>
<div class="col-detail">
    <pre><span class="hi">1.</span><span class="spaces">&nbsp;</span>Lists in a list item:
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hi">-</span><span class="spaces">&nbsp;</span>Indented four spaces.
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hi">*</span><span class="spaces">&nbsp;</span>indented eight spaces.
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hi">-</span><span class="spaces">&nbsp;</span>Four spaces again.
<span class="hi">2.</span><span class="spaces">&nbsp;&nbsp;</span>Multiple paragraphs in a list items:
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>It's best to indent the paragraphs four spaces
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>You can get away with three, but it can get
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>confusing when you nest other things.
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>Stick to four.
<span class="spaces">&nbsp;</span>
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>We indented the first line an extra space to align
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>it with these paragraphs.  In real use, we might do
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>that to the entire list so that all items line up.
<span class="spaces">&nbsp;</span>
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span>This paragraph is still part of the list item, but it looks messy to humans.  So it's a good idea to wrap your nested paragraphs manually, as we did with the first two.
<span class="spaces">&nbsp;</span>
<span class="hi">3.</span><span class="spaces">&nbsp;</span>Blockquotes in a list item:
<span class="spaces"> </span>
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hi">&gt;</span> Skip a line and
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hi">&gt;</span> indent the &gt;'s four spaces.
<span class="spaces">&nbsp;</span>
<span class="hi">4.</span><span class="spaces">&nbsp;</span>Preformatted text in a list item:
<span class="spaces">&nbsp;</span>
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Skip a line and indent eight spaces.
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>That's four spaces for the list
<span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>and four to trigger the code block.
</pre>
</div>
<div class="col-detail share-more help-menu"><a href="#" class="show-less">collapse</a>&nbsp;<span class="lsep">|</span>&nbsp;<a href="#" class="show-all" title="expand all FAQ sections">expand all</a>&nbsp;<span class="lsep">|</span>&nbsp;<a title="permalink to this section" href="#advanced-lists">link</a></div><a href="#" class="show-more">show more</a></div>

<div class="col-section">
    <h2 id="link-simple-blockquotes"><a href="#link-simple-blockquotes" class="section-link">
        <br />Simple blockquotes</a></h2>
    <p>
        Add a <code>&gt;</code> to the beginning of any line to create a <code>&lt;blockquote&gt;</code>.</p>
    <pre><span class="hi">&gt;</span> The syntax is based on the way email programs
<span class="hi">&gt;</span> usually do quotations. You don't need to hard-wrap
<span class="hi">&gt;</span> the paragraphs in your blockquotes, but it looks much nicer if you do.  Depends how lazy you feel.
</pre>
</div>

<div class="col-section expandable">
    <h2 id="link-advanced-blockquotes" data-title="Advanced blockquotes" title="click to expand/collapse this section"><a href="#link-advanced-blockquotes" class="section-link">
        <br />Advanced blockquotes: Nesting<span class="expander-arrow-small-hide"></span></a></h2>
    <p class="col-summary only">
        To put other Markdown blocks in a <code>&lt;blockquote&gt;</code>, just add a <code>
            &gt;</code> followed by a space.</p>
<div class="col-detail">
    <p>
        To put other Markdown blocks in a <code>&lt;blockquote&gt;</code>, just add a <code>
            &gt;</code> followed by a space:</p>
    <pre><span class="hi">&gt;</span> The &gt; on the blank lines is optional.
<span class="hi">&gt;</span> Include it or don't; Markdown doesn't care.
<span class="hi">&gt;</span><span class="spaces">&nbsp;</span>
<span class="hi">&gt;</span> But your plain text looks better to
<span class="hi">&gt;</span> humans if you include the extra `&gt;`
<span class="hi">&gt;</span> between paragraphs.
</pre>
    <p>
        Blockquotes within a blockquote:</p>
    <pre><span class="hi">&gt;</span> A standard blockquote is indented
<span class="hi">&gt;</span> <span class="hi">&gt;</span> A nested blockquote is indented more
<span class="hi">&gt;</span> <span class="hi">&gt;</span> <span class="hi">&gt;</span> <span class="hi">&gt;</span> You can nest to any depth.
</pre>
    <p>
        Lists in a blockquote:</p>
    <pre><span class="hi">&gt;</span><span class="spaces">&nbsp;</span><span class="hi">-</span> A list in a blockquote
<span class="hi">&gt;</span><span class="spaces">&nbsp;</span><span class="hi">-</span> With a &gt; and space in front of it
<span class="hi">&gt;</span><span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hi">*</span> A sublist
</pre>
    <p>
        Preformatted text in a blockquote:</p>
    <pre><span class="hi">&gt;</span><span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Indent five spaces total.  The first
<span class="hi">&gt;</span><span class="spaces">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>one is part of the blockquote designator.
</pre>
</div>
<div class="col-detail share-more help-menu"><a href="#" class="show-less">collapse</a>&nbsp;<span class="lsep">|</span>&nbsp;<a href="#" class="show-all" title="expand all FAQ sections">expand all</a>&nbsp;<span class="lsep">|</span>&nbsp;<a title="permalink to this section" href="#advanced-blockquotes">link</a></div><a href="#" class="show-more">show more</a></div>

<div class="col-section expandable">
    <h2 id="link-images" title="click to expand/collapse this section"><a href="#link-images" class="section-link">
        <br />Images<span class="expander-arrow-small-hide"></span></a></h2>
<div class="col-summary">
    <p>
        Images are exactly like links, but they have an exclamation point in front of them:</p>
    <pre>![Valid XHTML](http://w3.org/Icons/valid-xhtml10).
</pre>
</div>
<div class="col-detail">
    <p>
        The word in square brackets is the alt text, which gets displayed if the browser
        can't show the image. Be sure to include meaningful alt text for screen-reading
        software.</p>
    <p>
        Just like links, images work with reference syntax and titles:</p>
    <pre>This page is <span class="hi">![valid XHTML][checkmark]</span>.
 
<span class="hi">[checkmark]:</span> http://w3.org/Icons/valid-xhtml10
             "What are you smiling at?"
</pre>
    <p>
        Note: Markdown does not currently support the shortest reference syntax for images:</p>
    <pre>Here's a broken ![checkmark].
</pre>
    <p>
        But you can use a slightly more verbose version of implicit reference names:</p>
    <pre>This <span class="hi">![checkmark][]</span> works.
</pre>
    <p>
        The reference name is also used as the alt text.</p>
    <p>
        You can also use standard HTML image syntax, which allows you to scale the width
        and height of the image.</p>
    <pre>&lt;img src="http://example.com/sample.png" width="100" height="100"&gt;
</pre>
    <p>
        URLs can be relative or full.</p>
</div>
<div class="col-detail share-more help-menu"><a href="#" class="show-less">collapse</a>&nbsp;<span class="lsep">|</span>&nbsp;<a href="#" class="show-all" title="expand all FAQ sections">expand all</a>&nbsp;<span class="lsep">|</span>&nbsp;<a title="permalink to this section" href="#link-images">link</a></div><a href="#" class="show-more">show more</a></div>


</div>
</div>
<div id="sidebar">
    <div id="scroller-anchor"></div>
    <div id="scroller" class="rounded">
    	<div class="module newuser" id="toc">
    		<ul>
    			<li>
    				<a href="#link-code" title="">Code and Preformatted Text</a>
    			</li>
    			<li>
    				<a href="#link-linebreaks" title="">Linebreaks</a>
    			</li>
    			<li>
    				<a href="#link-italics-bold" title="">Italics and Bold</a>
    			</li>
    			<li>
    				<a href="#link-links" title="">Links</a>
    			</li>
    			<li>
    				<a href="#link-bare-urls" title="">Bare URLs</a>
    			</li>
    			<li>
    				<a href="#link-headers" title="">Headers</a>
    			</li>
    			<li>
    				<a href="#link-horizontal-rules" title="">Horizontal Rules</a>
    			</li>
    			<li>
    				<a href="#link-simple-lists" title="">Simple lists</a>
    			</li>
    			<li>
    				<a href="#link-advanced-lists" title="Advanced lists: Nesting">Advanced lists</a>
    			</li>
    			<li>
    				<a href="#link-simple-blockquotes" title="">Simple blockquotes</a>
    			</li>
    			<li>
    				<a href="#link-advanced-blockquotes" title="Advanced blockquotes: Nesting">Advanced blockquotes</a>
    			</li>
    			<li>
    				<a href="#link-images" title="">Images</a>
    			</li>
    		</ul>
    	</div>
    </div>
</div>			


			</fieldset>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>