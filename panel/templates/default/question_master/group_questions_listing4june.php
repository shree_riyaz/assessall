<?php
 /*****************Developed by :- Akshay Yadav
	                Date         :- 20-May-2014
					Module       :- Question master
					Purpose      :- Template for Browse all group question details and provide functionality to edit delete and see all question
	***********************************************************************************/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Group-Question Bank</title>

<script language="javascript" type="text/javascript">
function deleteQues(val,host)
{
	var x = confirm("Are you sure you want to delete this question?");
    if (x)
    { 
		load("<?php echo ROOTURL; ?>" + "/index.php?mod=question_master&do=delete&nameID="+val);
    }
    else
    { 
		return false;
    }
}
</script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.question_title_manage.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist">
			<legend>Question Bank</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['warning']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="left" style="padding:3px 3px 3px 3px;">';
				echo $_SESSION['warning'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['warning']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			<div class=" search-div">
				<fieldset class="rounded search-fieldset"><legend>Search</legend>
				<table border="0" cellspacing="1" cellpadding="3">
			  		<tr>
				 		<td>Question Title :</td>
						<td>
							<input type="text" value="<?php echo $frmdata['question_title_manage']; ?>" name="question_title_manage" id="question_title_manage"  class="rounded" style="width:250px" />
						</td>
					</tr>
			
					<tr>
						<td>Subject :</td>
						<td>
							<select class="rounded" name="subject_id_manage" id="subject_id_manage">
								<option value="">Select Subject</option>
							<?php 
							if(is_array($subject))
							{
								for($counter=0;$counter<count($subject);$counter++)
								{
									$selected='';
									if ($subject[$counter]->id == $frmdata['subject_id_manage'])
									{
										$selected='selected';
									}
									echo '<option value="'.$subject[$counter]->id.'"'.$selected.'>'.stripslashes($subject[$counter]->subject_name).'</option>';
								}
							}
								?>
							</select> 
						</td>
					</tr>
					<tr> 
					    <td colspan="6" align="center"> 
					    	<input type="submit" class="buttons rounded" name="Submit" value="Show" /> 
					    	<input type="submit" class="buttons" name="clear_search" id="clear_search" value="Clear Search" onclick=""/> 
					    </td> 
		 	 		</tr> 
				</table>
				</fieldset>
			</div><br />
		<?php
			if($questionlist)
			{ 
		?>
			<table border="0" cellspacing="1" cellpadding="4" width="100%" style="">
			<tr>
				<td><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+count($questionlist))." of ".$totalCount; ?></td>
				<td align="right"><?php echo getPageRecords();?> </td>
			</tr>
			</table>
		<?php
			} 
		?>
				<div id="celebs">
				<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
				<?php 
				if($questionlist && count(count($questionlist) > 0))
					{
				?>  <thead>
					  <tr class="tblheading">
						<td width="1%">#</td>
						<td width="60%"><div align="left">
						<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('question_title');">Questions
						<?php if($frmdata['orderby']=='question_title') 
							{
								print '<img src="'.ROOTURL.'/images/asc.gif">';
								$frmdata['orderby']='question_title';
							} 
							elseif($frmdata['orderby']=='question_title desc')
							{
								print '<img src="'.ROOTURL.'/images/desc.gif">';
							}
						?>
						</a></div></td>
						
						<td width="20%">
						<a style="cursor:pointer;color:#af5403;" onClick="OrderPage('subject_name');">Subject
						<?php if($frmdata['orderby']=='subject_name') 
							{
								print '<img src="'.ROOTURL.'/images/asc.gif">';
							} 
							elseif($frmdata['orderby']=='subject_name desc')
							{
								print '<img src="'.ROOTURL.'/images/desc.gif">';
							}
						?>
						</a></td>
						
						<td width="20%">Actions</td>
					  </tr>
					  </thead>
					  <tbody>
					  
					  <?php
						$srNo = $frmdata['from'];
						$count = count($questionlist);
						
						for ($counter = 0; $counter < $count; $counter++)
						{
							$title = (($questionlist[$counter]->question_title));	
							if ($questionlist[$counter]->subject_name != '')
								$subject = stripcslashes($questionlist[$counter]->subject_name);
							else
								$subject = '-';
							$id = $questionlist[$counter]->id;
							$srNo = $srNo + 1;
							if (($counter % 2) == 0)
							{
								$trClass = "tdbggrey";
							}
							else
							{
								$trClass = "tdbgwhite";
							}
						
						?>
						  <tr class='<?= $trClass; ?>'><td><?php echo $srNo; ?></td>
							<td>
								<div align='left'><a href="<?php echo CreateURL('index.php','mod=question_master&do=edit&id='.$id); ?>" class='alink'><?php echo $title; ?></a></div>
							</td>
							<td><?php echo $subject; ?></td>
							<td>
								<a href="<?php echo CreateURL('index.php','mod=question_master&do=manage&id='.$questionlist[$counter]->id); ?>" title='Edit'><img src="<?php echo IMAGEURL; ?>/b_edit.png" /></a>&nbsp;&nbsp;
								<img src="<?php echo IMAGEURL; ?>/b_drop.png" onclick="location.href='<?php print CreateURL('index.php','mod=question_master&do=delete&id='.$questionlist[$counter]->id);?>'" title='Delete' style="cursor: pointer;" />
							</td>
										
						</tr>
					<?php 
					   } 
					  }
					  else
					  {
						echo "<tr><td colspan='9' align='center'>(0) Record found.</td></tr>";
					  }
					  ?>
					  </tbody>
					</table>
				</div>
				<table border="0" cellspacing="5" cellpadding="1" width="100%" id="tbl_download_reports">
					  <tr>
						<td width="10%">&nbsp;</td>
						<td width="54%">&nbsp;</td>
						<td width="36%" align="right">&nbsp;
						</td>
					  </tr>
					  <tr>
					  <?php
					 if($questionlist[0]->id!='')
					{
					?>
						<td colspan="2"><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?></td>
			   			<?php } ?>
						<td align="right">
							<?php PaginationDisplay($totalCount);	?>
							</td>
					  </tr>
				</table>
				 <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" />
				<input name="orderby" id="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" />
				<input name="order" id="order" type="hidden" value="<?php print $frmdata['order']?>" />	
  			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->
</body>
</html>
