<?php
//print_r($_SESSION);exit;
 /*****************Developed by :- Chirayu Bansal
	            Date         :- 26-july-2011
				Module       :- preferences
				Purpose      :- Template for Browse all data
	***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Preferences</title>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 

$font_size = $PreferencesOBJ->getOptionValue('font-size');
$color = $PreferencesOBJ->getOptionValue('color');

?>
<body>

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<!--<form action="" method="post" name="frmlist" id="frmlist">-->
				<legend>Configuration Settings</legend>
				<table width="600" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences" style="margin: 0 auto;">
					<tr>
					    <td colspan="2" valign="top">
							<?php
							if (isset($_SESSION['ActionAccess']))
							{
								echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
								echo $_SESSION['ActionAccess'];
								echo '</td></tr></tbody></table>';
								unset($_SESSION['ActionAccess']);
							}
								if(isset($_SESSION['success']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="50%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
									echo $_SESSION['success'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['success']);
								} 
							?>
						</td>
				    </tr>
				<?php
					//for($counter=0; $counter<count($conflist); $counter++)
					{
				?>
					<!--<tr>
						<td align="right" width="50%">Minimum Passing Marks (in percentage):</td>
						<td align="left" width="50%"><strong><?php if($conflist[0]->variable_value) echo $conflist[0]->variable_value; else echo 'NA'; ?></strong></td>
					</tr>-->
				<?php
					} 
				?>
				<tr>
					<td colspan="2">Header Configuration:</td>
				</tr>				
				<tr>
					<td align="right">Left Logo:</td>
					<td>
						<?php if($value = $PreferencesOBJ->getOptionValue('left-logo')) { ?>
						<img src="<?php echo ROOTURL.'/uploadfiles/'.$value; ?>" /></td>
						<?php } else echo 'NA'; ?>
				</tr>
				<tr>
					<td align="right">Middle Text:</td>
					<td align="left">
						<label style="font-weight:bold;font-size:<?php echo $font_size; ?>px;color:#<?php echo $color; ?>;"><?php echo ($value = $PreferencesOBJ->getOptionValue('middle-text')) ? $value : 'Recruitment - Examination'; ?></label>
					</td>
				</tr>	
				<tr>
					<td align="right">Right Logo:</td>
					<td>
						<?php if($value = $PreferencesOBJ->getOptionValue('right-logo')) { ?>
						<img src="<?php echo ROOTURL.'/uploadfiles/'.$value; ?>" />
						<?php } else echo 'NA'; ?>
					</td>
				</tr>				
				</table>
				<br>
				<table width="920" border="0" cellpadding="4" cellspacing="0" align="center">
					<tr>
						<td align="center"><input type="button" class="buttons" name="edit" id="edit" value="Edit Settings" onclick="location.href='<?php echo CreateURL('index.php',"mod=preferences&do=edit"); ?>'"/></td>
					</tr>
				</table>
  			<!--</form>-->
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>
