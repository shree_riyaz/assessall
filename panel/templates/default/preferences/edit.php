<?php
 /*****************Developed by :- Chirayu Bansal
	            Date         :- 26-july-2011
				Module       :- preferences
				Purpose      :- Template for edit all data
	***********************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Preferences</title>
<script>
	function showTextPreview()
	{
		
		text = document.getElementById('middle-text').value;
		font_size = document.getElementById('font-size').value;	
		text_color = document.getElementById('color').value;		
		previewDiv = document.getElementById('preview');		
		previewDiv.innerHTML = text;
		previewDiv.style.fontSize = parseInt(font_size)+'px';
		previewDiv.style.color = text_color;
	}
</script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
		
			<form action="" method="post" name="editfrm" id="editfrm" enctype="multipart/form-data">
				<legend>Edit Configuration Settings</legend>
				<table width="600" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences" style="margin: 0 auto;">
					<tr>
					    <td colspan="2" valign="top">
							<?php
								if(isset($_SESSION['error']))
								{
									echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
									echo $_SESSION['error'];
									echo '</td></tr></tbody></table>';
									unset($_SESSION['error']);
								}
							?>
						</td>
				    </tr>
					
				<?php
					//for($counter=0; $counter<count($conflist); $counter++)
					{
				?>
					<!--<tr>
						<td align="right" width="50%">Minimum Passing Marks (in percentage):<span class="red">*</span></td>
						<td align="left" width="50%"><input type="text" class="rounded textfield" name="MIN_PASS_MARKS" id="MIN_PASS_MARKS" value="<?php if($frmdata) echo $frmdata['MIN_PASS_MARKS']; else echo $conflist[0]->variable_value; ?>" /></td>
					</tr>-->
				<?php
					} 
				?>
				<tr>
					<td colspan="2">Header Configuration:</td>
				</tr>
				<tr>
					<td align="right" valign="top">Left Logo:</td>
					<td align="left">
						<input type="file" name="left-logo" id="left-logo" size="30"/>
					</td>
				</tr>
				<?php if($value = $PreferencesOBJ->getOptionValue('left-logo')) {  ?>
				<tr>
					<td align="right">Current:</td>
					<td>
						<img src="<?php echo ROOTURL.'/uploadfiles/'.$value; ?>" /><br>
						<input type="checkbox" name="remove-left" id="remove-left" />
							<label for="remove-left">Remove Left Logo</label>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td align="right">Middle Text:</td>
					<td align="left"><input type="text" name="middle-text" id="middle-text" onkeyup="showTextPreview()" size="30" value="<?php if($value = $PreferencesOBJ->getOptionValue('middle-text')) { echo $value;} else {echo '42 MED REGT (DBN)'; }?>"/><span class="red">*</span>
						&nbsp; Size: 
						<select name="font-size" id="font-size" onchange="showTextPreview()">
						<?php
						$value = $PreferencesOBJ->getOptionValue('font-size');
						if (!$value)
						{
							$value=56;
						}
						for($size=5; $size <=72; $size++)
						{
							$selected = '';
							if ($value == $size)
							{
								$selected = ' selected';
							}
							
							echo "<option value='".$size."' ".$selected.">".$size."</option>";
						}						
						?>
						</select>						
						&nbsp; Color: <input type="text" name="color" class="color" readonly='readonly' id='color' size="8" onchange="showTextPreview()" value="<?php if($color = $PreferencesOBJ->getOptionValue('color')) { echo $color;} else {echo $color = '888888'; }?>"/></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<div style="width:890px;color:#<?php echo $color ?>;font-size:<?php if($value = $PreferencesOBJ->getOptionValue('font-size')) { echo $value.'px;';} else {echo '56px;'; }?> font-weight:bold" id="preview">
						<?php if($value = $PreferencesOBJ->getOptionValue('middle-text')) { echo $value;} else {echo 'Recruitment - Examination'; }?>
						</div>
					</td>
				</tr>	
				<tr>
					<td align="right">Right Logo:</td>
					<td align="left">
						<input type="file" name="right-logo" id="right-logo" size="30"/>
					</td>
				</tr>
				<?php if($value = $PreferencesOBJ->getOptionValue('right-logo')) { ?>
				<tr>
					<td align="right">Current:</td>
					<td>
						<img src="<?php  echo ROOTURL.'/uploadfiles/'.$value; ?>" /><br>
						<input type="checkbox" name="remove-right" id="remove-right" />
							<label for="remove-right">Remove Right Logo</label>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="2" align="left">
					<div align="left" id="mandatory">
						<strong>Note:</strong> Please upload image in .JPG, .GIF, .PNG and .JPEG format.
						<br> Image size should not be more than 2MB.
					</div>
					</td>
				</tr>	
				</table>
				<br>
				<table width="100%" border="0" cellpadding="4" cellspacing="0" align="center">
					<tr>
						<td align="center">
						<input type="submit" class="buttons" name="update" id="update" value="Update Settings" onclick="return checkforPercentage(document.getElementById('MIN_PASS_MARKS'));"/> 
						<input type="button" class="buttons" name="cancel" id="cancel" value="Cancel" onclick="location.href='<?php echo CreateURL('index.php',"mod=preferences&do=view"); ?>'"/>
						</td>
					</tr>
				</table>
  			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	  
	</table>
	
</div><!--Outer wrapper closed-->

</body>
</html>