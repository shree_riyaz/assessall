<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php if(!in_array($CFG->template, array('test_master/addsubjectquestion.php', 'homework/addsubjectquestion.php', 'paper_master/addsubjectquestion.php'))) { ?>
<head>

<script type="text/javascript" src="<?php echo JS; ?>/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo JS; ?>/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="<?php echo JS; ?>/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript" src="<?php echo JS; ?>/functions.js"></script>
<script type="text/javascript" src="<?php echo JS; ?>/thickbox.js"></script>
<script type="text/javascript" src="<?php echo JS; ?>/jscolor/jscolor.js"></script>
<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/exam.css' />
<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/basic.css' />
<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/dropline_one.css' />
<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/thickbox.css' />
<link rel='stylesheet' type='text/css' href='<?php echo STYLE; ?>/jquery-ui-1.10.1.custom.min.css' />

<script language="javascript" type="text/javascript">
$(document).ready(function()
{
	var date = $( ".date" ).length;
	if(date > 0)
	{
		$( ".date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "both",
			buttonImage: "<?php echo IMAGEURL?>/cal.gif",
			buttonImageOnly: true,
			yearRange: "-40:+20",
			dateFormat: "dd-mm-yy",
			showAnim: 'slideDown'
		});
	}

	var date = $( ".datetime" ).length;
	if(date > 0)
	{
		$( ".datetime" ).datetimepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "both",
			buttonImage: "<?php echo IMAGEURL?>/cal.gif",
			buttonImageOnly: true,
			yearRange: "-40:+20",
			dateFormat: "dd/mm/yy",
			showAnim: 'slideDown',
			ampm: true,
			timeFormat: 'hh:mm:ss TT'
		});
	}

	var combo = $( ".combobox" ).length;
	if(combo > 0)
	{
		$( ".combobox" ).combobox();
	}
	
});
</script>
</head>

<?php } ?>

<?php
if(isset($CFG->template))
{
	include_once(CURRENTTEMP."/".$CFG->template);
}
else
{
	include_once(CURRENTTEMP."/"."home.php");
}

if(!in_array($CFG->template, array('test_master/addsubjectquestion.php', 'homework/addsubjectquestion.php', 'paper_master/addsubjectquestion.php')))
{
	include_once(CURRENTTEMP."/"."footer.php");
}

?>