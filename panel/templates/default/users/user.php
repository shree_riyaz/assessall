<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Users</title>
<script language="javascript1.2">
function setUserID(userID,disable)
{
	document.frmlist.actUserID.value=userID;
	if(disable)
	{
		return confirm('Do you really want to disable this user?');
	}
	return true;
}
</script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.keywords.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<legend>Users</legend>
			<?php 
			if (isset($_SESSION['ActionAccess']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['ActionAccess'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['ActionAccess']);
			}
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>

			<div class=" search-div">
			<fieldset class="rounded search-fieldset"><legend>Search</legend>
				<table border="0" cellspacing="1" cellpadding="3" width="100%">
					<tr>
			    		<td>Name / User Name:</span></td>
						<td>
							<input name="keywords" type="text" class="rounded textfield" id="keywords" onblur="IsAlphaNum(this, 'keyword');" maxlength="20" value="<?php echo $frmdata['keywords']; ?>" />
			    		</td>
			    	</tr>
			    	<tr>
			    		<td align="right">Role:</td>
				      	<td>
				      		<select name="roleName" style="width:140px" class="rounded" id="roleName">
				      			<option value="">Select Role Name</option>
						      	<?php
									$roleList=$DB->SelectRecords('role',"roleName!='Admin'",'*','order by roleName');
									if($roleList)
									{
										$count=count($roleList);
										for($counter = 0; $counter < $count; $counter++)
										{
											if($roleList[$counter]->roleName == $frmdata['roleName'])
											{
												echo '<option value="'.$roleList[$counter]->roleName.'" selected >'.$roleList[$counter]->roleName.'</option>';
											}
											else
											{
												echo '<option value="'.$roleList[$counter]->roleName.'" >'.$roleList[$counter]->roleName.'</option>';
											}	
										}	
									}
								?>
							</select>
				      	</td>
					</tr>
					<tr>
			    		<td colspan="6" align="center">
							 <input class="buttons rounded" type="submit" name="searchuser" id="searchuser" value="Search" onclick="return crmUserSearch();" title="Click to search">
							 <input class="buttons rounded" type="submit" name="clearsearch" value="Clear Search" title="Click to clear"> </td>
			    		</td>
		  			</tr>
				</table>
			</fieldset>
			</div>
	
	  <?php 
		if($userlist)
		{
		   
		 $srNo=$frmdata['from'];
		 $count=count($userlist);	 
	   
		?>
		<br>
	  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td width="454" align="left" valign="top"><?php print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; ?></td>
				<td width="205" align="right" valign="top"><?php echo getPageRecords();?></td>
			</tr>
		</table>	
		
<div id="celebs">
	<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
		<thead>
			<tr class="tblheading">
				<td width="33" align="center">S.No</td>
				
				<td width="118" align="center" >
					<a style="color:#AF5403;cursor:pointer" onClick="OrderPage('us.name');">
						Name<?php if($frmdata['orderby']=='us.name') {print '<img src="'.ROOTURL.'/images/asc.gif">';} elseif($frmdata['orderby']=='us.name desc'){print '<img src="'.ROOTURL.'/images/desc.gif">';}?>
					</a>
				</td>
		  
		  		<td width="67" align="center" >
		  			<a style="color:#AF5403;cursor:pointer" onClick="OrderPage('us.user_name');">
		  				User name<?php if($frmdata['orderby']=='us.user_name') {print '<img src="'.ROOTURL.'/images/asc.gif">';} elseif($frmdata['orderby']=='us.user_name desc'){print '<img src="'.ROOTURL.'/images/desc.gif">';}?>
					</a>
				</td>	
				
				<td width="212" align="center">Email</td> 
				<td align="center" width="100">Role</td>    
				<td width="100" align="center" >Action</td>
			</tr>
		</thead>
		
		<?php 	
			for($counter=0;$counter<$count;$counter++)
			{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}
				
			
		?>
		<tr   class='<?= $trClass; ?>'>
		   <td align="center" ><?php  print $srNo ?></td>
		   
		  <td align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<?php echo $userlist[$counter]->name ?></td>
		  <td width="67" align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<strong><?php if($userlist[$counter]->user_name=='') print "n/a"; else print $userlist[$counter]->user_name; ?></strong></td>
		  <td width="212" align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<?php if($userlist[$counter]->email=='') print "n/a"; else  print $userlist[$counter]->email; ?></td>
		 <td align="center" class="fontstyle" ><?php echo ($userlist[$counter]->roleName != '') ?  $userlist[$counter]->roleName : 'n/a'; ?></td>
		
		  <td align="center" <?php echo $style ?>>
		  	<a  class="fontstyle" href='<?php print CreateURL('index.php','mod=users&do=edit&nameID='.$userlist[$counter]->id);?>' title="Edit User" ><img src="<?php echo IMAGEURL."/edit.gif" ?>" border=0 /></a>
		  	<a  class="fontstyle" href='<?php print CreateURL('index.php','mod=users&do=Change&nameID='.$userlist[$counter]->id);?>' title="Change Password"><img src="<?php echo IMAGEURL."/changepass.png" ?>" border=0 /></a> 
			  <?php 
				if($userlist[$counter]->disabled=='N')
			  	{
					echo '<input name="disableuser" type="submit" id="disableuser" title="Disable this user"  onClick="return setUserID('.$userlist[$counter]->id.',true)" class="btnClass_disable"   value="">';
			  	}
			  	else
			  	{
					echo '<input name="enableuser" type="submit" id="enableuser" title="Enable this user"  onClick="return setUserID('.$userlist[$counter]->id.')"  class="btnClass_enable"   value="">';
			  	}
			  
			  ?>
		 
		   </td>
		</tr>
		
		<?php
			} 
		?>
	</table>
	<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">		
	
		 
	
		 <tr>
		  <td align="center" colspan="12"><?php
				PaginationDisplay($totalCount);
			?>
		  </td>
		</tr>
	 </table>
	
	  <?php
	  }
	  else
	  { 
		echo "<center><br />No Record found.</center>";
	   }
	  
	  ?>
	  <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" />
	</form>	
		</div><!--Div Contents closed-->
				</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>

<style>
#disableuser
{
	background: url('<?php echo IMAGEURL.'/disable.png'; ?>');
	width: 16px;
	height: 16px;
	border: none;
}
#enableuser
{
	background: url('<?php echo IMAGEURL.'/enable.png'; ?>');
	width: 16px;
	height: 16px;
	border: none;
}
</style>