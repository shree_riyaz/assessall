<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Edit User</title>
<script language="javascript1.2">
function setUserID(userID,disable)
{
	document.frmlist.actUserID.value=userID;
	if(disable)
	{
		return confirm('Do you really want to disable this user?');
	}
	return true;
}
</script>
</head>
<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body onload="document.frmlist.name.focus();">
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
		<tr>
			<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
		</tr>
	  	<tr>
			<td>
				<div id="content">
					<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
				<fieldset class="rounded" style="width: 60%;">
					<legend>Edit User</legend>
					<?php 
					// Show particular Messages
					if(isset($_SESSION['error']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
						echo $_SESSION['error'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['error']);
					}
					if(isset($_SESSION['success']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
						echo $_SESSION['success'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['success']);
					}
					?>
					
<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences" style="padding: 20px;">
<tr>
	<td colspan="4">
		<div id="mandatory" align="left"><?php echo MANDATORYNOTE; ?></div>
	</td>
</tr>
<tr><td></td></tr>
<tr>
	<td align="">User Name:</td>	
	<td>
		<?php print $contsInfo->user_name;?>
	</td>
</tr>
<tr>
	<td width="" align="">Name:<span class="red">*</span></td>
	<td>
		<input name="name" type="text" id="name" class="rounded textfield" 
			value="<?php print $contsInfo->name;?>" size="40" maxlength="50"
			 onchange="isChar(this.value,this.id, 'name');"  />
	</td>
</tr>
<tr>
	<td>Email:</td>	
	<td>
		<input onchange="CheckEmailId(this);" name="email" type="text" class="rounded textfield"
			onBlur="return CheckEmailId(this)"value="<?php print $contsInfo->email;?>"
			id="email" size="40" maxlength="60" />
	</td>
</tr>			
<tr>				
	<td>Role:<span class="red">*</span></td>
	<td>
		<div id="entitydiv">
			<?php $roleCount=count($role); ?>
			
			<select id="roleID" name="role_id" class="rounded textfield">
				<option value="">Select Role</option>
				<?php
					$rolecnt=count($role);
					for ($counter=0; $counter < $rolecnt; $counter++)
					{
						$selected='';
						if ($role[$counter]->roleID !='')
						{
							if ($role[$counter]->roleID == $contsInfo->role_id)
							{
								$selected='selected';
							}
							echo '<option value="'.$role[$counter]->roleID.'" '.$selected.'>'.$role[$counter]->roleName.'</option>';
						}
					}
				?>
			</select>
		</div>
	</td>
</tr>
<tr>
	<td colspan="6">
		<div align="center" style="margin-top:10px">
			<input type="submit" name="edituser" id="edituser" value="Update User" class="buttons rounded"  title="Click to update">
			&nbsp;&nbsp;
			<input type="button" name="fileback" id="fileback" class="buttons rounded"  value="Back" onClick="location.href='<?php print CreateURL('index.php','mod=users');?>'">
		</div>
	</td>
</tr>
</table>
				</fieldset>
			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

				</div><!--Content div closed-->
			</td>
		</tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>