<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Change Password</title>

<script>
function hide()
{ 
	document.getElementById('showpass').style.display='none';
}
function IsWhiteSpaces(text)
{
	if(text.value.indexOf(' ') >= 0)
	{
		alert("Password can not contain blank spaces.");
		text.value='';
		text.focus();
	}
}
function togglePass(chk)
{
	if(chk.checked)
		$('#showpass').show();
	else
		$('#showpass').hide();
}
</script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body >

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
		<tr>
			<td><?php include_once(CURRENTTEMP."/"."header.php"); ?></td>
		</tr>
		<tr>
			<td>
				<div id="content">
					<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
				<fieldset class="rounded" style="width: 50%;">
					<legend>Change Admin Password</legend>
					<?php 
					// Show particular Messages
					if(isset($_SESSION['error']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
						echo $_SESSION['error'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['error']);
					}
					if(isset($_SESSION['success']))
					{
						echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
						echo $_SESSION['success'];
						echo '</td></tr></tbody></table>';
						unset($_SESSION['success']);
					}
					?>

<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tbl_preferences" style="padding: 20px;">
<tr>
	<td colspan="4">
		<div id="mandatory" align="left"><?php echo MANDATORYNOTE; ?></div>
	</td>
</tr>
<tr><td></td></tr>
<tr>
	<td width="" align="">Name:</td>
	<td><?php print $contsInfo->name;?></td>
</tr>
<tr>
	<td width="" align="">User Name:<span class="red">*</span></td>
	<td><?php print $contsInfo->user_name;?></td>
</tr>
<tr>
	<td></td>
	<td>
		<input type="checkbox" name="autopass" id="autopass" 
			onClick="xajax_testcaptchaWork(document.frmlist.autopass.checked);togglePass(this);" 
			value="auto pass" style="border:none">
			
		<label for="autopass" style=" font-family:Arial, Helvetica, sans-serif;font-size:12px">Auto generate password</label>
	</td>
	<td width="60px">
		<span style="background-color:#ddd;padding: 2px 5px;display: none;" id="showpass"></span>
	</td>
</tr>
<tr> 
	<td>Password:<span class="red">*</span></td>
	<td> 
		<input name="password" type="password" id="pass" class="rounded textfield" 
			size="20" maxlength="20" onchange="checkPassword(this);" >
	</td>
</tr>
<tr>
	<td>Retype Password:<span class="red">*</span></td>
	<td>
		<input name="repass" type="password" id="repass" class="rounded textfield" 
				size="20" maxlength="20" />
	</td>
</tr>
<tr>
	<td colspan="6">
		<div align="center" style="margin-top:10px">
			<input type="submit" style="margin-top:5px" name="changepass" class="buttons rounded" id="changepass"  value="Change Password">
			&nbsp;&nbsp;
			<input type="button" name="fileback" id="fileback" class="buttons rounded" value="Back"  onClick="location.href='<?php print CreateURL('index.php','mod=users');?>'">
		</div>
	</td>
</tr>
</table>
<input type="hidden" name="passwords" id="passwords" />
<input type="hidden" name="email" id="email" value="<?php print $contsInfo->email;?> " />

				</fieldset>			
			</form>
		</div><!--Div Contents closed-->
	</div><!--Div main closed-->

				</div><!--Content div closed-->
			</td>
		</tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>