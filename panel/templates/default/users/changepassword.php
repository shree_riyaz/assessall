<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Change Password</title>

<script>
function hide()
  { 
  document.getElementById('showpass').style.display='none';
  }
function IsWhiteSpaces(text)
{
	if(text.value.indexOf(' ') >= 0)
	{
		alert("Password can not contain blank spaces.");
		text.value='';
		text.focus();
	}
}
 </script>
</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body >

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<fieldset class="rounded"><legend>Users</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			<form method="post" action="" name="frmlist" enctype="multipart/form-data">
				<table width="40%" border="0" cellspacing="1" cellpadding="4"  style="margin-top:14px" align="center" class="tableccss">
				<tr> 
				  <th height="30" colspan="6" >Change Password</th>
				</tr>
				   <tr>
				   <td  style="font-size:9px"align="right"  colspan="6" class="fontstyle">Fields marked with ( <span style="color:red">*</span> ) are mandatory</td></tr>	
				  <tr>
				 <td height="0" align="right"  class="fontstyle">Name:</td>	
			   <td height="0" align="left">
			   <label  class="fontstyle" style="background:cccccc"><?php print $contsInfo->name;?></label>
			   </td>
				</tr>
				   <tr> 
				  <td align="right"  class="fontstyle">User Name: </td>
				  <td align="left"><label> 
					 <label class="fontstyle"><?php print $contsInfo->user_name;?></label>
				   </label></td>
				</tr>
					<tr><td align="left"><div  style="background-color:#cccccc;"  id="showpass">
					</div></td>
			<td align="right"  style=" border-top:none">
					<input type="checkbox" name="autopass" id="autopass"  onClick="xajax_testcaptchaWork(document.frmlist.autopass.checked)"	 value="auto pass"style="border:none"><label style=" font-family:Arial, Helvetica, sans-serif;font-size:12px">Auto generate password</label>
							</td>
			</tr>
				 <tr> 
					 <td align="right"  class="fontstyle">Password: </td>
				  <td align="left">
					<input name="pass" type="password" id="pass"  class="input" onchange="checkPassword(this);" size="20"  maxlength="15"  value="<?php echo $frmdata['pass'];?>"><?php echo MANMES?>
				   </td>
				</tr> 
			<tr>
					<td align="right" class="fontstyle">Retype Password: </td>
				  <td align="left"><input name="repass" type="password" id="repass" class="input" size="20" maxlength="15" /><?php echo MANMES?>
				  </td>
				</tr>
			
			  <td colspan="6"  align="center" >
				   <input type="submit" style="margin-top:5px" name="changepass" class="buttons rounded" id="changepass"  value="Change Password"				  
				   >
					  &nbsp;&nbsp;
					<input type="button" name="fileback" id="fileback" class="buttons rounded" value="Back"  onClick="location.href='<?php print CreateURL('index.php','mod=users');?>'">
					 
			</td>
				</tr>
			  </table>
			  <input type="hidden" name="passwords" id="passwords" />
			  <input type="hidden" name="email" id="email" value="<?php print $contsInfo->email;?> " />
			</form>
		<br>		
		</div><!--Div Contents closed-->
				</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>