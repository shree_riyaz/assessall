function finishTest()
{alert('here');
    var x = confirm("Are you sure you want to finish this Test ?");
    if (x)
    { 
        //return true;
		//document.getElementById('tbl_totalquestions').style.display='none';
		load('result.html');
    }
    else
    { 
		return false;
    }
}

function finishConfirmation()
{
    alert("You have completed all questions.\nPress the 'Previous Question' button to review all questions once again. \nPress the 'Finish Test' button to finish this test and view the results.");
}

function confirmSkip()
{    
    return;
    
    var x = confirm("Are you sure you want to skip this question and navigate to another one ?\nIf you like to select an answer, choose appropriate answer and press the button 'Select Answer'.\n\nPress 'OK' to skip this question. \n\nPress 'Cancel' to stay in this page so that you can select an answer.");
    if (x)
    { 
        return true;
    }
    else
    { 
        return false;
    }
}

function load(url) {
    location.href=url;
}

function popup(url, module) 
{
	if(module=='viewexam_detail')
	{
		var width  = 900;
		var height = 450;
		var left   = (screen.width  - width)/2;
		var top    = (screen.height - height)/2;
	}
	else if(module=='video_preview')
	{
		var width  = 700;
		var height = 420;
		var left   = (screen.width  - width)/2;
		var top    = (screen.height - height)/2;
	}
	else
	{
		 var width  = 1000;
		 var height = 630;
		 var left   = (screen.width  - width)/2;
		 var top    = (screen.height - height)/2;
	}

 var params = 'width='+width+', height='+height;
 params += ', top='+top+', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=no';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}

function PrintValues(){

  var myString = document.mainform.file.value
  var myStringLength = myString.length
  var Comma = myString.lastIndexOf(',')
  var SufNumChars = Comma + 1

  document.mainform.result.value=('');

  for(i=0; i<Comma; i++) 
	document.mainform.result.value+=(myString.charAt(i));

  document.mainform.result.value+=(' ');

  for(i=SufNumChars; i<myStringLength; i++) 
	document.mainform.result.value+=(myString.charAt(i));

}

function FormRecord()
{
	document.frmlist.pageNumber.value=1;
	document.frmlist.submit();

}

function GotoPage(Pagename) 
{
	location.href=Pagename;
}
//=============================================================================================================================
//THIS FUNCTION POSTS FORM FOR NEXT PAGE ....
//THIS FUNCTION POSTS FORM FOR NEXT PAGE ....
//=============================================================================================================================
function NextPage()
{
   document.frmlist.pageNumber.value = eval(document.frmlist.pageNumber.value) + 1;
   document.frmlist.submit();
}
//===========================================================================================================================
//=============================================================================================================================
//THIS FUNCTION POSTS FORM FOR PREVIOUS PAGE ....
//THIS FUNCTION POSTS FORM FOR PREVIOUS PAGE ....
//=============================================================================================================================
function PrePage()
{
   
	document.frmlist.pageNumber.value = eval(document.frmlist.pageNumber.value) - 1  ;
	document.frmlist.submit();
}

//=============================================================================================================================
//=============================================================================================================================
//THIS FUNCTION POSTS FORM FOR SPECIFIED PAGE NUMBER....
//THIS FUNCTION POSTS FORM FOR SPECIFIED PAGE NUMBER....
//=============================================================================================================================
function NextPageLink(val)
{
    
	document.frmlist.pageNumber.value = eval(val)-1  ;
	document.frmlist.submit();
}

function setvalue(tar,con,val)
{
	con.value=val;
	tar.submit();
}

function isChar(val,id, field_name)
{
	//alert(field_name);
	/*if(!isNaN(val))
	{
		alert('Please enter characters only.');
		document.getElementById(id).value='';
		document.getElementById(id).focus();
		return false;
	}*/
	if (val.length!=0)
    {  
		if(field_name=='rank name' || field_name=='subject name')
		{
			if(val.length>=2)
			{
			   for (i = 0; i < val.length; i++)
			   {
				   var ch = val.charAt(i);
				   //if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") ||  (ch == ".") || (ch == "&") || (ch == " ") || (ch == "'") || (ch == "(") || (ch == ")") || (ch == '"') || (ch == "/") || (ch == "-"))
				   
				   if(field_name=='rank name')
				   {
					   if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch == " ") || (ch == ".") || (ch == "'") || (ch == "/")) 
					   {
						  continue;
						}
					   else
					   {
						alert("Please enter only characters in "+field_name+".");
						document.getElementById(id).value='';
						document.getElementById(id).focus();
						return false;
					   }
				   }
				   else if(field_name=='subject name')
				   {
					   if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch == " ") || (ch == "&") || (ch == ".") || (ch == "'") || (ch == "/") || (ch == "(") || (ch == ")")) 
					   {
						  continue;
						}
					   else
					   {
						alert("Please enter only characters in "+field_name+".");
						document.getElementById(id).value='';
						document.getElementById(id).focus();
						return false;
					   }
				   }
			   }
			}
			else
			{
				alert("Please enter data at least 2 characters long.");
				document.getElementById(id).value='';
				document.getElementById(id).focus();
				return false;
			}
		}
		else
		{
			if(val.length>=3)
			{
			   for (i = 0; i < val.length; i++)
			   {
				   var ch = val.charAt(i);
				   //if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") ||  (ch == ".") || (ch == "&") || (ch == " ") || (ch == "'") || (ch == "(") || (ch == ")") || (ch == '"') || (ch == "/") || (ch == "-"))
				   
				   
					  // alert("ok");
					   if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch == " ") || (ch == ".") || (ch == "'")) 
					   {
						  continue;
						}
					   else
					   {
						alert("Please enter only characters in "+field_name+".");
						document.getElementById(id).value='';
						document.getElementById(id).focus();
						return false;
					   }
			   }
			}
			else
			{
				alert("Please enter data at least 3 characters long.");
				document.getElementById(id).value='';
				document.getElementById(id).focus();
				return false;
			}
		}
    }

   return true;
}

function IsAlphaNum(val, field_name) 
{
  if (val.value.length != 0)
   {
	  if(val.value.length>=3)
	  {
		   for (i = 0; i < val.value.length; i++)
		   {
			   var ch = val.value.charAt(i);
			  // if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch >= "0" && ch <= "9")  || (ch == " ") || (ch == ".") || (ch == ",") || (ch == "-") (ch == "|")|| 
			   if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch >= "0" && ch <= "9")  || (ch == " ") || (ch == ".") || (ch == "&") ||(ch == "'") || (ch == "(") || (ch == ")") || (ch == "-"))
			   {
				  continue;
				} 
			   else
			   {
				alert("Please enter alphanumeric value in "+field_name+".");
				val.value='';
				val.focus();
				return false;
			   }
		  }
	  }
	  else
	  {
		  alert("Please enter data at least 3 characters long.");
			val.value='';
			val.focus();
			return false;
	  }
  }

   return true;
}

function examName(val, field_name) 
{
  if (val.value.length != 0)
   {
	   	if(val.value.length>=2)
		{
			for (i = 0; i < val.value.length; i++)
			{
			 	var ch = val.value.charAt(i);
				if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch >= "0" && ch <= "9")  || (ch == " ") || (ch == ".") || (ch == "&") ||(ch == "'") || (ch == "(") || (ch == ")") || (ch == "-") || (ch == "/"))
				{
					continue;
				} 
				else
				{
					alert("Please enter alphanumeric value in "+field_name+".");
					val.value='';
					val.focus();
					return false;
				}
		  	}
		}
		else
		{
			alert("Please enter data at least 2 characters long.");
			val.value='';
			val.focus();
			return false;
		}
  	}

   return true;
}

function managePageCheckIsAlphaNum(val, field_name) 
{
  if (val.value.length != 0)
   {
	   for (i = 0; i < val.value.length; i++)
	   {
		   var ch = val.value.charAt(i);
		  // if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch >= "0" && ch <= "9")  || (ch == " ") || (ch == ".") || (ch == ",") || (ch == "-") (ch == "|")||
		   if(field_name=='student number')
		   {
			   if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch >= "0" && ch <= "9") || (ch == "-"))
			   {
				  continue;
				} 
			   else
			   {
				alert("Please enter alphanumeric value in "+field_name+".");
				val.value='';
				val.focus();
				return false;
			   }
		   }
		   else
		   {
			   if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch >= "0" && ch <= "9")  || (ch == " ") || (ch == ".") || (ch == "&") ||(ch == "'") || (ch == "(") || (ch == ")") || (ch == "-"))
			   {
				  continue;
				} 
			   else
			   {
				alert("Please enter alphanumeric value in "+field_name+".");
				val.value='';
				val.focus();
				return false;
			   }
		   }
	  }
	  
  }

   return true;
}

function managePageCheckIsChar(val,id, field_name)
{
	if (val.length!=0)
    {  
		for (i = 0; i < val.length; i++)
	    {
		   var ch = val.charAt(i);
		   if(field_name=='rank name' || field_name=='subject name')
		   {
		   //if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") ||  (ch == ".") || (ch == "&") || (ch == " ") || (ch == "'") || (ch == "(") || (ch == ")") || (ch == '"') || (ch == "/") || (ch == "-"))
			   if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch == " ") || (ch == "&") || (ch == ".") || (ch == "'") || (ch == "/")) 
			   {
				  continue;
				}
			   else
			   {
				alert("Please enter only characters in "+field_name+".");
				document.getElementById(id).value='';
				document.getElementById(id).focus();
				return false;
			   }
		   }
		   else
		   {
			   if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch == " ") || (ch == ".") || (ch == "'")) 
			   {
				  continue;
				}
			   else
			   {
				alert("Please enter only characters in "+field_name+".");
				document.getElementById(id).value='';
				document.getElementById(id).focus();
				return false;
			   }
		   }
	    }
    }

   return true;
}

function isNum(val,id)
{
	//if(isNaN(val))
	{
		if (val.length!=0)
	    {  
			for (i = 0; i < val.length; i++)
		    {
			   var ch = val.charAt(i);
			   
				   if ((ch >= "0" && ch <= "9")) 
				   {
					  continue;
					}
				   else
				   {
					alert("Please enter number only.");
					document.getElementById(id).value='';
					document.getElementById(id).focus();
					return false;
				   }
			   }
	    }
	}
		//alert('Please enter number only.');
		//document.getElementById(id).value='';
		//document.getElementById(id).focus();
		//return false;
	
}

function CheckEmailId(val) {
// Check for a properly formatted email address.
  if ((val.value.length == 0)) {
      return false;
   }

   if (val.value.length != 0) {
	   var emailformat = /^([a-z0-9._-]{1,100})+@([-a-z0-9]{3,500}\.)+([a-z]{2}|com|net|edu|org|gov|mil|int|biz|pro|info|arpa|aero|coop|name|museum|nic|in|co.in|co|inc|ac.in)$/;
      //var emailformat = /^[^@\s]+@([-a-z0-9]{3,500}\.)+([a-z]{2}|com|net|edu|org|gov|mil|int|biz|pro|info|arpa|aero|coop|name|museum|nic|in|co.in|co|inc|ac.in)$/;
	  //var emailformat = /^[^@\s]+@([-a-z0-9A-Z]+\.)+([a-z]{2}|com|net|edu|org|gov|mil|int|biz|pro|info|arpa|aero|coop|name|museum|nic|in|co.in|co|inc)$/;
  /*var emailformat = /^[^@\s]+@([-a-z0-9]+\.)+([a-z]{2}|com|net|edu|org|gov|mil|int|biz|pro|info|arpa|aero|coop|name|museum|nic|in|co.in|co|inc)$/;*/
      if (!emailformat.test(val.value))
	   {
	     alert("Please enter valid email id. E.g. example123@gmail.com");
		 val.value="";
		 val.focus();
		 return false;
      }
      else
      {
    	  //alert(val.value.indexOf('@'));
    	  var email_username = val.value.substr(0, val.value.indexOf('@'));
    	  if(email_username.length<6)
    	  {
    		//  alert("Please enter your email username atleast 6 characters long.");
    		//  val.value="";
    		//  val.focus();
    		//  return false;
    	  }
      }
   }
   return true;
}

$(document).ready(function(){
  //$('#celebs tbody tr:even').addClass('zebra');
  $('#celebs tbody tr').mouseover(function(){
    $(this).addClass('zebraHover');
  });
  $('#celebs tbody tr').mouseout(function(){
    $(this).removeClass('zebraHover');
  });
});


function deleterecord(val, path, message)
{
	//alert("ok");
	var x = confirm(message);
    if (x)
    { 
        //alert(path);
		window.location.href=path;
    }
    else
    { 
		return false;
    }
}

function OrderPage(OrderBy)
{
	
	document.frmlist.pageNumber.value = eval(document.frmlist.pageNumber.value);
	
	if(document.frmlist.orderby.value.search(OrderBy)<0)
		document.frmlist.orderby.value = OrderBy;
	else
	{
		if(document.frmlist.orderby.value.search(' desc')<0)
			document.frmlist.orderby.value = OrderBy +' desc';
		else
			document.frmlist.orderby.value = OrderBy;
	}
	//alert(OrderBy);
	document.frmlist.submit();
}

function checkPassword(val)
{
	if(val.value.length!=0)
	{
		if(val.value.length<5)
		{
			alert('Please enter password at least 5 characters long.');
			val.value="";
			val.focus();
			return false;
		}
	}
	return true;
}

function checkContactNumber(val)
{
	if (val.value.length != 0) 
	{
	  var contactformat = /^\+?([0-9]{2,4})-?([0-9]{3,5})-?([0-9]{4,8})$/;
      if (!contactformat.test(val.value))
	  {
	     alert("Please enter valid contact number. E.g. +91-151-1234567, 01234567891, +912345678901");
		 val.value="";
		 val.focus();
		 return false;
      }
	}
	return true;
}

function checkforPercentage(val) 
{
	if (val.value.length!=0)
    {  
		if(isNaN(val.value))
		{
			alert('Please enter only numeric value.');
			val.value='';
			val.focus();
			return false;
		}
		
		if(val.value<=100)
		{
			for (var i = 0; i < val.value.length; i++)
		    {
			   var ch = val.value.charAt(i);
			   //if ((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") ||  (ch == ".") || (ch == "&") || (ch == " ") || (ch == "'") || (ch == "(") || (ch == ")") || (ch == '"') || (ch == "/") || (ch == "-"))
			   if ((ch >= "0" && ch <= "9"))
			   {
				  continue;
			   }
			   else
			   {
				alert("Please enter only integer value.");
				val.value='';
				val.focus();
				return false;
			   }
		    }
		}
		else
		{
			alert("Please enter value less than or equal to 100.");
			val.value='';
			val.focus();
			return false;
		}
    }

   return true;
}
function selectAllModule()
{
	var node_list = document.getElementsByTagName('input'); 
	for (var i = 0; i < node_list.length; i++) {
    	var node = node_list[i]; 
    	if (node.getAttribute('type') == 'checkbox') {
			divid = node.getAttribute('id');
			if (document.getElementById('div:'+divid))
			document.getElementById('div:'+divid).style.display ='';
			node.checked= true;
    	}
	} 
}
function unselectAllModule()
{
	var node_list = document.getElementsByTagName('input'); 
	for (var i = 0; i < node_list.length; i++) {
    	var node = node_list[i]; 
    	if (node.getAttribute('type') == 'checkbox') {
			divid = node.getAttribute('id');
			if (document.getElementById('div:'+divid))
			document.getElementById('div:'+divid).style.display ='none';
			node.checked= false;
    	}
	} 
}
function roundNumber(number, digits) 
{
    var multiple = Math.pow(10, digits);
    var rndedNum = Math.round(number * multiple) / multiple;
    return rndedNum;
}
function CheckEmailMultipleId(val)
{
	//OriginalValue=val.value;
	loopVal=val.value;
	Str='';
	tmp=loopVal.split(',');
	for (i=0; i < tmp.length; i++)
	{
		if ((tmp[i].length == 0))
		{
			return false;
		}
		if (tmp[i].length != 0)
		{
			  var emailformat = /^[^@\s]+@([-a-z0-9A-Z]+\.)+([a-z]{2}|com|net|edu|org|gov|mil|int|biz|pro|info|arpa|aero|coop|name|museum|nic|in|co.in|co|inc)$/;
			if (!emailformat.test(tmp[i].trim()))
			{
				alert("Please enter valid email.");
				val.value=Str;
				val.focus();
				 return false;
			}
		}
		Str+=tmp[i];
	}
	return true;
}