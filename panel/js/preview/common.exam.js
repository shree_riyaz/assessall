function newpage(path)
{
	document.examform.action = path;
	document.examform.submit();
}

function blockduplicate(sbox)
{
	var match = $('.match');

	if(sbox.value == '')
	{
		return true;
	}
	for(var count=0; count < match.length; count++)
	{
		if(sbox.id == match[count].id)
		{
			continue;
		}
		if(sbox.value == match[count].value)
		{
			alert("This value is already matched.\nPlease select a diffrent one.");
			sbox.value = '';
			return false;
		}
	}
	return true;
}

$(document).ready(function()
{
	$('#ques-nav').scrollTop($('.current-que-link').offset().top-500);
	SyntaxHighlighter.defaults.toolbar = false;
	SyntaxHighlighter.all();
	
	if($("#hints"))
	{
		$("#hints").dialog({ autoOpen: false });
		$('#opener').click(function() {
			$("#hints").dialog('open')
			return false;
		});
	}
	
	if($("#video_preview"))
	{
		$("#video_preview").dialog({ autoOpen: false,width: 'auto' });
		$('#show_video').click(function() {
			$("#video_preview").dialog('open')
			return false;
		});
	}
});