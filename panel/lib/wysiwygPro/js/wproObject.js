/*
 * (c) Copyright Chris Bolt 2007, All Rights Reserved.
 * Unlicensed distribution, copying, reverse engineering, re-purposing or otherwise stealing of this code is a violation of copyright law.
 * Get yourself a license at www.wysiwygpro.com
 */
if (typeof(wproObject)=='undefined') {
	wproObject = {
		htmlSpecialChars:function (str) {
			return String(str).replace(/&/gi, '&amp;').replace(/\xA0/gi, '&nbsp;').replace(/</gi, '&lt;').replace(/>/gi, '&gt;').replace(/"/gi, '&quot;')	
		},
		serializeMediaToTag:function(data) {
			var str = '';
			var allowedEmpty = /^(title|alt)$/i
			if (data['object']) {
				str += '<object';
				for(var x in data['object']) {
					if (data['object'][x]=='') if (!allowedEmpty.test(x)) continue;
					str	+= ' '+x+'="'+this.htmlSpecialChars(data['object'][x])+'"';
				}
				str+='>';
				if (data['param']) {
					for(var x in data['param']) {
						str+='\n<param name="'+x+'" value="'+this.htmlSpecialChars(data['param'][x])+'"';
						str+=' />';	
					}
				}
			}
			if (data['embed']) {
				str += '<embed';
				for(var x in data['embed']) {
					if (data['embed'][x]=='') if (!allowedEmpty.test(x)) continue;
					str	+= ' '+x+'="'+this.htmlSpecialChars(data['embed'][x])+'"';
				}
				str+='>';
				if (data['content']) {
					str +='<noembed>'+data['content']+'</noembed>';	
				}
				str+='</embed>';
			}
			if (data['content']&&!data['embed']) {
				str+=data['content'];	
			}
			if (data['object']) {
				str+='</object>';
			}
			alert(str);
			return str;
		},
		write:function(data) {
			//document.write('<div id="foo"></div>');
			//document.write(this.serializeMediaToTag(data));
			document.getElementById('foo').innerHTML = this.serializeMediaToTag(data);
			if (!window.opera&&document.all&&window.attachEvent) {
				window.attachEvent("onbeforeunload", _wproObject_prepUnload);
			}
		}
	}
}
function _wproObject_prepUnload() {
	__flash_unloadHandler = function(){};
	__flash_savedUnloadHandler = function(){};
	window.attachEvent("onunload", _wproObject_cleanup);	
}
function _wproObject_cleanup() {
	var objects = document.getElementsByTagName("OBJECT");
	for (var i = objects.length - 1; i >= 0; i--) {
		objects[i].parentNode.removeChild(objects[i]);
	}
	var objects = document.getElementsByTagName("EMBED");
	for (var i = objects.length - 1; i >= 0; i--) {
		objects[i].parentNode.removeChild(objects[i]);
	}
}