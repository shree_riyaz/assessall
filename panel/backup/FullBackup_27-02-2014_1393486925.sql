-- MySQL dump 10.13  Distrib 5.5.8, for Win32 (x86)
--
-- Host: localhost    Database: emath360
-- ------------------------------------------------------
-- Server version	5.5.8

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(200) DEFAULT NULL,
  `disabled` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (2,'Admin','admin','sefL3MSA2sN5s',NULL,'2011-09-30 06:13:34',NULL,'N'),(3,'ashwini agarwal','agarwal.ashwini','seGBHvdfJ/vlI',1,'2012-11-08 10:57:13','ashwini.agarwal@sunarctechnologies.com','N'),(5,'tes','fdsf','seYR5IpfaNMjA',1,'2013-04-09 04:58:45','xyz@xyz.com','N'),(6,'john smith','john','sexFKbViIp1BM',1,'2013-04-09 05:15:00','john@xyz.com','N'),(7,'raj','raj','sezJtCsFtN.7.',1,'2014-02-13 06:21:55','raj@gmail.com','N');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer_title` varchar(250) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=368 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (1,'2',1),(2,'3',1),(3,'1',1),(4,'4',1),(5,'$_GET[];',2),(6,'Request.Form;',2),(7,'Request.QueryString;',2),(8,'$_POST[];',2),(9,'Print out a message asking your user to',3),(70,'21344505867.jpg',22),(10,'Open the input and output files, and use read() and write() to copy the data block by block until read() returns a zero',3),(11,'Use the built in copy() function',3),(12,'Use',3),(69,'11344505863.jpg',22),(68,'False',21),(13,'True',4),(14,'False',4),(15,'True',5),(16,'False',5),(17,'PreHypertextProcessor',6),(18,'HypertextPreprocessor',6),(19,'Hypertext Postprocessor',6),(20,'PostHypertextProcessor',6),(21,'Linux And Mysql Php',7),(22,'Linux Apache Mysql Php',7),(23,'producer:theatre',8),(24,'director:drama',8),(25,'conductor:bus',8),(26,'thespian:play',8),(27,'shard:pottery',9),(28,'shred:wood',9),(29,'blades:grass',9),(30,'chip:glass',9),(31,'mangle:iron',10),(32,'scabbard:sword',10),(33,'bow:arrow',10),(34,'fence:epee',10),(35,'comfort:stimulant',11),(36,'grief:consolation',11),(37,'trance:narcotic',11),(38,'ache:extraction',11),(39,'deallocate()',13),(40,'release ()',13),(41,'free ()',13),(42,'empty ()',13),(47,'Run time',15),(48,'Compile time',15),(49,'Depends on how it is invoked',15),(50,'Both b and c above',15),(51,'::',16),(52,'?',16),(53,':?',16),(54,'%',16),(55,'TRUE',17),(56,'FALSE',17),(57,'X~() {}',18),(58,'X() {}~',18),(59,'X() ~{}',18),(60,'~X() {}',18),(61,'No',19),(62,'Yes',19),(67,'True',21),(71,'31344505870.jpg',22),(72,'answer41344505873.jpg',22),(123,'_ (underscore)',80),(92,'14',40),(91,'13',40),(89,'10',40),(90,'12',40),(122,'| (pipeline)',80),(120,'no',79),(121,'* (asterisk)',80),(119,'yes',79),(124,'- (hyphen)',80),(132,'1344936477502a1a1d48761.jpg',82),(131,'1344936477502a1a1d47a07.jpg',82),(130,'1344936477502a1a1d46c97.jpg',82),(129,'1344936477502a1a1d45d72.jpg',82),(149,'option 4',87),(148,'option 3',87),(146,'option 1',87),(147,'option 2',87),(150,'ch 1',88),(151,'ch 2',88),(152,'ch 3',88),(153,'ch 4',88),(154,'The element will be set to 0.',90),(155,'The compiler would report an error.',90),(156,'The program may crash if some important data gets overwritten.',90),(157,'The array size would appropriately grow.',90),(158,'Value of elements in array',91),(159,'First element of the array',91),(160,'Base address of the array',91),(161,'Address of the last element of array',91),(162,'TRUE',92),(163,'FALSE',92),(164,'1',93),(165,'2',93),(166,'3',93),(167,'4',93),(252,'True',115),(253,'False',115),(254,'15',116),(255,'10',116),(256,'5',116),(257,'0',116),(262,'True',119),(263,'False',119),(294,'10',132),(295,'12',132),(296,'13',132),(297,'14',132),(298,'True',133),(299,'False',133),(300,'True',134),(301,'False',134),(302,'1',135),(303,'2',135),(304,'3',135),(305,'4',135),(309,'False',137),(308,'True',137),(310,'yes',138),(311,'no',138),(312,'thick',141),(313,'green',141),(314,'teeth',141),(315,'strict',141),(316,'work',142),(317,'deep',142),(318,'go',142),(319,'down',142),(320,'True',143),(321,'False',143),(322,'vocal',144),(323,'world',144),(324,'city',144),(325,'std',144),(326,'Yes',145),(327,'no',145),(328,'green',146),(329,'red',146),(330,'white',146),(331,'blue',146),(332,'gap',147),(333,'sad',147),(334,'laugh',147),(335,'rough',147),(336,'largest railway station',149),(337,'longest railway station',149),(338,'highest railway station',149),(339,'none of the above',149),(340,'asia',150),(341,'africa',150),(342,'england',150),(343,'india',150),(344,'Junagarh, Gujarat',151),(345,'Diphu, Assam',151),(346,'Kohima, Nagaland',151),(347,'Gangtok, Sikkim',151),(348,'Labour Party',152),(349,'Nazi Party',152),(350,'Ku-Klux-Klan',152),(351,'Democratic Party',152),(352,'Physics and Chemistry',153),(353,'Physiology or Medicine',153),(354,'Literature, Peace and Economics',153),(355,'All of the above',153),(356,'3.6',154),(357,'7.2',154),(358,'8.4',154),(359,'10',154),(360,'50',155),(361,'56',155),(362,'70',155),(363,'80',155),(364,'9',156),(365,'10',156),(366,'12',156),(367,'20',156);
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backup_data`
--

DROP TABLE IF EXISTS `backup_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backup_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) DEFAULT NULL,
  `backup_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `backup_file_path` varchar(250) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backup_data`
--

LOCK TABLES `backup_data` WRITE;
/*!40000 ALTER TABLE `backup_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate`
--

DROP TABLE IF EXISTS `candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `address` text,
  `email` varchar(150) DEFAULT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `stream_id` int(11) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state_id` int(3) DEFAULT NULL,
  `martial_status` enum('M','S') DEFAULT NULL,
  `children` int(2) DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `remark` text,
  `pro_resume` varchar(255) DEFAULT NULL,
  `pro_image` varchar(255) DEFAULT NULL,
  `pro_sign` varchar(255) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate`
--

LOCK TABLES `candidate` WRITE;
/*!40000 ALTER TABLE `candidate` DISABLE KEYS */;
INSERT INTO `candidate` VALUES (20,'anand','anand','pareek','','anand@gmail.com','',NULL,NULL,'seyeqIiI5u0nE','2014-02-20 10:50:50',NULL,'S',0,'2014-02-14','',NULL,NULL,NULL,0),(15,'sarah','sarah','smith','','','',NULL,NULL,'seWfr4VdeLxO.','2013-12-23 11:35:02',NULL,'S',0,'1970-01-01','',NULL,NULL,NULL,0),(2,'atul.sharma','Atul','Sharma','','atulsharma328@gmail.com','',NULL,NULL,'seWfr4VdeLxO.','2013-03-29 10:38:34',0,'S',0,'1990-04-11','',NULL,NULL,NULL,0),(21,'rajiv','rajiv','soni','','','',NULL,NULL,'seH.XAqOxhYvU','2014-02-20 10:53:44',NULL,'S',0,'2000-02-13','',NULL,NULL,NULL,0),(4,'shakti','shakti','purohit','','shakti.singh@sunarctechnologies.com','',NULL,NULL,'seuef6.QUCJSY','2013-04-04 11:14:57',0,'S',0,'1970-01-01','',NULL,'515d60b0adcf9_Screen Shot 2013-01-17 at 11.04.13 PM.jpg','515d60b16d641_Screen Shot 2013-01-17 at 11.04.13 PM.jpg',0),(8,'vijay','vijay','Sharma','jnv colony','xyz@gmail.com','01234567891',NULL,NULL,'seTyIMe1Ab79I','2013-04-05 07:26:30',27,'M',99,'2013-04-05','hgfhgf','Instruction1365146789.docx','apple1365146789.jpg','apple1365146790.jpg',0),(9,'ajay','ajay','sharma','','ajay@xyz.com','',NULL,NULL,'seWfr4VdeLxO.','2013-04-06 11:12:51',0,'S',0,'2013-04-09','',NULL,'51600333054f6_apple.jpg',NULL,0),(11,'vikas','vikas','purohit','','vikas@gmail.com','0123456789',NULL,NULL,'seWfr4VdeLxO.','2013-04-09 07:38:10',0,'S',0,'1970-01-01','',NULL,NULL,NULL,1),(22,'rohit','ROHIT','vyas','','rohit@gmail.com','',NULL,NULL,'seyg4aaQ.FM4Y','2014-02-20 10:58:05',NULL,'S',0,'0000-00-00','',NULL,NULL,NULL,0),(14,'shaktisingh','Shakti','','','2shakti.singh@sunarctechnologies.com','',NULL,NULL,'seYbqlVB7.n7k','2013-04-09 12:35:00',NULL,'S',0,'1970-01-01','',NULL,'51640af34da72_Unknown.jpeg',NULL,0),(16,'preety','preety','','','','',NULL,NULL,'seWfr4VdeLxO.','2014-02-13 06:08:51',NULL,'S',0,'2000-02-01','',NULL,NULL,NULL,0),(17,'ram','ram','kumar','','','',NULL,NULL,'seWfr4VdeLxO.','2014-02-19 06:24:21',NULL,'S',0,'1970-01-01','',NULL,NULL,NULL,0),(18,'sandeep','sandeep','','','','',NULL,NULL,'segwjcYBOwYeI','2014-02-19 07:10:28',NULL,'S',0,'0000-00-00','',NULL,NULL,NULL,0),(19,'chirag','chirag','vyas','','','',NULL,NULL,'seHiy7uLeVDa6','2014-02-19 08:02:47',NULL,'S',0,'1970-01-01','',NULL,NULL,NULL,0),(23,'ansuya','ansuya','purohit','','ansuya@gmail.com','',NULL,NULL,'seF7U2RBEp5Sk','2014-02-20 11:14:59',NULL,'S',0,'0000-00-00','',NULL,NULL,NULL,0),(24,'shweta','shweta','dixit','','','',NULL,NULL,'sejWTZlNYkso2','2014-02-20 11:16:22',NULL,'S',0,'1999-02-10','',NULL,NULL,NULL,0),(25,'priya','priya','modi','','','',NULL,NULL,'seV6.Ds7vSSkI','2014-02-20 11:17:43',NULL,'S',0,'1998-02-18','',NULL,NULL,NULL,0),(26,'shiv','shiv','sodi','','','',NULL,NULL,'seNbjBDAkjtbg','2014-02-20 11:18:31',NULL,'S',0,'0000-00-00','',NULL,NULL,NULL,0),(27,'navratan','navratan','joshi','','','',NULL,NULL,'seK5otziD0YWE','2014-02-20 11:19:52',NULL,'S',0,'0000-00-00','',NULL,NULL,NULL,0),(28,'dheeraj','dheeraj','ranga','','','',NULL,NULL,'se.X0sAliCtxo','2014-02-20 11:21:08',NULL,'S',0,'1999-02-10','',NULL,NULL,NULL,0),(29,'rishi','rishi','vyas','','','',NULL,NULL,'seNOL9v/f.VvY','2014-02-20 11:23:46',NULL,'S',0,'0000-00-00','',NULL,NULL,NULL,0),(30,'pinki','pinki','rathi','','','',NULL,NULL,'seyk6HiOB6m1s','2014-02-20 11:24:39',NULL,'S',0,'0000-00-00','',NULL,NULL,NULL,0),(31,'devesh','devesh','mundhra','','devesh@gmail.com','',NULL,NULL,'secQGMq1dv83A','2014-02-20 11:25:47',NULL,'S',0,'1999-02-24','',NULL,NULL,NULL,0),(32,'vishnu','vishnu','joshi','','','',NULL,NULL,'sebT6IaLSFV/g','2014-02-20 11:26:41',NULL,'S',0,'1997-02-05','',NULL,NULL,NULL,0),(33,'rajkumar','rajkumar','pandia','','','',NULL,NULL,'seYr42ULT2jZs','2014-02-20 11:27:41',NULL,'S',0,'1995-02-08','',NULL,NULL,NULL,0),(34,'nidhishanker','Nidhishanker','Modi',NULL,'nidhishanker.modi@sunarctechnologies.com',NULL,NULL,NULL,'seqwSfPe38tzU','2014-02-27 07:31:57',NULL,NULL,0,NULL,NULL,NULL,'530ee9ed9a023_csv-icon.jpg',NULL,0);
/*!40000 ALTER TABLE `candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_education`
--

DROP TABLE IF EXISTS `candidate_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `class` varchar(50) NOT NULL,
  `year` int(10) DEFAULT NULL,
  `percentage_marks` float(9,2) DEFAULT NULL,
  `maximum_marks` float(9,2) DEFAULT NULL,
  `obtained_marks` float(9,2) DEFAULT NULL,
  `grade` varchar(10) DEFAULT NULL,
  `stream` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_education`
--

LOCK TABLES `candidate_education` WRITE;
/*!40000 ALTER TABLE `candidate_education` DISABLE KEYS */;
INSERT INTO `candidate_education` VALUES (7,16,'12',2013,83.33,600.00,500.00,'A','computer'),(8,11,'10',2011,0.00,0.00,0.00,'a',''),(9,8,'Graduation',1950,91.00,5000.00,4550.00,'ds','dfsdf');
/*!40000 ALTER TABLE `candidate_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_experience`
--

DROP TABLE IF EXISTS `candidate_experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `company` varchar(50) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `stream` varchar(100) DEFAULT NULL,
  `details` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_experience`
--

LOCK TABLES `candidate_experience` WRITE;
/*!40000 ALTER TABLE `candidate_experience` DISABLE KEYS */;
INSERT INTO `candidate_experience` VALUES (10,8,'apple','2013-04-05','2013-04-03','iphone','fsdfsd'),(11,8,'\"fg\"','2013-04-11','2013-04-13','gfh','fhg');
/*!40000 ALTER TABLE `candidate_experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_homework_history`
--

DROP TABLE IF EXISTS `candidate_homework_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_homework_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `homework_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `marks_obtained` int(11) DEFAULT '0',
  `time_taken` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_homework_history`
--

LOCK TABLES `candidate_homework_history` WRITE;
/*!40000 ALTER TABLE `candidate_homework_history` DISABLE KEYS */;
INSERT INTO `candidate_homework_history` VALUES (1,1,1,1,0,16,'2013-03-30 12:42:34'),(2,1,2,1,8,502,'2013-04-01 05:56:40'),(3,4,4,8,3,1659,'2013-04-06 11:44:27'),(4,4,4,2,6,105,'2013-04-08 09:38:26'),(5,3,8,11,6,26,'2013-04-09 09:38:45'),(6,4,9,14,4,62,'2013-04-09 12:42:17'),(7,4,9,4,1,9,'2013-04-09 12:57:41'),(8,4,11,9,6,99,'2013-04-29 04:54:13'),(9,4,11,8,3,18,'2013-04-29 05:07:48'),(10,1,15,19,0,19,'2014-02-19 08:34:22'),(11,8,17,19,5,19,'2014-02-20 04:55:33'),(12,5,18,19,10,21,'2014-02-20 05:17:35'),(13,5,19,19,8,24,'2014-02-20 05:40:22');
/*!40000 ALTER TABLE `candidate_homework_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_homework_match_question_history`
--

DROP TABLE IF EXISTS `candidate_homework_match_question_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_homework_match_question_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `homework_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `question_match_left_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `given_answer_id` int(11) DEFAULT NULL,
  `is_answer_correct` enum('Y','N') DEFAULT NULL,
  `correct_question_match_right_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_homework_match_question_history`
--

LOCK TABLES `candidate_homework_match_question_history` WRITE;
/*!40000 ALTER TABLE `candidate_homework_match_question_history` DISABLE KEYS */;
INSERT INTO `candidate_homework_match_question_history` VALUES (1,4,8,4,24,43,5,57,'Y',57,'2013-04-06 11:44:27'),(2,4,8,4,24,44,5,59,'N',58,'2013-04-06 11:44:27'),(3,4,2,4,24,43,5,59,'N',57,'2013-04-08 09:38:25'),(4,4,2,4,24,44,5,58,'Y',58,'2013-04-08 09:38:26'),(5,4,14,9,25,10,5,16,'N',14,'2013-04-09 12:42:17'),(6,4,14,9,25,11,5,15,'N',16,'2013-04-09 12:42:17'),(7,4,4,9,25,10,5,NULL,NULL,NULL,'2013-04-09 12:57:41'),(8,4,4,9,25,11,5,NULL,NULL,NULL,'2013-04-09 12:57:41'),(9,4,9,11,25,10,5,14,'Y',14,'2013-04-29 04:54:12'),(10,4,9,11,25,11,5,16,'Y',16,'2013-04-29 04:54:12'),(11,4,9,11,24,43,5,57,'Y',57,'2013-04-29 04:54:13'),(12,4,9,11,24,44,5,59,'N',58,'2013-04-29 04:54:13'),(13,4,8,11,25,10,5,14,'Y',14,'2013-04-29 05:07:48'),(14,4,8,11,25,11,5,15,'N',16,'2013-04-29 05:07:48'),(15,4,8,11,24,43,5,57,'Y',57,'2013-04-29 05:07:48'),(16,4,8,11,24,44,5,59,'N',58,'2013-04-29 05:07:48'),(17,1,19,15,26,17,5,NULL,NULL,26,'2014-02-19 08:34:22'),(18,1,19,15,26,18,5,NULL,NULL,27,'2014-02-19 08:34:22');
/*!40000 ALTER TABLE `candidate_homework_match_question_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_homework_question_history`
--

DROP TABLE IF EXISTS `candidate_homework_question_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_homework_question_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `homework_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `given_answer_id` varchar(200) DEFAULT NULL,
  `is_answer_correct` enum('Y','N') DEFAULT NULL,
  `correct_answer_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `given_answer` text,
  `marks_obtained` float(9,2) DEFAULT NULL,
  `solve_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_homework_question_history`
--

LOCK TABLES `candidate_homework_question_history` WRITE;
/*!40000 ALTER TABLE `candidate_homework_question_history` DISABLE KEYS */;
INSERT INTO `candidate_homework_question_history` VALUES (1,1,1,1,8,1,'24','N',26,'2013-03-30 12:42:34',NULL,0.00,4),(2,1,1,1,9,1,'29','N',30,'2013-03-30 12:42:34',NULL,0.00,3),(3,1,1,1,10,1,'32','N',34,'2013-03-30 12:42:34',NULL,0.00,5),(4,1,1,1,11,1,'38','N',36,'2013-03-30 12:42:34',NULL,0.00,2),(5,1,1,2,89,5,'',NULL,0,'2013-04-01 05:56:40','This is answer of subjective type question.\r\n\r\n<script>alert(\'I am trying to make you fool.\');</script>\r\n\r\n\r\nFinally i rest my answer.',2.50,262),(6,1,1,2,93,5,'165','N',166,'2013-04-01 05:56:40',NULL,0.00,82),(7,1,1,2,87,5,'147','N',148,'2013-04-01 05:56:40',NULL,0.00,4),(8,1,1,2,115,5,'253','N',252,'2013-04-01 05:56:40',NULL,0.00,4),(9,1,1,2,15,2,'49','N',48,'2013-04-01 05:56:40',NULL,0.00,2),(10,1,1,2,18,2,'58','N',60,'2013-04-01 05:56:40',NULL,0.00,3),(11,1,1,2,90,2,'156','Y',156,'2013-04-01 05:56:40',NULL,0.00,2),(12,1,1,2,19,2,'62','Y',62,'2013-04-01 05:56:40',NULL,0.00,2),(13,1,1,2,10,1,'32','N',34,'2013-04-01 05:56:40',NULL,0.00,2),(14,1,1,2,8,1,'25','N',26,'2013-04-01 05:56:40',NULL,0.00,3),(15,1,1,2,11,1,'37','N',36,'2013-04-01 05:56:40',NULL,0.00,53),(16,1,1,2,9,1,'29','N',30,'2013-04-01 05:56:40',NULL,0.00,12),(17,4,8,4,8,1,'24','N',26,'2013-04-06 11:44:27',NULL,NULL,38),(18,4,8,4,10,1,'32','N',34,'2013-04-06 11:44:27',NULL,NULL,667),(19,4,8,4,87,5,'148','Y',148,'2013-04-06 11:44:27',NULL,NULL,222),(20,4,8,4,93,5,'165','N',166,'2013-04-06 11:44:27',NULL,NULL,6),(21,4,8,4,88,5,'151','N',153,'2013-04-06 11:44:27',NULL,NULL,16),(22,4,8,4,24,5,'-1','N',0,'2013-04-06 11:44:27',NULL,NULL,371),(23,4,8,4,22,5,'69','N',72,'2013-04-06 11:44:27',NULL,NULL,141),(24,4,2,4,9,1,'29','N',30,'2013-04-08 09:38:25',NULL,NULL,16),(25,4,2,4,10,1,'32','N',34,'2013-04-08 09:38:25',NULL,NULL,9),(26,4,2,4,24,5,'-1','N',0,'2013-04-08 09:38:26',NULL,NULL,46),(27,4,2,4,93,5,'166','Y',166,'2013-04-08 09:38:26',NULL,NULL,52),(28,4,2,4,88,5,'151,152','N',153,'2013-04-08 09:38:26',NULL,NULL,95),(29,4,2,4,87,5,'147','N',148,'2013-04-08 09:38:26',NULL,NULL,91),(30,4,2,4,22,5,'72','Y',72,'2013-04-08 09:38:26',NULL,NULL,23),(31,3,11,8,19,2,'61','N',62,'2013-04-09 09:38:45',NULL,NULL,230),(32,3,11,8,90,2,'156','Y',156,'2013-04-09 09:38:45',NULL,NULL,83),(33,3,11,8,18,2,'58','N',60,'2013-04-09 09:38:45',NULL,NULL,44),(34,3,11,8,119,2,'263','Y',263,'2013-04-09 09:38:45',NULL,NULL,254),(35,4,14,9,88,5,'',NULL,153,'2013-04-09 12:42:17',NULL,NULL,4),(36,4,14,9,89,5,'',NULL,0,'2013-04-09 12:42:17','<html>\r\n<script></script>\r\n<a href=\"mysite.com\">Mysite</a>',4.00,48),(37,4,14,9,25,5,'-1','N',0,'2013-04-09 12:42:17',NULL,NULL,7),(38,4,4,9,89,5,'',NULL,0,'2013-04-09 12:57:41','tsefdfsf',0.50,4),(39,4,4,9,25,5,'-1',NULL,0,'2013-04-09 12:57:41',NULL,NULL,2),(40,4,4,9,88,5,'',NULL,153,'2013-04-09 12:57:41',NULL,NULL,NULL),(41,4,9,11,87,5,'148','Y',148,'2013-04-29 04:54:12',NULL,NULL,59),(42,4,9,11,25,5,'-1','Y',0,'2013-04-29 04:54:13',NULL,NULL,59),(43,4,9,11,24,5,'-1','N',0,'2013-04-29 04:54:13',NULL,NULL,73),(44,4,9,11,88,5,'151','N',153,'2013-04-29 04:54:13',NULL,NULL,205),(45,4,8,11,87,5,'148','Y',148,'2013-04-29 05:07:48',NULL,NULL,10),(46,4,8,11,25,5,'-1','N',0,'2013-04-29 05:07:48',NULL,NULL,9),(47,4,8,11,24,5,'-1','N',0,'2013-04-29 05:07:48',NULL,NULL,54),(48,4,8,11,88,5,'152','N',153,'2013-04-29 05:07:48',NULL,NULL,9),(49,1,19,15,8,1,'25','N',26,'2014-02-19 08:34:22',NULL,NULL,11),(50,1,19,15,10,1,'',NULL,34,'2014-02-19 08:34:22',NULL,NULL,6),(51,1,19,15,11,1,'',NULL,36,'2014-02-19 08:34:22',NULL,NULL,NULL),(52,1,19,15,9,1,'',NULL,30,'2014-02-19 08:34:22',NULL,NULL,NULL),(53,1,19,15,16,2,'',NULL,51,'2014-02-19 08:34:22',NULL,NULL,NULL),(54,1,19,15,15,2,'',NULL,48,'2014-02-19 08:34:22',NULL,NULL,NULL),(55,1,19,15,79,2,'',NULL,119,'2014-02-19 08:34:22',NULL,NULL,NULL),(56,1,19,15,22,5,'',NULL,72,'2014-02-19 08:34:22',NULL,NULL,NULL),(57,1,19,15,26,5,'-1',NULL,0,'2014-02-19 08:34:22',NULL,NULL,NULL),(58,8,19,17,134,9,'300','Y',300,'2014-02-20 04:55:33',NULL,NULL,7),(59,8,19,17,82,9,'131','N',130,'2014-02-20 04:55:33',NULL,NULL,10),(60,5,19,18,9,1,'30','Y',30,'2014-02-20 05:17:35',NULL,NULL,4),(61,5,19,18,11,1,'36','Y',36,'2014-02-20 05:17:35',NULL,NULL,4),(62,5,19,18,8,1,'26','Y',26,'2014-02-20 05:17:35',NULL,NULL,4),(63,5,19,18,10,1,'34','Y',34,'2014-02-20 05:17:35',NULL,NULL,6),(64,5,19,19,10,1,'34','Y',34,'2014-02-20 05:40:22',NULL,NULL,12),(65,5,19,19,11,1,'38','N',36,'2014-02-20 05:40:22',NULL,NULL,2),(66,5,19,19,8,1,'26','Y',26,'2014-02-20 05:40:22',NULL,NULL,3),(67,5,19,19,9,1,'30','Y',30,'2014-02-20 05:40:22',NULL,NULL,6);
/*!40000 ALTER TABLE `candidate_homework_question_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_homework_subject_history`
--

DROP TABLE IF EXISTS `candidate_homework_subject_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_homework_subject_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `homework_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `marks_obtained` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_homework_subject_history`
--

LOCK TABLES `candidate_homework_subject_history` WRITE;
/*!40000 ALTER TABLE `candidate_homework_subject_history` DISABLE KEYS */;
INSERT INTO `candidate_homework_subject_history` VALUES (1,1,1,1,1,0),(2,1,2,1,1,0),(3,1,2,1,2,5),(4,1,2,1,5,3),(5,4,4,8,1,0),(6,4,4,8,5,3),(7,4,4,2,1,0),(8,4,4,2,5,6),(9,3,8,11,2,6),(10,4,9,14,5,4),(11,4,9,4,5,1),(12,4,11,9,5,6),(13,4,11,8,5,3),(14,1,15,19,1,0),(15,1,15,19,2,0),(16,1,15,19,5,0),(17,8,17,19,9,5),(18,5,18,19,1,10),(19,5,19,19,1,8);
/*!40000 ALTER TABLE `candidate_homework_subject_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_log`
--

DROP TABLE IF EXISTS `candidate_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(10) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_log`
--

LOCK TABLES `candidate_log` WRITE;
/*!40000 ALTER TABLE `candidate_log` DISABLE KEYS */;
INSERT INTO `candidate_log` VALUES (26,2,'2013-04-02 11:43:52'),(38,8,'2013-04-05 09:46:05'),(39,8,'2013-04-05 10:03:58'),(40,8,'2013-04-06 10:41:04'),(41,9,'2013-04-06 11:16:11'),(42,8,'2013-04-06 11:16:26'),(43,9,'2013-04-06 11:20:30'),(44,9,'2013-04-06 11:45:17'),(45,8,'2013-04-06 11:50:53'),(46,9,'2013-04-06 11:51:09'),(47,8,'2013-04-06 12:18:47'),(48,8,'2013-04-06 12:19:53'),(49,9,'2013-04-06 12:22:02'),(50,8,'2013-04-06 12:22:16'),(52,2,'2013-04-08 09:26:26'),(53,2,'2013-04-08 09:27:18'),(54,2,'2013-04-08 09:28:15'),(55,2,'2013-04-08 09:30:46'),(56,2,'2013-04-08 09:35:14'),(57,2,'2013-04-08 09:35:44'),(58,2,'2013-04-08 09:36:36'),(59,2,'2013-04-08 09:43:10'),(62,8,'2013-04-09 08:34:47'),(63,11,'2013-04-09 08:37:23'),(64,11,'2013-04-09 08:39:23'),(65,8,'2013-04-09 08:40:33'),(66,11,'2013-04-09 08:43:51'),(68,11,'2013-04-09 09:04:16'),(69,11,'2013-04-09 09:04:47'),(70,8,'2013-04-09 09:26:11'),(71,11,'2013-04-09 09:26:21'),(72,11,'2013-04-09 09:30:40'),(73,11,'2013-04-09 09:32:07'),(74,11,'2013-04-09 09:33:27'),(75,11,'2013-04-09 09:38:13'),(76,11,'2013-04-09 09:53:38'),(85,14,'2013-04-09 12:36:18'),(86,14,'2013-04-09 12:41:11'),(87,4,'2013-04-09 12:57:28'),(92,9,'2013-04-29 04:31:20'),(93,8,'2013-04-29 04:31:40'),(94,8,'2013-04-29 04:41:09'),(95,9,'2013-04-29 04:41:31'),(96,9,'2013-04-29 04:52:16'),(97,2,'2013-04-29 04:55:20'),(98,2,'2013-04-29 04:56:22'),(99,2,'2013-04-29 04:58:15'),(101,8,'2013-04-29 05:00:48'),(102,8,'2013-04-29 05:03:57'),(103,8,'2013-04-29 05:05:32'),(104,8,'2013-04-29 05:07:12'),(106,8,'2013-04-30 11:05:58'),(111,14,'2013-07-12 11:48:26'),(112,14,'2013-07-12 11:49:00'),(118,15,'2013-12-23 11:37:44'),(119,15,'2013-12-23 11:47:43'),(120,8,'2013-12-23 11:50:20'),(121,8,'2013-12-23 11:53:03'),(122,9,'2013-12-23 11:55:23'),(124,17,'2014-02-19 06:24:34'),(125,17,'2014-02-19 06:55:19'),(126,18,'2014-02-19 07:10:38'),(128,19,'2014-02-19 08:03:05'),(129,19,'2014-02-19 08:08:51'),(130,19,'2014-02-19 08:12:10'),(131,19,'2014-02-19 08:33:45'),(132,19,'2014-02-19 11:25:51'),(133,19,'2014-02-20 04:41:55'),(134,19,'2014-02-20 04:44:46'),(135,19,'2014-02-20 04:46:06'),(136,19,'2014-02-20 04:51:25'),(137,19,'2014-02-20 04:55:05'),(138,19,'2014-02-20 05:03:47'),(139,19,'2014-02-20 05:14:57'),(140,19,'2014-02-20 05:23:00'),(141,19,'2014-02-20 05:38:47'),(142,31,'2014-02-20 12:15:24'),(143,8,'2014-02-24 04:47:04'),(144,25,'2014-02-24 04:53:34'),(145,26,'2014-02-24 04:55:09'),(146,25,'2014-02-24 05:00:14'),(147,26,'2014-02-24 05:29:52'),(148,22,'2014-02-24 05:57:00'),(149,22,'2014-02-24 11:17:44'),(150,18,'2014-02-24 11:20:49'),(151,22,'2014-02-26 06:17:08'),(152,34,'2014-02-27 07:35:18');
/*!40000 ALTER TABLE `candidate_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_match_question_history`
--

DROP TABLE IF EXISTS `candidate_match_question_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_match_question_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `question_match_left_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `given_answer_id` int(11) DEFAULT NULL,
  `is_answer_correct` enum('Y','N') DEFAULT NULL,
  `correct_question_match_right_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_match_question_history`
--

LOCK TABLES `candidate_match_question_history` WRITE;
/*!40000 ALTER TABLE `candidate_match_question_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_match_question_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_question_history`
--

DROP TABLE IF EXISTS `candidate_question_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_question_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `given_answer_id` varchar(200) DEFAULT NULL,
  `is_answer_correct` enum('Y','N') DEFAULT NULL,
  `correct_answer_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `given_answer` text,
  `marks_obtained` float(9,2) DEFAULT NULL,
  `solve_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_question_history`
--

LOCK TABLES `candidate_question_history` WRITE;
/*!40000 ALTER TABLE `candidate_question_history` DISABLE KEYS */;
INSERT INTO `candidate_question_history` VALUES (29,13,31,29,143,1,'321','Y',321,'2014-02-20 12:21:57',NULL,NULL,4),(28,13,31,29,10,1,'34','Y',34,'2014-02-20 12:21:57',NULL,NULL,7),(27,13,31,29,141,1,'312','Y',312,'2014-02-20 12:21:57',NULL,NULL,5),(26,13,31,29,8,1,'26','Y',26,'2014-02-20 12:21:57',NULL,NULL,10),(5,1,19,21,89,5,'',NULL,0,'2014-02-19 08:36:02','',NULL,52),(6,1,19,21,22,5,'71','N',72,'2014-02-19 08:36:02',NULL,NULL,24),(7,1,19,21,13,2,'41','Y',41,'2014-02-19 08:36:02',NULL,NULL,12),(8,1,19,21,11,1,'37','N',36,'2014-02-19 08:36:02',NULL,NULL,13),(9,1,19,21,9,1,'28','N',30,'2014-02-19 08:36:02',NULL,NULL,8),(10,1,19,21,10,1,'33','N',34,'2014-02-19 08:36:02',NULL,NULL,18),(11,1,19,21,8,1,'26','Y',26,'2014-02-19 08:36:02',NULL,NULL,18),(12,5,19,24,8,1,'',NULL,26,'2014-02-20 04:46:27',NULL,NULL,13),(13,5,19,24,10,1,'',NULL,34,'2014-02-20 04:46:27',NULL,NULL,0),(14,5,19,24,9,1,'',NULL,30,'2014-02-20 04:46:27',NULL,NULL,0),(15,5,19,24,11,1,'',NULL,36,'2014-02-20 04:46:27',NULL,NULL,0),(16,5,19,25,8,1,'26','Y',26,'2014-02-20 05:15:47',NULL,NULL,12),(17,5,19,25,10,1,'34','Y',34,'2014-02-20 05:15:47',NULL,NULL,5),(18,5,19,25,11,1,'36','Y',36,'2014-02-20 05:15:47',NULL,NULL,8),(19,5,19,25,9,1,'29','N',30,'2014-02-20 05:15:47',NULL,NULL,11),(20,4,19,26,89,5,'',NULL,0,'2014-02-20 05:25:34','yes ',NULL,16),(21,4,19,26,22,5,'71','N',72,'2014-02-20 05:25:34',NULL,NULL,10),(22,5,19,27,11,1,'36','Y',36,'2014-02-20 05:39:21',NULL,NULL,9),(23,5,19,27,8,1,'26','Y',26,'2014-02-20 05:39:21',NULL,NULL,5),(24,5,19,27,10,1,'34','Y',34,'2014-02-20 05:39:21',NULL,NULL,4),(25,5,19,27,9,1,'30','Y',30,'2014-02-20 05:39:21',NULL,NULL,7),(30,13,31,29,9,1,'30','Y',30,'2014-02-20 12:21:57',NULL,NULL,4),(31,13,31,29,11,1,'36','Y',36,'2014-02-20 12:21:57',NULL,NULL,9),(32,13,31,29,142,1,'316','Y',316,'2014-02-20 12:21:57',NULL,NULL,12),(33,13,31,29,144,1,'325','Y',325,'2014-02-20 12:21:57',NULL,NULL,12),(34,8,8,30,141,1,'312','Y',312,'2014-02-24 04:48:10',NULL,NULL,10),(35,8,8,30,146,1,'330','Y',330,'2014-02-24 04:48:10',NULL,NULL,8),(36,8,8,30,142,1,'316','Y',316,'2014-02-24 04:48:10',NULL,NULL,7),(37,8,8,30,144,1,'325','Y',325,'2014-02-24 04:48:10',NULL,NULL,6),(38,8,8,30,143,1,'321','Y',321,'2014-02-24 04:48:10',NULL,NULL,16),(39,8,8,30,147,1,'333','Y',333,'2014-02-24 04:48:10',NULL,NULL,10),(40,12,26,31,141,1,'312','Y',312,'2014-02-24 04:57:17',NULL,NULL,9),(41,12,26,31,148,1,'',NULL,0,'2014-02-24 04:57:17','management is a process and a control is a protection.',NULL,67),(42,12,26,31,143,1,'321','Y',321,'2014-02-24 04:57:17',NULL,NULL,6),(43,12,26,31,142,1,'316','Y',316,'2014-02-24 04:57:17',NULL,NULL,5),(44,12,26,31,144,1,'325','Y',325,'2014-02-24 04:57:17',NULL,NULL,7),(45,12,25,31,144,1,'325','Y',325,'2014-02-24 05:01:18',NULL,NULL,6),(46,12,25,31,141,1,'312','Y',312,'2014-02-24 05:01:18',NULL,NULL,7),(47,12,25,31,148,1,'',NULL,0,'2014-02-24 05:01:18','management is a process. control is a protection.',NULL,33),(48,12,25,31,143,1,'321','Y',321,'2014-02-24 05:01:18',NULL,NULL,5),(49,12,25,31,142,1,'316','Y',316,'2014-02-24 05:01:18',NULL,NULL,8),(50,11,22,32,149,10,'336','Y',336,'2014-02-24 05:57:23',NULL,NULL,10),(51,11,22,32,151,10,'344','N',345,'2014-02-24 05:57:23',NULL,NULL,7),(52,11,22,32,152,10,'',NULL,349,'2014-02-24 05:57:23',NULL,NULL,0),(53,11,22,32,150,10,'',NULL,341,'2014-02-24 05:57:23',NULL,NULL,0),(54,11,22,32,153,10,'',NULL,355,'2014-02-24 05:57:23',NULL,NULL,0),(55,8,18,33,40,9,'89','N',91,'2014-02-24 11:21:12',NULL,NULL,18);
/*!40000 ALTER TABLE `candidate_question_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_sample_match_question_history`
--

DROP TABLE IF EXISTS `candidate_sample_match_question_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_sample_match_question_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `question_match_left_id` int(11) DEFAULT NULL,
  `given_answer_id` int(11) DEFAULT NULL,
  `is_correct_answer` enum('Y','N') DEFAULT NULL,
  `correct_question_match_right_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_sample_match_question_history`
--

LOCK TABLES `candidate_sample_match_question_history` WRITE;
/*!40000 ALTER TABLE `candidate_sample_match_question_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_sample_match_question_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_sample_question_history`
--

DROP TABLE IF EXISTS `candidate_sample_question_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_sample_question_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `given_answer_id` int(11) DEFAULT NULL,
  `is_answer_correct` enum('Y','N') DEFAULT NULL,
  `correct_answer_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_sample_question_history`
--

LOCK TABLES `candidate_sample_question_history` WRITE;
/*!40000 ALTER TABLE `candidate_sample_question_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_sample_question_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_sample_test_history`
--

DROP TABLE IF EXISTS `candidate_sample_test_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_sample_test_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `marks_obtained` int(11) DEFAULT NULL,
  `percentage` float(9,3) DEFAULT NULL,
  `time_taken` int(11) DEFAULT NULL,
  `result` enum('P','F') DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `level_selected` enum('B','I','H') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_sample_test_history`
--

LOCK TABLES `candidate_sample_test_history` WRITE;
/*!40000 ALTER TABLE `candidate_sample_test_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_sample_test_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_temp_answer`
--

DROP TABLE IF EXISTS `candidate_temp_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_temp_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `given_answer_id` text,
  `test_id` int(11) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `is_review_marked` tinyint(1) DEFAULT '0',
  `is_match_type` tinyint(1) DEFAULT '0',
  `time_remaining` int(10) DEFAULT '0',
  `solve_time` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_temp_answer`
--

LOCK TABLES `candidate_temp_answer` WRITE;
/*!40000 ALTER TABLE `candidate_temp_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_temp_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_temp_answer_homework`
--

DROP TABLE IF EXISTS `candidate_temp_answer_homework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_temp_answer_homework` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `given_answer_id` text,
  `homework_id` int(11) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `is_review_marked` tinyint(1) DEFAULT '0',
  `is_match_type` tinyint(1) DEFAULT '0',
  `time_consumed` int(10) DEFAULT '0',
  `solve_time` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_temp_answer_homework`
--

LOCK TABLES `candidate_temp_answer_homework` WRITE;
/*!40000 ALTER TABLE `candidate_temp_answer_homework` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_temp_answer_homework` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_temp_match_answer`
--

DROP TABLE IF EXISTS `candidate_temp_match_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_temp_match_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question_match_left_id` int(11) DEFAULT NULL,
  `given_answer_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `is_review_marked` tinyint(1) DEFAULT '0',
  `is_match_type` tinyint(1) DEFAULT '0',
  `time_remaining` int(10) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_temp_match_answer`
--

LOCK TABLES `candidate_temp_match_answer` WRITE;
/*!40000 ALTER TABLE `candidate_temp_match_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_temp_match_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_temp_match_answer_homework`
--

DROP TABLE IF EXISTS `candidate_temp_match_answer_homework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_temp_match_answer_homework` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question_match_left_id` int(11) DEFAULT NULL,
  `given_answer_id` int(11) DEFAULT NULL,
  `homework_id` int(11) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `is_review_marked` tinyint(1) DEFAULT '0',
  `is_match_type` tinyint(1) DEFAULT '0',
  `time_remaining` int(10) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_temp_match_answer_homework`
--

LOCK TABLES `candidate_temp_match_answer_homework` WRITE;
/*!40000 ALTER TABLE `candidate_temp_match_answer_homework` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_temp_match_answer_homework` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_test_history`
--

DROP TABLE IF EXISTS `candidate_test_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_test_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `marks_obtained` int(11) DEFAULT '0',
  `percentage` float(9,3) DEFAULT NULL,
  `time_taken` int(11) DEFAULT NULL,
  `result` enum('P','F') DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_test_history`
--

LOCK TABLES `candidate_test_history` WRITE;
/*!40000 ALTER TABLE `candidate_test_history` DISABLE KEYS */;
INSERT INTO `candidate_test_history` VALUES (7,13,29,31,50,100.000,60,'P','2014-02-20 12:21:57'),(2,1,21,19,13,43.330,58,'F','2014-02-19 08:36:03'),(3,5,24,19,0,0.000,60,'F','2014-02-20 04:46:27'),(4,5,25,19,7,70.000,60,'P','2014-02-20 05:15:47'),(5,4,26,19,0,0.000,60,'F','2014-02-20 05:25:34'),(6,5,27,19,10,100.000,60,'P','2014-02-20 05:39:21'),(8,8,30,8,50,100.000,60,'P','2014-02-24 04:48:10'),(9,12,31,26,40,80.000,60,'P','2014-02-24 04:57:17'),(10,12,31,25,40,80.000,60,'P','2014-02-24 05:01:18'),(11,11,32,22,10,20.000,60,'P','2014-02-24 05:57:23'),(12,8,33,18,0,0.000,60,'F','2014-02-24 11:21:12');
/*!40000 ALTER TABLE `candidate_test_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_test_subject_history`
--

DROP TABLE IF EXISTS `candidate_test_subject_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_test_subject_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `marks_obtained` int(11) DEFAULT '0',
  `result` enum('F','P') DEFAULT 'F',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_test_subject_history`
--

LOCK TABLES `candidate_test_subject_history` WRITE;
/*!40000 ALTER TABLE `candidate_test_subject_history` DISABLE KEYS */;
INSERT INTO `candidate_test_subject_history` VALUES (9,13,29,31,1,50,'P'),(2,1,21,19,1,3,'F'),(3,1,21,19,2,10,'P'),(4,1,21,19,5,0,'F'),(5,5,24,19,1,0,'F'),(6,5,25,19,1,7,'P'),(7,4,26,19,5,0,'F'),(8,5,27,19,1,10,'P'),(10,8,30,8,1,50,'P'),(11,12,31,26,1,40,'P'),(12,12,31,25,1,40,'P'),(13,11,32,22,10,10,'P'),(14,8,33,18,9,0,'F');
/*!40000 ALTER TABLE `candidate_test_subject_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `default_configuration`
--

DROP TABLE IF EXISTS `default_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `default_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_variable` varchar(255) DEFAULT NULL,
  `variable_value` varchar(255) DEFAULT NULL,
  `display_label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `default_configuration`
--

LOCK TABLES `default_configuration` WRITE;
/*!40000 ALTER TABLE `default_configuration` DISABLE KEYS */;
INSERT INTO `default_configuration` VALUES (1,'MIN_PASS_MARKS','80','Minimum Passing Marks (in percentage)'),(2,'middle-text','EMath360',NULL),(3,'font-size','40',NULL),(4,'color','545353',NULL),(8,'right-logo','',NULL),(7,'left-logo','',NULL);
/*!40000 ALTER TABLE `default_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_candidate`
--

DROP TABLE IF EXISTS `exam_candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_candidate`
--

LOCK TABLES `exam_candidate` WRITE;
/*!40000 ALTER TABLE `exam_candidate` DISABLE KEYS */;
INSERT INTO `exam_candidate` VALUES (9,1,3),(10,3,3),(11,2,3),(12,4,6),(14,4,7),(32,1,10),(35,4,12),(37,3,13),(75,9,16),(77,8,18),(78,5,1),(79,4,1),(80,1,1),(81,3,1),(82,2,1),(83,8,1),(115,11,20),(116,11,21),(117,11,22),(118,11,11),(119,11,14),(120,9,15),(121,9,17),(122,8,19),(123,8,9),(124,8,8),(125,9,4),(126,9,2),(127,8,23),(128,12,24),(129,12,25),(130,12,26),(131,12,27),(132,12,28),(133,13,29),(134,13,30),(135,13,31),(136,13,32),(137,13,33),(138,5,34);
/*!40000 ALTER TABLE `exam_candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_candidate_teacher`
--

DROP TABLE IF EXISTS `exam_candidate_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_candidate_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_candidate_teacher`
--

LOCK TABLES `exam_candidate_teacher` WRITE;
/*!40000 ALTER TABLE `exam_candidate_teacher` DISABLE KEYS */;
INSERT INTO `exam_candidate_teacher` VALUES (8,4,8,4,'2013-04-29 04:28:27'),(9,4,2,4,'2013-04-29 04:28:28'),(11,1,1,2,'2014-02-13 07:55:45');
/*!40000 ALTER TABLE `exam_candidate_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_subject_teacher`
--

DROP TABLE IF EXISTS `exam_subject_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_subject_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_subject_teacher`
--

LOCK TABLES `exam_subject_teacher` WRITE;
/*!40000 ALTER TABLE `exam_subject_teacher` DISABLE KEYS */;
INSERT INTO `exam_subject_teacher` VALUES (31,4,1,3),(32,4,2,3),(80,2,3,4),(81,2,4,4),(82,3,2,4),(83,4,1,4),(90,1,2,2),(91,2,2,2),(108,13,1,13),(109,12,1,13),(110,8,1,13),(111,9,1,13),(112,11,1,13),(113,13,6,12),(114,8,6,12),(115,9,6,12),(116,13,7,11),(117,12,7,11),(118,8,7,11),(119,9,7,11),(120,11,7,11),(121,13,9,10),(122,12,9,10),(123,8,9,10),(124,11,9,10),(125,13,11,9),(126,12,11,9),(127,9,11,9),(128,11,11,9);
/*!40000 ALTER TABLE `exam_subject_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_subjects`
--

DROP TABLE IF EXISTS `exam_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_min_mark` int(11) DEFAULT NULL COMMENT 'in percentage',
  `subject_max_mark` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_subjects`
--

LOCK TABLES `exam_subjects` WRITE;
/*!40000 ALTER TABLE `exam_subjects` DISABLE KEYS */;
INSERT INTO `exam_subjects` VALUES (6,1,5,60,10),(5,1,2,60,10),(4,1,1,60,10),(74,13,11,20,50),(73,13,1,20,50),(72,12,9,20,50),(71,12,7,20,50),(70,12,11,20,50),(69,12,10,20,50),(68,12,1,20,50),(27,4,1,60,10),(26,4,5,60,10),(25,4,2,60,10),(33,5,10,60,10),(32,5,1,50,10),(58,8,9,60,10),(67,9,11,20,50),(49,11,10,20,50),(48,11,1,20,50),(50,11,9,20,50),(51,11,11,20,50),(52,11,7,20,50),(66,9,10,20,50),(65,9,7,20,50),(64,9,6,20,50),(63,9,1,20,50),(59,8,1,20,50),(60,8,10,20,50),(61,8,6,20,50),(62,8,7,20,50),(75,13,7,20,50),(76,13,9,20,50),(77,13,6,20,50);
/*!40000 ALTER TABLE `exam_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examination`
--

DROP TABLE IF EXISTS `examination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examination`
--

LOCK TABLES `examination` WRITE;
/*!40000 ALTER TABLE `examination` DISABLE KEYS */;
INSERT INTO `examination` VALUES (1,'Exam One'),(12,'class 9 course'),(4,'Course One'),(5,'(p.m.t.)'),(8,'class 8 course'),(9,'class 7 course'),(13,'class 10 course'),(11,'class 6 course');
/*!40000 ALTER TABLE `examination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `feed_exam` text,
  `feed_software` text,
  `is_read` tinyint(1) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (1,8,6,'Test 1','test 2',0,'2013-04-06 12:23:14'),(2,2,12,'nice one','none',0,'2013-04-29 04:57:59'),(3,1,13,'Very logical test.','Equations are looking great.',1,'2013-04-30 11:25:40'),(4,1,17,'Test test','Test exam soft',1,'2013-12-23 12:31:53');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_reply`
--

DROP TABLE IF EXISTS `feedback_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feedback_id` int(11) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_reply`
--

LOCK TABLES `feedback_reply` WRITE;
/*!40000 ALTER TABLE `feedback_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homework`
--

DROP TABLE IF EXISTS `homework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homework` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `homework_name` varchar(50) NOT NULL,
  `homework_date` timestamp NULL DEFAULT NULL,
  `homework_date_end` timestamp NULL DEFAULT NULL,
  `maximum_marks` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enable_homework` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_informed` tinyint(1) DEFAULT '0',
  `informed_count` int(11) DEFAULT '0',
  `informed_date` timestamp NULL DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homework`
--

LOCK TABLES `homework` WRITE;
/*!40000 ALTER TABLE `homework` DISABLE KEYS */;
INSERT INTO `homework` VALUES (1,1,'Homework One','2013-03-19 18:30:00','2013-03-30 18:30:00',10,'2013-03-30 05:06:30','Y',1,1,'2013-03-30 05:37:01',NULL),(2,1,'Homework With Subjective Question','2013-03-30 18:30:00','2013-04-26 18:30:00',30,'2013-04-01 05:16:33','Y',0,0,NULL,NULL),(3,4,'HW 1 Course 1 Admin','2013-04-01 18:30:00','2013-04-04 18:30:00',30,'2013-04-04 05:32:11','N',0,0,NULL,NULL),(4,4,'Testing','2013-04-04 18:30:00','2013-04-09 17:30:00',20,'2013-04-05 08:48:53','Y',1,2,'2013-04-06 11:09:10',NULL),(8,3,'new test','2013-04-08 18:30:00','2013-04-09 17:30:00',10,'2013-04-09 09:20:51','Y',0,0,NULL,NULL),(9,4,'Shakti Testing','2013-04-08 18:30:00','2013-04-09 18:30:00',10,'2013-04-09 12:39:48','Y',0,0,NULL,NULL),(10,4,'New Homework','2013-04-21 18:30:00','2013-04-23 18:30:00',20,'2013-04-23 08:37:39','Y',1,4,'2013-04-23 08:56:10',NULL),(11,4,'test homework','2013-04-28 18:30:00','2013-04-29 08:30:00',10,'2013-04-26 05:29:32','Y',1,2,'2013-04-29 04:40:35',NULL),(12,1,'Test Homework','2013-10-14 18:30:00','2013-10-17 18:30:00',10,'2013-10-16 07:05:37','N',0,0,NULL,2),(13,4,'home 1','2014-02-13 18:30:00','2014-02-27 18:30:00',10,'2014-02-13 07:00:28','N',0,0,NULL,NULL),(14,8,'home 1','2014-02-13 18:30:00','2014-02-19 18:30:00',10,'2014-02-13 07:26:53','Y',0,0,NULL,NULL),(15,1,'iit homework','2014-02-18 18:30:00','2014-02-27 18:30:00',30,'2014-02-19 08:11:48','Y',1,3,'2014-02-19 08:12:34',NULL),(16,5,'home 1','2014-02-18 18:30:00','2014-02-20 18:30:00',10,'2014-02-20 04:51:01','Y',0,0,NULL,NULL),(17,8,'iit homework','2014-02-18 18:30:00','2014-02-20 18:30:00',10,'2014-02-20 04:54:46','Y',0,0,NULL,NULL),(18,5,'home 1','2014-02-19 18:30:00','2014-02-20 18:30:00',10,'2014-02-20 05:13:38','Y',0,0,NULL,NULL),(19,5,'iit homework','2014-02-20 05:37:19','2014-02-20 18:30:00',10,'2014-02-20 05:37:43','Y',0,0,NULL,NULL);
/*!40000 ALTER TABLE `homework` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homework_candidate`
--

DROP TABLE IF EXISTS `homework_candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homework_candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `homework_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homework_candidate`
--

LOCK TABLES `homework_candidate` WRITE;
/*!40000 ALTER TABLE `homework_candidate` DISABLE KEYS */;
INSERT INTO `homework_candidate` VALUES (10,10,2,'2013-04-23 08:40:03'),(11,10,1,'2013-04-23 08:40:03'),(12,10,9,'2013-04-23 08:40:03'),(13,10,8,'2013-04-23 08:40:03'),(16,11,8,'2013-04-29 04:25:48'),(17,11,9,'2013-04-29 04:25:48'),(18,0,1,'2013-10-16 07:01:06'),(19,15,1,'2014-02-19 08:11:48'),(20,15,19,'2014-02-19 08:11:48'),(21,15,3,'2014-02-19 08:11:48'),(22,17,19,'2014-02-20 04:54:47'),(23,18,19,'2014-02-20 05:14:09'),(24,19,19,'2014-02-20 05:38:08');
/*!40000 ALTER TABLE `homework_candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homework_question_selection`
--

DROP TABLE IF EXISTS `homework_question_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homework_question_selection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_level` enum('B','I','H') DEFAULT NULL,
  `question_marks` int(11) DEFAULT NULL,
  `question_total` int(11) DEFAULT NULL,
  `homework_id` int(11) NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homework_question_selection`
--

LOCK TABLES `homework_question_selection` WRITE;
/*!40000 ALTER TABLE `homework_question_selection` DISABLE KEYS */;
INSERT INTO `homework_question_selection` VALUES (1,'B',2,1,1,1),(2,'B',2,1,1,1),(3,'I',3,1,1,1),(4,'I',3,1,1,1),(5,'B',3,1,2,5),(6,'B',1,1,2,5),(7,'B',1,1,2,5),(8,'B',3,1,2,2),(9,'B',2,1,2,2),(10,'I',3,1,2,2),(11,'I',2,1,2,2),(12,'B',2,1,2,1),(13,'B',2,1,2,1),(14,'I',3,1,2,1),(15,'I',3,1,2,1),(37,'H',3,1,3,5),(36,'B',1,1,3,5),(35,'B',1,1,3,5),(34,'H',3,1,3,2),(33,'I',2,1,3,2),(32,'B',2,1,3,2),(31,'B',3,1,3,2),(30,'I',3,1,3,1),(29,'I',3,1,3,1),(28,'B',2,1,3,1),(27,'B',2,1,3,1),(65,'I',3,1,4,1),(64,'B',2,1,4,1),(63,'B',2,1,4,1),(62,'B',5,1,4,5),(61,'B',2,1,4,5),(60,'B',2,1,4,5),(59,'B',3,1,4,5),(58,'B',1,1,4,5),(109,'H',3,1,10,2),(108,'I',2,1,10,2),(107,'I',3,1,10,2),(106,'B',2,1,10,2),(105,'I',3,1,10,1),(104,'I',3,1,10,1),(103,'B',2,1,10,1),(102,'B',2,1,10,1),(117,'H',3,1,11,5),(116,'B',3,1,11,5),(115,'B',2,1,11,5),(114,'B',2,1,11,5),(118,'',0,1,0,0),(119,'B',2,1,13,5),(120,'B',2,1,13,5),(121,'B',3,1,13,5),(122,'H',3,1,13,5),(126,'H',5,1,14,9),(125,'B',5,1,14,9),(134,'I',3,1,16,1),(133,'I',3,1,16,1),(132,'B',2,1,16,1),(131,'B',2,1,16,1),(135,'B',5,1,17,9),(136,'H',5,1,17,9),(144,'I',3,1,19,1),(143,'I',3,1,19,1),(142,'B',2,1,19,1),(141,'B',2,1,19,1);
/*!40000 ALTER TABLE `homework_question_selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homework_questions`
--

DROP TABLE IF EXISTS `homework_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homework_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `homework_id` int(11) NOT NULL,
  `question_selection` enum('R','C') DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=188 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homework_questions`
--

LOCK TABLES `homework_questions` WRITE;
/*!40000 ALTER TABLE `homework_questions` DISABLE KEYS */;
INSERT INTO `homework_questions` VALUES (1,10,1,'R',1),(2,11,1,'R',1),(3,8,1,'R',1),(4,9,1,'R',1),(5,87,2,'R',5),(6,93,2,'R',5),(7,115,2,'R',5),(8,89,2,'C',5),(9,15,2,'R',2),(10,18,2,'R',2),(11,16,2,'R',2),(12,19,2,'R',2),(13,10,2,'R',1),(14,11,2,'R',1),(15,8,2,'R',1),(16,9,2,'R',1),(40,89,3,'C',5),(39,25,3,'R',5),(38,115,3,'R',5),(37,93,3,'R',5),(36,17,3,'R',2),(35,19,3,'R',2),(34,18,3,'R',2),(33,15,3,'R',2),(32,9,3,'R',1),(31,8,3,'R',1),(30,11,3,'R',1),(29,10,3,'R',1),(68,8,4,'R',1),(67,11,4,'R',1),(66,10,4,'R',1),(65,22,4,'R',5),(64,24,4,'R',5),(63,88,4,'R',5),(62,87,4,'R',5),(61,93,4,'R',5),(99,119,8,'C',2),(98,90,8,'C',2),(97,19,8,'C',2),(96,18,8,'C',2),(100,25,9,'C',5),(101,88,9,'C',5),(102,89,9,'C',5),(134,17,10,'R',2),(133,19,10,'R',2),(132,16,10,'R',2),(131,18,10,'R',2),(130,9,10,'R',1),(129,8,10,'R',1),(128,11,10,'R',1),(127,10,10,'R',1),(142,25,11,'R',5),(141,87,11,'R',5),(140,88,11,'R',5),(139,24,11,'R',5),(143,0,0,'R',0),(144,0,0,'C',0),(145,24,13,'R',5),(146,88,13,'R',5),(147,87,13,'R',5),(148,25,13,'R',5),(152,82,14,'R',9),(151,134,14,'R',9),(153,8,15,'C',1),(154,9,15,'C',1),(155,10,15,'C',1),(156,11,15,'C',1),(157,15,15,'C',2),(158,16,15,'C',2),(159,79,15,'C',2),(160,22,15,'C',5),(161,26,15,'C',5),(169,9,16,'R',1),(168,8,16,'R',1),(167,11,16,'R',1),(166,10,16,'R',1),(170,134,17,'R',9),(171,82,17,'R',9),(179,11,18,'C',1),(178,10,18,'C',1),(177,9,18,'C',1),(176,8,18,'C',1),(187,9,19,'R',1),(186,8,19,'R',1),(185,11,19,'R',1),(184,10,19,'R',1);
/*!40000 ALTER TABLE `homework_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homework_subject`
--

DROP TABLE IF EXISTS `homework_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homework_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `homework_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_total_marks` int(11) DEFAULT NULL,
  `question_selection` varchar(20) DEFAULT NULL,
  `show_subject_diff_question` enum('Y','N') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homework_subject`
--

LOCK TABLES `homework_subject` WRITE;
/*!40000 ALTER TABLE `homework_subject` DISABLE KEYS */;
INSERT INTO `homework_subject` VALUES (1,1,1,1,10,'R','N'),(2,1,2,5,10,'R,C','N'),(3,1,2,2,10,'R','Y'),(4,1,2,1,10,'R','N'),(10,4,3,5,10,'R,C','N'),(9,4,3,2,10,'R','N'),(8,4,3,1,10,'R','N'),(18,4,4,1,10,'R','Y'),(17,4,4,5,10,'R','N'),(34,3,8,2,10,'C','N'),(35,4,9,5,10,'C','N'),(43,4,10,2,10,'R','N'),(42,4,10,1,10,'R','N'),(45,4,11,5,10,'R','N'),(46,4,13,5,10,'R','N'),(48,8,14,9,10,'R','N'),(49,1,15,1,10,'C','N'),(50,1,15,2,10,'C','N'),(51,1,15,5,10,'C','N'),(53,5,16,1,10,'R','N'),(54,8,17,9,10,'R','N'),(56,5,18,1,10,'C','N'),(58,5,19,1,10,'R','N');
/*!40000 ALTER TABLE `homework_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `moduleID` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `moduleName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`moduleID`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` VALUES (1,'dashboard'),(2,'examination'),(3,'test_master'),(4,'question_master'),(7,'subject_master'),(9,'report'),(10,'candidate_master'),(11,'backup'),(12,'role'),(13,'preferences'),(14,'feedback'),(15,'users'),(16,'paper'),(18,'mailer'),(19,'homework'),(20,'teacher'),(21,'parent');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paper`
--

DROP TABLE IF EXISTS `paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_title` varchar(100) NOT NULL,
  `series` varchar(50) DEFAULT NULL,
  `description` text,
  `maximum_marks` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paper`
--

LOCK TABLES `paper` WRITE;
/*!40000 ALTER TABLE `paper` DISABLE KEYS */;
INSERT INTO `paper` VALUES (1,'Paper One For Exam One','P1E1','',30,'2013-03-29 09:37:35'),(2,'ib 2013','abc','tytr',25,'2013-04-06 06:28:29'),(3,'Paper PT','','',26,'2013-05-20 04:57:54'),(4,'ib 2013','FGTY','',20,'2014-02-13 07:25:38'),(5,'ib 2013','abc','xyz',30,'2014-02-19 12:12:01'),(6,'ib 2013','xyz','',30,'2014-02-20 05:05:03'),(7,'ib 2013','bb','',10,'2014-02-25 05:29:26');
/*!40000 ALTER TABLE `paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paper_question_selection`
--

DROP TABLE IF EXISTS `paper_question_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper_question_selection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_level` enum('B','I','H') DEFAULT NULL,
  `question_marks` int(11) DEFAULT NULL,
  `question_total` int(11) DEFAULT NULL,
  `paper_id` int(11) DEFAULT '0',
  `subject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paper_question_selection`
--

LOCK TABLES `paper_question_selection` WRITE;
/*!40000 ALTER TABLE `paper_question_selection` DISABLE KEYS */;
INSERT INTO `paper_question_selection` VALUES (20,'H',3,1,1,5),(19,'B',1,2,1,5),(18,'B',3,1,1,5),(17,'B',2,1,1,5),(16,'H',3,1,1,2),(15,'I',2,1,1,2),(14,'B',2,1,1,2),(13,'B',3,1,1,2),(12,'I',3,2,1,1),(11,'B',2,2,1,1),(26,'B',3,3,2,9),(27,'B',2,1,2,9),(28,'B',5,1,2,9),(29,'H',4,1,2,9),(30,'H',5,1,2,9),(31,'B',5,2,4,5),(32,'B',10,1,4,5),(33,'B',5,2,5,5),(34,'B',2,1,5,5),(35,'B',10,1,5,5),(36,'H',3,1,5,5),(37,'H',5,1,5,5),(38,'B',5,2,6,5),(39,'B',2,1,6,5),(40,'B',10,1,6,5),(41,'H',3,1,6,5),(42,'H',5,1,6,5),(43,'B',10,1,7,2);
/*!40000 ALTER TABLE `paper_question_selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paper_questions`
--

DROP TABLE IF EXISTS `paper_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `paper_id` int(11) DEFAULT NULL,
  `question_selection` enum('R','C') DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paper_questions`
--

LOCK TABLES `paper_questions` WRITE;
/*!40000 ALTER TABLE `paper_questions` DISABLE KEYS */;
INSERT INTO `paper_questions` VALUES (14,10,1,'R',1),(15,11,1,'R',1),(16,8,1,'R',1),(17,9,1,'R',1),(18,15,1,'R',2),(19,18,1,'R',2),(20,19,1,'R',2),(21,17,1,'R',2),(22,24,1,'R',5),(23,87,1,'R',5),(24,93,1,'R',5),(25,115,1,'R',5),(26,25,1,'R',5),(31,1,3,'C',7),(32,2,3,'C',7),(33,3,3,'C',7),(34,4,3,'C',7),(35,5,3,'C',7),(36,6,3,'C',7),(37,7,3,'C',7),(38,12,3,'C',7),(39,92,3,'C',7),(41,40,2,'R',9),(42,132,2,'R',9),(43,133,2,'R',9),(44,135,2,'R',9),(45,134,2,'R',9),(46,75,2,'R',9),(47,82,2,'R',9),(48,22,4,'R',5),(49,89,4,'R',5),(50,116,4,'R',5),(51,22,5,'R',5),(52,89,5,'R',5),(53,24,5,'R',5),(54,116,5,'R',5),(55,25,5,'R',5),(56,26,5,'R',5),(57,22,6,'R',5),(58,89,6,'R',5),(59,24,6,'R',5),(60,116,6,'R',5),(61,25,6,'R',5),(62,26,6,'R',5),(63,13,7,'R',2);
/*!40000 ALTER TABLE `paper_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paper_subject`
--

DROP TABLE IF EXISTS `paper_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_total_marks` int(11) DEFAULT NULL,
  `question_selection` varchar(20) DEFAULT NULL,
  `show_subject_diff_question` enum('Y','N') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paper_subject`
--

LOCK TABLES `paper_subject` WRITE;
/*!40000 ALTER TABLE `paper_subject` DISABLE KEYS */;
INSERT INTO `paper_subject` VALUES (4,1,1,10,'R','N'),(5,1,2,10,'R','N'),(6,1,5,10,'R','N'),(11,3,7,26,'C','N'),(13,2,9,25,'R','N'),(14,4,5,20,'R','N'),(15,5,5,30,'R','N'),(16,6,5,30,'R','N'),(17,7,2,10,'R','N');
/*!40000 ALTER TABLE `paper_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parent`
--

DROP TABLE IF EXISTS `parent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `address` text,
  `email` varchar(150) DEFAULT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pro_image` varchar(255) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parent`
--

LOCK TABLES `parent` WRITE;
/*!40000 ALTER TABLE `parent` DISABLE KEYS */;
INSERT INTO `parent` VALUES (2,'Ashwini','Agarwal','','ashwini.agarwal@sunarctechnologies.com','94601776008','parent.agarwal.ashwini','seldpJxxtvI7E','2013-03-28 13:04:53',NULL,0),(3,'Vedprakash','Saini','','ved.saini@gmail.com','','ved.saini','se6U4WYN/JvzE','2013-04-04 05:41:42',NULL,0),(4,'rahul','sharma','Vyas colony','rahul@xyz.com','01234567891','rahul','sec1K4SAJL3mQ','2013-04-05 06:59:42',NULL,0),(6,'Shakti','Singh','','shaktsdfshflkjs@fdjksflhjk.com','','test','setZX1h.wi.dk','2013-04-09 12:46:54',NULL,0),(7,'ROHIT','vyas','','rohit@gmail.com','','ROHIT','seyg4aaQ.FM4Y','2014-02-19 08:15:37',NULL,0),(8,'brijesh','joshi','','brijesh@gmail.com','','007','se9Z4K6FE0/jA','2014-02-24 04:28:56',NULL,0);
/*!40000 ALTER TABLE `parent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parent_candidate`
--

DROP TABLE IF EXISTS `parent_candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parent_candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `is_approved` tinyint(1) DEFAULT '1',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parent_candidate`
--

LOCK TABLES `parent_candidate` WRITE;
/*!40000 ALTER TABLE `parent_candidate` DISABLE KEYS */;
INSERT INTO `parent_candidate` VALUES (87,6,21,1,'2014-02-20 12:19:59'),(88,6,4,1,'2014-02-20 12:19:59'),(89,6,8,1,'2014-02-20 12:19:59'),(90,4,11,1,'2014-02-20 12:20:34'),(91,4,8,1,'2014-02-20 12:20:34'),(92,4,31,1,'2014-02-20 12:20:34'),(93,3,9,1,'2014-02-24 04:23:50'),(94,3,4,1,'2014-02-24 04:23:50'),(95,3,30,1,'2014-02-24 04:23:50'),(96,3,32,1,'2014-02-24 04:23:50'),(105,8,32,1,'2014-02-24 04:30:41'),(106,8,27,1,'2014-02-24 04:30:41'),(107,8,28,1,'2014-02-24 04:30:41'),(108,8,18,1,'2014-02-24 04:30:41'),(109,2,23,1,'2014-02-24 05:26:29'),(110,2,29,1,'2014-02-24 05:26:29'),(111,2,24,1,'2014-02-24 05:26:29'),(112,2,33,1,'2014-02-24 05:26:29'),(113,2,26,1,'2014-02-24 05:26:29'),(114,7,19,1,'2014-02-24 05:29:31'),(115,7,20,1,'2014-02-24 05:29:31'),(116,7,15,1,'2014-02-24 05:29:31'),(117,7,26,1,'2014-02-24 05:29:31');
/*!40000 ALTER TABLE `parent_candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `permissionID` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `permissionName` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`permissionID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (1,'R'),(2,'A'),(3,'E'),(4,'ED');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_title` text,
  `question_level` enum('B','I','H') DEFAULT NULL,
  `question_type` enum('T','M','F','I','S','MT') DEFAULT NULL,
  `is_sample` enum('T','F') DEFAULT 'T',
  `marks` int(2) DEFAULT '0',
  `discussion` varchar(255) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `hints` text,
  `image` varchar(500) DEFAULT NULL,
  `video` varchar(500) DEFAULT NULL,
  `min_time_to_solve` int(11) DEFAULT NULL,
  `max_time_to_solve` int(11) DEFAULT NULL,
  `experience_level` int(11) DEFAULT NULL,
  `is_group` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=157 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'Find output of the following code.\r\n\r\n    $x = array(&quot;aaa&quot;, &quot;ttt&quot;, &quot;www&quot;, &quot;ttt&quot;, &quot;yyy&quot;, &quot;tttt&quot;); \r\n    $y = array_count_values($x); \r\n    echo $y[\'ttt\'];','I','M','T',3,'',7,'unique values',NULL,NULL,0,0,0,0),(2,'How do you get information from a form that is submitted using the &quot;get&quot; method?','B','M','T',2,'',7,'get',NULL,NULL,NULL,NULL,NULL,0),(3,'What\'s the best way to copy a file from within a piece of PHP?','I','M','T',4,'',7,'copy',NULL,NULL,NULL,NULL,NULL,0),(4,'PHP code is embedded directly into XHTML document?','I','T','T',3,'',7,'XHTML',NULL,NULL,NULL,NULL,NULL,0),(5,'Is it possible to submit a form with out a submit button?','B','T','T',2,'',7,'ajax',NULL,NULL,NULL,NULL,NULL,0),(6,'Full form of PHP','B','M','T',2,'',7,'php',NULL,NULL,NULL,NULL,NULL,0),(7,'What is the expansion of LAMP?','B','T','T',2,'',7,'lamp',NULL,NULL,NULL,NULL,NULL,0),(8,'DIVA:OPERA','I','M','T',3,'',1,'hiny',NULL,NULL,NULL,NULL,NULL,0),(9,'GRAIN:SALT','I','M','T',3,'',1,'hint',NULL,NULL,NULL,NULL,NULL,0),(10,'THRUST:SPEAR','B','M','T',2,'',1,'hint',NULL,NULL,NULL,NULL,NULL,0),(11,'PAIN:SEDATIVE','B','M','T',2,'',1,'hint',NULL,NULL,NULL,NULL,NULL,0),(12,'Match the following.','I','MT','T',5,'',7,'match it.',NULL,NULL,NULL,NULL,NULL,0),(13,'What function is used to release the allocated memory space?','B','M','T',10,'',2,'hint',NULL,NULL,NULL,NULL,NULL,0),(15,'Inline functions are invoked at the time of','B','M','T',3,'xyz',2,'Hints',NULL,NULL,NULL,NULL,NULL,0),(16,'Which of the following operators below allow to define the member functions of a class outside the class?','I','M','T',3,'xyz',2,'Hints',NULL,NULL,NULL,NULL,NULL,0),(17,'Every class has at least one constructor function, even when none is declared.','H','T','T',3,'xyz',2,'Hints',NULL,NULL,NULL,NULL,NULL,0),(18,'How do we define a destructor?','B','M','T',2,'xyz',2,'Hints',NULL,NULL,NULL,NULL,NULL,0),(19,'Can constructors be overloaded?','I','T','T',2,'xyz',2,'Hints',NULL,NULL,NULL,NULL,NULL,0),(21,'&amp; operator is suitable for checking whether a particular bit is on or off.','I','T','T',4,'',6,'xyz',NULL,NULL,NULL,NULL,NULL,0),(22,'Find the next image?','B','I','T',5,'',5,'xyz',NULL,NULL,NULL,NULL,NULL,0),(23,'write some points on array?','H','S','T',5,'xyz',6,'',NULL,NULL,NULL,NULL,NULL,0),(24,'find the correct match?','B','MT','T',2,'',5,'xyz',NULL,NULL,15,30,NULL,0),(25,'find right one?','H','MT','T',3,'',5,'xyz',NULL,NULL,NULL,NULL,NULL,0),(26,'Select right one?','H','MT','T',5,'',5,'xyz',NULL,NULL,NULL,NULL,NULL,0),(40,'<p>Find output. (5^.5)^2=?</p>\r\n','B','M','T',10,'xyz',9,'Hint',NULL,NULL,0,0,0,0),(75,'Find the correct match from list','H','MT','T',4,'',9,'xyz',NULL,'',0,0,0,0),(79,'External, Internal and None are the types of linkages','B','T','T',4,'',2,'xyz',NULL,'',NULL,NULL,NULL,0),(80,'Which of the following special symbol allowed in a variable name?','I','M','T',2,'',7,'xyz',NULL,'',NULL,NULL,NULL,0),(82,'find the next picture','H','I','T',5,'',9,'xyz',NULL,'',NULL,NULL,NULL,0),(87,'This question has an image hint and a video hint.','B','M','T',3,'',5,'','json1349525880.gif','image_scroll1358413892.flv',0,0,0,0),(88,'This question has multiple answers.','B','M','T',2,'',5,'',NULL,NULL,NULL,NULL,NULL,0),(89,'This is a subjective type question.','B','S','T',5,'',5,'',NULL,NULL,NULL,NULL,NULL,0),(90,'What will happen if in a C program you assign a value to an array element whose subscript exceeds the size of array?','I','M','T',3,'',2,'xyz',NULL,'',NULL,NULL,NULL,0),(91,'In C, if you pass an array as an argument to a function, what actually gets passed?','I','M','T',2,'',7,'xyz',NULL,'',NULL,NULL,NULL,0),(92,'A pointer to a block of memory is effectively same as an array','B','T','T',3,'',7,'xyz',NULL,'',NULL,NULL,NULL,0),(93,'Time Tester Question.','B','M','T',1,'',5,'',NULL,NULL,306,428,NULL,0),(115,'Godawan is the state bird of rajasthan?','B','T','T',1,'',5,'',NULL,NULL,0,0,2,0),(116,'<p>This&nbsp;question&nbsp;was&nbsp;using&nbsp;markup.&nbsp;But&nbsp;changed&nbsp;because&nbsp;of&nbsp;equation&nbsp;editor&nbsp;introduction.</p>\r\n','B','M','T',10,'',5,'Apply trick rather than apply direct mathematics.',NULL,NULL,610,1220,0,0),(135,'<p>Solve the following equation.</p>\r\n\r\n<p><img align=\"absmiddle\" alt=\"x + y - z = 5;\" src=\"http://latex.codecogs.com/gif.latex?x&amp;space;+&amp;space;y&amp;space;-&amp;space;z&amp;space;=&amp;space;5;\" /></p>\r\n\r\n<p><img align=\"absmiddle\" alt=\"2x + 2y + 4z = 4;\" src=\"http://latex.codecogs.com/gif.latex?2x&amp;space;+&amp;space;2y&amp;space;+&amp;space;4z&amp;space;=&amp;space;4;\" /></p>\r\n\r\n<p><img align=\"absmiddle\" alt=\"x + y + z = ?;\" src=\"http://latex.codecogs.com/gif.latex?x&amp;space;+&amp;space;y&amp;space;+&amp;space;z&amp;space;=&amp;space;?;\" /></p>\r\n','B','M','T',2,'',9,'',NULL,NULL,0,0,0,0),(132,'<p>(5^.5)^2=?</p>\r\n','B','M','T',10,'xyz',9,'Hint',NULL,'',0,0,0,0),(119,'<p><i style=\"font-size: 13px;\">David would like to compare two phone plans from verizon using a graph of a system of linear equations. Plan A includes 450 minutes for $40.00 per month plus a charge of .45 per minute over 450. Plan B includes 900 minutes for $60.00 per month plus a charge of .40 per minute over 900. Solve for a user who averages 1250 minutes per month.</i></p>\r\n','B','T','T',3,'',2,'',NULL,NULL,0,0,0,0),(133,'<p>This&nbsp;is&nbsp;a&nbsp;test&nbsp;question&nbsp;containing&nbsp;maths&nbsp;equation.</p>\r\n\r\n<p><img align=\"absmiddle\" alt=\"x^{2} + y^{2} = 0\" src=\"http://latex.codecogs.com/gif.latex?x^{2}&amp;space;+&amp;space;y^{2}&amp;space;=&amp;space;0\" /></p>\r\n\r\n<p><img align=\"absmiddle\" alt=\"x^{2} + z^{2} = 0\" src=\"http://latex.codecogs.com/gif.latex?x^{2}&amp;space;+&amp;space;z^{2}&amp;space;=&amp;space;0\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n','B','T','T',3,'',9,'',NULL,NULL,0,0,0,0),(134,'<p><img align=\"absmiddle\" alt=\"\\sum \\frac{a}{b} = \\alpha\" src=\"http://latex.codecogs.com/gif.latex?\\sum&amp;space;\\frac{a}{b}&amp;space;=&amp;space;\\alpha\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img align=\"absmiddle\" alt=\"\\int_{a}^{b}x + \\int y = z\" src=\"http://latex.codecogs.com/gif.latex?\\int_{a}^{b}x&amp;space;+&amp;space;\\int&amp;space;y&amp;space;=&amp;space;z\" /></p>\r\n','B','T','T',5,'',9,'',NULL,NULL,0,0,0,0),(137,'<p>is computer is a device ?</p>\r\n','B','T','T',5,'',6,'xyz',NULL,NULL,NULL,NULL,0,0),(138,'2+2=4','B','T','T',1,'',9,'hint',NULL,'',NULL,NULL,NULL,0),(139,'<p>what is social allocation of apptitude result ?</p>\r\n','B','S','T',5,'',5,'',NULL,NULL,NULL,NULL,0,0),(140,'<p>what are the devices ?</p>\r\n','B','S','T',2,'xyz',5,'',NULL,NULL,NULL,NULL,0,0),(141,'<p>thin : ?</p>\r\n','B','M','T',10,'',1,'xyz',NULL,NULL,0,0,0,0),(142,'<p>rest: ?</p>\r\n','B','M','T',10,'',1,'',NULL,NULL,NULL,NULL,0,0),(143,'<p>bridgee</p>\r\n','B','T','T',10,'',1,'',NULL,NULL,NULL,NULL,0,0),(144,'<p>local: ?</p>\r\n','B','M','T',10,'',1,'',NULL,NULL,NULL,NULL,0,0),(145,'6-5=1','B','T','T',1,'',9,'Hint',NULL,'',NULL,NULL,NULL,0),(146,'<p>black : ?</p>\r\n','B','M','T',5,'',1,'',NULL,NULL,NULL,NULL,0,0),(147,'<p>happy : ?</p>\r\n','B','M','T',5,'',1,'',NULL,NULL,NULL,NULL,0,0),(148,'<p>what are the diffrance between management and control ?</p>\r\n','B','S','T',10,'management is a process. control is a protection.',1,'',NULL,NULL,0,0,0,0),(149,'<p><span style=\"color: rgb(0, 0, 0); font-family: Verdana, Tahoma, Arial, sans-serif; line-height: 17px;\">Grand Central Terminal, Park Avenue, New York is the world&#39;s ?</span></p>\r\n','B','M','T',10,'',10,'',NULL,NULL,NULL,NULL,0,0),(150,'<p><span style=\"color: rgb(0, 0, 0); font-family: Verdana, Tahoma, Arial, sans-serif; line-height: 17px;\">Eritrea, which became the 182nd member of the UN in 1993, is in the continent of..</span></p>\r\n','B','M','T',10,'',10,'',NULL,NULL,NULL,NULL,0,0),(151,'<p><span style=\"color: rgb(0, 0, 0); font-family: Verdana, Tahoma, Arial, sans-serif; line-height: 17px;\">Garampani sanctuary is located at ?</span></p>\r\n','B','M','T',10,'',10,'',NULL,NULL,NULL,NULL,0,0),(152,'<p><span style=\"color: rgb(0, 0, 0); font-family: Verdana, Tahoma, Arial, sans-serif; line-height: 17px;\">Hitler party which came into power in 1933 is known as</span></p>\r\n','B','M','T',10,'',10,'',NULL,NULL,NULL,NULL,0,0),(153,'<p><span style=\"color: rgb(0, 0, 0); font-family: Verdana, Tahoma, Arial, sans-serif; line-height: 17px;\">For which of the following disciplines is Nobel Prize awarded?</span></p>\r\n','B','M','T',10,'',10,'',NULL,NULL,NULL,NULL,0,0),(154,'<p><span style=\"color: rgb(0, 0, 0); font-family: Verdana, Tahoma, Arial, sans-serif; line-height: 17px;\">A person crosses a 600 m long street in 5 minutes. What is his speed in km per hour?</span></p>\r\n','B','M','T',10,'',9,'',NULL,NULL,NULL,NULL,0,0),(155,'<p><span style=\"color: rgb(0, 0, 0); font-family: Verdana, Tahoma, Arial, sans-serif; line-height: 17px;\">If a person walks at 14 km/hr instead of 10 km/hr, he would have walked 20 km more. The actual distance travelled by him is:</span></p>\r\n','B','M','T',10,'',9,'',NULL,NULL,NULL,NULL,0,0),(156,'<p><span style=\"color: rgb(0, 0, 0); font-family: Verdana, Tahoma, Arial, sans-serif; line-height: 17px;\">Excluding stoppages, the speed of a bus is 54 kmph and including stoppages, it is 45 kmph. For how many minutes does the bus stop per hour?</span></p>\r\n','B','M','T',10,'',9,'',NULL,NULL,NULL,NULL,0,0);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_answer`
--

DROP TABLE IF EXISTS `question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=228 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_answer`
--

LOCK TABLES `question_answer` WRITE;
/*!40000 ALTER TABLE `question_answer` DISABLE KEYS */;
INSERT INTO `question_answer` VALUES (152,1,1),(2,2,5),(3,3,11),(4,4,13),(5,5,15),(6,6,18),(7,7,22),(8,8,26),(9,9,30),(10,10,34),(11,11,36),(12,13,41),(14,15,48),(15,16,51),(16,17,55),(17,18,60),(18,19,62),(20,21,67),(21,22,72),(227,40,91),(66,79,119),(42,80,123),(77,88,153),(78,90,156),(156,87,148),(76,88,152),(64,82,130),(79,91,160),(81,92,162),(85,93,166),(108,115,252),(194,116,255),(205,119,263),(226,132,296),(203,133,298),(199,134,300),(206,135,304),(208,137,308),(209,138,310),(211,141,312),(212,142,316),(213,143,321),(214,144,325),(215,145,326),(216,146,330),(217,147,333),(218,149,336),(219,150,341),(220,151,345),(221,152,349),(222,153,355),(223,154,357),(224,155,360),(225,156,365);
/*!40000 ALTER TABLE `question_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_image`
--

DROP TABLE IF EXISTS `question_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_image` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question_id` int(10) NOT NULL,
  `image_path` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_image`
--

LOCK TABLES `question_image` WRITE;
/*!40000 ALTER TABLE `question_image` DISABLE KEYS */;
INSERT INTO `question_image` VALUES (1,22,'11344505775.jpg'),(2,22,'21344505789.jpg'),(3,22,'31344505931.jpg'),(4,22,'41344505825.jpg'),(5,22,'51344505827.jpg'),(9,82,'1344936477502a1a1d49bde.jpg'),(10,82,'1344936477502a1a1d52740.jpg'),(11,82,'1344936477502a1a1d5c5ca.jpg'),(12,123,'11365231443.jpg'),(13,123,'21365231446.jpg'),(14,123,'31365231450.jpg');
/*!40000 ALTER TABLE `question_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_match_left`
--

DROP TABLE IF EXISTS `question_match_left`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_match_left` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question_id` int(10) NOT NULL,
  `value` varchar(500) NOT NULL,
  `answer_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_match_left`
--

LOCK TABLES `question_match_left` WRITE;
/*!40000 ALTER TABLE `question_match_left` DISABLE KEYS */;
INSERT INTO `question_match_left` VALUES (3,12,'PHP',3),(4,12,'JavaScript',4),(10,25,'1+1',14),(11,25,'1+3',16),(17,26,'5/5',26),(18,26,'10/5',27),(19,69,'A',29),(20,69,'B',30),(21,69,'C',31),(22,69,'D',32),(23,70,'A',33),(24,70,'B',34),(25,70,'C',35),(26,70,'D',36),(27,71,'A',38),(28,71,'B',39),(29,71,'C',40),(30,71,'D',41),(31,72,'1+1',43),(32,72,'1+2',44),(33,73,'1+1.1+2',45),(34,73,'1',46),(38,74,'1+1',51),(39,74,'1+2',52),(43,24,'2*2',57),(44,24,'3*3',58),(45,75,'1+1',60),(46,75,'1+2',61),(47,75,'1+4',63),(48,123,'1',64),(49,123,'2',65),(50,123,'3',66),(51,126,'1',67),(52,126,'2',68),(53,126,'3',69),(54,126,'4',70);
/*!40000 ALTER TABLE `question_match_left` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_match_right`
--

DROP TABLE IF EXISTS `question_match_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_match_right` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question_id` int(10) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_match_right`
--

LOCK TABLES `question_match_right` WRITE;
/*!40000 ALTER TABLE `question_match_right` DISABLE KEYS */;
INSERT INTO `question_match_right` VALUES (3,12,'Sever Side Language'),(4,12,'Client Side Language'),(14,25,'2'),(15,25,'3'),(16,25,'4'),(26,26,'1'),(27,26,'2'),(28,26,'3'),(29,69,'Apple'),(30,69,'Boy'),(31,69,'Cat'),(32,69,'Dog'),(33,70,'Apple'),(34,70,'Boy'),(35,70,'Cat'),(36,70,'Dog'),(37,70,'Elephant'),(38,71,'Apple'),(39,71,'Boy'),(40,71,'Cat'),(41,71,'Dog'),(42,71,'Elephant'),(43,72,'2'),(44,72,'3'),(45,73,'2'),(46,73,'3'),(51,74,'2'),(52,74,'3'),(57,24,'4'),(58,24,'8'),(59,24,'9'),(60,75,'2'),(61,75,'3'),(62,75,'4'),(63,75,'5'),(64,123,'2'),(65,123,'3'),(66,123,'1'),(67,126,'3'),(68,126,'2'),(69,126,'1'),(70,126,'4');
/*!40000 ALTER TABLE `question_match_right` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `roleID` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `roleName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Administrator'),(2,'Teacher'),(3,'Parent');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolepermission`
--

DROP TABLE IF EXISTS `rolepermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolepermission` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `roleID` int(11) DEFAULT NULL,
  `permissionID` int(11) DEFAULT NULL,
  `moduleID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=678 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolepermission`
--

LOCK TABLES `rolepermission` WRITE;
/*!40000 ALTER TABLE `rolepermission` DISABLE KEYS */;
INSERT INTO `rolepermission` VALUES (242,1,3,16),(241,1,2,16),(240,1,1,16),(239,1,3,15),(238,1,2,15),(237,1,1,15),(236,1,1,14),(235,1,3,13),(234,1,2,13),(233,1,1,13),(232,1,3,12),(231,1,2,12),(230,1,1,12),(229,1,4,11),(228,1,3,11),(227,1,2,11),(226,1,1,11),(225,1,3,10),(224,1,2,10),(223,1,1,10),(222,1,4,9),(221,1,3,9),(220,1,2,9),(219,1,1,9),(218,1,3,7),(217,1,2,7),(216,1,1,7),(215,1,3,5),(214,1,2,5),(213,1,1,5),(212,1,4,4),(211,1,3,4),(210,1,2,4),(209,1,1,4),(208,1,3,3),(207,1,2,3),(206,1,1,3),(205,1,3,2),(204,1,2,2),(203,1,1,2),(202,1,4,1),(201,1,3,1),(200,1,2,1),(199,1,1,1),(243,1,1,18),(600,3,1,10),(599,3,1,9),(677,2,3,19),(676,2,2,19),(675,2,1,19),(674,2,1,10),(673,2,1,9),(672,2,3,4),(671,2,2,4),(670,2,1,4),(669,2,3,3),(668,2,2,3),(667,2,1,3),(666,2,1,2);
/*!40000 ALTER TABLE `rolepermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_title` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Andaman & Nicobar'),(2,'Andhra Pradesh'),(3,'Arunachal Pradesh'),(4,'Assam'),(5,'Bihar'),(6,'Chandigarh'),(7,'Chattisgarh'),(8,'Delhi'),(9,'Goa'),(10,'Gujarat'),(11,'Haryana'),(12,'Himachal Pradesh'),(13,'Jammu and Kashmir'),(14,'Jharkhand'),(15,'Karnataka'),(16,'Kerala'),(17,'Lakshadweep'),(18,'Madhya Pradesh'),(19,'Maharashtra'),(20,'Manipur'),(21,'Meghalaya'),(22,'Mizoram'),(23,'Nagaland'),(24,'Orissa'),(25,'Puducherry'),(26,'Punjab'),(27,'Rajasthan'),(28,'Sikkim'),(29,'Tamilnadu'),(30,'Tripura'),(31,'Uttar Pradesh'),(32,'Uttarakhand'),(33,'West Bengal'),(34,'up'),(35,'asdasd'),(36,'S');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (1,'English'),(2,'social sceince'),(16,'english grammer'),(5,'Aptitude'),(6,'Computer'),(7,'history'),(9,'Mathematics'),(10,'General science'),(11,'geography'),(15,'pmt');
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `address` text,
  `email` varchar(150) DEFAULT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pro_image` varchar(255) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` VALUES (2,'Ashwini','Agarwal','','ashwini.agarwal@sunarctechnologies.com','94601776008','teacher.agarwal.ashwini','seWfr4VdeLxO.','2013-03-28 12:02:23',NULL,0),(3,'Rupendra Singh','Rathore','','rupendra.singhrathore@gmail.com','','rupendra.singh','se2EGKELEJGe6','2013-04-04 05:35:41','blank1365053741.jpg',0),(4,'Dinesh','Swami','central avenue','xyz@gmail.com','+91-151-1234567','dinesh','seWfr4VdeLxO.','2013-04-05 06:36:02','apple1365143869.jpg',0),(13,'sumit','gupta','','sumit@gmail.com','','sumit','seFPoYbHxm33g','2014-02-20 11:32:05',NULL,0),(9,'Harsit','Mehta','','hardeep@sunarc.com','','harsit','sep4PLQDYmSxY','2013-04-19 06:43:18',NULL,0),(10,'vinay','vyas','','vinay@gmail.com','','vinay','se8chx.ZzOZOM','2013-04-26 05:33:05',NULL,0),(11,'rajiv','soni','','rajiv@gmail.com','','rajiv','seH.XAqOxhYvU','2014-02-13 06:02:46',NULL,0),(12,'chirag','vyas','','chirag@gmail.com','','chirag','sezJtCsFtN.7.','2014-02-19 08:42:24',NULL,0);
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `test_name` varchar(25) DEFAULT NULL,
  `test_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `test_date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_duration` int(11) DEFAULT NULL,
  `maximum_marks` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enable_test` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_archive` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_informed` tinyint(1) DEFAULT '0',
  `informed_count` int(11) DEFAULT '0',
  `informed_date` timestamp NULL DEFAULT NULL,
  `question_media` varchar(50) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (1,1,'Test One','2013-03-22 05:15:00','2013-05-16 09:19:00',60,30,'2013-03-29 09:35:43','Y','N',0,0,NULL,'subject',NULL),(2,1,'Test Two Exam One','2013-03-28 18:30:00','2013-04-26 18:30:00',60,30,'2013-04-01 05:06:06','Y','N',0,0,NULL,'subject',NULL),(6,4,'IB 2013','2013-04-05 18:30:00','2013-04-06 17:30:00',1,20,'2013-04-06 06:14:03','N','N',0,0,NULL,'subject',NULL),(8,1,'Test Three Exam One','2013-04-06 18:30:00','2013-04-09 18:30:00',60,30,'2013-04-08 10:57:09','Y','N',1,2,'2013-04-18 12:34:55','paper',NULL),(9,4,'Raw 2013','2013-04-08 18:30:00','2013-04-09 10:30:00',1,10,'2013-04-09 07:19:37','Y','N',1,6,'2013-04-18 12:34:43','subject',NULL),(11,4,'Test 23042013','2013-04-21 18:30:00','2013-04-24 18:30:00',10,30,'2013-04-23 07:23:22','Y','N',1,4,'2013-04-23 08:57:03','subject',NULL),(12,4,'iit 2013','2013-04-28 18:30:00','2013-04-29 07:30:00',2,10,'2013-04-26 05:19:19','Y','N',1,2,'2013-04-29 04:39:04','subject',NULL),(13,8,'Algebra Unit I','2013-04-28 18:30:00','2013-05-08 18:30:00',10,10,'2013-04-30 11:22:45','Y','N',0,0,NULL,'subject',NULL),(14,4,'test','2014-02-19 07:16:00','2014-02-19 18:30:00',18,30,'2013-07-12 11:47:19','Y','N',0,0,NULL,'paper',NULL),(15,4,'Test 23121013','2013-12-21 18:30:00','2013-12-30 18:30:00',20,30,'2013-12-23 10:50:53','N','N',0,0,NULL,'subject',NULL),(16,4,'iit 2014','2013-11-30 18:30:00','2013-12-30 18:30:00',2,30,'2013-12-23 11:47:20','N','N',0,0,NULL,'subject',NULL),(17,4,'Test 23121013 1','2013-12-21 18:30:00','2013-12-26 18:30:00',10,10,'2013-12-23 12:28:50','N','N',0,0,NULL,'subject',NULL),(18,1,'test','2014-02-13 18:30:00','2014-02-17 18:30:00',120,30,'2014-02-13 05:25:34','N','N',0,0,NULL,'paper',NULL),(19,4,'test 1','2014-02-14 18:30:00','2014-02-19 18:30:00',60,0,'2014-02-13 07:22:42','N','N',0,0,NULL,'paper',NULL),(20,4,'test 1','2014-02-14 18:30:00','2014-02-16 18:30:00',60,30,'2014-02-14 12:01:21','N','N',0,0,NULL,'paper',NULL),(21,1,'iit','2014-02-17 18:30:00','2014-02-19 18:30:00',60,30,'2014-02-19 07:56:23','Y','N',0,0,NULL,'subject',NULL),(22,8,'math','2014-02-18 18:30:00','2014-02-20 18:30:00',60,10,'2014-02-19 08:47:20','Y','N',0,0,NULL,'subject',12),(23,8,'iit','2014-02-18 18:30:00','2014-02-19 18:30:00',60,10,'2014-02-19 11:22:33','Y','N',0,0,NULL,'subject',12),(24,5,'test','2014-02-19 18:30:00','2014-02-21 18:30:00',60,10,'2014-02-20 04:43:56','Y','N',0,0,NULL,'subject',NULL),(25,5,'feb','2014-02-20 05:12:25','2014-02-20 18:30:00',60,10,'2014-02-20 05:12:34','Y','N',0,0,NULL,'subject',NULL),(26,4,'jan','2014-02-20 05:20:46','2014-02-20 18:30:00',60,10,'2014-02-20 05:22:23','Y','N',0,0,NULL,'subject',NULL),(27,5,'pmt','2014-02-20 05:35:23','2014-02-20 18:30:00',60,10,'2014-02-20 05:36:33','Y','N',0,0,NULL,'subject',NULL),(28,11,'first test','2014-02-19 18:30:00','2014-02-20 18:30:00',60,50,'2014-02-20 12:08:21','N','N',0,0,NULL,'subject',13),(29,13,'test 1','2014-02-19 18:30:00','2014-02-20 18:30:00',60,50,'2014-02-20 12:15:01','Y','N',0,0,NULL,'subject',NULL),(30,8,'english 1','2014-02-24 04:44:07','2014-02-24 18:30:00',60,50,'2014-02-24 04:45:15','Y','N',0,0,NULL,'subject',NULL),(31,12,'e 2','2014-02-24 04:52:26','2014-02-24 18:30:00',60,50,'2014-02-24 04:53:01','Y','N',0,0,NULL,'subject',NULL),(32,11,'english 1','2014-02-24 05:47:05','2014-02-24 18:30:00',60,50,'2014-02-24 05:48:26','Y','N',0,0,NULL,'subject',NULL),(33,8,'m11','2014-02-17 18:30:00','2014-02-25 18:30:00',60,10,'2014-02-24 11:17:15','Y','N',0,0,NULL,'subject',NULL);
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_candidate`
--

DROP TABLE IF EXISTS `test_candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_candidate`
--

LOCK TABLES `test_candidate` WRITE;
/*!40000 ALTER TABLE `test_candidate` DISABLE KEYS */;
INSERT INTO `test_candidate` VALUES (7,11,9,'2013-04-23 07:26:08'),(8,11,1,'2013-04-23 07:26:08'),(9,11,2,'2013-04-23 07:26:08'),(10,11,8,'2013-04-23 07:26:08'),(13,12,8,'2013-04-29 04:20:43'),(14,12,2,'2013-04-29 04:20:43'),(15,13,1,'2013-04-30 11:22:57'),(16,15,1,'2013-12-23 10:51:03'),(17,0,8,'2013-12-23 11:47:20'),(18,0,15,'2013-12-23 11:47:20'),(19,0,2,'2013-12-23 11:47:20'),(23,16,8,'2013-12-23 11:49:08'),(24,16,15,'2013-12-23 11:49:08'),(25,16,2,'2013-12-23 11:49:08'),(26,16,9,'2013-12-23 11:49:09'),(27,0,1,'2013-12-23 12:28:50'),(28,17,1,'2013-12-23 12:29:02'),(29,21,1,'2014-02-19 08:05:10'),(30,21,19,'2014-02-19 08:05:10'),(31,21,3,'2014-02-19 08:05:10'),(34,24,19,'2014-02-20 04:45:48'),(35,24,1,'2014-02-20 04:45:48'),(36,25,19,'2014-02-20 05:14:40'),(37,0,19,'2014-02-20 05:22:23'),(38,26,19,'2014-02-20 05:24:43'),(39,27,19,'2014-02-20 05:38:25'),(40,0,31,'2014-02-20 12:15:01'),(41,29,31,'2014-02-20 12:15:55'),(42,0,19,'2014-02-24 04:45:15'),(43,30,8,'2014-02-24 04:46:47'),(46,31,25,'2014-02-24 04:54:49'),(47,31,26,'2014-02-24 04:54:49'),(48,32,22,'2014-02-24 05:51:29'),(49,32,21,'2014-02-24 05:51:29'),(50,32,20,'2014-02-24 05:51:29'),(51,32,14,'2014-02-24 05:51:30'),(52,32,11,'2014-02-24 05:51:30'),(53,0,22,'2014-02-24 11:17:15'),(59,33,18,'2014-02-24 11:20:30');
/*!40000 ALTER TABLE `test_candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_paper`
--

DROP TABLE IF EXISTS `test_paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_paper`
--

LOCK TABLES `test_paper` WRITE;
/*!40000 ALTER TABLE `test_paper` DISABLE KEYS */;
INSERT INTO `test_paper` VALUES (1,7,1),(2,8,1),(4,18,1),(5,19,1),(6,20,1),(7,14,1);
/*!40000 ALTER TABLE `test_paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_question_selection`
--

DROP TABLE IF EXISTS `test_question_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_question_selection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_level` enum('B','I','H') DEFAULT NULL,
  `question_marks` int(11) DEFAULT NULL,
  `question_total` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT '0',
  `subject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=250 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_question_selection`
--

LOCK TABLES `test_question_selection` WRITE;
/*!40000 ALTER TABLE `test_question_selection` DISABLE KEYS */;
INSERT INTO `test_question_selection` VALUES (40,'H',3,1,1,5),(39,'B',1,2,1,5),(38,'B',3,1,1,5),(37,'B',2,1,1,5),(36,'H',3,1,1,2),(35,'I',2,1,1,2),(34,'B',2,1,1,2),(33,'B',3,1,1,2),(32,'I',3,2,1,1),(31,'B',2,2,1,1),(68,'I',3,1,2,2),(67,'I',2,1,2,2),(66,'B',3,1,2,2),(65,'B',2,1,2,2),(64,'I',3,2,2,1),(63,'B',2,2,2,1),(83,'B',10,1,6,2),(82,'I',3,2,6,1),(81,'B',2,2,6,1),(84,'B',2,2,9,1),(85,'I',3,2,9,1),(121,'H',3,1,11,5),(120,'B',3,1,11,5),(119,'B',2,2,11,5),(118,'H',3,1,11,2),(117,'I',2,1,11,2),(116,'I',3,1,11,2),(115,'B',2,1,11,2),(114,'I',3,2,11,1),(113,'B',2,2,11,1),(207,'H',5,1,23,9),(206,'B',5,1,23,9),(205,'H',4,1,22,9),(204,'B',3,2,22,9),(203,'B',5,2,21,5),(202,'B',10,1,21,2),(201,'I',3,2,21,1),(200,'B',2,2,21,1),(165,'H',3,1,15,5),(164,'B',1,2,15,5),(163,'B',3,1,15,5),(162,'B',2,1,15,5),(161,'H',3,1,15,2),(160,'I',2,1,15,2),(159,'B',2,1,15,2),(158,'B',3,1,15,2),(157,'I',3,2,15,1),(156,'B',2,2,15,1),(183,'I',3,2,16,1),(182,'B',2,2,16,1),(181,'H',3,1,16,2),(180,'I',3,1,16,2),(179,'B',4,1,16,2),(178,'B',5,2,16,5),(191,'H',3,1,17,2),(190,'I',2,1,17,2),(189,'I',3,1,17,2),(188,'B',2,1,17,2),(213,'I',3,2,24,1),(212,'B',2,2,24,1),(217,'I',3,2,25,1),(216,'B',2,2,25,1),(219,'B',5,2,26,5),(223,'I',3,2,27,1),(222,'B',2,2,27,1),(229,'I',3,2,28,1),(228,'B',10,4,28,1),(227,'B',2,2,28,1),(238,'I',3,2,29,1),(237,'B',2,2,29,1),(236,'B',10,4,29,1),(242,'B',5,2,30,1),(241,'B',10,4,30,1),(244,'B',10,5,32,10),(249,'B',10,1,33,9);
/*!40000 ALTER TABLE `test_question_selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_questions`
--

DROP TABLE IF EXISTS `test_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `question_selection` enum('R','C') DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=509 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_questions`
--

LOCK TABLES `test_questions` WRITE;
/*!40000 ALTER TABLE `test_questions` DISABLE KEYS */;
INSERT INTO `test_questions` VALUES (52,25,1,'R',5),(51,115,1,'R',5),(50,93,1,'R',5),(49,87,1,'R',5),(48,24,1,'R',5),(47,17,1,'R',2),(46,19,1,'R',2),(45,18,1,'R',2),(44,15,1,'R',2),(43,9,1,'R',1),(42,8,1,'R',1),(41,11,1,'R',1),(40,10,1,'R',1),(108,15,2,'R',2),(107,18,2,'R',2),(106,16,2,'R',2),(105,19,2,'R',2),(104,115,2,'C',5),(103,93,2,'C',5),(102,89,2,'C',5),(101,87,2,'C',5),(100,10,2,'R',1),(99,11,2,'R',1),(98,8,2,'R',1),(97,9,2,'R',1),(169,13,6,'R',2),(168,9,6,'R',1),(167,8,6,'R',1),(166,11,6,'R',1),(165,10,6,'R',1),(170,10,9,'R',1),(171,11,9,'R',1),(172,8,9,'R',1),(173,9,9,'R',1),(229,25,11,'R',5),(228,87,11,'R',5),(227,88,11,'R',5),(226,24,11,'R',5),(225,17,11,'R',2),(224,19,11,'R',2),(223,16,11,'R',2),(222,18,11,'R',2),(221,9,11,'R',1),(220,8,11,'R',1),(219,11,11,'R',1),(218,10,11,'R',1),(241,15,12,'C',2),(240,18,12,'C',2),(239,19,12,'C',2),(238,90,12,'C',2),(247,134,13,'C',9),(246,133,13,'C',9),(245,135,13,'C',9),(385,10,24,'R',1),(373,82,23,'R',9),(372,134,23,'R',9),(371,75,22,'R',9),(370,132,22,'R',9),(369,40,22,'R',9),(368,89,21,'R',5),(367,22,21,'R',5),(366,13,21,'R',2),(365,9,21,'R',1),(364,8,21,'R',1),(363,11,21,'R',1),(362,10,21,'R',1),(312,25,15,'R',5),(311,115,15,'R',5),(310,93,15,'R',5),(309,87,15,'R',5),(308,24,15,'R',5),(307,17,15,'R',2),(306,19,15,'R',2),(305,18,15,'R',2),(304,15,15,'R',2),(303,9,15,'R',1),(302,8,15,'R',1),(301,11,15,'R',1),(300,10,15,'R',1),(339,10,16,'R',1),(338,11,16,'R',1),(337,8,16,'R',1),(336,9,16,'R',1),(335,79,16,'R',2),(334,16,16,'R',2),(333,17,16,'R',2),(332,22,16,'R',5),(331,89,16,'R',5),(347,17,17,'R',2),(346,19,17,'R',2),(345,16,17,'R',2),(344,18,17,'R',2),(384,11,24,'R',1),(383,8,24,'R',1),(382,9,24,'R',1),(393,9,25,'R',1),(392,8,25,'R',1),(391,11,25,'R',1),(390,10,25,'R',1),(397,89,26,'R',5),(396,22,26,'R',5),(405,9,27,'R',1),(404,8,27,'R',1),(403,11,27,'R',1),(402,10,27,'R',1),(421,9,28,'R',1),(420,8,28,'R',1),(419,144,28,'R',1),(418,143,28,'R',1),(417,142,28,'R',1),(416,141,28,'R',1),(415,11,28,'R',1),(414,10,28,'R',1),(445,10,29,'R',1),(444,11,29,'R',1),(443,141,29,'R',1),(442,142,29,'R',1),(441,143,29,'R',1),(440,144,29,'R',1),(439,8,29,'R',1),(438,9,29,'R',1),(457,147,30,'R',1),(456,146,30,'R',1),(455,144,30,'R',1),(454,143,30,'R',1),(453,142,30,'R',1),(452,141,30,'R',1),(477,148,31,'C',1),(476,144,31,'C',1),(475,143,31,'C',1),(474,142,31,'C',1),(473,141,31,'C',1),(487,153,32,'R',10),(486,152,32,'R',10),(485,151,32,'R',10),(484,150,32,'R',10),(483,149,32,'R',10),(508,40,33,'R',9);
/*!40000 ALTER TABLE `test_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_subject`
--

DROP TABLE IF EXISTS `test_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_total_marks` int(11) DEFAULT NULL,
  `question_selection` varchar(20) DEFAULT NULL,
  `show_subject_diff_question` enum('Y','N') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_subject`
--

LOCK TABLES `test_subject` WRITE;
/*!40000 ALTER TABLE `test_subject` DISABLE KEYS */;
INSERT INTO `test_subject` VALUES (12,1,1,5,10,'R','N'),(11,1,1,2,10,'R','N'),(10,1,1,1,10,'R','N'),(25,1,2,2,10,'R','Y'),(24,1,2,5,10,'C','N'),(23,1,2,1,10,'R','N'),(41,4,6,2,10,'R','N'),(40,4,6,1,10,'R','Y'),(42,4,9,1,10,'R','N'),(56,4,11,5,10,'R','N'),(55,4,11,2,10,'R','N'),(54,4,11,1,10,'R','N'),(59,4,12,2,10,'C','N'),(61,8,13,9,10,'C','N'),(96,1,21,5,10,'R','N'),(95,1,21,2,10,'R','N'),(94,1,21,1,10,'R','N'),(76,4,15,5,10,'R','N'),(75,4,15,2,10,'R','N'),(74,4,15,1,10,'R','N'),(85,4,16,1,10,'R','N'),(84,4,16,2,10,'R','N'),(83,4,16,5,10,'R','N'),(87,4,17,2,10,'R','N'),(97,8,22,9,10,'R','N'),(98,8,23,9,10,'R','N'),(101,5,24,1,10,'R','N'),(103,5,25,1,10,'R','N'),(105,4,26,5,10,'R','N'),(107,5,27,1,10,'R','N'),(109,11,28,1,50,'R','N'),(112,13,29,1,50,'R','N'),(114,8,30,1,50,'R','N'),(118,12,31,1,50,'C','N'),(120,11,32,10,50,'R','N'),(125,8,33,9,10,'R','N');
/*!40000 ALTER TABLE `test_subject` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-27 13:12:08
