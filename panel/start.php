<?php 
 /*****************Developed by :- Richa verma
	                  Date         :- 15 June,2011
					  Module       :- index.php
					  Purpose      :- It works as a entry file for other files
	***********************************************************************************/

require_once("config.php");

//=======================Include "DBfilter.php=========================================
@include_once(ROOT."/lib/dbfilter.php");
@include_once(ROOT."/lib/dbqueries.php");

//=======================Include "common_function.php=================================
@include_once(ROOT."/lib/commonfunction.php");
@include_once(ROOT."/lib/libfunc.php");
@include_once(ROOT."/lib/commonclass.php");
@include_once(ROOT."/lib/constants.php");
@include_once(ROOT.'/lib/image.class.php');
@include_once(ROOT."/lib/class.authorise.php");

@include_once(ROOT."/lib/validateFields.php");
@include(ROOT."/ckeditor/ckeditor_php5.php");

$frmdata = ChangeData();
RemoveSpaces();
$getVars = array();
ParsingURL();
?>