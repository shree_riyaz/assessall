<?php
defined("ACCESS") or die("Access Restricted");
include_once('class.admin.php');
$AdminOBJ = new Admin();
global $DB;

if(isset($frmdata['login']))
	$AdminOBJ->loginAdmin();
	
$do = '';
if(isset($getVars['do']))
	$do = $getVars['do'];

switch($do)
{
	default :
	case 'login':
		if(isset($_SESSION['admin_user_id']) && ($_SESSION['admin_user_id'] != ''))
		{
			Redirect(CreateURL('index.php',"mod=dashboard&do=showinfo"));
			exit;
		}
		$CFG->template = "admin/login.php";
		break;
		
	case 'logout':
		$AdminOBJ->logoutAdmin();
		Redirect("index.php");
		exit;
}

include(CURRENTTEMP."/index.php");
exit;
?>