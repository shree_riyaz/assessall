<?php 
/**
 * 	@author : Ashwini Agarwal
 */

class Admin
{
	function loginAdmin()
	{
		global $DB, $frmdata;	 
		
		if($frmdata['loginid'] != '' && $frmdata['password'] != '')
		{
			$loginid = addslashes($frmdata['loginid']);
			$user = $DB->SelectRecord('admin_users',"(user_name='$loginid')");
			$type = 'A';
			
			if(!$user)
			{
				$user = $DB->SelectRecord('teacher',"(username='$loginid')");
				$type = 'T';
				if(!$user)
				{
					$user = $DB->SelectRecord('parent',"(username='$loginid')");
					$type = 'P';
				}
				
				if($user) 
				{
					$user->user_name = $user->username;
					$user->disabled = $user->is_disabled ? 'Y' : 'N';
				}
			}
			
			if(($frmdata['loginid'] == $user->user_name) && (Encrypt($frmdata['password']) == $user->password))
			{
				if($user->disabled == 'Y')
				{
					$_SESSION['error'] = "You are not authorised to login.";
				}
				else
				{
					$_SESSION['admin_user_name']=$user->user_name;
					$_SESSION['admin_user_id']=$user->id;
					$_SESSION['admin_user_type']= $type;
					Redirect(CreateURL('index.php',"mod=dashboard&do=showinfo"));
					exit;
				}
			}
			else
			{
				$_SESSION['error'] = "Please enter correct username and password.";
			}
		}
		
		else
		{
			$msg='';
			if($frmdata['loginid']=='')
				  $msg="Please enter username."."<br>";
			if($frmdata['password']=='')
				  $msg.="Please enter password.";
			$_SESSION['error'] = $msg;
		}
	}
	
	function logoutAdmin()
	{
		unset($_SESSION);
		session_unset();
		session_destroy();
	}
}
?>