<?php 
/*****************Developed by :- Richa verma
	                Date         :- 3-july-2011
					Module       :- Candidatet master
					Purpose      :- Entry file for any action, call in candidate master module
*********************************************/
@session_start();
defined("ACCESS") or die("Access Restricted");
include_once('class.candidate_master.php');		
$DB = new DBFilter();
$CandidateMasterOBJ= new CandidateMaster();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($frmdata['addCandidate']))
{
	$CandidateMasterOBJ->addCandidate();	
}

if(isset($frmdata['editCandidate']))
{
	$CandidateMasterOBJ->editCandidate($nameID);	
}

if(isset($frmdata['listCandidate']))
{
	$CandidateMasterOBJ->listCandidate();	
}

if(isset($frmdata['linkCandidate']))
{
	$CandidateMasterOBJ->linkCandidate();	
}

if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}

$exam = $DB->SelectRecords('examination', '', '*', 'order by exam_name');

switch($do)
{
	case 'add':
		if(!isset($frmdata['addCandidate']))
		{
			unset($_SESSION['cand_edu']);
			unset($_SESSION['cand_exp']);
		}

	   	$CFG->template="candidate_master/save.php";
	break;
	 
	default:  	
	case 'manage':
		if ($frmdata['page_name'] != 'manage_candidate')
			$frmdata = array();
		PaginationWork();
		$totalCount=0;			
		$candidatelist= $CandidateMasterOBJ->getRecordCandidate($totalCount);
		//print_r($candidatelist[0]);
	    $CFG->template="candidate_master/manage.php";
	break; 	
		
	case 'edit':	
		if(!isset($frmdata['editCandidate']))
		{
			$_SESSION['cand_edu'] = array();
			$_SESSION['cand_edu']['count_edu'] = 0;
			$_SESSION['cand_edu']['total_edu'] = 0;
			
			$cand_edu = $DB->SelectRecords('candidate_education','candidate_id='.$nameID);
			
			$count = 1;
			foreach($cand_edu as $edu)
			{
				$_SESSION['cand_edu']['class_'.$count] = $edu->class;
				$_SESSION['cand_edu']['year_'.$count] = $edu->year;
				$_SESSION['cand_edu']['pmarks_'.$count] = $edu->percentage_marks;
				$_SESSION['cand_edu']['mmarks_'.$count] = $edu->maximum_marks;
				$_SESSION['cand_edu']['omarks_'.$count] = $edu->obtained_marks;
				$_SESSION['cand_edu']['grade_'.$count] = $edu->grade;
				$_SESSION['cand_edu']['stream_'.$count++] = $edu->stream;
				$_SESSION['cand_edu']['count_edu']++;
				$_SESSION['cand_edu']['total_edu']++;
			}
			
			$_SESSION['cand_exp'] = array();
			$_SESSION['cand_exp']['count_exp'] = 0;
			$_SESSION['cand_exp']['total_exp'] = 0;
			
			$cand_exp = $DB->SelectRecords('candidate_experience','candidate_id='.$nameID);
			
			$count = 1;
			foreach($cand_exp as $exp)
			{
				$_SESSION['cand_exp']['company_'.$count] = $exp->company;
				$_SESSION['cand_exp']['from_'.$count] = date('d-m-Y', strtotime($exp->from_date));
				$_SESSION['cand_exp']['to_'.$count] = date('d-m-Y', strtotime($exp->to_date));
				$_SESSION['cand_exp']['stream_'.$count] = $exp->stream;
				$_SESSION['cand_exp']['detail_'.$count++] = $exp->details;
				$_SESSION['cand_exp']['count_exp']++;
				$_SESSION['cand_exp']['total_exp']++;
			}
			
			$cand_exam = $DB->SelectRecords('exam_candidate','candidate_id='.$nameID);
			foreach($cand_exam as $c_exam)
			{
				$frmdata['exam_id'][] = $c_exam->exam_id;
			}
		}

		$cand_detail=$DB->SelectRecord('candidate','id='.$nameID);
	  	//print_r($cand_detail);
	  	
	   	if(!$_POST)
	   	{
		   	foreach($cand_detail as $key => $cd)
		   		$frmdata[$key] = $cd;
		   		
		   	if($frmdata['birth_date'] != '' && $frmdata['birth_date'] != 0000-00-00)
		   		$frmdata['birth_date'] = date("d-m-Y", strtotime($frmdata['birth_date']));			else				$frmdata['birth_date'] = '';
	   	}
	   	
	   	$isEdit = true;
	  	$CFG->template="candidate_master/save.php";
	break;
	
	case 'upload_candidate':	
	   $CFG->template="candidate_master/upload_candidate.php";
	break;
	   
	case 'delete':
		$cand_detail=$DB->SelectRecord('candidate','id='.$nameID);
		if($cand_detail->pro_image)
		{
			@unlink(ROOT . "/uploadfiles/" . $cand_detail->pro_image);
		}
		
		if($cand_detail->pro_resume)
		{
			@unlink(ROOT . "/uploadfiles/" . $cand_detail->pro_resume);
		}
		
		$DB->DeleteRecord('candidate','id="'.$nameID.'"');
		$DB->DeleteRecord('parent_candidate','candidate_id="'.$nameID.'"');
		$DB->DeleteRecord('candidate_education','candidate_id="'.$nameID.'"');
		$DB->DeleteRecord('candidate_experience','candidate_id="'.$nameID.'"');
		
		$DB->DeleteRecord('candidate_log','candidate_id="'.$nameID.'"');
		$DB->DeleteRecord('candidate_match_question_history','candidate_id="'.$nameID.'"');
		$DB->DeleteRecord('candidate_question_history','candidate_id="'.$nameID.'"');
		$DB->DeleteRecord('candidate_sample_match_question_history','candidate_id="'.$nameID.'"');
		$DB->DeleteRecord('candidate_sample_question_history','candidate_id="'.$nameID.'"');
		$DB->DeleteRecord('candidate_sample_test_history','candidate_id="'.$nameID.'"');
		$DB->DeleteRecord('candidate_test_history','candidate_id="'.$nameID.'"');
		$DB->DeleteRecord('candidate_test_subject_history','candidate_id="'.$nameID.'"');
		
		$_SESSION['success']="Record has been deleted successfully.";
		Redirect(CreateURL('index.php','mod=candidate_master&do=manage'));
	die();
	
	case 'log':
		if ($frmdata['page_name'] != 'log')
			$frmdata = array();
 
		$log_type = array ('Login', 'Sample Test', 'Test');
		PaginationWork();
		$totalCount=0;	
		$candidatelog = $CandidateMasterOBJ->getCandidateLog($totalCount);
		$CFG->template="candidate_master/log.php";
	break;
	
	case 'link':
		if($_SESSION['admin_user_type'] != 'P')
		{
			Redirect(CreateURL('index.php','mod=candidate_master&do=manage'));
			exit;
		}
		
		$CFG->template="candidate_master/link.php";
	break;
} 
	
//$CFG->template="candidate_master/add.php";
include(CURRENTTEMP."/index.php");

exit;
?>