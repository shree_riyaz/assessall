<?php 

defined("ACCESS") or die("Access Restricted");
include_once('class.test_master.php');		
$DB = new DBFilter();
$TestMasterOBJ= new TestMaster();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($getVars['subject_id']))
{
	$subject_id=$getVars['subject_id'];
}

if(isset($getVars['exam_id']))
{
	$exam_id=$getVars['exam_id'];
}

if(isset($getVars['random_que_id_set']))
{
	$random_que_id_set=$getVars['random_que_id_set'];
}

if(isset($getVars['custom_que_id_set']))
{
	$custom_que_id_set=$getVars['custom_que_id_set'];
}
if(isset($getVars['group_que_id_set']))
{
	$group_que_id_set=$getVars['group_que_id_set'];
}

if(isset($getVars['subject_total_mark']))
{
	$subject_total_mark=$getVars['subject_total_mark'];
}

if(isset($getVars['subject_diff_question']))
{
	$subject_diff_question=$getVars['subject_diff_question'];
}

if(isset($frmdata['submitAdd_form']) && $frmdata['submitAdd_form']==1)
{
	$TestMasterOBJ->addTest();	
}
if(isset($frmdata['submitEdit_form']) && $frmdata['submitEdit_form']==1)
{
	$TestMasterOBJ->editTest($nameID);	
}
if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}

switch($do)
{
	case 'add':
		$query = 'SELECT e.* FROM examination e ';
		if ($_SESSION['admin_user_type'] == 'T')
		{
			$query .= " JOIN exam_subject_teacher est on (est.exam_id = e.id) ";
			$query .= " WHERE est.teacher_id = '".$_SESSION['admin_user_id']."' ";
		}
		$query .= ' GROUP BY e.id order by e.exam_name ';
		$exam=$DB->RunSelectQuery($query);
		
		if(!$_POST)
		{
			$_SESSION['test_candidates'] = array();
		}
		
		$subject=$DB->SelectRecords('subject', '', '*');
		$CFG->template="test_master/save.php";
	break;
	   
	case 'addsubjectquestion':
		$exam_subject_info = $DB->SelectRecord('exam_subjects', "subject_id=$subject_id and exam_id=$exam_id");
		$subject_info = $DB->SelectRecord('subject', "id=$subject_id");
		makeRandomSession($random_que_id_set);
		
		if ($custom_que_id_set!='') 
			$_SESSION['custom_question_id_set']=$custom_que_id_set;
		if ($group_que_id_set!='') 
			$_SESSION['group_question_id_set']=$group_que_id_set;
			
		$CFG->template="test_master/addsubjectquestion.php";
	break;
	 
	default:  	
	case 'manage':
		$_SESSION['pagename'] = 'manage';
		PaginationWork();
		$totalCount=0;			
		$testlist= $TestMasterOBJ->getRecordTest($totalCount,'N');
		
		if(isset($_SESSION['is_exam_begin']))
		unset($_SESSION['is_exam_begin']);
		
	    $CFG->template="test_master/manage.php";
	break;
		
	case 'edit':
	
		$test_detail= $DB->SelectRecord('test','id='.$nameID);
		if(!$_POST)
		{
			foreach($test_detail as $key => $val)
			{
				$frmdata[$key] = $val;
			}
			
		   	$_SESSION['test_candidates'] = array();
		   	$test_candidates = $DB->SelectRecords('test_candidate', "test_id = $nameID", '*');
		   	
		   	foreach($test_candidates as $tc)
		   	{
		   		$_SESSION['test_candidates'][$test_detail->exam_id][$tc->candidate_id] = $tc->candidate_id;
		   	}
		   	
			$frmdata['test_hr'] =  (int)($test_detail->time_duration / 60);
			$frmdata['test_min'] = (int)($test_detail->time_duration % 60);
			
			$frmdata['test_date'] =  date("d/m/Y h:i:s A", strtotime($frmdata['test_date']));
			$frmdata['test_date_end'] =  date("d/m/Y h:i:s A", strtotime($frmdata['test_date_end']));
		}
		$subject=$DB->SelectRecords('subject', '', '*');
	 	
	   	$test_paper = $DB->SelectRecord('test_paper', 'test_id='.$nameID);
	 	$_SESSION['selected_paper_id'] = $test_paper->paper_id;
	   	
	   	$test_subject = getTestSubjectByTestId($nameID);
	   	if(is_array($test_subject))
	   	{
			$subject_total_question 	 = getTotalSubjectQuestion($test_subject);
	   		$subject_random_question_set = getSubjectRandomQuestionId($test_subject);
	   		$subject_custom_question_set = getSubjectCustomQuestionId($test_subject);
			$subject_group_question_set  = getSubjectGroupQuestionId($test_subject);
	   	}
	   	
		$query = 'SELECT e.* FROM examination e ';
		if ($_SESSION['admin_user_type'] == 'T')
		{
			$query .= " JOIN exam_subject_teacher est on (est.exam_id = e.id) ";
			$query .= " WHERE est.teacher_id = '".$_SESSION['admin_user_id']."' ";
		}
		$query .= ' GROUP BY e.id order by e.exam_name ';
		$exam=$DB->RunSelectQuery($query);
		
	   	$isEdit = true;
		$CFG->template="test_master/save.php";
	break;
	 
	case 'discussion':	
		$test_detail=$DB->SelectRecord('test','id='.$nameID);
		
		$qMedia = $test_detail->question_media;
		
		if($qMedia == 'subject')
		{
			$qSourceTab = 'test';
			$qMediaCondition = " test_id = $nameID ";
		}
		elseif($qMedia == 'paper')
		{
			$qSourceTab = 'paper';
			$test_paper = $DB->SelectRecord('test_paper','test_id='.$nameID);
			$paper_id = $test_paper->paper_id;
			$qMediaCondition = " paper_id = $paper_id ";
		}
		
		$test_question_detail=$DB->SelectRecords($qSourceTab.'_questions', $qMediaCondition);
		$CFG->template="test_master/discussion.php";
	break;
	    
	case 'delete':
		$DB->DeleteRecord('test_question_selection','test_id="'.$nameID.'"');
		$DB->DeleteRecord('test_subject','test_id="'.$nameID.'"');
		$DB->DeleteRecord('test_questions','test_id="'.$nameID.'"');
		$DB->DeleteRecord('test','id="'.$nameID.'"');
		$_SESSION['success']="Record has been deleted successfully.";
		$pagename = isset($_SESSION['pagename']) ? $_SESSION['pagename'] : 'manage';
		Redirect(CreateURL('index.php',"mod=test_master&do=$pagename"));
	die();

	case 'archive':
		$_SESSION['pagename'] = 'archive';
		PaginationWork();
		$totalCount=0;			
		$testlist= $TestMasterOBJ->getRecordTest($totalCount,'Y');
	    $CFG->template="test_master/manage.php";
	break;
		
	case 'preview':
		$_SESSION['preview_id'] = $_GET['id'];
		
		if(isset($_GET['first_time']) && $_GET['first_time'] == 1)
		{
			unset($_SESSION['is_exam_begin']);
			foreach($_SESSION as $key=>$value)
			{
				if(is_array($value))
				{
					if(isset($value['given_answer']))
					{
						unset($_SESSION[$key]);
					}
				}
			}
			
			$_SESSION['attempted_questions'] = array();
			$_SESSION['viewed_questions'] = array();
			$_SESSION['last_unmarked_question'] = '';
			$_SESSION['last_marked_question'] = '';
			$_SESSION['review'] = array();
			
			$last = strrpos($_SERVER['REQUEST_URI'],'&');
			Redirect(substr($_SERVER['REQUEST_URI'],0,$last));
		}
		
		if(!isset($_SESSION['is_exam_begin']) && ($_SESSION['is_exam_begin'] != 1))
		{
			$TestMasterOBJ->preview();
		}
		
		$CFG->template='test_master/preview.php';
	break;

} 	

include(CURRENTTEMP."/index.php");

exit;

?>