<?php
/*****************
	Module       :- Test master
	Purpose      :- Class for function to add and edit test
*******************/

class TestMaster
{
	//==========================================================================
	//to add a test
	//==========================================================================	
	function addTest()
	{
		global $DB,$frmdata;
		$err='';
		if($frmdata['test_name']=='')
		{
			$err.="Please enter test name.<br>";
		}
		
		if($frmdata['exam_id']=='')
		{
			$err.="Please select course.<br>";
		}
		
		if($frmdata['test_date']=='')
		{
			$err.="Please enter start date of test.<br>";
		}
		else
		{
			$test_date = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['test_date'] ) ) );
		}
		
		if($frmdata['test_date_end']=='')
		{
			$err.="Please enter end date of test.<br>";
		}
		else
		{
			$test_date_end = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['test_date_end'] ) ) );
		}
		
		if($test_date_end != '')
		{
			if (($test_date != '') && ($test_date >= $test_date_end))
			{
				$err.="Sorry! You must enter a valid test date range.<br>";
			}
			
			elseif (date('Y-m-d H:i:s') > $test_date_end)
			{
				$err.="Sorry! You can't add old one test.<br>";
			}
		}
		
		if($frmdata['test_min']==0 && $frmdata['test_hr']==0)
		{
			$err.="Please enter test duration.<br>";
		}
				
		if (isset($frmdata['exam_id']) && $frmdata['exam_id']!='')
		{
			if($frmdata['question_media'] == 'subject')
			{
				if($frmdata['total_subject']<=0)
				{
					$err.="Please select subject.<br>";
				}
				else
				{
					for($counter=0;$counter<$frmdata['total_subject'];$counter++)
					{
						if($frmdata['subject_id_'.($counter+1)]!='')
						{
							break;
						}
					}
					if($frmdata['total_subject']==$counter)
					{
						$err.="Please select subject.<br>";
					}
				}
			}
			elseif ($frmdata['question_media'] == 'paper')
			{
				if($frmdata['paper_id'] == '')
				{
					$err .= "Please select paper.<br>";
				}
				else
				{
					if(!in_array($frmdata['paper_id'], $_SESSION['paperIdForCurrentExam']))
					{
						$err .= "Paper is not identical with selected course";
					}
					$_SESSION['selected_paper_id'] = $frmdata['paper_id'];
				}
			}
			else
			{
				$err .= "Please select question media.<br>";
			}
		}
		
		
		if($err=='')
		{
			$no_subjective = ' AND ( question_type <> "S" ) ';
			
			$frmdata['test_date'] = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['test_date'] ) ) );
			$frmdata['test_date_end'] = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['test_date_end'] ) ) );
			$frmdata['time_duration']=($frmdata['test_hr']*60)+$frmdata['test_min'];

			if ($_SESSION['admin_user_type'] == 'T')
			{
				$frmdata['teacher_id'] = $_SESSION['admin_user_id'];
			}
			
			$test_id=$DB->InsertRecord('test',$frmdata);
			
			if($frmdata['question_media'] == 'subject')
			{
				for($counter=0;$counter<$frmdata['total_subject'];$counter++)
				{
					if($frmdata['subject_id_'.($counter+1)]=='')
					{
						continue;
					}
					else 
					{
						$frmdata['test_id'] = $test_id;
						$frmdata['subject_id'] = $frmdata['subject_id_'.($counter+1)];
						$frmdata['subject_total_marks'] = $frmdata['subject_total_mark_'.($counter+1)];
						$frmdata['show_subject_diff_question'] = $frmdata['subject_diff_question_'.($counter+1)];
						
						if($frmdata['random_que_id_'.($counter+1)]!='' && $frmdata['custom_que_id_'.($counter+1)]!='')
						{
							$frmdata['question_selection']="R,C";
						}
						elseif ($frmdata['random_que_id_'.($counter+1)]!='')
						{
							$frmdata['question_selection']="R";
						}
						elseif ($frmdata['custom_que_id_'.($counter+1)]!='')
						{
							$frmdata['question_selection']="C";
						}elseif ($frmdata['group_que_id_'.($counter+1)]!='')
						{
							$frmdata['question_selection']="G";
						}
						
						$DB->InsertRecord('test_subject',$frmdata);
						
						if($frmdata['random_que_id_'.($counter+1)]!='')
						{
							
							$random_que_id_array = explode(",",$frmdata['random_que_id_'.($counter+1)]);
							
							for($index=0; $index<count($random_que_id_array); $index++)
							{
								$test_question_table_data['test_id'] = $test_id;
								$test_question_table_data['question_id'] = $random_que_id_array[$index];
								$test_question_table_data['question_selection'] = 'R';
								$test_question_table_data['subject_id'] = $frmdata['subject_id'];
								
								$DB->InsertRecord('test_questions',$test_question_table_data);
								
								$question_info = $DB->SelectRecord('question','id='.$random_que_id_array[$index]);
								
								if($question_info->question_level=='B')
								{
									if(isset($beginner_question[$question_info->marks]))
										$beginner_question[$question_info->marks]+=1;
									else 
										$beginner_question[$question_info->marks] = 1;
								}
								
								if($question_info->question_level=='I')
								{
									if(isset($inter_question[$question_info->marks]))
										$inter_question[$question_info->marks]+=1;
									else 
										$inter_question[$question_info->marks] = 1;
								}
							
								if($question_info->question_level=='H')
								{
									if(isset($higher_question[$question_info->marks]))
										$higher_question[$question_info->marks]+=1;
									else 
										$higher_question[$question_info->marks] = 1;
								}
							}
							
							if(isset($beginner_question))
							{
								foreach($beginner_question as $key=>$val) // key=marks val=no of question
								{
									$test_question_selection['test_id']=$test_id;
									$test_question_selection['question_level']='B';
									$test_question_selection['question_marks']=$key;
									$test_question_selection['question_total']=$val;
									$test_question_selection['subject_id']=$frmdata['subject_id'];
									
									$DB->InsertRecord('test_question_selection',$test_question_selection);
								}
							}
							unset($beginner_question);
							
							if(isset($inter_question))
							{
								foreach($inter_question as $key=>$val) // key=marks val=no of question
								{
									$test_question_selection['test_id']=$test_id;
									$test_question_selection['question_level']='I';
									$test_question_selection['question_marks']=$key;
									$test_question_selection['question_total']=$val;
									$test_question_selection['subject_id']=$frmdata['subject_id'];
									
									$DB->InsertRecord('test_question_selection',$test_question_selection);
								}
							}
							unset($inter_question);
						
							if(isset($higher_question))
							{
								foreach($higher_question as $key=>$val) // key=marks val=no of question
								{
									$test_question_selection['test_id']=$test_id;
									$test_question_selection['question_level']='H';
									$test_question_selection['question_marks']=$key;
									$test_question_selection['question_total']=$val;
									$test_question_selection['subject_id']=$frmdata['subject_id'];
									
									$DB->InsertRecord('test_question_selection',$test_question_selection);
								}
							}
							unset($higher_question);
						}
						
						if ($frmdata['custom_que_id_'.($counter+1)]!='')
						{	
							$custom_que_id_array = explode(",",$frmdata['custom_que_id_'.($counter+1)]);
							
							for($index=0; $index<count($custom_que_id_array); $index++)
							{
								$test_question_table_data['test_id'] = $test_id;
								$test_question_table_data['question_id'] = $custom_que_id_array[$index];
								$test_question_table_data['question_selection'] = 'C';
								//$test_question_table_data['group_question_id'] =0;
								$test_question_table_data['subject_id'] = $frmdata['subject_id'];
								
								$DB->InsertRecord('test_questions',$test_question_table_data);
							}
						}
						
						if ($frmdata['group_que_id_'.($counter+1)]!='')
						{
							$group_que_id_array = explode(",",$frmdata['group_que_id_'.($counter+1)]);
							
							for($index=0; $index<count($group_que_id_array); $index++)
							{	
								$query = "select q.id from question q where group_question_id=".$group_que_id_array[$index];
								$test_question= $DB->RunSelectQuery($query);
								for($i=0;$i<count($test_question);$i++)
								{		
									$test_question_table_data_group['test_id'] = $test_id;
									$test_question_table_data_group['question_id'] =$test_question[$i]->id ;
									$test_question_table_data_group['group_question_id'] =$group_que_id_array[$index] ;
									$test_question_table_data_group['question_selection'] = 'G';
									$test_question_table_data_group['subject_id'] = $frmdata['subject_id'];	
									$DB->InsertRecord('test_questions',$test_question_table_data_group);
								}
							}
						}
					}
				}
			}
			elseif($frmdata['question_media'] == 'paper')
			{
				$tp = array();
				$tp['test_id'] = $test_id;
				$tp['paper_id'] = $frmdata['paper_id'];
				$DB->InsertRecord('test_paper', $tp);
			}
		
			/******** Add candidate *************************************/
			
			foreach($_SESSION['test_candidates'] as $exam_id => $exam_candidate)
			{
				foreach($exam_candidate as $candidate_id)
				{
					$test_candidates = array('test_id' => $test_id);
					$test_candidates['candidate_id'] = $candidate_id;
					
					$DB->InsertRecord('test_candidate', $test_candidates);
				}
			}
			
			$_SESSION['success']='Test details has been added successfully.';
			$frmdata='';
			Redirect(CreateURL('index.php','mod=test_master&do=manage'));
			exit;
		}
		else
		{
			$_SESSION['error']=$err;
		}	
	}
	//==========================================================================
	
	
	//==========================================================================
	// to edit test information
	//==========================================================================
	function editTest($nameID)
	{
		global $DB,$frmdata;
		$err='';
		
		$test_history = $DB->SelectRecord('candidate_test_history', "test_id=$nameID");
		
		if($test_history)
		{
			//$err = 'Permission denied. This test is already conducted.';
		}
		else
		{
			if($frmdata['test_name']=='')
			{
				$err.="Please enter test name.<br>";
			}
			
			if($frmdata['exam_id']=='')
			{
				$err.="Please select course.<br>";
			}
			
			if($frmdata['test_date']=='')
			{
				$err.="Please enter start date of test.<br>";
			}
			else
			{
				$test_date = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['test_date'] ) ) );
			}
			
			if($frmdata['test_date_end']=='')
			{
				$err.="Please enter end date of test.<br>";
			}
			else
			{
				$test_date_end = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['test_date_end'] ) ) );
			}
			
			if($test_date_end != '')
			{
				if (($test_date != '') && ($test_date >= $test_date_end))
				{
					$err.="Sorry! You must enter a valid test date range.<br>";
				}
				
				elseif (date('Y-m-d H:i:s') > $test_date_end)
				{
					$err.="Sorry! You can't add old one test.<br>";
				}
			}
			
			if($frmdata['test_min']==0 && $frmdata['test_hr']==0)
			{
				$err.="Please enter test duration.<br>";
			}
			
			if (isset($frmdata['exam_id']) && $frmdata['exam_id']!='')
			{
				$exam_type='';
					
				if($frmdata['question_media'] == 'subject')
				{
					if($frmdata['total_subject']<=0)
					{
						$err.="Please select subject.<br>";
					}
					else
					{
						for($counter=0;$counter<$frmdata['total_subject'];$counter++)
						{
							if($frmdata['subject_id_'.($counter+1)]!='')
							{
								break;
							}
						}
						if($frmdata['total_subject']==$counter)
						{
							$err.="Please select subject.<br>";
						}
					}
				}
				elseif ($frmdata['question_media'] == 'paper')
				{
					if($frmdata['paper_id'] == '')
					{
						$err .= "Please select paper.<br>";
					}
					else
					{
						if(!in_array($frmdata['paper_id'], $_SESSION['paperIdForCurrentExam']))
						{
							$err .= "Paper is not identical with selected course";
						}
						$_SESSION['selected_paper_id'] = $frmdata['paper_id'];
					}
				}
				else
				{
					$err .= "Please select question media.<br>";
				}
			}
			
			
		}
		 
		if($frmdata['negative_marking']!='Y')
		{
			$frmdata['negative_marking']='N';
		}
		if($err=='')
		{
			$no_subjective = ' AND ( question_type <> "S" ) ';
		
			$DB->DeleteRecord('test_question_selection','test_id="'.$nameID.'"');
			$DB->DeleteRecord('test_questions','test_id="'.$nameID.'"');
			$DB->DeleteRecord('test_subject','test_id="'.$nameID.'"');
			$DB->DeleteRecord('test_paper','test_id="'.$nameID.'"');
			
			$frmdata['test_date'] = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['test_date'] ) ) );
			$frmdata['test_date_end'] = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['test_date_end'] ) ) );
			$frmdata['time_duration']=($frmdata['test_hr']*60)+$frmdata['test_min'];

			$DB->UpdateRecord('test',$frmdata,"id=$nameID");
			
			if($frmdata['question_media'] == 'subject')
			{
				for($counter=0;$counter<$frmdata['total_subject'];$counter++)
				{
					if($frmdata['subject_id_'.($counter+1)]=='')
					{
						continue;
					}
					else 
					{
						$frmdata['test_id'] = $nameID;
						$frmdata['subject_id'] = $frmdata['subject_id_'.($counter+1)];
						$frmdata['subject_total_marks'] = $frmdata['subject_total_mark_'.($counter+1)];
						$frmdata['show_subject_diff_question'] = $frmdata['subject_diff_question_'.($counter+1)];
						
						if($frmdata['random_que_id_'.($counter+1)]!='' && $frmdata['custom_que_id_'.($counter+1)]!='' && $frmdata['group_que_id_'.($counter+1)]!='')
						{
							$frmdata['question_selection']="R,C,G";
						}
						elseif ($frmdata['random_que_id_'.($counter+1)]!='')
						{
							$frmdata['question_selection']="R";
						}
						elseif ($frmdata['custom_que_id_'.($counter+1)]!='')
						{
							$frmdata['question_selection']="C";
						}elseif ($frmdata['group_que_id_'.($counter+1)]!='')
						{
							$frmdata['question_selection']="G";
						}
						
						$DB->InsertRecord('test_subject',$frmdata);
						
						if($frmdata['random_que_id_'.($counter+1)]!='')
						{
							$random_que_id_array = explode(",",$frmdata['random_que_id_'.($counter+1)]);
							
							for($index=0; $index<count($random_que_id_array); $index++)
							{
								$test_question_table_data['test_id'] = $nameID;
								$test_question_table_data['question_id'] = $random_que_id_array[$index];
								$test_question_table_data['question_selection'] = 'R';
								$test_question_table_data['subject_id'] = $frmdata['subject_id'];
								
								$DB->InsertRecord('test_questions',$test_question_table_data);
								
								$question_info = $DB->SelectRecord('question','id='.$random_que_id_array[$index]);
								
								if($question_info->question_level=='B')
								{
									if(isset($beginner_question[$question_info->marks]))
										$beginner_question[$question_info->marks]+=1;
									else 
										$beginner_question[$question_info->marks] = 1;
								}
								
								if($question_info->question_level=='I')
								{
									if(isset($inter_question[$question_info->marks]))
										$inter_question[$question_info->marks]+=1;
									else 
										$inter_question[$question_info->marks] = 1;
								}
							
								if($question_info->question_level=='H')
								{
									if(isset($higher_question[$question_info->marks]))
										$higher_question[$question_info->marks]+=1;
									else 
										$higher_question[$question_info->marks] = 1;
								}
							}
							
							if(isset($beginner_question))
							{
								foreach($beginner_question as $key=>$val) // key=marks val=no of question
								{
									$test_question_selection['test_id']=$nameID;
									$test_question_selection['question_level']='B';
									$test_question_selection['question_marks']=$key;
									$test_question_selection['question_total']=$val;
									$test_question_selection['subject_id']=$frmdata['subject_id'];
									
									$DB->InsertRecord('test_question_selection',$test_question_selection);
								}
							}
							unset($beginner_question);
							
							if(isset($inter_question))
							{
								foreach($inter_question as $key=>$val) // key=marks val=no of question
								{
									$test_question_selection['test_id']=$nameID;
									$test_question_selection['question_level']='I';
									$test_question_selection['question_marks']=$key;
									$test_question_selection['question_total']=$val;
									$test_question_selection['subject_id']=$frmdata['subject_id'];
									
									$DB->InsertRecord('test_question_selection',$test_question_selection);
								}
							}
							unset($inter_question);
						
							if(isset($higher_question))
							{
								foreach($higher_question as $key=>$val) // key=marks val=no of question
								{
									$test_question_selection['test_id']=$nameID;
									$test_question_selection['question_level']='H';
									$test_question_selection['question_marks']=$key;
									$test_question_selection['question_total']=$val;
									$test_question_selection['subject_id']=$frmdata['subject_id'];
									
									$DB->InsertRecord('test_question_selection',$test_question_selection);
								}
							}
							unset($higher_question);
						}
						
						if ($frmdata['custom_que_id_'.($counter+1)]!='')
						{
							$custom_que_id_array = explode(",",$frmdata['custom_que_id_'.($counter+1)]);
							
							for($index=0; $index<count($custom_que_id_array); $index++)
							{
								$test_question_table_data['test_id'] = $nameID;
								$test_question_table_data['question_id'] = $custom_que_id_array[$index];
								$test_question_table_data['question_selection'] = 'C';
								$test_question_table_data['subject_id'] = $frmdata['subject_id'];
								
								$DB->InsertRecord('test_questions',$test_question_table_data);
							}
						}
						if ($frmdata['group_que_id_'.($counter+1)]!='')
						{
							$group_que_id_array = explode(",",$frmdata['group_que_id_'.($counter+1)]);
							
							for($index=0; $index<count($group_que_id_array); $index++)
							{	
							 	$query = "select id from question q where group_question_id=".$group_que_id_array[$index];
								$test_question= $DB->RunSelectQuery($query);
								for($i=0;$i<count($test_question);$i++)
								{		
									$test_question_table_data['test_id'] = $nameID;
									$test_question_table_data['question_id'] = $test_question[$i]->id;
									$test_question_table_data['group_question_id'] = $group_que_id_array[$index];
									$test_question_table_data['question_selection'] = 'G';
									$test_question_table_data['subject_id'] = $frmdata['subject_id'];
									$DB->InsertRecord('test_questions',$test_question_table_data);
								}
							}
						}
					}
				}
			}
			elseif($frmdata['question_media'] == 'paper')
			{
				$tp = array();
				$tp['test_id'] = $nameID;
				$tp['paper_id'] = $frmdata['paper_id'];
				$DB->InsertRecord('test_paper', $tp);
			}
			
			/******** Add candidate *************************************/
			$DB->DeleteRecord('test_candidate','test_id="'.$nameID.'"');
			foreach($_SESSION['test_candidates'] as $exam_id => $exam_candidate)
			{
				foreach($exam_candidate as $candidate_id)
				{
					$test_candidates = array('test_id' => $nameID);
					$test_candidates['candidate_id'] = $candidate_id;
					
					$DB->InsertRecord('test_candidate', $test_candidates);
				}
			}
			
			$_SESSION['success']='Test details has been updated successfully.';
			$frmdata='';
			$pagename = isset($_SESSION['pagename']) ? $_SESSION['pagename'] : 'manage';
			Redirect(CreateURL('index.php',"mod=test_master&do=$pagename"));
			exit;
		}
		else
		{
			$_SESSION['error']=$err;
		}	
	}
//==========================================================================	
	
	/******************************************************************
	Des: A function to get Test information
	******************************************************************/
	
	function getRecordTest(&$totalCount, $is_archive)
	{
			
		global $DB,$frmdata;
		
		$query='';
		$query="select
	        test.*, exam.exam_name
	         from ".PREFIX."test 
					left join examination as exam on test.exam_id=exam.id";
		
		if ($_SESSION['admin_user_type'] == 'T')
		{
			$query .= " JOIN exam_subject_teacher est on (est.exam_id = exam.id) ";
			$query .= " WHERE est.teacher_id = '".$_SESSION['admin_user_id']."' AND test.teacher_id = '".$_SESSION['admin_user_id']."' ";
		}
		else
		{
			$query .= ' WHERE 1 ';
		}
		
		$query.=" AND is_archive='".$is_archive."' and ";
		
		if(isset($frmdata['name']) && $frmdata['name'] !='')
		{
			$query.="(";
			$query.=" test.test_name like '%".$frmdata['name']."%'" ; 
			$query.=")";
			$query.=" and";	
		}
		
		$query=substr($query,0,(strlen($query)-4));
		$query.= " GROUP BY test.id ";
		
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{
			if ($frmdata['orderby'] == 'test_date')
			{
				$query.=" order by date_format(test.test_date, '%y-%m-%d %H:%i:%s')";
			}
			
			elseif ($frmdata['orderby'] == 'test_date desc')
			{
				$query.=" order by date_format(test.test_date, '%y-%m-%d %H:%i:%s') desc";
			}
			
			elseif ($frmdata['orderby'] == 'test_date_end')
			{
				$query.=" order by date_format(test.test_date_end, '%y-%m-%d %H:%i:%s')";
			}
			
			elseif ($frmdata['orderby'] == 'test_date_end desc')
			{
				$query.=" order by date_format(test.test_date_end, '%y-%m-%d %H:%i:%s') desc";
			}
			
			else
			{
				$query.=" order by ".$frmdata['orderby'];
			}
		}	
		else
		{
			$query.=' order by id desc';
		}
		
		$_SESSION['queryForTest'] = $query;
		
		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		return $result;   
	}
	
	//==========================================================================
	//	to preview test
	//==========================================================================
	function preview()
	{
		global $DB,$frmdata;
		$currectTestId = $_SESSION['preview_id'];

		$no_subjective = " and question_type <> 'S' ";
		$test_info = $DB->SelectRecord('test', "id=$currectTestId");
		
		$_SESSION['questionMedia'] = $qMedia = $test_info->question_media;
		
		if($qMedia == 'subject')
		{
			$qSourceTab = 'test';
			$qMediaCondition = " test_id = $currectTestId ";
		}
		elseif($qMedia == 'paper')
		{
			$qSourceTab = 'paper';
			$test_paper = $DB->SelectRecord('test_paper','test_id='.$currectTestId);
			$_SESSION['testPaperId'] = $paper_id = $test_paper->paper_id;
			$qMediaCondition = " paper_id = $paper_id ";
		}
		
		$_SESSION['exam_id'] = $test_info->exam_id;
		$exam_type='';
		
		$_SESSION['time_takenfortest'] = $test_info->time_duration;

		if($test_info->show_diff_question!='Y')
		{
			$_SESSION['subject_info_arr'] = array();
			$test_subject = $DB->SelectRecords($qSourceTab.'_subject',$qMediaCondition);
			
			if($test_subject)
			{
				$subject_name_counter=0;
				$_SESSION['subject_info_arr'] = array();
				for($index=0; $index<count($test_subject); $index++)
				{
					$common_condition='';
					$beg_que = array();		//=======beginner level question
					$int_que = array();		//=======intermediate level question
					$high_que = array();	//=======higher level question
				
					$test_question_selection_info = $DB->SelectRecords($qSourceTab.'_question_selection', "$qMediaCondition and subject_id=".$test_subject[$index]->subject_id);
					
					for($counter=0;$counter<count($test_question_selection_info);$counter++)
					{
						if($test_question_selection_info[$counter]->question_level=='B')
						{
							$beg_que[$test_question_selection_info[$counter]->question_marks]['no_of_question']=$test_question_selection_info[$counter]->question_total;
						}
					
						if($test_question_selection_info[$counter]->question_level=='I')
						{
							$int_que[$test_question_selection_info[$counter]->question_marks]['no_of_question']=$test_question_selection_info[$counter]->question_total;
						}
					
						if($test_question_selection_info[$counter]->question_level=='H')
						{
							$high_que[$test_question_selection_info[$counter]->question_marks]['no_of_question']=$test_question_selection_info[$counter]->question_total;
						}
					}
				
					$subject_info = $DB->SelectRecord('subject', 'id='.$test_subject[$index]->subject_id);

					if($subject_info->stream_id)
					{
						$common_condition.= " and (stream_id=".$subject_info->stream_id." or subject_id=".$test_subject[$index]->subject_id.") ";
					}
					else
					{
						$common_condition.= " and subject_id=".$test_subject[$index]->subject_id;
					}

					if($test_subject[$index]->show_subject_diff_question=='Y')
					{
						if(stristr($subject_condidtion, $test_subject[$index]->subject_id) || $subject_condidtion=='')
						{
							$test_custom_question = $DB->SelectRecords($qSourceTab.'_questions',"$qMediaCondition and subject_id=".$test_subject[$index]->subject_id." and question_selection='C'",'*','ORDER BY subject_id, RAND()');
							if($test_custom_question)
							{
								for($counter=0;$counter<count($test_custom_question);$counter++,$subject_name_counter++)
								{
									$custom_que_set.= $test_custom_question[$counter]->question_id.",";
									$_SESSION['test_id-'.$currectTestId]['question_id-'.$subject_name_counter]=$test_custom_question[$counter]->question_id;
								
									if(!array_key_exists($test_custom_question[$counter]->subject_id, $_SESSION['subject_info_arr']))
									{
										$_SESSION['subject_info_arr'][$test_custom_question[$counter]->subject_id]['subject_name'] = getSubjectNameBySubjectId($test_custom_question[$counter]->subject_id);
										$_SESSION['subject_info_arr'][$test_custom_question[$counter]->subject_id]['start_question_id'] = $subject_name_counter;
									}
									else
									{
										$_SESSION['subject_info_arr'][$test_custom_question[$counter]->subject_id]['last_question_id'] = $subject_name_counter;
									}
								}
								$custom_que_set = substr($custom_que_set, 0, strlen($custom_que_set)-1);
								$cust_que_set_condition = " and id not in ($custom_que_set)";
							}
						
							foreach($beg_que as $marks => $no_of_question)
							{
								$beg_questionDetails=$DB->SelectRecords('question',"question_level='B' and marks=".$marks.$common_condition.$no_subjective.$cust_que_set_condition,'*'," ORDER BY RAND() limit ".$no_of_question['no_of_question']);
								if($beg_questionDetails)
								{
									$count=count($beg_questionDetails);
									for($counter1=0;$counter1<$count;$counter1++,$subject_name_counter++)
									{
										$_SESSION['test_id-'.$currectTestId]['question_id-'.$subject_name_counter]=$beg_questionDetails[$counter1]->id;

										if(!array_key_exists($test_subject[$index]->subject_id, $_SESSION['subject_info_arr']))
										{
											$_SESSION['subject_info_arr'][$test_subject[$index]->subject_id]['subject_name'] = getSubjectNameBySubjectId($test_subject[$index]->subject_id);
											$_SESSION['subject_info_arr'][$test_subject[$index]->subject_id]['start_question_id'] = $subject_name_counter;
										}
										else
										{
											$_SESSION['subject_info_arr'][$test_subject[$index]->subject_id]['last_question_id'] = $subject_name_counter;
										}
									}
								}
							}

							foreach($int_que as $marks => $no_of_question)
							{
								$int_questionDetails=$DB->SelectRecords('question',"question_level='I' and marks=".$marks.$common_condition.$no_subjective.$cust_que_set_condition,'*'," ORDER BY RAND() limit ".$no_of_question['no_of_question']);
								if($int_questionDetails)
								{
									$count=count($int_questionDetails);
									for($counter1=0;$counter1<$count;$counter1++,$subject_name_counter++)
									{
										$_SESSION['test_id-'.$currectTestId]['question_id-'.$subject_name_counter]=$int_questionDetails[$counter1]->id;

										if(!array_key_exists($test_subject[$index]->subject_id, $_SESSION['subject_info_arr']))
										{
											$_SESSION['subject_info_arr'][$test_subject[$index]->subject_id]['subject_name'] = getSubjectNameBySubjectId($test_subject[$index]->subject_id);
											$_SESSION['subject_info_arr'][$test_subject[$index]->subject_id]['start_question_id'] = $subject_name_counter;
										}
										else
										{
											$_SESSION['subject_info_arr'][$test_subject[$index]->subject_id]['last_question_id'] = $subject_name_counter;
										}
									}
								}
							}
						
							foreach($high_que as $marks => $no_of_question)
							{
								$high_questionDetails=$DB->SelectRecords('question',"question_level='H' and marks=".$marks.$no_subjective.$common_condition.$cust_que_set_condition,'*'," ORDER BY RAND() limit ".$no_of_question['no_of_question']);
								if($high_questionDetails)
								{
									$count=count($high_questionDetails);
									for($counter1=0;$counter1<$count;$counter1++,$subject_name_counter++)
									{
										$_SESSION['test_id-'.$currectTestId]['question_id-'.$subject_name_counter]=$high_questionDetails[$counter1]->id;
										
										if(!array_key_exists($test_subject[$index]->subject_id, $_SESSION['subject_info_arr']))
										{
											$_SESSION['subject_info_arr'][$test_subject[$index]->subject_id]['subject_name'] = getSubjectNameBySubjectId($test_subject[$index]->subject_id);
											$_SESSION['subject_info_arr'][$test_subject[$index]->subject_id]['start_question_id'] = $subject_name_counter;
										}
										else
										{
											$_SESSION['subject_info_arr'][$test_subject[$index]->subject_id]['last_question_id'] = $subject_name_counter;
										}
									}
								}
							}
						}
					}
					else
					{
						if(stristr($subject_condidtion, $test_subject[$index]->subject_id) || $subject_condidtion=='')
						{
							$testDetails=$DB->SelectRecords($qSourceTab.'_questions',"$qMediaCondition and subject_id=".$test_subject[$index]->subject_id,'*','ORDER BY subject_id, RAND()');
							$count=count($testDetails);

							for($counter=0;$counter<$count;$counter++,$subject_name_counter++)
							{
								$question_id=$testDetails[$counter]->question_id;

								if(!array_key_exists($testDetails[$counter]->subject_id, $_SESSION['subject_info_arr']))
								{
									$_SESSION['subject_info_arr'][$testDetails[$counter]->subject_id]['subject_name'] = getSubjectNameBySubjectId($testDetails[$counter]->subject_id);
									$_SESSION['subject_info_arr'][$testDetails[$counter]->subject_id]['start_question_id'] = $subject_name_counter;
								}
								else
								{
									$_SESSION['subject_info_arr'][$testDetails[$counter]->subject_id]['last_question_id'] = $subject_name_counter;
								}
								$_SESSION['test_id-'.$currectTestId]['question_id-'.$subject_name_counter]=$question_id;
							}
						}
					}
				}
			}
		}
		
		else
		{
			$test_custom_question = $DB->SelectRecords($qSourceTab.'_questions', $qMediaCondition.$subject_condidtion." and question_selection='C'",'*','ORDER BY subject_id, RAND()');
			$test_question_selection_info = $DB->SelectRecords($qSourceTab.'_question_selection', $qMediaCondition.$subject_condidtion);
			$_SESSION['subject_info_arr'] = array();
			
			$question_id_counter=0;
			if($test_question_selection_info)
			{
				$beg_que = array();		//=======beginner level question
				$int_que = array();		//=======intermediate level question
				$high_que = array();	//=======higher level question
				
				for($counter=0;$counter<count($test_question_selection_info);$counter++)
				{
					if($test_question_selection_info[$counter]->question_level=='B')
					{
						$beg_que[$test_question_selection_info[$counter]->question_marks]['no_of_question']=$test_question_selection_info[$counter]->question_total;
					}
					
					if($test_question_selection_info[$counter]->question_level=='I')
					{
						$int_que[$test_question_selection_info[$counter]->question_marks]['no_of_question']=$test_question_selection_info[$counter]->question_total;
					}
					
					if($test_question_selection_info[$counter]->question_level=='H')
					{
						$high_que[$test_question_selection_info[$counter]->question_marks]['no_of_question']=$test_question_selection_info[$counter]->question_total;
					}
				}

				if($test_custom_question)
				{
					for($counter=0;$counter<count($test_custom_question);$counter++,$question_id_counter++)
					{
						$custom_que_set.= $test_custom_question[$counter]->question_id.",";
						$_SESSION['test_id-'.$currectTestId]['question_id-'.$counter]=$test_custom_question[$counter]->question_id;
						
					}
					$custom_que_set = substr($custom_que_set, 0, strlen($custom_que_set)-1);
					$cust_que_set_condition = " and id not in ($custom_que_set)";
				}
				
				foreach($beg_que as $marks => $no_of_question)
				{
					$beg_questionDetails=$DB->SelectRecords('question',"question_level='B' and marks=".$marks.$no_subjective.$common_condition.$cust_que_set_condition,'*'," ORDER BY RAND() limit ".$no_of_question['no_of_question']);
					if($beg_questionDetails)
					{
						$count=count($beg_questionDetails);
						for($counter1=0;$counter1<$count;$counter1++,$question_id_counter++)
						{
							$_SESSION['test_id-'.$currectTestId]['question_id-'.$question_id_counter]=$beg_questionDetails[$counter1]->id;
						}
					}
				}
				
				foreach($int_que as $marks => $no_of_question)
				{
					$int_questionDetails=$DB->SelectRecords('question',"question_level='I' and marks=".$marks.$no_subjective.$common_condition.$cust_que_set_condition,'*'," ORDER BY RAND() limit ".$no_of_question['no_of_question']);
					if($int_questionDetails)
					{
						$count=count($int_questionDetails);
						for($counter1=0;$counter1<$count;$counter1++,$question_id_counter++)
						{
							$_SESSION['test_id-'.$currectTestId]['question_id-'.$question_id_counter]=$int_questionDetails[$counter1]->id;
						}
					}
				}
				
				foreach($high_que as $marks => $no_of_question)
				{
					$high_questionDetails=$DB->SelectRecords('question',"question_level='H' and marks=".$marks.$no_subjective.$common_condition.$cust_que_set_condition,'*'," ORDER BY RAND() limit ".$no_of_question['no_of_question']);
					if($high_questionDetails)
					{
						$count=count($high_questionDetails);
						for($counter1=0;$counter1<$count;$counter1++,$question_id_counter++)
						{
							$_SESSION['test_id-'.$currectTestId]['question_id-'.$question_id_counter]=$high_questionDetails[$counter1]->id;
						}
					}
				}
			}
		}
		
		$time=$_SESSION['time_takenfortest'];
		$duration_added=$time*60;
		$date= date('m/d/Y h:i:s A', time() + $duration_added);
		$_SESSION['date']=$date;
	}
}//end of class
?>