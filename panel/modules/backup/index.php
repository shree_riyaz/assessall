<?php
include_once(ROOT."/lib/class.backupdata.php");

$DB = new DBFilter();

$exportEnabled = '';
$auth = new Authorise();
if (($auth->isAuthorisedAction($_SESSION['admin_user_id'], 'backup', 'download') == false) && ($_SESSION['admin_user_name'] != 'admin'))
{
	$exportEnabled = 'disabled';
}

$backup =  new Backup;
//================ For getting URL variables ===================================================
if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}

if (isset($frmdata['search']))
{
		$frmdata['pageNumber']=1;
		
}


if (isset($frmdata['clearsearch']))
{
	unset($frmdata);
}

//restore selected backup
if(isset($frmdata['restore']))
{
	//$siteLockObj= new SiteLock();
	//lock the site first
		//fetch the backup file
		$backupSingalData=$DB->selectRecord("backup_data","id=".Decode($getVars['id']));
		if($backupSingalData)
		{
			//restor backup
			if($backup->restoreBackup(BACKUP."/".$backupSingalData->backup_file_path))
			{				
				$_SESSION['success']='Backup has been restored successfully.';
				Redirect(CreateURL('index.php','mod=backup'));
				die();
			}
			else
			{
				$_SESSION['error']="There is some problem occur while restoring a backup. Contact to your system administrator";
				Redirect(CreateURL('index.php','mod=backup'));
				die();
			}
			
		}
		else
		{
			$_SESSION['error']="No backup found.";
			Redirect(CreateURL('index.php','mod=backup'));
			die();
		}	
	
	
}

if(isset($frmdata['superDataFilefinish']))
{
	$_SESSION['mes']['text']="<strong>Backup has been restored successfully.";
	$_SESSION['mes']['width']='70%';
	Redirect(createURL('index.php',"mod=backup"));
}
/* Restore Backup */
if (isset($frmdata['upload']))
{
	$backup->uploadBackup();
}	
/*if($frmdata['backup'])
{
	
	$tables=array('defaultWeightsSets','defaultWeights');
	$path=ROOT."/backups/default_".date('d-m-Y')."-".time();
	mkdir($path);
	chmod($path,0777);
	$backup->backupTables($tables,$path);
}*/

switch($do)
{
	case 'takeBackup':
		if(isset($frmdata['finalBackup']))
		{
			$result = $backup->fullBackup(BACKUP);
			//check if the error variable isset
			//echo $result['error'];
			if (!is_array($result))
			{
				$backupData['backup_file_path']=$result;
				$backupData['user_name']=$_SESSION['admin_user_name'];
				$backupData['user_type']=$_SESSION['admin_user_type'];
				//insert records in backup data table
				if($backupDataID=$DB->insertRecord('backup_data',$backupData))
				{
					$_SESSION['success']="Backup has been taken successfully.";
					
					//if file is to be downloaded then set a session variable for this
					if(isset($frmdata['download']))
					{
						$_SESSION['u']=Encode(createURL('index.php',"mod=backup&do=download&id=$backupDataID"));
					}
					Redirect(createURL('index.php',"mod=backup"));
				}
				else
				{
					echo "Backup has not been created.";
					exit;
				}
			}
			else
			{
				//echo 'True';
				$_SESSION['error'] = $result['error'];
				$CFG->template="backup/backup.php";
			}		
		}
		else
		{
			$CFG->template="backup/backup.php";
		}
		
		break;
		
	case 'download':
		$backupSingalData=$DB->selectRecord("backup_data","id=".$getVars['id']);
		if($backupSingalData)
		{
			if(file_exists(BACKUP."/".$backupSingalData->backup_file_path))
			{
				$backup->downloadFile(BACKUP."/".$backupSingalData->backup_file_path);
			}
			else
			{
				$_SESSION['error']="Sorry! no backup file found";
			}	
		}
		else
		{
			$_SESSION['error']="Sorry! no backup found";
		}
		
		PaginationWork();
		$totalCount=0;
		$backupData=$backup->getBackups($totalCount);
		$CFG->template="backup/backupList.php";	
	break;
	
	case 'restore':
		$backupSingalData=$DB->selectRecord("backup_data","id=".Decode($getVars['id']));
		//to check current log status		
		$CFG->template="backup/restoreBackup.php";
		break;
		
	case 'upload':
		$CFG->template="backup/upload.php";
		break;
			
	case 'checkConditions':
		include_once("checkconditions.php");
		exit;
		break;
				
	default:
		PaginationWork();
		$totalCount=0;
		$backupData=$backup->getBackups($totalCount);
		//to check that if file is to be downloaded
		if(isset($_SESSION['u']))
		{
			$tempVar=$_SESSION['u'];
			unset($_SESSION['u']);
			
		}
		$CFG->template="backup/backupList.php";
	
}

		
include(CURRENTTEMP."/index.php");
?>