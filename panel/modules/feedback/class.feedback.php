<?php 
/**
 * 	@author	:	Ashwini Agarwal
 * 	@desc	:	Handle feedback calls
 */

class Feedback
{
	/**
	 * 	@author	:	Ashwini Agarwal
	 * 	@desc	:	Save feedback reply
	 */
	function saveFeedbackReply($nameID)
	{
		global $DB, $frmdata;
		
		if($frmdata['mailmessage'] == '')
		{
			$mail_error = true;
			$_SESSION['error'] = 'Please enter message.';
		}
		else
		{
			include_once(ROOT.'/lib/mailer.php');
			$mail = new mailer();
			$mail->addTo($feedback->email);
			$mail->setSubject('Feedback Reply');
			$mail->setMessage(stripslashes($frmdata['mailmessage']));
			$mail->send();
					
			$mail = array();
			$mail['admin_name'] = $_SESSION['admin_user_name'];
			$mail['message'] = $frmdata['mailmessage'];
			$mail['feedback_id'] = $nameID;
			
			$DB->InsertRecord('feedback_reply', $mail);
			
			$_SESSION['message'] = 'Mail has been successfully send.';
			Redirect(CreateURL('index.php','mod=feedback&do=preview&nameID='.$nameID));
			exit();
		}
	}
	
	/**
	 * 	@author	:	Ashwini Agarwal
	 * 	@desc	:	Get editor instanse
	 */
	function getEmailEditor()
	{
		include_once(ROOT.'/lib/wysiwygPro/wysiwygPro.class.php');
		$editor = new wysiwygPro();
		$editor->name = 'mailmessage';
		$editor->imageDir = ROOT ."/media/";
		$editor->mediaDir = ROOT ."/media/";
		$editor->imageURL = ROOTURL.'/media/';
		$editor->mediaURL = ROOTURL.'/media/';
		$editor->urlFormat = 'absolute';
		$editor->addFont('Calibri');
		$editor->editImages = true;
		$editor->renameFiles = true;
		$editor->renameFolders = true;
		$editor->deleteFiles = true;
		$editor->deleteFolders = true;
		$editor->copyFiles = true;
		$editor->copyFolders = true;
		$editor->moveFiles = true;
		$editor->moveFolders = true;
		$editor->upload = true;
		$editor->overwrite = true;
		$editor->createFolders = true;
		
		return $editor;
	}
	
	/*************************************************************************
	 * 	Author		:	Ashwini Agarwal
	 * 	Date		:	November 3, 2012
	 * 	Description	:	Get feedback data.
	 */
	function getRecordFeedback(&$totalCount, $id = '')
	{
		global $DB,$frmdata;
		
		$query='';
		$query="select test.test_name, CONCAT_WS(' ', first_name, last_name) as candidate_name, email, feedback.* ";
		
		$query.=" from ".PREFIX."feedback
				LEFT JOIN ".PREFIX."test on test.id = feedback.test_id
				left join candidate on candidate.id=feedback.candidate_id where 1 and ";
		
		if($id != '')
		{
			$query .= " (feedback.id = '$id') and ";
		}
		
		if(isset($frmdata['candidate_name']) && $frmdata['candidate_name'] !='')
		{
			$query.="(";
			$query.=" CONCAT_WS(' ', first_name, last_name) like '%".$frmdata['candidate_name']."%'" ; 
			$query.=")";
			$query.=" and";	
		}
		
		if(isset($frmdata['test_name']) && $frmdata['test_name'] !='')
		{
			$query.="(";
			$query.=" test_name like '%".$frmdata['test_name']."%'" ; 
			$query.=")";
			$query.=" and";	
		}
		
		$query=substr($query,0,(strlen($query)-4));
		
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{
			$query.=" order by ".$frmdata['orderby'];
		}	
		else
		{
			$query.=' order by id desc';
		}
		
		//echo $query;	
		
		if($id != '')
		{
			$result = $DB->RunSelectQuery($query);
		}
		else
		{
			$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		}
		// print_r($result);exit;
		return $result;   
	}
}
?>