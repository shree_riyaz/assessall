<?php 
/*****************************************************************************************
 * 		Developed by :- Ashwini Agarwal
 * 		Date         :- November 2, 2012
 * 		Module       :- Feedback
 * 		Purpose      :- Entry file for any action, call in feedback module
 ****************************************************************************************/
defined("ACCESS") or die("Access Restricted");
$DB = new DBFilter();

include_once('class.feedback.php');
$FeedbackOBJ= new Feedback();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($frmdata['sendmail']))
{
	$FeedbackOBJ->saveFeedbackReply($nameID);			
}

$do = '';
if(isset($getVars['do']))
$do=$getVars['do'];

switch($do)
{
	case 'manage':
	default:
		PaginationWork();
		$totalCount=0;			
		$list= $FeedbackOBJ->getRecordFeedback($totalCount);
	    $CFG->template="feedback/manage.php";
		break; 	

	case 'preview':
		$DB->UpdateRecord('feedback', array('is_read'=>1), "id='$nameID'");
		$feedback = $FeedbackOBJ->getRecordFeedback($totalCount, $nameID);
		$feedback = $feedback[0];
		$reply = $DB->SelectRecords('feedback_reply','feedback_id="'.$nameID.'"');
		
		$editor = $FeedbackOBJ->getEmailEditor();			
		$CFG->template="feedback/preview.php";
	break;
			
	case 'delete':
		$DB->DeleteRecord('feedback','id="'.$nameID.'"');
		$DB->DeleteRecord('feedback_reply','feedback_id="'.$nameID.'"');
		$_SESSION['success']="Record has been deleted successfully.";
		Redirect(CreateURL('index.php','mod=feedback&do=manage'));
	exit();
} 

include(CURRENTTEMP."/index.php");
exit;
?>