<?php
/*****************Developed by :- Chirayu Bansal
	                Date         :- 26-july-2011
					Module       :- preferences
					Purpose      :- Class for function to edit preferences
***********************************************************************************/

class Preferences
{

//==========================================================================
// to edit preferences information
//==========================================================================
	function editPreferences()
	{
		
		//echo $nameID;
		global $DB,$frmdata;
		//print_r($frmdata);//exit;
		$err = '';

		$err ='';	
		if (isset($frmdata['middle-text']) && $frmdata['middle-text'] != '')
		{
			$this->updateOption('middle-text', $frmdata['middle-text']);
		}
		else
		{
			$err .= 'Please fill middle text.';
		}
		if (isset($frmdata['font-size']) && $frmdata['font-size'] !='')
		{
			$this->updateOption('font-size', $frmdata['font-size']);
		}
		if (isset($frmdata['color']) && $frmdata['color'] !='')
		{
			$this->updateOption('color', $frmdata['color']);
		}
		
		if($frmdata['remove-left'] == true)
		{
			$this->updateOption('left-logo', '');
		}
		elseif (isset($_FILES['left-logo']) && $_FILES['left-logo']['name'] != '')
		{
			$this->uploadLogo('left-logo',$err);
		}
		
		if($frmdata['remove-right'] == true)
		{
			$this->updateOption('right-logo', '');
		}
		elseif (isset($_FILES['right-logo']) && $_FILES['right-logo']['name'] != '')
		{
			$this->uploadLogo('right-logo',$err);
		}
		if ( $err == '')
		{
			$_SESSION['success'] = 'Configuration settings have been saved successfully.';	
			Redirect(CreateURL('index.php','mod=preferences&do=view'));
			die();
		}
		else
		{
			$_SESSION['error'] = $err;
		}
	}
	//upload the logo
	function uploadLogo($val, &$err)
	{		
		if($_FILES[$val]['type']!='image/jpeg' && $_FILES[$val]['type']!='image/jpg' && $_FILES[$val]['type']!='image/gif' && $_FILES[$val]['type']!='image/png' && $_FILES[$val]['type']!='image/pjpeg' && $_FILES[$val]['type']!='image/x-png')
		{
			$err .= "Please upload logo in correct format.<br>";
		}
		elseif($_FILES[$val]['size'] > 2000000)
		{
			$err .= "Please upload image not more than 2MB.<br>";
		}
		else
		{
			$path_parts = pathinfo($_FILES[$val]['name']);	
			$fileName = $path_parts['basename'] . '_' . time() . '.' . $path_parts['extension'];	
			$uploadPath = FILEPATH . $fileName;	
			//$img = new thumb_image;
			//$img->GenerateThumbFile($_FILES[$val]['tmp_name'], $uploadPath);		
			$tempFilePath = FILEPATH.'tmp'.time().'.'.$path_parts['extension'];
			if (copy($_FILES[$val]['tmp_name'], $tempFilePath))
			{			
				$resizeObj = new resize($tempFilePath);				
				// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
				$resizeObj -> resizeImage(189, 151, 'exact');		
				// *** 3) Save image
				$resizeObj->saveImage($uploadPath, 100);				
				unlink($tempFilePath); //delete original file
				//update the database record for new logo
				$this->updateOption($val, $fileName);
			}						
			
			
		}
		
	}
	/*
		Check if the configuration option is exists, if exists update with the new value other wise insert the option
	*/	
	function updateOption($option, $value)
	{
		global $DB;
		$exists = $DB->SelectRecord('default_configuration', 'conf_variable ="'.$option.'"');
		if ($exists)
		{
			$data['variable_value'] = $value;
			$DB->UpdateRecord('default_configuration', $data, 'conf_variable ="'.$option.'"');
		}
		else
		{
			$data['conf_variable'] = $option;
			$data['variable_value'] = $value;
			$DB->InsertRecord('default_configuration', $data);
		}
	}
	/*
		get the configuration option value
		return the value of option if not blank, FALSE otherwise
	*/
	function getOptionValue($option)
	{
		global $DB;
		$info = $DB->SelectRecord('default_configuration', 'conf_variable ="'.$option.'"');
		if ($info->variable_value !='')
		{
			return $info->variable_value;
		}
		else
		{
			return false;
		}
	}
	
	/******************************************************************
	Des: A function to get configuration information
	******************************************************************/
	function getConfiguration()
	{
		global $DB,$frmdata;
		$query = "select * from default_configuration";
		//exit;			
		$result = $DB->RunSelectQuery($query);
		return $result;
	}
	//==========================================================================	
}//end of class
?>