<?php 
/*****************Developed by :- Chirayu Bansal
	                Date         :- 26-july-2011
					Module       :- Preferences
					Purpose      :- Entry file for any action, call in preferences module
***********************************************************************************/

defined("ACCESS") or die("Access Restricted");
include_once('class.preferences.php');	
include_once(ROOT.'/lib/resize-class.php');	
$DB = new DBFilter();
$PreferencesOBJ= new Preferences();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($frmdata['update']))
{
	$PreferencesOBJ->editPreferences();	
}

if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}

$conflist = $PreferencesOBJ->getConfiguration();
switch($do)
{  	
		default:
		case 'view':						
		    $CFG->template="preferences/view.php";
			break; 				
		case 'edit':		
		   $CFG->template="preferences/edit.php";
		   break;		
} 
include(CURRENTTEMP."/index.php");
?>