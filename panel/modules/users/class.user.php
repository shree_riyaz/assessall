<?php	
//This class have CRM user related functions
class User
{
	//==========================================================================
	//to add a user 
	//==========================================================================	
	function addUser()
	{
		
		global $DB,$frmdata;
		//echo $frmdata['name'];exit; 
		$Err = '';
		
		if($frmdata['user_name'] == '')
		{
			$Err .= 'Please enter user name.<br>';
		}
		
		if($frmdata['name'] == '')
		{
			$Err .= 'Please enter name.<br>';
		}
		
		if($frmdata['role_id'] == '')
		{
			$Err .= 'Please select role.<br>';
		}
		
		if ($frmdata['password']=='')
		{
			 $Err.="Please enter password.<br>";
		}
		else
		{
			if($frmdata['repass']=='')
			{
				$Err.="Please enter confirm password.<br>";
			}
			elseif($frmdata['repass']!=$frmdata['password'])
			{
				$Err.="Confirm password doesn't match with password.<br>";
			}
		}
		
		if ($Err != '')
		{
			 $_SESSION['error'] = $frmdata['message']['text']=$Err;
		} 
		else
		{
			$errorMessage='';
			if ($frmdata['email'] != '')
			{
				//check for duplicate email
				if ($emailExist=$DB->SelectRecord('admin_users',"email='".$frmdata['email']."'"))
				{
					$errorMessage="Email already exist<br>";
					$frmdata['email']='';
				}
			}	
			if ($nameExist=$DB->SelectRecord('admin_users',"user_name='".$frmdata['user_name']."'"))
			{
				$errorMessage.="User Name already exists. Please choose a different name.<br>";
				
			}
			//check for passwords
			if ($frmdata['password']!=$frmdata['repass'])
			{
				$errorMessage.="Re-entered password does not match with password<br>";
			}
			
			if ($errorMessage == '')
			{
				$user_name = $frmdata['user_name'];
				$pass = $frmdata['password'];
				$frmdata['password']=Encrypt($frmdata['password']);
				$recordID=$DB->InsertRecord('admin_users',$frmdata);
				$_SESSION['success']="User has been inserted successfully.<br>User name is ".$user_name." and Password is ".$pass;
				Redirect(CreateURL('index.php','mod=users'));
				die();
				
			}
			else
			{
				
				$_SESSION['error'] =$errorMessage;
			}
			
		}	
	}
	
	//==========================================================================
	
	
	//==========================================================================
	// to edit user information
	//==========================================================================
	function editUser($nameID)
	{
		global $DB,$frmdata;		
		$user= $DB->SelectRecord('admin_users',"id=".$nameID,'*');
		//Do common validation
		$Err = '';
		if($frmdata['name'] == '')
		{
			$Err .= 'Please enter name.<br>';
		}
		
		if($frmdata['role_id'] == '')
		{
			$Err .= 'Please select role.<br>';
		}
			
		if ($Err != '')
		{
			 $_SESSION['error'] = $frmdata['message']['text']=$Err;
		} 
		else
		{
			$errorMessage='';
			//check for duplicate email address
			if ($frmdata['email'] != '')
			{
				if ($emailExist = $DB->SelectRecord('admin_users',"email='".$frmdata['email']."' and id !='".$nameID."'"))
				{
					$errorMessage="Email already exist<br>";
				}
			}				
			if ($errorMessage == '')
			{
				if($DB->UpdateRecord('admin_users',$frmdata,'id="'.$nameID.'"'))
				{
					$_SESSION['success']="User has been updated successfully";
					Redirect(CreateURL('index.php','mod=users'));
					die();	
				}				
			}
			else
			{
				 $_SESSION['error'] = $errorMessage;
			}
		}		
	}
	
//==========================================================================
	// to change password of a user account
//==========================================================================
	function changePassword($nameID)
	{
		global $DB,$frmdata;		
		if($frmdata['password'] == '')
		{
			$err .= "Please enter password.<br>";
		}
		if ($_POST['repass'] == '')
		{
			$err.="Please enter Retype password.";
		}
		if($err == '')
		{
			if (Encrypt($_POST['password']) != Encrypt($_POST['repass']))
			{
				$err.="Re-entered password does not match with password.";
			}
		}	
		
		if($err)
		{
			$_SESSION['error'] = $err;
			return false;
		}
		
		$DB->ExecuteQuery("update admin_users set password='".Encrypt($frmdata['password'])."' where id=".$nameID);
		$member = $DB->SelectRecord('admin_users','id="'.$nameID.'"');	   
		
		$_SESSION['success']="Password for ".$member->user_name." has been changed successfully.";	
		Redirect(CreateURL('index.php','mod=users'));
		die();		
	}
	
	
	//==========================================================================
	// to enable user account
	//==========================================================================
	function enableUser()
	{
		global $DB, $frmdata;		
		//gather user information
		$userInfo=$DB->selectRecord('admin_users','id='.$frmdata['actUserID']);
					
		if($userInfo->disabled=='N')
		{
			$_SESSION['error']="User is already enabled.";
		}
		else
		{ 	
			$data['disabled']='N';			
			$DB->updateRecord('admin_users',$data,'id='.$frmdata['actUserID']);				
			$_SESSION['success'] = "User has been enabled successfully.";
					
		}	   
	}

	//==========================================================================
	//  to disabled a user account
	//==========================================================================
	function disableUser()
	{
		global $DB, $frmdata;
	
		$userInfo=$DB->SelectRecord('admin_users','id='.$frmdata['actUserID']);
		$data['disabled']='Y';		
	    if($userInfo->disabled=='Y')
	    {
			 $_SESSION['error']="User is already disabled.";
	    }
	    else
	    {
			//disable the super user
			$DB->UpdateRecord('admin_users',$data,'id='.$frmdata['actUserID']);			
			//if super user show a different message
			$_SESSION['success']="User has been disabled successfully.";
					
	    }	
  }
	//==========================================================================	
}
?>