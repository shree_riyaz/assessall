<?php 
defined("ACCESS") or die("Access Restricted");

include_once('class.user.php');		

$DB = new DBFilter();
$userOBJ= new User();
//================ For getting URL variables =============
if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}
else
{
	$nameID='';
}

if(isset($getVars['mes']))
{
		$frmdata['message']=$getVars['mes'];
}

//=======To clear search when clearsearch button is clicked =====
if(isset($frmdata['clearsearch']))
{
		unset ($frmdata);
		unset($_SESSION['pageNumber']);
		$CFG->template="user/user.php";
}

//========== Work for adding file ===============
//$userFlag=0;
if(isset($frmdata['adduser']))
{
	$userOBJ->addUser();	
 
}
	
//============= Work for Editing file ==============
if(isset($frmdata['edituser']))
{
	
	$userOBJ->editUser($nameID);
}




//================ Work for Change Password =====
if (isset($frmdata['changepass']))
{
	$userOBJ->changePassword($nameID);
} 
	
//================ Work for unable file ==============
if(isset($frmdata['enableuser']))
{
	$userOBJ->enableUser();	
} 
//================ Work for disable file =========


if(isset($frmdata['disableuser']))
{
	$userOBJ->disableUser();			
} 

switch($do)
{
		case 'add':		 
            $role= $DB->SelectRecords('role',"roleName!='Admin'",'*','order by roleID');
			$CFG->template="users/adduser.php";
			break;
		case 'edit':
		   
		    $contsInfo=$DB->SelectRecord('admin_users',"(id='$nameID') AND (user_name != 'admin')");		
 			
		    if($contsInfo == '')
		    {
			    Redirect(CreateURL('index.php',"mod=users"));
			    die();
		    }
			
		    $role= $DB->SelectRecords('role',"roleName!='Admin'",'*','order by roleName');
		
			$CFG->template="users/edituser.php";
		   break;
		   	
		case 'Change':
		    $contsInfo=$DB->SelectRecord('admin_users',"(id='$nameID') AND (user_name != 'admin')");
			//print_r($contsInfo);//exit;
		
		    if($contsInfo == '')
		    {
			    Redirect(CreateURL('index.php',"mod=users"));
			    die();
		    }
		    
		    $CFG->template="users/cpass.php";
			break; 	
		case 'cpass':
			$contsInfo=$DB->SelectRecord('admin_users','user_name="admin"');
			$CFG->template="users/cpass.php";
			break;	
			
	    default:
			if($frmdata['searchuser'])
			{
				$frmdata['pageNumber']=1;
			}
			
			PaginationWork();
			$totalCount=0;			
			$userlist= getRecordCRMUser($totalCount);
			$CFG->template="users/user.php";
			break;
} 	
		
include(CURRENTTEMP."/index.php");

function getRecordCRMUser(&$totalCount)
{
	
	global $DB,$frmdata;
	$query='';
	$query=" select us.id, user_name, us.name,
				   us.email,roleName,
				   us.disabled ";
	
	
		$query.=" from ".PREFIX."admin_users us 
			
				left join ".PREFIX."role bk on us.role_id= bk.roleID ";  
	$query.=" where  user_name != 'admin' and ";			
	if(isset($frmdata['keywords']) && $frmdata['keywords']!='')
	{
		$query.=" (";
		$query.=" us.name like '%".$frmdata['keywords']."%' or us.email like '%".$frmdata['keywords']."%' or us.user_name like '%".$frmdata['keywords']."%'" ;  $query.=") ";
		$query.=" and";	
	}
	
	
	if($frmdata['roleName']!='')
	{
		$query.=" roleName='".$frmdata['roleName']."'";
		$query.=" and ";	
	}		
		
	$query=substr($query,0,(strlen($query)-5));
	$query.=" and us.role_id < 6 group by us.id "; 
	
	if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
	{
		$query.=" order by ".$frmdata['orderby'];
	}	
	//echo $query;
	$result = $DB->RunSelectQueryWithPagination($query,$totalCount);	
	return $result;   
}
?>