<?php 
/*****************Developed by :- Chirayu Bansal
	                Date         :- 18-aug-2011
					Module       :- Stream master
					Purpose      :- Entry file for any action, call in Subject master module
***********************************************************************************/

defined("ACCESS") or die("Access Restricted");
include_once('class.subject_master.php');		
$DB = new DBFilter();
//print_r($frmdata);
$SubjectMasterOBJ= new SubjectMaster();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($frmdata['addsubject']))
{
	$SubjectMasterOBJ->addSubject();	
}

if(isset($frmdata['editsubject']))
{
	$SubjectMasterOBJ->editSubject($nameID);	
}

if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}
switch($do)
{
		case 'add':
		   $CFG->template="subject_master/add.php";
		   break;
		
		default:	   	
		case 'manage':
		
			PaginationWork();
			$subjectlist= $SubjectMasterOBJ->getRecordSubject($totalCount);
		    $CFG->template="subject_master/manage.php";
			break; 	
			
		case 'edit':	
		   $subject_detail=$DB->SelectRecord('subject','id='.$nameID);
		   $CFG->template="subject_master/edit.php";
		   break;
	    
		case 'delete':
			
			$question = $DB->SelectRecords('question', 'subject_id='.$nameID, 'count(*) as que');
			if($question[0]->que>0)
			{
				if($question[0]->que>1)
					$_SESSION['error']="There are ".$question[0]->que." questions of this subject in the question bank. First delete these questions.";
				elseif ($question[0]->que==1)
					$_SESSION['error']="There is ".$question[0]->que." question of this subject in the question bank. First delete this question.";
			}
			else 
			{
				$DB->DeleteRecord('subject','id="'.$nameID.'"');
				$_SESSION['success']="Record has been deleted successfully.";
			}
			Redirect(CreateURL('index.php','mod=subject_master&do=manage'));die();
} 
include(CURRENTTEMP."/index.php");

exit;

?>