<?php
/*****************Developed by :- Chirayu Bansal
	                Date         :- 18-aug-2011
					Module       :- Subject master
					Purpose      :- Class for function to add and edit Subject details
***********************************************************************************/

class SubjectMaster
{
	//==========================================================================
	//to add a Subject
	//==========================================================================	
	function addSubject()
	{
		global $DB,$frmdata;
		//print_r($frmdata);exit;
		$err='';
		
			
		if ($frmdata['subject_name']=='')
		{
			 $err.="Please enter subject name.<br>";
		}
		
		if ($frmdata['include_stream']=='Y' && $frmdata['stream_id']=='')
		{
			 $err.="Please select stream.<br>";
		}
		elseif ($frmdata['subject_name']!='')
		{
			 $exist=$DB->SelectRecord('subject',"subject_name='".addslashes($frmdata['subject_name'])."'");
			 //print_r($exist);exit;
			 if($exist->subject_name!='')
			 {
				$err.="This subject is already exist.<br>";
			 }
		}
		
		if($err=='')
		{
			if($frmdata['stream_id']=='')
			{
				$frmdata['stream_id'] = 'NULL';
			}
			
			$DB->InsertRecord('subject',$frmdata);
			$_SESSION['success']="Subject has been added successfully.";
			$frmdata='';
			Redirect(CreateURL('index.php','mod=subject_master&do=manage'));
			exit;
		}
		elseif($err!='')
		{
			$_SESSION['error']=$err;
		}
		
		
	}	
	
//==========================================================================
// to edit Subject information
//==========================================================================
	function editSubject($nameID)
	{
		//echo $nameID;
		global $DB,$frmdata;
		//print_r($frmdata);//exit;
		$err='';
		
		if ($frmdata['subject_name']=='')
		{
			 $err.="Please enter subject name.<br>";
		}
		
		if ($frmdata['include_stream']=='Y' && $frmdata['stream_id']=='')
		{
			 $err.="Please select stream.<br>";
		}
		elseif(!(isset($frmdata['include_stream'])) && $frmdata['stream_id']!='')
		{
			$frmdata['stream_id']='';
			$frmdata['include_stream']='N';
		}
		
		if ($frmdata['subject_name']!='')
		{
			 $exist=$DB->SelectRecord('subject',"subject_name='".addslashes($frmdata['subject_name'])."' and id!=".$nameID);
			 if($exist->subject_name!='')
			 {
				$err.="This subject is already exist.<br>";
			 }
		}
		
		if($err=='')
		{
			if($frmdata['stream_id']=='')
			{
				$frmdata['stream_id'] = 'NULL';
			}
			$DB->UpdateRecord('subject',$frmdata,'id="'.$nameID.'"');
			$_SESSION['success']="Subject has been updated successfully.";
			$frmdata='';
			Redirect(CreateURL('index.php','mod=subject_master&do=manage'));
			exit;
		}
		elseif($err!='')
		{
			$_SESSION['error']=$err;
		}
	}
		
	/******************************************************************
	Des: A function to get subject information
	******************************************************************/
	function getRecordSubject(&$totalCount)
	{
			
		global $DB,$frmdata;
		//print_r($frmdata);
		$cond = 0;
		$query='';
		$query="select sj.* from subject as sj ";
		
		if(trim($frmdata['subject_name_manage'])!='')
		{
			if($cond==0)
				$query.=" where ";
			else
				$query.="and ";
			$query.="sj.subject_name like '%".addslashes($frmdata['subject_name_manage'])."%'";
			$cond++;
		}
		
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{
	  		$query.=" order by ".$frmdata['orderby'];
		}	
		else
		{
			$query.=' order by id desc';
		}
	 	//echo $query;//exit;
		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		// print_r($result);exit;
		return $result;   
	}
		
	//==========================================================================	
}//end of class
?>