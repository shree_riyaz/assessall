<?php 
defined("ACCESS") or die("Access Restricted");

include_once('class.role.php');
$RoleOBJ = new Role();		
$DB = new DBFilter();

$do = '';
if(isset($getVars['do']))
$do=$getVars['do'];

$nameID = '';
if(isset($getVars['nameID']))
$nameID=$getVars['nameID'];

if(isset($getVars['mes']))
$frmdata['message'] = $getVars['mes'];

if(isset($frmdata['clearsearch']))
{
	unset ($frmdata);
	unset($_SESSION['pageNumber']);
	$CFG->template="role/role.php";
}

if(isset($frmdata['addrole']))
$RoleOBJ->addRole();

if(isset($frmdata['editrole']))
$RoleOBJ->editRole($nameID);

$modules = $DB->SelectRecords('module');
switch($do)
{
	case 'add':		 
		$CFG->template="role/addrole.php";
	break;
		
	case 'edit': 
		//get the record id of permission type r,a,e
		$readInfo = $DB->SelectRecord('permission','permissionName="R"');  			
		$read = $readInfo->permissionID;
		/*************************************************/

		$addInfo = $DB->SelectRecord('permission','permissionName="A"');  			
		$add = $addInfo->permissionID;
		/*************************************************/
		
		$editInfo = $DB->SelectRecord('permission','permissionName="E"');  			
		$edit = $editInfo->permissionID;
		/*************************************************/
		
		$exportInfo = $DB->SelectRecord('permission','permissionName="ED"');  			
		$export = $exportInfo->permissionID;

		/*************************************************/
		$role= $DB->SelectRecord('role',"roleID=".$nameID);
		$CFG->template="role/editrole.php";
	break;			

	case 'del':
		$role = $DB->SelectRecord('role','roleID = '.$nameID);
		$roleAttached = $DB->SelectRecord('admin_users','role_id = '.$nameID);
		if ($roleAttached)
		{
			$_SESSION['error'] = 'Role could not be deleted. Role is assigned to one or more users.';
		}
		elseif (in_array($role->roleName, array('Teacher', 'Parent')))
		{
			$_SESSION['error'] = 'You cannot delete teacher and parent roles.';
		}
		else
		{
			$DB->DeleteRecord('role','roleID = '.$nameID);
			$DB->DeleteRecord('rolepermission','roleID = '.$nameID);
			$_SESSION['success'] = 'Role has been deleted successfully.';		
		}
		Redirect(CreateURL('index.php','mod=role'));
	exit();
	
	default:
		if($frmdata['searchrole'])
		{
			$frmdata['pageNumber']=1;
		}			
		PaginationWork();
		$totalCount=0;			
		$rolelist= $RoleOBJ->getRole($totalCount);
		$CFG->template="role/role.php";
	break;
} 	
		
include(CURRENTTEMP."/index.php");
?>