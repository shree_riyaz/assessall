<?php 
/**
 * 	@author	:	Ashwini Agarwal
 * 	@desc	:	Handle calls for role
 */
class Role
{
	/**
	 * 	@author	:	Ashwini Agarwal
	 * 	@desc	:	Add new Role
	 */
	function addRole()
	{
		global $DB, $frmdata;
		//add the role and the permissions to that role
		$err = '';
		
		if ($frmdata['roleName'] =='')
		$err .= "Please enter role name.<br>";

		if (empty($frmdata['modules']))
		$err .= "Please define the module access.<br>";

		if ($frmdata['roleName'] != '')
		{
			$roleExists = $DB->SelectRecord('role', 'roleName ="'.$frmdata['roleName'].'"');
			if ($roleExists->roleName !='')
			{
				$err.= 'Role name already exists.';
			}
		}
		
		if($err)
		{
			$_SESSION['error'] = $err;
			return false;
		}
		
		//validation passed add the role and permission on their respective db tables
		$roleID = $DB->InsertRecord('role', $frmdata);		
		foreach($frmdata['modules'] as $moduleID => $value)
		{
			//add the read permission record 
			$permissionInfo = $DB->SelectRecord('permission','permissionName="R"');
			$data['roleID'] = $roleID;
			$data['permissionID'] = $permissionInfo->permissionID;
			$data['moduleID'] = $moduleID;
			$DB->InsertRecord('rolepermission',$data);
			
			if(is_array($value))
			{				
				//insert the add, edit permission records
				foreach ($value as $permission)
				{
					$permissionInfo = $DB->SelectRecord('permission','permissionName="'.$permission.'"');
					$data['roleID'] = $roleID;
					$data['permissionID'] = $permissionInfo->permissionID;
					$data['moduleID'] = $moduleID;
					$DB->InsertRecord('rolepermission',$data);
				}	
			}			
		}
		$_SESSION['success'] = 'Role has been added successfully.'; 
		Redirect(CreateURL('index.php','mod=role')); 
		exit();
	}
	
	/**
	 * 	@author	:	Ashwini Agarwal
	 * 	@desc	:	Edit Role
	 */
	function editRole($nameID)
	{
		global $DB, $frmdata;
		//edit the role and the permissions to that role
		$err = '';
		if ($frmdata['roleName'] =='')
		$err .= "Please enter role name.<br>";

		if (empty($frmdata['modules']))
		$err .= "Please define the module access.<br>";
		
		if ($frmdata['roleName'] != '')
		{
			$roleExists = $DB->SelectRecord('role', 'roleName ="'.$frmdata['roleName'].'" and roleID != '.$nameID);
			if ($roleExists->roleName !='')
			{
				$err.= 'Role name already exists.';
			}
		}	
		
		if($err)
		{
			$_SESSION['error'] = $err;
			return false;
		}
		
		//validation passed add the role and permission on their respective db tables
		$roleID = $nameID; 		
		$DB->UpdateRecord('role', $frmdata,'roleID = '.$roleID);
		// delete the old permission records for this role
		$DB->DeleteRecord('rolepermission', 'roleID='.$roleID);
		foreach($frmdata['modules'] as $moduleID => $value)
		{
			//add the read permission record 
			$permissionInfo = $DB->SelectRecord('permission','permissionName="R"');
			$data['roleID'] = $roleID;
			$data['permissionID'] = $permissionInfo->permissionID;
			$data['moduleID'] = $moduleID;
			$DB->InsertRecord('rolepermission',$data);
			
			if(is_array($value))
			{				
				//add the add, edit permission records
				foreach ($value as $permission)
				{
					$permissionInfo = $DB->SelectRecord('permission','permissionName="'.$permission.'"');
					$data['roleID'] = $roleID;
					$data['permissionID'] = $permissionInfo->permissionID;
					$data['moduleID'] = $moduleID;
					$DB->InsertRecord('rolepermission',$data);
				}	
			}			
		}
		$_SESSION['success'] = 'Role has been updated successfully.'; 
		Redirect(CreateURL('index.php','mod=role')); 
		exit();
	}

	function getRole(&$totalCount)
	{
		global $DB,$frmdata;	
		$query=" SELECT * FROM role ";
			
		if($frmdata['role'] !='')
		{
			$query.=" WHERE roleName LIKE '%".$frmdata['role']."%'";
		}
			
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{
			$query.=" order by ".$frmdata['orderby'];
		}
		else 
		{
			$query.=" order by roleID ";
		}	
		//echo $query;
		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);	
		return $result;   
	}
}
?>