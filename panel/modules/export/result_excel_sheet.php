<?php

chdir(ROOT . '/lib/phpxls');
require_once('Writer.php');
chdir('');

$Heading =  array(
		array('S/No.', 'Fisrt Name', 'Last Name'),
	);

$part1_flag = 0;
$part2_flag = 0;
$second_heading=array('', '', '');
$third_heading = array('', '', '');
$fourth_heading = array('', '', '');
$subject_first_heading_colspan = 0;
$part1_colspan=0;
$part2_colspan=0;
$total_marks=0;
	
//=====================First Row of Heading==============================================
array_push($Heading[0], 'Details Of Subjects');

for($index=0; $index<count($exam_subject); $index++)
{
	$total_marks+=$exam_subject[$index]->subject_max_mark;
	$subject_first_heading_colspan+=2;
	array_push($fourth_heading, 'Written ('.$exam_subject[$index]->subject_max_mark.')');
	array_push($Heading[0], '','');
	array_push($third_heading, ucfirst(strtolower(stripslashes($exam_subject[$index]->subject_name))), '');
	array_push($second_heading, '');
	array_push($fourth_heading, 'Result');
}
array_pop($Heading[0]);
$subject_first_heading_colspan-=1;
$part1_colspan-=1;
$part2_colspan-=1;

array_push($Heading[0], 'Details Of Questions','','','','','','','','');
array_push($second_heading, '','','','','','','','','');
array_push($third_heading, 'Beginner','','');
array_push($fourth_heading, 'Correct','Wrong','Skipped');
array_push($third_heading, 'Intermediate','','');
array_push($fourth_heading, 'Correct','Wrong','Skipped');
array_push($third_heading, 'Higher','','');
array_push($fourth_heading, 'Correct','Wrong','Skipped');

array_push($Heading[0], 'Total Marks Obtain ('.$total_marks.')', 'Percentage (%)', 'Result');

array_push($second_heading, '', '');
array_push($third_heading, '', '');
array_push($fourth_heading, '', '');

array_push($Heading, $second_heading, $third_heading, $fourth_heading);

//echo "<pre>";print_r($Heading);exit;
$workbook = new Spreadsheet_Excel_Writer();
$main_heading_format =& $workbook->addFormat();
$main_heading_format->setBold();
$main_heading_format->setColor('black');
$main_heading_format->setFontFamily('verdana,sans-serif');
$main_heading_format->setAlign('Center');
$main_heading_format->setVAlign('top');
$main_heading_format->setUnderline(1);

$heading_format =& $workbook->addFormat();
$heading_format->setBold();
$heading_format->setColor('black');
$heading_format->setFontFamily('verdana,sans-serif');
$heading_format->setSize(8);
$heading_format->setAlign('Center');
$heading_format->setVAlign('top');
$heading_format->setTextWrap(1); 
$heading_format->setBorder(1);   
$heading_format->setBottom(2);//thick

$data_format =& $workbook->addFormat();
$data_format->setColor('black');
$data_format->setFontFamily('verdana,sans-serif');
$data_format->setSize(7);
$data_format->setTextWrap(1);
$data_format->setAlign('Center');
$data_format->setVAlign('top');
$data_format->setBorder(1); 

$basic_format =& $workbook->addFormat();

$basic_format->setFontFamily('verdana,sans-serif');
$basic_format->setAlign('right');
$basic_format->setVAlign('bottom');

$arr = array('Result'=>$Heading);

//===========print heading ==============================
foreach($arr as $wbname=>$rows)
{
    $rowcount = count($rows);
    $colcount = count($rows[0]);

    $worksheet =& $workbook->addWorksheet($wbname);
	{
		$worksheet->setMerge(3, 0, 6, 0);
		$worksheet->setMerge(3, 1, 6, 1);
		$worksheet->setMerge(3, 2, 6, 2);
		
		$worksheet->setMerge(3, 3, 3, 3+$subject_first_heading_colspan);
		$worksheet->setMerge(4, 3, 4, 2);
	
		// merge cell for subject name
		$current_column = 3;
		for($index=0; $index<count($exam_subject); $index++)
		{
			$worksheet->setMerge(5, $current_column, 5, $current_column+$subject_name_td_colspan);
			$current_column+=2;
		}
		
		$worksheet->setMerge(3, $current_column, 3, $current_column+8);
		$worksheet->setMerge(5, $current_column, 5, $current_column+2);
		$worksheet->setMerge(5, $current_column+3, 5, $current_column+5);
		$worksheet->setMerge(5, $current_column+6, 5, $current_column+8);
		
		$worksheet->setMerge(3, $current_column+9, 6, $current_column+9);
		$worksheet->setMerge(3, $current_column+10, 6, $current_column+10);
		$worksheet->setMerge(3, $current_column+11, 6, $current_column+11);
		
		// for colspan main heading
		$worksheet->setMerge(0, 0, 0, 15+$subject_first_heading_colspan);
		
		if($test_id=='')
		{
			$main_heading_name = 'RESULT SHEET OF '.strtoupper(stripslashes($exam_detail->exam_name));
			$worksheet->write(0, 0, $main_heading_name, $main_heading_format);
		}
		else
		{
			$main_heading_name = 'RESULT SHEET OF '.strtoupper(stripslashes($test_info[0]->test_name));
			$worksheet->write(0, 0, $main_heading_name.' TEST HELD ON '.date('d-M-Y',strtotime($test_info[0]->test_date)), $main_heading_format);
		}
		
		$worksheet->setColumn(1,$colcount, 9);
	}
    
    $row=3;
    for($counter=0; $counter<$rowcount; $counter++ )
    {
    	for($cols=0; $cols<$colcount;$cols++)
        {
                $data=$rows[$counter][$cols];
                $worksheet->write($row, $cols, $data, $heading_format);
           
        }
        $row++;
    }
}

// start candidate data
$count = count($subject_result);
if($subject_result)
{
	$report_data = array();
	for($counter=0; $counter<$count; $counter++)
	{
		$total_marks_obtain = 0;
		
		$total_b = 0;
		$total_i = 0;
		$total_h = 0;
		
		$correct_b = 0;
		$correct_i = 0;
		$correct_h = 0;
		
		$wrong_b = 0;
		$wrong_i = 0;
		$wrong_h = 0;
		
		$skipped_b = 0;
		$skipped_i = 0;
		$skipped_h = 0;
			
		$candidate_overall_result = 'Pass';
		if(!isset($report_data[$subject_result[$counter]->id]) && $report_data[$subject_result[$counter]->id]=='')
		{
			$report_data[$subject_result[$counter]->id]['first_name'] = $subject_result[$counter]->first_name;
			$report_data[$subject_result[$counter]->id]['last_name'] = $subject_result[$counter]->last_name;
			for($counter1=0; $counter1<count($subject_result); $counter1++)
			{
				//$subject_total_marks_obtain = 0;
				$result = 'Pass';
				if($subject_result[$counter]->id==$subject_result[$counter1]->id)
				{
					$report_data[$subject_result[$counter]->id][$subject_result[$counter1]->subject_id]['marks']=$subject_result[$counter1]->marks_obtained;
						
					$total_marks_obtain += $subject_result[$counter1]->marks_obtained;
						
					$total_b += $subject_result[$counter1]->total_b;
					$total_i += $subject_result[$counter1]->total_i;
					$total_h += $subject_result[$counter1]->total_h;
					
					$correct_b += $subject_result[$counter1]->correct_b;
					$correct_i += $subject_result[$counter1]->correct_i;
					$correct_h += $subject_result[$counter1]->correct_h;
					
					$wrong_b += $subject_result[$counter1]->wrong_b;
					$wrong_i += $subject_result[$counter1]->wrong_i;
					$wrong_h += $subject_result[$counter1]->wrong_h;
					
					$skipped_b += $subject_result[$counter1]->skipped_b;
					$skipped_i += $subject_result[$counter1]->skipped_i;
					$skipped_h += $subject_result[$counter1]->skipped_h;
					
						
					if($subject_result[$counter1]->result=='F')
					{
						$result = 'Fail';
						$candidate_overall_result = 'Fail';
					}
			
					$report_data[$subject_result[$counter]->id][$subject_result[$counter1]->subject_id]['subject_result']=$result;
				}
			}
						
			$report_data[$subject_result[$counter]->id]['candidate_result'] = $candidate_overall_result;
			$report_data[$subject_result[$counter]->id]['candidate_total_marks_obtain'] = $total_marks_obtain;
				
			$report_data[$subject_result[$counter]->id]['total_b'] = $total_b;
			$report_data[$subject_result[$counter]->id]['total_i'] = $total_i;
			$report_data[$subject_result[$counter]->id]['total_h'] = $total_h;
			
			$report_data[$subject_result[$counter]->id]['correct_b'] = $correct_b;
			$report_data[$subject_result[$counter]->id]['correct_i'] = $correct_i;
			$report_data[$subject_result[$counter]->id]['correct_h'] = $correct_h;
			
			$report_data[$subject_result[$counter]->id]['wrong_b'] = $wrong_b;
			$report_data[$subject_result[$counter]->id]['wrong_i'] = $wrong_i;
			$report_data[$subject_result[$counter]->id]['wrong_h'] = $wrong_h;
			
			$report_data[$subject_result[$counter]->id]['skipped_b'] = $skipped_b;
			$report_data[$subject_result[$counter]->id]['skipped_i'] = $skipped_i;
			$report_data[$subject_result[$counter]->id]['skipped_h'] = $skipped_h;
		}
	}

	//echo '<pre>';print_r($report_data);exit;
	
	$row=7;
	$srNo=0;
	if($report_data)
	{
		foreach ($report_data as $candidate_id=>$data)
		{ 
			$srNo++;
			$worksheet->write($row, 0, $srNo, $data_format);
			$worksheet->write($row, 1, ucfirst(strtolower(stripslashes($data['first_name']))), $data_format);
			$worksheet->write($row, 2, ucfirst(strtolower(stripslashes($data['last_name']))), $data_format);
			
			$current_column = 2;
			for($counter = 0; $counter<count($exam_subject); $counter++) 
			{
				if($data[$exam_subject[$counter]->subject_id]['marks']!='')
				{
					$marks_obtained = $data[$exam_subject[$counter]->subject_id]['marks'];
				}
				else
				{
					$marks_obtained = 'NA';
					$data[$exam_subject[$counter]->subject_id]['subject_result'] = 'NA';
					$data['candidate_result']='NA';
				}
				
				$worksheet->write($row, $current_column+1, $marks_obtained, $data_format);
				$current_column+=1;
				$worksheet->write($row, $current_column+1, $data[$exam_subject[$counter]->subject_id]['subject_result'], $data_format);
				$current_column+=1;
				 
			}
				
			$worksheet->write($row, $current_column+1, $data['correct_b'], $data_format);
			$worksheet->write($row, $current_column+2, $data['wrong_b'], $data_format);
			$worksheet->write($row, $current_column+3, $data['skipped_b'], $data_format);

			$worksheet->write($row, $current_column+4, $data['correct_i'], $data_format);
			$worksheet->write($row, $current_column+5, $data['wrong_i'], $data_format);
			$worksheet->write($row, $current_column+6, $data['skipped_i'], $data_format);
			
			$worksheet->write($row, $current_column+7, $data['correct_h'], $data_format);
			$worksheet->write($row, $current_column+8, $data['wrong_h'], $data_format);
			$worksheet->write($row, $current_column+9, $data['skipped_h'], $data_format);
				
			$worksheet->write($row, $current_column+10, $data['candidate_total_marks_obtain'], $data_format);
			$worksheet->write($row, $current_column+11, number_format((($data['candidate_total_marks_obtain']/$total_marks)*100), 2, '.', ''), $data_format);
			$worksheet->write($row, $current_column+12, $data['candidate_result'], $data_format);
			

			$row++;
		}
	}
}

if($test_id=='')	
{
	$workbook->send('Result_'.implode('_',explode(' ',ucfirst(stripslashes($exam_detail->exam_name)))).'_'.date('d-M-Y').'.xls');
}
else
{
	$workbook->send('Result_'.implode('_',explode(' ',ucfirst(stripslashes($test_info[0]->test_name)))).'_'.date('d-M-Y').'.xls');
}
$workbook->close();

?>