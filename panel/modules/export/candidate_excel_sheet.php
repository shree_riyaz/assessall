<?php

chdir(ROOT . '/lib/phpxls');
require_once('Writer.php');
chdir('');

$Heading =  array(array('S/No.', 'Name', 'Email', 'Birth Date', 'Marital Status', 'Children', 'Remarks'));
$title = "Students List";

if (isset($_GET['exportfor']))
{
	$Heading[0][] = 'Marks';
	$title = "Total ". ucfirst($_GET['exportfor']) . "ed";
	
	if($_GET['exportfor'] == 'total')
	{
		$Heading[0][] = 'Result';
		$title = "Total Appeared";
	}
	
	$title .= " Students in '" . $_SESSION['lastTestName'] . "' Test of '" .$_SESSION['examName'] . "' Course (Maximum Marks : " . $_SESSION['maximumMarks'] . ")";
}
    
$workbook = new Spreadsheet_Excel_Writer();

$main_heading_format =& $workbook->addFormat();
$main_heading_format->setBold();
$main_heading_format->setColor('black');
$main_heading_format->setFontFamily('verdana,sans-serif');
$main_heading_format->setAlign('Center');
$main_heading_format->setVAlign('top');
$main_heading_format->setUnderline(1);

$heading_format =& $workbook->addFormat();
$heading_format->setBold();
$heading_format->setColor('black');
$heading_format->setFontFamily('verdana,sans-serif');
$heading_format->setSize(10);
$heading_format->setAlign('Center');
$heading_format->setVAlign('top');
$heading_format->setTextWrap(1); 
$heading_format->setBorder(1);   
$heading_format->setBottom(2);//thick

$data_format =& $workbook->addFormat();
$data_format->setColor('black');
$data_format->setFontFamily('verdana,sans-serif');
$data_format->setSize(9);
$data_format->setTextWrap(1);
$data_format->setAlign('left');
$data_format->setVAlign('top');
$data_format->setBorder(1); 

$arr = array('Students'=>$Heading);

foreach($arr as $wbname=>$rows)
{
    $rowcount = count($rows);
    $colcount = count($rows[0]);

    $worksheet =& $workbook->addWorksheet($wbname);
    
	// For main heading
	$worksheet->setMerge(0, 0, 0, 6);
	
	if (isset($_GET['exportfor']))
	{
		$worksheet->setMerge(0, 0, 0, 7);
		if($_GET['exportfor'] == 'total')
		{
			$worksheet->setMerge(0, 0, 0, 8);
		}
	}
	
    $worksheet->setColumn(0,0,7);
    $worksheet->setColumn(1,$colcount, 11);
    
    $worksheet->write(0, 0, $title, $main_heading_format);
    $row=2;
    for($counter=0; $counter<$rowcount; $counter++ )
    {
    	for($cols=0; $cols<$colcount;$cols++)
        {
                $data=$rows[$counter][$cols];
                $worksheet->write($row, $cols, $data, $heading_format);
        }
        $row++;
    }
}

$count=count($candidate);
if($candidate)
{
	$row=3;
	$srNo=0;
	for($counter=0;$counter<$count;$counter++) 
	{ 
		$srNo=$srNo+1;
		
			$worksheet->write($row, 0, $srNo, $data_format);
			$worksheet->write($row, 1, ucwords(strtolower(stripslashes($candidate[$counter]->first_name.' '.$candidate[$counter]->last_name))), $data_format);
			$worksheet->write($row, 2, stripslashes($candidate[$counter]->email), $data_format);
			
		//-------------------------------------------------------------------------------	
			if($candidate[$counter]->birth_date && ($candidate[$counter]->birth_date!='0000-00-00 00:00:00'))
			{
				$worksheet->write($row, 3, date('d-M-Y', strtotime($candidate[$counter]->birth_date)), $data_format);
			}
			else 
			{
				$worksheet->write($row, 3, 'NA', $data_format);
			}
		//-------------------------------------------------------------------------------		
			if($candidate[$counter]->martial_status=='M')
			{
				$worksheet->write($row, 4, 'Married', $data_format);
			}
			else 
			{
				$worksheet->write($row, 4, 'Single', $data_format);
			}

		//-------------------------------------------------------------------------------		
			$worksheet->write($row, 5, $candidate[$counter]->children, $data_format);
			
		//-------------------------------------------------------------------------------		
			if($candidate[$counter]->remark!='')
			{
				$worksheet->write($row, 6, ucfirst(strtolower(stripslashes($candidate[$counter]->remark))), $data_format);
			}
			else
			{
				$worksheet->write($row, 6, 'NA', $data_format);
			}
		//--------------------------------------------------------------------------------
			if (isset($_GET['exportfor']))
			{
				if($candidate[$counter]->marks!='')
				{
					$worksheet->write($row, 7, stripslashes($candidate[$counter]->marks), $data_format);
				}
				else
				{
					$worksheet->write($row, 7, 'NA', $data_format);
				}
				
				if($_GET['exportfor'] == 'total')
				{
					if($candidate[$counter]->result!='')
					{
						$worksheet->write($row, 8, $candidate[$counter]->result, $data_format);
					}
					else
					{
						$worksheet->write($row, 8, 'NA', $data_format);
					}
				}
			}
		
		$row++;
		//$srNo++;
	}
}

$workbook->send('student_details_'.date('d-M-Y').'.xls');

$workbook->close();
?>