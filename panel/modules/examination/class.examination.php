<?php
class Examination
{
	// add a Exam
	function addExam()
	{
		global $DB,$frmdata;
//		print_r($frmdata);exit;
		$err='';
		$tabSelected = false;
		
		if ($frmdata['exam_name']=='')
		{
			 $err.="Please enter course name.<br>";
		}
		else
		{
			 $exist=$DB->SelectRecord('examination',"exam_name='".addslashes($frmdata['exam_name'])."'");
			 //print_r($exist);exit;
			 if($exist->exam_name!='')
			 {
				$err.="Course with this name is already exist.<br>";
			 }
		}
		
		if(!$tabSelected && ($err != ''))
		{
			$_SESSION['select_tab'] = 0;
			$tabSelected = true;
		}

		if($frmdata['count_subject']<=0)
		{
			$err.="Please add atleast one subject.<br>";
		}
		
		if(!$tabSelected && ($err != ''))
		{
			$_SESSION['select_tab'] = 1;
			$tabSelected = true;
		}
				
		if($err=='')
		{
			$exam_id = $DB->InsertRecord('examination',$frmdata);
			// add exam subjects
			for($counter=0;$counter<$frmdata['total_subject'];$counter++)
			{
				if($frmdata['subject_id_'.($counter+1)]=='')
				{
					continue;
				}
				else 
				{
					$frmdata['exam_id'] = $exam_id;
					$frmdata['subject_id'] = $frmdata['subject_id_'.($counter+1)];
					$frmdata['subject_min_mark'] = $frmdata['subject_min_value_'.($counter+1)];
					$frmdata['subject_max_mark'] = $frmdata['subject_max_value_'.($counter+1)];
					
					$DB->InsertRecord('exam_subjects',$frmdata);
				}
			}
			
			$_SESSION['success']="Course has been added successfully.";
			$frmdata='';
			Redirect(CreateURL('index.php','mod=examination&do=manage'));
			exit;
		}
		elseif($err!='')
		{
			$_SESSION['error']=$err;
		}
		
		
	}	
	
	// to edit Examination information
	function editExam($nameID)
	{
		global $DB,$frmdata;
		$err='';
		
		$exam_history = $DB->SelectRecord('candidate_test_history', "exam_id=$nameID");		
		if($exam_history)
		{
			$err = 'Permission denied. This course is in use.';
		}
		else
		{	
			if ($frmdata['exam_name']=='')
			{
				 $err.="Please enter course name.<br>";
			}
			else
			{
				 $exist=$DB->SelectRecord('examination',"id!=$nameID and exam_name='".addslashes($frmdata['exam_name'])."'");
				 //print_r($exist);exit;
				 if($exist->exam_name!='')
				 {
					$err.="This course is already exist.<br>";
				 }
			}
				
			if(!$tabSelected && ($err != ''))
			{
				$_SESSION['select_tab'] = 0;
				$tabSelected = true;
			}

			if($frmdata['count_subject']<=0)
			{
				$err.="Please add atleast one subject.<br>";
			}
			
			if(!$tabSelected && ($err != ''))
			{
				$_SESSION['select_tab'] = 1;
				$tabSelected = true;
			}
		}
		
		if($err=='')
		{		
			$DB->UpdateRecord('examination',$frmdata, "id=$nameID");
			$DB->DeleteRecord('exam_subjects', "exam_id=$nameID");
			
			// add exam subjects
			for($counter=0;$counter<$frmdata['total_subject'];$counter++)
			{
				if($frmdata['subject_id_'.($counter+1)]=='')
				{
					continue;
				}
				else 
				{
					$frmdata['exam_id'] = $nameID;
					$frmdata['subject_id'] = $frmdata['subject_id_'.($counter+1)];
					$frmdata['subject_min_mark'] = $frmdata['subject_min_value_'.($counter+1)];
					$frmdata['subject_max_mark'] = $frmdata['subject_max_value_'.($counter+1)];
					
					$DB->InsertRecord('exam_subjects',$frmdata);
				}
			}
			
			$_SESSION['success']="Course has been updated successfully.";
			$frmdata='';
			Redirect(CreateURL('index.php','mod=examination&do=manage'));
			exit;
		}
		elseif($err!='')
		{
			$_SESSION['error']=$err;
		}
	}
	
	// get examination information
	function getRecordExam(&$totalCount)
	{
		global $DB,$frmdata;
		//print_r($frmdata);
		$cond = 0;
		$query='';
		$query="select exam.* from examination as exam ";
	
		if ($_SESSION['admin_user_type'] == 'T')
		{
			$query .= " JOIN exam_subject_teacher est on (est.exam_id = exam.id) ";
			$query .= " WHERE est.teacher_id = '".$_SESSION['admin_user_id']."' ";
			$cond++;
		}

		if(trim($frmdata['exam_name_manage'])!='')
		{
			if($cond==0)
				$query.=" where ";
			else
				$query.="and ";
			$query.="exam.exam_name like '%".addslashes($frmdata['exam_name_manage'])."%'";
			$cond++;
		}
		
		$query.=" GROUP BY exam.id ";
		
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{
	  		$query.=" order by ".$frmdata['orderby'];
		}	
		else
		{
			$query.=' order by id desc';
		}

		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		return $result;  
	}	
}
?>