<?php 

defined("ACCESS") or die("Access Restricted");
include_once('class.examination.php');		
$DB = new DBFilter();
$ExaminationOBJ= new Examination();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($frmdata['addexam']))
{
	$ExaminationOBJ->addExam();	
}

if(isset($frmdata['editexam']))
{
	$ExaminationOBJ->editExam($nameID);	
}

if(isset($frmdata['copyexam']))
{
	$isCopy = true;
}

$do = '';
if(isset($getVars['do']))
{
	$do=$getVars['do'];
}

switch($do)
{
	case 'add':
		$examlist= $DB->SelectRecords('examination','','*','order by exam_name');
		$subject = $DB->SelectRecords('subject', '', '*', 'order by subject_name');
		
		if(isset($frmdata['exam_list']) && $frmdata['exam_list'] != '')
		{
			$nameID = $frmdata['exam_list'];
		}
					
		if($nameID)
		{
			$exam_info = $DB->SelectRecord("examination", "id=$nameID");
			$subject_info = getSubjectInfoByExamId($nameID);
		}

		if(!isset($frmdata['addexam']))
		{
			$_SESSION['select_tab'] = 0;
		}
			
	   $CFG->template="examination/save.php";
	break;
		 
	default:  	
	case 'manage':
		PaginationWork();
		$examlist = $ExaminationOBJ->getRecordExam($totalCount);
	    $CFG->template="examination/manage.php";
	break; 	
			
	case 'edit':
		$examlist= $DB->SelectRecords('examination','','*','order by exam_name');
		$subject = $DB->SelectRecords('subject', '', '*', 'order by subject_name');
		
		if(isset($frmdata['exam_list']) && $frmdata['exam_list'] != '')
		{
			$nameID = $frmdata['exam_list'];
		}
		$exam_info = $DB->SelectRecord("examination", "id=$nameID");
		$subject_info = getSubjectInfoByExamId($nameID, $exam_info->exam_type_id);
		
		if(!isset($frmdata['editexam']))
		{
			$_SESSION['select_tab'] = 0;
		}
		
		$isEdit = true;
		$CFG->template="examination/save.php";
	break;
	    
	case 'delete':
		$DB->DeleteRecord('examination','id="'.$nameID.'"');
		$DB->DeleteRecord('exam_subjects','exam_id="'.$nameID.'"');
		$_SESSION['success']="Record has been deleted successfully.";
		Redirect(CreateURL('index.php','mod=examination&do=manage'));
		exit();
} 

include(CURRENTTEMP."/index.php");
exit;
?>