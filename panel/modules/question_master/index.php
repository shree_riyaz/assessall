<?php 
 /*****************Author			:-Rahul Gahlot
				   Date         :- 2-july-2014
                   Module       :- Question master
                   Purpose      :- Entry file for any action, call in question master module 
***********************************************************************************/
defined("ACCESS") or die("Access Restricted");
@session_start();

if(isset($frmdata['image_type']) && ($frmdata['image_type'] != ''))
{
	$val = $frmdata['image_type'];
	
	if(isset($_SESSION['fileObject_'.$val]) && $_SESSION['fileObject_'.$val]['final_path'] != "")
	{
		$image_path = ROOT . '/uploadfiles/' . $_SESSION['fileObject_'.$val]['final_path'];
		@unlink($image_path);
		unset($_SESSION[$val]);
		unset($_SESSION['fileObject_'.$val]);
	}
	if($_FILES[$val]['name'] != "")
	{
		if($_FILES[$val]['type']!='image/jpeg' && $_FILES[$val]['type']!='image/jpg' && $_FILES[$val]['type']!='image/gif' && $_FILES[$val]['type']!='image/png' && $_FILES[$val]['type']!='image/pjpeg' && $_FILES[$val]['type']!='image/x-png')
		{
			echo "<font color='red'>Please upload image in correct format.</font>";
		}
		elseif($_FILES[$val]['size']>2000000)
		{
			echo "<font color='red'>Please upload image not more than 2MB.</font>";
		}
		else
		{
			$image_path = ROOT . '/uploadfiles/' . $_FILES[$val]['name'];
			$FileObject = (object)$_FILES[$val];
			UploadImageFile($image_path,$FileObject,'image');
			
			$_SESSION[$val] = $image_path;
			$_SESSION['fileObject_'.$val] = $_FILES[$val];
			$_SESSION['fileObject_'.$val]['final_path'] = $image_path;
			
			if(file_exists(ROOT . "/uploadfiles/" . $image_path))
			{
				echo "<img src='". ROOTURL ."/uploadfiles/". $image_path ."' style='width:20px;heigth:20px;'/>";
			}
			else
			{
				echo "Preview not available.";
			}
		}
	}
	else
	{
		echo "File not uploaded. Please try again.";
	}
	die();
}

include_once('class.question_master.php');		
$DB = new DBFilter();
$QuestionMasterOBJ= new QuestionMaster();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}
if(isset($getVars['groupqID']))
{
	$nameID=$getVars['groupqID'];
}
if(isset($frmdata['addQuestion']))
{
	$QuestionMasterOBJ->addQuestion();	
}
if(isset($frmdata['addGroupQuestion']))
{
	$QuestionMasterOBJ->addGroupQuestion();	
}
if(isset($frmdata['editQuestion']))
{
	$QuestionMasterOBJ->editQuestion($nameID);	
}
if(isset($frmdata['edit_group_Question']))
{	$group_question_id = $getVars['id'];
	$QuestionMasterOBJ->editGroupQuestion($group_question_id);	
}
if(isset($frmdata['listQuestion']))
{
	$QuestionMasterOBJ->listQuestion();	
}
if(isset($frmdata['listQuestion_doc']))
{
	$QuestionMasterOBJ->listQuestion_doc();
}
if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}
$question_level=array("B"=>"Beginner", "I"=>"Intermediate", "H"=>"Higher");
$question_type=array("M"=>"Multiple Choice", "T"=>"True/False", "I"=>"Reasoning", "MT"=>"Match Type","S"=>"subjective");
//$question_type=array("M"=>"Multiple Choice", "T"=>"True/False", "I"=>"Reasoning", "MT"=>"Match Type");

$subjectQuery = " SELECT subject.* FROM subject ";
if ($_SESSION['admin_user_type'] == 'T')
{
	$subjectQuery .= " JOIN exam_subject_teacher est on (est.subject_id = subject.id) ";
	$subjectQuery .= " WHERE est.teacher_id = '".$_SESSION['admin_user_id']."' ";
}
$subjectQuery .= " GROUP BY subject.id ORDER BY subject_name ";
$subject = $DB->RunSelectQuery($subjectQuery);

switch($do)
{
		case 'add':
			if(!isset($frmdata['addQuestion']))
			{		// unset all the session variables used in image type question upload.
				$QuestionMasterOBJ->clearSession();
			}
			if($_GET['id'])
			{   
				$result_data = $DB->SelectRecord('group_questions','id='.$_GET['id']); 
				$result_data_name = $DB->SelectRecord('subject','id='.$result_data->subject_id); 				
			}
		   $CFG->template="question_master/save.php";
		   break;
		   
		case 'export_question' :
			$QuestionMasterOBJ->exportQuestionToCsv();
			break;
				   	
		case 'edit':
		
			if(!isset($frmdata['editQuestion']))
			{		// unset all the session variables used in image type question upload.
				$QuestionMasterOBJ->clearSession();
			}
                         
                     if($_GET['id'])
			{   
				$result_data = $DB->SelectRecord('group_questions','id='.$_GET['id']); 
				$result_data_name = $DB->SelectRecord('subject','id='.$result_data->subject_id); 
			}
			$question_detail=$DB->SelectRecord('question','id='.$nameID);
			$question_answer=$DB->SelectRecords('question_answer','question_id='.$nameID);
			$option_answer=$DB->SelectRecords('answer','question_id='.$nameID,'*');

			if($question_detail->question_type == 'I')
			{
				$question_image = $DB->SelectRecords('question_image', 'question_id='.$nameID);
				if(is_array($question_image))
				{
					foreach($question_image as $qi)
					{
						if(($qi->image_path == '') || ($qi->image_path == 0))
							continue;
						
						if(!file_exists(ROOT.'/uploadfiles/'.$qi->image_path))
						{
							$DB->DeleteRecord('question_image',"id = '$qi->id'");
						}
					}
				}
				
				$question_image = $DB->SelectRecords('question_image', 'question_id='.$nameID);
				if($question_image != '')
				{
					$_SESSION['no_of_pre_images'] = count($question_image);
				}
				
				$option_answer=$DB->SelectRecords('answer','question_id='.$nameID,'*');
				if(is_array($option_answer))
				{
					foreach($option_answer as $oa)
					{
						//echo "<pre>";print_r($qi);
						if(!file_exists(ROOT.'/uploadfiles/'.$oa->answer_title))
						{
							$DB->DeleteRecord('answer',"id = '$oa->id'");
						}
					}
				}
				
				$option_answer=$DB->SelectRecords('answer','question_id='.$nameID,'*');
				if($option_answer != '')
				{
					$_SESSION['no_of_pre_options'] = count($option_answer);
				}
			}
				elseif ($question_detail->question_type == 'S')
			{
				
				$option_answer=$DB->SelectRecords('answer','question_id='.$nameID,'*');
			
				 $_SESSION['answer_title']= $option_answer[0]->answer_title;
			}
			elseif ($question_detail->question_type == 'MT')
			{
				$left_cols = $DB->SelectRecords('question_match_left', 'question_id='.$nameID);
				$right_cols = $DB->SelectRecords('question_match_right', 'question_id='.$nameID);
				
				if(!isset($frmdata['no_of_cols'])) $frmdata['no_of_cols'] = count($right_cols);
			}
			
			if(!$_POST)
		   	{
			   	foreach($question_detail as $key => $cd)
			   		$frmdata[$key] = $cd;
			   		
				if($frmdata['min_time_to_solve'] != '')
				{
					$frmdata['minimum_min'] = floor($question_detail->min_time_to_solve / 60);
					$frmdata['minimum_sec'] = floor($question_detail->min_time_to_solve % 60); 
				}
				
			   	if($frmdata['max_time_to_solve'] != '')
				{
					$frmdata['maximum_min'] = floor($question_detail->max_time_to_solve / 60);
					$frmdata['maximum_sec'] = floor($question_detail->max_time_to_solve % 60); 
				}
				
				if(($question_detail->question_type == 'MT') && !isset($frmdata['editquestion']))
				{
					for($counter = 1; $counter <= $frmdata['no_of_cols']; $counter++)
					{
						$count = $counter - 1;
						$_SESSION['right_col_' . $counter] = ($right_cols[$count]->value) ? $right_cols[$count]->value : '';
						
						$right_id = $right_cols[$count]->id;
						$left_col = $DB->SelectRecord('question_match_left', 'answer_id='.$right_id);
						
						$_SESSION['left_col_' . $counter] = ($left_col->value) ? $left_col->value : '';
					}
				}
				else
				{
					for($count = 1;$count<=count($question_image); $count++)
					{
						$c = $count - 1;
						if($question_image[$c] != '')
						{
							$_SESSION['pre_image_'.$count] = $question_image[$c]->image_path;
							
							if($question_image[$c]->image_path == '0' || $question_image[$c]->image_path == null)
							{
								$_SESSION['question_mark'] = $count;
								unset($_SESSION['pre_image_'.$count]);
							}
						}
					}
				
					
					foreach($question_answer as $qans)
					$correct_ans[] = $qans->answer_id;
					//echo 'test'.count($option_answer);
					$total_ans_options = count($option_answer);
					for($counter=0;$counter<count($option_answer);$counter++)
					{
						$count=$counter+1;
						 $_SESSION['pre_option_'.$count]=$option_answer[$counter]->answer_title;
						$_SESSION['initial_answer_'.$count] = $option_answer[$counter]->answer_title;
						
						if(in_array($option_answer[$counter]->id, $correct_ans))
						{
							if($question_detail->question_type == 'T')
							{
								$_SESSION['correct_ans']=$count;
							}
							else
							{
								$_SESSION['correct_ans'][]=$count;
							}
						}
					}
					
					
				}
				
				if(!isset($_SESSION['no_of_images'])) $_SESSION['no_of_images'] = $_SESSION['no_of_pre_images'];
				if(!isset($_SESSION['no_of_options'])) $_SESSION['no_of_options'] = $_SESSION['no_of_pre_options'];
		   	}
			$isEdit = true;
			$CFG->template="question_master/save.php";
			break; 
         //----------------------

        case 'groupedit':
		
			if(!isset($frmdata['edit_group_Question']))
			{		// unset all the session variables used in image type question upload.
				$QuestionMasterOBJ->clearSession();
			}
                         
                     if($_GET['id'])
			{   
				$result_data = $DB->SelectRecord('group_questions','id='.$_GET['id']); 
				$result_data_name = $DB->SelectRecord('subject','id='.$result_data->subject_id); 
			}
			
			$isEdit = true;
			$CFG->template="question_master/edit_group_questions.php";
			break; 
                   
		//-----------------------
		case 'preview':
			$question_detail=$DB->SelectRecord('question','id='.$_GET['id']);
			$CFG->template="question_master/preview.php";
			break;

		case 'upload_question':
			$CFG->template="question_master/upload_question.php";
			break;

		case 'upload_question_doc':
			$CFG->template="question_master/upload_question_doc.php";
			break;

		case 'markdown_helper':
			$CFG->template="question_master/markdown_helper.php";
			break;
		
		case 'group_questions':
			if(!isset($frmdata['addGroupQuestion']))
			{
				$QuestionMasterOBJ->clearSession();
			}
			
			$CFG->template="question_master/group_questions.php";
			break;
			
		case 'group_questions_listing':
			if(!isset($frmdata['addGroupQuestion']))
			{
				$QuestionMasterOBJ->clearSession();
			}
                        PaginationWork();
			$totalCount=0;			
			$questionlist= $QuestionMasterOBJ->getRecordGroupQuestion($totalCount);
			$CFG->template="question_master/group_questions_listing.php";
			break;
			
		case 'delete':
                    if($_GET['id'])
                    {
                        $DB->DeleteRecord('group_questions','id="'.$_GET['id'].'"');   
                         $DB->DeleteRecord('question','group_question_id="'.$_GET['id'].'"');
                    }
			$question_detail=$DB->SelectRecord('question','id='.$nameID);
			if($question_detail->question_type == 'I')
			{
				$question_image=$DB->SelectRecords('question_image','id='.$nameID);
				for($count = 0; $count < count($question_image); $count++)
				{
					$image_name = $question_image[$count]->image_path;
					$image_name = ROOT . "/uploadfiles/" . $image_name;
					@unlink($image_name);
				}	
				$DB->DeleteRecord('question_image','question_id="'.$nameID.'"');
				
				$answer = $DB->SelectRecords('answer','question_id="'.$nameID.'"');
				for($count = 0; $count < count($answer); $count++)
				{
					$image_name = $answer[$count]->answer_title;
					$image_name = ROOT . "/uploadfiles/" . $image_name;
					@unlink($image_name);
				}	
			}
			
			if($question_detail->video != '')
			{
				$video = ROOT . "/uploadfiles/" . $question_detail->video;
				@unlink($video);
			}
			
			$DB->DeleteRecord('question','id="'.$nameID.'"');
			$DB->DeleteRecord('question_answer','question_id="'.$nameID.'"');
			$DB->DeleteRecord('answer','question_id="'.$nameID.'"');
			
                        if($_GET['id'])
                        {
                            Redirect(CreateURL('index.php','mod=question_master&do=group_questions_listing'));
                            $_SESSION['success']="Question has been deleted successfully.";
                        }else{
                            Redirect(CreateURL('index.php','mod=question_master&do=manage'));
                            $_SESSION['success']="Question has been deleted successfully.";
                        }
                        die();
                        break;

		case 'manage':
		default:
			PaginationWork();
			$totalCount=0;
                        if(isset($_GET['id']))
                        {
                           $questionlist= $QuestionMasterOBJ->getRecordGroupQuestionlist($totalCount);
                         }else{
                            $questionlist= $QuestionMasterOBJ->getRecordQuestion($totalCount);
                        }
			$CFG->template="question_master/manage.php";
			break; 	
} 	
include(CURRENTTEMP."/index.php");
?>