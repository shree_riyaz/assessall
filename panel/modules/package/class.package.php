<?php

/*****************Developed by :- Chirayu Bansal
Date         :- 18-aug-2011
Module       :- Subject master
Purpose      :- Class for function to add and edit Subject details
 ***********************************************************************************/
// ---------------------------------------Riyaz code start-------------------------------------------
class Package
{
    //==========================================================================
    //to add a Subject
    //==========================================================================
    function addPackage()
    {
        global $DB,$frmdata;

//        var_dump($frmdata);
//        exit();
        $get_course_name_by_its_id = array_keys($frmdata['number_of_paper']);

        $err='';

        if ($frmdata['package_name']== '') {
            $err.="Please enter package name.<br>";
        }

        if($frmdata['package_name']!='')
        {
            $question_info = $DB->SelectRecord('package_details', "package_name='".htmlentities(addslashes($frmdata['package_name']))."'");
            if($question_info) {
                $err.="This package is already exist.<br>";
            }
        }
        if($frmdata['special_price'] != '')
        {
            $total_price = (int) $frmdata['total_price'];
            $special_price = (int) $frmdata['special_price'];
            if ($special_price == $total_price || $special_price >= $total_price ) {
                $err.="Special price should not be greater or equal to total price.<br>";
            }
        }

        if($frmdata['total_price']== '') {
            $err.="Please enter total price.<br>";
        }

//========================================Start=========================================================================

        $exam_id = $frmdata['exam_id'];
        $number_of_paper = array_values($frmdata['number_of_paper']);
//        var_dump($exam_id);

        $array_in_json = [];
        if(is_array($exam_id))
        {
            foreach ($exam_id as $key=>$value){
                $query =  "SELECT id,exam_id FROM `test` WHERE exam_id= " .$value. " ORDER BY RAND() LIMIT " .$number_of_paper[$key] ;

                $package_details_by_joining = $DB->RunSelectQuery($query);
                $array_in_json[] = json_decode(json_encode($package_details_by_joining), True);
            }
        }

        $new_arr = [];
        $number_of_paper = [];
        foreach($array_in_json as $key1 => $value1)
        {
            foreach ($value1 as $key=>$value){
                if(array_key_exists($value['exam_id'],$new_arr))
                {
                    $new_arr[$value['exam_id']] = $new_arr[$value['exam_id']]+1;
                }
                else
                {
                    $new_arr[$value['exam_id']] = 1;
                }
            }
        }

        $array = implode(",",$get_course_name_by_its_id);
        $query = "select ex.exam_name from examination ex WHERE id in (" . $array . ")";

        $get_course_name = $DB->RunSelectQuery($query);

        $count = 0;
        foreach($new_arr as $key=>$val)
        {

            if($val < $frmdata['number_of_paper'][$key])
            {

                $err .= "Sorry, there is only " . '<b style="color:green;">' . $val . '</b>' . "  test available for course " . '<b style="color:green;">' . $get_course_name[$count]->exam_name . ' .'.'</b>' . "<br>";
            }
            $count++;
        }

        if($err)
        {
            $_SESSION['error']=$err;
        }

//================================================End=================================================================
        $_SESSION['allformdata'] = $frmdata;
        $_SESSION['exam_id'] = $frmdata['exam_id'];
//        var_dump('dd');

        if($err=='')
        {
            $data_for_first_table = [
                'package_name' =>$frmdata['package_name']
            ];

            $get_last_id = $DB->InsertRecord('package_details',$data_for_first_table);

            $count = count($frmdata['exam_id']);

            for ($i=0; $i<$count;$i++)
            {
                $data_for_second_table = [
                    'exam_id' => $frmdata['exam_id'][$i],
                    'number_of_paper' => array_values($frmdata['number_of_paper'])[$i],
                    'price_per_paper' => array_values($frmdata['price_per_paper'])[$i],
                    'special_price' => $frmdata['special_price'],
                    'total_price' => $frmdata['total_price'],
                    'package_id' =>$get_last_id,
                    'total' =>array_values($frmdata['total'])[$i]
                ];
                $DB->InsertRecord('selected_exam_details',$data_for_second_table);
            }
            foreach($array_in_json as $nnn)
            {
                foreach($nnn as $eee)
                {
                     $data_for_third_table = [
                        'exam_id' => $eee['exam_id'],
                        'package_id' =>$get_last_id,
                        'allotted_tests' =>$eee['id']
                    ];
                    $DB->InsertRecord('alloted_tests_for_package',$data_for_third_table);
                }

            }

            unset($_SESSION['exam_id']);
            unset($_SESSION['allformdata']);


            $_SESSION['success']="Package has been added successfully.";
            Redirect(CreateURL('index.php','mod=package&do=manage'));
            exit;
        }
        elseif($err!='')
        {
            $_SESSION['error']=$err;
        }


    }

/*==========================================================================
 to edit Subject information
==========================================================================*/
    function editPackage($nameID)
    {

        global $DBsun123ARC,$frmdata,$DB;


//        unset($_SESSION['allformdata_edit']);
//        unset($_SESSION['exam_id_edit']);
        $get_course_name_by_its_id = array_keys($frmdata['number_of_paper']);

        $err='';


        if ($frmdata['package_name']=='')
        {
            $err.="Please enter package name.<br>";
        }

        if ($frmdata['total_price']== '')
        {
            $err.="Please enter total price.<br>";
        }
        if($frmdata['special_price'] != '')
        {
            $total_price = (int) $frmdata['total_price'];
            $special_price = (int) $frmdata['special_price'];
            if ($special_price == $total_price || $special_price >= $total_price ) {

                $err.="Special price should not be greater or equal to total price.<br>";
            }
        }



//========================================Start Third table work=========================================================================
        $exam_id = $frmdata['exam_id'];
        $number_of_paper = array_values($frmdata['number_of_paper']);
        $array_in_json = [];

        foreach ($exam_id as $key=>$value){
            $query =  "SELECT id,exam_id FROM `test` WHERE exam_id= " .$value. " ORDER BY RAND() LIMIT " .$number_of_paper[$key] ;

            $package_details_by_joining = $DB->RunSelectQuery($query);
            $array_in_json[] = json_decode(json_encode($package_details_by_joining), True);
        }

        if($err)
        {
            $_SESSION['error']=$err;
        }
//========================================End Third table work =================================================================

        if($err=='')
        {

            $data_for_first_table = [
                'package_name' =>$frmdata['package_name']
            ];

//            $get_last_id = $DB->InsertRecord('package_details',$data_for_first_table);
            $DB->UpdateRecord('package_details',$data_for_first_table,'id="'.$nameID.'"');
            $DB->DeleteRecord('selected_exam_details','package_id="'.$nameID.'"');
            $DB->DeleteRecord('alloted_tests_for_package','package_id="'.$nameID.'"');

            $count = count($frmdata['exam_id']);
            for ($i=0; $i<$count;$i++)
            {
                $data_for_second_table = [
                    'exam_id' => $frmdata['exam_id'][$i],
                    'number_of_paper' => array_values($frmdata['number_of_paper'])[$i],
                    'price_per_paper' => array_values($frmdata['price_per_paper'])[$i],
                    'special_price' => $frmdata['special_price'],
                    'total_price' => $frmdata['total_price'],
                    'package_id' =>$nameID,
                    'total' =>array_values($frmdata['total'])[$i]
                ];
                $DB->InsertRecord('selected_exam_details',$data_for_second_table);
            }

            foreach($array_in_json as $nnn)
            {
//                for($j=0;$j<=count($frmdata['exam_id']);$j++)
//                {
                foreach($nnn as $eee)
                {

                    $data_for_third_table = [
//                        'exam_id' => implode(',',$frmdata['exam_id']),
                        'exam_id' => $eee['exam_id'],
                        'package_id' =>$nameID,
                        'allotted_tests' =>$eee['id']
                    ];
                    $DB->InsertRecord('alloted_tests_for_package',$data_for_third_table);
                }
            }
            $_SESSION['success']="Package has been updated successfully.";
            $frmdata='';
            Redirect(CreateURL('index.php','mod=package&do=manage'));
            exit;
        }
        elseif($err!='')
        {
            $_SESSION['error']=$err;
        }
    }

// ---------------------------------------Riyaz code end-------------------------------------------

    //==========================================================================
    /******************************************************************
    Des: A function to get subject information
     ******************************************************************/
    function getRecordPackage(&$totalCount)
    {
        global $DB,$frmdata;

        $cond = 0;
        $query='';
        $query="select pd.*, sed.special_price, sed.total_price from package_details as pd INNER JOIN selected_exam_details sed on sed.package_id = pd.id GROUP BY pd.id ";

        if(trim($frmdata['package_name'])!='')
        {
            if($cond==0)
                $query.=" where ";
            else
                $query.="and ";
            $query.="pd.package_name like '%".addslashes($frmdata['package_name'])."%'";
            $cond++;
        }

        if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
        {
            $query.=" order by ".$frmdata['orderby'];

        }
        else
        {
            $query.=' order by id desc';
        }

        $result = $DB->RunSelectQueryWithPagination($query,$totalCount);
//        var_dump($result);
        return $result;
    }

    //==========================================================================


}//end of class
?>