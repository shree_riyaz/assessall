<?php
/*****************Developed by :- Chirayu Bansal
Date         :- 18-aug-2011
Module       :- Stream master
Purpose      :- Entry file for any action, call in Subject master module
 ***********************************************************************************/

defined("ACCESS") or die("Access Restricted");
include_once('class.package.php');
$DB = new DBFilter();

$packageObject= new Package();

if(isset($getVars['nameID']))
{
    $nameID=$getVars['nameID'];
}

if(isset($frmdata['addpackage']))
{

    $packageObject->addPackage();
}

if(isset($frmdata['editpackage']))
{

    $packageObject->editPackage($nameID);
}

if(isset($getVars['do']))
{
    $do=$getVars['do'];
}
else
{
    $do='';
}

// -----------------------------------------------Riyaz Code start----------------------------------------------------------
//$examList = $DB->SelectRecords('examination', '', '*', 'order by exam_name');
//Get only those exam id which have tests.
$query = "select ex.exam_name,t.exam_id,t.test_name,t.exam_id from test t
          JOIN examination ex
          ON ex.id = t.exam_id
          GROUP BY t.exam_id";

$examList = $DB->RunSelectQuery($query);

$examList_for_edit_package = $DB->SelectRecords('examination', '', 'exam_name,id', 'order by exam_name');
//var_dump($examList_for_edit_package);
//die();
switch($do)
{
    case 'add':
        $CFG->template="package/add.php";
        break;

    default:
    case 'manage':

        PaginationWork();
        $packageList= $packageObject->getRecordPackage($totalCount);

    $CFG->template="package/manage.php";
        break;

    case 'edit':

        $package_detail=$DB->SelectRecord('selected_exam_details','','*');

        $get_all_package_from_table ="select * from package_details pd INNER JOIN selected_exam_details sed ON pd.id = sed.package_id 
        WHERE pd.id='".$nameID."' group by exam_id";

        $package_details_by_joining = $DB->RunSelectQuery($get_all_package_from_table);

        $CFG->template="package/edit.php";
        break;

    case 'delete':

        $package_list_for_delete = $DB->SelectRecords('package_details', 'id='.$nameID);

            $DB->DeleteRecord('package_details','id="'.$nameID.'"');
            $DB->DeleteRecord('selected_exam_details','package_id="'.$nameID.'"');
            $DB->DeleteRecord('alloted_tests_for_package','package_id="'.$nameID.'"');
            $_SESSION['success']="Record has been deleted successfully.";

        Redirect(CreateURL('index.php','mod=package&do=manage'));die();

}
// -----------------------------------------------Riyaz Code end----------------------------------------------------------

include(CURRENTTEMP."/index.php");

exit;

?>