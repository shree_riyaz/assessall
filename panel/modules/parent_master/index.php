<?php 
/**
 *	@author : Ashwini Agarwal
 *	@desc	: Parent management
 */

defined("ACCESS") or die("Access Restricted");
include_once('class.parent_master.php');		
$DB = new DBFilter();
$ParentMasterOBJ= new ParentMaster();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($frmdata['addparent']))
{
	$ParentMasterOBJ->saveParent();	
}

if(isset($frmdata['editparent']))
{
	$ParentMasterOBJ->saveParent($nameID);	
}

$do = '';
if(isset($getVars['do']))
{
	$do=$getVars['do'];
}

$candidate = $DB->SelectRecords('candidate');
switch($do)
{
	default:  	
	case 'manage':
		PaginationWork();
		$parentlist= $ParentMasterOBJ->getRecordParent($totalCount);
	    $CFG->template="parent_master/manage.php";
	break; 	

	case 'add':
		if(!$frmdata['addparent'])
	   	{
	   		$_SESSION['parent_candidate'] = array();
	   	}
	   	$CFG->template="parent_master/save.php";
   	break;
	
	case 'edit':
	   	$parent_detail=$DB->SelectRecord('parent','id='.$nameID);
	  	
	   	if(!$frmdata['editparent'])
	   	{
		   	foreach($parent_detail as $key => $cd)
		   	{
		   		$frmdata[$key] = $cd;
		   	}
		   	
		   	$_SESSION['parent_candidate'] = getCandidateByParent($nameID);
	   	}
		   	
		$isEdit = true;
		$CFG->template="parent_master/save.php";
	break;

	case 'delete':
		$parent_detail=$DB->SelectRecord('parent','id='.$nameID);
		if($parent_detail->pro_image)
		{
			@unlink(ROOT . "/uploadfiles/" . $parent_detail->pro_image);
		}
				
		$DB->DeleteRecord('parent','id="'.$nameID.'"');
		$DB->DeleteRecord('parent_candidate','parent_id="'.$nameID.'"');
		$_SESSION['success']="Record has been deleted successfully.";
		Redirect(CreateURL('index.php','mod=parent_master&do=manage'));
	exit();
} 
	
include(CURRENTTEMP."/index.php");
exit;
?>