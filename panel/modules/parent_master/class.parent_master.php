<?php
/**
 *	@author : Ashwini Agarwal
 *	@desc	: Parent management
 */

class ParentMaster
{

	function saveParent($nameID = '')
	{
		global $DB,$frmdata;
		$err = '';
		
		if ($frmdata['first_name']=='')
		{
			 $err.="Please enter first name.<br>";
		}
		
		if ($frmdata['email']=='')
		{
			$err.="Please enter email address.<br>";
		}
		else
		{
			$email = $frmdata['email'];
			if($nameID)
			{
				$editCondition = " AND (id != '$nameID') ";
			}
			
		 	$exist = $DB->SelectRecord('parent',"email='$email' $editCondition");
			if($exist && ($exist->email!=''))
			{
				$err.="This email id is already in use.<br>";
			}
		}

		if ($frmdata['username']=='')
		{
			$err.="Please enter username.<br>";
		}
		else
		{
			$username = $frmdata['username'];
			if($nameID)
			{
				$editCondition = " AND (id != '$nameID') ";
			}
			
		 	$exist = $DB->SelectRecord('parent',"username='$username' $editCondition");
			if($exist && ($exist->username != ''))
			{
				$err.="This username is not available.<br>";
			}
			else
			{
				$exist = $DB->SelectRecord('teacher',"username='$username'");
				if($exist && ($exist->username != ''))
				{
					$err.="This username is not available.<br>";
				}
				else
				{
					$exist = $DB->SelectRecord('admin_users',"user_name='$username'");
					if($exist && ($exist->user_name != ''))
					{
						$err.="This username is not available.<br>";
					}
				}
			}
		}
		
		if ($frmdata['password']=='')
		{
			if($nameID)
			{
				unset($frmdata['password']);
			}
			else
			{
				$err .= "Please enter password.<br>";
			}
		}
		else
		{
			if($frmdata['confirm_password']=='')
			{
				$err.="Please enter confirm password.<br>";
			}
			elseif($frmdata['confirm_password']!=$frmdata['password'])
			{
				$err.="Confirm password doesn't match with password.<br>";
			}
		}
		
		if(!is_array($_SESSION['parent_candidate']) || (count($_SESSION['parent_candidate']) == 0))
		{
			$err.="Please add at-least one student.<br>";
		}
		
		if($err)
		{
			$_SESSION['error'] = $err;
			return false;
		}
	
		if($frmdata['password'])
		{
			$frmdata['password'] = Encrypt($frmdata['password']);
		}
		
		if($nameID)
		{
			$DB->UpdateRecord('parent', $frmdata, "id='$nameID'");
			$action = 'updated';
		}
		else
		{
			$nameID = $DB->InsertRecord('parent', $frmdata);
			$action = 'added';
		}
	
		/******** Add parent candidate *************************************/

		$DB->DeleteRecord('parent_candidate','(parent_id="'.$nameID.'") AND (is_approved=1)');
		foreach($_SESSION['parent_candidate'] as $parent_candidate)
		{
			$pc = array('parent_id' => $nameID);
			$pc['candidate_id'] = $parent_candidate['candidate_id'];
			
			$DB->InsertRecord('parent_candidate', $pc);
		}
		
		$_SESSION['success']="Parent details has been $action successfully.";
		Redirect(CreateURL('index.php','mod=parent_master&do=manage'));
		exit;	
	}
	
	function getRecordParent(&$totalCount)
	{
			
		global $DB,$frmdata;
		
		$query='';
		$query="select parent.* ";
		$query.=" from parent ";
		
		$where = array();
		
		if(isset($frmdata['name']) && $frmdata['name'] !='')
		{
			$name = addslashes($frmdata['name']);
			$where[] =" ( (first_name like '%$name%') OR (last_name like '%$name%') OR
					(CONCAT_WS(' ', first_name, last_name) like '%$name%') )";
		}

		if(count($where) > 0)
		{
			$where = implode(' ) AND ( ', $where);
			$query.=" WHERE ($where)";	
		}
		
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{	
	  		$query.=" order by ".$frmdata['orderby'];
		}	
		else
		{
			$query.=' order by id desc ';
		}

		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		return $result;   
	}

}
?>