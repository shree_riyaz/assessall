<?php 
/*****************Developed by :- Chirayu Bansal
	                Date         :- 22-july-2011
					Module       :- Dashboard
					Purpose      :- Entry file for any action, call in Dashboard module
***********************************************************************************/

defined("ACCESS") or die("Access Restricted");
$DB = new DBFilter();

include_once('class.dashboard.php');
$DashboardOBJ = new Dashboard();

$exportEnabled = true;
$auth = new Authorise();
if (($auth->isAuthorisedAction($_SESSION['admin_user_id'], 'dashboard', 'download') == false) && ($_SESSION['admin_user_name'] != 'admin'))
{
	$exportEnabled = false;
}

$do = '';
if(isset($getVars['do']))
$do=$getVars['do'];

switch($do)
{
	default:
	case 'showinfo':
		$totalCandidate = $DashboardOBJ->countCandidate();
		$totalQue = $DashboardOBJ->countQuestion();
		$streamQue = $DashboardOBJ->countSubjectQuestion();
		$finishedTest = $DashboardOBJ->countfinishedTest();
		$remainingTest = $DashboardOBJ->countremainingTest();
		$testdetail = $DashboardOBJ->lastTestdetail();
		$testresult = $DashboardOBJ->TestResult($testdetail[0]->id);
		
		$parent_candidate = $DashboardOBJ->getStudentLinkRequest();
		$homeworkhistory = $DashboardOBJ->recentlySubmittedHomework();
		
		$CFG->template="dashboard/showinfo.php";
	break;
	
	case 'total' :
	case 'pass' :
	case 'fail' :
	
		PaginationWork();
		$totalCount = 0;
		
		$functionName = 'lastTestResult'.ucfirst($do);
		$candidatelist = $DashboardOBJ->$functionName($totalCount);
		
		$page_name = ($do == 'total') ? 'appear' : $do;
		$page_name = ucfirst($page_name)."ed";
		$CFG->template="dashboard/last_exam_result.php";
	break;
	
	case 'question_xml' :
		$DashboardOBJ->getQuestionXml();
		exit;
	break;
	
	case 'test_xml' :
	
		$DashboardOBJ->getTestXml();
		exit;
	break;
	
	case 'candidate_xml' :
		$DashboardOBJ->getCandidateXml();
		exit;
	break;
} 

include(CURRENTTEMP."/index.php");
exit;
?>