<?php
/**
 *	@author : Ashwini Agarwal
 *	@desc	: Teacher management
 */

class TeacherMaster
{

	function saveTeacher($nameID = '')
	{
		global $DB,$frmdata;
		$tabSelected = false;
		$err = '';
		
		if ($frmdata['first_name']=='')
		{
			 $err.="Please enter first name.<br>";
		}
		
		if ($frmdata['email']=='')
		{
			$err.="Please enter email address.<br>";
		}
		else
		{
			$email = $frmdata['email'];
			if($nameID)
			{
				$editCondition = " AND (id != '$nameID') ";
			}
			
		 	$exist = $DB->SelectRecord('teacher',"email='$email' $editCondition");
			if($exist && ($exist->email!=''))
			{
				$err.="This email id is already in use.<br>";
			}
		}
/*
		if (($frmdata['exam_id']=='') || (count($frmdata['exam_id']) == 0))
		{
			 $err.="Please select at-least one course.<br>";
		}
*/		
		if ($frmdata['username']=='')
		{
			$err.="Please enter username.<br>";
		}
		else
		{
			$username = $frmdata['username'];
			if($nameID)
			{
				$editCondition = " AND (id != '$nameID') ";
			}
			
		 	$exist = $DB->SelectRecord('teacher',"username='$username' $editCondition");
			if($exist && ($exist->username != ''))
			{
				$err.="This username is not available.<br>";
			}
			else
			{
				$exist = $DB->SelectRecord('parent',"username='$username'");
				if($exist && ($exist->username != ''))
				{
					$err.="This username is not available.<br>";
				}
				else
				{
					$exist = $DB->SelectRecord('admin_users',"user_name='$username'");
					if($exist && ($exist->user_name != ''))
					{
						$err.="This username is not available.<br>";
					}
				}
			}
		}
		
		if ($frmdata['password']=='')
		{
			if($nameID)
			{
				unset($frmdata['password']);
			}
			else
			{
				$err .= "Please enter password.<br>";
			}
		}
		else
		{
			if($frmdata['confirm_password']=='')
			{
				$err.="Please enter confirm password.<br>";
			}
			elseif($frmdata['confirm_password']!=$frmdata['password'])
			{
				$err.="Confirm password doesn't match with password.<br>";
			}
		}
		
		if($_FILES['pro_image']['tmp_name']!='')
		{
			if(!in_array($_FILES['pro_image']['type'], array('image/jpeg', 'image/jpg', 'image/gif', 'image/png')))
			{
				$err.="Please upload image in right format.<br>";
			}
			
			if($_FILES['pro_image']['size']>2000000)
			{
				$err.="Please upload image not more than 2MB.<br>";
			}
		}
		
		if(!$tabSelected && ($err != ''))
		{
			$_SESSION['select_tab'] = 0;
			$tabSelected = true;
		}
		
		if(!is_array($_SESSION['teacher_subject']) || (count($_SESSION['teacher_subject']) == 0))
		{
			$err.="Please add at-least one subject.<br>";
		}
		
		if(!$tabSelected && ($err != ''))
		{
			$_SESSION['select_tab'] = 1;
			$tabSelected = true;
		}
		
		if($err)
		{
			$_SESSION['error'] = $err;
			return false;
		}
	
		if($frmdata['password'])
		{
			$frmdata['password'] = Encrypt($frmdata['password']);
		}
		
		if($_FILES['pro_image']['tmp_name']!='')
		{
			if($nameID)
			{
				$teacher_detail=$DB->SelectRecord('teacher',"id=".$nameID);
				$image_path = ROOT.'/uploadfiles';
				unlink($image_path. "/". $teacher_detail->pro_image);
			}
			
			$FileObject = (object)$_FILES['pro_image'];
			UploadImageFile($image,$FileObject,'image');
			$frmdata['pro_image']=$image;
		}
	
		if($nameID)
		{
			$DB->UpdateRecord('teacher', $frmdata, "id='$nameID'");
			$action = 'updated';
		}
		else
		{
			$nameID = $DB->InsertRecord('teacher', $frmdata);
			$action = 'added';
		}
		
		
		/******** Add exam teachers *************************************/

		$DB->DeleteRecord('exam_subject_teacher','teacher_id="'.$nameID.'"');
		foreach($_SESSION['teacher_subject'] as $teacher_subject)
		{
			$exam_subject_teacher = array('teacher_id' => $nameID);
			$exam_subject_teacher['exam_id'] = $teacher_subject['exam_id'];
			$exam_subject_teacher['subject_id'] = $teacher_subject['subject_id'];
			
			$DB->InsertRecord('exam_subject_teacher', $exam_subject_teacher);
		}
	
		/******** Add teachers candidate *************************************/

		$DB->DeleteRecord('exam_candidate_teacher','teacher_id="'.$nameID.'"');
		foreach($_SESSION['teacher_candidates'] as $exam_id => $exam_candidate)
		{
			foreach($exam_candidate as $candidate_id)
			{
				$exam_candidate_teacher = array('teacher_id' => $nameID);
				$exam_candidate_teacher['exam_id'] = $exam_id;
				$exam_candidate_teacher['candidate_id'] = $candidate_id;
				
				$DB->InsertRecord('exam_candidate_teacher', $exam_candidate_teacher);
			}
		}
		
		$_SESSION['success']="Teacher details has been $action successfully.";
		Redirect(CreateURL('index.php','mod=teacher_master&do=manage'));
		exit;	
	}
	
	function getRecordTeacher(&$totalCount)
	{
			
		global $DB,$frmdata;
		
		$query='';
		$query="select teacher.* ";
		$query.=" from teacher ";
		
		$where = array();
		
		if(isset($frmdata['name']) && $frmdata['name'] !='')
		{
			$name = addslashes($frmdata['name']);
			$where[] =" ( (first_name like '%$name%') OR (last_name like '%$name%') OR
					(CONCAT_WS(' ', first_name, last_name) like '%$name%') )";
		}

		if(count($where) > 0)
		{
			$where = implode(' ) AND ( ', $where);
			$query.=" WHERE ($where)";	
		}
		
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{	
	  		$query.=" order by ".$frmdata['orderby'];
		}	
		else
		{
			$query.=' order by id desc ';
		}

		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		return $result;   
	}

}
?>