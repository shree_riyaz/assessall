<?php 
/**
 *	@author : Ashwini Agarwal
 *	@desc	: Teacher management
 */

defined("ACCESS") or die("Access Restricted");
include_once('class.teacher_master.php');		
$DB = new DBFilter();
$TeacherMasterOBJ= new TeacherMaster();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($frmdata['addteacher']))
{
	$TeacherMasterOBJ->saveTeacher();	
}

if(isset($frmdata['editteacher']))
{
	$TeacherMasterOBJ->saveTeacher($nameID);	
}

$do = '';
if(isset($getVars['do']))
{
	$do=$getVars['do'];
}

$exam = $DB->SelectRecords('examination', '', '*', 'order by exam_name');
switch($do)
{
	default:  	
	case 'manage':
		PaginationWork();
		$teacherlist= $TeacherMasterOBJ->getRecordTeacher($totalCount);
	    $CFG->template="teacher_master/manage.php";
	break; 	

	case 'add':

		if(!$frmdata['addteacher'])
		{
			$_SESSION['select_tab'] = 0;
			$_SESSION['teacher_candidates'] = array();
			$_SESSION['teacher_subject'] = array();
		}
		
	   	$CFG->template="teacher_master/save.php";
   	break;
	
	case 'edit':
	   	$teacher_detail=$DB->SelectRecord('teacher','id='.$nameID);
	  	
	   	if(!$frmdata['editteacher'])
	   	{
		   	foreach($teacher_detail as $key => $cd)
		   	{
		   		$frmdata[$key] = $cd;
		   	}
		   	
		   	$_SESSION['teacher_candidates'] = array();
		   	$teacher_candidates = $DB->SelectRecords('exam_candidate_teacher', "teacher_id = $nameID", '*');
		   	
		   	foreach($teacher_candidates as $tc)
		   	{
		   		$_SESSION['teacher_candidates'][$tc->exam_id][$tc->candidate_id] = $tc->candidate_id;
		   	}
		   	
		   	$_SESSION['teacher_subject'] = getSubjectsByTeacher($nameID);
		   	$_SESSION['select_tab'] = 0;
	   	}
		   	
		$isEdit = true;
		$CFG->template="teacher_master/save.php";
	break;

	case 'delete':
		$teacher_detail=$DB->SelectRecord('teacher','id='.$nameID);
		if($teacher_detail->pro_image)
		{
			@unlink(ROOT . "/uploadfiles/" . $teacher_detail->pro_image);
		}
				
		$DB->DeleteRecord('exam_subject_teacher','teacher_id="'.$nameID.'"');
		$DB->DeleteRecord('teacher','id="'.$nameID.'"');
		$_SESSION['success']="Record has been deleted successfully.";
		Redirect(CreateURL('index.php','mod=teacher_master&do=manage'));
	exit();
} 
	
include(CURRENTTEMP."/index.php");
exit;
?>