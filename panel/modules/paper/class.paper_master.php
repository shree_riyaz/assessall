<?php
/*****************Developed by :- Richa verma
	                Date         :- 21-june-2011
					Module       :- Paper master
					Purpose      :- Class for function to add and edit paper
***********************************************************************************/

class PaperMaster
{
	//==========================================================================
	//to add a paper
	//==========================================================================	
	function addPaper()
	{
		global $DB,$frmdata;
		//print_r($frmdata);exit;
		$err='';
		if($frmdata['paper_title']=='')
		{
			$err.="Please enter paper title.<br>";
		}

		if($frmdata['total_subject']<=0)
		{
			$err.="Please select subject.<br>";
		}
		else
		{
			for($counter=0;$counter<$frmdata['total_subject'];$counter++)
			{
				if($frmdata['subject_id_'.($counter+1)]!='')
				{
					break;
				}
			}
			if($frmdata['total_subject']==$counter)
			{
				$err.="Please select subject.<br>";
			}
		}
		
		if($err=='')
		{
			$no_subjective = ' AND ( question_type <> "S" ) ';
			$frmdata['subject_id']=-1;

			$paper_id=$DB->InsertRecord('paper',$frmdata);
				
			for($counter=0;$counter<$frmdata['total_subject'];$counter++)
			{
				if($frmdata['subject_id_'.($counter+1)]=='')
				{
					continue;
				}
				else 
				{
					$frmdata['paper_id'] = $paper_id;
					$frmdata['subject_id'] = $frmdata['subject_id_'.($counter+1)];
					$frmdata['subject_total_marks'] = $frmdata['subject_total_mark_'.($counter+1)];
					$frmdata['show_subject_diff_question'] = $frmdata['subject_diff_question_'.($counter+1)];
					
					if($frmdata['random_que_id_'.($counter+1)]!='' && $frmdata['custom_que_id_'.($counter+1)]!='')
					{
						$frmdata['question_selection']="R,C";
					}
					elseif ($frmdata['random_que_id_'.($counter+1)]!='')
					{
						$frmdata['question_selection']="R";
					}
					elseif ($frmdata['custom_que_id_'.($counter+1)]!='')
					{
						$frmdata['question_selection']="C";
					}
					
					$DB->InsertRecord('paper_subject',$frmdata);
					
					if($frmdata['random_que_id_'.($counter+1)]!='')
					{
						$random_que_id_array = explode(",",$frmdata['random_que_id_'.($counter+1)]);
						
						for($index=0; $index<count($random_que_id_array); $index++)
						{
							$paper_question_table_data['paper_id'] = $paper_id;
							$paper_question_table_data['question_id'] = $random_que_id_array[$index];
							$paper_question_table_data['question_selection'] = 'R';
							$paper_question_table_data['subject_id'] = $frmdata['subject_id'];
							
							$DB->InsertRecord('paper_questions',$paper_question_table_data);
							
							$question_info = $DB->SelectRecord('question','id='.$random_que_id_array[$index]);
							
							if($question_info->question_level=='B')
							{
								if(isset($beginner_question[$question_info->marks]))
									$beginner_question[$question_info->marks]+=1;
								else 
									$beginner_question[$question_info->marks] = 1;
							}
							
							if($question_info->question_level=='I')
							{
								if(isset($inter_question[$question_info->marks]))
									$inter_question[$question_info->marks]+=1;
								else 
									$inter_question[$question_info->marks] = 1;
							}
						
							if($question_info->question_level=='H')
							{
								if(isset($higher_question[$question_info->marks]))
									$higher_question[$question_info->marks]+=1;
								else 
									$higher_question[$question_info->marks] = 1;
							}
						}
						
						if(isset($beginner_question))
						{
							foreach($beginner_question as $key=>$val) // key=marks val=no of question
							{
								$paper_question_selection['paper_id']=$paper_id;
								$paper_question_selection['question_level']='B';
								$paper_question_selection['question_marks']=$key;
								$paper_question_selection['question_total']=$val;
								$paper_question_selection['subject_id']=$frmdata['subject_id'];
								
								$DB->InsertRecord('paper_question_selection',$paper_question_selection);
							}
						}
						unset($beginner_question);
						
						if(isset($inter_question))
						{
							foreach($inter_question as $key=>$val) // key=marks val=no of question
							{
								$paper_question_selection['paper_id']=$paper_id;
								$paper_question_selection['question_level']='I';
								$paper_question_selection['question_marks']=$key;
								$paper_question_selection['question_total']=$val;
								$paper_question_selection['subject_id']=$frmdata['subject_id'];
								
								$DB->InsertRecord('paper_question_selection',$paper_question_selection);
							}
						}
						unset($inter_question);
					
						if(isset($higher_question))
						{
							foreach($higher_question as $key=>$val) // key=marks val=no of question
							{
								$paper_question_selection['paper_id']=$paper_id;
								$paper_question_selection['question_level']='H';
								$paper_question_selection['question_marks']=$key;
								$paper_question_selection['question_total']=$val;
								$paper_question_selection['subject_id']=$frmdata['subject_id'];
								
								$DB->InsertRecord('paper_question_selection',$paper_question_selection);
							}
						}
						unset($higher_question);
					}
					
					if ($frmdata['custom_que_id_'.($counter+1)]!='')
					{
						$custom_que_id_array = explode(",",$frmdata['custom_que_id_'.($counter+1)]);
						
						for($index=0; $index<count($custom_que_id_array); $index++)
						{
							$paper_question_table_data['paper_id'] = $paper_id;
							$paper_question_table_data['question_id'] = $custom_que_id_array[$index];
							$paper_question_table_data['question_selection'] = 'C';
							$paper_question_table_data['subject_id'] = $frmdata['subject_id'];
							
							$DB->InsertRecord('paper_questions',$paper_question_table_data);
						}
					}
				}
			}
			$_SESSION['success']='Paper details has been added successfully.';
			$frmdata='';
			Redirect(CreateURL('index.php','mod=paper&do=manage'));
			exit;
		}
		else
		{
			$_SESSION['error']=$err;
		}	
		//exit;
	}
	//==========================================================================
	
	
	//==========================================================================
	// to edit paper information
	//==========================================================================
	function editPaper($nameID)
	{
		global $DB,$frmdata;
		$err='';
		
		$test_paper = $DB->SelectRecords('test_paper','paper_id='.$nameID);
		
		if($test_paper)
		{
			$count_test_paper = count($test_paper);
			if($count_test_paper>1)
				$err = "Permission denied. ".$count_test_paper." tests have been scheduled with this paper.";
			elseif($count_test_paper==1)
				$err = "Permission denied. ".$count_test_paper." test has been scheduled with this paper.";
		}
		else
		{
			if($frmdata['paper_title']=='')
			{
				$err.="Please enter paper title.<br>";
			}
			
			if($frmdata['total_subject']<=0)
			{
				$err.="Please select subject.<br>";
			}
			else
			{
				for($counter=0;$counter<$frmdata['total_subject'];$counter++)
				{
					if($frmdata['subject_id_'.($counter+1)]!='')
					{
						break;
					}
				}
				if($frmdata['total_subject']==$counter)
				{
					$err.="Please select subject.<br>";
				}
			}
		}
		
		if($err=='')
		{
			$no_subjective = ' AND ( question_type <> "S" ) ';
		
			$DB->DeleteRecord('paper_question_selection','paper_id="'.$nameID.'"');
			$DB->DeleteRecord('paper_questions','paper_id="'.$nameID.'"');
			$DB->DeleteRecord('paper_subject','paper_id="'.$nameID.'"');
			
			
			if($frmdata['show_diff_question']!='Y')
			{
				$frmdata['show_diff_question']='N';
			}
			$frmdata['subject_id']=-1;
			
			$DB->UpdateRecord('paper',$frmdata,"id=$nameID");
			for($counter=0;$counter<$frmdata['total_subject'];$counter++)
			{
				if($frmdata['subject_id_'.($counter+1)]=='')
				{
					continue;
				}
				else 
				{
					$frmdata['paper_id'] = $nameID;
					$frmdata['subject_id'] = $frmdata['subject_id_'.($counter+1)];
					$frmdata['subject_total_marks'] = $frmdata['subject_total_mark_'.($counter+1)];
					$frmdata['show_subject_diff_question'] = $frmdata['subject_diff_question_'.($counter+1)];
					
					if($frmdata['random_que_id_'.($counter+1)]!='' && $frmdata['custom_que_id_'.($counter+1)]!='')
					{
						$frmdata['question_selection']="R,C";
					}
					elseif ($frmdata['random_que_id_'.($counter+1)]!='')
					{
						$frmdata['question_selection']="R";
					}
					elseif ($frmdata['custom_que_id_'.($counter+1)]!='')
					{
						$frmdata['question_selection']="C";
					}
					
					$DB->InsertRecord('paper_subject',$frmdata);
					
					if($frmdata['random_que_id_'.($counter+1)]!='')
					{
						$random_que_id_array = explode(",",$frmdata['random_que_id_'.($counter+1)]);
						
						for($index=0; $index<count($random_que_id_array); $index++)
						{
							$paper_question_table_data['paper_id'] = $nameID;
							$paper_question_table_data['question_id'] = $random_que_id_array[$index];
							$paper_question_table_data['question_selection'] = 'R';
							$paper_question_table_data['subject_id'] = $frmdata['subject_id'];
							
							$DB->InsertRecord('paper_questions',$paper_question_table_data);
							
							$question_info = $DB->SelectRecord('question','id='.$random_que_id_array[$index]);
							
							if($question_info->question_level=='B')
							{
								if(isset($beginner_question[$question_info->marks]))
									$beginner_question[$question_info->marks]+=1;
								else 
									$beginner_question[$question_info->marks] = 1;
							}
							
							if($question_info->question_level=='I')
							{
								if(isset($inter_question[$question_info->marks]))
									$inter_question[$question_info->marks]+=1;
								else 
									$inter_question[$question_info->marks] = 1;
							}
						
							if($question_info->question_level=='H')
							{
								if(isset($higher_question[$question_info->marks]))
									$higher_question[$question_info->marks]+=1;
								else 
									$higher_question[$question_info->marks] = 1;
							}
						}
						
						if(isset($beginner_question))
						{
							foreach($beginner_question as $key=>$val) // key=marks val=no of question
							{
								$paper_question_selection['paper_id']=$nameID;
								$paper_question_selection['question_level']='B';
								$paper_question_selection['question_marks']=$key;
								$paper_question_selection['question_total']=$val;
								$paper_question_selection['subject_id']=$frmdata['subject_id'];
								
								$DB->InsertRecord('paper_question_selection',$paper_question_selection);
							}
						}
						unset($beginner_question);
						
						if(isset($inter_question))
						{
							foreach($inter_question as $key=>$val) // key=marks val=no of question
							{
								$paper_question_selection['paper_id']=$nameID;
								$paper_question_selection['question_level']='I';
								$paper_question_selection['question_marks']=$key;
								$paper_question_selection['question_total']=$val;
								$paper_question_selection['subject_id']=$frmdata['subject_id'];
								
								$DB->InsertRecord('paper_question_selection',$paper_question_selection);
							}
						}
						unset($inter_question);
					
						if(isset($higher_question))
						{
							foreach($higher_question as $key=>$val) // key=marks val=no of question
							{
								$paper_question_selection['paper_id']=$nameID;
								$paper_question_selection['question_level']='H';
								$paper_question_selection['question_marks']=$key;
								$paper_question_selection['question_total']=$val;
								$paper_question_selection['subject_id']=$frmdata['subject_id'];
								
								$DB->InsertRecord('paper_question_selection',$paper_question_selection);
							}
						}
						unset($higher_question);
					}
					
					if ($frmdata['custom_que_id_'.($counter+1)]!='')
					{
						$custom_que_id_array = explode(",",$frmdata['custom_que_id_'.($counter+1)]);
						
						for($index=0; $index<count($custom_que_id_array); $index++)
						{
							$paper_question_table_data['paper_id'] = $nameID;
							$paper_question_table_data['question_id'] = $custom_que_id_array[$index];
							$paper_question_table_data['question_selection'] = 'C';
							$paper_question_table_data['subject_id'] = $frmdata['subject_id'];
							
							$DB->InsertRecord('paper_questions',$paper_question_table_data);
						}
					}
				}
			}
			
			$_SESSION['success']='Paper details has been updated successfully.';
			$frmdata='';
			Redirect(CreateURL('index.php',"mod=paper&do=manage"));
			exit;
		}
		else
		{
			$_SESSION['error']=$err;
		}	
	}
	
		
	function getRecordPaper(&$totalCount)
	{
		global $DB,$frmdata;
		
		$query = " select paper.* ";	
		$query.= " from ".PREFIX."paper ";
		$query.= " where 1 and ";
		
		if(isset($frmdata['paper_title_manage']) && $frmdata['paper_title_manage'] !='')
		{
			$query.="(";
			$query.=" paper.paper_title like '%".$frmdata['paper_title_manage']."%'" ; 
			$query.=")";
			$query.=" and";	
		}
		
		$query=substr($query,0,(strlen($query)-4));
		
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{
			$query.=" order by ".$frmdata['orderby'];
		}	
		else
		{
			$query.=' order by id desc';
		}
		
		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		return $result;   
	}
		

}//end of class
?>