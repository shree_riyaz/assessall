<?php 
 /*****************Developed by :- Richa verma
	                Date         :- 21-june-2011
					Module       :- Paper master
					Purpose      :- Entry file for any action, call in paper master module 
***********************************************************************************/

defined("ACCESS") or die("Access Restricted");
include_once('class.paper_master.php');		
$DB = new DBFilter();
$PaperMasterOBJ= new PaperMaster();

//print_r($frmdata);
if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($getVars['subject_id']))
{
	$subject_id=$getVars['subject_id'];
}

if(isset($getVars['random_que_id_set']))
{
	$random_que_id_set=$getVars['random_que_id_set'];
}

if(isset($getVars['custom_que_id_set']))
{
	$custom_que_id_set=$getVars['custom_que_id_set'];
}

if(isset($getVars['subject_total_mark']))
{
	$subject_total_mark=$getVars['subject_total_mark'];
}

if(isset($getVars['subject_diff_question']))
{
	$subject_diff_question=$getVars['subject_diff_question'];
}

if(isset($frmdata['submitAdd_form']) && $frmdata['submitAdd_form']==1)
{
	$PaperMasterOBJ->addPaper();	
}
if(isset($frmdata['submitEdit_form']) && $frmdata['submitEdit_form']==1)
{
	$PaperMasterOBJ->editPaper($nameID);	
}

if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}

switch($do)
{
		case 'add':
			$subject=$DB->SelectRecords('subject', '', '*');
			$CFG->template="paper_master/save.php";
		   break;
		   
		case 'addsubjectquestion':
			$subject_info = $DB->SelectRecord('subject', "id=$subject_id");
			makeRandomSession($random_que_id_set);
			
			if ($custom_que_id_set!='')
				$_SESSION['custom_question_id_set']=$custom_que_id_set;
			
			$CFG->template="paper_master/addsubjectquestion.php";
		   break;

		default:
		case 'manage':
			PaginationWork();
			$totalCount=0;			
			$paperlist= $PaperMasterOBJ->getRecordPaper($totalCount);
			
		    $CFG->template="paper_master/manage.php";
			break;
			
		case 'edit':
			$subject=$DB->SelectRecords('subject', '', '*');
		   	$paper_detail=$DB->SelectRecord('paper','id='.$nameID);

			if(!$_POST)
			{
				foreach($paper_detail as $key => $val)
				{
					$frmdata[$key] = $val;
				}
			}
		   
		   	$paper_subject = getPaperSubjectByPaperId($nameID);
		   	if(is_array($paper_subject))
		   	{
		   		$subject_total_question = getTotalSubjectQuestion($paper_subject, 'paper');
		   		$subject_random_question_set = getSubjectRandomQuestionId($paper_subject, 'paper');
		   		$subject_custom_question_set = getSubjectCustomQuestionId($paper_subject, 'paper');
		   	}
		   	
		   	$isEdit = true;
		    $CFG->template="paper_master/save.php";
		   	break;
		
		case 'delete':
			$DB->DeleteRecord('paper_question_selection','paper_id="'.$nameID.'"');
			$DB->DeleteRecord('paper_subject','paper_id="'.$nameID.'"');
			$DB->DeleteRecord('paper_questions','paper_id="'.$nameID.'"');
			$DB->DeleteRecord('paper','id="'.$nameID.'"');
			$_SESSION['success']="Record has been deleted successfully.";
			Redirect(CreateURL('index.php',"mod=paper&do=manage"));die();
} 	

include(CURRENTTEMP."/index.php");

exit;

?>