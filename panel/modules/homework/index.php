<?php
/**
 * 	@author	:	Ashwini Agarwal
 */

defined("ACCESS") or die("Access Restricted");
include_once('class.homework.php');		
$DB = new DBFilter();
$HomeworkOBJ= new Homework();

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}

if(isset($getVars['subject_id']))
{
	$subject_id=$getVars['subject_id'];
}

if(isset($getVars['exam_id']))
{
	$exam_id=$getVars['exam_id'];
}

if(isset($getVars['random_que_id_set']))
{
	$random_que_id_set=$getVars['random_que_id_set'];
}

if(isset($getVars['custom_que_id_set']))
{
	$custom_que_id_set=$getVars['custom_que_id_set'];
}

if(isset($getVars['subject_total_mark']))
{
	$subject_total_mark=$getVars['subject_total_mark'];
}

if(isset($getVars['subject_diff_question']))
{
	$subject_diff_question=$getVars['subject_diff_question'];
}

if(isset($frmdata['submitAdd_form']) && $frmdata['submitAdd_form']==1)
{
	$HomeworkOBJ->saveHomework();	
}
if(isset($frmdata['submitEdit_form']) && $frmdata['submitEdit_form']==1)
{
	$HomeworkOBJ->saveHomework($nameID);	
}
if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}

switch($do)
{	 
	default:  	
	case 'manage':
		PaginationWork();
		$homeworklist= $HomeworkOBJ->getRecordHomework($totalCount);
	    $CFG->template="homework/manage.php";
	break;
	
	case 'add':
	
		$query = 'SELECT e.* FROM examination e ';
		if ($_SESSION['admin_user_type'] == 'T')
		{
			$query .= " JOIN exam_subject_teacher est on (est.exam_id = e.id) ";
			$query .= " WHERE est.teacher_id = '".$_SESSION['admin_user_id']."' ";
		}
		$query .= ' GROUP BY e.id order by e.exam_name ';
		$exam=$DB->RunSelectQuery($query);
		
		if(!$_POST)
		{
			$_SESSION['test_candidates'] = array();
		}
		
		$subject=$DB->SelectRecords('subject', '', '*');
		$CFG->template="homework/save.php";
   	break;
		
	case 'edit':
		$homework_detail= $DB->SelectRecord('homework','id='.$nameID);
		if(!$_POST)
		{
			foreach($homework_detail as $key => $val)
			{
				$frmdata[$key] = $val;
			}
			
		   	$_SESSION['test_candidates'] = array();
		   	$test_candidates = $DB->SelectRecords('homework_candidate', "homework_id = $nameID", '*');
		   	
		   	foreach($test_candidates as $tc)
		   	{
		   		$_SESSION['test_candidates'][$homework_detail->exam_id][$tc->candidate_id] = $tc->candidate_id;
		   	}
		   	
			$frmdata['homework_date'] =  date("d/m/Y h:i:s A", strtotime($frmdata['homework_date']));
			$frmdata['homework_date_end'] =  date("d/m/Y h:i:s A", strtotime($frmdata['homework_date_end']));
		}

		$subject=$DB->SelectRecords('subject', '', '*');
		   	
	   	$homework_subject = getHomeworkSubjectByHomeworkId($nameID);
	   	if(is_array($homework_subject))
	   	{
	   		$subject_total_question = getTotalSubjectQuestion($homework_subject, 'homework');
	   		$subject_random_question_set = getSubjectRandomQuestionId($homework_subject, 'homework');
	   		$subject_custom_question_set = getSubjectCustomQuestionId($homework_subject, 'homework');
	   	}
		   	
		$query = 'SELECT e.* FROM examination e ';
		if ($_SESSION['admin_user_type'] == 'T')
		{
			$query .= " JOIN exam_subject_teacher est on (est.exam_id = e.id) ";
			$query .= " WHERE est.teacher_id = '".$_SESSION['admin_user_id']."' ";
		}
		$query .= ' GROUP BY e.id order by e.exam_name ';
		$exam=$DB->RunSelectQuery($query);
		
	   	$isEdit = true;
		$CFG->template="homework/save.php";
   	break;
		   
	case 'addsubjectquestion':
		$exam_subject_info = $DB->SelectRecord('exam_subjects', "subject_id=$subject_id and exam_id=$exam_id");
		$subject_info = $DB->SelectRecord('subject', "id=$subject_id");
		makeRandomSession($random_que_id_set);
			
		if ($custom_que_id_set!='')
		{
			$_SESSION['custom_question_id_set']=$custom_que_id_set;
		}
				
		$CFG->template="homework/addsubjectquestion.php";
   	break;
			 
	case 'delete':
		$DB->DeleteRecord('homework_question_selection','homework_id="'.$nameID.'"');
		$DB->DeleteRecord('homework_subject','homework_id="'.$nameID.'"');
		$DB->DeleteRecord('homework_questions','homework_id="'.$nameID.'"');
		$DB->DeleteRecord('homework','id="'.$nameID.'"');
		$_SESSION['success']="Record has been deleted successfully.";
		Redirect(CreateURL('index.php',"mod=homework"));
	exit();
} 	

include(CURRENTTEMP."/index.php");

exit;

?>