<?php
/**
 * 	@author	:	Ashwini Agarwal
 */
class Homework
{
	function saveHomework($nameID = '')
	{
		global $DB,$frmdata;
		$err='';
		
		if($nameID)
		{
			$homework_history = $DB->SelectRecord('candidate_homework_history', "homework_id=$nameID");
			if($homework_history)
			{
				$_SESSION['error'] = 'Permission denied. This homework is already conducted.';
				return false;
			}
		}
		
		if($frmdata['homework_name']=='')
		{
			$err.="Please enter homework title.<br>";
		}
		
		if($frmdata['exam_id']=='')
		{
			$err.="Please select course.<br>";
		}
		
		if($frmdata['homework_date']=='')
		{
			$err.="Please enter start date of homework.<br>";
		}
		else
		{
			$homework_date = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['homework_date'] ) ) );
		}
		
		if($frmdata['homework_date_end']=='')
		{
			$err.="Please enter end date of homework.<br>";
		}
		else
		{
			$homework_date_end = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['homework_date_end'] ) ) );
		}
		
		if($homework_date_end != '')
		{
			if (($homework_date != '') && ($homework_date >= $homework_date_end))
			{
				$err.="Sorry! You must enter a valid date range.<br>";
			}
			
			elseif (date('Y-m-d H:i:s') > $homework_date_end)
			{
				$err.="Homework end date cannot be past.<br>";
			}
		}
			
		if (isset($frmdata['exam_id']) && $frmdata['exam_id']!='')
		{
			if($frmdata['total_subject']<=0)
			{
				$err.="Please select subject.<br>";
			}
			else
			{
				for($counter=0;$counter<$frmdata['total_subject'];$counter++)
				{
					if($frmdata['subject_id_'.($counter+1)]!='')
					{
						break;
					}
				}
				if($frmdata['total_subject']==$counter)
				{
					$err.="Please select subject.<br>";
				}
			}
		}
		
		
		if($err)
		{
			$_SESSION['error']=$err;
			return false;
		}

		$frmdata['homework_date'] = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['homework_date'] ) ) );
		$frmdata['homework_date_end'] = date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '-', $frmdata['homework_date_end'] ) ) );
		
		$frmdata['show_diff_question'] = ($frmdata['show_diff_question'] != 'Y')? 'N' : 'Y';
		
		if($nameID)
		{
			$DB->UpdateRecord('homework',$frmdata,"id=$nameID");
			$action = 'updated';
		}
		else
		{
			if ($_SESSION['admin_user_type'] == 'T')
			{
				$frmdata['teacher_id'] = $_SESSION['admin_user_id'];
			}
			
			$nameID=$DB->InsertRecord('homework',$frmdata);
			$action = 'added';
		}

		$DB->DeleteRecord('homework_question_selection', "homework_id='$nameID'");
		$DB->DeleteRecord('homework_questions', "homework_id='$nameID'");
		$DB->DeleteRecord('homework_subject', "homework_id='$nameID'");
		
		for($counter=0;$counter<$frmdata['total_subject'];$counter++)
		{
			if($frmdata['subject_id_'.($counter+1)]=='')
			{
				continue;
			}
		
			$frmdata['homework_id'] = $nameID;
			$frmdata['subject_id'] = $frmdata['subject_id_'.($counter+1)];
			$frmdata['subject_total_marks'] = $frmdata['subject_total_mark_'.($counter+1)];
			$frmdata['show_subject_diff_question'] = $frmdata['subject_diff_question_'.($counter+1)];
			
			if($frmdata['random_que_id_'.($counter+1)]!='' && $frmdata['custom_que_id_'.($counter+1)]!='')
			{
				$frmdata['question_selection']="R,C";
			}
			elseif ($frmdata['random_que_id_'.($counter+1)]!='')
			{
				$frmdata['question_selection']="R";
			}
			elseif ($frmdata['custom_que_id_'.($counter+1)]!='')
			{
				$frmdata['question_selection']="C";
			}
			
			$DB->InsertRecord('homework_subject',$frmdata);
			
			if($frmdata['random_que_id_'.($counter+1)]!='')
			{
				$random_que_id_array = explode(",",$frmdata['random_que_id_'.($counter+1)]);
				
				for($index=0; $index<count($random_que_id_array); $index++)
				{
					$homework_question_table_data['homework_id'] = $nameID;
					$homework_question_table_data['question_id'] = $random_que_id_array[$index];
					$homework_question_table_data['question_selection'] = 'R';
					$homework_question_table_data['subject_id'] = $frmdata['subject_id'];
					
					$DB->InsertRecord('homework_questions',$homework_question_table_data);
					
					$question_info = $DB->SelectRecord('question','id='.$random_que_id_array[$index]);
					
					if(!isset($questions[$question_info->question_level][$question_info->marks]))
					{
						$questions[$question_info->question_level][$question_info->marks] = 0;
					}
					$questions[$question_info->question_level][$question_info->marks]++;
					
					foreach($questions as $level=>$que)
					{
						foreach($que as $key=>$val)
						{
							$homework_question_selection['homework_id'] = $nameID;
							$homework_question_selection['question_level'] = $level;
							$homework_question_selection['question_marks'] = $key;
							$homework_question_selection['question_total'] = $val;
							$homework_question_selection['subject_id'] = $frmdata['subject_id'];
							
							$DB->InsertRecord('homework_question_selection',$homework_question_selection);
						}
					}
					unset($questions);
				}
			}
			
			if ($frmdata['custom_que_id_'.($counter+1)]!='')
			{
				$custom_que_id_array = explode(",",$frmdata['custom_que_id_'.($counter+1)]);
				for($index=0; $index<count($custom_que_id_array); $index++)
				{
					$homework_question_table_data['homework_id'] = $nameID;
					$homework_question_table_data['question_id'] = $custom_que_id_array[$index];
					$homework_question_table_data['question_selection'] = 'C';
					$homework_question_table_data['subject_id'] = $frmdata['subject_id'];
					
					$DB->InsertRecord('homework_questions',$homework_question_table_data);
				}
			}
		}

		/******** Add candidate *************************************/
		$DB->DeleteRecord('homework_candidate','homework_id="'.$nameID.'"');
		foreach($_SESSION['test_candidates'] as $exam_id => $exam_candidate)
		{
			foreach($exam_candidate as $candidate_id)
			{
				$test_candidates = array('homework_id' => $nameID);
				$test_candidates['candidate_id'] = $candidate_id;
				
				$DB->InsertRecord('homework_candidate', $test_candidates);
			}
		}
		
		$_SESSION['success']= "Homework details has been $action successfully.";
		Redirect(CreateURL('index.php','mod=homework&do=manage'));
		exit;
	}
	
	// get homework information
	function getRecordHomework(&$totalCount)
	{
			
		global $DB,$frmdata;
		
		$query='';
		$query="select homework.*, exam.exam_name from ".PREFIX."homework 
				left join examination as exam on homework.exam_id=exam.id ";
	
		if ($_SESSION['admin_user_type'] == 'T')
		{
			$query .= " JOIN exam_subject_teacher est on (est.exam_id = exam.id) ";
			$query .= " WHERE est.teacher_id = '".$_SESSION['admin_user_id']."' AND homework.teacher_id = '".$_SESSION['admin_user_id']."' AND ";
		}
		else
		{
			$query .= ' WHERE 1 AND ';
		}
		
		if(isset($frmdata['name']) && $frmdata['name'] !='')
		{
			$query.="(";
			$query.=" homework.homework_name like '%".$frmdata['name']."%'" ; 
			$query.=")";
			$query.=" and";	
		}
		
		$query=substr($query,0,(strlen($query)-4));
		
		if(isset($frmdata['orderby']) && $frmdata['orderby']!='' )
		{
			if ($frmdata['orderby'] == 'homework_date')
			{
				$query.=" order by date_format(homework.homework_date, '%y-%m-%d %H:%i:%s')";
			}
			
			elseif ($frmdata['orderby'] == 'homework_date desc')
			{
				$query.=" order by date_format(homework.homework_date, '%y-%m-%d %H:%i:%s') desc";
			}
			
			elseif ($frmdata['orderby'] == 'homework_date_end')
			{
				$query.=" order by date_format(homework.homework_date_end, '%y-%m-%d %H:%i:%s')";
			}
			
			elseif ($frmdata['orderby'] == 'homework_date_end desc')
			{
				$query.=" order by date_format(homework.homework_date_end, '%y-%m-%d %H:%i:%s') desc";
			}
			
			else
			{
				$query.=" order by ".$frmdata['orderby'];
			}
		}	
		else
		{
			$query.=' order by id desc';
		}
		
		$result = $DB->RunSelectQueryWithPagination($query,$totalCount);
		return $result;   
	}
}
?>