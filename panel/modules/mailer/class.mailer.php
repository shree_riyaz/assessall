<?php 
/**
 * 	@author	:	Ashwini Agarwal
 * 	@desc	:	Handles mail calls
 */

class Mail
{
	/**
	 * 	@author	:	Ashwini Agarwal
	 * 	@desc	:	Send Email
	 */
	function sendMail()
	{
		global $frmdata, $DB;
		
		$err = '';
		if(($frmdata['exam_id'] == '') && ($frmdata['mailto'] == ''))
		{
			$err .= 'Please select course or enter an email.<br>';
		}
	
		if($frmdata['mailsubject'] == '')
		{
			$err .= 'Please enter mail subject.<br>';
		}
		
		if($frmdata['mailmessage'] == '')
		{
			$err .= 'Please enter message.<br>';
		}
		
		if($err)
		{
			$_SESSION['error'] = $err;
			return false;
		}

		$memberids = $this->getMemberList();
		if((count($memberids) == 0) && ($frmdata['mailto'] ==''))
		{
			$_SESSION['error'] = "No student found in selected course.<br>";
			return false;
		}

		$ccArray = array();
		if ($frmdata['mailcc'] !='')
		{
			$ccArray = array_map('trim', explode(',', $frmdata['mailcc']));
		}

		$mailmessage = $frmdata['mailmessage'];
		$subject = $frmdata['mailsubject'];
		
		include_once(ROOT.'/lib/mailer.php');
		
		$emails = array();
		$userIDs = array_unique($memberids);
		foreach($userIDs as $user_id)
		{		
			$userObj = $DB->SelectRecord('candidate','id='.$user_id);
			$emails[] = $userObj->email;
			
			$cleanedArray = $this->removeTags($mailmessage, $subject, $userObj);
			
			
			$mail = new mailer(true);
			$mail->addTo($userObj->email, ucwords(strtolower($userObj->first_name.' '.$userObj->last_name)));
			$mail->addCcArray($ccArray);
			$mail->setSubject($cleanedArray['subject']);
			$mail->setMessage($cleanedArray['message']);
			$mail->send();
		}

		if ($frmdata['mailto'] != '')
		{
			$toArray = array_map('trim', explode(',' ,$frmdata['mailto']));
			foreach ($toArray as $email)
			{	
				if(in_array($email, $emails)) continue;
				$emails[] = $email;
				
				$userObj = $DB->SelectRecord('candidate', "email='$email'");
				$cleanedArray = $this->removeTags($mailmessage, $subject, $userObj);
				
				$mail = new mailer();
				$mail->addTo($email);
				$mail->addCcArray($ccArray);
				$mail->setSubject($cleanedArray['subject']);
				$mail->setMessage($cleanedArray['message']);
				$mail->send();
			}
		}	
		
		$total = count($emails);
		$_SESSION['success']= "Mail has been sent successfully to $total students.";
		Redirect(CreateURL('index.php', 'mod=mailer'));
		exit;
	}
	
	/**
	 * 	@author :	Ashwini Agarwal
	 * 	@desc	:	Get list of candidate to send mail
	 */
	function getMemberList()
	{
		global $frmdata, $DB;
		
		if($frmdata['exam_id'] != '')
		{
			$exam_id = $frmdata['exam_id']; 
			$candidate_selection_cond = implode(',', $_SESSION['test_candidates'][$exam_id]);
			$candidate_selection_cond = $candidate_selection_cond ? $candidate_selection_cond : -1;
					
			$candidates = $DB->SelectRecords('exam_candidate', "(exam_id = '$exam_id') AND (candidate_id IN ($candidate_selection_cond))", 'candidate_id');	
		}
		
		$memberids = array();
		if($candidates)
		{
			foreach($candidates as $c)
			{
				$memberids[] = $c->candidate_id;
			}
		}
		return $memberids;
	}
	
	/**
	 * 	@author	:	Ashwini Agarwal
	 * 	@desc	:	Remove Tags
	 */
	function removeTags($message, $subject, $user)
	{
		global $DB;
		
		if($user == '')
		{
			$user = (object) $user;
			$user->candidate_id = '';
			$user->email = '';
			$user->last_name = '';
			$user->first_name = '';
			$user->id = '';
			$user->candidate_id = '';
		}
		
		if(strpos($message, htmlentities('<username>')))
		$message = str_replace(htmlentities('<username>'), $user->candidate_id, $message);
		
		if(strpos($message, htmlentities('<email>')))
		$message = str_replace(htmlentities('<email>'), $user->email, $message);
		
		if(strpos($message, htmlentities('<lastname>')))
		$message = str_replace(htmlentities('<lastname>'), $user->last_name, $message);
		
		if(strpos($message, htmlentities('<firstname>')))
		$message = str_replace(htmlentities('<firstname>'), $user->first_name, $message);
		
		if(strpos($message, htmlentities('<password>')))
		{
			$plain_pass = strtolower($user->first_name).'_'.$user->candidate_id;
			
			$password = ENCODE($plain_pass);
			$DB->ExecuteQuery("UPDATE candidate SET password = '$password' WHERE id = '$user->id'");
			
			$message = str_replace(htmlentities('<password>'), $plain_pass, $message);
		}
							
		if ((stripos($subject, '<firstname>', 1) == '') || (stripos($subject, '<firstname>', 1) >= 1)) 
		$subject = str_replace('<firstname>', $user->first_name, $subject);
			
		if ((stripos($subject, '<username>', 1) == '') || (stripos($subject, '<username>', 1) >=1)) 
		{
			$subject = str_replace('<username>', $user->candidate_id, $subject);
		}
		
		return array('message' => stripslashes($message), 'subject' => stripslashes($subject));
	}

	/**
	 * 	@author	:	Ashwini Agarwal
	 * 	@desc	:	Get editor instanse
	 */
	function getEmailEditor()
	{
		include_once(ROOT.'/lib/wysiwygPro/wysiwygPro.class.php');
		$editor = new wysiwygPro();
		$editor->name = 'mailmessage';
		$editor->imageDir = ROOT ."/media/";
		$editor->mediaDir = ROOT ."/media/";
		$editor->imageURL = ROOTURL.'/media/';
		$editor->mediaURL = ROOTURL.'/media/';
		$editor->urlFormat = 'absolute';
		$editor->addFont('Calibri');
		$editor->editImages = true;
		$editor->renameFiles = true;
		$editor->renameFolders = true;
		$editor->deleteFiles = true;
		$editor->deleteFolders = true;
		$editor->copyFiles = true;
		$editor->copyFolders = true;
		$editor->moveFiles = true;
		$editor->moveFolders = true;
		$editor->upload = true;
		$editor->overwrite = true;
		$editor->createFolders = true;
		
		return $editor;
	}
}
?>