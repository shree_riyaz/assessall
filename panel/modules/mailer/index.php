<?php
/*****************************************************************************************
 * 		Developed by :- Ashwini Agarwal
 * 		Date         :- November 2, 2012
 ****************************************************************************************/
defined("ACCESS") or die("Access Restricted");
$DB = new DBFilter();

include_once('class.mailer.php');
$MailerOBJ = new Mail();

$tags = array('FN'=>'<firstname>','LN'=>'<lastname>','UN'=>'<username>','E'=>'<email>','P'=>'<password>');

if(isset($frmdata['sendmail']))
	$MailerOBJ->sendMail();

$exam = $DB->SelectRecords('examination','','*','order by exam_name');
		
$editor = $MailerOBJ->getEmailEditor();
$CFG->template="mailer/bulkmail.php";
include(CURRENTTEMP."/index.php");		
?>