<?php 
/**
 * 	@author	:	Ashwini Agarwal
 */

class HomeworkHistory
{
	function getHomeworkHistory()
	{
		global $DB, $frmdata;
		$params = $this->getSearchParams();
		if(count($params) == 0)
		{
			if($_SESSION['admin_user_type'] != 'P')
			{
				return false;
			}
		}
		
		$query = " SELECT *, c.id as candidate_id FROM candidate_homework_history chh "
				." JOIN candidate c ON c.id = chh.candidate_id "
				." JOIN examination e ON e.id = chh.exam_id "
				." JOIN homework h ON h.id = chh.homework_id ";

		$where = array();
		if($_SESSION['admin_user_type'] == 'P')
		{
			$query .= " JOIN parent_candidate pc on pc.candidate_id = c.id ";
			$where[] = "(pc.is_approved = '1')";
			$where[] = "(pc.parent_id = '".$_SESSION['admin_user_id']."')";
		}
		elseif ($_SESSION['admin_user_type'] == 'T')
		{
			$query .= " JOIN exam_candidate_teacher ect ON ((ect.candidate_id = c.id) AND (ect.exam_id = chh.exam_id)) ";
			$where[] = "(ect.teacher_id = '".$_SESSION['admin_user_id']."')";
			$where[] = "(h.teacher_id = '".$_SESSION['admin_user_id']."')";
		}
				
		if(isset($params['exam_id']) && ($exam_id = $params['exam_id']))
		{
			$where[] = "(chh.exam_id = $exam_id)";
		}
		if(isset($params['homework_id']) && ($homework_id = $params['homework_id']))
		{
			$where[] = "(chh.homework_id = $homework_id)";
		}
		if(isset($params['candidate_id']) && ($candidate_id = $params['candidate_id']))
		{
			$where[] = "(chh.candidate_id = $candidate_id)";
		}
		if(isset($params['year']) && ($year = $params['year']))
		{
			$where[] = "((YEAR(chh.created_date) = $year))";
			
			if(isset($params['month']) && ($month = $params['month']))
			{
				$where[] = "(DATE_FORMAT(chh.created_date, '%Y-%m-%d') <= LAST_DAY(STR_TO_DATE('01,$month,$year','%d,%m,%Y'))) AND (DATE_FORMAT(chh.created_date, '%Y-%m-%d') >= STR_TO_DATE('01,$month,$year','%d,%m,%Y'))";
			}
		}
		
		if(count($where) > 0)
		{
			$where = implode(' AND ', $where);
			$query .= " WHERE $where ";
		}
		
		$query.=" GROUP BY chh.id ORDER BY chh.created_date ";
		
		$result = $DB->RunSelectQuery($query);
		return $result;
	}
	
	function getSearchParams()
	{
		global $frmdata, $getVars;
		
		$vars = array('show_history', 'exam_id', 'candidate_id', 'homework_id', 'year', 'month');
		$params = array();
		
		foreach($vars as $var)
		{
			if(isset($frmdata[$var]))
			{
				$params[$var] = $frmdata[$var];
			}
			elseif (isset($getVars[$var]))
			{
				$params[$var] = $getVars[$var];
			}
			
			if(!$params[$var]) unset($params[$var]);
		}
		
		if(!isset($params['show_history'])) $params = array();
		
		return $params;
	}
	
	function getBackUrl()
	{
		$params = $this->getSearchParams();
		if(count($params) == 0) return false;
		
		$url = '';
		foreach($params as $key => $val)
		{
			$url .= "&$key=$val";
		}
		
		return urlencode($url);
	}
}
?>