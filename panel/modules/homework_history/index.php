<?php
/**
 * 	@author	:	Ashwini Agarwal
 * 	@desc	:	homework history
 */
defined("ACCESS") or die("Access Restricted");
include_once("class.homework_history.php");
$HomeworkHistoryOBJ = new HomeworkHistory();

$do = '';
if(isset($getVars['do']))
{
	$do = $getVars['do'];
}

switch($do)
{
	default:	
		PaginationWork();
		$exam = getConductedExamInfoForHomework();
		$homework = getConductedHomework();
		$candidate = $DB->SelectRecords('candidate', '1', '');
		$query="select cand.id, CONCAT_WS(' ', cand.first_name, cand.last_name) as candidate_name 
				from candidate cand ";
			
		if($_SESSION['admin_user_type'] == 'P')
		{
			$query .= " JOIN parent_candidate pc on pc.candidate_id = cand.id ";
			$query .= " WHERE (pc.is_approved = '1') AND pc.parent_id = '".$_SESSION['admin_user_id']."' ";
		}
		elseif ($_SESSION['admin_user_type'] == 'T')
		{
			$query .= " JOIN exam_candidate_teacher ect ON (ect.candidate_id = cand.id) ";
			$query .= " WHERE ect.teacher_id = '".$_SESSION['admin_user_id']."' ";
		}
		$query .= ' group by cand.id order by cand.candidate_id+0';
		
		$candidate = $DB->RunSelectQuery($query);
		
		$homeworkyearlist = getHomeworkYears();
		
		$backUrl = $HomeworkHistoryOBJ->getBackUrl();
		$params = $HomeworkHistoryOBJ->getSearchParams();
		$homework_history = $HomeworkHistoryOBJ->getHomeworkHistory();
		
	    $CFG->template="homework_history/history.php";
	break; 
			
	case 'details':
		$backUrl = urldecode($getVars['backUrl']);
		
		$candidate_id = $getVars['candidate_id'];
		$homework_id = $getVars['homework_id'];
		
		if(!$candidate_id || !$homework_id)
		{
			Redirect(CreateURL('index.php',"mod=homework_history$backUrl"));
			exit;
		}
		
		$candidateInfo = $DB->SelectRecord('candidate',"id = '$candidate_id'");
		$homeworkInfo = $DB->SelectRecord('homework',"id = '$homework_id'");
		$examInfo = $DB->SelectRecord('examination', "id='$homeworkInfo->exam_id'");
		
		$homeworkQuestions = $DB->SelectRecords('homework_questions',"homework_id = '$homework_id'");
		$questionHistory = $DB->SelectRecords('candidate_homework_question_history', "((homework_id='$homework_id') AND (candidate_id='$candidate_id'))");
		
		$CFG->template="homework_history/details.php";
	break;
} 
	
include(CURRENTTEMP."/index.php");
exit;
?>