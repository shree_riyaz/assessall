<?php 

$ParentRoot = basename(dirname(__DIR__));
$RootFolder = $ParentRoot . '/' . basename((__DIR__));

define("ROOT",$_SERVER['DOCUMENT_ROOT']."/$RootFolder");
define("PARENTROOT",$_SERVER['DOCUMENT_ROOT']."/$ParentRoot");
define("ROOTURL","http://".$_SERVER['HTTP_HOST']."/$RootFolder");

@define("ADMINROOT",ROOT);
@define("ADMINURL",ROOTURL);

define("TEMPIMG",PARENTROOT."/Import");
define("CIMGURL",ROOTURL."/images");
define("QIMG",ROOT."/uploadfiles/question_images");
define("QIMGURL",ROOTURL."/uploadfiles/question_images");
define("PARENTROOTURL","http://".$_SERVER['HTTP_HOST']."/$ParentRoot");
@define("TEMPLETPATH",ROOT."/templates");
@define("MOD",ROOT."/modules");
@define("CURRENTTEMP",ROOT."/templates/default");
@define("IMAGES",CURRENTTEMP."/pics");
@define("BACKUP",ROOT."/backup");
@define("IMAGEURL",ROOTURL."/images");
@define("CURRENTTEMPURL",ROOTURL."/templates/default");
@define("STYLE",ROOTURL."/style");
@define("JS",ROOTURL."/js");
@define("MANDATORYNOTE","<span><strong>Note</strong> : Fields marked by (<font color='#FF0000'><strong>*</strong></font>) are mandatory.</span>"); // for mandatory notes
@define("MANDATORYMARK","  <font color='#FF0000' size='3'><strong>*</strong></font>"); // for mandatory mark
@define("STYLE",ROOTURL."/style");
@define('ACCESS',1);

@define('BENCHMARKING','');
@define('PREFIX','');


@define("BACKUP",ROOT."/backups");
define("CHROOTDIR","/var/chroot/home");


define("IMAGESIZE",2000000);
define("IMAGEEXT","jpg,gif,png,jpeg,pjpeg");
define("VIDEOSIZE",20000000);
define("VIDEOEXT","flv,mp4,avi,wav,mpeg,mpeg4,mov,vmw,mpg,vid,m4v");
define("FILESIZE",2097152);
define("FILEEXT","pdf,doc,txt,xls,ppt,htm,html,msg,zip,rar,docx,csv");
define("FILEPATH",ROOT."/uploadfiles/");
define("FILEDOCPATH",ROOT."/");

define("MAXSIZEMESSAGE","File is too big in size");
define("MINSIZEMESSAGE","File did not contain any data.");
define("FAILEDCOPYMESSAGE","File can not be copied");

@define('IMAGEHEIGHT',400);
@define('IMAGEWIDTH',400);

@define("MANMES","<font color=red>*</font>");


@define("HOST","localhost");
//@define("USER","wwwasses_assess");
//@define("PASSWORD",'Assessall321');
//@define("DATABASE","wwwasses_assessall");
@define("USER","root");
@define("PASSWORD",'');
@define("DATABASE","wwwasses_asses_new");
@define("EMODE","CLIENT");
?>