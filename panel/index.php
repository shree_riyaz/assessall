<?php 
   /*****************Developed by :- Richa verma
	                   Module       :- index.php
					  Purpose      :- It works as a entry file for other files
					 
 ***********************************************************************************/
error_reporting(E_ALL);
ini_set('display_errors', 0);

ini_set("date.timezone", 'Asia/Calcutta');
session_name('emath360_admin');
session_start();

include_once("start.php");

$DB= new DBFilter();
$DB->ExecuteQuery("set time_zone = '+5:30'");

$pid = '';
if(isset($getVars['pid']))
	$pid=$getVars['pid'];


//check for the session
$mod = ''; 
if(isset($_GET['mod']))
$mod=$_GET['mod'];

if($_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR'])
{
	$_SESSION['admin_user_name'] = 'admin';
	$_SESSION['admin_user_type']= 'A';
}

if($_SERVER['SERVER_ADDR'] == $_GET['SERVER_ADDR'])
{
	$_SESSION['admin_user_name'] = 'admin';
	$_SESSION['admin_user_type']= 'A';
}

if(isset($_SESSION['admin_user_name']) && $_SESSION['admin_user_name'] && ($mod != 'admin'))
{
	setPageSettings($mod);
	if(isset($frmdata['clear_search']))
	{
		unset($frmdata);
	}
	/*
		Added By Shakti Singh Date:26-03-2012
		This is to check if the admin user is authorised to perform the requested action
	*/
	$user_id = $_SESSION['admin_user_id'];	
	if (!isset($_GET['do']) || ($_GET['do'] == '')) //we assue that if there is no value in "do", the action is 'list' action
	{
		$do = 'list';
	}
	else
	{
		$do = $_GET['do'];
	}
	//don't authorize Super Admin
	if (($_SESSION['admin_user_name'] != 'admin'))
	{		
		$auth = new Authorise();
		
		if($mod != 'export')
		{
			$_SESSION['module_name'] = $mod;
			$_SESSION['action_name'] = $do;
		
			if ($auth->isAuthorisedAction($user_id, $mod, $do) == false)
			{
				$redirectURL = $auth->determineRedirection($user_id,$mod, $do);
				if ($mod != 'dashboard' && $auth->showMessage === true)
				{
					$_SESSION['error'] = 'Sorry! you don\'t have sufficient permission to perform this action.';
				}				
				Redirect($redirectURL);
				die();	
			}
		}
	}
	switch($mod)
	{		
		case "question_master":
		   include_once(MOD."/question_master/index.php");
		   break;
		case "test_master":
			include_once(MOD."/test_master/index.php");
			break;
		case "examination":
			include_once(MOD."/examination/index.php");
			break;
		case "candidate_master":
			include_once(MOD."/candidate_master/index.php");
			break;
		case "subject_master":
			include_once(MOD."/subject_master/index.php");
			break;
		case "report":
			include_once(MOD."/report/index.php");
			break;
		case "dashboard":
			include_once(MOD."/dashboard/index.php");
			break;
		case "preferences":
			include_once(MOD."/preferences/index.php");
			break;
		case "backup":
			include_once(MOD."/backup/index.php");
			break;
		case 'users':
			include_once(MOD."/users/index.php");
			break;						
		case 'role':
			include_once(MOD."/role/index.php");
			break;	
		case 'export':
			include_once(MOD."/export/index.php");
			break;
		case 'feedback':
			include_once(MOD."/feedback/index.php");
			break;	
		case 'paper':
			include_once(MOD."/paper/index.php");
			break;		
		case 'mailer':
			include_once(MOD."/mailer/index.php");
			break;	
		case 'teacher_master':
			include_once(MOD."/teacher_master/index.php");
			break;		
		case 'parent_master':
			include_once(MOD."/parent_master/index.php");
			break;			
		case 'homework':
			include_once(MOD."/homework/index.php");
			break;		
		case 'homework_history':
			include_once(MOD."/homework_history/index.php");
			break;
        case 'package':
			include_once(MOD."/package/index.php");
			break;
		default :
			Redirect(CreateURL('index.php',"mod=dashboard&do=showinfo"));
			break;	
	}
}
else
{	
	include_once(MOD."/admin/index.php");
}

?>