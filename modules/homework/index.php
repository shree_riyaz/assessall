<?php
defined("ACCESS") or die("Access Restricted");
global $DB;
include_once('class.homework.php');
$HomeworkOBJ = new Homework(); 

if(isset($_SESSION['is_exam_begin']))
{
	Redirect(CreateURL('index.php',"mod=exam"));
}

$homeworkIds = $_SESSION['homework_ids'];
$candidate_id = $_SESSION['candidate_id'];

if(!$homeworkIds || (count($homeworkIds) == 0))
{
	Redirect(CreateURL('index.php',"mod=guidelines"));
	exit;
}

if(isset($_POST) && (count($_POST) > 0))
{
	$HomeworkOBJ->saveAnswer();
}

if(isset($_GET['do']) == 'save_time')
{
	$HomeworkOBJ->saveTime();
	exit;
}

// if exam is not started yet
if(!isset($_SESSION['is_homework_begin']))
{
	// reset session
	$HomeworkOBJ->presetSession();
		
	$valid = false;
	if(isset($_SESSION['homework_id']) && in_array($_SESSION['homework_id'], $homeworkIds))
	{
		$valid = true;
		$_SESSION['homework']['is_resumed'] = false;
		$currentHomeworkId = $_SESSION['homework_id'];
		$_SESSION['homework']['exam_id'] = $DB->SelectRecord('homework', "id = $currentHomeworkId")->exam_id;
		if(in_array($currentHomeworkId, $_SESSION['incompleteHomeworkIds']))
		{
			$_SESSION['homework']['is_resumed'] = true;
		}
	}
	elseif(isset($_GET['homework']) && ($_GET['homework']))
	{
		$currentHomeworkId = $_GET['homework'];
		$currentHomeworkId = Decode(Decode(urldecode($currentHomeworkId)));
		
		if(in_array($currentHomeworkId, $homeworkIds))
		{
			$_SESSION['homework_id'] = $currentHomeworkId;
			Redirect(CreateURL('index.php',"mod=homework"));
		}
	}
	
	if(!$valid)
	{
		Redirect(CreateURL('index.php',"mod=guidelines"));
		exit;
	}
	
	// get homework subjects/paper
	$homework_subject = $DB->SelectRecords('homework_subject', "homework_id = $currentHomeworkId");
	
	$_SESSION['homework']['question_counter'] = $question_counter = 0;
	$_SESSION['homework']['subject_info_arr'] = array();
		
	for($index=0; $index < count($homework_subject); $index++)
	{
		$hSubject = $homework_subject[$index];
	
		// show different Random Questions to different users
		if($hSubject->show_subject_diff_question == 'Y')
		{
			/* get all questions info for current subject */
			$que = $HomeworkOBJ->getQuestionsInfoForSubject($hSubject->subject_id);
		
			// get CUSTOM Question
			$homework_custom_question = $DB->SelectRecords('homework_questions', "homework_id = $currentHomeworkId and subject_id=$hSubject->subject_id and question_selection='C'", '*', 'ORDER BY subject_id, RAND()');
			// set session for custom question and get list of added questions
			$cust_que_set_condition = $HomeworkOBJ->setSessionData($homework_custom_question, $hSubject->subject_id, $currentHomeworkId, true);
			
			/* randomly add remaining questions */
			$HomeworkOBJ->addRandomQuestions($que, $hSubject->subject_id, $cust_que_set_condition, $currentHomeworkId);
		}
		else
		{
			/* get list of question from database and set session */
			$homeworkQuestionDetails=$DB->SelectRecords('homework_questions',"homework_id = $currentHomeworkId and subject_id=".$hSubject->subject_id,'*','ORDER BY subject_id, RAND()');
			$HomeworkOBJ->setSessionData($homeworkQuestionDetails, $hSubject->subject_id, $currentHomeworkId);
		}
	}

	/* if homework is resumed. set session data */
	$_SESSION['homework']['review'] = array();
	if(isset($_SESSION['homework']['is_resumed']) && ($_SESSION['homework']['is_resumed'] == true))
	{
		$HomeworkOBJ->setSessionToResumeHomework($candidate_id, $_SESSION['homework']['exam_id'], $currentHomeworkId);
	}
	/**/
	
	$HomeworkOBJ->initialiseTempTable();
	$_SESSION['is_homework_begin'] = 1;
}

$currentHomeworkId = $_SESSION['homework_id'];

$CFG->template="homework/homework.php";
include(TEMP."/index.php");
exit;
?>