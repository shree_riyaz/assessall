<?php 
/**
 * 	@author : Ashwini Agarwal
 * 	@desc	: Get questions for exam
 */

class Homework
{
	/**
	 * 	@author	: Ashwini Agarwal
	 * 	@param 	: homework-questions, subject id, homework id, flag for custom questions
	 * 	@desc 	: store homework questions to session
	 */
	function setSessionData($questionData, $subjectId, $currentHomeworkId, $isCustom = false)
	{
		$question_counter = $_SESSION['homework']['question_counter'];
		if($questionData == '')
		{
			return false;
		}
	
		$custom_que_set = array();
		$count = count($questionData);
		for($counter=0; $counter < $count; $counter++,$question_counter++)
		{
			$qData = $questionData[$counter];
			$custom_que_set[] = $qData->question_id;
			$_SESSION['homework']['homework_id-'.$currentHomeworkId]['question_id-'.$question_counter] = $qData->question_id;
			
			if(!array_key_exists($qData->subject_id, $_SESSION['homework']['subject_info_arr']))
			{
				$_SESSION['homework']['subject_info_arr'][$qData->subject_id]['subject_name'] = $this->getSubjectNameBySubjectId($qData->subject_id);
				$_SESSION['homework']['subject_info_arr'][$qData->subject_id]['start_question_id'] = $question_counter;
			}
			else
			{
				$_SESSION['homework']['subject_info_arr'][$qData->subject_id]['last_question_id'] = $question_counter;
			}
		}
	
		$_SESSION['homework']['question_counter'] = $question_counter;
		if(!$isCustom) return true;
		
		$cust_que_set_condition = '';
		if(count($custom_que_set) > 0)
		{
			$custom_que_set = implode(',', $custom_que_set);
			$cust_que_set_condition = "id not in ($custom_que_set)";
		}
		
		return $cust_que_set_condition;
	}
	
	/**
	 * 	@author : Ashwini Agarwal
	 * 	@desc	: get question info for a subject
	 */
	
	function getQuestionsInfoForSubject($subject_id)
	{
		global $DB;
		
		$currentHomeworkId = $_SESSION['homework_id'];
		/* get all questions for current homework */
		$que = array();
		$homework_question_selection_info = $DB->SelectRecords('homework_question_selection', "homework_id = $currentHomeworkId and subject_id = $subject_id");
		for($counter=0;$counter<count($homework_question_selection_info);$counter++)
		{
			$qLevel = $homework_question_selection_info[$counter]->question_level;
			$qMarks = $homework_question_selection_info[$counter]->question_marks;
			$qTotal = $homework_question_selection_info[$counter]->question_total;
			$que[$qLevel][$qMarks] = $qTotal;
		}
		/**/
		
		return $que;
	}
	
	/**
	 * 	@author : Ashwini Agarwal
	 * 	@desc	: Add random question to session
	 */
	
	function addRandomQuestions($que, $subject_id, $cust_que_set_condition, $currentHomeworkId)
	{
		global $DB;
		/* randomly add remaining questions */
		foreach($que as $qLevel => $arr)
		{
			foreach($arr as $marks => $no_of_question)
			{
				$condition = array();
				$condition[] = "(question_level = '$qLevel')";
				$condition[] = "(marks = '$marks')";
				$condition[] = "(subject_id='$subject_id')";
				
				if($cust_que_set_condition)
				$condition[] = $cust_que_set_condition;
			
				$condition = implode(' AND ', $condition);
				
				$questionDetails = $DB->SelectRecords('question', $condition, '*, id as question_id', " ORDER BY RAND() LIMIT $no_of_question ");
				$this->setSessionData($questionDetails, $subject_id, $currentHomeworkId);
			}
		}
	}
	
	/**
	 * 	@author : Ashwini Agarwal
	 * 	@desc	: set session to resume homework
	 */
	
	function setSessionToResumeHomework($candidate_id, $exam_id, $currentHomeworkId)
	{
		global $DB;
		// get answer from temp table
		$temp_data = $DB->SelectRecords('candidate_temp_answer_homework', "(candidate_id=$candidate_id) AND (exam_id=$exam_id) AND (homework_id=$currentHomeworkId)");
		foreach($temp_data as $temp)
		{
			// if question is match type
			if($temp->is_match_type)
			{
				$match_data = $DB->SelectRecords('candidate_temp_match_answer_homework', "(candidate_id=$candidate_id) AND (exam_id=$exam_id) AND (homework_id=$currentHomeworkId) AND (question_id=$temp->question_id)");
				foreach($match_data as $match)
				{
					$_SESSION['homework']['question-'.$temp->question_id]['given_answer'][$match->question_match_left_id] = $match->given_answer_id;
					if($match->given_answer_id)
						$_SESSION['homework']['attempted_questions'][$temp->question_id] = $temp->question_id;
				}
			}
			else
			{
				$_SESSION['homework']['question-'.$temp->question_id]['given_answer'] = $temp->given_answer_id;
				
				// if more than one answers are allowed
				$question_answer=$DB->SelectRecords('question_answer','question_id='.$temp->question_id);
				if(count($question_answer) > 1)
				{
					$_SESSION['homework']['question-'.$temp->question_id]['given_answer'] = explode(',',$temp->given_answer_id);
				}

				if($temp->given_answer_id)
					$_SESSION['homework']['attempted_questions'][$temp->question_id] = $temp->question_id;
			}
			
			$_SESSION['homework']['viewed_questions'][$temp->question_id] = $temp->question_id;
			$_SESSION['homework']['question-'.$temp->question_id]['subject_id'] = $temp->subject_id;
			$_SESSION['homework']['question-'.$temp->question_id]['solve_time'] = $temp->solve_time;
			if($temp->is_review_marked)
				$_SESSION['homework']['review'][$temp->question_id] = $temp->question_id;
		}
	}
	
	/**
	 * 	@author : Ashwini Agarwal
	 * 	@desc	: Save answer submitted by candidate
	 */
	function saveAnswer()
	{
		if(!isset($_POST['display_quest']) || ($_POST['display_quest'] == ''))
			return false;
			
		global $DB;
		$tempdata = array();
		$ques_id = $_POST['ques_id'];
		$last_disp_quest = $_POST['display_quest'];
		$is_empty = true;
		
		$solve_time = time() - $_SESSION['homework']['solve_time'];
		$_SESSION['homework']['solve_time'] = time();
		
		if(!isset($_SESSION['homework']['question-'.$ques_id]) || ($_SESSION['homework']['question-'.$ques_id]['solve_time'] == ''))
			$_SESSION['homework']['question-'.$ques_id]['solve_time'] = 0;
		$_SESSION['homework']['question-'.$ques_id]['solve_time'] += $solve_time;

		if($_POST['ques_type'] == "MT")
		{
			$left_ids = $_POST['left_ids'];
			$_SESSION['homework']['question-'.$ques_id]['given_answer'] = array();
			
			$match_data = array();
			$countMatchQues = 0;
			foreach($left_ids as $left_id)
			{
				$match_data[$countMatchQues]['question_match_left_id'] = $left_id = (int) $left_id;
				$match_data[$countMatchQues++]['given_answer_id'] = $_SESSION['homework']['question-'.$ques_id]['given_answer'][$left_id] = $_POST[$left_id];
				
				if($_POST[$left_id] != '')
					$is_empty = false;
			}
			
			$tempdata['is_match_type'] = 1;
			$tempdata['given_answer_id'] = 0;
		}
		else
		{
			if(!isset($_POST['option'])) $_POST['option'] = '';
			$g_id =	$_SESSION['homework']['question-'.$ques_id]['given_answer'] = $_POST['option'];// candidates choosed answer
			$tempdata['is_match_type'] = '';
			
			if($_POST['option'] != '')
				$is_empty = false;
			
			if(is_array($g_id))
			{
				$g_id = implode(',',$g_id);
			}
			else
			{
				$g_id = addslashes($g_id);
			}
			$tempdata['given_answer_id'] = $g_id;
		}
		
		$tempdata['subject_id'] = $s_id = $_SESSION['homework']['question-'.$ques_id]['subject_id']=$_POST['question_subject_id'];
		$tempdata['homework_id'] = $t_id = $_SESSION['homework_id'];
		$tempdata['exam_id'] = $e_id = $_SESSION['homework']['exam_id'];
		$tempdata['candidate_id'] = $c_id = $_SESSION['candidate_id'];
		$tempdata['question_id'] = $q_id = $ques_id;
	 	$tempdata['time_consumed'] = time() - $_SESSION['homework']['start_time'];
		$tempdata['is_review_marked'] = '';
	 	$tempdata['solve_time'] = $_SESSION['homework']['question-'.$ques_id]['solve_time'];
		
		if(in_array($ques_id, $_SESSION['homework']['review']) || isset($_POST['mark_review']))
		{
			$tempdata['is_review_marked'] = 1;
		}
		if(isset($_POST['unmark_review']))
		{
			$tempdata['is_review_marked'] = '';
		}
	
	 	if($q_id)
	 	{
	 		$DB->DeleteRecord('candidate_temp_answer_homework', "(candidate_id=$c_id) AND (question_id=$q_id) AND (homework_id=$t_id)");
		 	$DB->InsertRecord('candidate_temp_answer_homework', $tempdata);
	 	}
	 	
	 	if($tempdata['is_match_type'] == 1)
	 	{
	 		$DB->DeleteRecord('candidate_temp_match_answer_homework', "(candidate_id=$c_id) AND (question_id=$q_id) AND (homework_id=$t_id)");
	 		foreach($match_data as $match)
	 		{
	 			$tempdata['question_match_left_id'] = $match['question_match_left_id'];
	 			$tempdata['given_answer_id'] = $match['given_answer_id'];
	 			$DB->InsertRecord('candidate_temp_match_answer_homework', $tempdata);
	 		}
	 	}
		
		$_SESSION['homework']['attempted_questions'][$ques_id] = $ques_id;
		if($is_empty)
			unset($_SESSION['homework']['attempted_questions'][$ques_id]);
	}
	
	/**
	 * 	@author : Ashwini Agarwal
	 * 	@desc	: Preset session variables
	 */
	function presetSession()
	{
		$_SESSION['homework'] = array();
		
		$_SESSION['homework']['start_time'] = time();
		$_SESSION['homework']['solve_time'] = time();
		$_SESSION['homework']['attempted_questions'] = array();
		$_SESSION['homework']['viewed_questions'] = array();
		$_SESSION['homework']['last_unmarked_question'] = '';
		$_SESSION['homework']['last_marked_question'] = '';
		$_SESSION['homework']['review'] = array();
	}
	
	function getSubjectNameBySubjectId($subject_id)
	{
		global $DB,$frmdata;
		
		$subject_info = $DB->SelectRecord('subject', "id=$subject_id", 'subject_name');
		
		return $subject_info->subject_name;
	}
	
	function initialiseTempTable()
	{
		global $DB;
		
		$tempdata['homework_id'] = $t_id = $_SESSION['homework_id'];
		$tempdata['exam_id'] = $_SESSION['homework']['exam_id'];
		$tempdata['candidate_id']= $c_id = $_SESSION['candidate_id'];
		$tempdata['question_id'] = $_SESSION['homework']['homework_id-'.$_SESSION['homework_id']]['question_id-0'];
	 	$tempdata['time_consumed'] = time() - $_SESSION['homework']['start_time'];
	 	$tempdata['solve_time'] = 0;
	 	
	 	if(!$DB->SelectRecord('candidate_temp_answer_homework', "(candidate_id=$c_id) AND (homework_id=$t_id)"))
	 	{
			$DB->InsertRecord('candidate_temp_answer_homework', $tempdata);
	 	}
	}
	
	function saveTime()
	{
		global $DB;
		
		$h_id = $_SESSION['homework_id'];
		$c_id = $_SESSION['candidate_id'];
		
		$tempdata['time_consumed'] = time() - $_SESSION['homework']['start_time'];
		$DB->UpdateRecord('candidate_temp_answer_homework', $tempdata, "(candidate_id=$c_id) AND (homework_id=$h_id)");
	}
}
?>