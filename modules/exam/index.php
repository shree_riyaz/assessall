<?php
/**
 * 	@author : Akshay Yadav
 */


defined("ACCESS") or die("Access Restricted");
global $DB;
include_once('class.exam.php');

$ExamOBJ = new Exam(); 

$currectTestId = $_SESSION['testid'];
$candidate_id = $_SESSION['candidate_id'];
$exam_id = $_SESSION['exam_id'];

if(isset($_POST) && (count($_POST) > 0))
{
	$ExamOBJ->saveAnswer();
}
if(isset($_GET['do']) == 'save_time')
{
	$ExamOBJ->saveTime();
	exit;
}

// if exam is not started yet
if(!isset($_SESSION['is_exam_begin']))
{
	// reset session
	$ExamOBJ->presetSession();

	// get current test info
	$test_info = $DB->SelectRecord('test', "id = '$currectTestId'");
	
	/* get question media (subject/paper). set variables accordingly */
	$qSource = $ExamOBJ->getQuestionSource($test_info);;
	
	$qSourceTab = $qSource['qSourceTab'];
	$qMediaCondition = $qSource['qMediaCondition'];
	
	// get test subjects/paper
	$test_subject = $DB->SelectRecords($qSourceTab.'_subject',$qMediaCondition);
	
	$_SESSION['question_counter'] = $question_counter = 0;
	$_SESSION['subject_info_arr'] = array();
		
	for($index=0; $index < count($test_subject); $index++)
	{
		$tSubject = $test_subject[$index];
				
		// show different Random Questions to different users
		/*if($tSubject->show_subject_diff_question == 'Y')
		{
			
			$que = $ExamOBJ->getQuestionsInfoForSubject($tSubject->subject_id);
		
		
			$test_custom_question = $DB->SelectRecords($qSourceTab.'_questions', "$qMediaCondition and subject_id=$tSubject->subject_id and question_selection='C'", '*', 'ORDER BY subject_id, RAND()');
		
			$cust_que_set_condition = $ExamOBJ->setSessionData($test_custom_question, $tSubject->subject_id, $currectTestId,true);
			
		
			$ExamOBJ->addRandomQuestions($que, $tSubject->subject_id, $cust_que_set_condition, $currectTestId);
		}
		else
		{*/
			/* get list of question from database and set session */
			
				$testDetails=$DB->SelectRecords($qSourceTab.'_questions',"$qMediaCondition and subject_id=$tSubject->subject_id and question_selection='C'",'*','ORDER BY question_id');
				$testDetails_group=$DB->SelectRecords($qSourceTab.'_questions',"$qMediaCondition and subject_id=$tSubject->subject_id and question_selection='G'",'*','ORDER BY question_id');
			
			//print_r($testDetails);exit;
			$group_array = array();
			$test_group_data = array();
			$test_custom_data = array();
			$group_array_counter = array();
			$custom_array_counter = array();
			$unset_array = array();
			$new_total_array = array();
			$new_group_data = array();
			$random_custom_data = array();
			$custom_counter = 1;
			if($testDetails)
			{
			$count_custom = count($testDetails);
			}
			else
			{
			$count_custom = 0;
			}
			if($testDetails_group)
			{
			 $count_group = count($testDetails_group);
			}
			else
			{
			$count_group = 0;
			}
			
			 $count_total = $count_custom + $count_group;
			foreach($testDetails as $key=>$value)
			{
				
				$test_custom_data[$custom_counter] = $value->question_id;
					$custom_array_counter[] = $custom_counter;
					$custom_counter++;
			}

			foreach($testDetails_group as $key=>$value)
			{
				$result_group = $DB->SelectRecord('group_questions','id='.$value->group_question_id);
				if(!in_array($value->group_question_id,$group_array))
				{
					$counter = $result_group->start; 
					$group_array[] = $value->group_question_id;
					$test_group_data[$result_group->start] = $value->question_id;
				}else
				{
					$test_group_data[$counter] = $value->question_id;
				}
				$group_array_counter[] = $counter;
				$counter++;
			}
			
			foreach($test_custom_data as $key=>$value)
			{
				if(in_array($key,$group_array_counter))
				{
				unset($test_custom_data[$key]);
				$unset_array[] = $value;
				}
			}
			
			$new_group_data = $test_custom_data+$test_group_data;
			foreach($new_group_data as $key => $value)
			{
				$new_total_array[] = $key;
			}
			$unset_counter = 0;
			//Total Question 50
		
			for($k=1;$k <= $count_total;$k++ )
			{
				if(!in_array($k,$new_total_array))
				{
						$new_group_data[$k] = $unset_array[$unset_counter];
						$unset_counter++;
				}
			}
			
			
			
			
			$test_custom_data = implode(',', $test_custom_data);
			if($test_custom_data)
			$cust_que_set_condition = "$qMediaCondition and subject_id=$tSubject->subject_id and id not in ($test_custom_data) and question_selection='R'";
			else
			$cust_que_set_condition = "$qMediaCondition and subject_id=$tSubject->subject_id and question_selection='R'";
			
			$questionDetails_count = $DB->SelectRecords('test_questions', $cust_que_set_condition);
			/*if($questionDetails_count)
			{
			//$count_question = count($questionDetails_count);
				
		//	$questionDetails_random = $DB->SelectRecords('question', $condition, '*', " ORDER BY RAND() LIMIT $count_question ");
			}*/
			if($questionDetails_count)
			 $count_random = count($questionDetails_count);
			else
			 $count_random = 0;
			
			 $count_grand_total = $count_random + $count_total;
			/*foreach($questionDetails_random as $key=>$value)
			{
				if(in_array($key,$group_array_counter))
				{
				unset($test_custom_data[$key]);
				$unset_array[] = $value;
				}
			}*/
			foreach($questionDetails_count as $key=>$value)
			{
				
				$random_custom_data[] = $value->question_id;
					
				
			}
			$re_counter = 0;
			for($m=1;$m <= $count_grand_total;$m++ )
			{
				if($new_group_data[$m])
				{
						//$new_group_data[$m] = $new_total_array[$unset_counter];
						
				}
				else
				{
						$new_group_data[$m] = $random_custom_data[$re_counter];
						$re_counter++;
				}
			}
			ksort($new_group_data);
			
			
			$ExamOBJ->setSessionData($new_group_data, $tSubject->subject_id, $currectTestId);
			
			//$que = $ExamOBJ->getQuestionsInfoForSubject($tSubject->subject_id);
			
			//$ExamOBJ->addRandomQuestions($que, $tSubject->subject_id, $cust_que_set_condition, $currectTestId);
			
			
			
		
		
			
		/*}*/
	}
	
	/* if test is resumed. set session data */
	$_SESSION['review'] = array();
	if(isset($_SESSION['is_resumed']) && ($_SESSION['is_resumed'] == true))
	{
		$ExamOBJ->setSessionToResumeTest($candidate_id, $exam_id, $currectTestId);
	}	
	$_SESSION['date'] = date('m/d/Y h:i:s A', time() + ($_SESSION['time_takenfortest'] * 60));
	$_SESSION['is_exam_begin'] = 1;
	$_SESSION['test_start_time'] = $_SESSION['solve_time'] = time();
	
	if(!isset($_SESSION['review'])) $_SESSION['review'] = array();
	
	$ExamOBJ->initialiseTempTable();
}
$end = $_REQUEST['end'];
$mod = 'exam';
//----------------------------------
switch($end)
{
		case 'lastpg':
			
		   $CFG->template="exam/lastpg.php";
		   break;
		   
		
		default:
		$modResult = 'result';
		$isSampleTest = false;
		$currectTestId = $_SESSION['testid'];
			$CFG->template="exam/common.php";
					break; 	
} 	

//-----------------------------------

include(TEMP."/index.php");
exit;
?>