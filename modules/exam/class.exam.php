<?php 
/**
 * 	@author : Akshay Yadav
 * 	@desc	: Get questions for exam
 */

class Exam
{
	/**
	 * 	@author :  Akshay Yadav
	 * 	@desc	: get question media
	 */
	function getQuestionSource($test_info)
	{
		global $DB;
		/* get question media (subject/paper). set variables accordingly */
		$_SESSION['questionMedia'] = $qMedia = $test_info->question_media;
		
		if($qMedia == 'paper')
		{
			$qSourceTab = 'paper';
			$test_paper = $DB->SelectRecord('test_paper','test_id='.$test_info->id);
			$_SESSION['testPaperId'] = $paper_id = $test_paper->paper_id;
			$qMediaCondition = " paper_id = $paper_id ";
		}
		else
		{
			$qSourceTab = 'test';
			$qMediaCondition = " test_id = $test_info->id ";
		}
		
		$_SESSION['qSourceTab'] = $qSourceTab;
		$_SESSION['qMediaCondition'] = $qMediaCondition;
		
		return array('qSourceTab'=>$qSourceTab, 'qMediaCondition'=>$qMediaCondition);
	}
	
	/**
	 * 	@author	:  Akshay Yadav
	 * 	@param 	: test-questions, subject id, test id, flag for custom questions
	 * 	@desc 	: store test questions to session
	 */
	function setSessionData($questionData, $subjectId, $currectTestId, $isCustom = false)
	{
		$question_counter = $_SESSION['question_counter'];
		if($questionData == '')
		{
			return false;
		}

		$custom_que_set = array();
		$count = count($questionData);

		for($counter=1; $counter <= $count; $counter++,$question_counter++)
		{
			$qData = $questionData[$counter];
			if($qData)
			$custom_que_set[] = $qData;
			$_SESSION['test_id-'.$currectTestId]['question_id-'.$question_counter] = $qData;
			$_SESSION['subject_id-'.$subjectId]['question_id-'.$question_counter] = $qData;
			if(!array_key_exists($subjectId, $_SESSION['subject_info_arr']))
			{
				$_SESSION['subject_info_arr'][$subjectId]['subject_name'] = $this->getSubjectNameBySubjectId($subjectId);
				$_SESSION['subject_info_arr'][$subjectId]['subject_id'] = $subjectId;
				$_SESSION['subject_info_arr'][$subjectId]['start_question_id'] = $question_counter;
			}
			else
			{
				$_SESSION['subject_info_arr'][$subjectId]['last_question_id'] = $question_counter;
			}
		}
	
		$_SESSION['question_counter'] = $question_counter;
		if(!$isCustom) return true;
		
		$cust_que_set_condition = '';
		if(count($custom_que_set) > 0)
		{
			$custom_que_set = implode(',', $custom_que_set);
			$cust_que_set_condition = "id not in ($custom_que_set)";
		}
		
		return $cust_que_set_condition;
	}
	
	/**
	 * 	@author :  Akshay Yadav
	 * 	@desc	: get question info for a subject
	 */
	
	function getQuestionsInfoForSubject($subject_id)
	{
		global $DB;
		
		$qSourceTab = $_SESSION['qSourceTab'];
		$qMediaCondition = $_SESSION['qMediaCondition'];
		
		/* get all questions for current test */
		$que = array();
		$test_question_selection_info = $DB->SelectRecords($qSourceTab.'_question_selection', $qMediaCondition." and subject_id=".$subject_id);
		for($counter=0;$counter<count($test_question_selection_info);$counter++)
		{
			$qLevel = $test_question_selection_info[$counter]->question_level;
			$qMarks = $test_question_selection_info[$counter]->question_marks;
			$qTotal = $test_question_selection_info[$counter]->question_total;
			$que[$qLevel][$qMarks] = $qTotal;
		}
		/**/
		
		return $que;
	}
	
	/**
	 * 	@author :  Akshay Yadav
	 * 	@desc	: Add random question to session
	 */
	
	function addRandomQuestions($que, $subject_id, $cust_que_set_condition, $currectTestId)
	{
		global $DB;
		/* randomly add remaining questions */
		
		foreach($que as $qLevel => $arr)
		{
			foreach($arr as $marks => $no_of_question)
			{
			if($marks!='')
			{
				$condition = array();
				$condition[] = "(question_level = '$qLevel')";
				$condition[] = "(marks = '$marks')";
				$condition[] = "(subject_id='$subject_id')";
				
				if($cust_que_set_condition)
				$condition[] = $cust_que_set_condition;
			
				$condition = implode(' AND ', $condition);
				
				$questionDetails = $DB->SelectRecords('question', $condition, '*, id as question_id', " ORDER BY RAND() LIMIT $no_of_question ");
				
				$this->setSessionData($questionDetails, $subject_id, $currectTestId);
			}
			}
		}
	}
	
	/**
	 * 	@author :  Akshay Yadav
	 * 	@desc	: set session to resume test
	 */
	
	function setSessionToResumeTest($candidate_id, $exam_id, $currectTestId)
	{
		global $DB;
		// get answer from temp table
		
		$temp_data = $DB->SelectRecords('candidate_temp_answer', "(candidate_id=$candidate_id) AND (exam_id=$exam_id) AND (test_id=$currectTestId)");
		
		foreach($temp_data as $temp)
		{
			// if question is match type
			if($temp->is_match_type)
			{
			
				$match_data = $DB->SelectRecords('candidate_temp_match_answer', "(candidate_id=$candidate_id) AND (exam_id=$exam_id) AND (test_id=$currectTestId) AND (question_id=$temp->question_id)");
				if($match_data)
				foreach($match_data as $match)
				{
					$_SESSION['question-'.$temp->question_id]['given_answer'][$match->question_match_left_id] = $match->given_answer_id;
					if($match->given_answer_id)
						$_SESSION['attempted_questions'][$temp->question_id] = $temp->question_id;
				}
			}
			else
			{
				$_SESSION['question-'.$temp->question_id]['given_answer'] = $temp->given_answer_id;
				
				// if more than one answers are allowed
				$question_answer=$DB->SelectRecords('question_answer','question_id='.$temp->question_id);
				if(count($question_answer) > 1)
				{
					$_SESSION['question-'.$temp->question_id]['given_answer'] = explode(',',$temp->given_answer_id);
				}

				if($temp->given_answer_id)
					$_SESSION['attempted_questions'][$temp->question_id] = $temp->question_id;
			}
			
			$_SESSION['viewed_questions'][$temp->question_id] = $temp->question_id;
			$_SESSION['question-'.$temp->question_id]['subject_id'] = $temp->subject_id;
			$_SESSION['question-'.$temp->question_id]['solve_time'] = $temp->solve_time;
			if($temp->is_review_marked)
				$_SESSION['review'][$temp->question_id] = $temp->question_id;
		}
	}
	
	/**
	 * 	@author :  Akshay Yadav
	 * 	@desc	: Save answer submitted by candidate
	 */
	function saveAnswer()
	{
		if(!isset($_POST['display_quest']) || ($_POST['display_quest'] == ''))
			return false;
		
		global $DB;
		$tempdata = array();
		$ques_id = $_POST['ques_id'];
		$last_disp_quest = $_POST['display_quest'];
		$is_empty = true;
		
		$solve_time = time() - $_SESSION['solve_time'];
		$_SESSION['solve_time'] = time();
		
		if(!isset($_SESSION['question-'.$ques_id]) || ($_SESSION['question-'.$ques_id]['solve_time'] == ''))
			$_SESSION['question-'.$ques_id]['solve_time'] = 0;
		$_SESSION['question-'.$ques_id]['solve_time'] += $solve_time;

		if($_POST['ques_type'] == "MT")
		{
			$left_ids = $_POST['left_ids'];
			$_SESSION['question-'.$ques_id]['given_answer'] = array();
			
			$match_data = array();
			$countMatchQues = 0;
			foreach($left_ids as $left_id)
			{
				$match_data[$countMatchQues]['question_match_left_id'] = $left_id = (int) $left_id;
				$match_data[$countMatchQues++]['given_answer_id'] = $_SESSION['question-'.$ques_id]['given_answer'][$left_id] = $_POST[$left_id];
				
				if($_POST[$left_id] != '')
					$is_empty = false;
			}
			
			$tempdata['is_match_type'] = 1;
			$tempdata['given_answer_id'] = 0;
		}
		else
		{
			if(!isset($_POST['option'])) $_POST['option'] = '';
			$g_id =	$_SESSION['question-'.$ques_id]['given_answer'] = $_POST['option'];// candidates choosed answer
			$tempdata['is_match_type'] = '';
			
			if($_POST['option'] != '')
				$is_empty = false;
		
			if(is_array($g_id))
			{
				$g_id = implode(',',$g_id);
			}
			else
			{
				$g_id = addslashes($g_id);
			}
			$tempdata['given_answer_id'] = $g_id;
		}
		
		$tempdata['subject_id'] = $s_id = $_SESSION['question-'.$ques_id]['subject_id']=$_POST['question_subject_id']; //===question subject id
		$tempdata['test_id'] = $t_id = $_SESSION['testid'];
		$tempdata['exam_id'] = $e_id = $_SESSION['exam_id'];
		$tempdata['candidate_id'] = $c_id = $_SESSION['candidate_id'];
		$tempdata['question_id'] = $q_id = $ques_id;
	 	$tempdata['time_remaining'] = $time_remaining = strtotime($_SESSION['date']) - time();
		$tempdata['is_review_marked'] = '';
	 	$tempdata['solve_time'] = $_SESSION['question-'.$ques_id]['solve_time'];
		
		if(in_array($ques_id, $_SESSION['review']) || isset($_POST['mark_review']))
		{
			$tempdata['is_review_marked'] = 1;
		}
		if(isset($_POST['unmark_review']))
		{
			$tempdata['is_review_marked'] = '';
		}
	
	 	if($q_id)
	 	{
	 		$DB->DeleteRecord('candidate_temp_answer', "(candidate_id=$c_id) AND (question_id=$q_id) AND (test_id=$t_id)");
		 	$DB->InsertRecord('candidate_temp_answer', $tempdata);
	 	}
	 	
	 	if($tempdata['is_match_type'] == 1)
	 	{
	 		$DB->DeleteRecord('candidate_temp_match_answer', "(candidate_id=$c_id) AND (question_id=$q_id) AND (test_id=$t_id)");
	 		foreach($match_data as $match)
	 		{
	 			$tempdata['question_match_left_id'] = $match['question_match_left_id'];
	 			$tempdata['given_answer_id'] = $match['given_answer_id'];
	 			$DB->InsertRecord('candidate_temp_match_answer', $tempdata);
	 		}
	 	}
		
		$_SESSION['attempted_questions'][$ques_id] = $ques_id;
		
		$_SESSION['unattempted_questions'][$ques_id] = $ques_id;
		if($is_empty)
			unset($_SESSION['attempted_questions'][$ques_id]);
			else
				unset($_SESSION['unattempted_questions'][$ques_id]);
	}
	
	/**
	 * 	@author : Ashwini Agarwal
	 * 	@desc	: Preset session variables
	 */
	function presetSession()
	{
		global $DB;
		
		$toSave = array('candidate_id', 'candidate_fname', 'candidate_lname', 
						'testid', 'exam_id', 'total_marks', 'test_history', 'test_name',
						'total_question', 'maximum_marks', 'test_time_duration', 'is_resumed', 'time_takenfortest');
		
		$temp = array();
		foreach($toSave as $save)
		{
			$temp[$save] = $_SESSION[$save];
		}
		
		$_SESSION = array();
		$_SESSION = $temp;
		$_SESSION['attempted_questions'] = array();
		$_SESSION['viewed_questions'] = array();
		$_SESSION['last_unmarked_question'] = '';
		$_SESSION['last_marked_question'] = '';
		$_SESSION['review'] = array();
	}
	
	function getSubjectNameBySubjectId($subject_id)
	{
		global $DB,$frmdata;
		
		$subject_info = $DB->SelectRecord('subject', "id=$subject_id and id!=-1", 'subject_name');
		
		return $subject_info->subject_name;
	}
	
	function initialiseTempTable()
	{
		global $DB;
		
		$tempdata['test_id'] = $t_id = $_SESSION['testid'];
		$tempdata['exam_id'] = $_SESSION['exam_id'];
		$tempdata['candidate_id']= $c_id = $_SESSION['candidate_id'];
		$tempdata['question_id'] = $_SESSION['test_id-'.$_SESSION['testid']]['question_id-0'];
	 	$tempdata['time_remaining'] = strtotime($_SESSION['date']) - time();
	 	$tempdata['solve_time'] = 0;
	 	
	 	if(!$DB->SelectRecord('candidate_temp_answer', "(candidate_id=$c_id) AND (test_id=$t_id)"))
	 	{
			$DB->InsertRecord('candidate_temp_answer', $tempdata);
	 	}
	}
	
	function saveTime()
	{
		global $DB;
		
		$t_id = $_SESSION['testid'];
		$c_id = $_SESSION['candidate_id'];
		
		$tempdata['time_remaining'] = strtotime($_SESSION['date']) - time();
		$DB->UpdateRecord('candidate_temp_answer', $tempdata, "(candidate_id=$c_id) AND (test_id=$t_id)");
	}
}
?>