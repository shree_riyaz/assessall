 <?php 
/**
 * 	@author : Ashwini Agarwal
 */

class Result
{

	/**
	 * 	Get Test Questions
	 */
	function getTestQuestions()
	{
		global $DB,$DbHelper;
		$subject_condidtion = '';
		
		/*
		$candidate_test_subject_history = $DbHelper->getCandidateTestSubjectHistory($_SESSION['candidate_id'], $_SESSION['exam_id']);
		$subject_condidtion = $DbHelper->getCandidateFailSubjectCondidtion($candidate_test_subject_history);
		
		if($subject_condidtion)
		{
			$qMedia = $_SESSION['questionMedia'];
			$qTable = ($qMedia == 'paper') ? 'paper_questions' : 'test_questions'; 
			$_SESSION['subject_condition'] = str_replace('subject_id', $qTable.'.subject_id', $subject_condidtion);
		}
		*/
		
		if($_SESSION['questionMedia'] == 'paper')
		{
			$paperid = $_SESSION['testPaperId'];
			$testquesDetails = $DB->SelectRecords('paper_questions', "paper_id=".$paperid.$subject_condidtion, '*', 'order by id');
		}
		else
		{
			$testquesDetails = $DB->SelectRecords('test_questions', "test_id=".$_SESSION['testid'].$subject_condidtion, '*', 'order by id');
		}
		
		return $testquesDetails;
	}

	/**
	 * 	Add question history fo match type question
	 */
	function createMatchTypeQuestionHistory($question)
	{
		global $DB;
		$ques_id = $question->id;
		
		$match_data = array();
		$match_data['candidate_id'] = $_SESSION['candidate_id'];
		$match_data['question_id'] = $ques_id;
		$match_data['test_id'] = $_SESSION['testid'];
		$match_data['exam_id'] = $_SESSION['exam_id'];
		$match_data['subject_id'] = $question->subject_id;
			
		$given_answer = $_SESSION['question-'.$ques_id]['given_answer'];
		
		$blank = 1;
		if(is_array($given_answer))
		{
			$correct = 1;
			foreach($given_answer as $l => $r)
			{
				$match_data['question_match_left_id'] = $l;
				if($r != '')
				{
					$blank = 0;
					$match_data['given_answer_id'] = $r;
					
					$lcol = $DB->SelectRecord('question_match_left',"id='$l'");
					$answer_id = $lcol->answer_id;
					
					$match_data['correct_question_match_right_id'] = $answer_id;

					$match_data['is_answer_correct'] = 'Y';
					if($answer_id != $r)
					{
						$correct = 0;
						$match_data['is_answer_correct'] = 'N';
					}
				}
				else
				{
					$correct = 0;
					$match_data['given_answer_id'] = 0;
				}
				
				$DB->InsertRecord('candidate_match_question_history',$match_data);
			}
		}
		else
		{
			$left_cols = $DB->SelectRecords('question_match_left', "question_id='$ques_id'");
			foreach($left_cols as $lcol)
			{
				$match_data['given_answer_id'] = 0;
				$match_data['is_correct_answer'] = '';
				$match_data['question_match_left_id'] = $lcol->id;
				$match_data['correct_question_match_right_id'] = $lcol->answer_id;
				
				$DB->InsertRecord('candidate_match_question_history',$match_data);
			}
		}
		
		$return = array('question_id' => $ques_id);
		if($blank == 1)
		{
			$return['no_skipped'] = 1;
		}
		elseif ($correct == 1)
		{
			$return['is_answer_correct'] = 'Y';
			$return['no_correctans'] = 1;
			$return['marksObtained'] = $question->marks;
		}
		else
		{
			$return['is_answer_correct'] = 'N';
		}

		$return['given_answer_id'] = -1;
		$return['correct_answer_id'] = '';
		
		return $return;
	}
	
	/**
	 * 	Get question status
	 */
	function getQuestionStatus($question)
	{
		global $DB;
		$ques_id = $question->id;
		
		$given_answer = $_SESSION['question-'.$ques_id]['given_answer'];
		$correctansDetails = $DB->SelectRecords('question_answer', "question_id=$ques_id");
		
		$correct_ans = $correctansDetails[0]->answer_id;
		if(count($correctansDetails) > 1)
		{
			$correct_ans = array();
			foreach($correctansDetails as $cans)
			$correct_ans[] = $cans->answer_id;
		}
		
		$return = array('question_id' => $ques_id);
		if($given_answer == '')
		{
			$return['no_skipped'] = 1;
		}
		elseif($given_answer == $correct_ans)
		{
			$return['is_answer_correct'] = 'Y';
			$return['no_correctans'] = 1;
			$return['marksObtained'] = $question->marks;
		}
		else
		{
			$return['is_answer_correct'] = 'N';
		}

		$return['given_answer_id'] = is_array($given_answer) ? implode(',', $given_answer) : $given_answer;
		$return['correct_answer_id'] = is_array($correct_ans) ? implode(',',$correct_ans) : $correct_ans;
		
		return $return;
	}
	
	/**
	 * 	
	 */
	function getCandidatePercentageMarks($subject_id, $subject_marks)
	{
		global $DB;
		
		if($_SESSION['questionMedia'] == 'paper')
		{
			$test_subject = $DB->SelectRecord('paper_subject'," paper_id=".$_SESSION['testPaperId']." and subject_id=".$subject_id);
		}
		else
		{
			$test_subject = $DB->SelectRecord('test_subject',"exam_id=".$_SESSION['exam_id']." and test_id=".$_SESSION['testid']." and subject_id=".$subject_id);
		}
	
		$candidate_subject_marks_percentage = (($subject_marks / $test_subject->subject_total_marks)*100);
		$candidate_subject_marks_percentage = number_format($candidate_subject_marks_percentage, 2, '.', '');
		
		return $candidate_subject_marks_percentage;
	}
	
	/**
	 * 
	 */
	function getSubjectMinimumMarks($subject_id)
	{
		global $DB;
		$exam_subject = $DB->SelectRecord('exam_subjects', "exam_id='".$_SESSION['exam_id']."' and subject_id='$subject_id'");
		return $exam_subject->subject_min_mark;
	}
	
	/**
	 * 
	 */
	function deleteTempData()
	{
		global $DB;
		$DB->DeleteRecord('candidate_temp_answer', "(candidate_id='".$_SESSION['candidate_id']."') AND (test_id='".$_SESSION['testid']."')");
		$DB->DeleteRecord('candidate_temp_match_answer', "(candidate_id='".$_SESSION['candidate_id']."') AND (test_id='".$_SESSION['testid']."')");
	}
	
	/**
	 * 	Unset and restore session.
	 */
	function unsetAndRestoreSession()
	{
		global $DB;
		$cand_id = $_SESSION['candidate_id'];
		$test_id = $_SESSION['testid'];
		
		unset($_SESSION);
		session_unset();
		session_destroy();
		
		session_start();
		$user = $DB->SelectRecord('candidate',"id='$cand_id'");
		$_SESSION['candidate_id'] = $cand_id;
		$_SESSION['user_name'] = $user->user_name;
		$_SESSION['candidate_fname'] = $user->first_name;
		$_SESSION['candidate_lname'] = $user->last_name;
		$_SESSION['lastTestId'] = $test_id;
	}
	
	/**
	 * 	Send result to candidate's email address.
	 */
	function mailCandidate($candidateId, $testid, $result, $resultDetail)
	{
		global $DB;
		$cand = $DB->SelectRecord('candidate', "id = '$candidateId'");	
	
		if($cand->email != '')
		{
			$test = $DB->SelectRecord('test', "id = '$testid'");
			
			$message = 'Hello '.ucwords($cand->first_name.' '.$cand->last_name).'<br>';
			$message .= '<p>You have appeared in '. ucwords($test->test_name) .' Test And ';
			if($result == 'F')
			{
				$message .= 'we are sorry to inform you that you have failed this test.';
			}
			elseif($result == 'P')
			{
				$message .= 'we are happy to inform you that you have passed this test.';
			}
			
			$message .= '</p><br><b><u>Result Details</u></b><br>';
			$message .= $resultDetail;
			$message .= '<br><br>Regards';
			$message .= '<br>Recruitment Examination Team';
			echo $message;exit;
			include_once(ADMINROOT.'/lib/mailer.php');
			$mail = new mailer();
			$mail->addTo($cand->email, ucwords($cand->first_name.' '.$cand->last_name));
			$mail->setSubject('Result - Recruitment Examination');
			$mail->setMessage($message);
			$mail->send();
		}
	}
	
	function countCandidateSubjectMarks($exam_id, $test_id, $candidate_id)
	{
		global $DB,$frmdata;
		
	  $query ="SELECT sub.subject_name, cqh.subject_id, cqh.is_answer_correct, cqh.correct_answer_id, if( test.negative_marking = 'Y', SUM( if( cqh.is_answer_correct = 'Y', q.marks, 0 ) ) - SUM( if( cqh.is_answer_correct = 'N', q.marks/4, 0 ) ) , SUM( if( cqh.is_answer_correct = 'Y', q.marks, 0 ) ) ) AS subject_marks
FROM candidate_question_history AS cqh
LEFT JOIN test AS test ON test.id = cqh.test_id
LEFT JOIN question AS q ON q.id = cqh.question_id
LEFT JOIN subject AS sub ON cqh.subject_id = sub.id
where cqh.test_id=$test_id and cqh.exam_id=$exam_id and cqh.candidate_id=$candidate_id group by cqh.subject_id";
	
	$result=$DB->RunSelectQuery($query);
		
		return $result;
	}
}
?>