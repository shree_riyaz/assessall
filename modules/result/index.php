<?php 
defined("ACCESS") or die("Access Restricted");
global $DB;

include_once('class.result.php');
$resultOBJ = new Result();

$commonArray = array();
$commonArray['candidate_id'] = $candidate_id = $_SESSION['candidate_id'];
$commonArray['exam_id'] = $exam_id = $_SESSION['exam_id'];
$commonArray['test_id'] = $testid = $_SESSION['testid'];

if($testid == '')
{
	Redirect(CreateURL('index.php',"mod=guidelines"));
	exit;
}

$totalnoQuestion = count($resultOBJ->getTestQuestions());
$no_questions = $totalnoQuestion;
$no_skipped = 0;
$no_correctans = 0;
$totalmarks = 0;
$marksObtained = 0;
$candi_id = $_SESSION['candidate_id'];
/*
$is_attempted['is_attempted'] = 1;
 $DB->UpdateRecord('test_candidate', $is_attempted, 'candidate_id='.$candi_id.' and test_id='. $_SESSION['testid']);*/
 $instance = $DB->SelectRecords('candidate_question_history','exam_id='.$exam_id.' and test_id='.$testid.' and candidate_id='.$candidate_id, 'test_instance','order by id DESC limit 1');
	if($instance[0]->test_instance && $instance[0]->test_instance!='')
	$test_instance = $instance[0]->test_instance+1;
	else
	$test_instance = 1;
foreach($_SESSION['test_id-'.$testid] as $key => $ques_id)
{
	$question = $DB->SelectRecord('question', "id='$ques_id'");
	$totalmarks = $totalmarks + $question->marks;
	
	if($question->question_type == "MT")
	{
		$status = $resultOBJ->createMatchTypeQuestionHistory($question);
	}
	elseif ($question->question_type == "S")
	{
		$status = array();
		$status['question_id'] = $ques_id;
		$status['given_answer'] = addslashes($_SESSION['question-'.$ques_id]['given_answer']);
		$status['given_answer_id'] = '';
		$status['correct_answer_id'] = '';
	}
	else
	{
		$status = $resultOBJ->getQuestionStatus($question);
	}
	
	if(isset($status['no_skipped'])) $no_skipped += $status['no_skipped'];
	if(isset($status['no_correctans'])) $no_correctans += $status['no_correctans'];
	if(isset($status['marksObtained'])) $marksObtained += $status['marksObtained'];
	
	$frmdata = $commonArray + $status;
	$frmdata['subject_id'] = $question->subject_id;
	$frmdata['solve_time'] = $_SESSION['question-'.$ques_id]['solve_time'];
	$frmdata['test_instance'] = $test_instance;
	$DB->InsertRecord('candidate_question_history', $frmdata);
	
}

$no_wrongans = $no_questions - $no_skipped - $no_correctans;
$marksObtained = (!$marksObtained) ? 0 : (int) $marksObtained;
$percentage = number_format((($marksObtained * 100) / $totalmarks), 2, '.', '');

//===========calculate candidate marks according to subject============================
$candidate_subject_marks = $resultOBJ->countCandidateSubjectMarks($exam_id, $testid, $candidate_id);
$test_subject_result = array();
$test_result = '';
$message = '';


for($counter = 0; $counter < count($candidate_subject_marks); $counter++)
{
	$subject_id = $candidate_subject_marks[$counter]->subject_id;
	$instance = $DB->SelectRecords('candidate_test_subject_history','exam_id='.$exam_id.' and test_id='.$testid.' and candidate_id='.$candidate_id.' and subject_id='.$subject_id, 'test_instance','order by id DESC limit 1');
	if($instance[0]->test_instance && $instance[0]->test_instance!='')
	$test_instance = $instance[0]->test_instance+1;
	else
	$test_instance = 1;
	$subject_marks = $candidate_subject_marks[$counter]->subject_marks;
	
	$test_subject_history_data = $commonArray;
	$test_subject_history_data['is_outer_candidate'] = 'N';
	$test_subject_history_data['practical'] = 'N';
	$test_subject_history_data['subject_id'] = $subject_id;
	$test_subject_history_data['marks_obtained'] = $subject_marks;
	
	$min_marks = $resultOBJ->getSubjectMinimumMarks($subject_id, $subject_marks);
	$candidate_subject_marks_percentage = $resultOBJ->getCandidatePercentageMarks($subject_id, $subject_marks);
	$test_subject_result[$counter]['subject_percentage'] = $candidate_subject_marks_percentage;
	$test_subject_result[$counter]['subject_name'] = $candidate_subject_marks[$counter]->subject_name;
	
	$message .= '<br>';
	$message .= 'Subject - '.$candidate_subject_marks[$counter]->subject_name.'<br>';
	$message .= 'Minimum Passing Marks - '.$min_marks.'%<br>';
	$message .= 'Obtained Marks - '.$candidate_subject_marks_percentage.'%<br>';
	
	if($candidate_subject_marks_percentage < $min_marks)
	{
		$test_subject_history_data['result'] = 'F';
		$test_subject_result[$counter]['result'] = 'Fail';
		$test_result='F';
		$message .= 'Result - Fail<br>';
	}
	else 
	{
		$test_subject_history_data['result'] = 'P';
		$test_subject_result[$counter]['result'] = 'Pass';
		$message .= 'Result - Pass<br>';
	}
	$test_subject_history_data['test_instance'] = $test_instance;
	$DB->InsertRecord('candidate_test_subject_history', $test_subject_history_data);
	$candidate_data['is_exam_detail_added'] = 'Y';
	$DB->UpdateRecord('candidate', $candidate_data, 'id='.$candidate_id);
}

//exit;

//=================calculate candidate test marks===================================

$result = ($test_result == 'F') ? 'F' : 'P';

$testhistorydata = $commonArray;
$testhistorydata['marks_obtained'] = $marksObtained.'.0';
$testhistorydata['percentage'] = $percentage;
$testhistorydata['time_taken'] = $_SESSION['time_takenfortest'];
$testhistorydata['result'] = $result;
$instance_test = $DB->SelectRecords('candidate_test_history','exam_id='.$exam_id.' and test_id='.$testid.' and candidate_id='.$candidate_id,'test_instance','order by id DESC limit 1');
	if($instance_test[0]->test_instance && $instance_test[0]->test_instance!='')
	$instance_test = $instance_test[0]->test_instance+1;
	else
	$instance_test = 1;
$testhistorydata['test_instance'] = $instance_test ;
$DB->InsertRecord('candidate_test_history', $testhistorydata);

$resultOBJ->deleteTempData();
//$resultOBJ->mailCandidate($candidate_id, $testid, $result, $message);
//$resultOBJ->unsetAndRestoreSession();

//$href = CreateURL('index.php',"mod=feedback");
/*echo "<script>window.opener.window.location.reload();window.location.href='$href';</script>";*/
//exit;



$CFG->template="result/result.php";
include(TEMP."/".$CFG->template);
$resultOBJ->unsetAndRestoreSession();
echo "<script>window.opener.window.location.href='".CreateURL('index.php','mod=user')."';</script>";
/*echo "<script>window.opener.window.location.href='"+<?php echo CreateURL('index.php','mod=user')?>+"';</script>";*/
exit;
?>