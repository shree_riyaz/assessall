<?php 
/**
 * 	@author : Ashwini Agarwal
 */

class SaveHomework
{
	/**
	 * 	Add question history fo match type question
	 */
	function createMatchTypeQuestionHistory($question)
	{
		global $DB;
		$ques_id = $question->id;
		
		$match_data = array();
		$match_data['candidate_id'] = $_SESSION['candidate_id'];
		$match_data['question_id'] = $ques_id;
		$match_data['homework_id'] = $_SESSION['homework_id'];
		$match_data['exam_id'] = $_SESSION['homework']['exam_id'];
		$match_data['subject_id'] = $question->subject_id;
			
		$given_answer = $_SESSION['homework']['question-'.$ques_id]['given_answer'];
		
		$blank = 1;
		if(is_array($given_answer))
		{
			$correct = 1;
			foreach($given_answer as $l => $r)
			{
				$match_data['question_match_left_id'] = $l;
				if($r != '')
				{
					$blank = 0;
					$match_data['given_answer_id'] = $r;
					
					$lcol = $DB->SelectRecord('question_match_left',"id='$l'");
					$answer_id = $lcol->answer_id;
					
					$match_data['correct_question_match_right_id'] = $answer_id;

					$match_data['is_answer_correct'] = 'Y';
					if($answer_id != $r)
					{
						$correct = 0;
						$match_data['is_answer_correct'] = 'N';
					}
				}
				else
				{
					$correct = 0;
					$match_data['given_answer_id'] = 0;
				}
				
				$DB->InsertRecord('candidate_homework_match_question_history',$match_data);
			}
		}
		else
		{
			$left_cols = $DB->SelectRecords('question_match_left', "question_id='$ques_id'");
			foreach($left_cols as $lcol)
			{
				$match_data['given_answer_id'] = 0;
				$match_data['is_correct_answer'] = '';
				$match_data['question_match_left_id'] = $lcol->id;
				$match_data['correct_question_match_right_id'] = $lcol->answer_id;
				
				$DB->InsertRecord('candidate_homework_match_question_history',$match_data);
			}
		}
		
		$return = array('question_id' => $ques_id);
		if($blank == 1)
		{
			$return['no_skipped'] = 1;
		}
		elseif ($correct == 1)
		{
			$return['is_answer_correct'] = 'Y';
			$return['no_correctans'] = 1;
			$return['marksObtained'] = $question->marks;
		}
		else
		{
			$return['is_answer_correct'] = 'N';
		}

		$return['given_answer_id'] = -1;
		$return['correct_answer_id'] = '';
		
		return $return;
	}
	
	/**
	 * 	Get question status
	 */
	function getQuestionStatus($question)
	{
		global $DB;
		$ques_id = $question->id;
		
		$given_answer = '';
		if(isset($_SESSION['homework']['question-'.$ques_id]))
		{
			$given_answer = $_SESSION['homework']['question-'.$ques_id]['given_answer'];
		}
		$correctansDetails = $DB->SelectRecords('question_answer', "question_id=$ques_id");
		
		$correct_ans = $correctansDetails[0]->answer_id;
		if(count($correctansDetails) > 1)
		{
			$correct_ans = array();
			foreach($correctansDetails as $cans)
			$correct_ans[] = $cans->answer_id;
		}
		
		$return = array('question_id' => $ques_id);
		if($given_answer == '')
		{
			$return['no_skipped'] = 1;
		}
		elseif($given_answer == $correct_ans)
		{
			$return['is_answer_correct'] = 'Y';
			$return['no_correctans'] = 1;
			$return['marksObtained'] = $question->marks;
		}
		else
		{
			$return['is_answer_correct'] = 'N';
		}

		$return['given_answer_id'] = is_array($given_answer) ? implode(',', $given_answer) : $given_answer;
		$return['correct_answer_id'] = is_array($correct_ans) ? implode(',',$correct_ans) : $correct_ans;
		
		return $return;
	}
	
	/**
	 * 	
	 */
	function deleteTempData()
	{
		global $DB;
		$DB->DeleteRecord('candidate_temp_answer_homework', "(candidate_id='".$_SESSION['candidate_id']."') AND (homework_id='".$_SESSION['homework_id']."')");
		$DB->DeleteRecord('candidate_temp_match_answer_homework', "(candidate_id='".$_SESSION['candidate_id']."') AND (homework_id='".$_SESSION['homework_id']."')");
	}
	
	/**
	 * 	Unset and restore session.
	 */
	function unsetAndRestoreSession()
	{
		global $DB;
		$cand_id = $_SESSION['candidate_id'];
		
		unset($_SESSION);
		session_unset();
		session_destroy();
		
		session_start();
		$user = $DB->SelectRecord('candidate',"id='$cand_id'");
		$_SESSION['candidate_id'] = $cand_id;
		$_SESSION['user_name'] = $user->candidate_id;
		$_SESSION['candidate_fname'] = $user->first_name;
		$_SESSION['candidate_lname'] = $user->last_name;
	}

	function countCandidateSubjectMarks($exam_id, $homework_id, $candidate_id)
	{
		global $DB,$frmdata;
		
		$query ="select sub.subject_name, cqh.subject_id, cqh.is_answer_correct, cqh.correct_answer_id, SUM(if(cqh.is_answer_correct='Y', q.marks, 0)) as subject_marks 
					from candidate_homework_question_history as cqh
					left join question as q on q.id=cqh.question_id
					left join subject as sub on cqh.subject_id=sub.id
					where cqh.homework_id=$homework_id and cqh.exam_id=$exam_id and cqh.candidate_id=$candidate_id
					group by cqh.subject_id";
		
		$result=$DB->RunSelectQuery($query);
									
		return $result;
	}
}
?>