<?php 
defined("ACCESS") or die("Access Restricted");
global $DB;

include_once('class.save_homework.php');
$saveHomeworkOBJ = new SaveHomework();

if(!isset($_SESSION['is_homework_begin']))
{
	Redirect(CreateURL('index.php',"mod=guidelines"));
	exit;
}

$session = $_SESSION['homework'];

$commonArray = array();
$commonArray['candidate_id'] = $candidate_id = $_SESSION['candidate_id'];
$commonArray['exam_id'] = $exam_id = $_SESSION['homework']['exam_id'];
$commonArray['homework_id'] = $homework_id = $_SESSION['homework_id'];

if($homework_id == '')
{
	Redirect(CreateURL('index.php',"mod=guidelines"));
	exit;
}

$totalmarks = 0;
$marksObtained = 0;

foreach($session['homework_id-'.$homework_id] as $key => $ques_id)
{
	$question = $DB->SelectRecord('question', "id='$ques_id'");
	$totalmarks = $totalmarks + $question->marks;
	
	if($question->question_type == "MT")
	{
		$status = $saveHomeworkOBJ->createMatchTypeQuestionHistory($question);
	}
	elseif ($question->question_type == "S")
	{
		$status = array();
		$status['question_id'] = $ques_id;
		$status['given_answer'] = addslashes($session['question-'.$ques_id]['given_answer']);
		$status['given_answer_id'] = '';
		$status['correct_answer_id'] = '';
	}
	else
	{
		$status = $saveHomeworkOBJ->getQuestionStatus($question);
	}
	
	if(isset($status['marksObtained'])) $marksObtained += $status['marksObtained'];
	
	$frmdata = $commonArray + $status;
	$frmdata['subject_id'] = $question->subject_id;
	$frmdata['solve_time'] = isset($session['question-'.$ques_id]) ? $session['question-'.$ques_id]['solve_time'] : 0;
	$DB->InsertRecord('candidate_homework_question_history', $frmdata);
}

$marksObtained = (!$marksObtained) ? 0 : (int) $marksObtained;
$percentage = number_format((($marksObtained * 100) / $totalmarks), 2, '.', '');

//===========calculate candidate marks according to subject============================
$candidate_subject_marks = $saveHomeworkOBJ->countCandidateSubjectMarks($exam_id, $homework_id, $candidate_id);
$homework_subject_result = array();

for($counter = 0; $counter < count($candidate_subject_marks); $counter++)
{
	$subject_id = $candidate_subject_marks[$counter]->subject_id;
	$subject_marks = $candidate_subject_marks[$counter]->subject_marks;
	
	$homework_subject_history_data = $commonArray;
	$homework_subject_history_data['practical'] = 'N';
	$homework_subject_history_data['subject_id'] = $subject_id;
	$homework_subject_history_data['marks_obtained'] = $subject_marks;
	
	$DB->InsertRecord('candidate_homework_subject_history', $homework_subject_history_data);
}

//exit;

//=================calculate candidate homework marks===================================

$homeworkhistorydata = $commonArray;
$homeworkhistorydata['marks_obtained'] = $marksObtained.'.0';
$homeworkhistorydata['time_taken'] = time() - $session['start_time'];

$DB->InsertRecord('candidate_homework_history', $homeworkhistorydata);

$saveHomeworkOBJ->deleteTempData();
//$saveHomeworkOBJ->mailCandidate($candidate_id, $homework_id, $message);
$saveHomeworkOBJ->unsetAndRestoreSession();

$href = CreateURL('index.php');
echo "<script>window.location.href='$href';</script>";
exit;
?>