<?php 
/*****************Developed by :- Akshay Yadav
					Module       :- Rank master
					Purpose      :- Entry file for any action, call in rank master module
***********************************************************************************/

defined("ACCESS") or die("Access Restricted");
include_once("class.report.php");
$ReportOBJ = new Report();

$exportEnabled = true;
//$auth = new Authorise();
//if (($auth->isAuthorisedAction($_SESSION['admin_user_id'], 'report', 'download') == false) && ($_SESSION['admin_user_name'] != 'admin'))
//{
//	$exportEnabled = false;
//}

if(isset($frmdata['exam_id']))
{
	$exam_id=$frmdata['exam_id'];
}
elseif (isset($getVars['exam_id']))
{
	$exam_id=$getVars['exam_id'];
}
else 
{
	$exam_id='';
}

if(isset($frmdata['test_id']))
{
	$test_id=$frmdata['test_id'];
}
elseif(isset($getVars['test_id']))
{
	$test_id=$getVars['test_id'];
}
else
{
	$test_id='';
}

if(isset($getVars['return']))
{
	$return=$getVars['return'];
}
else
{
	$return='';
}

if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}
$test_instance = $getVars['ins'];
switch($do)
{
	default:
	case 'result_sheet':
		PaginationWork();
		$exam=getConductedExamInfo();
		$testyearlistStart = $DB->SelectRecords('test',"test_date<=now()", 'year(test_date) as year', 'group by year(test_date)');
		$testyearlistEnd = $DB->SelectRecords('test',"test_date<=now()", 'year(test_date_end) as year', 'group by year(test_date_end)');
		$testyearlist = array();
		foreach($testyearlistStart as $year)
		{
			$testyearlist[$year->year] = $year->year;
		}
			
		foreach($testyearlistEnd as $year)
		{
			$testyearlist[$year->year] = $year->year;
		}
			
		sort($testyearlist, SORT_NUMERIC);
			
		if($exam_id!='')
		{
			$test_detail = getTestDetailByExamId($totalCount, $exam_id);
			$showResult = true;
		}
		elseif($_SESSION['admin_user_type'] == 'P')
		{
			$test_detail = getTestDetailByExamId($totalCount, '');
			$showResult = true;
		}
		else
		{
			$test_detail = getTestDetail($totalCount);
			$showResult = true;
		}
		
	    $CFG->template="report/result_sheet.php";
		break; 
			
		case 'view_test_result':
			
			PaginationWork();
			$totalCount=0;
			unset($_SESSION['return_submit']);
			$exam_type='';
			
			$subject_result = getResultSheetReport($totalCount, $exam_type, $exam_id, $test_id,'',$test_instance);
			
			$exam_subject = getSubjectNameByExamId($exam_id, $test_id);
			$CFG->template="report/view_test_result.php";
			break;

		case 'candidate_detail':
			PaginationWork();
			$totalCount=0;
			$candidate = getCandidatePersonalData($totalCount);
			$CFG->template="report/candidate_detail.php";
			break;
			
		case 'candidate_performance':
			
			if(isset($_GET['getGraph']) && $_GET['getGraph'])
			{
				$year = $_GET['year'];
				$month = $_GET['month'];
				$test_id = $_GET['test'];
				$candidate_id = $_GET['candidate_id'];
				$type = $_GET['type'];
				$sub_type = isset($_GET['sub_type']) ? $_GET['sub_type'] : '';
				
				$ReportOBJ->getGraph($year,$month,$test_id, $candidate_id,$type,$sub_type);
				exit;
			}
			
	//		if(isset($frmdata['candidate_id']) && $frmdata['candidate_id'])
	//		{
				PaginationWork();
				$report = $ReportOBJ->getCandidatePerformanceReport($totalCount);
				// echo '<pre>';print_r($report);echo '</pre>';
	//		}
			
			$query="select cand.* from candidate cand where id=".$_SESSION['candidate_id'];
				
			$candidate = $DB->RunSelectQuery($query);
			
			$CFG->template="report/candidate_performance.php";
			break;
		
		case 'candidate_result_details_to_pdf':
			$CFG->template="report/candidate_result_details_to_pdf.php";
			$templateDone = true;
			
		case 'candidate_result_details':
			
			$candidate_id = $_GET['candidate_id'];
			$exam_id= $_GET['exam_id'];
			$test_id = $_GET['test_id'];
			$test_info = $DB->SelectRecord('test', "id=$test_id");
			$qMedia = $test_info->question_media;
			
			$test_paper = $DB->SelectRecord('test_paper','test_id='.$test_id);
			$paper_id = $test_paper->paper_id;
				
			$questionHistory = $DB->SelectRecords('candidate_question_history', "((exam_id='$exam_id') AND (test_id='$test_id') AND (candidate_id='$candidate_id') AND (test_instance='$test_instance'))");
			$testHistory = $DB->SelectRecord('candidate_test_history', "((exam_id='$exam_id') AND (test_id='$test_id') AND (candidate_id='$candidate_id') AND (test_instance='$test_instance'))");
			
			$candidateInfo = $DB->SelectRecord('candidate',"id = $candidate_id");
			$testInfo = $DB->SelectRecord('test',"id = $test_id");
			$examInfo = $DB->SelectRecord('examination', "id='$exam_id'");
			
			if($qMedia == 'subject')
			{
				$testQuestions = $DB->SelectRecords('test_questions',"test_id = '$test_id'");
			}
			elseif($qMedia == 'paper')
			{
				$testQuestions = $DB->SelectRecords('paper_questions',"paper_id = '$paper_id'");
			}
			
			$givenans = $DB->SelectRecords('candidate_question_history', "((exam_id='$exam_id') AND (test_id='$test_id') AND (candidate_id='$candidate_id') AND (test_instance='$test_instance'))" ,'is_answer_correct, given_answer_id' );
			
			$no_correctans = 0;
			$no_wrongans = 0;
			$no_skipped = 0;
			
			foreach($givenans as $ans)
			{
				if($ans->is_answer_correct == 'Y')
				{
					$no_correctans++;
				}
				elseif(($ans->is_answer_correct == 'N') && ($ans->given_answer_id != '')  && ($ans->given_answer_id != 0))
				{
					$no_wrongans++;
				}
				else
				{
					$no_skipped++;
				}			
			}
			
			
			$candidate_subject_marks = countCandidateSubjectMarks($exam_id, $test_id, $candidate_id,$test_instance);
			$test_subject_result = array();
			
			for($counter=0; $counter<count($candidate_subject_marks); $counter++)
			{
				$subject_id = $candidate_subject_marks[$counter]->subject_id;
				$subject_marks = $candidate_subject_marks[$counter]->subject_marks;
				
				if($qMedia == 'subject')
				{
					$test_subject = $DB->SelectRecord('test_subject',"exam_id=".$exam_id." and test_id=".$test_id." and subject_id=".$subject_id);
				}
				elseif($qMedia == 'paper')
				{
					$test_subject = $DB->SelectRecord('paper_subject',"paper_id = '$paper_id' and subject_id=".$subject_id);
				}
				
				$candidate_subject_marks_percentage = (($subject_marks/$test_subject->subject_total_marks)*100);
				$candidate_subject_marks_percentage = number_format($candidate_subject_marks_percentage, 2, '.', '');
			
				$exam_subject = $DB->SelectRecord('exam_subjects',"exam_id=".$exam_id." and subject_id=".$subject_id);
				$min_marks = $exam_subject->subject_min_mark;
				
				$test_subject_result[$counter]['subject_name'] = $candidate_subject_marks[$counter]->subject_name;
				$test_subject_result[$counter]['solve_time'] = $candidate_subject_marks[$counter]->solve_time;
				$test_subject_result[$counter]['subject_percentage'] = $candidate_subject_marks_percentage;
				$test_subject_result[$counter]['subject_min_marks'] = $exam_subject->subject_min_mark;
				$test_subject_result[$counter]['subject_max_marks'] = $exam_subject->subject_max_mark;
				
				if($candidate_subject_marks_percentage < $min_marks)
				{
					$test_subject_result[$counter]['result'] = 'Fail';
					$test_result='F';
				}
				else 
				{
					$test_subject_result[$counter]['result'] = 'Pass';
				}
			}
			
			if(!$templateDone)
			$CFG->template="report/candidate_result_details.php";
			break;
} 
	
//$CFG->template="report/add.php";
include(CURRENTTEMP."/index.php");
exit;
?>