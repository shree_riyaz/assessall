<?php 

class Guidelines
{
	/**
	 * 	@author	: Akshay Yadav
	 * 	@param	: Candidate id
	 * 	@desc	: get list of exams for candidate
	 */
	function getExamForCandidate($c_id)	{	
	global $DB;			
	$exam_candidate = $DB->SelectRecords('examination');	
	$exam_id_arr = array();		
	if($exam_candidate && (count($exam_candidate) > 0))	
	{	
	foreach($exam_candidate as $ec)		
	{
	$exam_id_arr[] = $ec->id;	
	}		
	}		
	return $exam_id_arr;	
	}
	
	/**
	 * 	@author	:  Akshay Yadav
	 * 	@param	: Candidate id
	 * 	@desc	: get list of incomplete test for resume.
	 */
	function getIncompleteTest($c_id)
	{
		global $DB;
		
		$temp_test_arr = array();
		$check_temp_data = $DB->SelectRecords('candidate_temp_answer', "(candidate_id=$c_id) and test_id =".$_GET['e'], 'test_id', 'GROUP BY test_id');
	
		if(is_array($check_temp_data) && (count($check_temp_data) > 0))
		{
			foreach($check_temp_data as $temp)
			{
				// if test is active
				if($this->isActiveTest($temp->test_id))
				{
					// check if test is completed
					$testGiven = $DB->SelectRecords('candidate_test_history', "candidate_id='$c_id' and test_id='$temp->test_id'", 'id');
					if($testGiven == '')
					{
						$temp_test_arr[] = $temp->test_id;
					}
				}
			}
		}
		return $temp_test_arr;
	}
	
	/**
	 * 	@author	: Akshay Yadav
	 * 	@param	: Candidate id
	 * 	@desc	: get list of incomplete homework for resume.
	 */
	function getIncompleteHomework($c_id)
	{
		global $DB;
		
		$temp_homework_arr = array();
		$check_temp_data = $DB->SelectRecords('candidate_temp_answer_homework', "(candidate_id=$c_id)", 'homework_id', 'GROUP BY homework_id');
	
		if(is_array($check_temp_data) && (count($check_temp_data) > 0))
		{
			foreach($check_temp_data as $temp)
			{
				// if homework is active
				if($this->isActiveHomework($temp->homework_id))
				{
					// check if homework is completed
					$homeworkGiven = $DB->SelectRecords('candidate_homework_history', "candidate_id='$c_id' and homework_id='$temp->homework_id'", 'id');
					if($homeworkGiven == '')
					{
						$temp_homework_arr[] = $temp->homework_id;
					}
				}
			}
		}
		
		$_SESSION['incompleteHomeworkIds'] = $temp_homework_arr;
		return $temp_homework_arr;
	}
	
	/**
	 * 	@author	:  Akshay Yadav
	 * 	@param	: Candidate id
	 * 	@desc	: get test to be conducted
	 */
	function setTestToBeConductedSession($exam_id_arr, $temp_test_arr)
	{
		global $DB;
	$candidate_id = $_SESSION['candidate_id'];
		
		// get exam condition
		$exam_condition = '';
		if($exam_id_arr && count($exam_id_arr) > 0)
		{
			$exam_id_set = implode(',', $exam_id_arr);
			$exam_condition = " and exam_id in($exam_id_set) ";
		}
		else
		{
			return false;
		}
		
		// get conserning teacher IDs
		$teacher_condition = ' AND ((teacher_id = "") OR (teacher_id IS NULL)) ';
		$teacher = $DB->SelectRecord('exam_candidate_teacher', "candidate_id=$candidate_id $exam_condition", 'GROUP_CONCAT(DISTINCT teacher_id) as teacher_id', 'GROUP BY candidate_id');
		if($teacher && $teacher->teacher_id)
		{	
			$teacher_condition = " AND ((teacher_id = '') OR (teacher_id IS NULL) OR (teacher_id IN ($teacher->teacher_id))) ";
		}
		
		// get test ids for candidates
		$test_condition = ' AND (id = -1) ';
		//$tests = $DB->SelectRecord('test_candidate', "candidate_id=$candidate_id and test_id=".$_GET['e'], 'GROUP_CONCAT(DISTINCT test_id) as test_id', 'GROUP BY candidate_id');		
		$tests = $DB->SelectRecord('test',"id=".$_GET['e']);
		if($tests && $tests->id)
		{
			
			$test_condition = " AND (id IN ($tests->id)) ";
		}
		
		// get incomplete test condition
		$temp_test_condition = '';
		if(count($temp_test_arr) > 0)
		{
			$temp_test = implode(',', $temp_test_arr);
			$temp_test_condition = " AND (id IN ($temp_test)) ";
		}
		
		$failedTestCondition = '';
		$testhistory = $DB->SelectRecord('candidate_test_history', "candidate_id=$candidate_id $exam_condition", 'GROUP_CONCAT(test_id) as test_id', 'GROUP BY candidate_id');
		
		
		if($testhistory && $testhistory->test_id)
		{
			//$failedTestCondition = " AND (id NOT IN ($testhistory->test_id)) ";
		}
		
		$testActiveCondition  = " (date_format(now(), '%Y-%m-%d %H:%i:%s') >= date_format(test_date, '%Y-%m-%d %H:%i:%s')) ";
		$testActiveCondition .= " AND ";
		$testActiveCondition .= " (date_format(now(), '%Y-%m-%d %H:%i:%s') <= date_format(test_date_end, '%Y-%m-%d %H:%i:%s')) ";
		$testActiveCondition .= " AND ";
		$testActiveCondition .= " (enable_test = 'Y') ";
		
		$totalTest = count($DB->SelectRecords('test', $testActiveCondition.$exam_condition.$failedTestCondition.$teacher_condition.$test_condition, 'id'));

		
		//echo $testActiveCondition.$exam_condition.$temp_test_condition.$failedTestCondition.$teacher_condition.$test_condition;
		for($testCount = 0; $testCount < $totalTest; $testCount++)
		{
			$testDetails = $DB->SelectRecord('test', $testActiveCondition.$exam_condition.$temp_test_condition.$failedTestCondition.$teacher_condition.$test_condition, '*', "order by test.id desc limit $testCount, 1");
			//echo 'thi8';print_r($testDetails);exit;
			if(!$testDetails) continue;
			
			$test_id = $_GET['e'];
			$exam_id = $_GET['t'];
			
			$testhistory=$DB->SelectRecord('candidate_test_history', "candidate_id=$candidate_id and exam_id=$exam_id and test_id=$test_id", '*', 'order by id desc');
	
			//if( ($testhistory == '') || ( ($testhistory->test_id != $test_id)) )
			//{
			//echo 'not ok'.$testDetails->id.'exam_id='.$exam_id.'maximum_marks'.$testDetails->maximum_marks;
				$_SESSION['testid'] = $testDetails->id;
				$_SESSION['exam_id'] = $exam_id;
				$_SESSION['total_marks'] = $testDetails->maximum_marks;
				break;
			//}	
			$temp_test_condition = '';
		}
		
		if(isset($_SESSION['testid']) && ($_SESSION['testid'])) $_SESSION['test_history'] = $testhistory;
		
		return $testDetails;
	}

		
	/**
	 * 	@author	: Ashwini Agarwal
	 * 	@param	: Candidate id
	 * 	@desc	: get homeworks to be conducted
	 */
	function getHomeworkToBeAssigned($exam_id_arr)
	{
		global $DB;
		
		$candidate_id = $_SESSION['candidate_id'];
		
		// get exam condition
		$exam_condition = '';
		if($exam_id_arr && count($exam_id_arr) > 0)
		{
			$exam_id_set = implode(',', $exam_id_arr);
			$exam_condition = " and exam_id in($exam_id_set) ";
		}
		else
		{
			return false;
		}
		
		// get conserning teacher IDs
		$teacher_condition = ' AND ((teacher_id = "") OR (teacher_id IS NULL)) ';
		$teacher = $DB->SelectRecord('exam_candidate_teacher', "candidate_id=$candidate_id $exam_condition", 'GROUP_CONCAT(DISTINCT teacher_id) as teacher_id', 'GROUP BY candidate_id');
		if($teacher && $teacher->teacher_id)
		{
			$teacher_condition = " AND ((teacher_id = '') OR (teacher_id IS NULL) OR (teacher_id IN ($teacher->teacher_id))) ";
		}
		
		// get homework ids for candidates
		$homework_condition = ' AND (homework.id = -1) ';
		$homeworks = $DB->SelectRecord('homework_candidate', "candidate_id=$candidate_id", 'GROUP_CONCAT(DISTINCT homework_id) as homework_id', 'GROUP BY candidate_id');
		if($homeworks && $homeworks->homework_id)
		{
			$homework_condition = " AND (homework.id IN ($homeworks->homework_id)) ";
		}
		
		// get completed homeworks
		$excludeHomeworkCondition = '';
		$homeworkhistory = $DB->SelectRecord('candidate_homework_history', "(candidate_id=$candidate_id) $exam_condition", 'GROUP_CONCAT(homework_id) as homework_id', 'GROUP BY candidate_id');
		if($homeworkhistory && $homeworkhistory->homework_id)
		{
			$excludeHomeworkCondition = " AND (homework.id NOT IN ($homeworkhistory->homework_id)) ";
		}
		
		$homeworkActiveCondition  = " (date_format(now(), '%Y-%m-%d %H:%i:%s') >= date_format(homework_date, '%Y-%m-%d %H:%i:%s')) ";
		$homeworkActiveCondition .= " AND ";
		$homeworkActiveCondition .= " (date_format(now(), '%Y-%m-%d %H:%i:%s') <= date_format(homework_date_end, '%Y-%m-%d %H:%i:%s')) ";
		$homeworkActiveCondition .= " AND ";
		$homeworkActiveCondition .= " (enable_homework = 'Y') ";		
		
		$query = "SELECT homework.*, e.exam_name FROM homework 
			JOIN examination e ON e.id = exam_id 
			WHERE $homeworkActiveCondition $exam_condition $teacher_condition $excludeHomeworkCondition $homework_condition";
		
		$totalHomeworks = $DB->RunSelectQuery($query);

		$_SESSION['homework_ids'] = array(); 
		
		if(is_array($totalHomeworks))
		{
			foreach($totalHomeworks as $homework) 
			{ 
				$_SESSION['homework_ids'][] = $homework->id;
			}
		}
		return $totalHomeworks;
	}
	
	
	/**
	 * 	@author	: Ashwini Agarwal
	 * 	@param	: Test id
	 * 	@desc	: check if test is active or not
	 */
	function isActiveTest($t_id)
	{
		global $DB;
	
		// get active condition
		$condition  = "(date_format(now(), '%Y-%m-%d %H:%i:%s') >= date_format(test_date, '%Y-%m-%d %H:%i:%s')) ";
		$condition .= " AND (date_format(now(), '%Y-%m-%d %H:%i:%s') <= date_format(test_date_end, '%Y-%m-%d %H:%i:%s')) ";
		$condition .= " AND (enable_test = 'Y') ";
		$condition .= " AND (id = '$t_id') ";
		
		$isActive = $DB->SelectRecord('test', $condition, 'id');
		
		if($isActive == '')
			return false;
		else
			return true;
	}
	
	/**
	 * 	@author	: Ashwini Agarwal
	 * 	@param	: homework id
	 * 	@desc	: check if homework is active or not
	 */
	function isActiveHomework($t_id)
	{
		global $DB;
	
		// get active condition
		$condition  = "(date_format(now(), '%Y-%m-%d %H:%i:%s') >= date_format(homework_date, '%Y-%m-%d %H:%i:%s')) ";
		$condition .= " AND (date_format(now(), '%Y-%m-%d %H:%i:%s') <= date_format(homework_date_end, '%Y-%m-%d %H:%i:%s')) ";
		$condition .= " AND (enable_homework = 'Y') ";
		$condition .= " AND (id = '$t_id') ";
		
		$isActive = $DB->SelectRecord('homework', $condition, 'id');
		
		if($isActive == '')
			return false;
		else
			return true;
	}
	
	/**
	 * 	@author : Ashwini Agarwal
	 * 	@desc	: get question media
	 */
	function getQuestionSource($test_info)
	{
		global $DB;
		/* get question media (subject/paper). set variables accordingly */
		$_SESSION['questionMedia'] = $qMedia = $test_info->question_media;
		
		if($qMedia == 'paper')
		{
			$qSourceTab = 'paper';
			$test_paper = $DB->SelectRecord('test_paper','test_id='.$test_info->id);
			$_SESSION['testPaperId'] = $paper_id = $test_paper->paper_id;
			$qMediaCondition = " paper_id = $paper_id ";
		}
		else
		{
			$qSourceTab = 'test';
			$qMediaCondition = " test_id = $test_info->id ";
		}
		/**/
		
		return array('qSourceTab'=>$qSourceTab, 'qMediaCondition'=>$qMediaCondition);
	}
	
	function unsetAndRestoreSession()
	{
		global $DB;
		$cand_id = $_SESSION['candidate_id'];
		$test_id = $_GET['e'];
		$exam_id = $_GET['t'];
		unset($_SESSION);
		session_unset();
		session_destroy();
		
		session_start();
		$user = $DB->SelectRecord('candidate',"id='$cand_id'");
		$_SESSION['candidate_id'] = $cand_id;
		$_SESSION['user_name'] = $user->user_name;
		$_SESSION['candidate_fname'] = $user->first_name;
		$_SESSION['candidate_lname'] = $user->last_name;
		/*$_SESSION['testid'] = $test_id;
		$_SESSION['exam_id'] = $exam_id;
		$_SESSION['is_resumed'] = $is_resume;
		$test = $DB->SelectRecord('test','id='.$test_id);
		$_SESSION['total_marks'] = $test->maximum_marks;
		$_SESSION['test_name'] = $test->test_name;
		$test_questions = $DB->SelectRecords('test_questions',"test_id=".$test_id);
		$_SESSION['total_question'] = count($test_questions->question_id);
		$_SESSION['maximum_marks'] = $test->maximum_marks;*/
		
	}
	
}
?>