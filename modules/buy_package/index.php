<?php
/*****************Developed by :- Chirayu Bansal
Date         :- 18-aug-2011
Module       :- Stream master
Purpose      :- Entry file for any action, call in Subject master module
 ***********************************************************************************/

defined("ACCESS") or die("Access Restricted");
//include_once('class.package.php');
$test_id = isset($_GET['t'])? $_GET['t'] :'';
$exam_id = isset($_GET['e'])? $_GET['e'] : "";

include_once('class.buy_package.php');

include_once("lib/config_paytm.php");
include_once("lib/encdec_paytm.php");

$DB = new DBFilter();

$buy_package_Object= new BuyPackage();

if(isset($getVars['nameID']))
{

    $nameID=$getVars['nameID'];
}

//if(isset($frmdata['id']))
//{
//
//    $buy_package_Object->addPackage();
//}
if(isset($frmdata['addbuypackage']))
{

    $buy_package_Object->buyPackageFunction();
}

if(isset($frmdata['editpackage']))
{

    $buy_package_Object->editPackage($nameID);
}

if(isset($getVars['do']))
{
    $do=$getVars['do'];
}
else
{
    $do='';
}

$examList = $DB->SelectRecords('examination', '', '*', 'order by exam_name');
$examList_for_edit_package = $DB->SelectRecords('examination', '', 'exam_name,id', 'order by exam_name');

$query = "select package_name,id from `package_details`";
$package_list_for_student = $DB->RunSelectQuery($query);


// Get all alloted tests that is buyed by student package.
$query_from_student_selected_package =  "SELECT sppd.package_id,exam_id,allotted_tests,candidate_id FROM student_personal_package_details sppd
JOIN alloted_tests_for_package atfp ON sppd.package_id = atfp.package_id" ;

$query_from_student_selected_package_list= $DB->RunSelectQuery($query_from_student_selected_package);



foreach ($query_from_student_selected_package_list as $student_package_key => $student_package_value)
{
    $query_from_examination_table =  "SELECT * FROM `examination` WHERE id = " .$student_package_value->exam_id ;
    $query_from_examination_table_lists= $DB->RunSelectQuery($query_from_examination_table);
}

switch($do)
{
    case "buy":
        $CFG->template = "buy_package/add_buy_package.php";
        break;

//    default:
    case 'buy_course':

        $CFG->template = "buy_package/course_list.php";
        break;

    case 'buy_exam_details':
        checker();
        if(!isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] == ''))
            Redirect(CreateURL("mod=user"));

        if(isset($_GET['exam']))
            $exam_id = $_GET['exam'];

//        $user_test_details = $UserOBJ->test_details($exam_id);
        $user_test_details = $buy_package_Object->test_details($exam_id);

        $CFG->template = "user/exam_details.php";
        break;

}
include(CURRENTTEMP."/index.php");

exit;

?>