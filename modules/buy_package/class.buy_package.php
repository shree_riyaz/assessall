<?php

/*****************Developed by :- Chirayu Bansal
Date         :- 18-aug-2011
Module       :- Subject master
Purpose      :- Class for function to add and edit Subject details
 ***********************************************************************************/

class BuyPackage
{
    //==========================================================================
    //to add a Subject
    //==========================================================================
    function buyPackageFunction()
    {
        global $DB,$frmdata;

        $candidate_id = $_SESSION['candidate_id'];
        $err='';

        if($frmdata['package_id'] != 0)
        {
            $query = "select special_price,total_price from selected_exam_details where package_id = ". $frmdata['package_id']." limit 1" ;
            $final_amount_to_pay = $DB->RunSelectQuery($query);
            $frmdata['TXN_AMOUNT'] = $final_amount_to_pay[0]->special_price ? $final_amount_to_pay[0]->special_price : $final_amount_to_pay[0]->total_price;
        }


        if($frmdata['package_id'] != 0)
        {


            $query = "select package_id from student_personal_package_details where package_id = ". $frmdata['package_id'].
                " AND candidate_id = " .$candidate_id. " AND status = 'TXN_SUCCESS'" ;
            $exist_package = $DB->RunSelectQuery($query);

            if($exist_package)
//            foreach($exist_package as  $key)
            {
//
//                if($frmdata['package_id'] == $key->package_id);
                $err.="You already have purchased this package, please choose new package.<br>";
            }
        }
//        elseif($exist_package == '')
//        {
//            $err.="Please select package1.<br>";
//        }

        if ($frmdata['package_id'] == 0)
        {
            $err.="Please select package.<br>";
        }

        elseif ($err!='') {

            $_SESSION['error'] = $err;

        }

        if($err=='')
        {

//            $expiry_date = date("Y-m-d", strtotime('+1 years'));
            $expiry_date = $oneYearOn = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));

            $student_first_table = [
                'candidate_id' =>$candidate_id,
                'package_id' =>$frmdata['package_id'],
                'date_of_buy' =>date("Y-m-d"),
                'date_of_package_expiry' =>$expiry_date,
                'order_id' =>$frmdata['ORDER_ID'],
                'cust_id' =>$frmdata['CUST_ID'],
                'industry_type_id' =>$frmdata['INDUSTRY_TYPE_ID'],
                'channel_id' =>$frmdata['CHANNEL_ID'],
                'txn_amount' =>$frmdata['TXN_AMOUNT'],
//                'status' =>$frmdata['STATUS'],
            ];

            $get_last_id = $DB->InsertRecord('student_personal_package_details',$student_first_table );
            $_SESSION['get_last_id'] = $get_last_id;
            $_SESSION['package_details'] = $student_first_table;

            ?>
            <?php
            $checkSum = "";
            $paramList = array();

            $ORDER_ID = $_POST["ORDER_ID"];
            $CUST_ID = $_POST["CUST_ID"];
            $INDUSTRY_TYPE_ID = $_POST["INDUSTRY_TYPE_ID"];
            $CHANNEL_ID = $_POST["CHANNEL_ID"];
            //$TXN_AMOUNT = $_POST["TXN_AMOUNT"];
            $TXN_AMOUNT = $frmdata['TXN_AMOUNT'];

            $CALLBACK_URL = $_POST["CALLBACK_URL"];

// Create an array having all required parameters for creating checksum.
            $paramList["MID"] = PAYTM_MERCHANT_MID;
            $paramList["ORDER_ID"] = $ORDER_ID;
            $paramList["CUST_ID"] = $CUST_ID;
            $paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
            $paramList["CHANNEL_ID"] = $CHANNEL_ID;
            $paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
            $paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
            $paramList["CALLBACK_URL"] = $CALLBACK_URL;

            $checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);

            ?>
            <form method="post" action="https://pguat.paytm.com/oltp-web/processTransaction" name="f1">
                <table border="1">
                    <tbody>
                    <?php
                    foreach($paramList as $name => $value) {
                        echo '<input type="hidden" name="' . $name .'" value="' . $value . '">';
                    }
                    ?>
                    <input type="hidden" name="CHECKSUMHASH" value="<?php echo $checkSum ?>">
                    </tbody>
                </table>
                <script type="text/javascript">
                    document.f1.submit();
                </script>
            </form>

            <?php
//            $_SESSION['success']="Package has been added successfully.";
            // Redirect(CreateURL('index.php','mod=user&do=course'));

            exit;
        }
//        elseif($err!='')
//        {
//            $_SESSION['error']=$err;
//        }


    }
    function courses()
    {
        global $DB;

        $query ="select e.* from examination as e";

        $result=$DB->RunSelectQuery($query);

        return $result;
    }

    function test_details($exam_id)
    {
        global $DB,$frmdata;

        //$query = "select *from test where exam_id=".$exam_id." AND enable_test= 'Y' ";

        $query = "select * from test where exam_id=".$exam_id."  AND enable_test= 'Y' and test_date_end>
now()ORDER BY `test_date_end`";
        $result = $DB->RunSelectQuery($query);
        return $result;
    }

}//end of class
?>