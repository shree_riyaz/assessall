<?php

$DB = new DBFilter();
/*$auth = new Authorise();

if (($_SESSION['admin_user_name'] != 'admin'))
{
	$mod = $_SESSION['module_name'];
	$do = $_SESSION['action_name'];
	
	$for = 'download';
	if (in_array($do, array('upload_question', 'upload_candidate')))
	{
		$for = 'add';
	}
	if ($auth->isAuthorisedAction($_SESSION['admin_user_id'], $mod, $for) == false)
	{
		$_SESSION['error'] = "Sorry! You don't have sufficient permission to perform this action.";
		if ($do == 'view_test_result')
			$do = 'result_sheet';
		Redirect(CreateURL(ROOTURL . '/index.php', "mod=$mod&do=$do"));
		exit;
	}
}

if (isset($_GET['csv_file']) && (($csv_file = $_GET['csv_file']) != ''))
{
	$filename = ROOT . "/format_".$csv_file."_list.csv";
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=" . basename($filename));
	readfile(($filename));
	exit;
}
*/
$exam_id = '';
if (isset($_GET['exam_id']))
{
	$exam_id = $_GET['exam_id'];
}

$test_id = '';
if (isset($_GET['test_id']))
{
	$test_id = $_GET['test_id'];
}

$candidate_id = '';
if (isset($_GET['candidate_id']))
{
	$candidate_id = $_GET['candidate_id'];
}

if(isset($_GET['candidate_result_details']))
{
	include_once 'candidate_result_details_pdf_sheet.php';
	include_once 'generatePdfFile.php';
}
elseif ($exam_id != '')
{
	$exam_detail = $DB->SelectRecord('examination', "id=$exam_id");
	$subject_result = getResultSheetReportForExcel($exam_id, $test_id, '', $_GET['year'], '', $_GET['month']);
	$exam_subject = getSubjectNameByExamId($exam_id, $test_id);
	
	$exam_summary = getExamSummaryData($exam_id);
	$first_time = 0;
	$partial = 0;
	for ($counter = 0; $counter < count($exam_summary); $counter++)
	{
		if ($exam_summary[$counter]->no_of_times == 1)
		{
			$first_time++;
		}
		elseif ($exam_summary[$counter]->no_of_times > 1)
		{
			$partial++;
		}
	}
	
	if ($test_id != '')
	{
		$test_info = getTestInfoByTestId($test_id);
	}
	else
	{
		$last_test = $DB->SelectRecords('candidate_test_history', "exam_id=$exam_id", 'test_id', 'order by test_id desc limit 1');
		$test_info = getTestInfoByTestId($last_test[0]->test_id);
	}
	
	include_once 'result_excel_sheet.php';
}
else
{
	if(isset($_GET['exportfor']) && (($exportFor = $_GET['exportfor']) != ''))
	{
		$query = $_SESSION['queryFor'.ucfirst($exportFor)];
		$candidate = $DB->RunSelectQuery($query);
	}
	else
	{
		$candidate = getCandidatePersonalDataForExcel();
	}
	include_once 'candidate_excel_sheet.php';
}
exit;
?>