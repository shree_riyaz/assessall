<?php

/**
 * @author :  Akshay Yadav
 */
class User
{

    function validateUser($getError = false)
    {
        global $DB, $frmdata;
        $error = '';

        if (isset($frmdata['candidate_id']) && ($frmdata['candidate_id'] != ''))
            $candidate_id = trim($frmdata['candidate_id']);
        else
            $error .= "Please enter username.<br>";

        if (isset($frmdata['password']) && ($frmdata['password'] != ''))
            $password = $frmdata['password'];
        else
            $error .= "Please enter password.<br>";

        if ($error == '') {
            $user = $DB->SelectRecord('candidate', "candidate_id='" . $candidate_id . "'");
            if ($user && (Encrypt($password) == $user->password)) {
                if ($user->is_disabled == 0) {
                    //$data= $_GET['data'];
                    $link = $_GET['link'];
                    //echo $data;
                    $this->loginUser($user);
                    //	if($data){
                    //	Redirect(CreateURL('index.php',"mod=guidelines"));
                    //	unset($_SESSION);
                    //	return true;
                    //}
                    //else
                    if ($link == 'dir') {

                        Redirect(CreateURL('index.php', "mod=guidelines&e=" . $_GET['t'] . "&t=" . $_GET['e']));
                    } else {
                        Redirect(CreateURL('index.php', "mod=dashboard"));
                    }
                } else {
                    $error .= "You are not authorised to login. Please contact system administrator.";
                }
            } else {
                $error .= "Please enter correct username or password.<br>";
            }
        }

        if ($error != '') {
            $_SESSION['error'] = $error;
        }

        if (!$getError) $error = false;
        return $error;
    }

    function loginUser($user)
    {
        global $DB;

        $_SESSION['candidate_id'] = $user->id;
        $_SESSION['candidate_fname'] = $user->first_name;
        $_SESSION['candidate_lname'] = $user->last_name;

        $user_log_data['candidate_id'] = $_SESSION['candidate_id'];
        $user_log_data['login_time'] = date("Y-m-d H:i:s");
        $user_log_data['ip'] = $_SERVER['REMOTE_ADDR'];
        $_SESSION['login_time'] = $user_log_data['login_time'];
        $DB->InsertRecord('user_log', $user_log_data);

        $log = array();
        $log['candidate_id'] = $user->id;
        $DB->InsertRecord('candidate_log', $log);
    }

    function logoutUser()
    {
        //$lastLogin=date('d-m-Y'); echo $lastLogin;
        unset($_SESSION);
        session_unset();
        session_destroy();
    }

    function registerUser()
    {
        global $DB, $frmdata;
        $err = '';

        if ($frmdata['first_name'] == '') {
            $err .= "Please enter first name.<br>";
        }
        if ($frmdata['last_name'] == '') {
            $err .= "Please enter last name.<br>";
        }

        if ($frmdata['email'] == '') {
            $err .= "Please enter email address.<br>";
        }


        if ($frmdata['candidate_id'] == '') {
            $err .= "Please enter username.<br>";
        }
        if ($frmdata['candidate_id'] != '') {
            $candidate_id = $frmdata['candidate_id'];
            $exist = $DB->SelectRecord('candidate', "candidate_id='" . $candidate_id . "'");
            if ($exist->candidate_id != '') {
                $err .= "Username already exist.<br>";
            }
        }

        if ($frmdata['email'] != '') {
            if (!filter_var($frmdata['email'], FILTER_VALIDATE_EMAIL)) {
                $err .= "Please enter a valid email address.<br>";
            } else {
                $exist = $DB->SelectRecord('candidate', "email='" . $frmdata['email'] . "'");
                if ($exist && ($exist->email != '')) {
                    $err .= "This email id is already in use by another user.<br>";
                }
            }
        }

        if ($err != '') {
            $_SESSION['error'] = $err;
            return false;
        }
        if ($err == '') {
            $password = MakeNewpassword();
            $frmdata['password'] = Encrypt($password);
            //$frmdata['first_name'] = mysql_real_escape_string($frmdata['first_name']);
            //$frmdata['last_name'] = mysql_real_escape_string($frmdata['last_name']);
            //$frmdata['candidate_id'] = mysql_real_escape_string($frmdata['candidate_id']);
            $id = $DB->InsertRecord('candidate', $frmdata);
            $test_id = $DB->SelectRecords('test', '', 'id');
            $exam_data = $DB->SelectRecords('examination', '', 'id');

            for ($i = 0; $i < count($exam_data); $i++) {
                $course_data['exam_id'] = $exam_data[$i]->id;
                $course_data['candidate_id'] = $id;

                $exam_data_id = $DB->InsertRecord('exam_candidate', $course_data);
            }


            for ($i = 0; $i < count($test_id); $i++) {
                $data['test_id'] = $test_id[$i]->id;
                $data['candidate_id'] = $id;
                $test_data = $DB->InsertRecord('test_candidate', $data);
            }

            $candidate_name = $DB->SelectRecords('candidate', 'id =' . $id, 'candidate_id,first_name,last_name,email');

            $subject = 'Welcome To Assessall.com';

            /*$mydata['name'] = ucfirst($candidate_name[0]->first_name).' '.ucfirst($candidate_name[0]->last_name);*/
            $mydata['name'] = ucfirst($candidate_name[0]->first_name);
            $mydata['user_name'] = $candidate_name[0]->candidate_id;
            $mydata['email'] = $candidate_name[0]->email;
            $mydata['password'] = $password;
            $mydata = (object)$mydata;
            $message = "<div style='background: none repeat scroll 0% 0% rgb(241, 241, 241); border: 1px solid rgb(6, 106, 117); width: 450px; color: rgb(153, 153, 153); font-family: Georgia; height: 500px;'>
				<div style='width: 100%; background: none repeat scroll 0% 0% rgb(6, 106, 117); height: 80px;'>
				<div style='width:150px; height:60px;margin:8px; float:left'>
				<img alt='Assess all' src='http://assessall.com/wp-content/uploads/2014/07/logo.png'>
				</div>
				</div>
				  <div style='margin: auto; float: left; text-align: left; padding: 10px 10px 0px; width: 95%; height: 365px;'>
				  <h2 style='color:#999'>Hello $mydata->name,</h2>
				  
				  <h3 style='color:#999'>Thank you for your registration.</h3>
				  <h3 style='color:#999'>Below are your login details for Assessall.</h3>
				  <br>
				  <h3 style='color:#999'>Username: $mydata->user_name</h3>
				  
				  <h3 style='color:#999'>Password: $mydata->password</h3>
				  <br><br><a style='font-size: 1.2em; color: rgb(255, 255, 255); text-decoration: none; background: none repeat scroll 0% 0% rgb(6, 106, 117); padding: 10px 20px; font-weight: bold; margin-top: 10px;' href='http://assessall.com/assessall/index.php'>Login</a>
            <br><br> <p style='color:#999'>Please check your mail regularly to get the updates/information regarding your examination date and time slot</p>
				<p>  <a style='font-size: 1.2em;text-decoration: none; color:rgb(6, 106, 117); padding:10px; font-weight: bold;;' href='http://assessall.com'>www.assessall.com</a></p>
            
				  </div>
				  <div style='height:30px;padding-top:10px;float:left; width:100%;text-align:center;background:#1d5770;'>
					<span style='color:#fff;font-weight:bold'>&copy; 2014 Assess All.</span>
					
				  </div>
				</div>

";
            //$message = "<br><br>Username:$mydata->user_name <br><br><br>Password:$mydata->password <br>";

            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //$headers .= "To: $mydata->name <$mydata->email>" . "\r\n";
            $headers .= "From: Assessall.com <contact@Assessall.com>" . "\r\n";

            mail($mydata->email, $subject, $message, $headers);        // Sending a mail to the Registered User
            $this->Mailtoadmin($id);

            $_SESSION['success'] = "Your account has been created successfully.<br/> An Username & Password Credentials are sent to your email address.";
            if (isset($_GET['e']) && $_GET['e'] != '') {
                Redirect(CreateURL('index.php', 'mod=user&link=dir&e=' . $_GET['e'] . '&t=' . $_GET['t']));
            } else {
                Redirect(CreateURL('index.php', '?mod=user'));
            }
            exit;
        }
    }

    function Mailtoadmin($id)
    {
        global $DB;
        $candidate_name = $DB->SelectRecords('candidate', 'id =' . $id, 'candidate_id,first_name,last_name,email');

        $subject = 'A New Member has Registered';

        /*$mydata['name'] = ucfirst($candidate_name[0]->first_name).' '.ucfirst($candidate_name[0]->last_name);*/
        $mydata['name'] = ucfirst($candidate_name[0]->first_name);
        $mydata['user_name'] = $candidate_name[0]->candidate_id;
        $mydata['email'] = $candidate_name[0]->email;
        $mydata['password'] = $password;
        $mydata = (object)$mydata;
        $message = "<div style='background: none repeat scroll 0% 0% rgb(241, 241, 241); border: 1px solid rgb(6, 106, 117); width: 450px; color:#666; font-family: Georgia; height: 380px;'>
				<div style='width: 100%; background: none repeat scroll 0% 0% rgb(6, 106, 117); height: 80px;'>
				<div style='width:150px; height:60px;margin:8px; float:left'>
				<img alt='Assess all' src='http://assessall.com/wp-content/uploads/2014/07/logo.png'>
				</div>
				</div>
				  <div style='margin: auto; float: left; text-align: left; padding: 10px 10px 0px; width: 95%; height: 250px;'>
				  <h2 style='color:#666; '>Hello Admin,</h2>
				  
				  <h3 style='color:#666; '>A New Member has Registered.</h3>
				  <h3 style='color:#666; '>Below are the details of user.</h3>
				  <br>
				  <h3 style='color:#666; '>Username: $mydata->user_name</h3>
				  <h3 style='color:#666; '>Email: $mydata->email</h3>
				 <p>  <a style='font-size: 1.2em;text-decoration: none; color:rgb(6, 106, 117); padding:10px; font-weight: bold;;' href='http://assessall.com'>www.assessall.com</a></p>
            
				  </div>
				  <div style='height:30px;padding-top:10px;float:left; width:100%;text-align:center;background:#1d5770;'>
					<span style='color:#fff;font-weight:bold'>&copy; 2014 Assess All.</span>
					
				  </div>
				</div>

";
        //$message = "<br><br>Username:$mydata->user_name <br><br><br>Password:$mydata->password <br>";

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        //$headers .= "To: Admin <manoj.sharma@sunarctechnologies.com>" . "\r\n";
        $headers .= "From: Assessall.com <contact@Assessall.com>" . "\r\n";

        mail('manoj.sharma@sunarctechnologies.com', $subject, $message, $headers);        // Sending a mail to the Registered User


    }

    function getUserInfo($userId)
    {
        global $DB;

        $query = 'SELECT candidate.*
					FROM candidate 
					WHERE candidate.id=' . $userId;
        $userInfo = $DB->RunSelectQuery($query);
        $userInfo = $userInfo[0];

        return $userInfo;
    }

    function uploadImage()
    {
        global $DB, $frmdata;
        $err = array();

        $file = $_FILES['profile_picture'];
        if ($file['tmp_name'] == '') {
            exit;
        }

        $ext = array('image/jpeg', 'image/jpg', 'image/gif', 'image/png');
        if (!in_array($file['type'], $ext)) {
            $err[] = "Please upload image in correct format.";
        }

        if ($file['size'] > 2000000) {
            $err[] = "Please upload image not more than 2MB.";
        }

        if (count($err) > 0) {
            $responce = array('sucess' => 0, 'message' => $err[0]);
        } else {
            $filename = uniqid() . '_' . $file['name'];
            $image_path = ADMINURL . '/uploadfiles/' . $filename;
            $FileObject = (object)$file;

            if (is_uploaded_file($file['tmp_name'])) {
                include_once(ADMINROOT . '/lib/image.class.php');
                $img = new thumb_image;
                $img->GenerateThumbFile($file['tmp_name'], $image_path);

                if (file_exists($image_path)) {
                    $frmdata = array('pro_image' => $filename);
                    $DB->UpdateRecord('candidate', $frmdata, 'id="' . $_SESSION['candidate_id'] . '"');
                    $responce = array('success' => 1, 'image' => ADMINURL . '/uploadfiles/' . $filename);
                } else {
                    $responce = array('sucess' => 0, 'message' => "An error occurred while saving file.\nPlease try after some time.");
                }
            }
        }

        echo json_encode($responce);
    }

    function deleteImage()
    {
        global $DB;
        $user = $DB->SelectRecord('candidate', 'id="' . $_SESSION['candidate_id'] . '"');
        @unlink(ADMINURL . '/uploadfiles/' . $user->pro_image);

        $frmdata = array('pro_image' => '');
        $DB->UpdateRecord('candidate', $frmdata, 'id="' . $_SESSION['candidate_id'] . '"');
    }

    function updatePersonalInfo()
    {
        global $DB, $frmdata;

        $nameID = $_SESSION['candidate_id'];
        $err = '';

        if ($frmdata['first_name'] == '') {
            $err .= "Please enter first name.<br>";
        }

        if ($frmdata['email'] == '') {
            //	$err .= "Please enter email address.<br>";
        }

        if ($frmdata['email'] != '') {
            $exist = $DB->SelectRecord('candidate', "email='" . $frmdata['email'] . "' and id!=" . $nameID);
            if ($exist && ($exist->email != '')) {
                $err .= "This email id is already in use by another student.<br>";
            }
        }

        if ($err != '') {
            $responce = array('success' => 0, 'message' => $err);
        } else {
            if ($frmdata['birth_date']) {
                $frmdata['birth_date'] = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $frmdata['birth_date'])));
            }

            $DB->UpdateRecord('candidate', $frmdata, 'id="' . $nameID . '"');

            if ($frmdata['birth_date']) {
                $frmdata['birth_date'] = date('d/m/Y', strtotime($frmdata['birth_date']));
            }

            $responce = array('success' => 1, 'data' => $frmdata);
        }

        echo json_encode($responce);
    }

    function changePassword()
    {
        global $DB, $frmdata;
        $err = '';

        if ($frmdata['old_password'] == '') {
            $err .= "Please enter old password.<br>";
        } elseif ($frmdata['old_password'] != '') {
            $exist = $DB->SelectRecord('candidate', "id=" . $_SESSION['candidate_id']);
            if ($exist->password != Encrypt($frmdata['old_password'])) {
                $err .= "Old password is not matching with your password.<br>";
            }
        }

        if ($frmdata['new_password'] == '') {
            $err .= "Please enter new password.<br>";
        } elseif (strlen($frmdata['new_password']) < 5) {
            $err .= "Please enter new password atleast 5 characters long.<br>";
        }

        if ($frmdata['confirm_password'] == '') {
            $err .= "Please enter confirm password.<br>";
        }

        if ($frmdata['new_password'] != $frmdata['confirm_password']) {
            $err .= "Your new password doesn't match with confirm password.<br>";
        }

        if ($err == '') {
            $frmdata['password'] = Encrypt($frmdata['new_password']);
            $DB->UpdateRecord('candidate', $frmdata, 'id=' . $_SESSION['candidate_id']);
            $_SESSION['success'] = "Your password has been changed successfully.";
            $frmdata = '';
        } elseif ($err != '') {
            $_SESSION['error'] = $err;
        }
    }

    function courses()
    {
        global $DB;

        $query = "select e.* from examination as e";

        $result = $DB->RunSelectQuery($query);

        return $result;
    }
    function getPackageIdByLoggedUser()
    {
        global $DB;

        $query = "select package_id
 from student_personal_package_details WHERE candidate_id =" .$_SESSION['candidate_id'];

        $result = $DB->RunSelectQuery($query);

        return $result;
    }

    function getPackageDetails()
    {

        global $DB, $frmdata;

        // Get all alloted tests that is buyed by student package.
        $query = "
                    SELECT exm.exam_name,sppd.*,exam_id,allotted_tests,candidate_id
                    FROM student_personal_package_details sppd
                    JOIN alloted_tests_for_package atfp
                    ON sppd.package_id = atfp.package_id
                    JOIN examination exm
                    ON atfp.exam_id = exm.id

                    WHERE sppd.candidate_id =" . $_SESSION['candidate_id'] . "
                    GROUP BY exm.exam_name
                    ORDER BY exm.exam_name ASC";

        $result = $DB->RunSelectQuery($query);

        return $result;
    }

    function getCourseByPackageID($package_id_from_url)
    {

        global $DB, $frmdata;

        // Get all alloted tests that is buyed by student package.
        $query = "
                    SELECT exm.exam_name,sppd.*,exam_id,allotted_tests,candidate_id
                    FROM student_personal_package_details sppd
                    JOIN alloted_tests_for_package atfp
                    ON sppd.package_id = atfp.package_id
                    JOIN examination exm
                    ON atfp.exam_id = exm.id

                    WHERE sppd.candidate_id =" . $_SESSION['candidate_id'] . " AND sppd.package_id = " . $package_id_from_url .
                    " GROUP BY exm.exam_name
                    ORDER BY exm.exam_name ASC";

        $result = $DB->RunSelectQuery($query);
        return $result;
    }
    function getPackageDetailsWithName()
    {
        global $DB, $frmdata;

        // Get all alloted tests that is buyed by student package.
        $query = "SELECT pd.package_name,exm.exam_name,sppd.*,exam_id,allotted_tests,candidate_id
                    FROM student_personal_package_details sppd
                    JOIN alloted_tests_for_package atfp
                    ON sppd.package_id = atfp.package_id
                    JOIN examination exm
                    ON atfp.exam_id = exm.id

                    JOIN package_details pd
                    ON sppd.package_id = pd.id

                    WHERE sppd.candidate_id =" . $_SESSION['candidate_id'] . "
                    /*GROUP BY exm.exam_name*/
                    GROUP BY pd.package_name

                    ORDER BY exm.exam_name ASC";

        $result = $DB->RunSelectQuery($query);


//        echo '<pre>';
//        print_r($query);
//        print_r($result);
//        exit();


        return $result;

    }

    function test_details($exam_id,$package_id_from_url)
    {
        global $DB, $frmdata;

        $query = "
                  SELECT sppd.package_id,sppd.date_of_buy,sppd.date_of_package_expiry, sppd.candidate_id, tst.test_name, tst.id, tst.exam_id
                  FROM alloted_tests_for_package atfp
                  
                  JOIN test tst
                  ON atfp.allotted_tests = tst.id
                    
                  JOIN student_personal_package_details sppd
                  JOIN candidate cnd
                  ON sppd.candidate_id = cnd.id
                
                  JOIN alloted_tests_for_package altp
                
                  ON atfp.package_id = sppd.package_id
                
                  WHERE sppd.candidate_id =" . $_SESSION['candidate_id'] . "
            
                  AND 
                  tst.exam_id=" . $exam_id. "
                AND
                  sppd.package_id=" . $package_id_from_url. "

                  GROUP BY atfp.allotted_tests;
                    
                ";

        $result = $DB->RunSelectQuery($query);

        return $result;
    }
//-----------------------------

    function getOrderId($package_id)
    {

        global $DB;

        $query = "SELECT id,order_id from student_personal_package_details WHERE id = " .$package_id;

        $result = $DB->RunSelectQuery($query);

        return $result;
    }

    function updateCurrentResponse($package_id,$payment_transaction_details)
    {
        global $DB;
//    $query = "update student_personal_package_details WHERE candidate_id = " .$_SESSION['candidate_id']. "AND order_id = " .$order_id ;
        $result = $DB->UpdateRecord('student_personal_package_details', $payment_transaction_details, 'id="'.$package_id.'"');
        unset($_SESSION['get_last_id']);
        unset($_SESSION['package_details']);



        return $result;
    }

}


?>