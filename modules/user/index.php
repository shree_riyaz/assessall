<?php
defined("ACCESS") or die("Access Restricted");
$test_id = $_GET['t'];
$exam_id = $_GET['e'];

//        echo '<pre>';
//        print_r($package_id_from_url);
//        exit();

//include_once("../buy_package/lib/config_paytm.php");
//include_once("../buy_package/lib/encdec_paytm.php");

require_once LIBPAYTM . 'config_paytm.php';
require_once LIBPAYTM . 'encdec_paytm.php';
//require_once LIBPAYTM . 'encdec_paytm.php';
include_once('class.user.php');
$UserOBJ = new User();
global $DB;

if(isset($frmdata['submit']))
	$UserOBJ->validateUser();
if(isset($frmdata['register_user']))
	$UserOBJ->registerUser();
if(isset($frmdata['changepassword'])) 
	$UserOBJ->changePassword();
	
$do = '';
if(isset($getVars['do']))
	$do = $getVars['do'];

$package_id = intval($_GET['q']);

$query = "select * from selected_exam_details sed
			JOIN examination
			ON examination.id = sed.exam_id
			where package_id=" . $package_id . " ";
$get_package_result = $DB->RunSelectQuery($query);
?>
<br>

<table style="display: <?php if($get_package_result == '') {?> none <?php } else { ?> block <?php } ?>" class="package_details_table" width="100%" border="1px solid red">
<tr>
	<th>Exam Name</th>
	<th>Number of Paper</th>
	<th>Price per paper</th>
</tr>
<?php foreach ($get_package_result as $key) { ?>
<tr>

	<td><?php echo $key->exam_name; ?></td>
	<td><?php echo $key->number_of_paper; ?></td>
	<td><?php echo $key->price_per_paper; ?></td>
</tr>
<?php
}
?>
    <tr>
        <td colspan="2">Total Price</td>
        <td colspan="1"> <?php echo $key->total_price; ?></td>
    </tr>
    <tr>
        <td colspan="2">Special Price</td>
        <td style="color: green; font-weight: bold" colspan="1"> <?php echo $key->special_price ? $key->special_price : 'No discount available for this package now.'; ?></td>
    </tr>
</table>
<?php
switch($do)
{
	default :
	case 'login':
		if(isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] != '') && $_GET['e']!='')
		Redirect(CreateURL('index.php',"mod=guidelines&t=".$exam_id."&e=".$test_id));
		elseif(isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] != ''))
		Redirect(CreateURL('index.php',"mod=dashboard"));


		require LIB . '/facebook/src/facebook.php';
		require_once LIB . '/google/src/Google_Client.php';
		require_once LIB . '/google/src/contrib/Google_PlusService.php';
		require_once LIB . '/google/src/contrib/Google_Oauth2Service.php';
		$facebook = new Facebook(array('appId' =>FB_APP_ID,'secret' =>FB_APP_SECRET,));
		$facebookLoginUrl = $facebook->getLoginUrl(array('scope' =>'user_birthday,email,offline_access','redirect_uri' =>FB_REDIRECT_URL));
		global $apiConfig;
		$apiConfig['oauth2_client_id'] = GOOGLE_CLIENT_ID;
		$apiConfig['oauth2_client_secret'] = GOOGLE_CLIENT_SECRET;
		$apiConfig['oauth2_redirect_uri'] = GOOGLE_REDIRECT_URL;
		$client = new Google_Client();
		$client->setScopes('https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email');
		$googleLoginUrl = $client->createAuthUrl();
		if($exam_id!='' && $test_id!='')
		{
			$_SESSION['e']=$exam_id;
			$_SESSION['t']=$test_id;
		}
		$CFG->template = "user/login.php";
		break;
	case "forgotpass":

		include_once(MOD."/forgotpass/forgotpass.php");
		break;
	case "package_list":

	checker();
		if(!isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] == ''))
			Redirect(CreateURL("mod=package_list"));

		$getPackageDetailsWithNameList = $UserOBJ->getPackageDetailsWithName();

		//		Riyaz
        $paytmChecksum = "";
        $paramList = array();
        $isValidChecksum = "FALSE";

        $paramList = $_POST;
        $paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application�s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
        $isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

        if($isValidChecksum == "TRUE") {
            if (isset($_POST) && count($_POST)>0 )
            {
//                foreach($_POST as $paramName => $paramValue) {
                    $package_id = $_SESSION['get_last_id'];
                    $package_details = $_SESSION['package_details'];
                    $order_id = $_SESSION['package_details']['order_id'];

                    $payment_transaction_details = [
                        'order_id' =>$_POST['ORDERID'],
                        'currency' =>$_POST['CURRENCY'],
                        'txn_id' =>$_POST['TXNID'],
                        'status' =>$_POST['STATUS'],
                        'bank_txn_id' =>$_POST['BANKTXNID'],
                        'resp_code' =>$_POST['RESPCODE'],
                        'resp_msg' =>$_POST['RESPMSG'],
                        'txn_date' =>$_POST['TXNDATE'],
                        'bank_name' =>$_POST['BANKNAME'],
                        'payment_mode' =>$_POST['PAYMENTMODE'],
                        'gateway_name' =>$_POST['GATEWAYNAME'],
                    ];


	//				Get last inserted ID to check last order id .
					$getLastOrderId= $UserOBJ->getOrderId($package_id);
				$err='';
                    if ($_POST['STATUS'] == 'TXN_SUCCESS' && $getLastOrderId[0]->order_id == $_POST['ORDERID'])
                    {
//						Update row
						$updatePackage = $UserOBJ->updateCurrentResponse($package_id,$payment_transaction_details);
						$response_payment = "<center><b>Congratulation! Transaction done successfully1. </b></center>" . "<br/>";
						$_SESSION['success']="<center><b>Congratulation! Transaction done successfully2. </b></center>" . "<br/>";
                    }
                    else
                    {
						$DB->DeleteRecord('student_personal_package_details','id="'.$_SESSION['get_last_id'].'"');

						unset($_SESSION['get_last_id']);
						unset($_SESSION['package_details']);

						$response_payment = "<center><b>We apologies for inconvenience,Transaction status failure, please try again to make payment1.</b></center>" . "<br/>";
						$_SESSION['success']="We apologies for inconvenience, Transaction status failure, please try again to make payment2.";
                    }
					if($err!='')
					{
						$_SESSION['error']=$err;
					}
//                    echo "<br/>" . $paramName . " = " . $paramValue;
//                }
            }
        }
//		Riyaz

		$CFG->template = "user/package.php";

		break;
	case 'course':
	checker();
		if(!isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] == ''))
			Redirect(CreateURL("mod=user"));
//		$user = $UserOBJ->courses();
			$package_id_from_url = $_GET['package'];

//		$package_details = $UserOBJ->getPackageDetails();
		$package_details = $UserOBJ->getCourseByPackageID($package_id_from_url);

		$CFG->template = "user/course.php";
		break;
	
	case 'exam_details':
		checker();
		if(!isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] == ''))
			Redirect(CreateURL("mod=user"));
			
		if(isset($_GET['exam']))
			$exam_id = $_GET['exam'];
		$package_id_from_url = $_GET['package'];
		$getPackageIdByLoggedUser = $UserOBJ->getPackageIdByLoggedUser();
		$user_test_details = $UserOBJ->test_details($exam_id,$package_id_from_url);


		$CFG->template = "user/exam_details.php";
		break;

	case 'register':
		if(isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] != ''))
			Redirect(CreateURL('index.php',"mod=guidelines")); 
		
		
		$CFG->template = "user/register.php";
		break;
		
	case 'search':
		$term = trim(strip_tags($_GET['term']));
		$result_data = $DB->SelectRecords('cities',"cityName LIKE '%$term%'",'cityName,cityID,stateID');
		for($i=0;$i<count($result_data);$i++)
		{
			$city[] = $result_data[$i]->cityName;
		}
		
		echo  json_encode($city);
		exit;
	case "sample_test":
		include_once(MOD."/sample_test/index.php");
		//$CFG->template = "";
		break;
	case "sample_guidelines":
		include_once(MOD."/sample_guidelines/index.php");
		//$CFG->template = "";
		break;
	case "sample_result":
		include_once(MOD."/sample_result/index.php");
		//$CFG->template = "";
		break;
	
	case 'profile':
		if(!isset($_SESSION['candidate_id']) || ($_SESSION['candidate_id'] == ''))
			Redirect(CreateURL('index.php'));

		$candidate_id = $_SESSION['candidate_id'];
		$user = $UserOBJ->getUserInfo($candidate_id);
		
		$CFG->template = "user/profile.php";
		break;
	
	case 'upload_image':
		if(isset($_GET['delete_image']) && ($_GET['delete_image'] == 1))
		{
			$UserOBJ->deleteImage();
		}
		else
		{
			$UserOBJ->uploadImage();
		}
		exit;

	case 'edit_personal_info':
		$UserOBJ->updatePersonalInfo();
		exit;

	case 'validate_username':
		echo $DB->SelectRecord('candidate',"candidate_id='".$frmdata['candidate_id']."'") ? 0 : 1;
		exit;
		
	case 'logout':
		$user_log_data['logout_time']=date("Y-m-d H:i:s");
		$DB->UpdateRecord('user_log', $user_log_data,"candidate_id='".$_SESSION['candidate_id']."' AND login_time='".$_SESSION['login_time']."'");

		$UserOBJ->logoutUser();
			

			if(isset($_GET['closewin']) && $_GET['closewin']==1)
			{
			echo  "<script type='text/javascript'>";
			echo "window.close();";
			echo "</script>";
			}


		Redirect('http://'.$_SERVER['HTTP_HOST']);
		exit;
	
	case 'changepwd':
	$CFG->template = "user/changepwd.php";
	
}

include(TEMP."/index.php");
exit;
?>