<?php
defined("ACCESS") or die("Access Restricted");

include_once('class.user.php');
$UserOBJ = new User();
global $DB;

if(isset($frmdata['submit']))
	$UserOBJ->validateUser();
if(isset($frmdata['register_user']))
	$UserOBJ->registerUser();
if(isset($frmdata['changepassword']))
	$UserOBJ->changePassword();
	//print_r($_SESSION);
	/*if(isset($_SESSION['is_sexam_begin']) && $_SESSION['is_sexam_begin']==1)
	{
		include_once(MOD."/sample_test/index.php");
	}*/
$do = '';
if(isset($getVars['do']))
	$do = $getVars['do'];

switch($do)
{
	default :

	case 'login':
		if(isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] != ''))
			{	
			Redirect(CreateURL('index.php',"mod=dashboard")); 
			}
		$CFG->template = "user/login.php";
		break;
	case "forgotpass":
	
		include_once(MOD."/forgotpass/forgotpass.php");
		break;	
	case 'course':
		if(!isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] == ''))
			Redirect(CreateURL("mod=user")); 
		$user = $UserOBJ->courses();
				
		$CFG->template = "user/course.php";
		break;
	case "exam":
		include_once(MOD."/exam/index.php");
		break;
	case 'exam_details':
		if(!isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] == ''))
			Redirect(CreateURL("mod=user")); 
			
		if(isset($_GET['exam']))
			$exam_id = $_GET['exam'];
		
		$user_test_details = $UserOBJ->test_details($exam_id);
				
		$CFG->template = "user/exam_details.php";
		break;
	
	case 'register':
		if(isset($_SESSION['candidate_id']) && ($_SESSION['candidate_id'] != ''))
			Redirect(CreateURL('index.php',"mod=guidelines")); 
		
		
		$CFG->template = "user/register.php";
		break;
	case "sample_test":
		include_once(MOD."/sample_test/index.php");
		//$CFG->template = "";
		break;
	case "sample_result":
		include_once(MOD."/sample_result/index.php");
		//$CFG->template = "";
		break;
	case "sample_guidelines":
		include_once(MOD."/sample_guidelines/index.php");
		//$CFG->template = "";
		break;		
	case 'search':
		$term = trim(strip_tags($_GET['term']));
		$result_data = $DB->SelectRecords('cities',"cityName LIKE '%$term%'",'cityName,cityID,stateID');
		for($i=0;$i<count($result_data);$i++)
		{
			$city[] = $result_data[$i]->cityName;
		}
		
		echo  json_encode($city);
		exit;
	
	case 'profile':
		if(!isset($_SESSION['candidate_id']) || ($_SESSION['candidate_id'] == ''))
			Redirect(CreateURL('index.php'));

		$candidate_id = $_SESSION['candidate_id'];
		$user = $UserOBJ->getUserInfo($candidate_id);
		
		$CFG->template = "user/profile.php";
		break;
	
	case 'upload_image':
		if(isset($_GET['delete_image']) && ($_GET['delete_image'] == 1))
		{
			$UserOBJ->deleteImage();
		}
		else
		{
			$UserOBJ->uploadImage();
		}
		exit;

	case 'edit_personal_info':
		$UserOBJ->updatePersonalInfo();
		exit;

	case 'validate_username':
		echo $DB->SelectRecord('candidate',"candidate_id='".$frmdata['candidate_id']."'") ? 0 : 1;
		exit;
	
	case 'logout':
		
		$user_log_data['logout_time']=date("Y-m-d H:i:s");
		$DB->UpdateRecord('user_log', $user_log_data,"candidate_id='".$_SESSION['candidate_id']."' AND login_time='".$_SESSION['login_time']."'");
		$UserOBJ->logoutUser();
		Redirect("index.php");
		exit;
	
	case 'changepwd':
	$CFG->template = "user/changepwd.php";
	
}

include(TEMP."/index.php");
exit;
?>