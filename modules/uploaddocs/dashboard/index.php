<?php 

/*****************Developed by 	 :- Akshay Yadav
					Module       :- Candidate Dashboard
					Purpose      :- Entry file for any action, call in Dashboard module
***********************************************************************************/



defined("ACCESS") or die("Access Restricted");
$DB = new DBFilter();

include_once('class.dashboard.php');
$DashboardOBJ = new Dashboard();

$exportEnabled = true;

$do = '';
if(isset($getVars['do']))
$do=$getVars['do'];

switch($do)
{
	default:
	case 'showinfo':
			$candidate_id = $_SESSION['candidate_id'];
			$lastLoginData = $DB->SelectRecords('user_log', "candidate_id='".$candidate_id."'",'login_time','order by id desc');
			
			$testdetail = $DashboardOBJ->lastTestdetail();
			if($testdetail)
			{
				//$candidate_id = $_SESSION['candidate_id'];
				$exam_id= $testdetail[0]->exam_id;
				$test_id = $testdetail[0]->test_id;

				$test_info = $DB->SelectRecord('test', "id=$test_id");
				$qMedia = $test_info->question_media;
				
				$test_paper = $DB->SelectRecord('test_paper','test_id='.$test_id);
				$paper_id = $test_paper->paper_id;
					
				$questionHistory = $DB->SelectRecords('candidate_question_history', "((exam_id='$exam_id') AND (test_id='$test_id') AND (candidate_id='$candidate_id'))");
				$testHistory = $DB->SelectRecord('candidate_test_history', "((exam_id='$exam_id') AND (test_id='$test_id') AND (candidate_id='$candidate_id'))");
				
				$testHistoryAll = $DB->SelectRecords('candidate_test_history', 'candidate_id='.$candidate_id);
				
				$candidateInfo = $DB->SelectRecord('candidate',"id = $candidate_id");
				$testInfo = $DB->SelectRecord('test',"id = $test_id");
				$examInfo = $DB->SelectRecord('examination', "id='$exam_id'");
				
				if($qMedia == 'subject')
				{
					$testQuestions = $DB->SelectRecords('test_questions',"test_id = '$test_id'");
				}
				
				$givenans = $DB->SelectRecords('candidate_question_history', "((exam_id='$exam_id') AND (test_id='$test_id') AND (candidate_id='$candidate_id'))" ,'is_answer_correct, given_answer_id' );
				
				$no_correctans = 0;
				$no_wrongans = 0;
				$no_skipped = 0;
				
				foreach($givenans as $ans)
				{
					if($ans->is_answer_correct == 'Y')
					{
						$no_correctans++;
					}
					elseif(($ans->is_answer_correct == 'N') && ($ans->given_answer_id != '')  && ($ans->given_answer_id != 0))
					{
						$no_wrongans++;
					}
					else
					{
						$no_skipped++;
					}			
				}
				

				$candidate_subject_marks = countCandidateSubjectMarks($exam_id, $test_id, $candidate_id);
				$test_subject_result = array();
				
				for($counter=0; $counter<count($candidate_subject_marks); $counter++)
				{
					$subject_id = $candidate_subject_marks[$counter]->subject_id;
					$subject_marks = $candidate_subject_marks[$counter]->subject_marks;
					
					if($qMedia == 'subject')
					{
						$test_subject = $DB->SelectRecord('test_subject',"exam_id=".$exam_id." and test_id=".$test_id." and subject_id=".$subject_id);
					}
					
					$candidate_subject_marks_percentage = (($subject_marks/$test_subject->subject_total_marks)*100);
					$candidate_subject_marks_percentage = number_format($candidate_subject_marks_percentage, 2, '.', '');
				
					$exam_subject = $DB->SelectRecord('exam_subjects',"exam_id=".$exam_id." and subject_id=".$subject_id);
					$min_marks = $exam_subject->subject_min_mark;
					
					$test_subject_result[$counter]['subject_name'] = $candidate_subject_marks[$counter]->subject_name;
					$test_subject_result[$counter]['solve_time'] = $candidate_subject_marks[$counter]->solve_time;
					$test_subject_result[$counter]['subject_percentage'] = $candidate_subject_marks_percentage;
					$test_subject_result[$counter]['subject_min_marks'] = $exam_subject->subject_min_mark;
					$test_subject_result[$counter]['subject_max_marks'] = $exam_subject->subject_max_mark;
					
					if($candidate_subject_marks_percentage < $min_marks)
					{
						$test_subject_result[$counter]['result'] = 'Fail';
						$test_result='F';
					}
					else 
					{
						$test_subject_result[$counter]['result'] = 'Pass';
					}
				}
			}
		$CFG->template="dashboard/showinfo.php";
	break;

	case 'question_xml' :
		$DashboardOBJ->getQuestionXml();
		exit;
	break;
	
	case 'test_xml' :
		$DashboardOBJ->getTestXml();
		exit;
	break;
	
	case 'candidate_xml' :
		$DashboardOBJ->getCandidateXml();
		exit;
	break;
} 

include(CURRENTTEMP."/index.php");
exit;
?>