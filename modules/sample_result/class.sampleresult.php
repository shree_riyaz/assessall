<?php 
/**
 * 	@author : Akshay Yadav
 */

class SampleResult
{
		function getGraph($year='2012', $month='', $test_id, $candidate_id, $type, $sub_type='')
	{
		global $DB, $exportEnabled;
		
		$export = "exportEnabled='1'";
		if (!$exportEnabled)
		{
			$export = "exportEnabled='0'";
		}
		
		$MonthsNamesShort = array(null, 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun');
		$MonthsNames = array(null, 'July', 'August', 'September', 'October', 'November', 'December', 'January', 'February', 'March', 'April', 'May', 'June');
		switch($type)
		{
			default:
			case 'monthly':
				$Query = "SELECT YEAR( cth.created_date ) AS year,
						MONTH( cth.created_date ) AS month,
						IF(MONTH( cth.created_date ) <= 6, 
								(MONTH( cth.created_date ) + 6), 
								((MONTH( cth.created_date ) + 6) % 12)
							) AS month_modified,
						SUM( IF( cth.result = 'P', 1, 0 ) ) AS pass, 
						SUM( IF( cth.result = 'F', 1, 0 ) ) AS fail,
						COUNT(*) as total
    					
						FROM candidate AS c
		    			JOIN candidate_test_history AS cth ON c.id = cth.candidate_id
    					JOIN test AS t ON t.id = cth.test_id
   						
    					WHERE c.id = '$candidate_id'
    					GROUP BY month
    					HAVING ( year = '$year' AND month >= '7') OR ( year = '" . ($year+1) . "' AND month <= '6')
    					ORDER BY month_modified";
				
				$ResultArray = array_fill(1, 12, 0); // fill the Result array with 0 values for each month
				$ChartHeading = 'Monthly Number of Appeared Tests: ' . $year . ' - ' . ($year+1);
				$XaxisName = 'Months';
				$YaxisName = 'Number of Test';
			break;
			
			case 'test_wise':

				$sub_query = '';
				if($sub_type == 'Pass')
				{
					$sub_query = " AND cth.result='P' ";
				}
				elseif($sub_type == 'Fail')
				{
					$sub_query = " AND cth.result='F' ";
				}
				
				
				$Query = "SELECT 
							COUNT( * ) AS total,
							t.id AS test_id,
							t.test_name as test_name,
							YEAR( cqh.created_date ) AS year,
							MONTH( cqh.created_date ) AS month,
							sum( if((q.question_level = 'B') AND (cqh.given_answer_id = '0'), q.marks, 0 ) ) AS Skip_B,
							sum( if((q.question_level = 'B') AND (is_answer_correct = 'Y'), q.marks, 0 ) ) AS Corr_B,
							sum( if((q.question_level = 'B') AND (is_answer_correct = 'N') AND ((cqh.given_answer_id <> '0') OR (cqh.given_answer_id <> '-1')), q.marks, 0 ) ) AS Wrng_B,
							
							sum( if((q.question_level = 'I') AND (cqh.given_answer_id = '0'), q.marks, 0 ) ) AS Skip_I,
							sum( if((q.question_level = 'I') AND (is_answer_correct = 'Y'), q.marks, 0 ) ) AS Corr_I,
							sum( if((q.question_level = 'I') AND (is_answer_correct = 'N') AND ((cqh.given_answer_id <> '0') OR (cqh.given_answer_id <> '-1')), q.marks, 0 ) ) AS Wrng_I,
							
							sum( if((q.question_level = 'H') AND (cqh.given_answer_id = '0'), q.marks, 0 ) ) AS Skip_H,
							sum( if((q.question_level = 'H') AND (is_answer_correct = 'Y'), q.marks, 0 ) ) AS Corr_H,
							sum( if((q.question_level = 'H') AND (is_answer_correct = 'N') AND ((cqh.given_answer_id <> '0') OR (cqh.given_answer_id <> '-1')), q.marks, 0 ) ) AS Wrng_H
							
							FROM candidate AS c
							JOIN candidate_question_history AS cqh ON c.id = cqh.candidate_id
							JOIN question AS q ON q.id = cqh.question_id
							JOIN test AS t ON t.id = cqh.test_id
							JOIN candidate_test_history AS cth ON ((t.id = cth.test_id) AND (c.id = cth.candidate_id))
							
							WHERE c.id = '$candidate_id' $sub_query
							
							GROUP BY year, test_name, month
							HAVING (year = '$year' AND month = '$month')
							ORDER BY t.test_name";
	
				$monthNo = ($month + 6) % 12;
				$monthNo = ($monthNo == 0) ? 12 : $monthNo;
				$ChartHeading = $sub_type.'ed Tests in: '.$MonthsNames[$monthNo] . ' - ' .$year;
				$XaxisName = 'Test';
				$YaxisName = 'Marks';
			break;
		}

		$result = $DB->RunSelectQuery($Query);
		//echo "<pre>";print_r($result);count($result);exit;
		
		//$logourl = ROOTURL . "/head.jpg";
		$FCExporter = ROOTURL . "/lib/FusionCharts/ExportHandlers/JavaScript/index.php";
		
		$Output = '<chart caption="'.$ChartHeading.'" xAxisName="'.$XaxisName.'" yAxisName="'.$YaxisName.'" 
					showNames="1" bgColor="E6E6E6,F0F0F0" bgAlpha="100,50" bgRatio="50,100" bgAngle="270"
					labelDisplay="AUTO" useEllipsesWhenOverflow="0" showBorder="1" borderColor="000000" 
					baseFontSize="12" decimalPrecision="0" '. $export .' html5ExportHandler="'. $FCExporter .'" 
					exportFileName="Candidate_Performance" ';

		switch ($type)
		{
			default:
			case 'monthly':
			
			if($result != 0)
			{	
				$count_month = 0;
				$max = 0;
				$pass = "<dataset seriesName='Pass' color='55FF55' showValues='0'>";
				$fail = "<dataset seriesName='Fail' color='FF5555' showValues='0'>";
				
				for($counter = 1; $counter <= 12; $counter++)
				{
					$value_p = 0;
					$value_f = 0;
					
					if($result[$count_month]->month_modified == $counter)
					{
						$url = "/index.php?mod=report&do=candidate_performance&getGraph=1&year=".$result[$count_month]->year."&month=".$result[$count_month]->month."&test=&candidate_id=$candidate_id&type=test_wise";
						
						$url_p = ROOTURL . urlencode($url . "&sub_type=Pass");
						$url_f = ROOTURL . urlencode($url . "&sub_type=Fail");
						
						$value_p = $result[$count_month]->pass;
						$value_f = $result[$count_month]->fail;
					
						$link_p = '';
						$link_f = '';
						
						if($value_p > 0)
						$link_p = " link='JavaScript:newGraph(\"$url_p\",\"chart1\",\"$value_p\");' " ;						
						if($value_f > 0)
						$link_f = " link='JavaScript:newGraph(\"$url_f\",\"chart1\",\"$value_f\");' " ;					
						
						$max = ($max > $result[$count_month]->total) ? $max : $result[$count_month]->total;
						$count_month++;
					}
					
					$pass .= "<set value='$value_p' $link_p />";
					$fail .= "<set value='$value_f' $link_f />";
				}
				$max = getMax($max);
				
				$pass .= "</dataset>";
				$fail .= "</dataset>";
				
				$Output .= " numDivLines='$max'>";
				
				$Output .= "<categories>";
				for($counter = 1;$counter <= 12; $counter++)
				{
					$Output .= "<category name='$MonthsNamesShort[$counter]' />";
				}
				$Output .= "</categories>";
				$Output .= "<dataset>" . $pass . $fail . "</dataset>";
			}
			else
			{
				$Output = "<chart>";
			}
			break;
			 
			case 'test_wise':
			
			if($result != 0)
			{	
				$max = 0;
				
				$Corr_H_Data = "<dataset seriesName='Beginner Correct' showValues='0' color='116611'>";
				$Corr_I_Data = "<dataset seriesName='Intermediate Correct' showValues='0' color='44FF44'>";
				$Corr_B_Data = "<dataset seriesName='Higher Correct' showValues='0' color='88DD88'>";
				
				$Wrng_H_Data = "<dataset seriesName='Beginner Wrong' showValues='0' color='661111'>";
				$Wrng_I_Data = "<dataset seriesName='Intermediate Wrong' showValues='0' color='FF4444'>";
				$Wrng_B_Data = "<dataset seriesName='Higher Wrong' showValues='0' color='DD8888'>";
				
				$Skip_H_Data = "<dataset seriesName='Beginner Skipped' showValues='0' color='111166'>";
				$Skip_I_Data = "<dataset seriesName='Intermediate Skipped' showValues='0' color='4444FF'>";
				$Skip_B_Data = "<dataset seriesName='Higher Skipped' showValues='0' color='8888DD'>";
				
				$test_names = "<categories>";
				
				$count = count($result);
				for($counter = 0; $counter < $count; $counter++)
				{
					$Corr_H = $result[$counter]->Corr_H;
					$Corr_I = $result[$counter]->Corr_I;
					$Corr_B = $result[$counter]->Corr_B;
					
					$Wrng_H = $result[$counter]->Wrng_H;
					$Wrng_I = $result[$counter]->Wrng_I;
					$Wrng_B = $result[$counter]->Wrng_B;
					
					$Skip_H = $result[$counter]->Skip_H;
					$Skip_I = $result[$counter]->Skip_I;
					$Skip_B = $result[$counter]->Skip_B;
					
					$link_H = '';
					$link_I = '';
					$link_B = '';
					
/*					$test = $result[$counter]->test_id;
					$url = ROOTURL . "/index.php?mod=report&do=candidate_performance&getGraph=1&year=$year&month=$month&test=$test&candidate_id=$candidate_id&type=question_wise";
					
					$url_H = urlencode($url . "&q_level=H");
					$url_I = urlencode($url . "&q_level=I");
					$url_B = urlencode($url . "&q_level=B");

					$link_H = " link='JavaScript:newGraph(\"$url_H\", \"chart2\");' " ;
					$link_I = " link='JavaScript:newGraph(\"$url_I\", \"chart2\");' " ;
					$link_B = " link='JavaScript:newGraph(\"$url_B\", \"chart2\");' " ;
*/					
					$Corr_H_Data .= "<set value='$Corr_H' $link_H />";
					$Corr_I_Data .= "<set value='$Corr_I' $link_I />";
					$Corr_B_Data .= "<set value='$Corr_B' $link_B />";
					
					$Wrng_H_Data .= "<set value='$Wrng_H' $link_H />";
					$Wrng_I_Data .= "<set value='$Wrng_I' $link_I />";
					$Wrng_B_Data .= "<set value='$Wrng_B' $link_B />";
					
					$Skip_H_Data .= "<set value='$Skip_H' $link_H />";
					$Skip_I_Data .= "<set value='$Skip_I' $link_I />";
					$Skip_B_Data .= "<set value='$Skip_B' $link_B />";
					
					$test_names .= "<category name='". $result[$counter]->test_name. "' />";
					$max = ($max > $result[$counter]->total) ? $max : $result[$counter]->total;
				}
				
				$max = getMax($max);
				
				$Output .= " numDivLines='$max'>";
				
				$Corr_H_Data .= "</dataset>";
				$Corr_I_Data .= "</dataset>";
				$Corr_B_Data .= "</dataset>";
				
				$Wrng_H_Data .= "</dataset>";
				$Wrng_I_Data .= "</dataset>";
				$Wrng_B_Data .= "</dataset>";
				
				$Skip_H_Data .= "</dataset>";
				$Skip_I_Data .= "</dataset>";
				$Skip_B_Data .= "</dataset>";
				
				$test_names .= "</categories>";
				
				$Output .= $test_names;
				$Output .= "<dataset>" . $Corr_H_Data . $Wrng_H_Data . $Skip_H_Data . "</dataset>";
				$Output .= "<dataset>" . $Corr_I_Data . $Wrng_I_Data . $Skip_I_Data . "</dataset>";
				$Output .= "<dataset>" . $Corr_B_Data . $Wrng_B_Data . $Skip_B_Data . "</dataset>";
			}
			else
			{
				$Output = " <chart>";
			}	
			break;
		}
		$Output .= '</chart>';
			
		//Set the output header to XML
		header('Content-type: text/xml');
		
		//Send output
		echo $Output;
		exit;
	}
	

	/**
	 * 	Add question history fo match type question
	 */
	function createMatchTypeQuestionHistory($question)
	{
		global $DB;
		$ques_id = $question->id;
		
		$match_data = array();
		$match_data['candidate_id'] = $_SESSION['candidate_id'];
		$match_data['question_id'] = $ques_id;
		$match_data['test_id'] = $_SESSION['testid'];
		$match_data['exam_id'] = $_SESSION['exam_id'];
		$match_data['subject_id'] = $question->subject_id;
			
		$given_answer = $_SESSION['question-'.$ques_id]['given_answer'];
		
		$ids = array();
		$blank = 1;
		if(is_array($given_answer))
		{
			$correct = 1;
			foreach($given_answer as $l => $r)
			{
				$match_data['question_match_left_id'] = $l;
				if($r != '')
				{
					$blank = 0;
					$match_data['given_answer_id'] = $r;
					
					$lcol = $DB->SelectRecord('question_match_left',"id='$l'");
					$answer_id = $lcol->answer_id;
					
					$match_data['correct_question_match_right_id'] = $answer_id;

					$match_data['is_answer_correct'] = 'Y';
					if($answer_id != $r)
					{
						$correct = 0;
						$match_data['is_answer_correct'] = 'N';
					}
				}
				else
				{
					$correct = 0;
					$match_data['given_answer_id'] = 0;
				}
				
				$ids[] = $DB->InsertRecord('candidate_sample_match_question_history',$match_data);
			}
		}
		else
		{
			$left_cols = $DB->SelectRecords('question_match_left', "question_id='$ques_id'");
			foreach($left_cols as $lcol)
			{
				$match_data['given_answer_id'] = 0;
				$match_data['is_correct_answer'] = '';
				$match_data['question_match_left_id'] = $lcol->id;
				$match_data['correct_question_match_right_id'] = $lcol->answer_id;
				
				$ids[] = $DB->InsertRecord('candidate_sample_match_question_history',$match_data);
			}
		}
		
		$return = array('question_id' => $ques_id);
		if($blank == 1)
		{
			$return['no_skipped'] = 1;
		}
		elseif ($correct == 1)
		{
			$return['is_answer_correct'] = 'Y';
			$return['no_correctans'] = 1;
			$return['marksObtained'] = $question->marks;
		}
		else
		{
			$return['is_answer_correct'] = 'N';
		}

		$return['given_answer_id'] = -1;
		$return['correct_answer_id'] = '';
		$return['id'] = $ids;
		
		return $return;
	}
	
	/**
	 * 	Get question status
	 */
	function getQuestionStatus($question)
	{
		global $DB;
		$ques_id = $question->id;
		
		$given_answer = $_SESSION['question-'.$ques_id]['given_answer'];
		$correctansDetails = $DB->SelectRecords('question_answer', "question_id=$ques_id");
		
		$correct_ans = $correctansDetails[0]->answer_id;
		if(count($correctansDetails) > 1)
		{
			$correct_ans = array();
			foreach($correctansDetails as $cans)
			$correct_ans[] = $cans->answer_id;
		}
		
		$return = array('question_id' => $ques_id);
		if($given_answer == '')
		{
			$return['no_skipped'] = 1;
		}
		elseif($given_answer == $correct_ans)
		{
			$return['is_answer_correct'] = 'Y';
			$return['no_correctans'] = 1;
			$return['marksObtained'] = $question->marks;
		}
		else
		{
			$return['is_answer_correct'] = 'N';
		}

		$return['given_answer_id'] = is_array($given_answer) ? implode(',', $given_answer) : $given_answer;
		$return['correct_answer_id'] = is_array($correct_ans) ? implode(',',$correct_ans) : $correct_ans;
		
		return $return;
	}
	function unsetAndRestoreSession()
	{
		global $DB;
		$cand_id = $_SESSION['candidate_id'];
		$test_id = $_SESSION['testid'];
		
		unset($_SESSION);
		session_unset();
		session_destroy();
		
		session_start();
		if($cand_id)
		{
			$user = $DB->SelectRecord('candidate',"id='$cand_id'");
			$_SESSION['candidate_id'] = $cand_id;
			$_SESSION['user_name'] = $user->user_name;
			$_SESSION['candidate_fname'] = $user->first_name;
			$_SESSION['candidate_lname'] = $user->last_name;
			$_SESSION['lastTestId'] = $test_id;
		}
	}
}
?>