<?php 
//defined("ACCESS") or die("Access Restricted");

if(!isset($_SESSION['is_sexam_begin']) || ($_SESSION['is_sexam_begin'] == ''))
{
	Redirect(CreateURL('index.php',"mod=guidelines"));
	exit;
}

include_once('class.sampleresult.php');
$SampleResultOBJ = new SampleResult(); 

global $DB;

$commonArray = array();
$commonArray['candidate_id'] = $candidate_id = $_SESSION['candidate_id'];
$commonArray['ip_adderss'] = $ip_adderss = $_SESSION['ip_adderss'];

$testid = 0;
$mark = 1;

$totalnoQuestion = count($_SESSION['test_id-'.$testid]);
$no_questions = $totalnoQuestion;
$no_skipped = 0;
$no_correctans = 0;
$totalmarks = 0;
$marksObtained = 0;

$ids = array();
$match_ids = array();
foreach($_SESSION['test_id-'.$testid] as $key => $ques_id)
{
	$question = $DB->SelectRecord('question', "id='$ques_id'");
	if($question->question_type == "MT")
	{
		$status = $SampleResultOBJ->createMatchTypeQuestionHistory($question);
		$match_ids[] = implode(',', $status['id']);
		unset($status['id']);
	}
	else
	{
		$status = $SampleResultOBJ->getQuestionStatus($question);
	}
	
	if(isset($status['no_skipped'])) $no_skipped += $status['no_skipped'];
	if(isset($status['no_correctans'])) $no_correctans += $status['no_correctans'];
	if(isset($status['marksObtained'])) $marksObtained += $status['marksObtained'];
	$status['solve_time'] = $_SESSION['question-'.$ques_id]['solve_time'];
	$status['subject_id'] = $question->subject_id;
	$frmdata = $commonArray + $status;
	$ids[] = $DB->InsertRecord('candidate_sample_question_history',$frmdata);	
}

$ids = implode(',', $ids);
$match_ids = implode(',', $match_ids);

$no_wrongans = $totalnoQuestion - ($no_correctans + $no_skipped);
$totalmarks = $totalnoQuestion * $mark; 				// marking for each question is 1
$marksObtained = $no_correctans * $mark; 				// according to correct answer
$percentage = ($marksObtained * 100) / $totalmarks; 
$percentage = number_format($percentage, 2, '.', '');

if($percentage > 40)
{
	$show_result = "Pass";
	$result = 'P';
}
else
{
	$show_result = "Fail";
	$result = 'F';
}

$testhistorydata['candidate_id'] = $_SESSION['candidate_id'];
$testhistorydata['marks_obtained'] = $marksObtained;
$testhistorydata['percentage'] = $percentage;
$testhistorydata['time_taken'] = $_SESSION['time_takenfortest'];
$testhistorydata['result'] = $result;
$testhistorydata['level_selected'] = $_SESSION['level_choosed'];

$test_id = $DB->InsertRecord('candidate_sample_test_history', $testhistorydata);
$data['test_id'] = $test_id;

//upload all the question_history record to add the test_id
$DB->UpdateRecord('candidate_sample_match_question_history', $data, 'id IN('.$match_ids.')');
$DB->UpdateRecord('candidate_sample_question_history', $data, 'id IN('.$ids.')');

unset($_SESSION['is_exam_begin']);
//--------------------

	
		
			
			$CFG->template="SampleResultOBJ/candidate_performance.php";
		


//-----------------------
$CFG->template="sample_result/sample_result.php";
include(TEMP."/index.php");
$SampleResultOBJ->unsetAndRestoreSession();
echo "<script>window.opener.window.location.reload();</script>";

exit;
?>