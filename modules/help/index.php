<?php 
/*****************************************************************************************
 * 		Developed by :- Ashwini Agarwal
 * 		Date         :- November 2, 2012
 * 		Module       :- Feedback
 * 		Purpose      :- Entry file for any action, call in feedback module
 ****************************************************************************************/
defined("ACCESS") or die("Access Restricted");
$DB = new DBFilter();
$do = '';
if(isset($getVars['do']))
$do=$getVars['do'];
$CFG->template="help/help.php";
include(TEMP."/index.php");
exit;
?>